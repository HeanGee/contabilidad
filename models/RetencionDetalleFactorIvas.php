<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Iva;

/**
 * This is the model class for table "cont_retencion_detalle_factor".
 *
 * @property string $id
 * @property string $retencion_id
 * @property string $factor
 * @property string $iva_id
 *
 * @property Retencion $retencion
 * @property Iva $iva
 */
class RetencionDetalleFactorIvas extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_retencion_detalle_factor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['retencion_id', 'iva_id'], 'required'],
            [['retencion_id', 'iva_id'], 'integer'],
//            [['factor'], 'number'],
            [['retencion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Retencion::className(), 'targetAttribute' => ['retencion_id' => 'id']],
            [['iva_id'], 'exist', 'skipOnError' => true, 'targetClass' => Iva::className(), 'targetAttribute' => ['iva_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'retencion_id' => 'Retencion ID',
            'factor' => 'Factor',
            'iva_id' => 'Iva ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRetencion()
    {
        return $this->hasOne(Retencion::className(), ['id' => 'retencion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIva()
    {
        return $this->hasOne(Iva::className(), ['id' => 'iva_id']);
    }
}
