<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%cont_tipo_documento_set}}".
 *
 * @property string $id
 * @property string $nombre
 * @property string $detalle
 * @property string $estado
 *
 * @property TimbradoDetalle[] $detallesTimbrado
 * @property TipoDocumento[] $tiposDocumento
 */
class TipoDocumentoSet extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cont_tipo_documento_set}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['estado'], 'string'],
            [['nombre', 'detalle'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'detalle' => Yii::t('app', 'Detalle'),
            'estado' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetallesTimbrado()
    {
        return $this->hasMany(TimbradoDetalle::className(), ['tipo_documento_set_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiposDocumento()
    {
        return $this->hasMany(TipoDocumento::className(), ['tipo_documento_set_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public static function getTiposDocumentoSet()
    {
        $query = TipoDocumentoSet::find()->where(['estado' => 'activo']);
        return ArrayHelper::map($query->asArray()->all(), 'id', 'nombre');
    }

    public static function getLista($solo_activo = true)
    {
        $query = TipoDocumentoSet::find();
        $query->select(['id', 'text' => 'nombre']);
        if($solo_activo)
            $query->andWhere([
                'estado' => 'activo',
            ]);
        $query->orderBy(['nombre' => SORT_ASC]);
        return $query->asArray()->all();
    }
}
