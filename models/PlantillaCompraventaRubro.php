<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;

/**
 * This is the model class for table "cont_plantilla_compraventa_rubro".
 *
 * @property string $id
 * @property string $plantilla_compraventa_id
 * @property string $rubro_id
 *
 * @property PlantillaCompraventa $plantillaCompraventa
 * @property Rubro $rubro
 */
class PlantillaCompraventaRubro extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_plantilla_compraventa_rubro';
    }

    /**
     * @param $conjuntoA PlantillaCompraventaRubro[]
     * @param $conjuntoB PlantillaCompraventaRubro[]
     * @return array El primer elemento es el conjunto de objetos comunes. El segundo elemento es el conjunto de objetos no comunes.
     * El tercer elemento es el conjunto de objetos no comunes de A, y el tercero es el conjunto de objetos no comunes de B.
     */
    public static function getIntersectionsAndDivergents($conjuntoA, $conjuntoB)
    {
        /** @var PlantillaCompraventaRubro[] $commons */
        /** @var PlantillaCompraventaRubro[] $uncommons */
        /** @var PlantillaCompraventaRubro[] $uncommonsOfA */
        /** @var PlantillaCompraventaRubro[] $uncommonsOfB */

        $commons = [];
        $uncommons = [];
        $uncommonsOfA = [];
        $uncommonsOfB = [];

        foreach ($conjuntoA as $elementoA) {
            if (PlantillaCompraventaRubro::findElement($elementoA, $conjuntoB) != -1) {
                $commons[] = $elementoA;
            } else {
                $uncommons[] = $elementoA;
                $uncommonsOfA[] = $elementoA;
            }
        }

        foreach ($conjuntoB as $elementoB) {
            if (PlantillaCompraventaRubro::findElement($elementoB, $conjuntoA) == -1) {
                $uncommons[] = $elementoB;
                $uncommonsOfB[] = $elementoB;
            }
        }

        return [$commons, $uncommons, $uncommonsOfA, $uncommonsOfB];
    }

    /**
     * @param $needle PlantillaCompraventaRubro
     * @param $conjunto PlantillaCompraventaRubro[]
     * @return int|mixed|string
     */
    public static function findElement($needle, $conjunto)
    {
        foreach ($conjunto as $arrayIndex => $elemento) {
            if ($needle->plantilla_compraventa_id == $elemento->plantilla_compraventa_id && $needle->rubro_id == $elemento->rubro_id)
                return $arrayIndex;
        }

        return -1;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plantilla_compraventa_id', 'rubro_id'], 'required'],
            [['plantilla_compraventa_id', 'rubro_id'], 'integer'],
            [['plantilla_compraventa_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlantillaCompraventa::className(), 'targetAttribute' => ['plantilla_compraventa_id' => 'id']],
            [['rubro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubro::className(), 'targetAttribute' => ['rubro_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plantilla_compraventa_id' => 'Plantilla Compraventa ID',
            'rubro_id' => 'Rubro ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantillaCompraventa()
    {
        return $this->hasOne(PlantillaCompraventa::className(), ['id' => 'plantilla_compraventa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubro()
    {
        return $this->hasOne(Rubro::className(), ['id' => 'rubro_id']);
    }
}
