<?php

namespace backend\modules\contabilidad\models;

/**
 * This is the model class for table "cont_presentacion_iva_rubro".
 *
 * @property string $id
 * @property string $presentacion_iva_id
 * @property string $inciso
 * @property string $columna
 * @property int $casilla
 * @property int $rubro
 * @property int $monto
 * @property int $empresa_id
 * @property int $periodo_contable_id
 *
 * @property PresentacionIva $iva
 */
class PresentacionIvaRubro extends \backend\models\BaseModel
{
    const SCENARIO_RUBRO_4 = 'rubro4';
    const SCENARIO_RUBRO_2 = 'rubro2';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_presentacion_iva_rubro';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_RUBRO_4] = [
            'presentacion_iva_id', 'inciso', 'casilla', 'rubro', 'monto', 'empresa_id', 'periodo_contable_id', 'columna',
        ];
        $scenarios[self::SCENARIO_RUBRO_2] = [
            'presentacion_iva_id', 'inciso', 'casilla', 'rubro', 'monto', 'empresa_id', 'periodo_contable_id', 'columna',
        ];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['presentacion_iva_id', 'inciso', 'casilla', 'rubro', 'monto', 'empresa_id', 'periodo_contable_id'], 'required'],
            [['presentacion_iva_id', 'casilla', 'rubro', 'monto'], 'integer'],
            [['empresa_id', 'periodo_contable_id'], 'safe'],
            [['inciso'], 'string', 'max' => 2],
            [['columna'], 'string', 'max' => 3],
            [['columna'], 'required', 'on' => self::SCENARIO_DEFAULT],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'presentacion_iva_id' => 'Presentacion Iva ID',
            'inciso' => 'Inciso',
            'casilla' => 'Casilla',
            'rubro' => 'Rubro',
            'monto' => 'Monto',
            'columna' => 'Columna',
        ];
    }

    public function getAllErrorsAsString()
    {
        $msg = '';
        foreach ($this->getErrorSummary(true) as $error) {
            $msg .= substr($error, 0, (strlen($error) - 1)) . ', ';
        }

        return substr($msg, 0, (strlen($msg) - 2)) . '.';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIva()
    {
        return $this->hasOne(PresentacionIva::className(), ['id' => 'presentacion_iva_id']);
    }

    public function loadDefaults()
    {
        $this->empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
    }

    public static function getScenarioBy($nro_rubro)
    {
        switch ($nro_rubro) {
            case 2:
                return self::SCENARIO_RUBRO_2;
            case 4:
                return self::SCENARIO_RUBRO_4;
        }
        return self::SCENARIO_DEFAULT;
    }
}
