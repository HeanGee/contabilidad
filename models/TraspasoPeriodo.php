<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use yii\db\ActiveQuery;

/**
 * Class TraspasoPeriodo
 * @package backend\modules\contabilidad\models
 *
 * @property string $rango
 * @property array $balance
 * @property AsientoDetalle[] $asientos
 * @property string $asiento_cierre
 * @property string $asiento_apertura
 * @property string $siguiente_periodo_id
 * @property string $periodo_actual_id
 * @property boolean $balanceExists
 */
class TraspasoPeriodo extends BaseModel
{
    public $rango = null;
    public $balance = null;
    public $arboles = null;
    public $asientos = [];
    public $asiento_cierre;
    public $asiento_apertura;
    public $siguiente_periodo_id;
    public $periodo_actual_id;
    private $balanceExists = false;
    public $monto_debe = 0;
    public $monto_haber = 0;

    public function __construct($anho, $config = [])
    {
        $this->rango = "01-01-$anho - 31-12-$anho";
        $this->asiento_apertura = $this->asiento_cierre = '1';

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['asiento_cierre', 'asiento_apertura', 'siguiente_periodo_id', 'periodo_actual_id'], 'safe'],

            [['periodo_actual_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_actual_id' => 'id']],
            [['siguiente_periodo_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['siguiente_periodo_id' => 'id']],

            [['siguiente_periodo_id'], 'required', 'when' => function ($model) {
                return $model->asiento_apertura == 1;
            }, 'whenClient' => "function (attribute, value) {
                return $('input[id$=\"asiento_apertura\"]').val() === '1';
            }"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'periodo_actual_id' => "Periodo actual",
            'siguiente_periodo_id' => "Siguiente Periodo",
        ];
    }

    /**
     * @return EmpresaPeriodoContable
     */
    public function getPeriodoActual()
    {
        return EmpresaPeriodoContable::findOne(['id' => $this->periodo_actual_id]);
    }

    /**
     * @return EmpresaPeriodoContable
     */
    public function getSiguientePeriodo()
    {
        return EmpresaPeriodoContable::findOne(['id' => $this->siguiente_periodo_id]);
    }

    public function setBalance($empresa_id, $periodo_id)
    {
        $desde = $this->getDesde();
        $hasta = $this->getHasta();

        /** @var ActiveQuery $query */
        $query = AsientoDetalle::find()->alias('detalle')
            ->joinWith('asiento as asiento')
            ->where([
                'asiento.empresa_id' => $empresa_id,
                'asiento.periodo_contable_id' => $periodo_id
            ])
            ->leftJoin('cont_plan_cuenta cuenta', 'cuenta.id = detalle.cuenta_id')
            ->andWhere(['BETWEEN', 'asiento.fecha', $desde, $hasta])
            ->select([
                'cuenta_id' => 'MIN(cuenta_id)',
                'cuenta_codigo' => 'MIN(cod_completo)',
                'cuenta_nombre' => 'MIN(cuenta.nombre)',
                'debe' => 'SUM(detalle.monto_debe)',
                'haber' => 'SUM(detalle.monto_haber)',
                'saldo' => 'SUM(detalle.monto_debe)-SUM(detalle.monto_haber)',

                'asiento_id' => "MIN(asiento.asiento_id)", // exige que exista asiento_id asi que cualquier cosa se le carga.
            ])
            ->groupBy('cuenta_id')
            ->orderBy('cod_ordenable ASC');

        if ($query->exists()) {
            $this->balanceExists = true;
            $this->balance = $query->asArray()->all();
            $this->setArboles();
        } else {
            $this->balanceExists = false;
        }
    }

    public function getDesde($de_DE = false)
    {
        if ($this->rango == "") return "";

        $slices = explode(' - ', $this->rango);
        if ($de_DE)
            return $slices[0];
        return implode('-', array_reverse(explode('-', $slices[0])));
    }

    public function getHasta($de_DE = false)
    {
        if ($this->rango == "") return "";

        $slices = explode(' - ', $this->rango);
        if ($de_DE)
            return $slices[1];
        return implode('-', array_reverse(explode('-', $slices[1])));
    }

    public function verifyDate()
    {
        return preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{4}\ -\ [0-9]{2}-[0-9]{2}-[0-9]{4}$/', $this->rango);
    }

    public function isBalanceSetted()
    {
        return $this->balanceExists;
    }

    public function setArboles()
    {
        if (!isset($this->arboles)) {
            $cods_cta = [];
            for ($i = 1; $i < 50; $i++) $cods_cta[] = "{$i}";
            $cuentas = PlanCuenta::find()->where(['IN', 'cod_completo', $cods_cta])->orderBy('cod_ordenable ASC')->all();
            $arboles = [];
            foreach ($cuentas as $cuenta) {
                $nodo = new Nodo($cuenta, $this->balance);
                $nodo->root = true;
                $arboles[] = $nodo;
            }
            $this->arboles = $arboles;
        }
    }

    /**
     * @param Nodo $nodo
     * @param TraspasoPeriodo $traspasoPeriodo
     */
    static function procesarNodo($nodo, &$traspasoPeriodo)
    {
        if ($nodo->monto != 0 && $nodo->cuenta->asentable == 'si') {
            $asiento = new AsientoDetalle();
            $asiento->monto_debe = ($nodo->monto > 0 ? abs($nodo->monto) : 0);
            $asiento->monto_haber = ($nodo->monto < 0 ? abs($nodo->monto) : 0);
            $asiento->cuenta_id = $nodo->cuenta->id;

            $traspasoPeriodo->asientos[] = $asiento;
            $traspasoPeriodo->monto_debe += (int)$asiento->monto_debe;
            $traspasoPeriodo->monto_haber += (int)$asiento->monto_haber;
        }

        foreach ($nodo->hijos as $hijo) {
            self::procesarNodo($hijo, $traspasoPeriodo);
        }
    }

    public function generarAsiento()
    {
        if (empty($this->asientos)) {
            foreach ($this->arboles as $nodo) {
                self::procesarNodo($nodo, $this);
            }
        }

        $debes = $haberes = [];
        foreach ($this->asientos as $asiento) {
            if ($asiento->monto_debe > 0) $debes[] = $asiento;
            else $haberes[] = $asiento;
        }

        $this->asientos = array_merge($debes, $haberes);
    }
}
