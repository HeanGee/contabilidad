<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "cont_parametro_sistema".
 *
 * @property string $id
 * @property string $nombre
 * @property string $valor
 */
class ParametroSistema extends BaseModel
{
    public $regex;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cont_parametro_sistema';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'valor'], 'required'],
            [['nombre', 'valor'], 'string', 'max' => 2000],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'valor' => Yii::t('app', 'Valor'),
        ];
    }
    
    public static function getKeysPlantillaVenta() {
        $json_plantillas_venta = ParametroSistema::findOne(['nombre' => 'cuentas_venta'])->valor;
        $plantillas = Json::decode($json_plantillas_venta);
        $keys = array_keys($plantillas);
        $keyvalue = [];

        foreach($keys as $key)
            $keyvalue[$key] = ucfirst($key);

        return $keyvalue;
    }

    public static function getValorByNombre($nombre)
    {
        $model = ParametroSistema::find()->where(['nombre' => $nombre])->one();
        return $model ? $model->valor : '';
    }

    public static function getEmpresaActualCoefCosto()
    {
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-coeficiente_costo";
        $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);

        return isset($parametro_sistema) ? $parametro_sistema->valor : null;
    }
}
