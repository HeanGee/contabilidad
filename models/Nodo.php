<?php

namespace backend\modules\contabilidad\models;

use yii\base\Model;

/**
 * Class Nodo
 *
 * @property PlanCuenta $cuenta
 * @property Nodo $padre
 * @property int $monto
 * @property Nodo[] $hijos
 * @property array $balance
 * @property boolean $root
 * @property boolean $imprimir
 *
 */
class Nodo extends Model
{
    public $cuenta;
    public $padre;
    public $monto;
    public $hijos = [];
    public $balance;
    public $root = false;
    public $imprimir = false;

    public function __construct($cuenta_root = null, $balance = [])
    {
        parent::__construct();

        $this->monto = 0;
        $this->padre = null;
        $this->hijos = [];
        $this->root = false;
        $this->imprimir = false;

        $this->balance = $balance;

        // instanciar root e hijos
        if (isset($cuenta_root) && $cuenta_root instanceof PlanCuenta) {
            $this->padre = null; // o hacer unset($this->padre);
            $this->cuenta = $cuenta_root;

            $this->loadHijos($balance);
            $this->loadMontoFromBalance();
            $this->carryUpMonto();
        }
    }

    /**
     * @return Nodo[]
     */
    public function getHijos()
    {
        return $this->hijos;
    }

    public function loadHijos($balance)
    {
        if (!isset($this->cuenta)) return;

        $this->hijos = [];
        foreach (PlanCuenta::find()->where(['padre_id' => $this->cuenta->id])->orderBy(['cod_ordenable' => SORT_ASC])->all() as $cuenta_hijo) {
            $hijo = new Nodo(null, $balance);
            $hijo->cuenta = $cuenta_hijo;
            $hijo->padre = $this;

            $this->hijos[] = $hijo;
        }

        foreach ($this->hijos as $hijo) {
            if ($hijo->cuenta->flujo_efectivo != 'no') {
                $hijo->imprimir = true;
                $hijo->imprimirPadre();
            }
            $hijo->loadHijos($balance);
        }
    }

    private function imprimirPadre()
    {
        if ($this->hasPadre() && !$this->padre->imprimir) {
            $this->padre->imprimir = true;
            $this->padre->imprimirPadre();
        }
    }

    public function loadMontoFromBalance()
    {
        if (!isset($this->balance) || empty($this->balance)) return;

        // root no puede estar asentado.

        // recorrer todos los hijos de esta generacion y cargar monto si hay en el balance.
        foreach ($this->hijos as $hijo) {
            foreach ($this->balance as $balance) {
                $ida = $hijo->cuenta->id;
                $idb = $balance['cuenta_id'];

                if ($ida == $idb) {
                    $hijo->monto = (int)$balance['saldo'];
                }
            }
        }

        // Por cada hijo de la generacion actual, recursion
        foreach ($this->hijos as $hijo) {
            $hijo->loadMontoFromBalance();
        }
    }

    public function carryUpMonto()
    {
        if (empty($this->hijos) || !isset($this->hijos)) {
//            return $this->monto;
//            $this->padre->monto += (int)$this->monto;
            return;
        }

        foreach ($this->hijos as $hijo) {
//            $hijo->monto = $hijo->carryUpMonto();
            $hijo->carryUpMonto();
            $hijo->padre->monto += $hijo->monto;
        }
        return;
    }

    public function carryUpMontoFlujoEfectivo()
    {
        if (empty($this->hijos) || !isset($this->hijos)) {
            return;
        }

        foreach ($this->hijos as $hijo) {
            $hijo->carryUpMontoFlujoEfectivo();
            $hijo->padre->monto += empty($hijo->hijos) ? (($hijo->cuenta->flujo_efectivo != 'no') ? $hijo->monto : 0) : $hijo->monto;
        }
        return;
    }

    public function resetMonto()
    {
        if (empty($this->hijos)) return;

        $this->monto = 0;
        foreach ($this->hijos as $hijo) {
            $hijo->resetMonto();
        }
    }

    public function hasPadre()
    {
        return isset($this->padre);
    }
}