<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;

/**
 * This is the model class for table "cont_empresa_sub_obligacion".
 *
 * @property int $id
 * @property int $sub_obligacion_id
 * @property string $empresa_id
 *
 * @property SubObligacion $subObligacion
 * @property Empresa $empresa
 */
class EmpresaSubObligacion extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_empresa_sub_obligacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_obligacion_id', 'empresa_id'], 'required'],
            [['sub_obligacion_id', 'empresa_id'], 'integer'],
            [['sub_obligacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubObligacion::className(), 'targetAttribute' => ['sub_obligacion_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sub_obligacion_id' => 'Sub Obligacion ID',
            'empresa_id' => 'Empresa ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubObligacion()
    {
        return $this->hasOne(SubObligacion::className(), ['id' => 'sub_obligacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }
}
