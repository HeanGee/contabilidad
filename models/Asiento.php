<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use common\models\User;
use Exception;

/**
 * This is the model class for table "cont_asiento".
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $periodo_contable_id
 * @property string $fecha
 * @property int $monto_debe
 * @property int $monto_haber
 * @property string $concepto
 * @property int $usuario_id
 * @property string $creado
 * @property string $modulo_origen
 * @property string $asiento_id
 * @property string $ref_esquema_bd
 *
 * @property EmpresaPeriodoContable $periodoContable
 * @property Empresa $empresa
 * @property User $usuario
 * @property AsientoDetalle[] $asientoDetalles
 * @property Venta[] $ventas
 * @property Compra[] $compras
 * @property string $montoDebeFormatted
 * @property string $montoHaberFormatted
 * @property string $fechaFormatted
 * @property long $totalDebe
 * @property long $totalHaber
 */
class Asiento extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_asiento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'periodo_contable_id', 'fecha', 'monto_debe', 'monto_haber', 'usuario_id', 'creado'], 'required'],
            [['empresa_id', 'periodo_contable_id', 'monto_debe', 'monto_haber', 'usuario_id', 'asiento_id'], 'integer'],
            [['fecha', 'creado'], 'safe'],
            [['concepto', 'modulo_origen', 'ref_esquema_bd'], 'string'],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['usuario_id' => 'id']],
            [['asiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return self::getAttributesLabelText();
    }

    public static function getAttributesLabelText()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa',
            'periodo_contable_id' => 'Periodo Contable',
            'fecha' => 'Fecha',
            'monto_debe' => 'Debe',
            'monto_haber' => 'Haber',
            'concepto' => 'Concepto',
            'usuario_id' => 'Usuario',
            'creado' => 'Creado',
            'modulo_origen' => 'Modulo de Origen',
            'asiento_id' => "Asiento Id",
        ];
    }

    public function getDetallesId()
    {
        $ids = [];
        foreach ($this->asientoDetalles as $item) {
            $ids[] = $item->id;
        }
        return $ids;
    }

    public static function manejarTextoAuditoria($campo, &$viejo, &$nuevo)
    {
        switch ($campo) {
            case 'empresa_id':
                $viejo[0] = !empty($viejo[0]) ? Empresa::findOne($viejo[0])->razon_social : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Empresa::findOne($nuevo[0])->razon_social : $nuevo[0];
                return true;
            case 'usuario_id':
                $viejo[0] = !empty($viejo[0]) ? User::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? User::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'asiento_id':
                $viejo[0] = !empty($viejo[0]) ? Asiento::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Asiento::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'periodo_contable_id':
                $viejo[0] = !empty($viejo[0]) ? EmpresaPeriodoContable::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? EmpresaPeriodoContable::findOne($nuevo[0])->id : $nuevo[0];
                return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsientoDetalles()
    {
        return $this->hasMany(AsientoDetalle::className(), ['asiento_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compra::className(), ['asiento_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Venta::className(), ['asiento_id' => 'id']);
    }

    public function isBorrable()
    {
        return $this->asiento_id == null;
    }

    public function getMontoDebeFormatted()
    {
        return number_format(round($this->monto_debe), 0, ',', '.');
    }

    public function getMontoHaberFormatted()
    {
        return number_format(round($this->monto_haber), 0, ',', '.');
    }

    public function getFechaFormatted()
    {
        return date_create_from_format('Y-m-d', $this->fecha)->format('d-m-Y');
    }

    public function getTotalDebe()
    {
        $total = $this->getAsientoDetalles()->sum('monto_debe');
        return number_format(round($total), 0, ',', '.');
    }

    public function getTotalHaber()
    {
        $total = $this->getAsientoDetalles()->sum('monto_haber');
        return number_format(round($total), 0, ',', '.');
    }

    public function balance()
    {
        $sumatory = 0;
        $debe = 0;
        $haber = 0;

        foreach ($this->asientoDetalles as $asientoDetalle) {
            $debe += round($asientoDetalle->monto_debe);
            $haber += round($asientoDetalle->monto_haber);
        }

        $this->monto_debe = $debe;
        $this->monto_haber = $haber;

        $sumatory = $debe - $haber;
        if ($sumatory != 0) {
            $debe = number_format($debe, 0, '', '.');
            $haber = number_format($haber, 0, '', '.');
            $this->addError('monto_debe', "El asiento no está balanceado: Debe = {$debe}, Haber = {$haber}");
        }

        return !$this->hasErrors();
    }

    public function setDebeHaberFromDetalles()
    {
        $this->monto_haber = $this->monto_debe = 0;
        foreach ($this->asientoDetalles as $asientoDetalle) {
            $this->monto_debe += $asientoDetalle->monto_debe;
            $this->monto_haber += $asientoDetalle->monto_haber;
        }
    }

    /**
     * Verifica si el periodo pasado como parametro tiene datos cargados.
     *
     * Retorna mensaje indicando cual es el dato registrado, o false en caso contrario.
     *
     * @param $periodo_id
     * @param $empresa_id
     * @param bool $excludeSelf
     * @return bool|string
     * @throws Exception
     */
    public function hasData($periodo_id, $empresa_id, $excludeSelf = true)
    {
        $fname = __FUNCTION__;

        if (!isset($periodo_id)) {
            throw new Exception("Error desde $fname: El parámetro `\$periodo_id` no puede ser nulo.");
        }

        if (!isset($empresa_id)) {
            throw new Exception("Error desde $fname: El parámetro `\$empresa_id` no puede ser nulo.");
        }

        $query = EmpresaPeriodoContable::find()->where(['id' => $periodo_id]);
        if (!$query->exists())
            throw new Exception("No existe periodo contable con el id `$periodo_id`.");

        $query = Venta::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay facturas de venta registradas en el periodo.");
        $query = Compra::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay facturas de compra registradas en el periodo.");
        $query = Recibo::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay recibos registrados en el periodo.");
        $query = Retencion::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay retenciones registradas en el periodo.");
        $query = Prestamo::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay préstamos registrados en el periodo.");
        $query = ActivoFijo::find()->where(['empresa_periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id])
            ->andWhere(['IS', 'activo_fijo_id', null]);  # activos fijos creados y no traspasados.
        if ($query->exists())
            return ("Hay activos fijos (nuevos) registrados en el periodo.");
        $query = ActivoBiologico::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay activos biológicos registrados en el periodo.");
        $query = Asiento::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($excludeSelf)
            $query->andWhere(['!=', 'id', $this->id]);
        if ($query->exists())
            return ("Hay asientos registrados en el periodo.");
        $query = CoeficienteRevaluo::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay coeficientes de revalúo registrados en el periodo.");
        $query = Poliza::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay pólizas registradas en el periodo.");
        $query = PolizaDevengamiento::find()->where(['periodo_contable_id' => $periodo_id]);
        if ($query->exists())
            return ("Hay devengamiento de pólizas de seguros registrados en el periodo.");
        $query = ReciboPrestamo::find()->where(['periodo_contable_id' => $periodo_id, 'empresa_id' => $empresa_id]);
        if ($query->exists())
            return ("Hay recibos de préstamos registrados en el periodo.");

        return false;
    }
}

