<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;

/**
 * This is the model class for table "cont_activo_fijo_tipo".
 *
 * @property int $id
 * @property string $nombre
 * @property int $vida_util
 * @property int $cuenta_id
 * @property int $cuenta_depreciacion_id
 * @property string $estado
 * @property string $cantidad_requerida
 *
 * @property PlanCuenta $cuenta
 * @property PlanCuenta $cuentaDepreciacion
 * @property ActivoFijoAtributo[] $atributos
 * @property ActivoFijo[] $activoFijos
 */
class ActivoFijoTipo extends BaseModel
{
    public $tmp_cuenta;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_activo_fijo_tipo';
    }

    public static function getValoresEnum($columna)
    {
        $_retornar = [];
        try {
            $valores_enum = self::getTableSchema()->columns[$columna]->enumValues;
            foreach ($valores_enum as $_v)
                $_retornar[$_v] = ucfirst($_v);

        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
        }
        return $_retornar;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'vida_util', 'cuenta_id', 'estado', 'cuenta_depreciacion_id', 'cantidad_requerida'], 'required'],
            [['vida_util', 'cuenta_id', 'cuenta_depreciacion_id'], 'integer'],
            [['estado'], 'string'],
            [['nombre'], 'string', 'max' => 255],
            [['cantidad_requerida'], 'default', 'value' => 'si'],
            [['estado'], 'default', 'value' => 'activo'],
            [['cuenta_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['cuenta_id' => 'id']],
            [['cuenta_depreciacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['cuenta_depreciacion_id' => 'id']],
            [['tmp_cuenta'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'vida_util' => 'Vida Util',
            'cuenta_id' => 'Cuenta',
            'estado' => 'Estado',
            'tmp_cuenta' => 'Cuenta'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuenta()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'cuenta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaDepreciacion()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'cuenta_depreciacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoFijos()
    {
        return $this->hasMany(ActivoFijo::className(), ['activo_fijo_tipo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtributos()
    {
        return $this->hasMany(ActivoFijoTipoAtributo::className(), ['activo_fijo_tipo_id' => 'id']);
    }

    public function getActivosFijos()
    {
        return $this->hasMany(ActivoFijo::className(), ['activo_fijo_tipo_id' => 'id']);
    }
}
