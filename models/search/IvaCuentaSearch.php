<?php

namespace backend\modules\contabilidad\models\search;

use backend\models\Iva;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\contabilidad\models\IvaCuenta;

/**
 * IvaCuentaSearch represents the model behind the search form of `backend\modules\contabilidad\models\IvaCuenta`.
 */
class IvaCuentaSearch extends IvaCuenta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'iva_id', 'cuenta_compra_id', 'cuenta_venta_id', ], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IvaCuenta::find();
        $iva_cta = IvaCuenta::find();
        $iva = Iva::findOne(['porcentaje' => 0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'iva_id' => $this->iva_id,
            'cuenta_compra_id' => $this->cuenta_compra_id,
            'cuenta_venta_id' => $this->cuenta_venta_id,
        ]);
        
        if($iva != null) {
            $iva_cta->where(['iva_id' => $iva->id]);

            if(!$iva_cta->exists())
                FlashMessageHelpsers::createErrorMessage('Es ESTRICTAMENTE NECESARIO definir un IVA Cuenta para IVA 0%');
        }

        return $dataProvider;
    }
}
