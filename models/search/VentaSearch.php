<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Venta;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VentaSearch represents the model behind the search form of `backend\modules\contabilidad\models\Venta`.
 *
 * @property string $fecha_emision_hasta
 */
class VentaSearch extends Venta
{
    public $partida_correcto;
    public $fecha_emision_hasta;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tipo_documento_id', 'entidad_id', 'moneda_id', 'empresa_id', 'obligaciones', 'timbrado_detalle_id', 'periodo_contable_id'], 'integer'],
            [['fecha_emision', 'condicion', 'nro_factura', 'fecha_vencimiento', 'nombre_documento', 'nro_completo',
                'estado', 'moneda_nombre', 'nombre_entidad', 'nro_timbrado', 'timbrado_detalle_id', 'tipo_documento_id',
                'tipo', 'creado', 'partida_correcto', 'asiento_id', 'fecha_emision_hasta'], 'safe'],
            [['total', 'saldo'], 'number'],
            [['nro_factura'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param bool $es_nota
     * @return ActiveDataProvider
     */
    public function search($params, $es_nota = false)
    {
        $tname = Venta::tableName();
        $concat = "CONCAT(({$tname}.prefijo), ('-'), ({$tname}.nro_factura))";
        $query = Venta::find();
        $query
            ->joinWith('moneda as moneda')
            ->joinWith('tipoDocumento as tipodoc')
            ->joinWith('entidad as entidad')
            ->joinWith('timbradoDetalle as timbradod')
            ->leftJoin('cont_timbrado AS timbrado', 'timbrado.id = timbradod.timbrado_id')
            ->select([
                $tname . '.id',
                $tname . '.nro_factura',
                $tname . '.prefijo',
                $tname . '.fecha_emision',
                $tname . '.condicion',
                $tname . '.total',
                $tname . '.saldo',
                $tname . '.moneda_id',
                $tname . '.tipo_documento_id',
                $tname . '.timbrado_detalle_id',
                $tname . '.entidad_id',
                $tname . '.asiento_id',
                $tname . '.estado',
                $tname . '.tipo',
                $tname . '.bloqueado',
//                'tipodoc.id', // indicar el id de la tabla relacionada, extranhamente hace que solo se muestre 1 sola fila.
//                'tipodoc.nombre',
                'timbrado.nro_timbrado',
                'timbradod.timbrado_id',
                $concat . ' AS nro_completo'
            ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['nro_completo' => SORT_DESC]]
        ]);
        $dataProvider->sort->attributes['nombre_documento'] = [
            'asc' => ['tipodoc.nombre' => SORT_ASC],
            'desc' => ['tipodoc.nombre' => SORT_DESC],
            'default' => SORT_ASC
        ];
        $dataProvider->sort->attributes['nro_completo'] = [
            'asc' => [$concat => SORT_ASC],
            'desc' => [$concat => SORT_DESC],
            'default' => SORT_ASC
        ];
        $dataProvider->sort->attributes['moneda_id'] = [
            'asc' => ['moneda.id' => SORT_ASC],
            'desc' => ['moneda.id' => SORT_DESC],
            'default' => SORT_ASC
        ];
        $dataProvider->sort->attributes['nombre_entidad'] = [
            'asc' => ['entidad.razon_social' => SORT_ASC],
            'desc' => ['entidad.razon_social' => SORT_DESC],
            'default' => SORT_ASC
        ];
        $dataProvider->sort->attributes['nro_timbrado'] = [
            'asc' => ['timbradod.timbrado.nro_timbrado' => SORT_ASC],
            'desc' => ['timbradod.timbrado.nro_timbrado' => SORT_DESC],
            'default' => SORT_ASC
        ];
        $dataProvider->sort->attributes['fecha_emision_hasta'] = [
            'asc' => ['timbradod.timbrado.fecha_emision' => SORT_ASC],
            'desc' => ['timbradod.timbrado.fecha_emision' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $msg = '';
            foreach ($this->getErrorSummary(true) as $error) {
                $msg .= $error;
            }
            FlashMessageHelpsers::createWarningMessage($msg);
            return $dataProvider;
        }

//        // grid filtering conditions
//        $query->andWhere([
//            'or', ['tipo_documento_id' => null], //['tipo_entidad' => $entidad]
//        ]);

        // grid filtering conditions
        $query->andFilterWhere(['periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        $query->andFilterWhere([
            $tname . '.id' => $this->id,
            'fecha_vencimiento' => $this->fecha_vencimiento,
            'entidad_id' => $this->entidad_id,
            'moneda_id' => $this->moneda_id > 0 ? $this->moneda_id : null,
            'tipo_documento_id' => $this->tipo_documento_id > 0 ? $this->tipo_documento_id : null,
            'nro_factura' => trim($this->nro_factura),
            'obligaciones' => $this->obligaciones,
            'total' => $this->total,
            'saldo' => $this->saldo,
            'empresa_id' => Yii::$app->getSession()->get('core_empresa_actual'),
        ]);

        $query->andFilterWhere(['like', $tname . '.condicion', $this->condicion != 'todos' ? $this->condicion : '']);
        $query->andFilterWhere(['OR', ['like', 'entidad.razon_social', $this->nombre_entidad], ['like', 'entidad.ruc', $this->nombre_entidad]]);
        $query->andFilterWhere(['like', 'timbrado.nro_timbrado', $this->nro_timbrado]);
        $query->andFilterWhere(['like', $concat, $this->nro_completo]);
        $query->andFilterWhere(['=', $tname . '.estado', $this->estado == 'todos' ? null : $this->estado]);

        if (!empty($this->fecha_emision)) {
            if (empty($this->fecha_emision_hasta))
                $query->andFilterWhere(['=', 'cont_factura_venta.fecha_emision', date('y-m-d', strtotime($this->fecha_emision))]);
            else {
                $query->andFilterWhere(['>=', 'cont_factura_venta.fecha_emision', date('y-m-d', strtotime($this->fecha_emision))]);
                $query->andFilterWhere(['<=', 'cont_factura_venta.fecha_emision', date('y-m-d', strtotime($this->fecha_emision_hasta))]);
            }
        } else {
            ($this->fecha_emision_hasta == '') || $query->andFilterWhere(['<=', 'cont_factura_venta.fecha_emision', date('y-m-d', strtotime($this->fecha_emision_hasta))]);
        }

        if ($es_nota) {
            $query->andWhere(['OR', ['AND', ['IS', 'factura_venta_id', null], ['=', 'tipo', 'nota_credito']], ['NOT', ['factura_venta_id' => null]]]);
            $this->tipo == 'todos' || $query->andFilterWhere(['LIKE', 'cont_factura_venta.tipo', $this->tipo]);
        } else {
            $query->andWhere(['cont_factura_venta.`tipo`' => 'factura']);
        }

        #Creado
        $creado = implode('-', array_reverse(explode('-', $this->creado)));
        $query->andFilterWhere(['LIKE', 'cont_factura_venta.creado', $creado]);

        #Asentado o no
        if ($this->asiento_id != '')
            $query->andWhere([($this->asiento_id == 'si' ? "IS NOT" : "IS"), 'cont_factura_venta.asiento_id', null]);

        return $dataProvider;
    }
}
