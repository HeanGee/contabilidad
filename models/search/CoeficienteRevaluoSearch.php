<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\CoeficienteRevaluo;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CoeficienteRevaluoSearch represents the model behind the search form of `backend\modules\contabilidad\models\CoeficienteRevaluo`.
 */
class CoeficienteRevaluoSearch extends CoeficienteRevaluo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return  [
                [['id', 'resolucion'], 'integer'],
                [['periodo'], 'safe'],
                ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoeficienteRevaluo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider(['query' => $query,]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $periodo = null;
        if (!empty($this->periodo)) {
            $periodo = $this->periodo;
        }
        $query->andFilterWhere(['id' => $this->id, 'periodo' => $periodo,]);

        $query->andFilterWhere(['like', 'resolucion', $this->resolucion]);

        //filtrar por empresa actual y periodo contable actual
        $query->andFilterWhere(['empresa_id' => Yii::$app->session->get('core_empresa_actual')]);
        $query->andFilterWhere(['periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);


        return $dataProvider;
    }
}
