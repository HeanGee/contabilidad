<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ActivoBiologicoEspecieClasificacionSearch represents the model behind the search form of `backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion`.
 */
class ActivoBiologicoEspecieClasificacionSearch extends ActivoBiologicoEspecieClasificacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'activo_biologico_especie_id', 'empresa_id', 'periodo_contable_id'], 'integer'],
            [['nombre', 'descripcion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivoBiologicoEspecieClasificacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'activo_biologico_especie_id' => $this->activo_biologico_especie_id,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
