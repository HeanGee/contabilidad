<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\PlantillaCompraventa;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PlantillaCompraventaSearch represents the model behind the search form of `backend\modules\contabilidad\models\PlantillaCompraventa`.
 */
class PlantillaCompraventaSearch extends PlantillaCompraventa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nombre', 'tipo', 'estado', 'costo_mercad', 'deducible'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $tipo = null)
    {
        $query = PlantillaCompraventa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'tipo', $tipo != null ? $tipo : $this->tipo])
            ->andFilterWhere(['=', 'estado', $this->estado != null ? $this->estado : 'activo'])
            ->andFilterWhere(['=', 'deducible', $this->deducible])
            ->andFilterWhere(['like', 'costo_mercad', $this->costo_mercad]);

        return $dataProvider;
    }
}
