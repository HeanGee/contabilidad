<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\PermisosHelpers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PlanCuentaSearch represents the model behind the search form of `backend\modules\contabilidad\models\PlanCuenta`.
 */
class PlanCuentaSearch extends PlanCuenta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'padre_id'], 'integer'],
            [['empresa_id', 'cod_cuenta', 'nombre', 'asentable', 'cod_completo', 'estado', 'cod_ordenable', 'es_global'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlanCuenta::find()->alias('cuenta');
        $query->joinWith('empresa as empresa');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['cod_ordenable' => SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['empresa_id'] = [
            'asc' => ['empresa.razon_social' => SORT_ASC],
            'desc' => ['empresa.razon_social' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cuenta.id' => $this->id,
            'cuenta.padre_id' => $this->padre_id,
        ]);

        $query->andFilterWhere(['OR', ['=', 'empresa_id', $this->empresa_id], ['like', 'empresa.razon_social', $this->empresa_id]]);
        $query->andFilterWhere(['like', 'cuenta.cod_cuenta', $this->cod_cuenta])
            ->andFilterWhere(['like', 'cuenta.nombre', $this->nombre])
            ->andFilterWhere(['like', 'cuenta.asentable', $this->asentable])
            ->andFilterWhere(['cuenta.cod_completo' => $this->cod_ordenable])// cruzado
            ->andFilterWhere(['like', 'cuenta.estado', $this->estado]);

        $empresa_actual = Yii::$app->session->get('core_empresa_actual');

        if ($this->es_global == 'si') {
            $query->andWhere(['cuenta.empresa_id' => null]);
        } elseif ($this->es_global == 'no') {
            if (empty($empresa_actual))
                $query->andWhere(['cuenta.empresa_id' => null]);
            else
                if (PermisosHelpers::esSuperUsuario())
                    $query->andWhere(['IS NOT', 'cuenta.empresa_id', null]);
                else
                    $query->andWhere(['cuenta.empresa_id' => $empresa_actual]);
        }

//        if(empty($empresa_actual)){
//            $query->andWhere(['empresa_id' => null]);
//        }else{
//            $query->andWhere(['OR', ['empresa_id' => null], ['empresa_id' => $empresa_actual]]);
//        }

        return $dataProvider;
    }
}
