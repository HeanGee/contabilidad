<?php

namespace backend\modules\contabilidad\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\contabilidad\models\CuotaVenta;

/**
 * CuotaVentaSearch represents the model behind the search form of `backend\modules\contabilidad\models\CuotaVenta`.
 */
class CuotaVentaSearch extends CuotaVenta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'factura_venta_id', 'nro_cuota'], 'integer'],
            [['fecha_vencimiento', 'pagado'], 'safe'],
            [['monto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CuotaVenta::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'factura_venta_id' => $this->factura_venta_id,
            'fecha_vencimiento' => $this->fecha_vencimiento,
            'monto' => $this->monto,
            'nro_cuota' => $this->nro_cuota,
        ]);

        $query->andFilterWhere(['like', 'pagado', $this->pagado]);

        return $dataProvider;
    }
}
