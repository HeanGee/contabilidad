<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\TipoDocumento;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * CompraSearch represents the model behind the search form of `backend\modules\contabilidad\models\Compra`.
 *
 * @property $autofactura string
 */
class CompraSearch extends Compra
{
    public $fecha_emision_f;
    public $fecha_emision_hasta;
    public $partida_correcto;
    public $autofactura;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tipo_documento_id', 'entidad_id', 'obligaciones', 'periodo_contable_id', 'factura_compra_id'], 'integer'],
            [['fecha_emision', 'condicion', 'fecha_vencimiento', 'nro_factura', 'nombre_documento', 'timbrado',
                'fecha_emision_f', 'creado', 'nombre_entidad', 'moneda_id', 'estado', 'tipo', 'asiento_id', 'autofactura', 'fecha_emision_hasta'], 'safe'],
            [['total', 'saldo'], 'number'],
            [['para_iva'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param bool $es_nota
     *
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
    public function search($params, $es_nota = false)
    {
        $query = Compra::find();
        $query->joinWith('tipoDocumento');
        $query->joinWith('entidad');
        $query->joinWith('timbradoDetalle as timbrado_detalle');
        $query->leftJoin('cont_timbrado', 'timbrado_detalle.timbrado_id = cont_timbrado.id');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['nombre_documento'] = [
            'asc' => ['cont_tipo_documento.nombre' => SORT_ASC],
            'desc' => ['cont_tipo_documento.nombre' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['entidad_id'] = [
            'asc' => ['cont_entidad.razon_social' => SORT_ASC],
            'desc' => ['cont_entidad.razon_social' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['timbrado'] = [
            'asc' => ['cont_timbrado.nro_timbrado' => SORT_ASC],
            'desc' => ['cont_timbrado.nro_timbrado' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['fecha_emision_f'] = [
            'asc' => ['cont_factura_compra.fecha_emision' => SORT_ASC],
            'desc' => ['cont_factura_compra.fecha_emision' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['nombre_entidad'] = [
            'asc' => ['cont_entidad.razon_social' => SORT_ASC],
            'desc' => ['cont_entidad.razon_social' => SORT_DESC],
            'default' => SORT_ASC
        ];
        $dataProvider->sort->attributes['autofactura'] = [
            'asc' => ['cont_factura_compra.cedula_autofactura' => SORT_ASC],
            'desc' => ['cont_factura_compra.cedula_autofactura' => SORT_DESC],
            'default' => SORT_ASC
        ];
        $dataProvider->sort->attributes['fecha_emision_hasta'] = [
            'asc' => ['cont_factura_compra.fecha_emision' => SORT_ASC],
            'desc' => ['cont_factura_compra.fecha_emision' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['cont_factura_compra.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);

        $query->andFilterWhere([
            'cont_factura_compra.id' => $this->id,
            'cont_factura_compra.tipo_documento_id' => $this->tipo_documento_id,
            'cont_factura_compra.obligaciones' => $this->obligaciones,
            'cont_factura_compra.cotizacion' => $this->cotizacion,
            'cont_factura_compra.total' => $this->total,
            'cont_factura_compra.empresa_id' => Yii::$app->getSession()->get('core_empresa_actual'),
        ]);

        $this->fecha_emision = trim($this->fecha_emision);
        $query->andFilterWhere(['like', 'cont_factura_compra.nro_factura', ($this->nro_factura = trim($this->nro_factura))]);
        $query->andFilterWhere(['like', 'cont_tipo_documento.nombre', $this->nombre_documento]);
        $query->andFilterWhere(['=', 'cont_timbrado.nro_timbrado', $this->timbrado]);
        # Entidad separated by semi-colon
        if ($this->nombre_entidad) {
            $entidades = explode(';', $this->nombre_entidad);
            if (sizeof($entidades) > 0) {
                $like = $dislike = [];
                foreach ($entidades as $entidad) {
                    $sign = substr($entidad, 0, 1);
                    $is_dislike = $sign === "-";
                    $entidad = strtolower(substr($entidad, $is_dislike ? 1 : 0, strlen($entidad)));
                    $entidad_not_empty = strlen($entidad) > 0;
                    if ($entidad_not_empty) {
                        if ($is_dislike) {
                            $dislike[] = $entidad;
                        } else {
                            $like[] = $entidad;
                        }
                    }     }

                if (sizeof($like)) {
                    $ent_names = implode("%' or LOWER(cont_entidad.razon_social) like '%", $like);
                    $ent_rucs = implode("%' or LOWER(cont_entidad.ruc) like '%", $like);
                    $query->andWhere("LOWER(cont_entidad.razon_social) like '%{$ent_names}%' or LOWER(cont_entidad.ruc) like '%{$ent_rucs}%'");
                }
                if (sizeof($dislike)) {
                    $ent_names = implode("%' and LOWER(cont_entidad.razon_social) not like '%", $dislike);
                    $ent_rucs = implode("%' and LOWER(cont_entidad.ruc) not like '%", $dislike);
                    $query->andWhere("LOWER(cont_entidad.razon_social) not like '%{$ent_names}%' and LOWER(cont_entidad.ruc) not like '%{$ent_rucs}%'");
                    Yii::warning($query->createCommand()->sql);
                }
            }
//            elseif (substr($this->nombre_entidad, 0, 1) === "-") {
//                $lengh = strlen($this->nombre_entidad) - 1;
//                $str = substr($this->nombre_entidad, 1, $lengh);
//                $query->andWhere(
//                    "(cont_entidad.razon_social not like '{$str}%' and cont_entidad.ruc not like '{$str}%')"
//                );
//            }
//            else {
//                $query->andFilterWhere([
//                    'OR',
//                    ['like', 'cont_entidad.razon_social', ($this->nombre_entidad = trim($this->nombre_entidad))],
//                    ['like', 'cont_entidad.ruc', ($this->nombre_entidad = trim($this->nombre_entidad))]
//                ]);
//            }
        }
        $query->andFilterWhere(['=', 'cont_factura_compra.moneda_id', $this->moneda_id != 'todos' ? $this->moneda_id : null]);
        $query->andFilterWhere(['=', 'cont_factura_compra.condicion', $this->condicion != 'todos' ? $this->condicion : null]);
        $query->andFilterWhere(['=', 'cont_factura_compra.estado', $this->estado != 'todos' ? $this->estado : null]);

        if (!empty($this->fecha_emision_hasta)) {
            $query->andFilterWhere(['<=', 'cont_factura_compra.fecha_emision', date('Y-m-d', strtotime($this->fecha_emision_hasta))]);
            if (!empty($this->fecha_emision_f)) {
                $query->andFilterWhere(['>=', 'cont_factura_compra.fecha_emision', date('Y-m-d', strtotime($this->fecha_emision_f))]);
            }
        } else {
            if (!empty($this->fecha_emision_f)) {
                $query->andFilterWhere(['=', 'cont_factura_compra.fecha_emision', date('Y-m-d', strtotime($this->fecha_emision_f))]);
            }
        }

	    if (!empty($this->fecha_vencimiento))
		    $query->andFilterWhere(['=', 'cont_factura_compra.fecha_vencimiento', date('Y-m-d', strtotime($this->fecha_vencimiento))]);

        $TDNotaCredito = ArrayHelper::map(TipoDocumento::find()
            ->where(['tipo_documento_set_id' => Compra::getTipodocSetNotaCredito()->id])
            ->asArray()->all(), 'id', 'id');
        if ($es_nota) {
            $query->andWhere([
                'OR',
                ['NOT', ['factura_compra_id' => null]],
                ['IN', 'cont_factura_compra.tipo_documento_id', $TDNotaCredito],
            ]);
            $this->tipo == 'todos' || $query->andFilterWhere(['LIKE', 'cont_factura_compra.tipo', $this->tipo]);
        } else {
            $query->andWhere(['cont_factura_compra.`tipo`' => 'factura']);
            $query->andWhere(['NOT IN', 'cont_factura_compra.tipo_documento_id', $TDNotaCredito]);
        }

        #Creado
        $creado = implode('-', array_reverse(explode('-', $this->creado)));
        $query->andFilterWhere(['LIKE', 'cont_factura_compra.creado', $creado]);

        #Asentado o no
        if ($this->asiento_id != '')
            $query->andWhere([($this->asiento_id == 'si' ? "IS NOT" : "IS"), 'cont_factura_compra.asiento_id', null]);

        #Autofactura
        if ($this->autofactura != '')
            $query->andWhere([($this->autofactura == 'si' ? "!=" : "="), 'cont_factura_compra.cedula_autofactura', '']);

        #Debug - Begin
        #$query->andFilterWhere(['IN', 'cont_factura_compra.id', [2186,2252,2469,2572,2657,2920,2921,3096]]);
        #Debug - End //para_iva

        $query->andFilterWhere(['=', 'cont_factura_compra.para_iva', $this->para_iva]);

        return $dataProvider;
    }
}
/**
 *
 * (LOWER(cont_entidad.razon_social) like '%mongo%' or LOWER(cont_entidad.ruc) like '%mongo%') AND
(LOWER(cont_entidad.razon_social) not like '%guarani%' and LOWER(cont_entidad.razon_social) not like '%diaz%' and LOWER(cont_entidad.razon_social) not like '%g siete%' and LOWER(cont_entidad.ruc) not like '%guarani%' and LOWER(cont_entidad.ruc) not like '%diaz%' and LOWER(cont_entidad.ruc) not like '%g siete%') AND

 */