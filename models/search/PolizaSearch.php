<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Poliza;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PolizaSearch represents the model behind the search form of `backend\modules\contabilidad\models\Poliza`.
 */
class PolizaSearch extends Poliza
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fecha_desde', 'fecha_hasta', 'nro_poliza', 'forma_devengamiento', 'importe_diario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Poliza::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_desde' => (isset($this->fecha_desde) && date_create_from_format('d-m-Y', $this->fecha_desde)) ?
                date_create_from_format('d-m-Y', $this->fecha_desde)->format('Y-m-d') : null,
            'fecha_hasta' => (isset($this->fecha_hasta) && date_create_from_format('d-m-Y', $this->fecha_hasta)) ?
                date_create_from_format('d-m-Y', $this->fecha_hasta)->format('Y-m-d') : null,
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'importe_diario' => $this->importe_diario,
        ]);

        $query->andFilterWhere(['like', 'nro_poliza', $this->nro_poliza]);

        return $dataProvider;
    }
}
