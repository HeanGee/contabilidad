<?php
namespace backend\modules\contabilidad\models\search;
use backend\modules\contabilidad\models\DetalleCompra;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DetalleCompraSearch represents the model behind the search form of `backend\modules\contabilidad\models\DetalleCompra`.
 */
class DetalleCompraSearch extends DetalleCompra
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'factura_compra_id', 'plan_cuenta_id', 'periodo_contable_id'], 'integer'],
            [['subtotal'], 'number'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetalleCompra::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere(['periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        $query->andFilterWhere([
            'id' => $this->id,
            'factura_compra_id' => $this->factura_compra_id,
            'subtotal' => $this->subtotal,
            'plan_cuenta_id' => $this->plan_cuenta_id,
        ]);
        return $dataProvider;
    }
}