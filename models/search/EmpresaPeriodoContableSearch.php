<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EmpresaPeriodoContableSearch represents the model behind the search form of `backend\modules\contabilidad\models\EmpresaPeriodoContable`.
 */
class EmpresaPeriodoContableSearch extends EmpresaPeriodoContable
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'empresa_id'], 'integer'],
            [['descripcion',], 'string'],
            [['anho', 'activo', 'descripcion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $empresa_id = false, $exclusivo = false)
    {
        $query = EmpresaPeriodoContable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions

        // uso where y no andFilterWhere para que sí o sí traiga sólo los asociados a empresa_id.
        if($exclusivo) {
            $query->where([
                'empresa_id' => $empresa_id,
            ]);
        } else {
            $query->andFilterWhere([
                'empresa_id' => $empresa_id ? $empresa_id : $this->empresa_id,
            ]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'activo' => $this->activo != 'Todos' ? $this->activo : '',
        ]);

        $query->andFilterWhere(['like', 'anho', $this->anho])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
