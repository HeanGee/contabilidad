<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\ParametroSistema;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ParametroSistemaSearch represents the model behind the search form of `common\models\ParametroSistema`.
 */
class ParametroSistemaSearch extends ParametroSistema
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nombre', 'valor'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ParametroSistema::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'valor', $this->valor]);

        if ($this->nombre != 'todos') {
            $query->andFilterWhere(['REGEXP', 'nombre', explode('||', $this->nombre)[0]]);
        }

        return $dataProvider;
    }
}
