<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\ActivoBiologico;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ActivoBiologicoSearch represents the model behind the search form of `backend\modules\contabilidad\models\ActivoBiologico`.
 */
class ActivoBiologicoSearch extends ActivoBiologico
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'empresa_id', 'periodo_contable_id', 'stock_inicial', 'stock_actual', 'stock_final',
                'activo_biologico_padre_id', 'especie_id', 'clasificacion_id'], 'integer'],
            [['tipo', 'total'], 'safe'],
            [['precio_unitario', 'precio_unitario_actual'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivoBiologico::find()->alias('activob');
        $query->joinWith('especie as especie');
        $query->joinWith('clasificacion as clasificacion');
        $query->select([
            'activob.*',
            'especie.nombre',
            'clasificacion.nombre',
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['total'] = [
            'asc' => ['(stock_final * precio_unitario)' => SORT_ASC],
            'desc' => ['(stock_final * precio_unitario)' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'activob.id' => $this->id,
            'activob.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'activob.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'stock_final' => $this->dbFormattedStockFinal,
            'stock_actual' => $this->dbFormattedStockActual,
            'stock_inicial' => $this->dbFormattedStockInicial,
            'precio_unitario' => $this->dbFormattedPrecioUnitario,
            'activo_biologico_padre_id' => $this->activo_biologico_padre_id,
            '(precio_unitario * stock_final)' => $this->total != '' ? $this->total : null,
            'especie.nombre' => $this->especie_id,
            'clasificacion.nombre' => $this->clasificacion_id
        ]);

        return $dataProvider;
    }
}
