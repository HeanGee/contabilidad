<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\ActivoFijo;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ActivoFijoSearch represents the model behind the search form of `backend\modules\contabilidad\models\ActivoFijo`.
 */
class ActivoFijoSearch extends ActivoFijo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'activo_fijo_tipo_id', 'empresa_id', 'empresa_periodo_contable_id', 'cuenta_id'], 'integer'],
            [['nombre', 'activo_fijo_tipo_id', 'empresa_id', 'empresa_periodo_contable_id', 'cuenta_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivoFijo::find()->alias('afijo');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

//        if (!$this->validate()) {
//             uncomment the following line if you do not want to return any records when validation fails
//             $query->where('0=1');
//            return $dataProvider;
//        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'afijo.nombre', $this->nombre]);
        $query->andFilterWhere([
//            'afijo.activo_fijo_tipo_id' => $this->activo_fijo_tipo_id,
            'afijo.empresa_periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'afijo.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
        ]);
//
//
//        $txt = $this->cuenta_;
//        $slices = explode(' - ', $txt);
//        $codCompleto = $slices[0];
//        $nombre = $slices[1];
//        $query->andFilterWhere(['OR',
//            ['like', 'cuenta.cod_completo', $codCompleto],
//            ['like', 'cuenta.nombre', $nombre]
//        ]);

        return $dataProvider;
    }
}
