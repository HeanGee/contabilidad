<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\EmpresaObligacion;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EmpresaObligacionSearch represents the model behind the search form of `backend\modules\contabilidad\models\EmpresaObligacion`.
 */
class EmpresaObligacionSearch extends EmpresaObligacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'obligacion_id', 'empresa_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpresaObligacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'obligacion_id' => $this->obligacion_id,
            'empresa_id' => $this->empresa_id,
        ]);

        return $dataProvider;
    }
}
