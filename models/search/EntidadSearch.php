<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Entidad;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EntidadSearch represents the model behind the search form of `backend\modules\contabilidad\models\Entidad`.
 */
class EntidadSearch extends Entidad
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'digito_verificador'], 'integer'],
            [['ruc', 'es_contribuyente', 'tipo_entidad'], 'string'],
            [['razon_social', 'direccion', 'telefonos', 'tipo_entidad', 'extranjero'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $entidad = null)
    {
        $query = Entidad::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andWhere([
//            'or', ['tipo_entidad' => null], ['tipo_entidad' => $entidad]
//        ]);

        $query->andFilterWhere([
            'id' => $this->id,
            'es_contribuyente' => $this->es_contribuyente != "Todos" ? $this->es_contribuyente : null,
            'extranjero' => $this->extranjero != "Todos" ? $this->extranjero : null,
        ]);

        $query->andFilterWhere(['like', 'razon_social', trim($this->razon_social)])
            ->andFilterWhere(['like', 'direccion', trim($this->direccion)])
            ->andFilterWhere(['like', 'ruc', trim($this->ruc)])
            ->andFilterWhere(['like', 'digito_verificador', trim($this->digito_verificador)])
        ;

        return $dataProvider;
    }
}
