<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Compra;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CompraSearch represents the model behind the search form of `backend\modules\contabilidad\models\Compra`.
 */
class CompraSearchHechauka extends Compra
{
    public $fecha_emision_f;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tipo_documento_id', 'entidad_id', 'obligaciones', 'periodo_contable_id', 'factura_compra_id'], 'integer'],
            [['fecha_emision', 'condicion', 'fecha_vencimiento', 'nro_factura', 'nombre_documento', 'timbrado', 'fecha_emision_f', 'nombre_entidad', 'moneda_id', 'estado', 'tipo'], 'safe'],
            [['total', 'saldo'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param bool $es_nota
     *
     * @return ActiveDataProvider
     */
    public function search($params, $es_nota = false)
    {
        $query = Compra::find();
        $query->joinWith('tipoDocumento');
        $query->joinWith('entidad');
        $query->joinWith('timbradoDetalle as timbrado_detalle');
        $query->leftJoin('cont_timbrado', 'timbrado_detalle.timbrado_id = cont_timbrado.id');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['nombre_documento'] = [
            'asc' => ['cont_tipo_documento.nombre' => SORT_ASC],
            'desc' => ['cont_tipo_documento.nombre' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['entidad_id'] = [
            'asc' => ['cont_entidad.razon_social' => SORT_ASC],
            'desc' => ['cont_entidad.razon_social' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['timbrado'] = [
            'asc' => ['cont_timbrado.nro_timbrado' => SORT_ASC],
            'desc' => ['cont_timbrado.nro_timbrado' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['fecha_emision_f'] = [
            'asc' => ['cont_factura_compra.fecha_emision' => SORT_ASC],
            'desc' => ['cont_factura_compra..fecha_emision' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['nombre_entidad'] = [
            'asc' => ['cont_entidad.razon_social' => SORT_ASC],
            'desc' => ['cont_entidad.razon_social' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $periodo = Yii::$app->request->get('periodo');
        $query->where([
            'DATE_FORMAT(fecha_emision, "%m/%Y")' => $periodo,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ])->orderBy(['fecha_emision' => SORT_DESC]);

//        $query->all();

        return $dataProvider;
    }
}
