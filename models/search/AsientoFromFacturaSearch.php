<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Asiento;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\Venta;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AsientoSearch represents the model behind the search form of `backend\modules\contabilidad\models\Asiento`.
 */
class AsientoFromFacturaSearch extends Asiento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'empresa_id', 'periodo_contable_id', 'monto_debe', 'monto_haber', 'usuario_id'], 'integer'],
            [['fecha', 'concepto', 'creado', 'modulo_origen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $operacion)
    {
        $query = Asiento::find();

        $tname = Asiento::tableName() . '.';
        $tVname = Venta::tableName() . '.';
        $tCname = Compra::tableName() . '.';
        $joinTable = ($operacion == 'venta') ? Venta::tableName() : Compra::tableName();
        $joinCondition = ($operacion == 'venta') ? $tname . 'id = ' . $tVname . 'asiento_id' : $tname . 'id = ' . $tCname . 'asiento_id';

        $query->leftJoin($joinTable, $joinCondition);

        $query->select([
            $tname . '*',
            'MIN(' . $joinTable . '.asiento_id)',
        ])->groupBy($joinTable . '.asiento_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $fecha = null;
        if (!empty($this->fecha)) {
            $fecha = \DateTime::createFromFormat('d-m-Y', $this->fecha);
            $fecha = $fecha->format('Y-m-d');
        }
        $query->andFilterWhere([
            $tname . 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            $tname . 'id' => $this->id,
            $tname . 'empresa_id' => $this->empresa_id,
            $tname . 'periodo_contable_id' => $this->periodo_contable_id,
            $tname . 'fecha' => $fecha,
            $tname . 'monto_debe' => $this->monto_debe,
            $tname . 'monto_haber' => $this->monto_haber,
            $tname . 'usuario_id' => $this->usuario_id,
            $tname . 'creado' => $this->creado,
        ]);

        $query->andFilterWhere(['like', $tname . 'concepto', $this->concepto])
            ->andFilterWhere(['like', $tname . 'modulo_origen', $this->modulo_origen]);

        return $dataProvider;
    }
}
