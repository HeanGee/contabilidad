<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Prestamo;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PrestamoSearch represents the model behind the search form of `backend\modules\contabilidad\models\Prestamo`.
 */
class PrestamoSearch extends Prestamo
{
    public $nro_factura;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cant_cuotas', 'periodo_contable_id', 'empresa_id'], 'integer'],
            [['fecha_operacion', 'vencimiento', 'usar_iva_10', 'incluye_iva', 'gastos_bancarios', 'caja', 'compra',
                'nro_factura', 'entidad_id', 'tiene_factura', 'nro_prestamo'], 'safe'],
            [['monto_operacion', 'intereses_vencer', 'tasa', 'gastos_bancarios', 'caja'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prestamo::find()->alias('prestamo');
        $query->joinWith('compra as compra');
        $query->leftJoin('cont_entidad as entidad', 'entidad.id = prestamo.entidad_id');
        $query->select([
            'prestamo.*',
            'compra.nro_factura',
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['nro_factura'] = [
            'asc' => ['compra.nro_factura' => SORT_ASC],
            'desc' => ['compra.nro_factura' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

//        if (!$this->validate()) {
        // uncomment the following line if you do not want to return any records when validation fails
        // $query->where('0=1');
//            return $dataProvider;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_operacion' => implode('-', array_reverse(explode('-', $this->fecha_operacion))),
            'monto_operacion' => $this->monto_operacion,
            'intereses_vencer' => $this->intereses_vencer,
            'vencimiento' => implode('-', array_reverse(explode('-', $this->vencimiento))),
            'tasa' => $this->tasa,
            'cant_cuotas' => $this->cant_cuotas,
            'prestamo.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'prestamo.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'usar_iva_10' => $this->usar_iva_10 == 'todos' ? null : $this->usar_iva_10,
            'incluye_iva' => $this->incluye_iva == 'todos' ? null : $this->incluye_iva,
            'tiene_factura' => $this->tiene_factura == 'todos' ? null : $this->tiene_factura,
//            'iva_interes_cobrado' => $this->iva_interes_cobrado == 'todos' ? null : $this->iva_interes_cobrado, // eliminado
            'caja' => $this->caja,
            'gastos_bancarios' => $this->gastos_bancarios,
        ]);

        $query->andFilterWhere(['like', 'compra.nro_factura', $this->nro_factura]);
        $query->andFilterWhere(['OR', ['like', 'entidad.ruc', $this->entidad_id], ['like', 'entidad.razon_social', $this->entidad_id]]);

        $query->orderBy(['id' => SORT_DESC]);

        return $dataProvider;
    }
}
