<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\PresentacionIva;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PresentacionIvaSearch represents the model behind the search form of `backend\modules\contabilidad\models\PresentacionIva`.
 */
class PresentacionIvaSearch extends PresentacionIva
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nro_orden', 'periodo_contable_id', 'empresa_id', 'presentacion_iva_id'], 'integer'],
            [['tipo_declaracion', 'estado', 'periodo_fiscal', 'codigo_set'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PresentacionIva::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nro_orden' => $this->nro_orden,
            'codigo_set' => $this->codigo_set,
            'periodo_fiscal' => $this->periodo_fiscal,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ]);

        $query->andFilterWhere(['=', 'tipo_declaracion', $this->tipo_declaracion == 'todos' ? null : $this->tipo_declaracion]);
        $query->andFilterWhere(['=', 'estado', $this->estado == 'todos' ? null : $this->estado]);

        $query->orderBy(['periodo_fiscal' => SORT_DESC]);

        return $dataProvider;
    }
}
