<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Recibo;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ReciboSearch represents the model behind the search form of `backend\modules\contabilidad\models\Recibo`.
 *
 * @property string $nro_factura
 * @property string $con_monto_extra
 */
class ReciboSearch extends Recibo
{
    public $nro_factura = '';
    public $con_monto_extra = '';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'moneda_id'], 'integer'],
            [['numero', 'fecha', 'ruc', 'razon_social', 'entidad_id', 'nro_factura', 'total', 'monto_extra', 'con_monto_extra', 'asiento_id'], 'safe'],
            [['valor_moneda'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param string $operacion
     * @return ActiveDataProvider
     */
    public function search($params, $operacion = '')
    {
        $query = Recibo::find()->alias('recibo');
        $query->leftJoin('cont_recibo_detalle detalle', 'recibo.id = detalle.recibo_id');
        $query->leftJoin('cont_entidad entidad', 'entidad.id = recibo.entidad_id');
        $query->leftJoin("cont_factura_{$operacion} factura", "factura.id = detalle.factura_{$operacion}_id");
        $select = [
            'id' => "MIN(recibo.id)",
            'numero' => "MIN(recibo.numero)",
            'fecha' => "MIN(recibo.fecha)",
            'entidad_id' => "MIN(recibo.entidad_id)",
            'moneda_id' => "MIN(recibo.moneda_id)",
            'valor_moneda' => "MIN(recibo.valor_moneda)",
            'factura_tipo' => "MIN(recibo.factura_tipo)",
            'empresa_id' => "MIN(recibo.empresa_id)",
            'asiento_id' => "MIN(recibo.asiento_id)",
            'periodo_contable_id' => "MIN(recibo.periodo_contable_id)",
            'total' => "MIN(recibo.total)",
            'monto_extra' => "MIN(recibo.monto_extra)",
            'bloqueado' => "MIN(recibo.bloqueado)",
        ];
        $query->select($select);
        $query->where(['!=', "detalle.factura_{$operacion}_id", '']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['entidad_id'] = [
            'asc' => ['entidad.razon_social' => SORT_ASC],
            'desc' => ['entidad.razon_social' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $dataProvider->sort->attributes['con_monto_extra'] = [
            'asc' => ['recibo.monto_extra' => SORT_ASC],
            'desc' => ['recibo.monto_extra' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'recibo.id' => $this->id,
            'recibo.fecha' => implode('-', array_reverse(explode('-', trim($this->fecha)))),
            'recibo.moneda_id' => $this->moneda_id,
            'recibo.valor_moneda' => trim($this->valor_moneda),
            'recibo.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'recibo.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ]);

        $query->andFilterWhere(['like', 'numero', trim($this->numero)]);
        $query->andFilterWhere([
            'OR',
            ['LIKE', 'entidad.razon_social', trim($this->entidad_id)],
            [
                'OR',
                ['LIKE', 'entidad.ruc', trim($this->entidad_id)],
                ['=', 'recibo.entidad_id', trim($this->entidad_id)]
            ]
        ]);
        $query->andFilterWhere(['factura.id' => trim($this->nro_factura)]);  #ahora que se usa Select2, ya no es por numero de factura sino por id.

        if ($this->con_monto_extra != '')
            $query->andFilterWhere([trim($this->con_monto_extra), 'recibo.monto_extra', 0]);

        #Asentado o no
        if ($this->asiento_id != '')
            $query->andWhere([($this->asiento_id == 'si' ? "IS NOT" : "IS"), 'recibo.asiento_id', null]);

        $query->groupBy(['recibo.id']);  #para que en cada pagina del index aparezca la misma cantidad de registros.
        $query->orderBy(['recibo.id' => SORT_DESC]);

        return $dataProvider;
    }
}
