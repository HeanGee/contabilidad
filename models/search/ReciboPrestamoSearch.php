<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\ReciboPrestamo;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ReciboPrestamoSearch represents the model behind the search form of `backend\modules\contabilidad\models\ReciboPrestamo`.
 */
class ReciboPrestamoSearch extends ReciboPrestamo
{
    public $razon_social_s;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'empresa_id', 'periodo_contable_id'], 'integer'],
            [['numero', 'razon_social_s', 'fecha'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReciboPrestamo::find()->alias('recibop');
        $query->joinWith('entidad as entidad');
        $query->select([
            'recibop.*',
            'entidad.razon_social',
        ]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['razon_social_s'] = [
            'asc' => ['entidad.razon_social' => SORT_ASC],
            'desc' => ['entidad.razon_social' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'recibop.id' => $this->id,
            'recibop.total' => $this->total,
            'recibop.fecha' => implode('-', array_reverse(explode('-', $this->fecha))),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
        ]);

        $query->andFilterWhere(['like', 'recibop.numero', $this->numero]);
        $query->andFilterWhere(['like', 'entidad.razon_social', $this->razon_social_s]);

        return $dataProvider;
    }
}
