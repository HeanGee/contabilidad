<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Asiento;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AsientoSearch represents the model behind the search form of `backend\modules\contabilidad\models\Asiento`.
 */
class AsientoSearch extends Asiento
{
    public $cuentas;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'periodo_contable_id', 'monto_debe', 'monto_haber', 'usuario_id', 'periodo_contable_id'], 'integer'],
            [['fecha', 'concepto', 'creado', 'modulo_origen', 'empresa_id', 'cuentas'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param bool $showAllEmpresa
     * @return ActiveDataProvider
     */
    public function search($params, $showAllEmpresa = false)
    {
        $query = Asiento::find()->alias('asiento');
        $query->leftJoin('core_empresa empresa', 'empresa.id = asiento.empresa_id');
        $query->leftJoin('cont_asiento_detalle detalle', 'asiento.id = detalle.asiento_id');
        $query->leftJoin('cont_plan_cuenta cuenta', 'cuenta.id = detalle.cuenta_id');

        $query->select([
            'id' => "MIN(asiento.id)",
            'periodo_contable_id' => "MIN(asiento.periodo_contable_id)",
            'fecha' => "MIN(asiento.fecha)",
            'monto_debe' => "MIN(asiento.monto_debe)",
            'monto_haber' => "MIN(asiento.monto_haber)",
            'usuario_id' => "MIN(asiento.usuario_id)",
            'creado' => "MIN(asiento.creado)",
            'empresa_id' => "MIN(asiento.empresa_id)",
            'ruc' => "MIN(empresa.ruc)",
            'cedula' => "MIN(empresa.cedula)",
            'concepto' => "MIN(asiento.concepto)",
            'cuentas' => "GROUP_CONCAT(cuenta.nombre)",
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $fecha = null;
        if (!empty($this->fecha)) {
            $fecha = \DateTime::createFromFormat('d-m-Y', trim($this->fecha));
            $fecha = $fecha->format('Y-m-d');
        }
        $showAllEmpresa || $query->andFilterWhere([
            'asiento.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'asiento.empresa_id' => \Yii::$app->session->get('core_empresa_actual')
        ]);
        $query->andFilterWhere([
            'asiento.id' => $this->id,
            'fecha' => $fecha,
            'monto_debe' => $this->monto_debe,
            'monto_haber' => $this->monto_haber,
            'usuario_id' => $this->usuario_id,
            'creado' => $this->creado,
        ]);

        !$showAllEmpresa || $query->andFilterWhere([
            'OR',
            ['=', 'empresa_id', trim($this->empresa_id)],
            [
                'OR',
                ['LIKE', 'empresa.razon_social', trim($this->empresa_id)],
                [
                    'OR',
                    ['LIKE', 'empresa.ruc', trim($this->empresa_id)],
                    ['LIKE', 'empresa.cedula', trim($this->empresa_id)],
                ]
            ]
        ]);

        $query->andFilterWhere(['OR', ['like', 'concepto', $this->concepto], ['LIKE', 'cuenta.nombre', $this->concepto]])
            ->andFilterWhere(['like', 'modulo_origen', $this->modulo_origen]);

        $query->groupBy(['asiento.id']);

        return $dataProvider;
    }
}
