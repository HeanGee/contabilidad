<?php

namespace backend\modules\contabilidad\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\contabilidad\models\ActivoFijoTipo;

/**
 * ActivoFijoTipoSearch represents the model behind the search form of `backend\modules\contabilidad\models\ActivoFijoTipo`.
 */
class ActivoFijoTipoSearch extends ActivoFijoTipo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vida_util', 'cuenta_id'], 'integer'],
            [['nombre', 'estado', 'tmp_cuenta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivoFijoTipo::find()
            ->joinWith('cuenta AS cuenta');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['tmp_cuenta'] = [
            'asc' => ['cuenta.cod_completo' => SORT_ASC],
            'desc' => ['cuenta.cod_completo' => SORT_DESC],
            'label' => 'Cuenta',
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vida_util' => $this->vida_util,
            'cuenta_id' => $this->cuenta_id,
            'cont_activo_fijo_tipo.estado' => $this->estado
        ]);

        $query->andFilterWhere(['like', 'cont_activo_fijo_tipo.nombre', $this->nombre])
            ->andFilterWhere(['like', 'cuenta.nombre', $this->tmp_cuenta]);

        return $dataProvider;
    }
}
