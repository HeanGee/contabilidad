<?php

namespace backend\modules\contabilidad\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\contabilidad\models\Importacion;

/**
 * ImportacionSearch represents the model behind the search form of `backend\modules\contabilidad\models\Importacion`.
 */
class ImportacionSearch extends Importacion
{
    public $fecha_;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'numero_despacho', 'numero', 'empresa_id', 'periodo_contable_id'], 'integer'],
            [['fecha', 'estado', 'criterio_retencion', 'medio_transporte', 'cal_retencion_fact_local_flete_seguro', 'fecha_'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Importacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['fecha_'] = [
            'asc' => ['fecha' => SORT_ASC],
            'desc' => ['fecha' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'numero_despacho' => $this->numero_despacho,
            'numero' => $this->numero,
            'empresa_id' => $this->empresa_id,
            'periodo_contable_id' => $this->periodo_contable_id,
        ]);

        $query->andFilterWhere(['like', 'estado', $this->estado != 'todos' ? $this->estado : null])
            ->andFilterWhere(['like', 'criterio_retencion', $this->criterio_retencion])
            ->andFilterWhere(['like', 'medio_transporte', $this->medio_transporte])
            ->andFilterWhere(['like', 'cal_retencion_fact_local_flete_seguro', $this->cal_retencion_fact_local_flete_seguro]);

        if (!empty($this->fecha_)) {
            $query->andFilterWhere(['=', 'fecha', date('y-m-d', strtotime($this->fecha_))]);
        };

        return $dataProvider;
    }
}
