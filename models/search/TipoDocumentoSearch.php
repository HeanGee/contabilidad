<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\TipoDocumento;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TipoDocumentoSearch represents the model behind the search form about `backend\modules\contabilidad\models\TipoDocumento`.
 */
class TipoDocumentoSearch extends TipoDocumento
{
    public $tipo_documento_set_nombre;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nombre', 'detalle', 'asociacion', 'estado', 'condicion', 'tipo_documento_set_id',
                'tipo_documento_set_nombre', 'asiento_independiente', 'para', 'cuenta_id'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $tname = TipoDocumento::tableName() . '.';
        $query = TipoDocumento::find()
            ->joinWith('cuenta AS cuenta')
            ->joinWith('tipoDocumentoSet tipodoc_set');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['tipo_documento_set_nombre'] = [
            'asc' => ['tipodoc_set.nombre' => SORT_ASC],
            'desc' => ['tipodoc_set.nombre' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'cont_tipo_documento.id' => $this->id,
            'condicion' => $this->condicion == 'todos' ? null : $this->condicion,
            'asiento_independiente' => $this->asiento_independiente,
            'cuenta_id' => $this->cuenta_id,
        ]);

        if ($this->para == 'not_null')
            $query->andWhere(['IS NOT', 'para', null]);
        else
            $query->andFilterWhere(['para' => trim($this->para)]);

        $query->andFilterWhere(['like', $tname . 'nombre', $this->nombre])
            ->andFilterWhere(['like', $tname . 'detalle', $this->detalle])
            ->andFilterWhere([$tname . 'asociacion' => $this->asociacion])
            ->andFilterWhere([$tname . 'estado' => $this->estado]);

        $empresa_actual = Yii::$app->session->get('core_empresa_actual');
        if (empty($empresa_actual)) {
            $query->andWhere(['cuenta.empresa_id' => null]);
        } else {
            $query->andWhere(['OR', ['cuenta.empresa_id' => null], ['cuenta.empresa_id' => $empresa_actual]]);
        }

        $query->andFilterWhere(['like', 'tipodoc_set.nombre', $this->tipo_documento_set_nombre]);

        return $dataProvider;
    }
}
