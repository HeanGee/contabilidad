<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\Timbrado;
use DateTime;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TimbradoSearch represents the model behind the search form of `backend\modules\contabilidad\models\Timbrado`.
 */
class TimbradoSearch extends Timbrado
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nro_timbrado', 'entidad_id'], 'integer'],
            [['fecha_inicio', 'fecha_fin', 'entidad_tmp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Timbrado::find()
            ->joinWith('entidad AS entidad');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['entidad_tmp'] = [
            'asc' => ['entidad.razon_social' => SORT_ASC],
            'desc' => ['entidad.razon_social' => SORT_DESC],
            'label' => 'Entidad',
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            Timbrado::tableName() . '.id' => $this->id,
            Timbrado::tableName() . '.entidad_id' => $this->entidad_id
        ])
            ->andFilterWhere(['OR', ['like', 'entidad.razon_social', $this->entidad_tmp], ['like', 'entidad.ruc', $this->entidad_tmp]])
            ->andFilterWhere(['like', Timbrado::tableName() . '.nro_timbrado', $this->nro_timbrado]);

        if (!empty($this->fecha_inicio)) {
            $_tmp = DateTime::createFromFormat('d-m-Y', $this->fecha_inicio);
            $query->andWhere(['>=', Timbrado::tableName() . '.fecha_inicio', $_tmp->format('Y-m-d')]);
        }
        if (!empty($this->fecha_fin)) {
            $_tmp = DateTime::createFromFormat('d-m-Y', $this->fecha_fin);
            $query->andWhere(['<=', Timbrado::tableName() . '.fecha_fin', $_tmp->format('Y-m-d')]);
        }

        return $dataProvider;
    }
}
