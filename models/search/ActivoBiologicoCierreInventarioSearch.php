<?php

namespace backend\modules\contabilidad\models\search;

use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ActivoBiologicoCierreInventarioSearch represents the model behind the search form of `backend\modules\contabilidad\models\ActivoBiologicoCierreInventario`.
 */
class ActivoBiologicoCierreInventarioSearch extends ActivoBiologicoCierreInventario
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'especie_id', 'clasificacion_id', 'precio_foro', 'procreo_cantidad', 'venta_cantidad', 'consumo_cantidad', 'mortandad_cantidad', 'consumo_porcentaje_gd', 'mortandad_porcentaje_gd', 'empresa_id', 'periodo_contable_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActivoBiologicoCierreInventario::find();
        $query->select([
            'fecha' => 'MIN(fecha)',
        ]);
        $query->groupBy(['empresa_id', 'periodo_contable_id']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'id' => $this->id,
//            'especie_id' => $this->especie_id,
//            'clasificacion_id' => $this->clasificacion_id,
//            'precio_foro' => $this->precio_foro,
//            'procreo_cantidad' => $this->procreo_cantidad,
//            'venta_cantidad' => $this->venta_cantidad,
//            'consumo_cantidad' => $this->consumo_cantidad,
//            'mortandad_cantidad' => $this->mortandad_cantidad,
//            'consumo_porcentaje_gd' => $this->consumo_porcentaje_gd,
//            'mortandad_porcentaje_gd' => $this->mortandad_porcentaje_gd,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ]);

        return $dataProvider;
    }
}
