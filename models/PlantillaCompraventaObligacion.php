<?php

namespace backend\modules\contabilidad\models;

/**
 * This is the model class for table "cont_plantilla_compraventa_obligacion".
 *
 * @property string $id
 * @property string $plantilla_compraventa_id
 * @property int $obligacion_id
 *
 * @property Obligacion $obligacion
 * @property PlantillaCompraventa $plantillaCompraventa
 */
class PlantillaCompraventaObligacion extends \backend\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_plantilla_compraventa_obligacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plantilla_compraventa_id', 'obligacion_id'], 'required'],
            [['plantilla_compraventa_id', 'obligacion_id'], 'integer'],
            [['obligacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Obligacion::className(), 'targetAttribute' => ['obligacion_id' => 'id']],
            [['plantilla_compraventa_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlantillaCompraventa::className(), 'targetAttribute' => ['plantilla_compraventa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plantilla_compraventa_id' => 'Plantilla Compraventa ID',
            'obligacion_id' => 'Obligacion ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObligacion()
    {
        return $this->hasOne(Obligacion::className(), ['id' => 'obligacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantillaCompraventa()
    {
        return $this->hasOne(PlantillaCompraventa::className(), ['id' => 'plantilla_compraventa_id']);
    }
}
