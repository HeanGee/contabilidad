<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;

/**
 * This is the model class for table "cont_recibo_prestamo_detalle".
 *
 * @property string $id
 * @property string $capital
 * @property string $interes
 * @property string $monto_cuota
 * @property string $recibo_prestamo_id
 * @property string $prestamo_detalle_id
 * @property string $empresa_id
 * @property int $periodo_contable_id
 * @property int $asiento_id
 *
 * @property string $capitalFormatted
 * @property string $interesFormatted
 * @property string $cuotaFormatted
 *
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoContable
 * @property ReciboPrestamo $reciboPrestamo
 * @property PrestamoDetalle $prestamoDetalle
 * @property Asiento $asiento
 */
class ReciboPrestamoDetalle extends \backend\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_recibo_prestamo_detalle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['capital', 'interes', 'monto_cuota', 'recibo_prestamo_id', 'empresa_id', 'periodo_contable_id', 'prestamo_detalle_id'], 'required'],
            [['capital', 'interes', 'monto_cuota'], 'number'],
            [['asiento_id'], 'safe'],
            [['recibo_prestamo_id', 'empresa_id', 'periodo_contable_id', 'prestamo_detalle_id', 'asiento_id'], 'integer'],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['recibo_prestamo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReciboPrestamo::className(), 'targetAttribute' => ['recibo_prestamo_id' => 'id']],
            [['prestamo_detalle_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrestamoDetalle::className(), 'targetAttribute' => ['prestamo_detalle_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'capital' => 'Capital',
            'interes' => 'Interes',
            'monto_cuota' => 'Monto Cuota',
            'recibo_prestamo_id' => 'Recibo Prestamo',
            'prestamo_detalle_id' => 'Prestamo Detalle',
            'empresa_id' => 'Empresa',
            'periodo_contable_id' => 'Empresa Periodo Contable',
            'asiento_id' => "Asiento ID"
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsiento()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboPrestamo()
    {
        return $this->hasOne(ReciboPrestamo::className(), ['id' => 'recibo_prestamo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestamoDetalle()
    {
        return $this->hasOne(PrestamoDetalle::className(), ['id' => 'prestamo_detalle_id']);
    }

    public function getCapitalFormatted()
    {
        return number_format($this->capital, 2, ',', '.');
    }

    public function getInteresFormatted()
    {
        return number_format($this->interes, 2, ',', '.');
    }

    public function getCuotaFormatted()
    {
        return number_format($this->monto_cuota, 2, ',', '.');
    }

    public function beforeValidate()
    {
        if (strpos($this->capital, '.') && strlen(substr($this->capital, (strpos($this->capital, '.') + 1))) >= 3) {
            $this->capital = str_replace('.', '', $this->capital);
            $this->capital = str_replace(',', '.', $this->capital);
        } elseif (strpos($this->capital, ',') == strlen($this->capital) - 3)
            $this->capital = str_replace(',', '.', $this->capital);

        if (strpos($this->interes, '.') && strlen(substr($this->interes, (strpos($this->interes, '.') + 1))) >= 3) {
            $this->interes = str_replace('.', '', $this->interes);
            $this->interes = str_replace(',', '.', $this->interes);
        } elseif (strpos($this->interes, ',') == strlen($this->interes) - 3)
            $this->interes = str_replace(',', '.', $this->interes);

        if (strpos($this->monto_cuota, '.') && strlen(substr($this->monto_cuota, (strpos($this->monto_cuota, '.') + 1))) >= 3) {
            $this->monto_cuota = str_replace('.', '', $this->monto_cuota);
            $this->monto_cuota = str_replace(',', '.', $this->monto_cuota);
        } elseif (strpos($this->monto_cuota, ',') == strlen($this->monto_cuota) - 3)
            $this->monto_cuota = str_replace(',', '.', $this->monto_cuota);

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');

        return parent::loadDefaultValues($skipIfSet); // TODO: Change the autogenerated stub
    }

    /**
     * @param $prestamo_detalle_attributes array
     */
    public function setAttributesBasedOn($prestamo_detalle_attributes)
    {
        $this->capital = $prestamo_detalle_attributes['capital_saldo'];
        $this->interes = $prestamo_detalle_attributes['interes_saldo'];
        $this->monto_cuota = $prestamo_detalle_attributes['monto_cuota_saldo'];
        $this->prestamo_detalle_id = $prestamo_detalle_attributes['id'];
    }
}
