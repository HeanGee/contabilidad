<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;

/**
 * This is the model class for table "cont_activo_fijo_tipo_atributo".
 *
 * @property int $id
 * @property int $activo_fijo_tipo_id
 * @property string $atributo
 * @property string $obligatorio
 * @property int $empresa_id
 * @property int $periodo_contable_id
 *
 * @property ActivoFijoTipo $activoFijoTipo
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoContable
 */
class ActivoFijoTipoAtributo extends \backend\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_activo_fijo_tipo_atributo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo_fijo_tipo_id', 'atributo', 'empresa_id', 'periodo_contable_id', 'obligatorio'], 'required'],
            [['activo_fijo_tipo_id', 'atributo', 'empresa_id', 'periodo_contable_id'], 'safe'],
            [['activo_fijo_tipo_id', 'empresa_id', 'periodo_contable_id'], 'integer'],
            [['obligatorio'], 'string'],
            [['atributo'], 'string', 'max' => 100],
            [['activo_fijo_tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoFijoTipo::className(), 'targetAttribute' => ['activo_fijo_tipo_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activo_fijo_tipo_id' => 'Activo Fijo Tipo ID',
            'atributo' => 'Atributo',
            'obligatorio' => 'Obligatorio',
            'empresa_id' => 'Empresa ID',
            'periodo_contable_id' => 'Periodo Contable ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoFijoTipo()
    {
        return $this->hasOne(ActivoFijoTipo::className(), ['id' => 'activo_fijo_tipo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    public function loadEmpresaPeriodo()
    {
        $this->empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
    }
}
