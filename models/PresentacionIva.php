<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "cont_presentacion_iva".
 *
 * @property string $id
 * @property int $nro_orden
 * @property string $tipo_declaracion
 * @property int $tipo_formulario
 * @property int $periodo_contable_id
 * @property string $empresa_id
 * @property string $estado
 * @property string $presentacion_iva_id
 * @property string $periodo_fiscal
 * @property string $codigo_set
 *
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoable
 * @property PresentacionIva $presentacionIva
 * @property PresentacionIvaRubro[] $presentacionIvaRubros
 */
class PresentacionIva extends \backend\models\BaseModel
{
    public static $_ORIGINAL = 'jurada_original';
    public static $_RECTIFICATIVA = 'jurada_rectificativa';
    public static $_CARACTER_CLAUS = 'jurada_en_caracter_de_clausura_o_ces.';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_presentacion_iva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nro_orden', 'periodo_contable_id', 'empresa_id', 'presentacion_iva_id', 'tipo_formulario'], 'integer'],
            [['tipo_declaracion', 'estado', 'periodo_fiscal', 'codigo_set'], 'string'],
            [['periodo_contable_id', 'empresa_id', 'periodo_fiscal', 'tipo_formulario'], 'required'],
            [['periodo_fiscal', 'codigo_set'], 'safe'],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['presentacion_iva_id'], 'exist', 'skipOnError' => true, 'targetClass' => PresentacionIva::className(), 'targetAttribute' => ['presentacion_iva_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nro_orden' => 'Nro Orden',
            'tipo_declaracion' => 'Tipo Declaracion',
            'periodo_contable_id' => 'Periodo able ID',
            'empresa_id' => 'Empresa ID',
            'estado' => 'Estado',
            'presentacion_iva_id' => 'Presentacion Iva ID',
            'periodo_fiscal' => 'Periodo Fiscal',
            'codigo_set' => 'Código',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPresentacionIva()
    {
        return $this->hasOne(PresentacionIva::className(), ['id' => 'presentacion_iva_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPresentacionIvaRubros()
    {
        return $this->hasMany(PresentacionIvaRubro::className(), ['presentacion_iva_id' => 'id']);
    }

    public static function getValoresEnum($columna, $todos = false, $enumerate = false)
    {
        $_retornar = [];
        $contador = 1;
        if ($todos) $_retornar['todos'] = ($enumerate ? $contador++ . ' - ' : '') . 'Todos';
        try {
            $valores_enum = self::getTableSchema()->columns[$columna]->enumValues;
            foreach ($valores_enum as $_v) {
                $_retornar[$_v] = ($enumerate ? $contador++ . ' - ' : '') . str_replace('_', ' ', ucfirst($_v));
            }

        } catch (\Exception $e) {
            throw new ForbiddenHttpException('Error interno: ' . $e->getMessage());
        }
        return $_retornar;
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');

        return parent::loadDefaultValues($skipIfSet);
    }

    public function getAllErrorsAsString()
    {
        $msg = '';
        foreach ($this->getErrorSummary(true) as $error) {
            $msg .= substr($error, 0, (strlen($error) - 1)) . ', ';
        }

        return substr($msg, 0, (strlen($msg) - 2)) . '.';
    }

    public function beforeDelete()
    {
        $no_presentado = $this->estado != 'presentado';
        // more conditions to prove goes here and concat it with '&&' at the return statement.

        return parent::beforeDelete() && $no_presentado;
    }

    public function isUpdateable()
    {
        return (!$this->isPresented() && !$this->existsNexts());
    }

    public function isDeleteable()
    {
        // Por el momento, borrabilidad === editabilidad. Pero posteriormente puede variar las condiciones.
        if ($this->isOriginal())
            return (!$this->isPresented() && !$this->existsNexts());
        if ($this->isRectificative())
            return !$this->isPresented();

        return false;
    }

    public function isRectificable()
    {
        return ($this->isPresented() && $this->isOriginal());
    }

    public function isOriginal()
    {
        return $this->tipo_declaracion == 'jurada_original';
    }

    public function isRectificative()
    {
        return $this->tipo_declaracion == 'jurada_rectificativa';
    }

    public function isClosingCharacter()
    {
        return $this->tipo_declaracion == 'jurada_en_caracter_de_clausura_o_ces.';
    }

    public function isPresented()
    {
        return $this->estado == 'presentado';
    }

    /**
     * @return array|null|\yii\db\ActiveRecord|PresentacionIva
     */
    public function getNext()
    { // Este devuelve la declaracion siguiente inmediata.
        $sigte_periodo_f = date('m-Y', strtotime('+1 month', strtotime('01-' . $this->periodo_fiscal)));
        return PresentacionIva::findOne([
            'periodo_fiscal' => $sigte_periodo_f,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')
        ]);
    }

    public function isNextPresented()
    {
        if ($this->getNext() == null) return false;
        return $this->getNext()->isPresented();
    }

    public function existsNexts()
    { // Este devuelve todas las declaraciones posteriores al actual, sin importar la consecutividad.
        $presentaciones = PresentacionIva::find()
            ->where(['>', 'periodo_fiscal', $this->periodo_fiscal])
            ->andWhere([
                'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')
            ]);
        return $presentaciones->exists();
    }

    public function isNextGenerable()
    {
        if (!$this->isOriginal()) return false; // Si es rectificativa por ej, no se debe poder generar un sigte.
        return !$this->isNextPresented();
    }

    public function monthYearToString()
    {
        $mes = explode('-', $this->periodo_fiscal)[0];
        $anho = explode('-', $this->periodo_fiscal)[1];
        $mes_nombre = [
            '01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio',
            '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre',
        ];
        return $mes_nombre[$mes] . ' del ' . $anho;
    }
}
