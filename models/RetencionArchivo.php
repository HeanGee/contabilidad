<?php

namespace backend\modules\contabilidad\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * @property UploadedFile $archivo
 */
class RetencionArchivo extends Model
{
    /* valores auxiliares */
    public $archivo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['archivo'], 'required'],
            [['archivo'], 'safe'],
            [['fecha'], 'safe'],
            [['fecha'], 'required'],
            ['archivo', 'file', 'extensions' => ['csv', 'xlsx', 'xls'], 'skipOnEmpty' => false, 'maxSize' => 1024 * 1024],
            [['nombreArchivo'], 'match', 'pattern' => '/^\d+-\d{1}_\d{6}']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Archivo a subir',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->archivo->saveAs('uploads/' . $this->archivo->baseName . '.' . $this->archivo->extension);
            return true;
        } else {
            return false;
        }
    }

}
