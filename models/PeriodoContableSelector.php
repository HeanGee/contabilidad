<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 02/10/2018
 * Time: 11:04
 */

namespace backend\modules\contabilidad\models;


use yii\db\ActiveRecord;

/**
 * @property int $periodo_contable_id
 */
class PeriodoContableSelector extends ActiveRecord
{
    public $periodo_contable_id;

    public function rules()
    {
        $array = [
            [['periodo_contable_id'], 'required'],
        ];

        return $array;
    }

    public function attributeLabels()
    {
        return [
            'periodo_contable_id' => "Periodo Contable ID",
        ];
    }

    /**
     * @return EmpresaPeriodoContable|null
     */
    public function periodoContable()
    {
        return EmpresaPeriodoContable::findOne(['id' => $this->periodo_contable_id]);
    }
}