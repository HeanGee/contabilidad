<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;

/**
 * This is the model class for table "cont_factura_venta_iva_cuenta_usada".
 * ESTA TABLA SE USA PARA REGISTRAR LA ASOCIACION DEL 'MONTO POR IVA' CON LA 'CUENTA DEL IVA' CORRESPONDIENTE UTILIZADA.
 * SU UTILIDAD EMPIEZA CUANDO SE TENGA QUE MODIFICAR UNA FACTURA. AL TRAER LOS DETALLES, SE TIENE QUE OBTENER LOS VALORES
 *   CORRESPONDIENTES A CADA CAMPO total-iva-N-disp Y RELLENAR ESOS CAMPOS. LOS MISMOS NO SON CAMPOS QUE EXISTAN EN LA
 *   TABLA FACTURA.
 * UNA ENTRADA EN ESTA TABLA ES OCASIONADA POR LA EXISTENCIA DE UN VALOR EN EL CAMPO total-iva-N-disp.
 *
 * @property string $id
 * @property string $periodo_contable_id
 * @property string $monto
 * @property int $iva_cta_id // Las cuentas asociadas al iva puede cambiar
 * @property string $plan_cuenta_id // La cuenta usada por el iva N al principio debe permanecer
 * @property int $factura_venta_id
 * @property int $plantilla_id
 * @property string $gnd_motivo
 *
 * @property Venta $venta
 * @property IvaCuenta $ivaCta
 * @property PlanCuenta $planCuenta
 * @property EmpresaPeriodoContable $periodoContable
 * @property PlantillaCompraventa $plantilla
 */
class VentaIvaCuentaUsada extends BaseModel
{
    public $ivas = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_factura_venta_iva_cuenta_usada';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['monto', 'factura_venta_id'], 'required'],
            [['monto'], 'number'],
            [['iva_cta_id', 'plan_cuenta_id', 'factura_venta_id', 'plantilla_id'], 'integer'],
            [['gnd_motivo'], 'safe'],
            [['factura_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['factura_venta_id' => 'id']],
            [['iva_cta_id'], 'exist', 'skipOnError' => true, 'targetClass' => IvaCuenta::className(), 'targetAttribute' => ['iva_cta_id' => 'id']],
            [['plan_cuenta_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['plan_cuenta_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['plantilla_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlantillaCompraventa::className(), 'targetAttribute' => ['plantilla_id' => 'id']],
            [['gnd_motivo'], 'required', 'when' => function ($model) {
                return (isset($model->plantilla) && $model->plantilla->deducible == 'no');
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'monto' => 'Monto',
            'iva_cta_id' => 'Iva Cta ID',
            'plan_cuenta_id' => 'Plan Cuenta ID',
            'factura_venta_id' => 'Factura Venta ID',
            'gnd_motivo' => 'Motivo por el que se envia a GND',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'factura_venta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIvaCta()
    {
        return $this->hasOne(IvaCuenta::className(), ['id' => 'iva_cta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanCuenta()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'plan_cuenta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantilla()
    {
        return $this->hasOne(PlantillaCompraventa::className(), ['id' => 'plantilla_id']);
    }
}
