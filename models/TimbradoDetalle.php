<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\modules\contabilidad\models\validators\NumberValidator;
use backend\modules\contabilidad\models\validators\NumeroActualValidator;
use backend\modules\contabilidad\models\validators\RangoValidator;

/**
 * This is the model class for table "cont_timbrado_detalle".
 *
 * @property int $id
 * @property int $timbrado_id
 * @property int $tipo_documento_set_id
 * @property string $prefijo
 * @property int $nro_inicio
 * @property int $nro_fin
 * @property int $nro_actual
 * @property int $nro_completo
 *
 * @property Timbrado $timbrado
 * @property TipoDocumentoSet $tipoDocumentoSet
 * @property Venta[] $facturasVenta
 * @property Compra[] $facturasCompra
 * @property Retencion[] $retenciones
 */
class TimbradoDetalle extends BaseModel
{
    public $nro_completo; // usado para addtimbrado desde retencion.

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_timbrado_detalle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['timbrado_id', 'tipo_documento_set_id', 'prefijo', 'nro_inicio', 'nro_fin'], 'required'],
            [['timbrado_id', 'tipo_documento_set_id', 'nro_inicio', 'nro_fin', 'nro_actual'], 'integer'],
            [['prefijo'], 'string', 'max' => 45],
            [['timbrado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Timbrado::className(), 'targetAttribute' => ['timbrado_id' => 'id']],
            [['tipo_documento_set_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumentoSet::className(), 'targetAttribute' => ['tipo_documento_set_id' => 'id']],
            [['nro_completo'], 'safe'],
            [['nro_inicio'], NumberValidator::className()],
            [['nro_fin'], NumberValidator::className()],
            [['nro_actual'], NumeroActualValidator::className()],
            [['nro_inicio'], RangoValidator::className()],
            [['nro_fin'], RangoValidator::className()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timbrado_id' => 'Timbrado',
            'tipo_documento_set_id' => 'Tipo Documento SET',
            'prefijo' => 'Prefijo',
            'nro_inicio' => 'Nro Inicio',
            'nro_fin' => 'Nro Fin',
            'nro_actual' => 'Nro Actual',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimbrado()
    {
        return $this->hasOne(Timbrado::className(), ['id' => 'timbrado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumentoSet()
    {
        return $this->hasOne(TipoDocumentoSet::className(), ['id' => 'tipo_documento_set_id']);
    }

    public function validarNumeros($attribute, $params)
    {
        if ($this->nro_inicio > $this->nro_fin)
            $this->addError($attribute, 'Número Inicio no puede ser mayor que Número Fin.');
    }

    public function validarActual($attribute, $params)
    {
        if ($this->nro_actual > $this->nro_fin || $this->nro_actual < $this->nro_inicio)
            $this->addError($attribute, 'Número Actual no puede ser mayor a Número Fin, ni menor a Número Inicial.');
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (strpos($this->prefijo, '-') !== false) {
            $_tmp = explode('-', $this->prefijo);
            array_walk($_tmp, function (&$value, &$key) {
                $value = str_pad(trim($value, " \t\n\r\0\x0B_"), 3, '0', STR_PAD_LEFT);
            });
            $this->prefijo = implode('-', $_tmp);
        } else {
            $this->prefijo = str_pad($this->prefijo, 6, '0', STR_PAD_LEFT);
            $this->prefijo = substr($this->prefijo, 0, 3) . '-' . substr($this->prefijo, 3, 3);
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturasCompra()
    {
        return $this->hasMany(Compra::className(), ['timbrado_detalle_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturasVenta()
    {
        return $this->hasMany(Venta::className(), ['timbrado_detalle_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRetenciones()
    {
        return $this->hasMany(Retencion::className(), ['timbrado_detalle_id' => 'id']);
    }

    // TODO: Este metodo no servira si se permite que varias personas carguen varios rangos de un mismo timbrado.
    public static function getTimDetIdPrefijo($prefijo, $nro) {
        /** @var TimbradoDetalle $query */
        $query = TimbradoDetalle::find()
            ->where(['=', 'prefijo', $prefijo])
            ->andWhere(['<=', 'nro_inicio', $nro])
            ->andWhere(['>=', 'nro_fin', $nro])
            ->one();

        return ($query != null) ? $query->id : null;
    }

    /**
     *  Determina si el nro de documento en formato 999-999-9999999 recibido como parametro se encuentra en el rango del
     * timbrado detalle.
     *
     * @param $nro_completo
     * @param TipoDocumentoSet|null $tipodocSet
     * @return bool
     */
    public function inRange($nro_completo, $tipodocSet = null)
    {
        $slices = explode('-', $nro_completo);
        $lastSeven = (int)array_pop($slices); // array_pop() devuelve el ultimo elemento y lo remueve del array.
        $prefijo = implode('-', $slices);

        $inRange = $prefijo == $this->prefijo && (int)$this->nro_inicio <= $lastSeven && $lastSeven <= (int)$this->nro_fin;
        $inRange = isset($tipodocSet) ? ($inRange && $this->tipo_documento_set_id == $tipodocSet->id) : $inRange;

        return $inRange;
    }

    /**
     *  Detarmina si el rango del timbrado detalle actual no este lleno.
     * Esta lleno si el nro_actual es mayor al nro_fin.
     *
     * @return bool
     */
    public function availableSlotExist()
    {
        return (int)$this->nro_actual <= (int)$this->nro_fin;
    }


    /**
     *  Determina si el nro de documento en formato 999-999-9999999 recibido como parametro corresponde a un timbrado
     * detalle.
     *
     * @param $nro_completo
     * @return bool
     */
    public function canHoldThisNumber($nro_completo, $tipodocSet = null)
    {
        return $this->availableSlotExist() && $this->inRange($nro_completo, $tipodocSet);
    }
}