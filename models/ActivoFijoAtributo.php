<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;
use backend\modules\contabilidad\models\validators\ActivoFijoAtributoValidator;

/**
 * This is the model class for table "cont_activo_fijo_atributo".
 *
 * @property int $id
 * @property int $activo_fijo_id
 * @property string $atributo
 * @property string $valor
 * @property int $activo_fijo_tipo_id
 * @property int $empresa_id
 * @property int $periodo_contable_id
 *
 * @property ActivoFijo $activoFijo
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoContable
 */
class ActivoFijoAtributo extends \backend\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_activo_fijo_atributo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo_fijo_id', 'atributo', 'empresa_id', 'periodo_contable_id'], 'required'],
            [['activo_fijo_id', 'atributo', 'empresa_id', 'periodo_contable_id', 'activo_fijo_tipo_id', 'valor'], 'safe'],
            [['activo_fijo_id', 'empresa_id', 'periodo_contable_id'], 'integer'],
            [['atributo'], 'string', 'max' => 100],
            [['valor'], 'string', 'max' => 256],
            [['activo_fijo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoFijo::className(), 'targetAttribute' => ['activo_fijo_id' => 'id']],
            [['activo_fijo_tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoFijoTipo::className(), 'targetAttribute' => ['activo_fijo_tipo_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],

            // Validaciones personalizadas
            [['valor'], ActivoFijoAtributoValidator::className()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activo_fijo_id' => 'Activo Fijo ID',
            'atributo' => 'Atributo',
            'valor' => 'Valor',
            'empresa_id' => 'Empresa ID',
            'periodo_contable_id' => 'Periodo Contable ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoFijo()
    {
        return $this->hasOne(ActivoFijo::className(), ['id' => 'activo_fijo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    public function loadEmpresaPeriodo()
    {
        $this->empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
    }
}
