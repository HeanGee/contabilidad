<?php

namespace backend\modules\contabilidad\models;

use common\helpers\FlashMessageHelpsers;
use yii\base\Model;
use yii\web\UploadedFile;


class EntidadArchivo extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $archivo_txt;

    public $archivo_path;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['archivo_txt'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 11, 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'archivo_txt' => 'Archivo'
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->archivo_path = [];

            foreach ($this->archivo_txt as $file) {
                $current_usr = \Yii::$app->user->identity->username;
                $basepath = "uploads/$current_usr/entidades/";
                \yii\helpers\FileHelper::createDirectory($basepath);
                $path = $basepath . $file->baseName . "." . $file->extension;
                $saved = $file->saveAs($path);
                if ($saved) {
                    $this->archivo_path[] = $path;
                } else {
                    return false;
                }
            }

            return true;
        } else {
            \Yii::error($this->errors);
            return false;
        }
    }

}
