<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use backend\modules\contabilidad\models\validators\RetencionNroValidator;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cont_retencion".
 *
 * Retenciones desde venta: retenciones recibidas.
 * Retenciones desde compra: retenciones emitidas.
 *
 * La entidad que compra emite una retención, si la misma está autorizada por la set, a la entidad que vende.
 * Esto significa que en vez de pagarle X monto, se paga/entrega al vendedor X-0.3*IVA(del X) al vendedor.
 * La retención así emitida se documenta usando el timbrado del comprador.
 * Para la entidad vendedora, no le preocupa velar por la correctitud de la retención recibida ya que la SET garantiza
 * su control.
 *
 * @property string $id
 * @property string $fecha_emision
 * @property string $nro_retencion
 * @property string $base_renta_porc
 * @property string $base_renta_cabezas
 * @property string $base_renta_toneladas
 * @property string $factor_renta_porc
 * @property string $factor_renta_cabezas
 * @property string $factor_renta_toneladas
 * @property int $factura_compra_id
 * @property int $factura_venta_id
 * @property int $asiento_id
 * @property int $recibo_id
 * @property int $empresa_id
 * @property int $periodo_contable_id
 * @property int $timbrado_detalle_id
 * @property string $tipo_factura
 * @property string $total
 * @property string $bloqueado
 *
 * @property string $factura_id
 * @property int $retencion_id_selector
 * @property int $timbrado_nro
 *
 * @property Compra $facturaCompra
 * @property Venta $facturaVenta
 * // * @property RetencionDetalleBaseIvas[] $retencionDetalleBases
 * // * @property RetencionDetalleFactorIvas[] $retencionDetalleFactors
 * @property Asiento $asiento
 * @property Recibo $recibo
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoContable
 * @property RetencionDetalleIvas[] $detalles
 * @property TimbradoDetalle $timbradoDetalle
 * @property Timbrado $timbrado
 * @property string $baseRenta
 * @property string $factorRenta
 */
class Retencion extends BaseModel
{
    public $factura_id;
    public $retencion_id_selector;
    public $timbrado_nro; // se usaba hasta antes de implementar select2 para timbrado_detalle_id, similar a factura compraventa.

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_retencion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_emision', 'nro_retencion', 'factura_id', 'empresa_id', 'periodo_contable_id', 'timbrado_detalle_id', 'bloqueado'], 'required'],
//            [['base_renta_porc', 'base_renta_cabezas', 'base_renta_toneladas'], 'required'],
//            [['factor_renta_porc', 'factor_renta_cabezas', 'factor_renta_toneladas'], 'required'],
            [['fecha_emision', 'factura_id', 'tipo_factura', 'retencion_id_selector', 'timbrado_nro', 'asiento_id'], 'safe'],
            [['factura_compra_id', 'factura_venta_id', 'factura_id', 'asiento_id', 'recibo_id', 'retencion_id_selector', 'empresa_id', 'periodo_contable_id', 'timbrado_detalle_id'], 'integer'],
            [['tipo_factura', 'bloqueado'], 'string'],
            [['nro_retencion'], 'string', 'max' => 15],
            [['nro_retencion'], RetencionNroValidator::className()],
            [['nro_retencion'], 'unique'],
            [['total'], 'number'],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],
            [['factura_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['factura_venta_id' => 'id']],
            [['asiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id' => 'id']],
            [['recibo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recibo::className(), 'targetAttribute' => ['recibo_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['timbrado_detalle_id'], 'exist', 'skipOnError' => true, 'targetClass' => TimbradoDetalle::className(), 'targetAttribute' => ['timbrado_detalle_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_emision' => 'Fecha Emisión',
            'nro_retencion' => 'Nº Retención',
            'base_renta_porc' => 'Base Renta Porc',
            'base_renta_cabezas' => 'Base Renta Cabezas',
            'base_renta_toneladas' => 'Base Renta Toneladas',
            'factor_renta_porc' => 'Factor Renta Porc',
            'factor_renta_cabezas' => 'Factor Renta Cabezas',
            'factor_renta_toneladas' => 'Factor Renta Toneladas',
            'factura_compra_id' => 'Factura Compra',
            'factura_venta_id' => 'Factura Venta',
            'factura_id' => 'Factura',
            'retencion_id_selector' => "Retenciones existentes",
            'timbrado_nro' => 'Timbrado',
            'timbrado_detalle_id' => 'Timbrado',
        ];
    }

    public function getBaseRenta()
    {
        return number_format($this->base_renta_porc, 2, ',', '.');
    }

    public function getFactorRenta()
    {
        return number_format($this->factor_renta_porc, 2, ',', '.');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * Usado en el view _facturas_relacionadas.php en la carpeta facturas-relacionadas.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->getFacturaCompra();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'factura_venta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimbradoDetalle()
    {
        return $this->hasOne(TimbradoDetalle::className(), ['id' => 'timbrado_detalle_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimbrado()
    {
        $timbradoDetalle = TimbradoDetalle::findOne(['id' => $this->timbrado_detalle_id]);

        return isset($timbradoDetalle) ? $timbradoDetalle->getTimbrado() : null;
    }

    /**
     * Usado en el view _facturas_relacionadas.php en la carpeta facturas-relacionadas.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->getFacturaVenta();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecibo()
    {
        return $this->hasOne(Recibo::className(), ['id' => 'recibo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsiento()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id']);
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getRetencionDetalleBases()
//    {
//        return $this->hasMany(RetencionDetalleBaseIvas::className(), ['retencion_id' => 'id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getRetencionDetalleFactors()
//    {
//        return $this->hasMany(RetencionDetalleFactorIvas::className(), ['retencion_id' => 'id']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalles()
    {
        return $this->hasMany(RetencionDetalleIvas::className(), ['retencion_id' => 'id']);
    }

    /**
     * Obtiene el total de la retencion actual, de la base de datos.
     * Significa que no obtiene el total configurado en el formulario, sino el original.
     *
     * Si se solicita dividir las retenciones en IVAs, la retencion de RENTA no se incluye.
     * @param bool $splitForEachIva
     * @return array|float|mixed
     */
    public function getTotalRetencion($splitForEachIva = false)
    {
        /** @var RetencionDetalleBaseIvas[] $bases */
        /** @var RetencionDetalleFactorIvas[] $factores */

        $retencion = Retencion::findOne(['id' => $this->id]);
        $detalles = RetencionDetalleIvas::findAll(['retencion_id' => $this->id]);

        $total_ivas = [];
        $total = 0.0;
        !isset($retencion) || $total += round(round($retencion->base_renta_porc, 2) * (round($retencion->factor_renta_porc, 2) / 100), 2);
        foreach ($detalles as $detalle) {
            $total_ivas[$detalle->iva->porcentaje] = round(round($detalle->base, 2) * round($detalle->factor / 100.0, 2), 2);
            $total += round($total_ivas[$detalle->iva->porcentaje], 2);
        }
        if ($splitForEachIva) return $total_ivas;
        return $total;
    }

    /**
     * @param $factores RetencionDetalleFactorIvas[]
     * @param $base RetencionDetalleBaseIvas
     * @return int
     */
    private function getFactorWithSameIva($factores, $base)
    {
        foreach ($factores as $index => $factor) {
            if ($base->iva->porcentaje == $factor->iva->porcentaje) return $index;
        }
        return -1;
    }

    /**
     *  Retorna TRUE si los datos necesarios antes de la creacion de retenciones existen.
     * Lanza excepcion en caso contrario.
     *
     * @return bool
     * @throws \Exception
     */
    public static function isCreable($operacion)
    {
        try {
            if (!isset($operacion) || $operacion == '')
                throw new \Exception("Error en la URL: Falta el parametro \$operacion");

            $empresaPeriodo = self::checkEmpresaPeriodoActual();
            $tipodocSET = self::tipodocSetRetencionExist();
            $timbradoRetenc = true;
            if ($operacion == 'compra') {
                $timbradoRetenc = self::timbradoParaRetencionesExist();
            }
            // more check-function-call goes here

            return $empresaPeriodo && $timbradoRetenc && $tipodocSET; // concatenate more check-variables here
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     *  Retorna TRUE si existe empresa y periodo actual. Lanza excepcion en caso contrario.
     *
     * @return bool
     * @throws Exception
     */
    public static function checkEmpresaPeriodoActual()
    {
        try {
            if (Yii::$app->session->has('core_empresa_actual') && Yii::$app->session->has('core_empresa_actual_pc'))
                return true;
            else {
                if (!Yii::$app->session->has('core_empresa_actual'))
                    throw new Exception("Falta especificar empresa actual.");
                else
                    throw new Exception("Falta especificar periodo actual.");
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     *  Retorna TRUE si existe timbrado para retenciones parametrizado en la empresa actual.
     * Lanza excepcion en caso contrario.
     *
     * @return bool
     * @throws Exception
     */
    public static function timbradoParaRetencionesExist()
    {
        try {
            if (self::checkEmpresaPeriodoActual()) {
                $empresa = \Yii::$app->session->get('core_empresa_actual');
                $periodo = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$empresa}-periodo-{$periodo}-timbrado_para_retenciones";
                $parmsys = ParametroSistema::find()->where(['nombre' => $nombre]);
                if (!$parmsys->exists() || $parmsys->exists() && $parmsys->one()->valor == "")
                    throw new Exception("Falta parametrizar en la Empresa Actual un Timbrado sólo para Retenciones.");
                return true;
            }
            return false;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     *  Verifica si el parametro empresa por tipo de documento SET para retenciones EMITIDAS existe.
     * Lanza excepcion en caso contrario.
     *
     * @return bool
     * @throws Exception
     */
    public static function tipodocSetRetencionExist()
    {
        try {
            if (self::checkEmpresaPeriodoActual()) {
                $empresa = \Yii::$app->session->get('core_empresa_actual');
                $periodo = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$empresa}-periodo-{$periodo}-tipodoc_set_retencion";
                $parmsys = ParametroSistema::find()->where(['nombre' => $nombre]);
                if (!$parmsys->exists() || $parmsys->exists() && $parmsys->one()->valor == "")
                    throw new Exception("Falta parametrizar en la Empresa Actual el Tipo de Documento SET para Retenciones Emitidas.");
                return true;
            }
            return false;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     *  Obtiene el timbrado para retenciones del parametro empresa. Lanza excepcion en caso contrario.
     *
     * @return Timbrado|null
     * @throws Exception
     */
    public static function getTimbradoRetencion()
    {
        try {
            self::checkEmpresaPeriodoActual();
            self::timbradoParaRetencionesExist();

            $empresa = \Yii::$app->session->get('core_empresa_actual');
            $periodo = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$empresa}-periodo-{$periodo}-timbrado_para_retenciones";
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);

            $timbrado = Timbrado::findOne($parmsys->valor);
            return $timbrado;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     *  Devuelve el detalle de timbrado segun sea emitidas (retenciones de compra) o recibidas (retenciones de venta).
     * Lanza excepcion si hay algun error en el proceso.
     *
     * Solamente existe 1 solo timbrado de retencion por entidades.
     *
     * @param $operacion
     * @return TimbradoDetalle
     * @throws Exception
     * @throws \Exception
     */
    public function manageNroRetencion($operacion)
    {
        /** @var Timbrado $timbrado */
        try {
            $timbrado = null;
            $timbradoDetalle = null;
            $tipodocSetRet = self::getTipodocSetRetencion();

            if ($operacion == 'compra') {
                // El timbrado si o si es para retenciones y SOLAMENTE PARA retenciones.
                $timbrado = self::getTimbradoRetencion(); // empresa-periodo actuales y timbrado para retenciones existen
            } elseif ($operacion == 'venta') {
                $entidad_id = $this->facturaVenta->entidad_id;
                $timbrado = Timbrado::find()->alias('tim')
                    ->leftJoin('cont_timbrado_detalle as det', 'tim.id = det.timbrado_id')
                    ->where(['tim.entidad_id' => $entidad_id, 'det.tipo_documento_set_id' => $tipodocSetRet->id]);
                if ($timbrado->exists()) {
                    // El timbrado si o si es para retenciones y SOLAMENTE PARA retenciones.
                    $timbrado = $timbrado->one();
                    if ($timbrado->nro_timbrado != $this->timbrado_nro) {
                        throw new \Exception("No se puede usar el timbrado {$this->timbrado_nro}: Ya hay un Timbrado para retenciones de la Entidad {$this->facturaVenta->entidad->razon_social} que es {$timbrado->nro_timbrado}.");
                    }
                } else {
                    // En este punto puede ocurrir que:
                    // a) Existe un timbrado sin detalles: Ocurre cuando es timbrado para retenciones pero se borraron
                    // todas las retenciones, eliminando asi tambien los detalles timbrados que ya no son utilizados por
                    // ninguna retencion.
                    // b) El timbrado ingresado es uno que se usa para las facturas.
                    if ($this->timbrado_nro == '') {
                        throw new \Exception("Error en el nro de Timbrado: El Nro de Timbrado no puede estar vacio.");
                    }
                    $timbrado = Timbrado::findOne(['nro_timbrado' => $this->timbrado_nro]);
                    if (isset($timbrado))
                        foreach ($timbrado->timbradoDetalles as $timbradoDetalle) {
                            if ($timbradoDetalle->tipo_documento_set_id != $tipodocSetRet->id) {
                                throw new \Exception("Error en el nro de Timbrado: Este Timbrado contempla detalles que no son para retenciones también. Significa que fue utilizado desde Compra o desde Venta.");
                            }
                        }
                    else {
                        $timbrado = new Timbrado(['scenario' => Timbrado::SCENARIO_RETENCION]);
                    }
                    $timbrado->nro_timbrado = $this->timbrado_nro;
                    $fecha = $this->fecha_emision;
                    {
                        $slices = explode('-', $fecha);
                        if (strlen($slices[2]) > 2)
                            $fecha = implode('-', array_reverse($slices));
                    }
                    $timbrado->fecha_fin = $timbrado->fecha_inicio = $fecha;
                    $timbrado->entidad_id = $entidad_id;
                    $timbrado->ruc = $timbrado->nombre_entidad = '-';
                    if (!$timbrado->save()) {
                        throw new \Exception("Error guardando nuevo timbrado: {$timbrado->getErrorSummaryAsString()}");
                    }
                    $timbrado->refresh();

                    $timbradoDetalle = new TimbradoDetalle();
                    $slices = explode('-', $this->nro_retencion);
                    $lastSeven = (int)array_pop($slices); // array_pop() devuelve el ultimo elemento y lo remueve del array.
                    $prefijo = implode('-', $slices);
                    $timbradoDetalle->timbrado_id = $timbrado->id;
                    $timbradoDetalle->tipo_documento_set_id = $tipodocSetRet->id;
                    $timbradoDetalle->prefijo = $prefijo;
                    $timbradoDetalle->nro_inicio = 1;
                    $timbradoDetalle->nro_fin = 9999999;
                    $timbradoDetalle->nro_actual = $lastSeven + 1;
                    if (!$timbradoDetalle->save()) {
                        throw new \Exception("Error guardando nuevo detalle: {$timbradoDetalle->getErrorSummaryAsString()}");
                    }
                    $timbradoDetalle->refresh();
                    $timbrado->refresh();
                    Yii::warning("timbrado id: {$timbrado->id}, detalle id: {$timbradoDetalle->id}");
                }
            }

            // En este punto se supone que si o si va a haber un timbrado para retenciones.
            $timbradoDetalle = $timbrado->findDetailThatCanHoldThisNumber($this->nro_retencion, $tipodocSetRet);

            // Hay timbrado pero no hay detalle que contemple al nro de retencion
            if (!isset($timbradoDetalle)) {
                $timbradoDetalle = new TimbradoDetalle();
                $slices = explode('-', $this->nro_retencion);
                $lastSeven = (int)array_pop($slices); // array_pop() devuelve el ultimo elemento y lo remueve del array.
                $prefijo = implode('-', $slices);
                $timbradoDetalle->timbrado_id = $timbrado->id;
                $timbradoDetalle->tipo_documento_set_id = $tipodocSetRet->id;
                $timbradoDetalle->prefijo = $prefijo;
                $timbradoDetalle->nro_inicio = 1;
                $timbradoDetalle->nro_fin = 9999999;
                $timbradoDetalle->nro_actual = $lastSeven + 1;
                if (!$timbradoDetalle->save()) {
                    throw new \Exception("Error guardando nuevo detalle: {$timbradoDetalle->getErrorSummaryAsString()}");
                }
                $timbradoDetalle->refresh();
                $timbrado->refresh();
            }

            $this->timbrado_nro = $timbradoDetalle->timbrado->nro_timbrado;
            return $timbradoDetalle;

        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return TipoDocumentoSet|null
     * @throws Exception
     */
    public static function getTipodocSetRetencion()
    {
        try {
            self::checkEmpresaPeriodoActual();

            $empresa = \Yii::$app->session->get('core_empresa_actual');
            $periodo = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$empresa}-periodo-{$periodo}-tipodoc_set_retencion";
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);

            $tipoDocSet = TipoDocumentoSet::findOne($parmsys->valor);
            return $tipoDocSet;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     *  Verifica formato y unicidad del nro de retencion.
     *
     *  Es utilizado por el deferred y por el validator.
     *
     * @param $nro_retencion
     * @param $retencion_id
     * @return array
     */
    public static function checkNroRetencion($nro_retencion, $retencion_id)
    {
        $query = Retencion::find()->where([
            'nro_retencion' => $nro_retencion,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ])->andFilterWhere(['!=', 'id', $retencion_id]);

        if ($query->exists()) {
            return ['result' => '', 'error' => "Nro de Retención {$nro_retencion} ya fue utilizado.", 'attribute' => 'nro_retencion'];
        }

        if (strlen(str_replace('_', '', $nro_retencion)) < 15)
            return ['result' => '', 'error' => "Error en el formato del número de retención.", 'attribute' => 'nro_retencion'];

        return ['result' => '', 'error' => null, 'attribute' => ""];
    }

    public function beforeValidate()
    {
        self::numberFormatForDB();

        if ($this->bloqueado == null)
            $this->bloqueado = 'no';

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function numberFormatForDB()
    {
        $attributes = ['base_renta_porc', 'base_renta_cabezas', 'base_renta_toneladas', 'factor_renta_porc', 'factor_renta_cabezas', 'factor_renta_toneladas'];
        foreach ($attributes as $attribute) {
            if (strpos($this->$attribute, '.') && strlen(substr($this->$attribute, (strpos($this->$attribute, '.') + 1))) >= 3) {
                $this->$attribute = str_replace('.', '', $this->$attribute);
                $this->$attribute = str_replace(',', '.', $this->$attribute);
            } elseif (strpos($this->$attribute, ','))
                $this->$attribute = str_replace(',', '.', $this->$attribute);

            if ($this->$attribute == "") $this->$attribute = 0;
            if ($this->$attribute == "") $this->$attribute = 0;
        }
    }

    /**
     * @param bool $doMap
     * @param array $ids
     * @param null $condicion
     * @param bool $concatRazonSocial
     * @param bool $sinRetenciones
     * @param bool $saldoGreaterThanZero
     * @param string $entidad_id
     * @param null $fecha_hasta
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getVentasParaRetener(
        $doMap = false, $ids = [], $condicion = null/*el cliente quiso que no predetermine a credito*/, $concatRazonSocial = false, $sinRetenciones = false,
        $saldoGreaterThanZero = false, $entidad_id = "", $fecha_hasta = null)
    {
        $tname = Venta::tableName();
        $query = Venta::find();
        $concat = "CONCAT((" . $tname . ".prefijo), ('-'), (" . $tname . ".nro_factura), (' - '), (moneda.nombre))";
        if ($concatRazonSocial) $concat = "CONCAT((" . $tname . ".prefijo), ('-'), (" . $tname . ".nro_factura), (' \('), (entidad.razon_social), ('\)'), (' - '), (moneda.nombre))";
        $query->joinWith('moneda as moneda');
        $query->leftJoin('core_cotizacion AS cotizacion', 'cotizacion.moneda_id = moneda.id');
        $query->joinWith('entidad as entidad');
        $query->select([
            'MIN(' . $tname . '.id) as id',
            $concat . ' as text',
            'MIN(' . $tname . '.moneda_id) as moneda_id',
            'MIN(' . $tname . '.entidad_id) as entidad_id',
            'MIN(' . $tname . '.saldo) as saldo',
            'MIN(' . $tname . '.valor_moneda) as valor_moneda',
            'MIN(entidad.razon_social) AS razon_social',
            'MIN(entidad.ruc) AS ruc',
            'MIN(' . $tname . '.fecha_emision) as fecha_emision',
        ]);

//        // cotizacion de 1 dia anterior a la fecha_hasta o de ayer
//        $fecha_cotizacion = isset($fecha_hasta) ?
//            date('Y-m-d', strtotime('-1 day', strtotime($fecha_hasta))) : date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
//        $query->where(
//            ['OR',
//                ['moneda.id' => 1],
//                ['AND',
//                    ['!=', 'moneda.id', 1],
//                    ['=', 'cotizacion.fecha', date('Y-m-d', strtotime($fecha_cotizacion))]
//                ]
//            ]
//        );
        $query->andWhere([
            $tname . '.empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            $tname . '.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
            $tname . '.estado' => 'vigente',
        ]);

        if ($saldoGreaterThanZero) $query->andWhere(['>', $tname . '.saldo', 0]);
        $query->andFilterWhere([$tname . '.condicion' => $condicion]);
        $query->andFilterWhere(['<=', $tname . '.fecha_emision', $fecha_hasta]);
        if (sizeof($ids) > 0) {
            $query->andFilterWhere(['NOT IN', $tname . '.id', $ids]);
        }
        if ($sinRetenciones) {
            $ventaConRetencionIDs = [];
            $retenciones = Retencion::findAll(['!=', 'factura_venta_id', null]);
            foreach ($retenciones as $retencion) {
                $ventaConRetencionIDs[] = $retencion->id;
            }
            $query->andWhere(['NOT IN', 'id', $ventaConRetencionIDs]);
        }
        $query->andFilterWhere(['entidad_id' => $entidad_id]);

        // No incluir notas
        $query->andWhere(['IS', "{$tname}.factura_venta_id", null]);

        $query->groupBy($tname . '.id');
        return $doMap ? ArrayHelper::map($query->asArray()->all(), 'id', 'text') : $query->asArray()->all();
    }

    /**
     * @param bool $doMap
     * @param array $ids
     * @param null $condicion
     * @param bool $concatRazonSocial
     * @param bool $sinRetenciones
     * @param bool $saldoGreaterThanZero
     * @param string $entidad_id
     * @param null $fecha_hasta
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getComprasParaRetener(
        $doMap = false, $ids = [], $condicion = null /*el cliente quiso que no predetermine a credito*/, $concatRazonSocial = false, $sinRetenciones = false,
        $saldoGreaterThanZero = false, $entidad_id = "", $fecha_hasta = null)
    {
        $tname = Compra::tableName();
        $query = Compra::find();
        $concat = $tname . '.nro_factura';
        if ($concatRazonSocial) $concat = "CONCAT((" . $tname . ".nro_factura), ('-'), (' \('), (entidad.razon_social), ('\)'))";
        $query->joinWith('moneda as moneda');
        $query->leftJoin('core_cotizacion AS cotizacion', 'cotizacion.moneda_id = moneda.id');
        $query->joinWith('entidad as entidad');
        $query->select([
            'MIN(' . $tname . '.id) as id',
            $concat . ' as text',
            'MIN(' . $tname . '.moneda_id) as moneda_id',
            'MIN(' . $tname . '.entidad_id) as entidad_id',
            'MIN(moneda.nombre) as moneda_nombre',
            'MIN(' . $tname . '.saldo) as saldo',
            'MIN(' . $tname . '.cotizacion) as valor_moneda',
            'MIN(entidad.razon_social) AS razon_social',
            'MIN(entidad.ruc) AS ruc',
            'MIN(cotizacion.compra) as compra',
            'MIN(cotizacion.venta) as venta',
            'MIN(' . $tname . '.fecha_emision) as fecha_emision',
        ]);

//        // cotizacion de 1 dia anterior a la fecha_hasta o de ayer
//        $fecha_cotizacion = isset($fecha_hasta) ?
//            date('Y-m-d', strtotime('-1 day', strtotime($fecha_hasta))) : date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
//        $query->where(
//            ['OR',
//                ['moneda.id' => 1],
//                ['AND',
//                    ['!=', 'moneda.id', 1],
//                    ['=', 'cotizacion.fecha', date('Y-m-d', strtotime($fecha_cotizacion))]
//                ]
//            ]
//        );
        $query->andWhere([
            $tname . '.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            $tname . '.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            $tname . '.estado' => 'vigente',
        ]);

        if ($saldoGreaterThanZero) $query->andWhere(['>', $tname . '.saldo', 0]);
        $query->andFilterWhere([$tname . '.condicion' => $condicion,]);
        $query->andFilterWhere(['<=', $tname . '.fecha_emision', $fecha_hasta]);
        if (sizeof($ids) > 0) {
            $query->andWhere(['NOT IN', $tname . '.id', $ids]);
        }
        if ($sinRetenciones) {
            $compraConRetencionesIDs = [];
            $retenciones = Retencion::findAll(['!=', 'factura_compra_id', null]);
            foreach ($retenciones as $retencion) {
                $compraConRetencionesIDs[] = $retencion->id;
            }
            $query->andWhere(['NOT IN', 'id', $compraConRetencionesIDs]);
        }
        $query->andFilterWhere(['entidad_id' => $entidad_id]);

        // No incluir notas
        $query->andWhere(['IS', "{$tname}.factura_compra_id", null]);

        $query->groupBy($tname . '.id');
        return $doMap ? ArrayHelper::map($query->asArray()->all(), 'id', 'text') : $query->asArray()->all();
    }

    public function checkEditability()
    {
        $msg = '';
        if (isset($this->asiento))
            $msg = "Esta retencion tiene asiento asociado (ID:{$this->asiento_id})";

        return $msg;
    }
}
