<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;

/**
 * @property string $id
 * @property string $cotizacion
 * @property int $es_local //Si - No
 * @property int $importacion_id
 * @property int $factura_compra_id
 * @property int $plantilla_id
 * @property string $plantilla_factura_id
 * @property int $absorber_impuesto
 * @property int $incluir_costo
 *
 * @property Importacion $importacion
 * @property Compra $facturaCompra
 * @property PlantillaCompraventa $plantilla
 * @property int $monto
 * @property int $montoSinImpuesto
 */
class ImportacionDetalleFacturaLocalExterior extends BaseModel
{
    public $ivas = [];
    public $plantilla_factura_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_importacion_detalle_factura_local_exterior';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['es_local', 'importacion_id', 'plantilla_factura_id'], 'required'],
            [['cotizacion', 'monto', 'absorber_impuesto', 'incluir_costo'], 'number'],
            [['importacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Importacion::className(), 'targetAttribute' => ['importacion_id' => 'id']],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],
            [['plantilla_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlantillaCompraventa::className(), 'targetAttribute' => ['plantilla_id' => 'id']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cotizacion' => 'Cotización',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportacion()
    {
        return $this->hasOne(Importacion::className(), ['id' => 'importacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantilla()
    {
        return $this->hasOne(PlantillaCompraventa::className(), ['id' => 'plantilla_id']);
    }

    public function getMonto()
    {
        $array = CompraIvaCuentaUsada::find()
            ->where(['factura_compra_id' => $this->factura_compra_id, 'plantilla_id' => $this->plantilla_id])
            ->all();

        $monto = 0;
        /** @var CompraIvaCuentaUsada $icu */
        foreach ($array as $icu)
            $monto += (float)$icu->monto;

        return $monto;
    }

    public function getMontoSinImpuesto()
    {
        $array = CompraIvaCuentaUsada::find()
            ->where(['factura_compra_id' => $this->factura_compra_id, 'plantilla_id' => $this->plantilla_id])
            ->all();

        $monto = 0;
        /** @var CompraIvaCuentaUsada $icu */
        foreach ($array as $icu) {
            if ($icu->iva_cta_id == null or $icu->ivaCta->iva == 0)
                $monto += (float)$icu->monto;
            else
                $monto += ((float)$icu->monto - ($icu->monto / (1.0 + $icu->ivaCta->iva->porcentaje / 100.0)));
        }
        return $monto;
    }

    public function getMontoByPorcentajeIva()
    {
        $toReturn = [];
        $array = CompraIvaCuentaUsada::find()->select(['min(monto) monto', 'min(iva.porcentaje) porcentaje', 'min(iva_cta_id) iva_cta_id'])
            ->joinWith('ivaCta ivaCta')
            ->joinWith('ivaCta.iva iva')
            ->where(['factura_compra_id' => $this->factura_compra_id, 'plantilla_id' => $this->plantilla_id])
            ->groupBy(['iva_cta_id'])
            ->asArray()
            ->all();


        foreach ($array as $item) {
            $porcentaje = 0;
            if ($item['porcentaje'] != null)
                $porcentaje = $item['porcentaje'];
            $toReturn[$porcentaje] = $item['monto'];
        }

        return $toReturn;
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->absorber_impuesto = $this->absorber_impuesto == null ? 0 : $this->absorber_impuesto;
        $this->incluir_costo = $this->incluir_costo == null ? 0 : $this->incluir_costo;
        return parent::loadDefaultValues($skipIfSet);
    }
}
