<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;

/**
 * This is the model class for table "cont_activo_biologico_stock_manager".
 *
 * @property int $id
 * @property int $factura_venta_id
 * @property int $factura_compra_id
 * @property int $nota_compra_id
 * @property int $nota_venta_id
 * @property int $especie_id
 * @property int $clasificacion_id
 * @property int $cantidad
 * @property string $precio_unitario
 * @property int $periodo_contable_id
 * @property int $empresa_id
 *
 * @property ActivoBiologicoEspecieClasificacion $clasificacion
 * @property Compra $facturaCompra
 * @property Compra $notaCompra
 * @property Empresa $empresa
 * @property ActivoBiologicoEspecie $especie
 * @property EmpresaPeriodoContable $periodoContable
 * @property Venta $facturaVenta
 * @property Venta $notaVenta
 * @property ActivoBiologico $activoBiologico // agregado al hacer nota credito compra
 */
class ActivoBiologicoStockManager extends \backend\models\BaseModel
{
    const SCENARIO_NOTA = 'nota';

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_NOTA] = ['username', 'password'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_activo_biologico_stock_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['factura_venta_id', 'factura_compra_id', 'especie_id', 'clasificacion_id', 'periodo_contable_id',
                'empresa_id',], 'integer'],
            [['cantidad'], 'integer'],
            [['precio_unitario', 'nota_compra_id', 'nota_venta_id'], 'safe'],
            [['periodo_contable_id', 'empresa_id'], 'required'],
            [['clasificacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoBiologicoEspecieClasificacion::className(), 'targetAttribute' => ['clasificacion_id' => 'id']],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['especie_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoBiologicoEspecie::className(), 'targetAttribute' => ['especie_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['factura_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['factura_venta_id' => 'id']],
            [['nota_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['nota_venta_id' => 'id']],
            [['nota_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['nota_compra_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'factura_venta_id' => 'Factura Venta ID',
            'factura_compra_id' => 'Factura Compra ID',
            'especie_id' => 'Especie ID',
            'clasificacion_id' => 'Clasificacion ID',
            'cantidad' => 'Cantidad',
            'precio_unitario' => 'Precio Unitario',
            'periodo_contable_id' => 'Periodo Contable ID',
            'empresa_id' => 'Empresa ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasificacion()
    {
        return $this->hasOne(ActivoBiologicoEspecieClasificacion::className(), ['id' => 'clasificacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotaCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'nota_compra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecie()
    {
        return $this->hasOne(ActivoBiologicoEspecie::className(), ['id' => 'especie_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoBiologico()
    {
        return ActivoBiologico::find()->where([
            'especie_id' => $this->especie_id, 'clasificacion_id' => $this->clasificacion_id,
            'empresa_id' => $this->empresa_id, 'periodo_contable_id' => $this->periodo_contable_id
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'factura_venta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotaVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'nota_venta_id']);
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');

        return parent::loadDefaultValues($skipIfSet); // TODO: Change the autogenerated stub
    }

    /**
     *  Actualiza el stock de un activo biologico, dependiendo de si es venta o compra.
     *  Se puede saber si es compra o venta viendo si es null o no factura_compra_id y factura_venta_id.
     *  Utiliza promedio ponderado para calcular el nuevo precio unitario.
     *
     *  Si es compra, devuelve un Activo Biologico, que puede ser uno nuevo
     * o uno cuyo precio_unitario_actual y stock_actual se haya actaulizado.
     *
     *  Si es venta, devuelve un Activo Biologico, cuyo stock_actual y precio_unitario_actual fue actualizado. Devuelve
     * null si no existe ningun Activo Biologico (especie_id y clasificacion_id no matchean con los de este stockManager).
     * Cuando devuelve null significa que no se puede vender un A. Biologico que aún no exista en el stock.
     *
     * @return ActivoBiologico devuelve el activo biologico actualizado.
     */
//    public function actualizarStock()
//    {
//        // busca el a.bio que matchee especie_id y clasificacion_id de este StockManager.
//        /** @var ActivoBiologico $actbio */
//        $actbio = ActivoBiologico::find()->where([
//            'especie_id' => $this->especie_id,
//            'clasificacion_id' => $this->clasificacion_id,
//            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
//            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
//        ])->one();
//
//        // si es compra...
//        if (isset($this->factura_compra_id)) {
//
//            // ... pero el a.bio no existe, se crea uno nuevo.
//            if (!isset($actbio)) {
//                $actbio = new ActivoBiologico();
//                $actbio->loadDefaultValues();
//                $actbio->especie_id = $this->especie_id;
//                $actbio->clasificacion_id = $this->clasificacion_id;
//                $actbio->stock_inicial = $this->cantidad;
//                $actbio->stock_actual = $this->cantidad;
//                $actbio->stock_final = $this->cantidad;
//                $actbio->precio_unitario = $this->precio_unitario;
//                $actbio->precio_unitario_actual = $this->precio_unitario;
//                $actbio->empresa_id = \Yii::$app->session->get('core_empresa_actual');
//                $actbio->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
//
//                // ... y existe un a.bio que matchea especie_id y clasificacion_id de este StockManager,
//                // actualiza stock_actual y precio_unitario_actual del a.bio encontrado, usando promedio ponderado.
//            } else {
//                $_stock_acum = 0;
//                $_precio_total_acum = 0;
//
//                $query = ActivoBiologicoStockManager::find()->where([
//                    'especie_id' => $this->especie_id,
//                    'clasificacion_id' => $this->clasificacion_id,
//                    'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
//                    'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
//                ]);
//                $query->andWhere(['factura_compra_id' => $this->factura_compra_id]);
//
//                /** @var ActivoBiologicoStockManager $stock_manager */
//                foreach ($query->all() as $stock_manager) {
//                    $_stock_acum += (int)$stock_manager->cantidad;
//                    $_precio_total_acum += (int)$stock_manager->cantidad * (int)$stock_manager->precio_unitario;
//                }
//
//                $actbio->precio_unitario_actual = round(($_precio_total_acum + (int)$actbio->precio_unitario * (int)$actbio->stock_inicial) / ($_stock_acum + (int)$actbio->stock_inicial));
//                $actbio->stock_actual = $actbio->stock_inicial + (int)$_stock_acum;
//            }
//
//            // si es venta...
//        } elseif (isset($this->factura_venta_id)) {
//            // ... y existe un a.bio que matchea con especie_id y clasificacion_id de este StockManager,
//            // actualiza stock_actual, decrementando la cantidad vendida.
//            if (isset($actbio)) {
//                $actbio->stock_actual = (int)$actbio->stock_actual - (int)$this->cantidad;
//            }
//
//            // ... pero no existe ningun a.bio que matchee, entonces devuelve null,
//            // indicando que no se puede vender un activo que no existe en el sistema (en el stock)
//        }
//
//        return $actbio;
//    }

//    /**
//     *  Restablece el stock de un activo biologico, dependiendo de si es venta o compra
//     *
//     * @return ActivoBiologico devuelve el activo biologico restablecido.
//     */
//    public function restablecerStock()
//    {
//        // busca el a.bio que matchee especie_id y clasificacion_id de este StockManager.
//        /** @var ActivoBiologico $actbio */
//        $actbio = ActivoBiologico::find()->where([
//            'especie_id' => $this->especie_id,
//            'clasificacion_id' => $this->clasificacion_id,
//            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
//            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
//        ])->one();
//
//        // si es compra...
//        if (isset($this->factura_compra_id)) {
//
//            // ... y existe el a.bio, se actualiza el stock_actual y precio_unitario_actual como en compra, pero
//            // excluyendo el StockManager actual.
//            if (isset($actbio)) {
//                $_stock_acum = 0;
//                $_precio_unitario_acum = 0;
//
//                $query = ActivoBiologicoStockManager::find()->where([
//                    'especie_id' => $this->especie_id,
//                    'clasificacion_id' => $this->clasificacion_id,
//                    'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
//                    'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
//                ]);
//                $query->andWhere(['factura_compra_id' => $this->factura_compra_id]);
//                $query->andWhere(['<>', 'id', $this->id]); // el que excluye el StockManager actual.
//
//                /** @var ActivoBiologicoStockManager $stock_manager */
//                foreach ($query->all() as $stock_manager) {
//                    $_stock_acum += (int)$stock_manager->cantidad;
//                    $_precio_unitario_acum += (int)$stock_manager->cantidad * (int)$stock_manager->precio_unitario;
//                }
//
//                \Yii::warning($_stock_acum);
//                \Yii::warning($_precio_unitario_acum);
//
//                $actbio->precio_unitario_actual = round(($_precio_unitario_acum + (int)$actbio->precio_unitario * (int)$actbio->stock_inicial) / ($_stock_acum + (int)$actbio->stock_inicial));
//                $actbio->stock_actual = $actbio->stock_inicial + (int)$_stock_acum;
//                \Yii::warning($actbio->stock_actual);
//                \Yii::warning($actbio->precio_unitario_actual);
//            }
//
//            // si es venta...
//        } elseif (isset($this->factura_venta_id)) {
//            // ... y existe un a.bio que matchea con especie_id y clasificacion_id de este StockManager,
//            // restaura el stock actual, sumándole cantidad de este StockManager.
//            if (isset($actbio)) {
//                $actbio->stock_actual = (int)$actbio->stock_actual + (int)$this->cantidad;
//            }
//
//            // ... pero no existe ningun a.bio que matchee, entonces devuelve null,
//            // indicando que no se puede vender un activo que no existe en el sistema (en el stock)
//        }
//
//        return $actbio;
//    }

    /**
     *  Se utiliza este en lugar del validador porque en el modelo se quito el required, para que en el modal no se queje
     * cuando se elimina una fila pero el script validator se queda aun
     */
    public function datosValidos()
    {
        if ($this->scenario != self::SCENARIO_NOTA) {
            if ($this->precio_unitario == "" || $this->precio_unitario == '0' || $this->precio_unitario == 0) {
                $this->addError('precio_unitario', "Precio Unitario no puede ser vacío");
            }
            if ($this->cantidad == "" || $this->cantidad == '0' || $this->cantidad == 0) {
                $this->addError('cantidad', "Cantidad no puede ser vacío");
            }
            if ($this->especie_id == "" || $this->especie_id == "0") {
                $this->addError('especie_id', "Debe elegir una especie");
            }
            if ($this->clasificacion_id == "" || $this->clasificacion_id == "0") {
                $this->addError('clasificacion_id', "Debe elegir una clasificación");
            }
        }

        return !$this->hasErrors();
    }
}
