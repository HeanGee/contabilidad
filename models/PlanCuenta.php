<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use Exception;
use Yii;

/**
 * This is the model class for table "cont_plan_cuenta".
 *
 * @property int $id
 * @property string $cod_cuenta
 * @property string $nombre
 * @property int $empresa_id
 * @property string $asentable
 * @property int $padre_id
 * @property string $cod_completo
 * @property string $estado
 * @property string $cod_ordenable
 * @property string $flujo_efectivo
 *
 * @property PlanCuenta $padre
 * @property PlanCuenta[] $planCuentas
 * @property Empresa $empresa
 */
class PlanCuenta extends BaseModel
{
    public $es_global;

    const INIT_COD_CUENTA_DEBE = [1, 5, 10, 11, 13, 15];
    const INIT_COD_CUENTA_HABER = [2, 3, 4, 7, 8, 14];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_plan_cuenta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_cuenta', 'nombre', 'asentable', 'cod_completo', 'estado', 'cod_ordenable'], 'required'],
            [['empresa_id', 'padre_id'], 'integer'],
            [['asentable', 'estado', 'cod_ordenable', 'flujo_efectivo'], 'string'],
            [['flujo_efectivo'], 'default', 'value' => 'no'],
            [['cod_cuenta', 'cod_completo', 'cod_ordenable'], 'string', 'max' => 45],
            [['nombre'], 'string', 'max' => 255],
            [['cod_completo', 'empresa_id'], 'unique', 'targetAttribute' => ['cod_completo', 'empresa_id']],
            [['padre_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['padre_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['es_global'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_cuenta' => 'Código de Cuenta',
            'nombre' => 'Nombre',
            'empresa_id' => 'Empresa',
            'asentable' => 'Asentable',
            'padre_id' => 'Padre',
            'cod_completo' => 'Código Completo',
            'estado' => 'Estado',
            'es_global' => 'Es Global',
            'cod_ordenable' => 'Código Ordenable',
            'flujo_efectivo' => 'Para Flujo de Efectivo?'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPadre()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'padre_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanCuentas()
    {
        return $this->hasMany(PlanCuenta::className(), ['padre_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->nombre = strtoupper($this->nombre);
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert == false) {
            if (!empty($changedAttributes['cod_cuenta'])) {
                $consulta = $this->getTodosSusHijos();
                /** @var PlanCuenta $_h */
                foreach ($consulta as $_h) {
                    $_h->setCodigos();
                    $_h->save();
                }
            }
            if (!empty($changedAttributes['estado']) && $changedAttributes['estado'] == 'activo' && $this->estado == 'inactivo') {
                !empty($consulta) || $consulta = $this->getTodosSusHijos();
                foreach ($consulta as $_h) {
                    $_h->estado = 'inactivo';
                    $_h->save();
                }
            }
        }
    }

    public static function getValoresEnum($columna, $paraForm = false, bool $enum = false)
    {
        $_retornar = [];
        try {
            $valores_enum = PlanCuenta::getTableSchema()->columns[$columna]->enumValues;
            if ($paraForm) {
                $serie = $enum ? 1 : null;
                foreach ($valores_enum as $_v)
                    $_retornar[] = ['id' => $_v, 'text' => ($enum ? $serie++ . ' - ' : '') . ucfirst(str_replace('_', ' ', $_v))];
            } else {
                foreach ($valores_enum as $_v)
                    $_retornar[$_v] = ucfirst($_v);
            }

        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
        }
        return $_retornar;
    }

    public static function getCuentaLista($solo_asentable = false, $format = '')
    {
        $query = PlanCuenta::find();
        $concat = 'CONCAT(`cod_completo`, " - ", `nombre`)';
        $query->select(['id', 'text' => $concat, 'asentable', 'nombre']);
        $empresa_actual = Yii::$app->session->get('core_empresa_actual');
        if (empty($empresa_actual)) {
            $query->where(['empresa_id' => null]);
        } else {
            $query->where(['OR', ['empresa_id' => null], ['empresa_id' => $empresa_actual]]);
        }
        if ($solo_asentable)
            $query->andWhere([
                'estado' => 'activo',
                'asentable' => 'si'
            ]);
        $query->orderBy(['cod_ordenable' => SORT_ASC]);
        if ($format == 'ajax')
            return ['results' => $query->asArray()->all()];
        elseif ($format == 'index') {
            $data = [];
            foreach ($query->asArray()->all() as $item) {
                $data[$item['id']] = $item['text'];
            }
            return $data;
        }
        return $query->asArray()->all();
    }

    public function checkExisteCuenta()
    {
        $check = PlanCuenta::find();
        $check->where(['cod_completo' => $this->cod_completo]);
        if ($this->es_global == 'no')
            $check->andWhere(['OR', ['empresa_id' => null], ['empresa_id' => $this->empresa_id]]);
        $check->asArray();
        return !empty($check->one());
    }

    public static function getCodigoSiguiente($es_global, $cuenta_id = null)
    {
        $respuesta['siguiente'] = 1;
        $respuesta['respuesta'] = 200;
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $query = self::find()
            ->where([
                'padre_id' => $cuenta_id,
                'CAST(cod_cuenta AS UNSIGNED)' => '1'
            ])
            ->asArray();
        if ($es_global == 'si') {
            $query->andWhere(['empresa_id' => null]);
        } else {
            $query->andWhere(['OR', ['empresa_id' => null], ['empresa_id' => $empresa_id]]);
        }
        $db_resultado = $query->one();
        if (!empty($db_resultado)) {
            $sub_query = self::find()
                ->select('`cod_cuenta` + 0 AS `cod_cuenta`')
                ->where(['`padre_id`' => $cuenta_id]);
            $query = self::find()
                ->alias('plan1')
                ->select('plan1.`cod_cuenta` + 1 AS `siguiente`')
                ->leftJoin(['plan2' => $sub_query], 'plan1.`cod_cuenta` + 1 = plan2.`cod_cuenta`')
                ->where([
                    'plan1.`padre_id`' => $cuenta_id,
                    'plan2.`cod_cuenta`' => null
                ])
                ->orderBy(['`siguiente`' => SORT_ASC])
                ->asArray();
            if ($es_global == 'no') {
                $query->andWhere(['OR', ['plan1.`empresa_id`' => null], ['plan1.`empresa_id`' => $empresa_id]]);
            }
            $db_resultado = $query->one();
            $respuesta['siguiente'] = (int)$db_resultado['siguiente'];
        }
        return $respuesta;
    }

    public function globalAIndividual()
    {
        $empresa_actual = Yii::$app->session->get('core_empresa_actual');
        $original = $this->attributes;
        $empresas = Empresa::find()
            ->select('id')
            ->where(['activo' => 1])
            ->andWhere(['!=', 'id', $empresa_actual])
            ->asArray()
            ->all();
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            foreach ($empresas as $_empresa) {
                $_modelo = new PlanCuenta();
                $_modelo->attributes = $original;
                $_modelo->empresa_id = $_empresa['id'];
                if (!$_modelo->save())
                    throw new Exception('Error al guardar una cuenta en una de las empresas.');

                $individuales = PlanCuenta::find()
                    ->where([
                        'padre_id' => $this->id,
                        'empresa_id' => $_empresa['id']
                    ])
                    ->all();
                foreach ($individuales as $_individual) {
                    $_individual->padre_id = $_modelo->id;
                    if (!$_individual->save())
                        throw new Exception('Error al actualizar una cuenta individual en una de las empresas.');
                }
            }
            $transaction->commit();
            $this->empresa_id = $empresa_actual;

        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }
        return true;
    }

    public function individualAGlobal()
    {
        $check = PlanCuenta::find()
            ->alias('plan')
            ->joinWith('empresa AS empresa')
            ->where([
                'plan.`cod_completo`' => $this->cod_completo,
                'empresa.`activo`' => 1
            ])
            ->asArray()
            ->all();
        if (empty($check)) {
            $this->empresa_id = null;
            return true;
        }
        return false;
    }

    public function setCodigos()
    {
        if (!empty($this->padre_id)) {
            $this->cod_cuenta = str_pad($this->cod_cuenta, 2, '0', STR_PAD_LEFT);
            $this->cod_completo = $this->padre->cod_completo . '.' . $this->cod_cuenta;
        } else {
            $this->cod_completo = $this->cod_cuenta = (string)intval($this->cod_cuenta);
        }
        $this->setCodigoOrdenable();
    }

    public function setCodigoOrdenable()
    {
        $_temp = explode('.', $this->cod_completo);
        foreach ($_temp as &$_codigo)
            $_codigo = str_pad($_codigo, 3, '0', STR_PAD_LEFT);
        $this->cod_ordenable = implode('.', $_temp);
    }

    public function getActivosFijos()
    {
        return $this->hasMany(ActivoFijo::className(), ['activo_fijo_tipo_id' => 'id']);
    }

    public function getNombreConCodigo()
    {
        return $this->cod_completo . ' - ' . $this->nombre;
    }

    public function getCuentasPrimerNivel()
    {
        return PlanCuenta::find()
            ->where(['padre_id' => null])
            ->all();
    }


    /**
     * @param bool $includeSelf Especifica si en la lista de ids, se va a incluir el padre.
     * @return array
     */
    public function getHijosIds($includeSelf = false)
    {
        $ids = [];
        $padres = [];
        $padres[] = $this;
        !$includeSelf || $ids[] = $this->id;

        // Este algoritmo aprovecha la existencia de la columna 'cod_completo' para lanzar un solo query y
        // obtener todos los hijos de una. Inclusive, se puede obtener de una vez ordenado por cod_completo.
        $start = microtime(true);
        $hijos = PlanCuenta::find()
            ->where(['LIKE', 'cod_completo', $this->cod_completo . '.%', false])
            ->orderBy(['cod_completo' => SORT_ASC])->all();
        foreach ($hijos as $hijo) {
            $ids[] = $hijo->id;
        }
        $time_elapsed_secs = microtime(true) - $start;
        $tiempo = number_format($time_elapsed_secs, 10, ',', '.');
//        FlashMessageHelpsers::createSuccessMessage("Tardó {$tiempo} segundos");

//        // Este algoritmo es generico, considerando que minimamente la tabla posee una columna de padre.
//        $ids = [];
//        $i = 0;
//        $start = microtime(true);
//        while ($i < sizeof($padres)) {
//            $padre = $padres[$i];
//            Yii::$app->session->set('debug', $padre);
//            $hijos = PlanCuenta::findAll(['padre_id' => $padre->id]);
//            foreach ($hijos as $hijo) {
//                $ids[] = $hijo->id;
//                $padres[] = $hijo;
//            }
//            $i++;
//            $size = sizeof($padres);
//        }
//        $time_elapsed_secs = microtime(true) - $start;
//        $tiempo = number_format($time_elapsed_secs, 10, ',', '.');
//        FlashMessageHelpsers::createSuccessMessage("Tardó {$tiempo} segundos");
        return $ids;
    }

    public static function getCtaGananciaRoundingError()
    {
        $nombre_base = "cuenta_error_redondeo_ganancia";
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-{$nombre_base}";
        $parm = ParametroSistema::findOne(['nombre' => $nombre]);
        $id = null;
        if (!isset($parm)) {
            $nombre = $nombre_base;
            $parm = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parm)) {
                $parm = \common\models\ParametroSistema::findOne(['nombre' => $nombre]);
                if (!isset($parm)) return null;
            }
        }
        $id = $parm->valor;
        return self::findOne(['id' => $id]);
    }

    public static function getCtaPerdidaRoundingError()
    {
        $nombre_base = "cuenta_error_redondeo_perdida";
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-{$nombre_base}";
        $parm = ParametroSistema::findOne(['nombre' => $nombre]);
        $id = null;
        if (!isset($parm)) {
            $nombre = $nombre_base;
            $parm = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parm)) {
                $parm = \common\models\ParametroSistema::findOne(['nombre' => $nombre]);
                if (!isset($parm)) return null;
            }
        }
        $id = $parm->valor;
        return self::findOne(['id' => $id]);
    }
}
