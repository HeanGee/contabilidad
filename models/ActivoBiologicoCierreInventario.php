<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;
use backend\modules\contabilidad\models\validators\ActivoBiologicoCierreInventarioValidator;
use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "cont_activo_biologico_cierre_inventario".
 *
 * @property int $id
 * @property string $fecha
 * @property int $especie_id
 * @property int $clasificacion_id
 * @property int $precio_foro Precio establecido arbitrariamente y aceptado consensuadamente entre los productores de ganado.
 * @property int $ajuste_inventario
 * @property int $recoluta
 * @property int $abigeato
 * @property int $faena
 * @property int $faena_gnd
 * @property int $procreo_cantidad Cantidad de nacimientos
 * @property int $venta_cantidad Cantidad vendida.
 * @property int $venta_cantidad_gd Cantidad vendida GD.
 * @property int $venta_cantidad_gnd Cantidad vendida GND.
 * @property int $compra_cantidad Cantidad comprada.
 * @property int $consumo_cantidad Cantidad consumida.
 * @property int $consumo_cantidad_gd Cantidad consumida GD.
 * @property int $consumo_cantidad_gnd Cantidad consumida GND.
 * @property int $mortandad_cantidad Cantidad de muertes
 * @property int $mortandad_cantidad_gd Cantidad de muertes GD
 * @property int $mortandad_cantidad_gnd Cantidad de muertes GND
 * @property int $consumo_porcentaje_gd
 * @property int $mortandad_porcentaje_gd
 * @property int $reclasificacion_entrada
 * @property int $reclasificacion_salida
 * @property int $stock_final
 * @property int $empresa_id
 * @property int $periodo_contable_id
 *
 * @property string $precioForoForView
 * @property string $procreoCantidadForView
 * @property string $ventaCantidadForView
 * @property string $consumoCantidadForView
 * @property string $mortandadCantidadForView
 * @property string $consumoPorcentajeGdForView
 * @property string $mortandadPorcentajeGdForView
 *
 * @property ActivoBiologicoEspecieClasificacion $clasificacion
 * @property Empresa $empresa
 * @property ActivoBiologicoEspecie $especie
 * @property EmpresaPeriodoContable $periodoContable
 * @property ActivoBiologico $activoBiologico
 */
class ActivoBiologicoCierreInventario extends \backend\models\BaseModel
{
//    public static $_MSG_NOT_CREABLE;
//    public static $_MSG_NOT_EDITABLE;
//    public static $_MSG_NOT_DELETEABLE;

    public $total_guaranies; // usado desde cierre de inventario;
    public $total_gd_guaranies; // usado desde cierre de inventario;
    public $total_gnd_guaranies; // usado desde cierre de inventario;
    public $faena_total; // para poder prorratear como consumo y mortandad pero no se va a guardar.

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_activo_biologico_cierre_inventario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'especie_id', 'clasificacion_id', 'precio_foro', 'empresa_id', 'periodo_contable_id'], 'required'],
            [['ajuste_inventario', 'recoluta', 'abigeato', 'faena', 'faena_gnd', 'procreo_cantidad', 'compra_cantidad',
                'venta_cantidad', 'venta_cantidad_gd', 'venta_cantidad_gnd', 'consumo_cantidad', 'consumo_cantidad_gd',
                'consumo_cantidad_gnd', 'mortandad_cantidad', 'mortandad_cantidad_gd', 'mortandad_cantidad_gnd',
                'consumo_porcentaje_gd', 'mortandad_porcentaje_gd', 'reclasificacion_entrada', 'reclasificacion_salida',
                'stock_final',], 'safe'],
            [['especie_id', 'clasificacion_id', 'empresa_id', 'periodo_contable_id'], 'integer'],
            [['fecha'], 'string'],
            [['clasificacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoBiologicoEspecieClasificacion::className(), 'targetAttribute' => ['clasificacion_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['especie_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoBiologicoEspecie::className(), 'targetAttribute' => ['especie_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],

            [['stock_final'], ActivoBiologicoCierreInventarioValidator::className()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => "Fecha",
            'especie_id' => 'Especie ID',
            'clasificacion_id' => 'Clasificacion ID',
            'precio_foro' => 'Precio Foro',
            'ajuste_inventario' => 'Ajuste Inventario',
            'recoluta' => 'Recoluta',
            'abigeato' => 'Abigeato',
            'faena' => 'Faena',
            'faena_gnd' => 'Faena GND',
            'procreo_cantidad' => 'Procreo Cantidad',
            'venta_cantidad' => 'Venta Cantidad',
            'venta_cantidad_gd' => 'Venta Cantidad GD',
            'venta_cantidad_gnd' => 'Venta Cantidad GND',
            'compra_cantidad' => 'Compra Cantidad',
            'consumo_cantidad' => 'Consumo Cantidad',
            'consumo_cantidad_gd' => 'Consumo Cantidad GD',
            'consumo_cantidad_gnd' => 'Consumo Cantidad GND',
            'mortandad_cantidad' => 'Mortandad Cantidad',
            'mortandad_cantidad_gd' => 'Mortandad Cantidad GD',
            'mortandad_cantidad_gnd' => 'Mortandad Cantidad GND',
            'consumo_porcentaje_gd' => 'Consumo %',
            'mortandad_porcentaje_gd' => 'Mortandad %',
            'reclasificacion_entrada' => 'Reclasificación Entrada',
            'reclasificacion_salida' => 'Reclasificación Salida',
            'stock_final' => 'Stock Final',
            'empresa_id' => 'Empresa ID',
            'periodo_contable_id' => 'Periodo Contable ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoBiologico()
    {
        return ActivoBiologico::find()->where([
            'especie_id' => $this->especie_id,
            'clasificacion_id' => $this->clasificacion_id,
            'empresa_id' => $this->empresa_id,
            'periodo_contable_id' => $this->periodo_contable_id,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasificacion()
    {
        return $this->hasOne(ActivoBiologicoEspecieClasificacion::className(), ['id' => 'clasificacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecie()
    {
        return $this->hasOne(ActivoBiologicoEspecie::className(), ['id' => 'especie_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    public function beforeValidate()
    {
        if (strpos($this->precio_foro, '.') && strlen(substr($this->precio_foro, (strpos($this->precio_foro, '.') + 1))) >= 3) {
            $this->precio_foro = str_replace('.', '', $this->precio_foro);
            $this->precio_foro = str_replace(',', '.', $this->precio_foro);
        } elseif (strpos($this->precio_foro, ',') == strlen($this->precio_foro) - 3)
            $this->precio_foro = str_replace(',', '.', $this->precio_foro);

        if (strpos($this->procreo_cantidad, '.') && strlen(substr($this->procreo_cantidad, (strpos($this->procreo_cantidad, '.') + 1))) >= 3) {
            $this->procreo_cantidad = str_replace('.', '', $this->procreo_cantidad);
            $this->procreo_cantidad = str_replace(',', '.', $this->procreo_cantidad);
        } elseif (strpos($this->procreo_cantidad, ',') == strlen($this->procreo_cantidad) - 3)
            $this->procreo_cantidad = str_replace(',', '.', $this->procreo_cantidad);
        if ($this->procreo_cantidad == '') $this->procreo_cantidad = 0;

        if (strpos($this->venta_cantidad, '.') && strlen(substr($this->venta_cantidad, (strpos($this->venta_cantidad, '.') + 1))) >= 3) {
            $this->venta_cantidad = str_replace('.', '', $this->venta_cantidad);
            $this->venta_cantidad = str_replace(',', '.', $this->venta_cantidad);
        } elseif (strpos($this->venta_cantidad, ',') == strlen($this->venta_cantidad) - 3)
            $this->venta_cantidad = str_replace(',', '.', $this->venta_cantidad);
        if ($this->venta_cantidad == '') $this->venta_cantidad = 0;

        if (strpos($this->venta_cantidad_gd, '.') && strlen(substr($this->venta_cantidad_gd, (strpos($this->venta_cantidad_gd, '.') + 1))) >= 3) {
            $this->venta_cantidad_gd = str_replace('.', '', $this->venta_cantidad_gd);
            $this->venta_cantidad_gd = str_replace(',', '.', $this->venta_cantidad_gd);
        } elseif (strpos($this->venta_cantidad_gd, ',') == strlen($this->venta_cantidad_gd) - 3)
            $this->venta_cantidad_gd = str_replace(',', '.', $this->venta_cantidad_gd);
        if ($this->venta_cantidad_gd == '') $this->venta_cantidad_gd = 0;

        if (strpos($this->venta_cantidad_gnd, '.') && strlen(substr($this->venta_cantidad_gnd, (strpos($this->venta_cantidad_gnd, '.') + 1))) >= 3) {
            $this->venta_cantidad_gnd = str_replace('.', '', $this->venta_cantidad_gnd);
            $this->venta_cantidad_gnd = str_replace(',', '.', $this->venta_cantidad_gnd);
        } elseif (strpos($this->venta_cantidad_gnd, ',') == strlen($this->venta_cantidad_gnd) - 3)
            $this->venta_cantidad_gnd = str_replace(',', '.', $this->venta_cantidad_gnd);
        if ($this->venta_cantidad_gnd == '') $this->venta_cantidad_gnd = 0;

        if (strpos($this->mortandad_cantidad, '.') && strlen(substr($this->mortandad_cantidad, (strpos($this->mortandad_cantidad, '.') + 1))) >= 3) {
            $this->mortandad_cantidad = str_replace('.', '', $this->mortandad_cantidad);
            $this->mortandad_cantidad = str_replace(',', '.', $this->mortandad_cantidad);
        } elseif (strpos($this->mortandad_cantidad, ',') == strlen($this->mortandad_cantidad) - 3)
            $this->mortandad_cantidad = str_replace(',', '.', $this->mortandad_cantidad);
        if ($this->mortandad_cantidad == '') $this->mortandad_cantidad = 0;

        if (strpos($this->mortandad_cantidad_gd, '.') && strlen(substr($this->mortandad_cantidad_gd, (strpos($this->mortandad_cantidad_gd, '.') + 1))) >= 3) {
            $this->mortandad_cantidad_gd = str_replace('.', '', $this->mortandad_cantidad_gd);
            $this->mortandad_cantidad_gd = str_replace(',', '.', $this->mortandad_cantidad_gd);
        } elseif (strpos($this->mortandad_cantidad_gd, ',') == strlen($this->mortandad_cantidad_gd) - 3)
            $this->mortandad_cantidad_gd = str_replace(',', '.', $this->mortandad_cantidad_gd);
        if ($this->mortandad_cantidad_gd == '') $this->mortandad_cantidad_gd = 0;

        if (strpos($this->mortandad_cantidad_gnd, '.') && strlen(substr($this->mortandad_cantidad_gnd, (strpos($this->mortandad_cantidad_gnd, '.') + 1))) >= 3) {
            $this->mortandad_cantidad_gnd = str_replace('.', '', $this->mortandad_cantidad_gnd);
            $this->mortandad_cantidad_gnd = str_replace(',', '.', $this->mortandad_cantidad_gnd);
        } elseif (strpos($this->mortandad_cantidad_gnd, ',') == strlen($this->mortandad_cantidad_gnd) - 3)
            $this->mortandad_cantidad_gnd = str_replace(',', '.', $this->mortandad_cantidad_gnd);
        if ($this->mortandad_cantidad_gnd == '') $this->mortandad_cantidad_gnd = 0;

        if (strpos($this->consumo_cantidad, '.') && strlen(substr($this->consumo_cantidad, (strpos($this->consumo_cantidad, '.') + 1))) >= 3) {
            $this->consumo_cantidad = str_replace('.', '', $this->consumo_cantidad);
            $this->consumo_cantidad = str_replace(',', '.', $this->consumo_cantidad);
        } elseif (strpos($this->consumo_cantidad, ',') == strlen($this->consumo_cantidad) - 3)
            $this->consumo_cantidad = str_replace(',', '.', $this->consumo_cantidad);
        if ($this->consumo_cantidad == '') $this->consumo_cantidad = 0;

        if (strpos($this->consumo_cantidad_gd, '.') && strlen(substr($this->consumo_cantidad_gd, (strpos($this->consumo_cantidad_gd, '.') + 1))) >= 3) {
            $this->consumo_cantidad_gd = str_replace('.', '', $this->consumo_cantidad_gd);
            $this->consumo_cantidad_gd = str_replace(',', '.', $this->consumo_cantidad_gd);
        } elseif (strpos($this->consumo_cantidad_gd, ',') == strlen($this->consumo_cantidad_gd) - 3)
            $this->consumo_cantidad_gd = str_replace(',', '.', $this->consumo_cantidad_gd);
        if ($this->consumo_cantidad_gd == '') $this->consumo_cantidad_gd = 0;

        if (strpos($this->consumo_cantidad_gnd, '.') && strlen(substr($this->consumo_cantidad_gnd, (strpos($this->consumo_cantidad_gnd, '.') + 1))) >= 3) {
            $this->consumo_cantidad_gnd = str_replace('.', '', $this->consumo_cantidad_gnd);
            $this->consumo_cantidad_gnd = str_replace(',', '.', $this->consumo_cantidad_gnd);
        } elseif (strpos($this->consumo_cantidad_gnd, ',') == strlen($this->consumo_cantidad_gnd) - 3)
            $this->consumo_cantidad_gnd = str_replace(',', '.', $this->consumo_cantidad_gnd);
        if ($this->consumo_cantidad_gnd == '') $this->consumo_cantidad_gnd = 0;

        if (strpos($this->consumo_porcentaje_gd, '.') && strlen(substr($this->consumo_porcentaje_gd, (strpos($this->consumo_porcentaje_gd, '.') + 1))) >= 3) {
            $this->consumo_porcentaje_gd = str_replace('.', '', $this->consumo_porcentaje_gd);
            $this->consumo_porcentaje_gd = str_replace(',', '.', $this->consumo_porcentaje_gd);
        } elseif (strpos($this->consumo_porcentaje_gd, ',') == strlen($this->consumo_porcentaje_gd) - 3)
            $this->consumo_porcentaje_gd = str_replace(',', '.', $this->consumo_porcentaje_gd);
        if ($this->consumo_porcentaje_gd == '') $this->consumo_porcentaje_gd = 0;

        if (strpos($this->mortandad_porcentaje_gd, '.') && strlen(substr($this->mortandad_porcentaje_gd, (strpos($this->mortandad_porcentaje_gd, '.') + 1))) >= 3) {
            $this->mortandad_porcentaje_gd = str_replace('.', '', $this->mortandad_porcentaje_gd);
            $this->mortandad_porcentaje_gd = str_replace(',', '.', $this->mortandad_porcentaje_gd);
        } elseif (strpos($this->mortandad_porcentaje_gd, ',') == strlen($this->mortandad_porcentaje_gd) - 3)
            $this->mortandad_porcentaje_gd = str_replace(',', '.', $this->mortandad_porcentaje_gd);
        if ($this->mortandad_porcentaje_gd == '') $this->mortandad_porcentaje_gd = 0;

        if (strpos($this->abigeato, '.') && strlen(substr($this->abigeato, (strpos($this->abigeato, '.') + 1))) >= 3) {
            $this->abigeato = str_replace('.', '', $this->abigeato);
            $this->abigeato = str_replace(',', '.', $this->abigeato);
        } elseif (strpos($this->abigeato, ',') == strlen($this->abigeato) - 3)
            $this->abigeato = str_replace(',', '.', $this->abigeato);
        if ($this->abigeato == '') $this->abigeato = 0;

        if (strpos($this->recoluta, '.') && strlen(substr($this->recoluta, (strpos($this->recoluta, '.') + 1))) >= 3) {
            $this->recoluta = str_replace('.', '', $this->recoluta);
            $this->recoluta = str_replace(',', '.', $this->recoluta);
        } elseif (strpos($this->recoluta, ',') == strlen($this->recoluta) - 3)
            $this->recoluta = str_replace(',', '.', $this->recoluta);
        if ($this->recoluta == '') $this->recoluta = 0;
        if ($this->abigeato == '') $this->abigeato = 0;

        if (strpos($this->faena, '.') && strlen(substr($this->faena, (strpos($this->faena, '.') + 1))) >= 3) {
            $this->faena = str_replace('.', '', $this->faena);
            $this->faena = str_replace(',', '.', $this->faena);
        } elseif (strpos($this->faena, ',') == strlen($this->faena) - 3)
            $this->faena = str_replace(',', '.', $this->faena);
        if ($this->faena == '') $this->faena = 0;

        if (strpos($this->faena_gnd, '.') && strlen(substr($this->faena_gnd, (strpos($this->faena_gnd, '.') + 1))) >= 3) {
            $this->faena_gnd = str_replace('.', '', $this->faena_gnd);
            $this->faena_gnd = str_replace(',', '.', $this->faena_gnd);
        } elseif (strpos($this->faena_gnd, ',') == strlen($this->faena_gnd) - 3)
            $this->faena_gnd = str_replace(',', '.', $this->faena_gnd);
        if ($this->faena_gnd == '') $this->faena_gnd = 0;

        if (strpos($this->reclasificacion_entrada, '.') && strlen(substr($this->reclasificacion_entrada, (strpos($this->reclasificacion_entrada, '.') + 1))) >= 3) {
            $this->reclasificacion_entrada = str_replace('.', '', $this->reclasificacion_entrada);
            $this->reclasificacion_entrada = str_replace(',', '.', $this->reclasificacion_entrada);
        } elseif (strpos($this->reclasificacion_entrada, ',') == strlen($this->reclasificacion_entrada) - 3)
            $this->reclasificacion_entrada = str_replace(',', '.', $this->reclasificacion_entrada);
        if ($this->reclasificacion_entrada == '') $this->reclasificacion_entrada = 0;

        if (strpos($this->reclasificacion_salida, '.') && strlen(substr($this->reclasificacion_salida, (strpos($this->reclasificacion_salida, '.') + 1))) >= 3) {
            $this->reclasificacion_salida = str_replace('.', '', $this->reclasificacion_salida);
            $this->reclasificacion_salida = str_replace(',', '.', $this->reclasificacion_salida);
        } elseif (strpos($this->reclasificacion_salida, ',') == strlen($this->reclasificacion_salida) - 3)
            $this->reclasificacion_salida = str_replace(',', '.', $this->reclasificacion_salida);
        if ($this->reclasificacion_salida == '') $this->reclasificacion_salida = 0;

        if (strpos($this->ajuste_inventario, '.') && strlen(substr($this->ajuste_inventario, (strpos($this->ajuste_inventario, '.') + 1))) >= 3) {
            $this->ajuste_inventario = str_replace('.', '', $this->ajuste_inventario);
            $this->ajuste_inventario = str_replace(',', '.', $this->ajuste_inventario);
        } elseif (strpos($this->ajuste_inventario, ',') == strlen($this->ajuste_inventario) - 3)
            $this->ajuste_inventario = str_replace(',', '.', $this->ajuste_inventario);
        if ($this->ajuste_inventario == '') $this->ajuste_inventario = 0;

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function loadDefaultValues($skipIfSet = true)
    {
//        $this->especie_id = 0;
//        $this->clasificacion_id = 0;
        $this->precio_foro = 0;
        $this->procreo_cantidad = 0;
        $this->compra_cantidad = 0;
        $this->venta_cantidad = 0;
        $this->venta_cantidad_gd = 0;
        $this->venta_cantidad_gnd = 0;
        $this->mortandad_cantidad = 0;
        $this->mortandad_cantidad_gd = 0;
        $this->mortandad_cantidad_gnd = 0;
        $this->consumo_cantidad = 0;
        $this->consumo_cantidad_gd = 0;
        $this->consumo_cantidad_gnd = 0;
        $this->fecha = date('d-m-Y');

        $this->empresa_id = Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

        return parent::loadDefaultValues($skipIfSet); // TODO: Change the autogenerated stub
    }

    /**
     *  Retorna TRUE si existe empresa y periodo actual. Lanza excepcion en caso contrario.
     *
     * @return bool
     * @throws Exception
     */
    public static function checkEmpresaPeriodoActual()
    {
        try {
            if (Yii::$app->session->has('core_empresa_actual') && Yii::$app->session->has('core_empresa_actual_pc'))
                return true;
            else {
                if (!Yii::$app->session->has('core_empresa_actual'))
                    throw new Exception("Falta especificar empresa actual.");
                else
                    throw new Exception("Falta especificar periodo actual.");
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     *  Retorna TRUE si la empresa actual esta definida.
     *
     * @return bool
     * @throws Exception
     */
    public static function checkEmpresaActual()
    {
        try {
            if (Yii::$app->session->has('core_empresa_actual'))
                return true;
            else {
                throw new Exception("Falta especificar empresa actual.");
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public static function isDeleteable()
    {
        return true;
    }

    public static function isEditable()
    {
        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public static function isCreable()
    {
        /** Solo se puede crear 1 cierre de inventario por periodo y empresa */
        $cierreInventario = self::find()->where([
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
        if ($cierreInventario->exists()) throw new Exception("Ya existe un registro de cierre para este periodo");

        /** Solo se puede crear si es que existe a.bio creados al menos 1 */
        $abio = ActivoBiologico::find()->where([
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
        if (!$abio->exists()) throw new Exception("No se ha registrado ningún A. Biológico todavía");

        return true;
    }

    /**
     *  Retorna todos los cierres de inventario de activos biologicos
     * del periodo contable y empresa actual.
     *
     * @return ActivoBiologicoCierreInventario[]
     * @throws Exception
     */
    public static function getInventarioCurrentPeriodo()
    {
        self::checkEmpresaPeriodoActual();

        return ActivoBiologicoCierreInventario::findAll([
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
    }

    /**
     *  Restablece el stock final del activo biologico
     * asociado a este cierre a stock_actual del mismo.
     *
     * @return ActivoBiologico
     */
    public function restoreABioStockFinal()
    {
        $abio = $this->activoBiologico;
        $abio->stock_final = $abio->stock_actual;
        return $abio;
    }

    /**
     *  Verifica si las cuentas necesarias para el asiento de cierre
     * de inventario de a.bio estan parametrizadas en la empresa actual.
     *
     * @throws Exception
     */
    public static function isCuentasForAsiento()
    {
        self::checkEmpresaPeriodoActual();

        $conceptos = ['cuenta_ganado', 'cuenta_costo_venta_gnd', 'cuenta_costo_venta_gd', 'cuenta_procreo',
            'cuenta_mortandad_gnd', 'cuenta_mortandad_gd', 'cuenta_consumo_gnd', 'cuenta_consumo_gd',
            'cuenta_valorizacion_hacienda'];
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombrePrefijo = "core_empresa-{$empresa_id}-";
        foreach ($conceptos as $concepto) {
            if (!ParametroSistema::find()->where(['nombre' => "{$nombrePrefijo}{$concepto}"])->exists()) {
                throw new Exception("Falta especificar en el Parámetro de la Empresa Actual las cuentas
                 contables para Asiento del Cierre de Inventario de Activos Biológicos. Las cuentas que se mostrarán en la simulación correspondiente serán establecidas aleatoriamente.");
            }
        }
    }

    /**
     *  Retorna la cuenta contable para ganados.
     *
     * @return PlanCuenta
     * @throws Exception
     */
    public static function getCuentaGanado()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_ganado";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    /**
     *  Retorna la cuenta contable para costo de venta gnd.
     *
     * @throws Exception
     */
    public static function getCuentaCostoVentaGnd()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_costo_venta_gnd";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    /**
     *  Retorna la cuenta contable para costo de venta gd.
     *
     * @throws Exception
     */
    public static function getCuentaCostoVentaGd()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_costo_venta_gd";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    /**
     *  Retorna la cuenta contable para procreo.
     *
     * @throws Exception
     */
    public static function getCuentaProcreo()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_procreo";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    /**
     *  Retorna la cuenta contable para mortandad gnd.
     *
     * @throws Exception
     */
    public static function getCuentaMortandadGnd()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_mortandad_gnd";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    /**
     *  Retorna la cuenta contable para mortandad gd.
     *
     * @throws Exception
     */
    public static function getCuentaMortandadGd()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_mortandad_gd";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    /**
     *  Retorna la cuenta contable para consumo gnd.
     *
     * @throws Exception
     */
    public static function getCuentaConsumoGnd()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_consumo_gnd";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    /**
     *  Retorna la cuenta contable para consumo gd.
     *
     * @throws Exception
     */
    public static function getCuentaConsumoGd()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_consumo_gd";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    /**
     *  Retorna la cuenta contable para valorizacion hacienda.
     *
     * @throws Exception
     */
    public static function getCuentaValorizacionHacienda()
    {
        self::checkEmpresaActual();
        self::isCuentasForAsiento();

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $nombre = "core_empresa-{$empresa_id}-cuenta_valorizacion_hacienda";
        return PlanCuenta::findOne(ParametroSistema::findOne(['nombre' => $nombre])->valor);
    }

    public function getPrecioForoForView()
    {

    }

    public function getProcreoCantidadForView()
    {

    }

    public function getVentaCantidadForView()
    {

    }

    public function getConsumoCantidadForView()
    {

    }

    public function getMortandadCantidadForView()
    {

    }

    public function getConsumoPorcentajeGdForView()
    {

    }

    public function getMortandadPorcentajeGdForView()
    {

    }
}
