<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use backend\modules\contabilidad\models\validators\NroTimbradoValidator;
use backend\modules\contabilidad\models\validators\TimbradoFechaValidator;
use backend\modules\contabilidad\models\validators\TimbradoVigenciaMinimaValidator;
use common\helpers\FlashMessageHelpsers;
use Exception;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cont_timbrado".
 *
 * @property int $id
 * @property int $nro_timbrado
 * @property int $entidad_id
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $nombre_entidad
 * @property string $ruc
 *
 * @property Entidad $entidad
 * @property TimbradoDetalle[] $timbradoDetalles
 * @property Compra[] $compras
 * @property Venta[] $ventas
 */
class Timbrado extends BaseModel
{
    public $entidad_tmp;
    public $ruc;
    public $nombre_entidad;

    const SCENARIO_RETENCION = 'retencion';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_RETENCION] = [
            'nro_timbrado',
        ];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_timbrado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entidad_id', 'ruc', 'nombre_entidad'], 'required'],
            [['entidad_id', 'nro_timbrado'], 'integer'],
            [['fecha_inicio', 'fecha_fin', 'entidad_tmp'], 'safe'],
            [['nro_timbrado'], 'unique'],
            [['entidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entidad::className(), 'targetAttribute' => ['entidad_id' => 'id']],
            ['nro_timbrado', NroTimbradoValidator::className()],
//            [['fecha_inicio'], TimbradoFechaValidator::className()],
            [['fecha_fin'], TimbradoFechaValidator::className()],
            [['nro_timbrado'], 'required', 'on' => self::SCENARIO_RETENCION],

            [['fecha_inicio', 'fecha_fin'], TimbradoVigenciaMinimaValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nro_timbrado' => 'Nro Timbrado',
            'entidad_id' => 'Entidad',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'entidad_tmp' => 'Entidad'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidad()
    {
        return $this->hasOne(Entidad::className(), ['id' => 'entidad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimbradoDetalles()
    {
        return $this->hasMany(TimbradoDetalle::className(), ['timbrado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        $td_ids = [];
        foreach ($this->timbradoDetalles as $timbradoDetalle) {
            $td_ids[] = $timbradoDetalle->id;
        }
        return Compra::find()->where(['IN', 'timbrado_detalle_id', $td_ids]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        $td_ids = [];
        foreach ($this->timbradoDetalles as $timbradoDetalle) {
            $td_ids[] = $timbradoDetalle->id;
        }
        return Venta::find()->where(['IN', 'timbrado_detalle_id', $td_ids]);
    }

    public function isTimbradoVirtual() {
        return $this->fecha_inicio != null && $this->fecha_fin == null;
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        foreach (['fecha_inicio', 'fecha_fin'] as $_fecha) {
            if (!empty($this->$_fecha) && $this->$_fecha != '') {
                $slices = explode('-', $this->$_fecha);
                $fecha = $this->$_fecha;
                if (strlen($slices[2]) > 2)
                    $fecha = implode('-', array_reverse($slices));
                $this->$_fecha = $fecha;
            } else {
                $this->$_fecha = null;
            }
        }

        return true;
    }

    /**
     * Retorna lista de prefijos para los formularios.
     */
    public static function getPrefsEmpActual()
    {
        /* select * from cont_timbrado_detalle as tide LEFT JOIN cont_timbrado as ti on tide.timbrado_id = ti.id ORDER BY tide.prefijo, tide.nro_inicio*/
        /** @var Timbrado $t */
        /** @var TimbradoDetalle[] $t_d */

        $hoy = date('Y-m-d');
        $tname = Timbrado::tableName() . '.';
        $concat = "CONCAT((MIN(detalles.id)), (' - '), (PREFIJO)) AS text";
        $razon_social = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->razon_social;
        $entidad = Entidad::findOne(['razon_social' => $razon_social]);
        $t_d = Timbrado::find()
            ->joinWith('timbradoDetalles as detalles')
            ->select([
                'COUNT(detalles.prefijo)',
                'MIN(detalles.id) AS id',
                'MIN(detalles.nro_actual)',
                'MIN(detalles.nro_fin)',
                'MIN(' . $tname . 'entidad_id)',
//                'MIN(detalles.prefijo) AS PREFIJO',
                $concat,
            ])
            ->where(['<=', $tname . 'fecha_inicio', $hoy])
            ->andWhere(['>=', $tname . 'fecha_fin', $hoy])
            ->andWhere(['=', $tname . 'entidad_id', $entidad->id])
            ->andWhere('detalles.nro_actual <= detalles.nro_fin')
            ->groupBy('detalles.id, detalles.prefijo')
            ->orderBy('detalles.id ASC')
            ->asArray()->all();

        if (sizeof($t_d) == 0)
            FlashMessageHelpsers::createWarningMessage('No tiene ningún timbrado vigente.');

//        return ArrayHelper::map($t_d, 'id', 'PREFIJO');
        return ArrayHelper::map($t_d, 'id', 'text');
    }

    public static function getPrefsEmpActualAjax($tipoDocId = null, $model_id = null, $action_id = null, $es_tipo_set = null, $fecha_factura = null, $vencido = '0')
    {
        /** SELECT * from cont_timbrado_detalle det where
         *
         * det.timbrado_id in (SELECT id from cont_timbrado tim where tim.entidad_id = 905582 and tim.fecha_fin <= '2018-09-04' AND (YEAR(tim.fecha_inicio) = '2018' OR YEAR(tim.fecha_fin) = '2018')) AND
         * det.tipo_documento_set_id = 1 AND
         * det.nro_actual <= det.nro_fin
         *
         */

        /** @var Timbrado $t */
        /** @var TimbradoDetalle[] $t_d */

        $resultado = [
            'results' => [
                [
                    'id' => '', 'text' => ''
                ]
            ]
        ];

        $tname = Timbrado::tableName() . '.';
        $ruc = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->ruc;
        $entidad = Entidad::findOne(['ruc' => $ruc]);

        if (!empty($es_tipo_set)) {
            $tipo_documento_set_id = $tipoDocId;
        } else {
            /** @var TipoDocumento $tipoDoc */
            $tipoDoc = TipoDocumento::find()->where(['id' => $tipoDocId])->one();
            $tipo_documento_set_id = isset($tipoDoc) ? $tipoDoc->tipo_documento_set_id : null;
        }

        $fecha_factura = ($fecha_factura != "") ? $fecha_factura : date('Y-m-d');
        if (strlen(($slices = explode('-', $fecha_factura))[2]) == 4)
            $fecha_factura = implode('-', array_reverse($slices));
        $periodo_anho = EmpresaPeriodoContable::findOne(['id' => Yii::$app->session->get('core_empresa_actual_pc')])->anho;

        $t_d = Timbrado::find()
            ->joinWith('timbradoDetalles as detalles')
            ->select([
                '(detalles.prefijo)',
                '(detalles.id) AS id',
                '(detalles.nro_actual)',
                '(detalles.nro_fin)',
                '(' . $tname . 'entidad_id)',
                'nro_timbrado',
//                "CONCAT((prefijo), (' ('), (nro_timbrado), (') ')) AS text",
                "CONCAT((prefijo), (' ('), (detalles.nro_actual), (')')) AS text",
            ])
            ->where(['=', $tname . 'entidad_id', $entidad->id])
            ->andFilterWhere(['=', 'detalles.tipo_documento_set_id', $tipo_documento_set_id])
            ->andWhere('detalles.nro_actual <= detalles.nro_fin');  # este es lo que diferencia entre lo que hay abajo y este

        // El cliente quizo para compra, poder traer timbrados vencidos segun check.
        // Jose recomendo incorporar lo mismo para venta, por si digan que van a querer y evitar tener que hacer despues.
        if ($vencido == '1') {
            $t_d->andWhere(['<', 'fecha_fin', $fecha_factura]);
        } else {
            $t_d->andWhere(['>=', $tname . 'fecha_fin', $fecha_factura])
                ->andWhere(['<=', $tname . 'fecha_inicio', $fecha_factura])
                ->andFilterWhere([
                    'or',
                    ['YEAR(' . $tname . 'fecha_inicio)' => $periodo_anho],
                    ['YEAR(' . $tname . 'fecha_fin)' => $periodo_anho]
                ]);
        }

        // Excluir detalle de timbrado para retenciones. Es necesario porque en la factura de venta hay casos en donde
        // no se asocia a ningun tipo de documento (por tanto a ningun tipo de doc set) pero sí o sí tiene prefijo y nro de
        // factura los cuales deben validarse contra algún timbrado por ej facturas anuladas y faltantes.
        $empresa = Yii::$app->session->get('core_empresa_actual');
        $periodo = Yii::$app->session->get('core_empresa_actual_pc');
        $nombreTipodocSetRet = "core_empresa-{$empresa}-periodo-{$periodo}-tipodoc_set_retencion";
        $parmsysTipoddcSet = ParametroSistema::find()->where(['nombre' => $nombreTipodocSetRet]);
        $tipodocSetRetId = null;
        if ($parmsysTipoddcSet->exists()) {
            $tipodocSetRetId = $parmsysTipoddcSet->one()->valor;
        }
        if (isset($tipodocSetRetId)) {
            $t_d->andWhere(['!=', 'detalles.tipo_documento_set_id', $tipodocSetRetId]);
        }
        // Fin excluir detalle timbrado para retenciones

        if ($action_id == 'update')
            $t_d->orWhere(['detalles.id' => Venta::findOne(['id' => $model_id])->timbrado_detalle_id]);

        $t_d = $t_d/*->groupBy('detalles.id, detalles.prefijo')*/
            ->orderBy('detalles.prefijo ASC, detalles.nro_actual ASC')
            ->asArray()->all();

        # Agregar numeros no usados. Posibles rangos o numeros que no se usaron.
        # TODO: Al modificarse algo en el codigo de arriba, actualizar tambien lo que esta aqui abajo.
        if (empty($t_d)) {
            $t_d = Timbrado::find()
                ->joinWith('timbradoDetalles as detalles')
                ->select([
                    '(detalles.prefijo)',
                    '(detalles.id) AS id',
                    '(detalles.nro_actual)',
                    '(detalles.nro_fin)',
                    'nro_timbrado',
                    '(' . $tname . 'entidad_id)',
                    "CONCAT((prefijo), (' ('), (detalles.nro_actual), (')')) AS text",
                ])
                ->where(['=', $tname . 'entidad_id', $entidad->id])
                ->andFilterWhere(['=', 'detalles.tipo_documento_set_id', $tipo_documento_set_id]);
//                ->andWhere('detalles.nro_actual <= detalles.nro_fin');

            // El cliente quizo para compra, poder traer timbrados vencidos segun check.
            // Jose recomendo incorporar lo mismo para venta, por si digan que van a querer y evitar tener que hacer despues.
            if ($vencido == '1') {
                $t_d->andWhere(['<', 'fecha_fin', $fecha_factura]);
            } else {
                $t_d->andWhere(['>=', $tname . 'fecha_fin', $fecha_factura])
                    ->andWhere(['<=', $tname . 'fecha_inicio', $fecha_factura])
                    ->andFilterWhere([
                        'or',
                        ['YEAR(' . $tname . 'fecha_inicio)' => $periodo_anho],
                        ['YEAR(' . $tname . 'fecha_fin)' => $periodo_anho]
                    ]);
            }

            // Excluir detalle de timbrado para retenciones. Es necesario porque en la factura de venta hay casos en donde
            // no se asocia a ningun tipo de documento (por tanto a ningun tipo de doc set) pero sí o sí tiene prefijo y nro de
            // factura los cuales deben validarse contra algún timbrado por ej facturas anuladas y faltantes.
            if ($parmsysTipoddcSet->exists()) {
                $tipodocSetRetId = $parmsysTipoddcSet->one()->valor;
            }
            if (isset($tipodocSetRetId)) {
                $t_d->andWhere(['!=', 'detalles.tipo_documento_set_id', $tipodocSetRetId]);
            }
            // Fin excluir detalle timbrado para retenciones

            if ($action_id == 'update')
                $t_d->orWhere(['detalles.id' => Venta::findOne(['id' => $model_id])->timbrado_detalle_id]);

            $t_d = $t_d->groupBy('detalles.id, detalles.prefijo')
                ->orderBy('detalles.prefijo ASC, detalles.nro_actual ASC')
                ->asArray()->all();

            foreach ($t_d as $index => $row) {
                $nrosUsadosVenta = (new Query())
                    ->select('nro_factura')
                    ->from('cont_factura_venta AS venta')
                    ->where(['empresa_id' => $empresa, 'periodo_contable_id' => $periodo, 'timbrado_detalle_id' => $row['id']])
                    ->andWhere(['IN', 'estado', ['vigente', 'anulada']])
                    ->all();

                $nros_usados = [];
                foreach ($nrosUsadosVenta as $nroUsadoVenta) {
                    $nros_usados[] = (int)($nroUsadoVenta['nro_factura']);
                }

                $timbradoDetalle = TimbradoDetalle::findOne($row['id']);
                $all_nros = range($timbradoDetalle->nro_inicio, $timbradoDetalle->nro_fin);

                $nros_disponibles = array_diff($all_nros, $nros_usados);
                if (empty($nros_disponibles))
                    unset($t_d[$index]);

                Yii::$app->session->set('nros_retornar', $t_d);
            }
        }

        $resultado['results'] = array_values($t_d);

        return $resultado;
    }

    public static function getTimbradosVigentesAjax($ruc, $fecha, $nro_factura, $tipo_documento_id, $vencidos)
    {
        $resultado = [
            'results' => [
                [
                    'id' => '', 'text' => ''
                ]
            ]
        ];

        $t_d = [];
        if ($ruc != '' && $fecha != '' && $nro_factura != '' && $tipo_documento_id != '') {
            $slices = explode('-', $fecha);
            $fecha = (strlen($slices[2]) > 2) ? implode('-', array_reverse($slices)) : $fecha;
            $tname = Timbrado::tableName() . '.';
            $entidad = Entidad::findOne(['ruc' => $ruc]);
            $nro = (int)substr($nro_factura, 8, 15);
            $prefijo = (int)$ptrfijo = substr($nro_factura, 0, 7);
            $tipoDoc = TipoDocumento::findOne($tipo_documento_id);

            $timbradoQuery = Timbrado::find()->alias('timbrado')->joinWith('timbradoDetalles as detalle');

	        if ($vencidos == '0' || $vencidos == '') {
                // Nullity control flags
                $case_timbradoFechasNotNulls = [
                    'and',
                    ['is not', 'timbrado.fecha_inicio', null],
                    ['is not', 'timbrado.fecha_fin', null]
                ];
                $case_timbradoOnlyFin = [
                    'and',
                    ['is', 'timbrado.fecha_inicio', null],
                    ['is not', 'timbrado.fecha_fin', null],
                ];
                $case_timbradoOnlyInicio = [
                    'and',
                    ['is not', 'timbrado.fecha_inicio', null],
                    ['is', 'timbrado.fecha_fin', null],
                ];

                // In-Range control flags
                $case_timbradoFechasInPerfectRange = [
                    'and',
                    ['<=', 'timbrado.fecha_inicio', $fecha],
                    ['>=', 'timbrado.fecha_fin', $fecha],
                ];
                $case_timbradoOnlyFinInRange =['>=', 'timbrado.fecha_fin', $fecha];
                $case_timbradoOnlyInicioInRange =['<=', 'timbrado.fecha_inicio', $fecha];

                $caseA_normalTimbrado = [
                    'and',
                    $case_timbradoFechasNotNulls,
                    $case_timbradoFechasInPerfectRange

                ];
                $case_timbradoVirtual = [
                    'and',
                    $case_timbradoOnlyInicio,
                    $case_timbradoOnlyInicioInRange
                ];

                $timbradoQuery->andWhere(
                    [
                        'or',
                        $case_timbradoVirtual,
                        $caseA_normalTimbrado
                    ]
                );
                Yii::warning("Timbrado no vencido");
            } else {
                $timbradoQuery->andWhere([
                    'AND',
                    ['IS NOT', 'timbrado.fecha_fin', null],
                    ['<', 'timbrado.fecha_fin', $fecha]
                ]);
                Yii::warning("Timbrado vencido");
            }

            // Excluir detalle de timbrado para retenciones. Es necesario porque en la factura de compra hay casos en donde
            // no se asocia a ningun tipo de documento set (existen tipos de doc no asociada a ningun tipodoc set) pero
            // sí o sí tiene un numero de factura el cual debe validarse contra algun timbrado.
            $empresa = Yii::$app->session->get('core_empresa_actual');
            $periodo = Yii::$app->session->get('core_empresa_actual_pc');
            $nombreTipodocSetRet = "core_empresa-{$empresa}-periodo-{$periodo}-tipodoc_set_retencion";
            $parmsysTipoddcSet = ParametroSistema::find()->where(['nombre' => $nombreTipodocSetRet]);
            $tipodocSetRetId = null;
            if ($parmsysTipoddcSet->exists()) {
                $tipodocSetRetId = $parmsysTipoddcSet->one()->valor;
            }
            if (isset($tipodocSetRetId)) {
                $timbradoQuery->andWhere([
                    'OR',
                    ['IS', 'detalle.timbrado_id', null], #timbrados sin detalles, ahora se incluyen
                    ['AND',
                        ['IS NOT', 'detalle.tipo_documento_set_id', null],
                        ['!=', 'detalle.tipo_documento_set_id', $tipodocSetRetId]
                    ],
                ]);
            }
            // Fin excluir detalle timbrado para retenciones

            $timbradoQuery->andWhere([
                'OR',
                ['IS', 'detalle.timbrado_id', null], #timbrados sin detalles, ahora se incluyen
                ['AND',
                    ['IS NOT', 'detalle.tipo_documento_set_id', null],
                    ['=', 'detalle.tipo_documento_set_id', $tipoDoc->tipo_documento_set_id]
                ],
            ]);

            /** @var Timbrado[] $timbrados */
            $timbrados = $timbradoQuery->andWhere(['timbrado.entidad_id' => $entidad->id])->all();
            foreach ($timbrados as $timbrado) {
                $vencido = 'no';
                if ($timbrado->fecha_fin != '' and $timbrado->fecha_fin < $fecha)
                    $vencido = 'si';
                $element = ['id' => $timbrado->id, 'text' => $timbrado->nro_timbrado, 'vencido' => $vencido];
                $t_d[] = $element;
            }

            $resultado['results'] = array_values($t_d);
        }

        return $resultado;
    }

    /**
     *  Recibe un nro de documento en formato 999-999-9999999 y devuelve el timbrado detalle que le corresponde.
     * Devuelve null en caso de que no exista.
     *
     * @param $nro_completo
     * @return TimbradoDetalle|null
     */
    public function findDetailThatCanHoldThisNumber($nro_completo, $tipodocSet = null)
    {
        foreach ($this->timbradoDetalles as $timbradoDetalle) {
            Yii::warning("timbrado id: {$timbradoDetalle->timbrado_id}, detalle id: {$timbradoDetalle->id}, nro inicion fin: {$timbradoDetalle->nro_inicio} {$timbradoDetalle->nro_fin}");
            if ($timbradoDetalle->canHoldThisNumber($nro_completo, $tipodocSet))
                return $timbradoDetalle;
        }
        return null;
    }

//    public static function isVigente($nro_timbrado, $fecha)
//    {
//        $t = Timbrado::find()->where(['nro_timbrad']);
//    }

    /**
     * [2019-06-10] Comunicado de Jose: Magali quiere que se unifique todo.
     *
     * @throws Exception
     * @throws \Throwable
     */
    public function unificarTalonarios()
    {
        $rangeByTDS_P = [];  # rango de nros por tipo de documento set y por prefijo.
        $alreadyUnified = true;
        $tDets = $this->timbradoDetalles;

        # Obtener el min y el max por tipo de documento set, del timbrado actual.
        foreach ($tDets as $tDet) {
            if (!array_key_exists($tDet->tipo_documento_set_id, $rangeByTDS_P))
                $rangeByTDS_P[$tDet->tipo_documento_set_id] = [$tDet->prefijo => ['nro_inicio' => $tDet->nro_inicio, 'nro_fin' => $tDet->nro_fin, 'alreadyUnified' => $alreadyUnified]];
            else {
                if (!array_key_exists($tDet->prefijo, $rangeByTDS_P[$tDet->tipo_documento_set_id])) {
                    $rangeByTDS_P[$tDet->tipo_documento_set_id][$tDet->prefijo] = ['nro_inicio' => $tDet->nro_inicio, 'nro_fin' => $tDet->nro_fin, 'alreadyUnified' => $alreadyUnified];
                } else {
                    $alreadyUnified = false;  # bandera: si hay mas de un detalle con los mismos tipos de documento-set, aun no se unifico.
                    $range = $rangeByTDS_P[$tDet->tipo_documento_set_id][$tDet->prefijo];
                    if ($tDet->nro_inicio < $range['nro_inicio'])
                        $rangeByTDS_P[$tDet->tipo_documento_set_id][$tDet->prefijo]['nro_inicio'] = $tDet->nro_inicio;
                    if ($tDet->nro_fin > $range['nro_fin'])
                        $rangeByTDS_P[$tDet->tipo_documento_set_id][$tDet->prefijo]['nro_fin'] = $tDet->nro_fin;
                    $rangeByTDS_P[$tDet->tipo_documento_set_id][$tDet->prefijo]['alreadyUnified'] = $alreadyUnified;
                }
            }
        }

//        echo sizeof($tDets) . '<br/>';
//        echo print_r($rangeByTDS_P). '<br/>';
//        exit();

        # Retornar si no hay nada que unificar.
        if ($alreadyUnified) return;

        # Unificar.
        # Crear nuevos detalles y actualizar las referencias a los viejos detalles por referencia al nuevo detalle, en compra/venta/retencion.
        # Si hay solamente un detalle de timbrado por el tipo de documento-set, skip.
        foreach ($rangeByTDS_P as $tipoDocSetId => $rangeByPrefix) {
            foreach ($rangeByPrefix as $prefix => $range) {
                if ($range['alreadyUnified']) continue;
                Yii::warning('a');

                $newDetalle = new TimbradoDetalle();
                $newDetalle->timbrado_id = $this->id;
                $newDetalle->tipo_documento_set_id = $tipoDocSetId;
                $newDetalle->prefijo = $prefix;
                $newDetalle->nro_inicio = $range['nro_inicio'];
                $newDetalle->nro_fin = $range['nro_fin'];
                $newDetalle->nro_actual = $newDetalle->nro_inicio;

                if (!$newDetalle->save(false))  # no validar porque sino va a quejarse por las colisiones.
                    throw new Exception("Error al guardar el nuevo detalle unificado: {$newDetalle->getErrorSummaryAsString()}");
                $newDetalle->refresh();

                foreach ($tDets as $key => $tDet) {
                    if ($tDet->tipo_documento_set_id == $tipoDocSetId && $tDet->prefijo == $prefix) {
                        # Actualizar ventas.
                        $models = Venta::findAll(['timbrado_detalle_id' => $tDet->id]);
                        foreach ($models as $venta) {
                            $venta->timbrado_detalle_id = $newDetalle->id;
                            if (!$venta->save(false)) {
                                throw new Exception("Error actualizando venta: {$venta->getErrorSummaryAsString()}");
                            }
                        }

                        # Actualizar compras.
                        $models = Compra::findAll(['timbrado_detalle_id' => $tDet->id]);
                        foreach ($models as $compra) {
                            $compra->timbrado_detalle_id = $newDetalle->id;
                            if (!$compra->save(false)) {
                                throw new Exception("Error actualizando venta: {$compra->getErrorSummaryAsString()}");
                            }
                        }

                        # Actualizar retenciones.
                        $models = Retencion::findAll(['timbrado_detalle_id' => $tDet->id]);
                        foreach ($models as $retencion) {
                            $retencion->timbrado_detalle_id = $newDetalle->id;
                            if (!$retencion->save(false)) {
                                throw new Exception("Error actualizando venta: {$retencion->getErrorSummaryAsString()}");
                            }
                        }

                        # Borrar el detalle de timbrado.
                        if (!$tDet->delete())
                            throw new Exception("Error borrando timbrado detalle (id = '{$tDet->id}'): {$tDet->getErrorSummaryAsString()}");
                        $tDet->refresh();
                    }
                }
            }
        }
    }
}
