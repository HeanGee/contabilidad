<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\ActivoFijo;
use yii\helpers\Url;
use yii\validators\Validator;

class ActivoFijoCantidadValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var $model ActivoFijo */

        $action = \Yii::$app->controller->action->id;

        if ($action == 'manejar-desde-factura-compra') {
            if (isset($model->activoFijoTipo) && $model->activoFijoTipo->cantidad_requerida == 'si' && $model->$attribute == '') {
                $model->addError($attribute, "{$model->getAttributeLabel($attribute)} no puede estar vacio.");
            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $action = \Yii::$app->controller->action->id;
        $url = Url::to(['/contabilidad/activo-fijo/cantidad-deferred-validator']);
        return <<<JS
if ("$action" === 'manejar-desde-factura-compra') {
    deferred.push(
        $.get("$url", { // se envia asi porque en la action no se usa
            activo_fijo_tipo_id: $("#activofijo-activo_fijo_tipo_id").val(),
            cantidad: $("#activofijo-cantidad").val(), // variable del POS_HEAD
        }).done(
            function (error_msg) {
                if (error_msg) {
                    messages.push(error_msg);
                }
            }
        )
    );
}
JS;
    }
}
