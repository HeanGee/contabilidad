<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Prestamo;
use yii\validators\Validator;

class PrestamoProformaCasesValidator extends Validator
{
    /**
     * @param Prestamo $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        /** @var Prestamo $model */
        $usar_iva_10 = ($attribute == 'usar_iva_10' ? $model->$attribute : $model->usar_iva_10);
        $tiene_factu = ($attribute == 'tiene_factura' ? $model->$attribute : $model->tiene_factura);

        if ($usar_iva_10 == 'si' && $tiene_factu == 'por_cuota') {
            $msg = "No es una opción válida: Si {$model->getAttributeLabel('usar_iva_10')} es Sí, {$model->getAttributeLabel('tiene_factura')} tiene que ser No o Por prestamo.";
            $this->addError($model, 'tiene_factura', $msg);
        }

        if ($usar_iva_10 == 'no' && $tiene_factu == 'por_prestamo') {
            $msg = "No es una opción válida: Si {$model->getAttributeLabel('usar_iva_10')} es No, {$model->getAttributeLabel('tiene_factura')} tiene que ser No o Por cuota.";
            $this->addError($model, 'tiene_factura', $msg);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $msg1 = "No es una opción válida: Si {$model->getAttributeLabel('usar_iva_10')} es Sí, {$model->getAttributeLabel('tiene_factura')} tiene que ser No o Por prestamo.";
        $msg2 = "No es una opción válida: Si {$model->getAttributeLabel('usar_iva_10')} es No, {$model->getAttributeLabel('tiene_factura')} tiene que ser No o Por cuota.";
        return <<<JS
let tiene_factura = $('#prestamo-tiene_factura').val();
let usar_iva_diez = $('#prestamo-usar_iva_10').val();

if (usar_iva_diez === 'si' && tiene_factura === 'por_cuota') {
    messages.push("$msg1");
}

if (usar_iva_diez === 'no' && tiene_factura === 'por_prestamo') {
    messages.push("$msg2");
}
JS;
    }
}
