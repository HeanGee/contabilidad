<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\helpers\ParametroSistemaHelpers;
use backend\modules\contabilidad\models\Compra;
use yii\helpers\Url;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class NroFacturaValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var Compra $model */
        $value = $model->$attribute;
        $sinTimbrado = $model->tipoDocumento->tipo_documento_set_id == ParametroSistemaHelpers::getValorByNombre('tipo_documento_set_ninguno');
        if (!$sinTimbrado && strlen(str_replace('_', '', $value)) < 15) {
            $model->addError($attribute, 'Nro de factura inválido');
        } elseif ($value == '')
            $model->addError($attribute, "{$model->getAttributeLabel($attribute)} no puede ser vacío.");

        $compraMismoNro = Compra::find()
            ->where(['!=', 'id', $model->id])
            ->andWhere(['timbrado_detalle_id' => $model->timbrado_detalle_id, 'nro_factura' => $model->nro_factura]);
        if ($compraMismoNro->exists())
            $model->addError('nro_factura', "Ya existe una factura con el mismo número para el mismo proveedor (id: {$compraMismoNro->one()->id}");
    }

    /**
     * El nro de factura debe cumplir con la longitud definida en el formato "001-001-1234567"
     *
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param \yii\web\View $view
     * @return string
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode('Nro de factura inválido', JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $message_vacio = json_encode('Nro de factura es requerido', JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $url = Url::toRoute(['compra/nro-factura-validator']);
        $modelId = $model->id;

        return <<<JS
        let nro_factura_field = $('#compra-nro_factura');
        if (nro_factura_field.val() === ''){
            messages.push($message_vacio);
        } else {
            // No efectua ninguna validacion si es sin timbrado
            if (typeof $('#compra-tipo_documento_id').select2('data') !== "undefined" && $('#compra-tipo_documento_id').select2('data')[0]['tipodocsetnombre'] === 'NINGUNO')
                return false;
            
            if (nro_factura_field.val().split("_").join("").length < 15) {
                messages.push($message);
            }else{ //Verificar nro duplicado de factura
                deferred.push(
                    $.get("$url", {
                        nro_factura: nro_factura_field.val(),
                        ruc: $('#compra-ruc').val(),
                        timbrado_id: $('#compra-timbrado_id').val(),
                        modelId: "$modelId",
                        tipo: !esNota ? 'factura' : $('#compra-tipo').val() 
                    }).done(
                        function (data) {
                            if (data.error) {
                                messages.push(data.error);
                            }
                        }
                    )
                );
            }
        }
JS;
    }
}
