<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Prestamo;
use yii\validators\Validator;

class PrestamoFechaVencValidator extends Validator
{
    public static $_MSG = "Vencimiento debe ser mayor al 1er. Venc.";

    public function validateAttribute($model, $attribute)
    {
        /** @var $model Prestamo */

        if ($model->primer_vencimiento >= $model->vencimiento)
            $this->addError($model, $attribute, self::$_MSG);
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $msg = self::$_MSG;

        return <<<JS
        let p_venc = $('#prestamo-primer_vencimiento');
        if (value <= p_venc.val()) {
            messages.push("$msg");
        }
JS;
    }
}
