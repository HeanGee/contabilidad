<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Prestamo;
use yii\validators\Validator;

class PrestamoPriVencValidator extends Validator
{
    public static $_MSG = "1er. Venc. debe ser menor al Vencimiento";

    public function validateAttribute($model, $attribute)
    {
        /** @var $model Prestamo */

        if ($model->primer_vencimiento >= $model->vencimiento)
            $this->addError($model, $attribute, self::$_MSG);
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $msg = self::$_MSG;

        return <<<JS
        let venc = $('#prestamo-vencimiento');
        if (value >= venc.val()) {
            messages.push("$msg");
        }
JS;
    }
}
