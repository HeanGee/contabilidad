<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Compra;
use yii\validators\Validator;

/**
 * Class CompraTimbradoValidator
 * @package backend\modules\contabilidad\models\validators
 */
class CompraNotaFechaValidator extends Validator
{
    /**
     * @param Compra $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (isset($model->facturaCompra)) {
            \Yii::warning('validando fecha de nota');
            $fechaNota = $model->fecha_emision;
            $fechaFactura = $model->facturaCompra->fecha_emision;

            // unificar formato: dd-mm-YYYY
            $slices = explode('-', $fechaNota);
            if (strlen($slices[2]) == 4)
                $fechaNota = implode('-', array_reverse($slices));

            $slices = explode('-', $fechaFactura);
            if (strlen($slices[2]) == 4)
                $fechaFactura = implode('-', array_reverse($slices));

            if ($fechaNota < $fechaFactura) {
                \Yii::warning("nota: $fechaNota y factura: $fechaFactura");
                $msg = "{$model->getAttributeLabel('fecha_emision')} debe ser igual o mayor a la fecha de emision de la factura.";
                $model->addError('fecha_emision', $msg);
            }
        }
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param \yii\web\View $view
     * @return string
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $msg = "{$model->getAttributeLabel('fecha_emision')} debe ser igual o mayor a la Fecha Emisión de la factura.";
        return <<<JS
let fechaNota = $('#compra-fecha_emision');
let fechaFact = $('#compra-emision_factura');


let fecha_factura = ((fechaFact.val().split('-')).reverse()).join('-');
let fecha_nota = ((fechaNota.val().split('-')).reverse()).join('-');

console.log('debe ser solo para nota', fecha_factura, fecha_nota, fecha_factura <= fecha_nota);

if (fecha_nota < fecha_factura)
    messages.push("{$msg}");
JS;
    }
}
