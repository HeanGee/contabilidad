<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Venta;
use yii\validators\Validator;

class VentaFechaVencimientoValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var Venta $model */

        if ($model->condicion == 'credito') {
            if ($model->fecha_vencimiento != '' && !strtotime($model->$attribute)) {
                $this->addError($model, $attribute, "Error de Formato.");
            }

            if ($model->fecha_emision == $model->fecha_vencimiento)
                $this->addError($model, 'fecha_vencimiento', 'Fecha de vencimiento no puede ser igual a la fecha de emision.');
            elseif ($model->fecha_emision > $model->fecha_vencimiento)
                $this->addError($model, 'fecha_vencimiento', 'Fecha de vencimiento no puede ser menor a la fecha de emision.');
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        return <<<JS
if ($('#venta-condicion').val() === 'credito') {
    
    let vencimiento = $('#venta-fecha_vencimiento');
    
    if (vencimiento.val() !== "") {
        let element = $("#venta-$attribute");
    
        let fecha = element.val().replace(new RegExp('-', 'g'), '/'); // reemplazar '-' por '/'
        fecha = ((fecha.split('/')).reverse()).join('/'); // invertir dia y anho de lugares.
    
        let date = new Date(fecha); // crear objeto Date.
    
        if (isNaN(date)) { // isNaN() verifica si se creo bien el objeto.
            messages.push("Error de Formatox.");
        }
    }
    
    let emision = $('#venta-fecha_emision');
    
    if (vencimiento.val() === emision.val())
        messages.push("No puede ser igual a la fecha de emision.");
    let venc = ((vencimiento.val().split('-')).reverse()).join('-');
    let emis = ((emision.val().split('-')).reverse()).join('-');
    if (venc < emis) {
        messages.push("No puede ser menor a la fecha de emision.");
        console.log('client validation de fecha de venci', 'venci: ', venc, 'emision: ', emis);
    }
}
JS;
    }
}
