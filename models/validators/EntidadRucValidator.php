<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Entidad;
use yii\validators\Validator;

class EntidadRucValidator extends Validator
{
    const MSG_RUC_NOVALIDO = "EL R.U.C. sólo debe contener números";
    const MSG_RUC_EXTRANJERO = "El R.U.C debe terminar con una (1) letra en mayúscula. Ej: 1234567A";

    /**
     * @param Entidad $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if ($model->extranjero == 'no') {
            $ruc = $model->$attribute;
            $tiene_letras = preg_match('/[A-Za-z]+/', $ruc); // verifica si tiene letras.
            if ($tiene_letras) {
                $this->addError($model, 'ruc', self::MSG_RUC_NOVALIDO);
            }
        } else {
            $ruc = $model->$attribute;
            $formato_correcto = preg_match('/^\d+[A-Z]$/', $ruc); // numeros seguido de una letra en mayuscula
            if (!$formato_correcto) {
                $this->addError($model, 'ruc', self::MSG_RUC_EXTRANJERO);
            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message_ruc_novalido = json_encode(self::MSG_RUC_NOVALIDO, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $message_ruc_extranjero = json_encode(self::MSG_RUC_EXTRANJERO, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return <<<JS
let ruc = value,
    solo_nros = (/^\d+$/).test(ruc), // verifica si es solo numeros.
    extranjero = $('#entidad-extranjero').val() === 'si';

if (!extranjero && !solo_nros) {
    messages.push($message_ruc_novalido);
} else if (extranjero) {
    let formato_correcto = (/^\d+[A-Z]$/).test(ruc); // numeros seguido de una letra en mayuscula
    if (!formato_correcto) {
        messages.push($message_ruc_extranjero);
    }    
}
JS;
    }
}