<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Timbrado;
use yii\validators\Validator;

class TimbradoVigenciaMinimaValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if ($model->getScenario() == Timbrado::SCENARIO_RETENCION) return;
        /** @var Timbrado $model */
        if ($model->fecha_inicio != '' and $model->fecha_fin != '' and $model->id == '') {  #Magali pidio que se haga este control solamente por aquellos nuevos timbrados que se crean.
            $fechaFinMin = date('t-m-Y', strtotime($model->fecha_inicio . '+3 months'));
            $fechaFinMin = implode('-', array_reverse(explode('-', $fechaFinMin)));
            if (implode('-', array_reverse(explode('-', $model->fecha_fin))) < $fechaFinMin)
                $model->addError('fecha_fin', "La mínima fecha fin debe ser {$fechaFinMin}");
        }
    }

//    public function clientValidateAttribute($model, $attribute, $view)
//    {
//        return <<<JS
//if ($('#timbrado2-fecha_inicio').val() !== '') {
//
//}
//JS;
//    }
}
