<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\Venta;
use yii\validators\Validator;

class FechaValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var Venta */

        $periodo_contable_actual = EmpresaPeriodoContable::find()->where(['id' => \Yii::$app->session->get('core_empresa_actual_pc')])->one()->anho;
        $date = \DateTime::createFromFormat("Y-m-d", $model->$attribute);

        if (($date) && ($date instanceof \DateTime) && ($date->format('Y') != $periodo_contable_actual) && ($attribute == 'fecha_emision')) {
            $this->addError($model, $attribute, "La fecha {$model->$attribute} no corresponde al periodo contable actual");
        }

        if (!strtotime($model->$attribute)) {
            $this->addError($model, $attribute, "Error de Formato.");
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        return <<<JS
let element = $("#venta-$attribute");

if (element.length === 0)
    element = $("#compra-$attribute");

let fecha = element.val().replace(new RegExp('-', 'g'), '/'); // reemplazar '-' por '/'
fecha = ((fecha.split('/')).reverse()).join('/'); // invertir dia y anho de lugares.

let date = new Date(fecha); // crear objeto Date.

if ("$attribute" === 'fecha_emision') {
    if (isNaN(date)) { // isNaN() verifica si se creo bien el objeto.
        messages.push("Error de Formato.");
    }
} else if ("$attribute" === 'fecha_vencimiento') {
    if ($('#venta-condicion').val() === 'credito') {
        if (isNaN(date)) { // isNaN() verifica si se creo bien el objeto.
            messages.push("Error de Formato.");
        }
    }
}

JS;
    }
}
