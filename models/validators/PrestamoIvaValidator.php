<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Prestamo;
use yii\helpers\Url;
use yii\validators\Validator;

class PrestamoIvaValidator extends Validator
{
    /** El nro del recibo no debe repetir bajo mismo proveedor/cliente..
     *
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        /** @var Prestamo $model */
        if ($model->usar_iva_10 == 'si' && ($model->iva_10 == "" || (int)$model->iva_10 <= 0)) {
            $msg = "";
            if ($attribute == 'usar_iva_10')
                $msg = "No olvide de establecer un valor para el campo Iva 10%";
            else
                $msg = "El campo {$model->getAttributeLabel('iva_10')} no puede ser menor a 1.";
            $model->addError($attribute, $msg);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = Url::toRoute(['recibo/recibo-numero-validator']);
        return <<<JS
        let campo_iva10 = $('#prestamo-iva_10');
if ($('#prestamo-usar_iva_10').val() === 'si' && (campo_iva10.val() === "" || parseFloat(campo_iva10.val()) <= 0.0)) {
    let msg = '';
    if ("$attribute" === 'usar_iva_10')
        msg = "No olvide de establecer un valor para el campo Iva 10%";
    else
        msg = "El campo {$model->getAttributeLabel('iva_10')} no puede ser menor a 1.";
    messages.push(msg);
}
JS;
    }
}
