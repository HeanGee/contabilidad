<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\auxiliar\TimbradoDetalle2;
use backend\modules\contabilidad\models\Timbrado;
use yii\validators\Validator;

class TimbradoDetalle2Validator extends Validator
{
    public $skipOnEmpty = false;
    public $skipOnError = false;

    public function validateAttribute($model, $attribute)
    {
        /** @var TimbradoDetalle2 $model */

        if (!isset($model->timbrado_id)) return;
        if (!isset($model->timbrado)) return;
        if (!isset($model->timbrado->entidad_id)) return;
        if (!isset($model->timbrado->nro_timbrado)) return;

        $query = Timbrado::find()->alias('timbrado')
            ->leftJoin('cont_timbrado_detalle detalle', 'timbrado.id = detalle.timbrado_id')
            ->where(['!=', 'timbrado.id', $model->timbrado_id])
            ->andWhere(['=', 'timbrado.entidad_id', $model->timbrado->entidad_id])
            ->andWhere(['=', 'timbrado.fecha_inicio', $model->timbrado->fecha_inicio])
            ->andWhere(['=', 'timbrado.fecha_fin', $model->timbrado->fecha_fin])
            ->andWhere(['=', 'detalle.prefijo', $model->prefijo])
            ->andWhere(['=', 'detalle.tipo_documento_set_id', $model->tipo_documento_set_id]);

        if ($query->exists())
            $model->addError('prefijo', "Existe otro timbrado con el mismo prefijo y fechas de vigencia.");
    }
}
