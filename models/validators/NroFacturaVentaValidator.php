<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Venta;
use yii\helpers\Url;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class NroFacturaVentaValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var Venta $model */
        $nro_completo = "{$model->prefijo}-{$model->nro_factura}";
        $concat = "CONCAT((prefijo), ('-'), (nro_factura))";
        $venta = Venta::find()->where([$concat => $nro_completo])
            ->andWhere(['timbrado_detalle_id' => $model->timbrado_detalle_id_prefijo])
            ->andWhere(['!=', 'estado', 'faltante'])
            ->andFilterWhere(['!=', 'id', $model->id]);
        if ($venta->exists()) {
            $venta = $venta->one();
            $model->addError('nro_factura', "Este Nro de Factura ya fue utilizada por la empresa de razón social {$venta->empresa->razon_social}");
        }
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param \yii\web\View $view
     * @return null|string
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = Url::toRoute(['venta/check-nro-factura',]);

        return <<<JS
let select2Prefijo = $('#venta-timbrado_detalle_id_prefijo');
let hayOpciones = select2Prefijo.select2('data').length !== 0;
let prefijo = hayOpciones ? select2Prefijo[0].options[select2Prefijo[0].selectedIndex].text.substr(0, 7) : "";
let nro_factura = $('#venta-nro_factura').val();
if (nro_factura !== '')
    deferred.push(
        $.get("$url", {
            nro: nro_factura,
            prefijo: prefijo,
            timbrado_detalle_id: select2Prefijo.val(),
            action_id: action_id, // action_id del lado derecho es una variable del _form2_js declarada en POS_HEAD.
            idToExclude: "$model->id"
        }).done(
            function (data) {
                if (data.error) {
                    messages.push(data.error);
                }
            }
        )
    );
JS;
    }
}