<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\ActivoFijoAtributo;
use backend\modules\contabilidad\models\ActivoFijoTipoAtributo;
use yii\validators\Validator;

class ActivoFijoAtributoValidator extends Validator
{
    /**
     * @param ActivoFijoAtributo $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
//        \Yii::warning('validando valor');
        if (isset($model->activoFijo)) {
            /** @var ActivoFijoTipoAtributo $atributo */
            $valor = $model->$attribute;
            $atributo = $model->activoFijo->activoFijoTipo->getAtributos()->where(['atributo' => $model->atributo])->one();
            if ($atributo->obligatorio == 'si' && ($valor == '' || $valor == ' '))
                $this->addError($model, 'valor', "El valor del atributo `{$model->atributo}` es obligatorio.");
        }
    }

//    public function clientValidateAttribute($model, $attribute, $view)
//    {
//        $url = Url::to(['validate-valor']);
//        return <<<JS
//deferred.push(
//    $.get("$url", {
//        tipo_id: $('#activofijo-activo_fijo_tipo_id').val(),
//        atributo: $(':input[id$="atributo"]').val(),
//        valor: $('input[id$="valor"]').val(),
//    }).done(
//        function (data) {
//            if (data.error) {
//                messages.push(data.error);
//            }
//        }
//    )
//);
//JS;
//    }
}
