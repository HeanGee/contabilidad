<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Venta;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class RequiredValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var Venta $model */
        if (!in_array($model->estado, ['anulada', 'faltante'])) {
            // Si es total, tener su propia validacion.
            // Se desea permitir valor cero al total de factura para casos de descuentos del 100%.
            // Se va a seguir exigiendo escribir un monto correspondiente a la plantilla pero en el detalle de la factura se va a ajustar con la cuenta del descuento.
            // Entonces, si la factura es de monto cero por no especificar monto de alguna plantilla, eso es un error.
            // Si habiendo usado una plantilla con un monto > 0 y aun asi el total de factura da cero, eso es permitido.
            if ($attribute == 'total') {
                if ((int)$model->$attribute < 0) {
                    $model->addError($attribute, "Total de factura de venta no puede ser negativo.");
                }
            } elseif ($model->$attribute == null) {
                $model->addError($attribute, $model->getAttributeLabel($attribute) . ' no puede estar vacío.');
            }
        } elseif ($model->estado != 'faltante') {
            if ($model->fecha_emision == null) {
                $model->addError('fecha_emision', $model->getAttributeLabel('fecha_emision') . ' no puede estar vacío.');
            }
            // Segun reunion del 14 setiembre y lo dicho por jose despues de esa reunion,
            //  una anulada es exactamente igual a faltante excepto que fecha emision es requerida.

//            if ($model->tipo_documento_id == null) {
//                $model->addError('tipo_documento_id', $model->getAttributeLabel('tipo_documento_id') . ' no puede estar vacío.');
//            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        switch ($attribute) {
            case 'timbrado_detalle_id_prefijo':
                return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    if ($('#venta-timbrado_detalle_id_prefijo').val() === '') {
        messages.push('No puede estar vacío.');
    }
}
JS;

# Miercoles 19 junio 2019: Magali "solicita/se pone de acuerdo en" que el tdoc sea siempre requerido porque otros tdos pueden tambien ser anulados, por ej, nota de credito.
//            case 'tipo_documento_id':
//                return <<<JS
//if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
//    if ($('#venta-tipo_documento_id').val() === '') {
//        messages.push('No puede estar vacío.');
//    }
//}
//JS;
            case 'entidad_id':
                return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    if ($('#venta-entidad_id').val() === '') {
        messages.push('No puede estar vacío.');
    }
}
JS;
            case 'ruc':
                return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    if ($('#venta-ruc').val() === '') {
        messages.push('No puede estar vacío.');
    }
}
JS;
            case 'moneda_id':
                return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    if ($('#venta-moneda_id').val() === '') {
        messages.push('No puede estar vacío.');
    }
}
JS;
            case 'fecha_emision':
                return <<<JS
let venta_estado = $('#venta-estado').val();
if (venta_estado !== 'anulada') {
    if ($('#venta-fecha_emision').val() === '')
        messages.push('No puede estar vacío.');        
}
JS;
            case 'total':
                return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    if ($('#venta-total-disp').val() === '') {
        messages.push('No puede estar vacío.');
    }
}
JS;
            case 'cotizacion_propia':
                return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    if ($('#venta-cotizacion_propia').val() === '') {
        messages.push('No puede estar vacío.');
    }
}
JS;
            case 'obligacion_id':
                return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    if ($('#venta-obligacion_id').val() === '') {
        messages.push('No puede estar vacío.');
    }
}
JS;
            case 'para_iva':
                return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    if ($('#venta-para_iva').val() === '') {
        messages.push('No puede estar vacío.');
    }
}
JS;

            default:
                return false;
        }

    }
}