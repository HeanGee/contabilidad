<?php

namespace backend\modules\contabilidad\models\validators;

use yii\validators\Validator;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\helpers\ParametroSistemaHelpers;

/**
 * Class CompraTimbradoValidator
 * @package backend\modules\contabilidad\models\validators
 */
class CompraTimbradoValidator extends Validator
{
    /**
     * @param Compra $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
	    if (isset($model->tipoDocumento) && !$model->tipoDocumento->isTipoSetNinguno() && $model->timbrado_detalle_id == '') {
		    $model->addError('timbrado_id', 'Timbrado es un campo requerido.');
        }
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param \yii\web\View $view
     * @return string
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode('Timbrado es un campo requerido.', JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $tipo_documento_set_ninguno_id = ParametroSistemaHelpers::getValorByNombre('tipo_documento_set_ninguno');
        $controllerId = \Yii::$app->controller->id; // si no es compra, no hay select2 de tipo de documento.

        return <<<JS
        if ("{$controllerId}" === 'compra' && $('#compra-tipo_documento_id').select2('data')[0]['tipo_documento_set_id'] != $tipo_documento_set_ninguno_id && $('#compra-timbrado_id').val() === ''){
            messages.push($message);
        }
        
        // si es nota, el tipo de documento set es siempre distinto a ninguno porque son uno de los dos: nota_credito o nota_debito.
        if ("{$controllerId}" === 'compra-nota' && $('#compra-timbrado_id').val() === '')
             messages.push($message);
JS;
    }
}
