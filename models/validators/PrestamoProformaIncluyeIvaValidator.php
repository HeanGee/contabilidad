<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Prestamo;
use yii\validators\Validator;

class PrestamoProformaIncluyeIvaValidator extends Validator
{
    /**
     * @param Prestamo $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        /** @var Prestamo $model */
        $usar_iva_10 = $model->usar_iva_10;
        $tiene_factu = $model->tiene_factura;
        $incluye_iva = $model->$attribute;

        if ($usar_iva_10 == 'no' && $tiene_factu == 'no' && $incluye_iva == 'si') {
            $this->addError($model, $attribute, "No es una opción válida: Si {$model->getAttributeLabel('usar_iva_10')} y {$model->getAttributeLabel('tiene_factura')} son 'No', {$model->getAttributeLabel('incluye_iva')} debe ser 'No'");
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $msg1 = "No es una opción válida: Si {$model->getAttributeLabel('usar_iva_10')} y {$model->getAttributeLabel('tiene_factura')} son 'No', {$model->getAttributeLabel('incluye_iva')} debe ser 'No'";
        return <<<JS
let tiene_factura = $('#prestamo-tiene_factura').val();
let usar_iva_diez = $('#prestamo-usar_iva_10').val();
let incluye_iva = $('#prestamo-incluye_iva').val();

if (usar_iva_diez === 'no' && tiene_factura === 'no' && incluye_iva === 'si') {
    messages.push("$msg1");
}
JS;
    }
}
