<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\ReciboPrestamo;
use yii\validators\Validator;

class ReciboPrestamoTotalValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var ReciboPrestamo */

        $total = $model->$attribute;
        if ($total == null || $total == "" || $total == 0)
            $this->addError($model, $attribute, 'Total debe ser mayor a cero');
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        return <<<JS
let val = $('#reciboprestamo-total').val();
if(val === 0 || val === '0' || val === "") {
    messages.push('Total debe ser mayor a cero.');
}
JS;
    }
}
