<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\PrestamoDetalle;
use yii\validators\Validator;

class PrestamoDetalleFechaValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var PrestamoDetalle */

        if (!strtotime($model->$attribute)) {
            $this->addError($model, $attribute, "Error de Formato: {$model->$attribute}");
        }
    }

//    public function clientValidateAttribute($model, $attribute, $view)
//    {
//        return <<<JS
//let element = $("#PrestamoDetalle_new18_vencimiento");
//
//let fecha = element.val().replace(new RegExp('-', 'g'), '/'); // reemplazar '-' por '/'
//fecha = ((fecha.split('/')).reverse()).join('/'); // invertir dia y anho de lugares.
//
//let date = new Date(fecha); // crear objeto Date.
//
//if (isNaN(date)) { // isNaN() verifica si se creo bien el objeto.
//    messages.push("Error de Formato.");
//}
//
//JS;
//    }
}
