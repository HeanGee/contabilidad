<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\DetalleCompra;
use yii\validators\Validator;

class CompraDetalleSubtotalValidator extends Validator
{

    public function validateAttribute($model, $attribute)
    {
        /** @var DetalleCompra $model */

        $attribute = "subtotal";
        if ($model->$attribute <= 0)
            $model->addError($attribute, "{$model->getAttributeLabel($attribute)} debe ser mayor a cero.");
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $msg = "{$model->getAttributeLabel($attribute)} debe ser mayor a cero.";

        return <<<JS
        let valor = $('#detallecompra-$attribute').val();
        valor = parseFloat(valor);
        
        if (valor <= 0)
            messages.push("$msg");
JS;
    }
}
