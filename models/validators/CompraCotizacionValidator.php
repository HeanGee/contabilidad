<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\helpers\ParametroSistemaHelpers;
use backend\modules\contabilidad\models\Compra;
use common\models\ParametroSistema;
use yii\helpers\Url;
use yii\validators\Validator;

/**
 * Class CompraTimbradoValidator
 * @package backend\modules\contabilidad\models\validators
 */
class CompraCotizacionValidator extends Validator
{
    /**
     * @param Compra $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if ($model->moneda_id != ParametroSistema::getMonedaBaseId() && $model->condicion == "")
            $model->addError($attribute, "{$model->getAttributeLabel($attribute)} no puede estar vacío si la moneda es extrangera.");
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param \yii\web\View $view
     * @return string
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $monedaBaseId = ParametroSistema::getMonedaBaseId();
        $msg = "{$model->getAttributeLabel($attribute)} no puede estar vacío si la moneda es extrangera.";
        return <<<JS
let select2Moneda = $('#compra-moneda_id');
let fieldCotizac = $('#compra-cotizacion');
if (select2Moneda.val() !== "{$monedaBaseId}" && fieldCotizac.val() === '')
    messages.push("{$msg}");
JS;
    }
}
