<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\TimbradoDetalle;
use yii\validators\Validator;

class NumberValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var TimbradoDetalle $model */
        if ((($attribute == 'nro_inicio' && $model->$attribute > $model->nro_fin) || ($attribute == 'nro_fin' && $model->$attribute < $model->nro_inicio)) &&
            ($model->nro_inicio != null && $model->nro_fin != null))
            $this->addError($model, $attribute, 'Número Inicio no puede ser mayor que Número Fin.');
    }
}
