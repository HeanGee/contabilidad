<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\ReciboDetalle;
use backend\modules\contabilidad\models\Venta;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class ReciboDetalleMontoValidator extends Validator
{
    /**
     * @param ReciboDetalle $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $msg = '';
        $monto = $model->$attribute;
        $factura_id = $model->factura_venta_id != null ? $model->factura_venta_id :
            $model->factura_compra_id != null ? $model->factura_compra_id : "";
        if ($factura_id != "") {
            $factura = $model->factura_venta_id != null ?
                Venta::findOne(['id' => $factura_id]) :
                Compra::findOne(['id' => $factura_id]);
            $saldo = $factura->saldo;
            $msg = "";
            if ($monto > $saldo) {
                if (strlen($msg) == 0)
                    $msg .= 'El monto no puede ser mayor al saldo de la factura seleccionada.';
                else
                    $msg .= ' y ' . 'El monto no puede ser mayor al saldo de la factura seleccionada.';
            }

            retorno:;
            if (strlen($msg) > 0)
                $model->addError($attribute, $msg);
        }
    }
}