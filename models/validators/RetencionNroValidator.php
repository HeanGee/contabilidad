<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Retencion;
use yii\helpers\Url;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class RetencionNroValidator extends Validator
{
    /**
     * @param Retencion $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $error = [];

        $result = Retencion::checkNroRetencion($model->$attribute, $model->id);
        if ($result['error']) {
            $model->addError($result['attribute'], $result['error']);
        }

//        $query = Retencion::find()->where([
//            'nro_retencion' => $model->$attribute,
//            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
//            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
//        ])->andFilterWhere(['!=', 'id', $model->id]);
//
//        if ($query->exists()) {
//            $model->addError($attribute, "Nro de Retención {$model->nro_retencion} ya fue utilizado.");
//        }
//
//        if (strlen(str_replace('_', '', $model->nro_retencion)) <15)
//            $model->addError($attribute, "Error en el formato del número de retención.");

//        $operacion = \Yii::$app->request->getQueryParam('operacion');
//
//        if ($operacion == 'venta') {
//
//            $factura_id = $model->factura_id;
//            $nro_retencion = $model->nro_retencion;
//            $action_id = \Yii::$app->controller->action->id;
//            $retencion_id = $model->id;
//
//            if ($factura_id != "" && $nro_retencion != "") {
//
//                $prefijo = substr($nro_retencion, 0, 7);
//                $siete_digitos = substr($nro_retencion, 8, strlen($nro_retencion));
//                if (strlen(str_replace('_', '', $siete_digitos)) < 7) {
//                    $error[] = 'Formato no válido para Nro de retención';
//                }
//                $siete_digitos = (int)$siete_digitos;
//
//                $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-tipodoc_set_retencion";
//                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
//
//                if ($parametro_sistema == null) {
//                    $error[] = 'Falta definir el tipo de documento para retenciones en el parámetro del sistema.';
//                }
//
//                $tipodoc_set_retencion = TipoDocumentoSet::findOne(['id' => $parametro_sistema->valor]);
//
//                if ($tipodoc_set_retencion == null) {
//                    $error[] = 'Falta definir el tipo de documento set para retenciones.';
//                }
//
//                $entidad = null;
//                if ($operacion == 'compra') {
//                    $ruc = Empresa::findOne(['id' => \Yii::$app->session->get('core_empresa_actual')])->ruc;
//                    $entidad = Entidad::findOne(['ruc' => $ruc]);
//                } else {
//                    $venta = Venta::findOne(['id' => $factura_id]);
//                    $entidad = $venta->entidad;
//                }
//
//                $timbrado_detalles = TimbradoDetalle::find()->alias('det')
//                    ->leftJoin('cont_timbrado AS tim', 'det.timbrado_id = tim.id')
//                    ->where(['tim.entidad_id' => $entidad->id])
//                    ->andWhere(['=', 'det.tipo_documento_set_id', $tipodoc_set_retencion->id])
//                    ->andWhere(['=', 'det.prefijo', $prefijo])
//                    ->andWhere(['<=', 'det.nro_inicio', $siete_digitos])
//                    ->andWhere(['>=', 'det.nro_fin', $siete_digitos])->all();
//
//                if (sizeof($timbrado_detalles) == 0) {
//                    $error[] = 'Este número no pertenece a ningún timbrado.';
//                }
//
//                $retencion = Retencion::find()->where(['nro_retencion' => $nro_retencion]);
//
//                if ($action_id != 'create' || isset($model->id)) {
//                    $retencion->andWhere(['!=', 'id', $retencion_id]);
//                }
//
//                if ($retencion->exists()) {
//                    $error[] = "Este número ya fue utilizado por la retención (id = {$retencion->one()->id}).";
//                }
//            }
//        }
//
//        retorno:;
//        if (!empty($error))
//            $model->addError($attribute, implode(', ', $error));
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = Url::toRoute(['retencion/check-nro-retencion']);
        $operacion = \Yii::$app->request->getQueryParam('operacion');
        $action_id = \Yii::$app->controller->action->id;
        return <<<JS
let factura_id = 
deferred.push(
    $.get("$url", {
        nro_retencion: $('#retencion-nro_retencion').val(),
        factura_id: $("#retencion-factura_id").val(),
        operacion: "$operacion",
        action_id: "$action_id",
        retencion_id: "$model->id",
    }).done(
        function (data) {
            if (data.error) {
                messages.push(data.error);
            }
        }
    )
);
JS;
    }
}