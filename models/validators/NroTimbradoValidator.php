<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Timbrado;
use yii\validators\Validator;

/**
 * Class NroTimbradoValidator
 * @package backend\modules\contabilidad\models\validators
 */
class NroTimbradoValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if ($model->$attribute == null) {
            $model->addError($attribute, 'Nro de timbrado es requerido');
        } elseif ($model->scenario == Timbrado::SCENARIO_DEFAULT) {
            $longitud = strlen((string)$model->$attribute);
            if ($longitud != 8) {
                $model->addError($attribute, 'Nro de timbrado debe tener 8 dígitos');
            }
        }
    }

    /**
     * El nro de timbrado debe tener exactamente 8 dígitos.
     *
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param \yii\web\View $view
     * @return string
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode('Nro de timbrado debe tener 8 dígitos', JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $message_vacio = json_encode('Nro de timbrado es requerido', JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return <<<JS
        var nro_timbrado_field = $('#timbrado-nro_timbrado');
        if (nro_timbrado_field.val() === ''){
            messages.push($message_vacio);
        } else {
            if (nro_timbrado_field.val().length !== 8) {
                messages.push($message);
            }
        }
JS;
    }
}
