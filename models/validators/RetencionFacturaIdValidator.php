<?php

namespace backend\modules\contabilidad\models\validators;

use yii\helpers\Url;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class RetencionFacturaIdValidator extends Validator
{
    private $msg = 'Factura no puede estar vacio.';

    public function validateAttribute($model, $attribute)
    {
        $factura_id = $model->$attribute;
        if ($factura_id == "" || $factura_id == null)
            $model->addError($attribute, $this->msg);
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = Url::toRoute(['venta/check-ruc']);
        return <<<JS
if ($('#retencion-factura_id').val() === "") {
    messages.push("Factura o puede estar vacio.");
}
JS;
    }
}