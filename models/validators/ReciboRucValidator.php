<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Entidad;
use yii\helpers\Url;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class ReciboRucValidator extends Validator
{
    private $msg = 'Este R.U.C. no corresponde a ninguna entidad registrada.';

    public function validateAttribute($model, $attribute)
    {
        $ruc = $model->$attribute;
        $entidad = Entidad::findOne(['ruc' => $ruc]);
        if (!$entidad) {
            $model->addError($attribute, $this->msg);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = Url::toRoute(['recibo/check-ruc']);
        $msg = $this->msg;
        return <<<JS
deferred.push(
        $.get("$url" + '&operacion=' + '', { // se envia asi porque en la action no se usa
            ruc: $('#recibo-ruc').val()
        }).done(
            function (data) {
                if (data.length === 0) {
                    messages.push("$msg");
                }
            }
        )
    );
JS;
    }
}