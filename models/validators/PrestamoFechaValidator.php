<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Prestamo;
use yii\validators\Validator;

class PrestamoFechaValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var Prestamo */

        if (!strtotime($model->$attribute)) {
            $this->addError($model, $attribute, "Error de Formato.");
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        return <<<JS
if (isNaN(new Date(((value.replace(new RegExp('-', 'g'), '/')).split('/')).reverse().join('/')))) { // isNaN() verifica si se creo bien el objeto.
    messages.push("Error de Formato.");
}
JS;
    }
}
