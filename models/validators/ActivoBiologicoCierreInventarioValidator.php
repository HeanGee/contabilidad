<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use yii\validators\Validator;

class ActivoBiologicoCierreInventarioValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var $model ActivoBiologicoCierreInventario */

        if ($attribute == 'stock_final') {
            if ((int)$model->$attribute < 0) {
                $model->addError($attribute, "Stock Final no puede ser negativo.");
            } /*elseif ((int)$model->$attribute == "") {
                $model->addError($attribute, "Stock Final no puede ser vacío."); // default es cero
            }*/
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        return <<<JS

JS;
    }
}
