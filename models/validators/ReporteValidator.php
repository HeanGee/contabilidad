<?php

namespace backend\modules\contabilidad\models\validators;

use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class ReporteValidator extends Validator
{
    public $skipOnError = false;
    public $skipOnEmpty = false;

    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        $criterioList = ['criterio1', 'criterio2', 'criterio3'];
        $criterioFechaList = ['criterio1fecha', 'criterio2fecha', 'criterio3fecha'];

        if (in_array($attribute, $criterioList)) {  #validador de campos de criterio.
            $countEmpty = 0;
            $fieldSet = $criterioList;

            if ($model->$attribute != '') {
                unset($fieldSet[array_search($attribute, $fieldSet)]);
                $values = [];
                foreach ($fieldSet as $field) {
                    $values[] = $model->$field;
                }
                if (!empty($values) and in_array($value, $values))
                    $model->addError($attribute, "Uno o más criterios tienen el mismo valor.");
            } else {
                foreach ($fieldSet as $field) {
                    if ($model->$field == '')
                        $countEmpty++;
                }

                if ($countEmpty == sizeof($fieldSet)) {
                    $model->addError($attribute, 'Se debe elegir al menos un criterio.');
                }
            }
        } elseif (in_array($attribute, $criterioFechaList)) {  #validador de campos de fecha para criterios.
            $queryAttr = str_replace('fecha', '', $attribute);
            if (in_array($model->$queryAttr, ['factura.creado', /*'factura.fecha_emision'*/]) and $value == '')  #se requiere campo de fecha si el criterio es `creado`
                $model->addError($attribute, "{$model->getAttributeLabel($attribute)} no puede estar vacío.");
        } elseif ($attribute == 'separar_visualmente') {
            $criterioValues = [];
            foreach ($criterioList as $criterio) {
                $criterioValues[] = $model->$criterio;
            }

            $valueUpCase = strtoupper(str_replace('_', ' ', $value));
            #Se elige agrupar por prefijo pero no hay ningun criterio para ordenar por NUMERO DE FACTURA, error
            if (in_array($value, ['prefijo',]) and !in_array('factura.nro_factura', $criterioValues))
                $model->addError($attribute, "`{$valueUpCase} sólo es válido si existe al menos un criterio `NUMERO DE FACTURA`");

            #Se elige agrupar por cuenta principal pero no hay ningun criterio para ordenar por CUENTA PRINCIPAL, error
            if (in_array($value, ['cuenta_principal',]) and !in_array('cta.nombre', $criterioValues))
                $model->addError($attribute, "{$valueUpCase} sólo es válido si existe al menos un criterio `CUENTA PRINCIPAL`");
        }
    }

//    public function clientValidateAttribute($model, $attribute, $view)
//    {
//
//    }
}