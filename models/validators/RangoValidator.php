<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\TimbradoDetalle;
use yii\helpers\Url;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class RangoValidator extends Validator
{
    /**
     * @param TimbradoDetalle $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if ($model->$attribute != null) {
            $detallesExistentes = TimbradoDetalle::find()->where([
                'timbrado_id' => $model->timbrado_id,
                'prefijo' => $model->prefijo,
                'tipo_documento_set_id' => $model->tipo_documento_set_id,
            ]);

            if ($model->id != null) {//En caso de update no se debe validar contra sí misma
                $detallesExistentes->andWhere(['<>', 'cont_timbrado_detalle.id', $model->id]);
            }

            // Verificar si los rangos no se solapan para un mismo prefijo
            if (($model->nro_inicio == null && $model->nro_fin != null) || ($model->nro_inicio != null && $model->nro_fin == null)) {
                $nro = $model->nro_inicio == null ? $model->nro_fin : $model->nro_inicio;
                if ($detallesExistentes->exists()) {
                    /** @var TimbradoDetalle $_detalle */
                    foreach ($detallesExistentes->all() as $_detalle) {
                        if ($nro >= $_detalle->nro_inicio && $nro <= $_detalle->nro_fin && !($_detalle->nro_inicio == $_detalle->nro_fin)) {
                            $model->addError($attribute, 'Se solapa con uno de los detalles.');
                            break;
                        }
                    }
                }
            }

            if ($model->nro_inicio != null && $model->nro_fin != null) {
                // Verificar si los rangos no se solapan para un mismo prefijo
                if ($detallesExistentes->exists()) {
                    /** @var TimbradoDetalle $_detalle */
                    foreach ($detallesExistentes->all() as $_detalle) {
                        if ((($model->nro_inicio >= $_detalle->nro_inicio && $model->nro_inicio <= $_detalle->nro_fin) ||
                                ($model->nro_fin >= $_detalle->nro_inicio && $model->nro_fin <= $_detalle->nro_fin)) &&
                            !($_detalle->nro_inicio == $_detalle->nro_fin)) {
                            $model->addError('nro_inicio', 'El rango se solapa con uno de los detalles.');
                            $model->addError('nro_fin', 'El rango se solapa con uno de los detalles.');
                            break;
                        }

                        if ((($model->nro_inicio == $_detalle->nro_inicio && $model->nro_inicio == $_detalle->nro_fin) ||
                                ($model->nro_fin == $_detalle->nro_inicio && $model->nro_fin == $_detalle->nro_fin)) &&
                            ($_detalle->nro_inicio == $_detalle->nro_fin)) {
                            $model->addError('nro_inicio', 'El rango se solapa con uno de los detalles.');
                            $model->addError('nro_fin', 'El rango se solapa con uno de los detalles.');
                            break;
                        }
                    }
                }

                $query_2 = TimbradoDetalle::find()
                    ->joinWith('timbrado as timbrado')
                    ->where([
                        'timbrado_id' => $model->timbrado_id,
                        'prefijo' => $model->prefijo,
                        'tipo_documento_set_id' => $model->tipo_documento_set_id,
                    ])
                    ->andWhere(['>', 'nro_inicio', $model->nro_inicio])
                    ->andWhere(['<', 'nro_fin', $model->nro_fin]);
                if ($model->id != 'false') {//En caso de update no se debe validar contra sí misma
                    $query_2->andWhere(['<>', 'cont_timbrado_detalle.id', $model->id]);
                }
                if ($query_2->exists()) {
                    /** @var TimbradoDetalle $_detalle */
                    foreach ($query_2->all() as $_detalle) {
                        if (!($_detalle->nro_inicio == $_detalle->nro_fin)) {
                            $model->addError('nro_inicio', 'El rango se solapa con uno de los detalles.');
                            $model->addError('nro_fin', 'El rango se solapa con uno de los detalles.');
                            break;
                        }
                    }
                }
            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = Url::toRoute(['timbrado/range-validator']);
        $model_id_string = $model->id == null ? false : $model->id;
        return <<<JS
        var nro_fin = '';
        var nro_inicio = '';
        var prefijo = '';
        if (typeof $('#timbradodetalle2-nro_fin').val() === 'undefined'){
            nro_fin = $('#timbradodetalle-nro_fin').val();
            nro_inicio = $('#timbradodetalle-nro_inicio').val();
            prefijo = $('#timbradodetalle-prefijo').val();
        } else{
            nro_fin = $('#timbradodetalle2-nro_fin').val();
            nro_inicio = $('#timbradodetalle2-nro_inicio').val();
            prefijo = $('#timbradodetalle2-prefijo').val();
        }
        nro_inicio = nro_inicio === '' ? false : nro_inicio;
        nro_fin = nro_fin === '' ? false : nro_fin;
        var nro_timbrado = '';
        if (typeof $('#timbrado2-nro_timbrado').val() === 'undefined'){
            nro_timbrado = $('#timbrado-nro_timbrado').val()
        } else {
            nro_timbrado = $('#timbrado2-nro_timbrado').val();
        }
deferred.push(
    $.get("$url", {
        nro_inicio: nro_inicio,
        nro_fin: nro_fin,
        nro_timbrado: nro_timbrado,
        prefijo: prefijo,
        model_id: "$model_id_string"
    }).done(
        function (data) {
            if (data.error) {
                messages.push(data.error);
            }
        }
    )
);
JS;
    }
}