<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 *
 * Esta clase se utiliza para establecer validadores de los atributos deseados, mediante switch(){}.
 * Recordar NO UTILIZAR break, si desea que se ejecuten todos los validadores.
 *
 * @package backend\modules\contabilidad\models\validators
 */
class PlantillaDetalleValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var PlantillaCompraventaDetalle $model */

        $tipo = \Yii::$app->request->getQueryParam('tipo');
        switch ($attribute) {
            case 'p_c_gravada_id':
                break; // se hace break como para no proceder: se quiere permitir crear plantillas con cuentas ya usadas en otras plantillas segun Magali en la reunion presencial del 22 de enero.
                if (!isset($model->plantilla_id) ||
                    (isset($model->plantilla_id) &&
                        !$model->plantilla->esParaPrestamo() &&
                        !$model->plantilla->esParaCuotaPrestamo() &&
                        !$model->plantilla->esParaActivoBiologico() &&
                        !$model->plantilla->esParaActivoFijo() &&
                        !$model->plantilla->esParaImportacion())) {

                    $query = PlantillaCompraventa::find()->alias('plantilla');
                    $query->leftJoin('cont_plantilla_compraventa_detalle  AS detalle', 'detalle.plantilla_id = plantilla.id');

                    // Permitir usar misma cuenta en otra plantilla si tipo_asiento es diferente.
                    // Ej.: La Cuenta 'IMPUESTOS, PATENTES, TASAS Y OTRAS CONTRIBUCIONES' deberia poder usarse en una plantilla
                    //   de venta como en una de compra.
                    //      Si el usuario pone en una plantilla de compra la cuenta 'VENTAS DE MERCADERIAS GRAVADAS 10%'
                    //      eso es su culpa.
                    $query->where(['detalle.tipo_asiento' => $tipo, 'detalle.p_c_gravada_id' => $model->$attribute, 'estado' => 'activo']);
                    $query->andWhere(['!=', 'para_prestamo', 'si'])
                        ->andWhere(['!=', 'para_cuota_prestamo', 'si'])
                        ->andWhere(['!=', 'activo_biologico', 'si'])
                        ->andWhere(['!=', 'activo_fijo', 'si'])
                        ->andWhere(['=', 'para_importacion', 'no']);

                    // No permitir agregar un detalle con cuenta ya utilizada en otra plantilla del mismo tipo_asiento, pero si permitir crear
                    //  varios detalles con misma cuenta para una plantilla.
                    if (\Yii::$app->session->has('cont_plantilla_en_edicion'))
                        $query->andFilterWhere(['!=', 'plantilla_id', \Yii::$app->session->get('cont_plantilla_en_edicion')]);

                    $detalle = $query->asArray()->all();
                    if (!empty($detalle)) {
                        $msg = "La cuenta {$model->pCGravada->nombre} ya fue utilizada en la plantilla '{$detalle[0]['nombre']}' (id = {$detalle[0]['id']})";
                        $model->addError('p_c_gravada_id', $msg);
                        \Yii::warning($model->getErrorSummaryAsString());
                    }
                }
                break;
            case 'iva_cta_id':
                if (isset($model->plantilla_id) && $model->plantilla->esParaImportacionExterior() && $model->iva_cta_id != "") {
                    $model->addError('iva_cta_id', "Plantillas para Importación NO PUEDE tener Cuenta de IVA. Sólo se permiten cuentas exentas.");
                }
                break;
        }
    }
}