<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Recibo;
use yii\helpers\Url;
use yii\validators\Validator;

class ReciboNroValidator extends Validator
{
    /** El nro del recibo no debe repetir bajo mismo proveedor/cliente..
     *
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        /** @var Recibo $model */
        $recibo = Recibo::find()
            ->where(['numero' => $model->numero, 'entidad_id' => $model->entidad_id])
            ->andFilterWhere(['!=', 'id', $model->id]); // TODO: Ver si permitir repetir nro del mismo proveedor si periodo contable es distinto.

        if ($recibo->exists()) {
            $recibo = $recibo->one();
            /** @var Recibo $recibo */
            $model->addError($attribute, "El número del recibo ya fue usado para la entidad {$recibo->entidad->razon_social} de la empresa {$recibo->empresa->razon_social} en el periodo {$recibo->periodoContable->anho}.");
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = Url::toRoute(['recibo/recibo-numero-validator']);
        return <<<JS
let recibo_nro = $('#recibo-numero').val();
let entidad_id = $('#recibo-entidad_id').val();

if (recibo_nro !== "" && entidad_id !== "") {
    deferred.push(
        $.get("$url", { // se envia asi porque en la action no se usa
            recibo_nro: recibo_nro,
            entidad_id: entidad_id,
            recibo_id: global_recibo_id, // variable del POS_HEAD
        }).done(
            function (data) {
                if (data.error) {
                    messages.push(data.error);
                }
            }
        )
    );
}
JS;
    }
}
