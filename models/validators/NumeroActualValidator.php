<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\TimbradoDetalle;
use yii\validators\Validator;

class NumeroActualValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var TimbradoDetalle $model */
        if ($model->nro_fin != null && $model->nro_inicio != null && ($model->nro_fin != $model->nro_inicio)) {
            if (($model->$attribute > $model->nro_fin + 1) || ($model->$attribute < $model->nro_inicio))
                $this->addError($model, $attribute, 'Número Actual no puede ser mayor que Número Fin ni menor que Numero Inicio.');
        }
    }
}
