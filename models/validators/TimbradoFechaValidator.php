<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Timbrado;
use DateTime;
use yii\validators\Validator;

class TimbradoFechaValidator extends Validator
{
    // Valida que el periodo de vigencia no sea mayor a 1 anho.
    // Peticion de Sara a Jose, enviado por Jose el 22 enero 6.53pm.
    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @throws \Exception
     */
    public function validateAttribute($model, $attribute)
    {
        /** @var $model Timbrado */
        $desde = $model->fecha_inicio != '' && $model->fecha_inicio != null ? ((strlen(explode('-', $model->fecha_inicio)[2]) == 4) ?
            implode('-', array_reverse(explode('-', $model->fecha_inicio))) : $model->fecha_inicio) : null;
        $hasta = $model->fecha_fin != '' && $model->fecha_fin != null ? ((strlen(explode('-', $model->fecha_fin)[2]) == 4) ?
            implode('-', array_reverse(explode('-', $model->fecha_fin))) : $model->fecha_fin) : null;

        if ($desde != null && $hasta != null) {
            $d1 = new DateTime($desde);
            $d2 = new DateTime($hasta);

            $diff = $d2->diff($d1);
            \Yii::warning("$diff->y $diff->m $diff->d");
            if (!(($diff->y == 1 && $diff->m == 0) || ($diff->y == 0)))
                $model->addError('fecha_fin', "El periodo de vigencia es de $diff->y año(s) $diff->m mes(es) $diff->d día(s).");
        }

        if ($desde == null && $hasta == null) {
            $model->addError('fecha_fin', "Fecha Fin es requerido si Fecha Inicio es vacio.");
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = \yii\helpers\Url::to(['validate-attribute-fecha']);
        return <<<JS
deferred.push(
    $.get("$url", {
        desde: $('input[id*="timbrado"][id$="fecha_inicio"]').val(),
        hasta: $('input[id*="timbrado"][id$="fecha_fin"]').val(),
    }).done(
        function (result) {
            if (result !== '') {
                messages.push(result);
            }
        }
    )
);
JS;
    }
}
