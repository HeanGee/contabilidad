<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\Venta;
use yii\validators\Validator;

class VentaFechaEmisionValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        /** @var Venta $model */

        $periodo_contable_actual = EmpresaPeriodoContable::find()->where(['id' => \Yii::$app->session->get('core_empresa_actual_pc')])->one()->anho;
        $date = \DateTime::createFromFormat("Y-m-d", $model->$attribute);

        if (($date) && ($date instanceof \DateTime) && ($date->format('Y') != $periodo_contable_actual) && ($attribute == 'fecha_emision')) {
            $this->addError($model, $attribute, "La fecha {$model->$attribute} no corresponde al periodo contable actual");
        }

        if ($model->estado != 'faltante') {
            if ($model->fecha_emision == "")
                $this->addError($model, 'fecha_emision', "No puede estar vacio.");
            elseif (!strtotime($model->$attribute)) {
                $this->addError($model, $attribute, "Error de Formato.");
            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $periodoContable = EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        $anho = $periodoContable->anho;

        return <<<JS
if ($('#venta-estado').val() !== 'faltante') {
    let emision = $('#venta-fecha_emision');
    
    if (emision.val() === "") {
        messages.push("No puede estar vacio.");
    } else {
        let element = $("#venta-$attribute");

        let fecha = element.val().replace(new RegExp('-', 'g'), '/'); // reemplazar '-' por '/'
        fecha = ((fecha.split('/')).reverse()).join('/'); // invertir dia y anho de lugares.

        let date = new Date(fecha); // crear objeto Date.

        if (isNaN(date)) { // isNaN() verifica si se creo bien el objeto.
            messages.push("Error de Formato.");
        } else if (emision.val().substring(6,10) !== "$anho")
            messages.push("No corresponde al periodo contable actual.");
    }
}
JS;
    }
}
