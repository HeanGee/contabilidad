<?php

namespace backend\modules\contabilidad\models\validators;

use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\Venta;
use yii\helpers\Url;
use yii\validators\Validator;

/**
 * Class NroFacturaValidator
 * @package backend\modules\contabilidad\models\validators
 */
class RucValidator extends Validator
{
    private $msg = 'Este R.U.C. no corresponde a ninguna entidad registrada.';

    public function validateAttribute($model, $attribute)
    {
        if ($model instanceof Venta && in_array($model->estado, ['anulada', 'faltante'])) {
            $ruc = $model->$attribute;
            $entidad = Entidad::findOne(['ruc' => $ruc]);
            if (!$entidad) {
                $model->addError($attribute, $this->msg);
            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $url = Url::toRoute(['venta/check-ruc']);
        return <<<JS
if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
    deferred.push(
        $.get("$url", {
            ruc: $('#venta-ruc').val()
        }).done(
            function (data) {
                if (data.error) {
                    messages.push(data.error);
                }
            }
        )
    );
}
JS;
    }
}