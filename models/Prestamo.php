<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;
use backend\modules\contabilidad\models\validators\PrestamoProformaCasesValidator;
use backend\modules\contabilidad\models\validators\PrestamoProformaIncluyeIvaValidator;
use Exception;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "cont_prestamo".
 *
 * @property string $id
 * @property string $nro_prestamo
 * @property string $fecha_operacion
 * @property string $monto_operacion
 * @property string $intereses_vencer
 * @property string $gastos_bancarios
 * @property string $iva_10
 * @property string $caja
 * @property string $tipo_interes
 * @property string $primer_vencimiento
 * @property string $vencimiento El ultimo mes en el que se paga la ultima cuota.
 * @property string $tasa
 * @property int $cant_cuotas
 * @property int $asiento_id
 * @property int $entidad_id
 * @property int $factura_compra_id
 * @property int $periodo_contable_id
 * @property int $cuenta_caja
 * @property int $cuenta_intereses_vencer
 * @property int $cuenta_gastos_bancarios
 * @property int $cuenta_iva_10
 * @property int $cuenta_gastos_no_deducibles
 * @property int $cuenta_monto_operacion
 * @property int $cuenta_intereses_pagados
 * @property int $cuenta_intereses_a_pagar
 * @property string $empresa_id
 * @property string $usar_iva_10
 * @property string $incluye_iva
 * @property string $iva_interes_cobrado // Eliminado el 28 de marzo del 2019.
 * @property string $bloqueado
 * @property string $concepto_devengamiento_cuota
 * @property string $tiene_factura
 * @property string $es_del_periodo_anterior
 *
 * @property string $formattedMontoOperacion
 * @property string $formattedInteresesVencer
 * @property string $formattedGastosBancarios
 * @property string $formattedCaja
 * @property string $formattedTasa
 * @property string $formattedIva10
 * @property string $formattedFechaOperacion
 * @property string $formattedVencimiento
 * @property string $formattedPrimerVencimiento
 * @property string $formattedDeducible
 * @property string $formattedIncluyeIva
 * @property string $formattedIvaInteresCobrado
 * @property string $ruc
 * @property string $razon_social
 * @property int $prestamo_id_selector
 * @property string $nroFactura
 * @property string $capital
 *
 * @property Empresa $empresa
 * @property Entidad $entidad
 * @property Asiento $asiento
 * @property EmpresaPeriodoContable $periodoContable
 * @property PrestamoDetalle[] $prestamoDetalles
 * @property PrestamoDetalle[] $prestamoDetallesConSaldo
 * @property Compra[] $compras
 * @property Compra $compra
 * @property Compra[] $facturasCuotas
 * @property ReciboPrestamo[] $recibos
 *
 * @property string $esGravada
 */
class Prestamo extends \backend\models\BaseModel
{
    # Redisenho realizado segun las directricies del correo de magali,
    # del dia 22/3/19 19:21, asunto : 'RE: Fwd: Fwd: RE: Asiento por cuotas de prestamos devengados'.
//    public $detalles = [];
    public $prestamo_id_selector;
    public $ruc;
    public $razon_social;
    public $capital;

    public static $_EDITABLE_ERROR_MSGS = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_prestamo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['fecha_operacion', 'monto_operacion', 'vencimiento', 'primer_vencimiento', 'periodo_contable_id', 'empresa_id',
                    'intereses_vencer', 'gastos_bancarios', 'caja', 'tipo_interes', 'cant_cuotas', 'entidad_id', 'ruc',
                    'razon_social', 'incluye_iva', 'cuenta_caja', 'cuenta_intereses_vencer', 'cuenta_gastos_bancarios', 'cuenta_iva_10',
                    'cuenta_gastos_no_deducibles', 'cuenta_monto_operacion', 'cuenta_intereses_pagados', /*'iva_interes_cobrado' eliminado el 28 03 19 ,*/ 'bloqueado',
                    'concepto_devengamiento_cuota', 'cuenta_intereses_a_pagar', 'tiene_factura', 'usar_iva_10', 'nro_prestamo',
                    'es_del_periodo_anterior'],
                'required'
            ],
            [
                ['monto_operacion', 'tasa', 'cant_cuotas', 'intereses_vencer', 'gastos_bancarios', 'bloqueado',
                    'iva_10', 'caja', 'usar_iva_10', 'tipo_interes', 'razon_social', 'incluye_iva', /*'iva_interes_cobrado' eliminado el 28 03 19 ,*/
                    'concepto_devengamiento_cuota', 'tiene_factura', 'nro_prestamo', 'es_del_periodo_anterior'],
                'string'
            ],
            [['periodo_contable_id', 'empresa_id', 'asiento_id', 'entidad_id', 'cuenta_caja', 'factura_compra_id',
                'cuenta_intereses_vencer', 'cuenta_gastos_bancarios', 'cuenta_iva_10', 'cuenta_gastos_no_deducibles',
                'cuenta_monto_operacion', 'cuenta_intereses_pagados'], 'integer'],
            [['fecha_operacion', 'vencimiento', 'primer_vencimiento', 'prestamo_id_selector', 'usar_iva_10',
                'cuenta_caja', 'cuenta_intereses_vencer', 'cuenta_gastos_bancarios', 'cuenta_iva_10', 'cuenta_gastos_no_deducibles',
                'cuenta_monto_operacion', 'cuenta_intereses_pagados', 'capital', 'tiene_factura', 'cuenta_intereses_a_pagar'], 'safe'],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['asiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id' => 'id']],
            [['entidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entidad::className(), 'targetAttribute' => ['entidad_id' => 'id']],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],

            /** Personal validations */
            [['tiene_factura'], PrestamoProformaCasesValidator::className()],
            [['incluye_iva'], PrestamoProformaIncluyeIvaValidator::className()],
//            [['iva_10', 'usar_iva_10'], PrestamoIvaValidator::className()],

//            [['vencimiento'], PrestamoFechaVencValidator::className()],
//            [['primer_vencimiento'], PrestamoPriVencValidator::className()],

//            [['vencimiento'], PrestamoFechaValidator::className()],
//            [['fecha_operacion'], PrestamoFechaValidator::className()],
//            [['primer_vencimiento'], PrestamoFechaValidator::className()],
        ];
    }

    public function getDetallesId()
    {
        $ids = [];
        foreach ($this->prestamoDetalles as $item) {
            $ids[] = $item->id;
        }
        return $ids;
    }

    public function attributeLabels()
    {
        return self::getAttributesLabelText();
    }

    public static function getAttributesLabelText()
    {
        return [
            'id' => 'ID',
            'fecha_operacion' => 'Fecha Operación',
            'monto_operacion' => 'Monto Operación',
            'vencimiento' => 'Último Vencimiento',
            'primer_vencimiento' => '1er. Vencimiento',
            'tasa' => 'Tasa (anual)',
            'cant_cuotas' => 'Cantidad de Cuotas',
            'periodo_contable_id' => 'Periodo Contable',
            'asiento_id' => 'Asiento ID',
            'empresa_id' => 'Empresa ID',
            'intereses_vencer' => "Intereses a Vencer",
            'gastos_bancarios' => "Gastos Bancarios",
            'iva_10' => "IVA 10%",
            'caja' => 'Neto a Entregar',
            'usar_iva_10' => 'Usar IVA 10?',
            'tipo_interes' => 'Tipo Interés',
            'entidad_id' => "Entidad",
            'ruc' => "R.U.C",
            'razon_social' => "Entidad Razón social",
            'factura_compra_id' => "Factura de Compra",
            'incluye_iva' => "Interés IVA incluído?",
//            'iva_interes_cobrado' => "I.V.A. Interés facturado?", // eliminado el 28 de marzo
            'cuenta_caja' => "Cuenta Caja",
            'cuenta_intereses_vencer' => "Cuenta Interes a vencer",
            'cuenta_gastos_bancarios' => "Cuenta Gastos Bancarios",
            'cuenta_iva_10' => "Cuenta Iva 10%",
            'cuenta_gastos_no_deducibles' => "Cuenta gastos no deducibles",
            'cuenta_monto_operacion' => 'Cuenta Banco XX',
            'cuenta_intereses_pagados' => "Cuenta Intereses pagados",
            'cuenta_intereses_a_pagar' => "Cuenta Intereses a pagar",
            'nro_prestamo' => 'Número',
            'concepto_devengamiento_cuota' => "Concepto devengamiento interés",
        ];
    }

    public static function manejarTextoAuditoria($campo, &$viejo, &$nuevo)
    {
        switch ($campo) {
            case 'empresa_id':
                $viejo[0] = !empty($viejo[0]) ? Empresa::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Empresa::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'asiento_id':
                $viejo[0] = !empty($viejo[0]) ? Asiento::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Asiento::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'entidad_id':
                $viejo[0] = !empty($viejo[0]) ? Entidad::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Entidad::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'factura_compra_id':
                $viejo[0] = !empty($viejo[0]) ? Compra::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Compra::findOne($nuevo[0])->id : $nuevo[0];
                return true;
        }
        return false;
    }

    public function getEsGravada()
    {
        return !$this->incluye_iva == 'si';
    }

    public function getFormattedIncluyeIva()
    {
        return ucfirst($this->incluye_iva);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidad()
    {
        return $this->hasOne(Entidad::className(), ['id' => 'entidad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsiento()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id']);
    }

    /**
     * @return string
     */
    public function getNroFactura()
    {
        return isset($this->compra->nro_factura) ? $this->compra->nro_factura : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestamoDetalles()
    {
        return $this->hasMany(PrestamoDetalle::className(), ['prestamo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturasCuotas()
    {
        return Compra::find()->alias('compra')
            ->leftJoin('cont_prestamo_detalle_compra as pdcompra', 'compra.id = pdcompra.factura_compra_id')
            ->leftJoin('cont_prestamo_detalle as pdetalle', 'pdetalle.id = pdcompra.prestamo_detalle_id')
            ->leftJoin('cont_prestamo as prestamo', 'prestamo.id = pdetalle.prestamo_id')
            ->where(['prestamo.id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompras() // usado por el view de facturas relacionadas.
    {
        return Compra::find()->alias('compra')
            ->leftJoin('cont_prestamo as prestamo', 'compra.id = prestamo.factura_compra_id')
            ->where(['prestamo.id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompra() // usado para saber si esta facturado o no
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * @return Prestamo[]
     */
    public static function getPrestamosNoFacturados($_desde = null, $_hasta = null)
    {
        return self::find()->where(['IS', 'factura_compra_id', null])
            ->andWhere(['IS', 'asiento_id', null])
            ->andFilterWhere(['BETWEEN', 'fecha_operacion', $_desde, $_hasta])
            ->andFilterWhere(['periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')
                , 'empresa_id' => Yii::$app->session->get('core_empresa_actual')
            ])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecibos()
    {
        return ReciboPrestamo::find()->alias('recibo')
            ->leftJoin('cont_recibo_prestamo_detalle as recibod', 'recibod.recibo_prestamo_id = recibo.id')
            ->leftJoin('cont_prestamo_detalle as prestamod', 'prestamod.id = recibod.prestamo_detalle_id')
            ->leftJoin('cont_prestamo as prestamo', 'prestamo.id = prestamod.prestamo_id')
            ->where(['prestamo.id' => $this->id]);
    }

    public function getFormattedDeducible()
    {
        return ucfirst($this->usar_iva_10);
    }

    public function getFormattedMontoOperacion()
    {
        return number_format($this->monto_operacion, 0, ',', '.');
    }

    public function getFormattedInteresesVencer()
    {
        return number_format($this->intereses_vencer, 0, ',', '.');
    }

    public function getFormattedGastosBancarios()
    {
        return number_format($this->gastos_bancarios, 0, ',', '.');
    }

    public function getFormattedCaja()
    {
        return number_format($this->caja, 0, ',', '.');
    }

    public function getFormattedTasa()
    {
        return number_format($this->tasa, '2', ',', '.') . ' %';
    }

    public function getFormattedIva10()
    {
        return number_format($this->iva_10, 2, ',', '.');
    }

    public function getFormattedIvaInteresCobrado()
    {
        return "iva_interes_cobrado eliminado.";
    }

    public function getFormattedFechaOperacion()
    {
        return $this->fecha_operacion = implode('-', array_reverse(explode('-', $this->fecha_operacion)));
    }

    public function getFormattedVencimiento()
    {
        return $this->vencimiento = implode('-', array_reverse(explode('-', $this->vencimiento)));
    }

    public function getFormattedPrimerVencimiento()
    {
        return $this->primer_vencimiento = implode('-', array_reverse(explode('-', $this->primer_vencimiento)));
    }


    /** Invierte la posicion del anho y del dia.
     *
     * Se supone que el campo de fecha esta bien puesto.
     *
     */
    public function formatDateField($_formatFor)
    {
        if ($this->fecha_operacion != null) {
            $_parts = explode('-', $this->fecha_operacion);
            if ($_formatFor == 'db' && strlen($_parts[0]) != 4) {
                $this->fecha_operacion = implode('-', array_reverse(explode('-', $this->fecha_operacion)));
            } elseif ($_formatFor == 'view' && strlen($_parts[0]) != 2) {
                $this->fecha_operacion = implode('-', array_reverse(explode('-', $this->fecha_operacion)));
            }

        }

        if ($this->vencimiento != null) {
            $_parts = explode('-', $this->vencimiento);
            if ($_formatFor == 'db' && strlen($_parts[0]) != 4) {
                $this->vencimiento = implode('-', array_reverse(explode('-', $this->vencimiento)));
            } elseif ($_formatFor == 'view' && strlen($_parts[0]) != 2) {
                $this->vencimiento = implode('-', array_reverse(explode('-', $this->vencimiento)));
            }

        }

        if ($this->primer_vencimiento != null) {
            $_parts = explode('-', $this->primer_vencimiento);
            if ($_formatFor == 'db' && strlen($_parts[0]) != 4) {
                $this->primer_vencimiento = implode('-', array_reverse(explode('-', $this->primer_vencimiento)));
            } elseif ($_formatFor == 'view' && strlen($_parts[0]) != 2) {
                $this->primer_vencimiento = implode('-', array_reverse(explode('-', $this->primer_vencimiento)));
            }

        }
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        /** TEST */
//        $this->fecha_operacion = '01-01-2019';
//        $this->vencimiento = '01-01-2020';
//        $this->primer_vencimiento = '01-02-2019';
//        $this->cant_cuotas = 12;
//        $this->monto_operacion = 11780000;
//        $this->intereses_vencer = 7800000;
//        $this->gastos_bancarios = 2200000;
//        $this->tipo_interes = 'fr';
//        $this->ruc = 2446209;
//        $this->razon_social = Entidad::findOne(['ruc' => $this->ruc])->razon_social;
        /** FIN TEST */

        $campo_nombres = ['cuenta_caja', 'cuenta_intereses_vencer', 'cuenta_gastos_bancarios', 'cuenta_iva_10', 'cuenta_gastos_no_deducibles', 'cuenta_monto_operacion', 'cuenta_intereses_pagados', 'cuenta_intereses_pagados'];
        foreach ($campo_nombres as $campo_nombre) {
            if (!isset($this->$campo_nombre) || $this->$campo_nombre == 0 || $this->$campo_nombre == '0') {
                $_cuenta = str_replace("cuenta_", '', $campo_nombre);
                $empresa_id = \Yii::$app->session->get('core_empresa_actual');
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_prestamo-{$_cuenta}";
                $parametro = ParametroSistema::find()->where(['like', 'nombre', $nombre]);
                if ($parametro->exists()) {
                    $this->$campo_nombre = $parametro->one()->valor;
                } else
                    $this->$campo_nombre = null;
            }
        }

        isset($this->empresa_id) || $this->empresa_id = Yii::$app->session->get('core_empresa_actual');
        isset($this->periodo_contable_id) || $this->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
//        isset($this->iva_interes_cobrado) || $this->iva_interes_cobrado = 'si'; // iva_interes_cobrado eliminado el 28 de marzo del 2019
        isset($this->usar_iva_10) || $this->usar_iva_10 = 'si';
        isset($this->tiene_factura) || $this->tiene_factura = 'no';
        isset($this->tipo_interes) || $this->tipo_interes = 'sm';

        return parent::loadDefaultValues($skipIfSet); // TODO: Change the autogenerated stub
    }

    public function beforeValidate()
    {
        if (strpos($this->monto_operacion, '.') && strlen(substr($this->monto_operacion, (strpos($this->monto_operacion, '.') + 1))) >= 3) {
            $this->monto_operacion = str_replace('.', '', $this->monto_operacion);
            $this->monto_operacion = str_replace(',', '.', $this->monto_operacion);
        } elseif (strpos($this->monto_operacion, ',') == strlen($this->monto_operacion) - 3)
            $this->monto_operacion = str_replace(',', '.', $this->monto_operacion);

        $this->cant_cuotas = str_replace('.', '', $this->cant_cuotas);
        $this->tasa = str_replace(',', '.', $this->tasa);

        if (strpos($this->intereses_vencer, '.') && strlen(substr($this->intereses_vencer, (strpos($this->intereses_vencer, '.') + 1))) >= 3) {
            $this->intereses_vencer = str_replace('.', '', $this->intereses_vencer);
            $this->intereses_vencer = str_replace(',', '.', $this->intereses_vencer);
        } elseif (strpos($this->intereses_vencer, ',') == strlen($this->intereses_vencer) - 3)
            $this->intereses_vencer = str_replace(',', '.', $this->intereses_vencer);

        if (strpos($this->gastos_bancarios, '.') && strlen(substr($this->gastos_bancarios, (strpos($this->gastos_bancarios, '.') + 1))) >= 3) {
            $this->gastos_bancarios = str_replace('.', '', $this->gastos_bancarios);
            $this->gastos_bancarios = str_replace(',', '.', $this->gastos_bancarios);
        } elseif (strpos($this->gastos_bancarios, ',') == strlen($this->gastos_bancarios) - 3)
            $this->gastos_bancarios = str_replace(',', '.', $this->gastos_bancarios);

        if (strpos($this->iva_10, '.') && strlen(substr($this->iva_10, (strpos($this->iva_10, '.') + 1))) >= 3) {
            $this->iva_10 = str_replace('.', '', $this->iva_10);
            $this->iva_10 = str_replace(',', '.', $this->iva_10);
        } elseif (strpos($this->iva_10, ',') == strlen($this->iva_10) - 3)
            $this->iva_10 = str_replace(',', '.', $this->iva_10);

        if (strpos($this->caja, '.') && strlen(substr($this->caja, (strpos($this->caja, '.') + 1))) >= 3) {
            $this->caja = str_replace('.', '', $this->caja);
            $this->caja = str_replace(',', '.', $this->caja);
        } elseif (strpos($this->caja, ',') == strlen($this->caja) - 3)
            $this->caja = str_replace(',', '.', $this->caja);

        $this->formatDateField('db');

        $entidad = Entidad::find()->where(['ruc' => $this->ruc]);
        if ($entidad->exists()) {
            $this->entidad_id = $entidad->one()->id;
            $this->razon_social = $entidad->one()->razon_social;
        } else {
            $this->addError('ruc', 'No existe entidad con este R.U.C.');
        }

        if ($this->cant_cuotas == 0 || $this->cant_cuotas == "0") {
            $this->addError('cant_cuotas', "Cantidad de cuotas no puede ser cero.");
        } elseif ($this->primer_vencimiento > $this->vencimiento) {
            $this->addError('primer_vencimiento', "Primer vencimiento no puede ser mayor al último vencimiento.");
        }

        if ($this->bloqueado == null)
            $this->bloqueado = 'no';

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function convertToViewFormat()
    {
        $this->monto_operacion = number_format($this->monto_operacion, '2', ',', '.');

        $this->cant_cuotas = number_format($this->cant_cuotas, '2', ',', '.');

        $this->intereses_vencer = number_format($this->intereses_vencer, '2', ',', '.');

        $this->tasa = number_format($this->tasa, '2', ',', '.');

        $this->gastos_bancarios = number_format($this->gastos_bancarios, '2', ',', '.');

        $this->iva_10 = number_format($this->iva_10, '2', ',', '.');

        $this->caja = number_format($this->caja, '2', ',', '.');
    }

    public static function getTipoInteresLista()
    {
        $_retornar = [];
        $map = self::tipoIntereses();

        try {
            $valores_enum = Prestamo::getTableSchema()->columns['tipo_interes']->enumValues;
            foreach ($valores_enum as $_v)
                $_retornar[] = ['id' => $_v, 'text' => $map[$_v]];

        } catch (Exception $e) {
            /* TODO: manejar la excepción */
        }

        return $_retornar;
    }

    public static function getEsGravadaLista()
    {
        $_retornar = [];

        try {
            $valores_enum = Prestamo::getTableSchema()->columns['incluye_iva']->enumValues;
            foreach ($valores_enum as $_v)
                $_retornar[] = ['id' => $_v, 'text' => ucfirst($_v)];

        } catch (Exception $e) {
            /* TODO: manejar la excepción */
        }

        return $_retornar;
    }

    public static function getTieneFacturaList()
    {
        $_retornar = [];

        try {
            $valores_enum = Prestamo::getTableSchema()->columns['tiene_factura']->enumValues;
            foreach ($valores_enum as $_v)
                $_retornar[] = ['id' => $_v, 'text' => ucfirst(str_replace('_', ' ', $_v))];

        } catch (Exception $e) {
            /* TODO: manejar la excepción */
        }

        return $_retornar;
    }

    public static function getTieneFacturaTxt($key)
    {
        try {
            $valores_enum = Prestamo::getTableSchema()->columns['tiene_factura']->enumValues;
            foreach ($valores_enum as $_v)
                if ($key == $_v) return ucfirst(str_replace('_', ' ', $_v));

        } catch (Exception $e) {
            /* TODO: manejar la excepción */
        }

        return "";
    }

    public static function tipoIntereses()
    {
        $map = [
            'us' => "Americano",
            'fr' => 'Francés',
            'de' => 'Alemán',
            'sm' => 'Simple'
        ];

        asort($map);

        return $map;
    }

    public static function getTipoInteresTxt($key)
    {
        $map = self::tipoIntereses();

        return $map[$key];
    }

    public static function getEsGravadaTxt($key)
    {
        try {
            $valores_enum = Prestamo::getTableSchema()->columns['incluye_iva']->enumValues;
            foreach ($valores_enum as $_v)
                if ($key == $_v) return ucfirst($_v);

        } catch (Exception $e) {
            /* TODO: manejar la excepción */
        }

        return "";
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function validarMontoCuotas()
    {
        foreach ($this->prestamoDetalles as $prestamoDetalle) {
            if ($prestamoDetalle->interes < 0)
                throw new Exception("En la cuota #{$prestamoDetalle->nro_cuota}, el interés no puede ser negativo.");
        }

//        // Se deja como backup.
//        $interes_t = 0.0;
//        $capital_t = 0.0;
//        $cuota_t = 0.0;
//
//        foreach ($this->prestamoDetalles as $prestamoDetalle) {
//            $interes_t += (int)$prestamoDetalle->interes;
//            $capital_t += (int)$prestamoDetalle->capital;
//            $cuota_t += (int)$prestamoDetalle->monto_cuota;
//        }
//
//
//        $interes_prestamo = round($this->intereses_vencer * (($this->incluye_iva == 'no') ? 1.1 : 1));
//        if (round($interes_t) != $interes_prestamo) {
//            $interes_t = number_format($interes_t, 0, ',', '.');
//            $this->addError('interes_vencer', "El interés no coincide con la suma de intereses {$interes_t}");
//        }
//
//        if (round($capital_t) != $this->caja) {
//            $caja = number_format($capital_t, 0, ',', '.');
//            $this->addError('caja', "El capital no coincide con la suma de los capitales {$caja}");
//        }
//
//        $total_cuota = round($this->monto_operacion - $this->gastos_bancarios * (($this->incluye_iva == 'no') ? 1.1 : 1));
//        if (round($cuota_t) != $total_cuota) {
//            $total_cuota = number_format($total_cuota, 0, ',', '.');
//            $cuota_t = number_format($cuota_t, 0, ',', '.');
//            $this->addError('monto_operacion',
//                "Se espera que la suma de las cuotas sea {$total_cuota}. La suma dió {$cuota_t}.");
//        }

        return !$this->hasErrors();
    }

    public function isEditable()
    {
        // Cuotas relacionadas con compra.
        $ids_detalle = $this->getPrestamoDetalles()->all();
        $ids_detalle_compra = PrestamoDetalleCompra::find()
            ->where(['periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->andWhere(['empresa_id' => Yii::$app->session->get('core_empresa_actual')])
            ->select(['prestamo_detalle_id'])
            ->all();

        $ids_needle = [];
        foreach ($ids_detalle as $item) {
            $ids_needle[] = $item['id'];
        }

        $ids_haystack = [];
        foreach ($ids_detalle_compra as $item) {
            $ids_haystack[] = $item['prestamo_detalle_id'];
        }

        // interseccion entre todos los detalles y aquellos facturados; si el conj no es vacio, no debe poder editarse.
        $ids_diff1 = array_intersect($ids_needle, $ids_haystack);

        // cuotas relacionadas con ReciboPrestamo
        $ids_detalle_recibo = ReciboPrestamoDetalle::find()
            ->where(['periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->andWhere(['empresa_id' => Yii::$app->session->get('core_empresa_actual')])
            ->select(['prestamo_detalle_id'])
            ->all();

        $ids_haystack = [];
        foreach ($ids_detalle_recibo as $item) {
            $ids_haystack[] = $item['prestamo_detalle_id'];
        }

        // interseccion entre todos los detalles y los que tienen recibo.
        $ids_diff2 = array_intersect($ids_needle, $ids_haystack); // si el conj no es vacio, no debe poder editarse.

        // prestamo tiene asiento generado
        $prestamo_asentado = $this->asiento_id != null;

        // prestamo tiene factura
        $prestamo_tiene_factura = $this->factura_compra_id != null;

        // detalle tiene asiento de devengamiento
        $tiene_detalle_asentado = $this->getPrestamoDetalles()->where(['IS NOT', 'asiento_devengamiento_id', null])->exists();

        if ($prestamo_tiene_factura)
            self::$_EDITABLE_ERROR_MSGS[] = "Este Préstamo tiene asociado Factura {$this->getNroFactura()}";
        if (sizeof($ids_diff1) > 0)
            self::$_EDITABLE_ERROR_MSGS[] = "Este Préstamo tiene facturas por cuotas";
        if (sizeof($ids_diff2) > 0)
            self::$_EDITABLE_ERROR_MSGS[] = "Este Préstamo tiene recibos por cuotas";
        if ($prestamo_asentado) {
            $fecha = implode('-', array_reverse(explode('-', $this->asiento->fecha)));
            self::$_EDITABLE_ERROR_MSGS[] = "Este Préstamo tiene asiento con ID {$this->asiento_id} de la fecha {$fecha}";
        }
        if ($tiene_detalle_asentado) {
            self::$_EDITABLE_ERROR_MSGS[] = "Este Préstamo tiene cuotas que generaron asiento de devengamiento.";
        }

        return (sizeof($ids_diff1) == 0 && sizeof($ids_diff2) == 0) && !$prestamo_asentado && !$prestamo_tiene_factura && !$tiene_detalle_asentado;
    }

    public function getPrestamoDetallesConSaldo()
    {
        return $this->hasMany(PrestamoDetalle::className(), ['prestamo_id' => 'id'])
            ->where([
                'OR',
                ['>', 'monto_cuota_saldo', 0],
                ['OR', ['>', 'interes_saldo', 0], ['>', 'capital_saldo', 0]]
            ]);
    }

    public static function cuentasForAsientoPrestamoExists()
    {
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_prestamo-";
        $parametro = ParametroSistema::find()->where(['like', 'nombre', $nombre]);

        return $parametro->exists();
    }

    public static function cuentaInteresesPagadosExist()
    {
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_prestamo-intereses_pagados";
        $parametro = ParametroSistema::find()->where(['=', 'nombre', $nombre]);

        return $parametro->exists();
    }

    public function refillCalculatedFields()
    {
        $entidad = Entidad::findOne($this->entidad_id);
        $this->ruc = $entidad->ruc;
        $this->razon_social = $entidad->razon_social;
        $this->prestamo_id_selector = $this->id;
    }

    public function getCuentasFields()
    {
        return ['cuenta_caja', 'cuenta_intereses_vencer', 'cuenta_gastos_bancarios', 'cuenta_iva_10', 'cuenta_gastos_no_deducibles', 'cuenta_monto_operacion', 'cuenta_intereses_pagados', 'cuenta_intereses_a_pagar'];
    }

    /**
     * @param integer|string $id_cuenta
     * @return array|\yii\db\ActiveRecord|Prestamo|null
     */
    public static function getPrestamoWithCuenta($id_cuenta)
    {
        return Prestamo::find()->where(['cuenta_caja' => $id_cuenta])
            ->orWhere(['cuenta_intereses_vencer' => $id_cuenta])
            ->orWhere(['cuenta_gastos_bancarios' => $id_cuenta])
            ->orWhere(['cuenta_iva_10' => $id_cuenta])
            ->orWhere(['cuenta_gastos_no_deducibles' => $id_cuenta])
            ->orWhere(['cuenta_monto_operacion' => $id_cuenta])
            ->orWhere(['cuenta_intereses_pagados' => $id_cuenta])
            ->one();
    }

    public function isCase1()
    {
        return ($this->usar_iva_10 == 'si' && $this->tiene_factura == 'por_prestamo');
    }

    public function isCase2()
    {
        return ($this->usar_iva_10 == 'si' && $this->tiene_factura == 'no');
    }

    public function isCase3()
    {
        return ($this->usar_iva_10 == 'no' && $this->tiene_factura == 'no');
    }

    public function isCase4()
    {
        return ($this->usar_iva_10 == 'no' && $this->tiene_factura == 'por_cuota');
    }

    /**
     * @return array
     */
    public static function getDataToRenderFromCore()
    {
        $directory = str_replace('Prestamo.php', '', __FILE__);
        $view = realpath(dirname(__FILE__) . "/views-for-externals/view-cuotas-devengadas.php");
        $today = date("Y-m-d");
        $firstDay = date("Y-m-01", strtotime($today . "-1 month"));
        $lastDay = date("Y-m-t", strtotime($firstDay));

        {
            $lastDateOfCurMonth = date("Y-m-t");
            if (explode('-', $today)[2] == explode('-', $lastDateOfCurMonth)[2]) {
                $firstDay = $lastDay = $lastDateOfCurMonth;
            }
        }

        $query = new Query();
        $query->select(['de.id', 'de.nro_cuota', 'de.prestamo_id', 'de.asiento_devengamiento_id']);
        $query->from('cont_prestamo_detalle de');
        $query->leftJoin('cont_prestamo pr', 'pr.id = de.prestamo_id');
        $query->where([
            'pr.empresa_id' => Yii::$app->session->get("core_empresa_actual"),
            'pr.periodo_contable_id' => Yii::$app->session->get("core_empresa_actual_pc"),
        ]);
        $query->andWhere(['AND', ['IS NOT', 'de.asiento_devengamiento_id', null], ['BETWEEN', 'de.vencimiento', $firstDay, $lastDay]]);
//        $query->andWhere(['IS NOT', 'de.asiento_devengamiento_id', null]);

        return ['view' => $view, 'query' => $query];
    }
}
