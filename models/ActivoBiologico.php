<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;
use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "cont_activo_biologico".
 *
 * Antes se llamaba 'cont_activo_ganado' por lo que en algunas partes aun se utiliza
 * mnemotecnicos en funcion del 'ganado' en vez de 'biologico'.
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $periodo_contable_id
 * @property int $stock_inicial
 * @property int $stock_final
 * @property int $precio_unitario
 * @property int $precio_unitario_actual
 * @property int $activo_biologico_padre_id // se pretende utilizar al migrar de periodo, para conocer de que a.bio. se migro especificamente.
 * @property int $especie_id
 * @property int $clasificacion_id
 * @property int $sexo
 * @property string $stock_actual
 *
 * @property string $total
 * @property string $guardar_cerrar
 *
 * @property string $formattedPrecioUnitario
 * @property string $formattedTotal
 * @property string $formattedStockFinal
 * @property string $formattedStockActual
 * @property string $formattedStockInicial
 * @property string $formattedPrecioUnitarioActual
 *
 * @property string $dbFormattedStockFinal
 * @property string $dbFormattedStockActual
 * @property string $dbFormattedStockInicial
 * @property string $dbFormattedPrecioUnitario
 * @property string $dbFormattedPrecioUnitarioActual
 * @property string $dbFormattedTotal
 * @property string $dbFformattedStockInicial
 * @property string $clasificacionNombre
 *
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoContable
 * @property ActivoBiologico $padre
 * @property ActivoBiologicoEspecie $especie
 * @property ActivoBiologicoEspecieClasificacion $clasificacion
 * @property ActivoBiologicoCierreInventario $inventario
 * @property string $nombre
 */
class ActivoBiologico extends \backend\models\BaseModel
{
    public $total;
    public $guardar_cerrar;
    public $total_inicial; // usado desde cierre de inventario

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_activo_biologico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'periodo_contable_id', 'stock_final', 'stock_inicial', 'precio_unitario', 'sexo', 'especie_id', 'clasificacion_id', 'stock_actual', 'precio_unitario_actual'], 'required'],
            [['empresa_id',], 'integer'],
            [['guardar_cerrar', 'total'], 'safe'],
//            [['stock_final', 'stock_inicial', 'stock_actual'], 'string'],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['activo_biologico_padre_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoBiologico::className(), 'targetAttribute' => ['activo_biologico_padre_id' => 'id']],
            [['especie_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoBiologicoEspecie::className(), 'targetAttribute' => ['especie_id' => 'id']],
            [['clasificacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoBiologicoEspecieClasificacion::className(), 'targetAttribute' => ['clasificacion_id' => 'id']],

            [['clasificacion_id'], 'especieClasificUnico'],
        ];
    }

    /**
     * Valida que al crear, la especie y clasificacion sea unica.
     *
     * Este mecanismo impide crear mas de una vez un mismo activo biologico,
     * de modo que el stock sea controlado por compra, venta o notas.
     * Se puede editar cambiando unos valores iniciales como
     * precio_unitario y cantidad actuales.
     *
     * @param $attribute
     * @param $params
     */
    function especieClasificUnico($attribute, $params)
    {
        $query = self::find()->where([
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
            'especie_id' => $this->especie_id,
            'clasificacion_id' => $this->$attribute,
        ])->andWhere(['!=', 'id', $this->id]);

        if ($query->exists())
            $this->addError($attribute, "A. Biológico de especie '{$this->especie->nombre}' y clasificación '{$this->clasificacion->nombre}' ya existe para la empresa y periodo actual.");
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa ID',
            'periodo_contable_id' => 'Periodo Contable ID',
            'stock_final' => 'Stock Final',
            'precio_unitario' => 'P. U.  Inicial',
            'precio_unitario_actual' => 'P. U. Actual',
            'stock_inicial' => 'Stock Inicial',
            'stock_actual' => 'Stock Actual',
            'activo_biologico_padre_id' => "Periodo anterior",
            'especie_id' => 'Especie',
            'clasificacion_id' => "Clasificación",
            'sexo' => "Sexo",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventario()
    {
        return ActivoBiologicoCierreInventario::find()->where([
            'empresa_id' => $this->empresa_id,
            'periodo_contable_id' => $this->periodo_contable_id,
            'especie_id' => $this->especie_id,
            'clasificacion_id' => $this->clasificacion_id,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPadre()
    {
        return $this->hasOne(ActivoBiologico::className(), ['id' => 'activo_biologico_padre_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecie()
    {
        return $this->hasOne(ActivoBiologicoEspecie::className(), ['id' => 'especie_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasificacion()
    {
        return $this->hasOne(ActivoBiologicoEspecieClasificacion::className(), ['id' => 'clasificacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    public static function getClasificacionByEspecie($especie_id = null)
    {
        $result = [];
        $clasificaciones = ActivoBiologicoEspecieClasificacion::find()->filterWhere(['activo_biologico_especie_id' => $especie_id])->all();
        $i = 0;
        foreach ($clasificaciones as $clasificacion) {
            $i++;
            $nombre = ucfirst($clasificacion->nombre);
            $result[$clasificacion->id] = "{$i}. {$nombre}";
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        $query = Compra::find()->alias('compra');
        $query->leftJoin('cont_activo_biologico_stock_manager as stockmanager', 'compra.id = stockmanager.factura_compra_id');
        $query->where(['stockmanager.empresa_id' => Yii::$app->session->get('core_empresa_actual'), 'stockmanager.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
        $query->andWhere(['stockmanager.especie_id' => $this->especie_id, 'stockmanager.clasificacion_id' => $this->clasificacion_id]);

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        $query = Venta::find()->alias('venta');
        $query->leftJoin('cont_activo_biologico_stock_manager as stockmanager', 'venta.id = stockmanager.factura_venta_id');
        $query->where(['stockmanager.empresa_id' => Yii::$app->session->get('core_empresa_actual'), 'stockmanager.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
        $query->andWhere(['stockmanager.especie_id' => $this->especie_id, 'stockmanager.clasificacion_id' => $this->clasificacion_id]);

        return $query;
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->empresa_id = Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
        $this->activo_biologico_padre_id = "";
        $this->sexo = 'generico';

        return parent::loadDefaultValues($skipIfSet); // TODO: Change the autogenerated stub
    }

    public function getTotal()
    {
//        $this_total = (int)$this->precio_unitario * (int)$this->stock_inicial;
//
//        $_precio_total_acum = 0;
//
//        $query = ActivoBiologicoStockManager::find()->where([
//            'especie_id' => $this->especie_id,
//            'clasificacion_id' => $this->clasificacion_id,
//            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
//            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
//        ]);
//        $query->andWhere(['IS NOT', 'factura_compra_id', null]);
//
//        /** @var ActivoBiologicoStockManager $stock_manager */
//        foreach ($query->all() as $stock_manager) {
//            $delta = (int)$stock_manager->cantidad * (int)$stock_manager->precio_unitario;
//            $delta *= ($stock_manager->nota_compra_id == '') ? 1 : -1;
//            $msg = "Factura: $stock_manager->factura_compra_id, tipo: {$stock_manager->facturaCompra->tipo}, {$stock_manager->especie->nombre} {$stock_manager->clasificacion->nombre}, Total: {$stock_manager->cantidad}x{$stock_manager->precio_unitario}={$delta}";
////            Yii::warning($msg);
//            $_precio_total_acum += $delta;
//        }
////        Yii::warning('----------------');

        return (($this->precio_unitario_actual * $this->stock_actual));
    }

    public function getNombre()
    {
        if (!isset($this->especie) || !isset($this->clasificacion)) return "- sin especie/clasificación -";
        return "{$this->especie->nombre} {$this->clasificacion->nombre}";
    }

    public function getFormattedPrecioUnitario()
    {
        $mod = fmod((float)$this->precio_unitario, (int)$this->precio_unitario);
        if ($mod == 0) {
            return number_format($this->precio_unitario, 0, '', '.');
        }
        return number_format($this->precio_unitario, 2, ',', '.');
    }

    public function getFormattedPrecioUnitarioActual()
    {
        $mod = fmod((float)$this->precio_unitario_actual, (int)$this->precio_unitario_actual);
        if ($mod == 0) {
            return number_format($this->precio_unitario_actual, 0, '', '.');
        }
        return number_format($this->precio_unitario_actual, 2, ',', '.');
    }

    public function getFormattedTotal()
    {
        $this->total = $this->getTotal();

        $mod = fmod((float)$this->total, (int)$this->total);
        if ($mod == 0) {
            return number_format($this->total, 0, '', '.');
        }
        return number_format($this->total, 2, ',', '.');
    }

    public function getFormattedStockFinal()
    {
        return number_format($this->stock_final, 0, '', '.');
    }

    public function getFormattedStockInicial()
    {
        return number_format($this->stock_inicial, 0, '', '.');
    }

    public function getFormattedStockActual()
    {
        return number_format($this->stock_actual, 0, '', '.');
    }

    public function beforeValidate()
    {
        if (strpos($this->precio_unitario, '.') && strlen(substr($this->precio_unitario, (strpos($this->precio_unitario, '.') + 1))) >= 3) {
            $this->precio_unitario = str_replace('.', '', $this->precio_unitario);
            $this->precio_unitario = str_replace(',', '.', $this->precio_unitario);
        } elseif (strpos($this->precio_unitario, ',') == strlen($this->precio_unitario) - 3)
            $this->precio_unitario = str_replace(',', '.', $this->precio_unitario);

        if (strpos($this->precio_unitario_actual, '.') && strlen(substr($this->precio_unitario_actual, (strpos($this->precio_unitario_actual, '.') + 1))) >= 3) {
            $this->precio_unitario_actual = str_replace('.', '', $this->precio_unitario_actual);
            $this->precio_unitario_actual = str_replace(',', '.', $this->precio_unitario_actual);
        } elseif (strpos($this->precio_unitario_actual, ',') == strlen($this->precio_unitario_actual) - 3)
            $this->precio_unitario_actual = str_replace(',', '.', $this->precio_unitario_actual);

        if (strpos($this->total, '.') && strlen(substr($this->total, (strpos($this->total, '.') + 1))) >= 3) {
            $this->total = str_replace('.', '', $this->total);
            $this->total = str_replace(',', '.', $this->total);
        } elseif (strpos($this->total, ',') == strlen($this->total) - 3)
            $this->total = str_replace(',', '.', $this->total);

        $this->stock_final = str_replace('.', '', $this->stock_final);

        $this->stock_inicial = str_replace('.', '', $this->stock_inicial);

        $this->stock_actual = str_replace('.', '', $this->stock_actual);

//        if ($this->stock_inicial > $this->stock_final) { // si puede ser
//            $this->addError('stock_inicial', "Stock Inicial no puede ser mayor al stock final.");
//        }
//
//        if ((float)$this->precio_unitario * (int)$this->stock_final == 0) {
//            $this->addError('total', 'Total no puede ser cero.');
//        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function getDbFormattedStockFinal()
    {
        $stock_final = $this->stock_final;
        return str_replace('.', '', $stock_final);
    }

    public function getDbFormattedStockInicial()
    {
        $stock_inicial = $this->stock_inicial;
        return str_replace('.', '', $stock_inicial);
    }

    public function getDbFormattedStockActual()
    {
        $stock_actual = $this->stock_actual;
        return str_replace('.', '', $stock_actual);
    }

    public function getDbFormattedPrecioUnitarioActual()
    {
        $precio_unitario_actual = $this->precio_unitario_actual;
        return str_replace('.', '', $precio_unitario_actual);
    }

    public function getDbFormattedPrecioUnitario()
    {
        $precio_unitario = $this->precio_unitario;
        return str_replace('.', '', $precio_unitario);
    }

    public function getDbFormattedTotal()
    {
        $total = $this->total;
        return str_replace('.', '', $total);
    }

    public function getClasificacionNombre()
    {
        return isset($this->clasificacion_id) ? $this->clasificacion->nombre : '';
    }


    /**
     * @param $model_from ActivoBiologico
     */
    public function copyFrom($model_from)
    {
        $this->especie_id = $model_from->especie_id;
        $this->clasificacion_id = $model_from->clasificacion_id;
        $this->stock_inicial = $model_from->stock_final;
        $this->stock_final = $this->stock_inicial; // al comienzo inicial == final
        $this->precio_unitario = $model_from->precio_unitario;
        $this->empresa_id = $model_from->empresa_id;
    }

    public function isTraspasable($periodo_id)
    {
        return !ActivoBiologico::find()->where(['periodo_contable_id' => $periodo_id, 'activo_biologico_padre_id' => $this->id])->exists();
    }

    public static function getSexoList()
    {
        $_retornar = [];
        try {
            $valores_enum = self::getTableSchema()->columns['sexo']->enumValues;
            foreach ($valores_enum as $_v)
                $_retornar[] = ['id' => $_v, 'text' => ucfirst($_v)];

        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
        }
        return $_retornar;
    }

    public static function getSexoName($key)
    {
        $map = self::getSexoList();
        foreach ($map as $item) {
            if ($item['id'] == $key)
                return $item['text'];
        }
        return "";
    }

    /**
     * @throws \yii\base\Exception
     */
    public static function isCreable()
    {
        if (!ActivoBiologicoEspecie::find()->where([
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])->exists()) {
            throw new \yii\base\Exception("No se puede crear Activos Biológicos. Este error suele ocurrir si es que no se ha creado ninguna especie y clasificación aún para la empresa y periodo actuales. Revise.");
        }

        $inventarioPeriodoActual = ActivoBiologicoCierreInventario::getInventarioCurrentPeriodo();
        if (!empty($inventarioPeriodoActual)) {
            throw new \yii\base\Exception("Ya se generó el cierre de inventario de activos biológicos de este periodo contable, por lo tanto no se puede crear más.");
        }

        return true;
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function isEditable()
    {
        if ($this->getVentas()->exists()) {
            throw new \yii\base\Exception("El activo biológico tiene asociado ventas. Por lo tanto no puede modificar.");
        }

        if ($this->getInventario()->exists()) {
            throw new \yii\base\Exception("Ya se generó el cierre de inventario de activos biológicos de este periodo contable, por lo tanto no se puede modificar más.");
        }

        return true;
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function isDeleteable()
    {
        if ($this->getCompras()->exists()) {
            throw new \yii\base\Exception("Tiene asociado factura de Compra.");
        }

        if ($this->getVentas()->exists()) {
            throw new \yii\base\Exception("Tiene asociado factura de Venta.");
        }

        if ($this->getInventario()->exists()) {
            throw new \yii\base\Exception("Ya se generó el cierre de inventario de activos biológicos de este periodo contable, por lo tanto no se puede borrar más.");
        }

        return true;
    }

    /**
     *  Recalcula el precio_unitario y stock actuales del a.bio, en base a todos los StockManagers registrados.
     *
     *  Tal vez sea utilizado solamente cuando es compra.
     *
     */
    public function recalcularPrecioUnitarioStockActuales($idToExclude = null)
    {
        $_precio_total_acum = 0;
        $_stock_total_acum = 0;

        $query = ActivoBiologicoStockManager::find()->where([
            'especie_id' => $this->especie_id,
            'clasificacion_id' => $this->clasificacion_id,
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $query->andWhere(['IS NOT', 'factura_compra_id', null]);
        $query->andWhere(['IS', 'nota_compra_id', null]);
        $query->andFilterWhere(['!=', 'id', $idToExclude]);

        /** @var ActivoBiologicoStockManager $stock_manager */
        foreach ($query->all() as $stock_manager) {
            $_precio_total_acum += (int)$stock_manager->cantidad * (int)$stock_manager->precio_unitario;
            $_stock_total_acum += (int)$stock_manager->cantidad;
        }

        $this->stock_actual = (int)$this->stock_inicial + (int)$_stock_total_acum;
        if ($this->stock_actual > 0)
            $this->precio_unitario_actual = round(($_precio_total_acum + (int)$this->precio_unitario * (int)$this->stock_inicial) / $this->stock_actual);
        else
            $this->precio_unitario_actual = 0;

        $this->recalcularStockActual($idToExclude); // si existe managers por venta, tambien hay que tener en cuenta.
    }

    /**
     * Recalcula el stock actual del a.bio, en base a todos los StockManagers registrados.
     *
     * Si es nota (segun jose por el momento sera solamente de credito, porque debito no maneja stock)
     * simplemente se cambia el stock_actual mas no el precio_unitario_actual.
     * @param null $idToExclude
     */
    public function recalcularStockActual($idToExclude = null)
    {
        $_stock_total_acum = 0;

        // acumular cantidad comprada.
        $query = ActivoBiologicoStockManager::find()->where([
            'especie_id' => $this->especie_id,
            'clasificacion_id' => $this->clasificacion_id,
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $query->andWhere(['IS NOT', 'factura_compra_id', null])->andWhere(['IS', 'nota_compra_id', null]);
        $query->andFilterWhere(['!=', 'id', $idToExclude]); // si el id era de manager generado por venta, no tiene efecto.

        /** @var ActivoBiologicoStockManager $stock_manager */
        foreach ($query->all() as $stock_manager) {
            $_stock_total_acum += (int)$stock_manager->cantidad;
        }

        // restarle cantidad devuelta (nota de credito)
        $query = ActivoBiologicoStockManager::find()->where([
            'especie_id' => $this->especie_id,
            'clasificacion_id' => $this->clasificacion_id,
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $query->andWhere(['IS NOT', 'factura_compra_id', null])->andWhere(['IS NOT', 'nota_compra_id', null]);
        $query->andFilterWhere(['!=', 'id', $idToExclude]); // si el id era de manager generado por venta, no tiene efecto.

        /** @var ActivoBiologicoStockManager $stock_manager */
        foreach ($query->all() as $stock_manager) {
            $_stock_total_acum -= (int)$stock_manager->cantidad;
        }

        // restar cantidad vendida
        $query = ActivoBiologicoStockManager::find()->where([
            'especie_id' => $this->especie_id,
            'clasificacion_id' => $this->clasificacion_id,
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $query->andWhere(['IS NOT', 'factura_venta_id', null]);
        $query->andWhere(['IS', 'nota_venta_id', null]);
        $query->andFilterWhere(['!=', 'id', $idToExclude]); // si el id era de manager generado por compra, no tiene efecto.

        /** @var ActivoBiologicoStockManager $stock_manager */
        foreach ($query->all() as $stock_manager) {
            $_stock_total_acum -= (int)$stock_manager->cantidad;
        }

        // sumar cantidad retornada (nota de credito
        $query = ActivoBiologicoStockManager::find()->where([
            'especie_id' => $this->especie_id,
            'clasificacion_id' => $this->clasificacion_id,
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $query->andWhere(['IS NOT', 'factura_venta_id', null]);
        $query->andWhere(['IS NOT', 'nota_venta_id', null]);
        $query->andFilterWhere(['!=', 'id', $idToExclude]); // si el id era de manager generado por compra, no tiene efecto.

        /** @var ActivoBiologicoStockManager $stock_manager */
        foreach ($query->all() as $stock_manager) {
            $_stock_total_acum += (int)$stock_manager->cantidad;
        }

        // se espera que $_stock_acum no sea negativo.
        // sera necesario controlar en venta, que no se indique una cantidad superior al stock_actual del a.bio existente.
        $this->stock_actual = (int)$this->stock_inicial + (int)$_stock_total_acum;
    }

    /**
     *  Recalcula el stock y precio_unitario actuales de todos los a.bios EN LOTE, basandose en todos los managers
     * registrados por compra y venta. Dependiendo del parametro $_tipo_factura, ejecuta la actualizacion de
     * precio_unitario_actual y stock_actual, o solamente stock_actual.
     *
     * @param string $_tipo_factura Indica si el metodo fue invocado desde compra o desde venta.
     * @return ActivoBiologico[] retorna todos los activos biologicos cuyos stock y precio_unitario actuales fueron recalculados.
     * @throws \Exception lanza excepcion si no logra validar.
     */
    public static function recalcPrecioUStockActualesAll($_tipo_factura)
    {
        $query = self::find()->where([
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
        ]);

        $actbios = [];

        /** @var ActivoBiologico $actibo_biologico */
        foreach ($query->all() as $actibo_biologico) {
            if ($_tipo_factura == 'compra') {
                $actibo_biologico->recalcularPrecioUnitarioStockActuales();
            } elseif ($_tipo_factura == 'venta') {
                $actibo_biologico->recalcularStockActual();
            }

            if (!$actibo_biologico->validate()) {
                throw new \Exception("Error recalculando (en lote) stock y precio unitario actuales de todos
                 los A. Biológicos: {$actibo_biologico->getFormattedStockActual()}");
            }
            $actbios[] = $actibo_biologico;
        }

        return $actbios;
    }

    /**
     *  Actualiza el stock (y precio_unitario si es compra) actuales del a.bio que concuerda con especie y clasificacion
     * del $stock_manager recibido como parametro. Si tal a.bio no existe y el $stock_manager fue creado desde compra,
     * se crea nuevo a.bio con $precio_unitario, $stock_inicial y $stock_final cero. Si tal a.bio no existe y el manager
     * fue creado desde venta, entonces lanza una excepcion indicando que no puede pretender vender algo que no existe
     * en el stock.
     *
     *  Posiblemente sera utilizado solamente desde create() de Compra y Venta.
     *
     * @param $stock_manager ActivoBiologicoStockManager
     * @return ActivoBiologico|null
     * @throws Exception Lanza excepcion si la cantidad a vender en stock_manager es mayor a stock_actual del a.bio.
     */
    public static function actualizarStock_O_CrearNuevo($stock_manager, $idToExclude = null)
    {
        // busca el a.bio que matchee especie_id y clasificacion_id del $stock_manager.
        /** @var ActivoBiologico $actbio */
        $actbio = ActivoBiologico::find()->where([
            'especie_id' => $stock_manager->especie_id,
            'clasificacion_id' => $stock_manager->clasificacion_id,
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
        ])->one();

        // si es compra...
        if (isset($stock_manager->factura_compra_id)) {

            // ... pero el a.bio no existe, se crea uno nuevo.
            if (!isset($actbio)) {
                $actbio = new ActivoBiologico();
                $actbio->loadDefaultValues();
                $actbio->especie_id = $stock_manager->especie_id;
                $actbio->clasificacion_id = $stock_manager->clasificacion_id;
                $actbio->stock_inicial = 0;
                $actbio->stock_actual = $stock_manager->cantidad;
                $actbio->stock_final = 0;
                $actbio->precio_unitario = 0;
                $actbio->precio_unitario_actual = $stock_manager->precio_unitario;
                $actbio->empresa_id = Yii::$app->session->get('core_empresa_actual');
                $actbio->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

                // ... y existe un a.bio que matchea especie_id y clasificacion_id del $stock_manager,
                // actualiza stock_actual y precio_unitario_actual del a.bio encontrado, usando promedio ponderado.
            } else {
                $actbio->recalcularPrecioUnitarioStockActuales($idToExclude);
            }

            // si es venta...
        } elseif (isset($stock_manager->factura_venta_id)) {

            if (isset($actbio)) {
                // ... y existe un a.bio que matchea con especie_id y clasificacion_id del $stock_manager,
                // actualiza stock_actual, decrementando la cantidad vendida.

                // Del dia 12 febrero 2019: Con Jose, se llego al acuerdo de que se permita vender aun si la cantidad
                // es superior al stock_actual, ya que no se tuvo el inventario inicial en el momento.
//                if (($stock_manager->id != $idToExclude) && (int)$stock_manager->cantidad > (int)$actbio->stock_actual) {
//                    $msg = "Cantidad de {$actbio->nombre} a vender no puede ser mayor a {$actbio->stock_actual}";
//                    throw new Exception($msg);
//                }

                $actbio->recalcularStockActual($idToExclude);
//                $actbio->stock_actual = (int)$actbio->stock_actual - (int)$stock_manager->cantidad;
            } else {
                throw new Exception("No existe ningún Activo Biológico " .
                    "registrado que sea de especie '{$stock_manager->especie->nombre}' y clasificacion '{$stock_manager->clasificacion->nombre}'");
            }
        }

        return $actbio;
    }


}
