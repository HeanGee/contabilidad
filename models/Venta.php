<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use backend\models\Moneda;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\validators\NroFacturaVentaValidator;
use backend\modules\contabilidad\models\validators\RequiredValidator;
use backend\modules\contabilidad\models\validators\RucValidator;
use backend\modules\contabilidad\models\validators\VentaFechaEmisionValidator;
use backend\modules\contabilidad\models\validators\VentaFechaVencimientoValidator;
use common\helpers\ValorHelpers;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 * This is the model class for table "cont_factura_venta".
 *
 * @property int $id
 * @property int $tipo_documento_id
 * @property string $fecha_emision
 * @property string $condicion
 * @property string $fecha_vencimiento
 * @property int $entidad_id
 * @property int $obligaciones
 * @property string $total
 * @property string $saldo
 * @property string $prefijo
 * @property string $nro_factura
 * @property int $moneda_id
 * @property int $cant_cuotas
 * @property int $timbrado_detalle_id
 * @property string $valor_moneda
 * @property int $empresa_id
 * @property int $periodo_contable_id
 * // * @property int $retencion_id
 * @property string $estado
 * @property string $asiento_id
 * @property string $tipo
 * @property string $cotizacion_propia
 * @property int $factura_venta_id
 * @property string $creado
 * @property string $modificado
 * @property string $creado_por
 * @property string $observaciones
 *
 * @property string $nombre_documento
 * @property string $nro_completo
 * @property string[] $camposTotales;
 * @property string $timbrado_detalle_id_prefijo;
 * @property string $nro_timbrado;
 * @property string $ruc;
 * @property string $nombre_entidad;
 * @property string $moneda_nombre;
 * @property array $_iva_ctas_usadas;
 * @property string $usar_tim_vencido;
 * @property int $obligacion_id
 * @property string $para_iva
 * @property string $nroFacturaCompleto
 * @property string $bloqueado
 *
 * Para la vista:
 * @property string $exenta;
 * @property string $impuesto5;
 * @property string $impuesto10;
 * @property string $gravada5;
 * @property string $gravada10;
 * @property string $plantillas;
 *
 * @property VentaIvaCuentaUsada[] $ivasCuentaUsadas
 * @property CuotaVenta[] $cuotaVentas
 * @property DetalleVenta[] $detalleVentas
 * @property Moneda $moneda
 * @property Entidad $entidad
 * @property Empresa $empresa
 * @property TipoDocumento $tipoDocumento
 * @property PlantillaCompraventa $plantilla
 * @property TimbradoDetalle $timbradoDetalle
 * @property Timbrado $timbrado
 * @property Asiento $asiento
 * @property EmpresaPeriodoContable $periodoContable
 * @property Retencion $retencion
 * @property Venta $facturaVenta
 * @property Venta[] $notas
 * @property ActivoFijo[] $activoFijos
 * @property ActivoBiologico[] $activosBiologicos
 * @property Obligacion $obligacion
 * @property ReciboDetalle[] $reciboDetalles
 * @property PlantillaCompraventa[] $plantillasUsadas
 */
class Venta extends BaseModel
{
    public $nombre_documento;
    public $nro_completo;
    public $camposTotales = [];
    public $rules = [];
    public $timbrado_detalle_id_prefijo;
    public $ruc;
    public $nombre_entidad;
    public $nro_timbrado;
    public $moneda_nombre;
    public $_total_ivas;
    public $_total_gravadas;
    public $emision_factura;
    public $razon_social;
    public $dv;
    public $fecha;
    public $usar_tim_vencido;
    public $nroFacturaCompleto;

    const NRO_FACTURA_CORRECTO = 1;
    const NRO_FACTURA_DUPLICADO = 2;
    const NRO_FACTURA_NO_PREFIJO = 3;
    const NRO_FACTURA_NO_RANGO = 4;

    public $_iva_ctas_usadas = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_factura_venta';
    }

    public function behaviors()
    {
        $parentBehabiors = parent::behaviors();
        $behabiors = [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'creado',
                'updatedAtAttribute' => 'modificado',
                'value' => new Expression('NOW()'),
            ],
        ];

        return array_merge($parentBehabiors, $behabiors);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
        $motivoNoVigente = self::getMotivoNoVigente($empresa_id, $periodo_id);
        $this->rules = [
            # Miercoles 19 junio 2019: Magali "solicita/se pone de acuerdo en" que el tdoc sea siempre requerido porque otros tdos pueden tambien ser anulados, por ej, nota de credito.
            [['tipo_documento_id', 'timbrado_detalle_id_prefijo', 'prefijo', 'estado', 'bloqueado'], 'required'], // no requerir nro_factura ya que es suficiente con requerir timbrado_detalle_id_prefijo
            [['fecha_emision', 'condicion', 'ruc', 'entidad_id', 'moneda_id', 'timbrado_detalle_id_prefijo', 'total', 'timbrado_detalle_id', 'cotizacion_propia', 'para_iva'], RequiredValidator::className()],
            [['id', 'tipo_documento_id', 'entidad_id', 'moneda_id', 'empresa_id', 'obligaciones', 'timbrado_detalle_id', 'asiento_id', 'periodo_contable_id', 'factura_venta_id'], 'integer'],
            [['fecha_emision', 'fecha_vencimiento', 'nombre_documento', 'plantilla', 'timbrado_detalle_id_prefijo',
                'nro_completo', 'nro_timbrado', 'valor_moneda', 'total', 'ruc', 'nombre_entidad', 'moneda_nombre',
                'timbrado_detalle_id', '_total_ivas', '_total_gravadas', 'saldo', 'creado_por', 'observaciones'], 'safe'],
            [[/*'total', 'valor_moneda',*/
                'cant_cuotas'], 'number'],
            [['estado', 'tipo', 'cotizacion_propia', 'bloqueado', 'observaciones'], 'string',],
            [['condicion'], 'string', 'max' => 7],
            [['nro_factura'], 'string', 'max' => 7],
            [['prefijo'], 'string', 'max' => 7],
            [['id'], 'unique'],
            [['usar_tim_vencido'], 'safe'],

            [['nro_factura'], NroFacturaVentaValidator::className()],

            [['moneda_id'], 'exist', 'skipOnError' => false, 'targetClass' => Moneda::className(), 'targetAttribute' => ['moneda_id' => 'id']],
            [['entidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entidad::className(), 'targetAttribute' => ['entidad_id' => 'id']],
            [['tipo_documento_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['tipo_documento_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['plantilla_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlantillaCompraventa::className(), 'targetAttribute' => ['plantilla_id' => 'id']],
            [['asiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],

            [['fecha_emision'], VentaFechaEmisionValidator::className()],
            [['fecha_vencimiento'], VentaFechaVencimientoValidator::className()],
            [['cant_cuotas'], 'attributeValidator', 'when' => function ($model) {
                return (!in_array($model->estado, ['anulada', 'faltante']) && $model->condicion == 'credito');
            }, 'skipOnEmpty' => false, 'skipOnError' => false],
            [['fecha_vencimiento'], 'attributeValidator', 'when' => function ($model) {
                return (!in_array($model->estado, ['anulada', 'faltante']) && $model->condicion == 'credito');
            }, 'skipOnEmpty' => false, 'skipOnError' => false],
            [['fecha_vencimiento'], 'fechaVenceValidator', 'when' => function ($model) {
                return (!in_array($model->estado, ['anulada', 'faltante']) && $model->condicion == 'credito');
            }, 'skipOnEmpty' => false, 'skipOnError' => false],
            [['ruc'], RucValidator::className()],
            [['factura_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['factura_venta_id' => 'id']],
            [['observaciones'], 'required', 'when' => function ($model) use ($motivoNoVigente) {
                return $model->estado != 'vigente' && strlen($motivoNoVigente) > 0;
            }, 'whenClient' => "function(attribute, value) {
                let estado = $('input[id$=\"estado\"]').val();
                let empresaParametroExist = '$motivoNoVigente';
                return (estado !== 'vigente' && empresaParametroExist.length > 0);
            }", 'message' => "El motivo de no vigencia no puede estar vacío porque la empresa actual tiene parametrizado dicho motivo",
            ],

            [['tipo_documento_id', 'timbrado_detalle_id', 'timbrado_detalle_id_prefijo'], 'timbradoDetalleValidator'],
            [['saldo'], 'saldoValidator'],
            [['obligacion_id'], 'required', 'when' => function ($model) {
                return $model->factura_venta_id == '' && $model->tipo != 'factura' && $model->estado == 'vigente';
            }, 'whenClient' => "function(attribute, value) {
                console.log('factura asociada:', $('#venta-factura_venta_id').val() === '');
                return ($('#venta-factura_venta_id').val() === '' && $('#venta-estado').val() === 'vigente');
            }"],

            // documento unico
            [['entidad_id', 'tipo_documento_id', 'timbrado_detalle_id', 'prefijo', 'nro_factura'], 'facturaUnicoValidator'],
        ];

        return $this->rules;
    }

    public function facturaUnicoValidator($attribute, $params, $validator)
    {
        $timbrado = ($this->timbrado_detalle_id == '' ? "" : $this->timbradoDetalle->timbrado_id);
        $combined_val = [];
        $combined_attr = [];
        $query = self::find()->alias('factura')
            ->leftJoin('cont_timbrado_detalle timdet', 'timdet.id = factura.timbrado_detalle_id')
            ->leftJoin('cont_timbrado tim', 'tim.id = timdet.timbrado_id');

        if ($this->entidad_id == '') {
            $query->where(['IS', 'factura.entidad_id', null]);
        } else {
            $query->where(['=', 'factura.entidad_id', $this->entidad_id]);
            $combined_val[] = $this->ruc;
            $combined_attr[] = 'RUC';
        }

        if ($this->tipo_documento_id == '') {
            $query->andWhere(['IS', 'factura.tipo_documento_id', null]);
        } else {
            $query->andWhere(['=', 'factura.tipo_documento_id', $this->tipo_documento_id]);
            $combined_val[] = $this->tipoDocumento->nombre;
            $combined_attr[] = 'Tipo de Documento';
        }

        if ($timbrado == '') {
            $query->andWhere(['IS', 'factura.timbrado_detalle_id', null]);
        } else {
            $query->andWhere(['=', 'tim.id', $timbrado]);
            $combined_val[] = $this->timbradoDetalle->timbrado->nro_timbrado;
            $combined_attr[] = "Timbrado";
        }

        if ($this->nro_factura == '') {
            $query->andWhere(['IS', 'factura.nro_factura', null]);
        } else {
            $query->andWhere(['=', 'factura.nro_factura', $this->nro_factura]);
            $combined_val[] = $this->nro_factura;
            $combined_attr[] = 'Nro Factura';
        }

        $query->andFilterWhere(['!=', 'factura.id', $this->id]);
        if ($query->exists()) {
            $combined_val = "\"" . implode('", "', $combined_val) . '"';
            $combined_attr = "\"" . implode('", "', $combined_attr) . '"';
            $this->addError('nro_factura', "La combinación de $combined_val para $combined_attr ya fue utilizado");
        }
    }

    public function timbradoDetalleValidator($attribute, $params, $validator)
    {
        if (isset($this->tipoDocumento) and isset($this->timbradoDetalle))
            if ($this->tipoDocumento->tipo_documento_set_id != $this->timbradoDetalle->tipo_documento_set_id) {
                $this->addError($attribute, "El prefijo no corresponde al TIPO DE DOCUMENTO SET del TIPO DE DOCUMENTO seleccionado.");
            } elseif ($this->timbradoDetalle->timbrado->entidad_id != Entidad::findOne(['ruc' => $this->empresa->ruc])->id)
                $this->addError($attribute, "El timbrado no pertenece a la empresa actual");
    }

    public function rucValidator($attribute, $params)
    {
        $ruc = $this->$attribute;
        $entidad = Entidad::findOne(['ruc' => $ruc]);
        if (!$entidad) {
            $this->addError($attribute, 'Este R.U.C. no corresponde a ninguna entidad registrada.');
        }
    }

    public function saldoValidator($attribute, $params)
    {
        if (round($this->saldo, 2) < 0)
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} no puede ser negativo");
    }

    public function attributeValidator($attribute, $params)
    {
        if ($this->$attribute == null) {
            $this->addError($attribute, $this->getAttributeLabel($attribute) . ' no puede estar vacío.');
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws Exception
     */
    public function fechaVenceValidator($attribute, $params)
    {
        $emision = $this->fecha_emision;
        $vencimi = $this->$attribute;

        $slicesEmision = explode('-', $emision);
        $slicesVencimi = explode('-', $vencimi);

        if (strlen($slicesEmision[2]) > 2)
            $emision = implode('-', $slicesEmision);
        if (strlen($slicesVencimi[2]) > 2)
            $vencimi = implode('-', $slicesVencimi);

        if (strtotime($vencimi) < strtotime($emision)) {
            Yii::warning("emi: $emision, ven: $vencimi");
            $this->addError($attribute, $this->getAttributeLabel($attribute) . ' no puede ser una fecha pasada.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return self::getAttributesLabelText();
    }

    public static function getAttributesLabelText()
    {

        return [
            'id' => 'ID Venta',
            'tipo_documento_id' => 'Tipo de Documento',
            'fecha_emision' => 'Fecha Emisión',
            'condicion' => 'Condicion',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'entidad_id' => 'Cliente',
            'moneda_id' => 'Moneda',
            'nro_factura' => 'Nro Factura',
            'prefijo' => 'Prefijo',
            'obligaciones' => 'Obligaciones',
            'total' => 'Total',
            'plantilla_id' => 'Plantilla',
            'valor_moneda' => 'Valor Moneda',
            'empresa_id' => 'Empresa',
            'timbrado_detalle_id_prefijo' => 'Prefijo',
            'estado' => 'Estado',
            'moneda_nombre' => 'Moneda',
            'tipo' => 'Tipo',
            'saldo' => 'Saldo',
            'factura_venta_id' => 'Factura Venta',
            'cotizacion_propia' => 'Cotización Propia?',
            'periodo_contable_id' => "Periodo Contable",
            'timbrado_detalle_id' => 'Timbrado Detalle Id',
            'cant_cuotas' => "Cantidad de Cuotas.",
            'usar_tim_vencido' => 'Tim. Vencido',
            'creado' => "Creado",
            'modificado' => "Última modificación",
            'obligacion_id' => "Obligación",
            'para_iva' => 'Para I.V.A.?',
            'creado_por' => "Creado por",
            'bloqueado' => "Bloqueado"
        ];
    }

    public function getDetallesId()
    {
        $ids = [];
        foreach ($this->detalleVentas as $item) {
            $ids[] = $item->id;
        }
        return $ids;
    }

    public static function manejarTextoAuditoria($campo, &$viejo, &$nuevo)
    {
        switch ($campo) {
            case 'tipo_documento_id':
                $viejo[0] = !empty($viejo[0]) ? TipoDocumento::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? TipoDocumento::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'entidad_id':
                $viejo[0] = !empty($viejo[0]) ? Entidad::findOne($viejo[0])->razon_social : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Entidad::findOne($nuevo[0])->razon_social : $nuevo[0];
                return true;
            case 'moneda_id':
                $viejo[0] = !empty($viejo[0]) ? Moneda::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Moneda::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'empresa_id':
                $viejo[0] = !empty($viejo[0]) ? Empresa::findOne($viejo[0])->razon_social : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Empresa::findOne($nuevo[0])->razon_social : $nuevo[0];
                return true;
            case 'timbrado_detalle_id':
                $viejo[0] = !empty($viejo[0]) ? TimbradoDetalle::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? TimbradoDetalle::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'asiento_id':
                $viejo[0] = !empty($viejo[0]) ? Asiento::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Asiento::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'periodo_contable_id':
                $viejo[0] = !empty($viejo[0]) ? EmpresaPeriodoContable::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? EmpresaPeriodoContable::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'factura_venta_id':
                $viejo[0] = !empty($viejo[0]) ? self::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? self::findOne($nuevo[0])->id : $nuevo[0];
                return true;
        }
        return false;
    }

    public function getExenta()
    {
        $exenta = 0.0;
        $ivaCtaUsadas = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (!isset($iva_cta_usada->ivaCta)) {
                $exenta += (float)$iva_cta_usada->monto;
            }
        }
        return $exenta;
    }

    public function getGravada5()
    {
        $gravada = 0.0;
        $ivaCtaUsadas = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->ivaCta) && $iva_cta_usada->ivaCta->iva->porcentaje == 5) {
                $gravada += (float)$iva_cta_usada->monto / 1.05;
            }
        }
        return $gravada;
    }

    public function getGravada10()
    {
        $gravada = 0.0;
        $ivaCtaUsadas = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->ivaCta) && $iva_cta_usada->ivaCta->iva->porcentaje == 10) {
                $gravada += (float)$iva_cta_usada->monto / 1.1;
            }
        }
        return $gravada;
    }

    public function getImpuesto5()
    {
        $impuesto5 = 0.0;
        $ivaCtaUsadas = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->ivaCta) && $iva_cta_usada->ivaCta->iva->porcentaje == 5) {
                $impuesto5 += (float)$iva_cta_usada->monto - (float)$this->gravada5;
            }
        }
        return $impuesto5;
    }

    public function getImpuesto10()
    {
        $impuesto10 = 0.0;
        $ivaCtaUsadas = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->ivaCta) && $iva_cta_usada->ivaCta->iva->porcentaje == 10) {
                $impuesto10 += (float)$iva_cta_usada->monto - (float)$this->gravada10;
            }
        }
        return $impuesto10;
    }

    public function getPlantillas()
    {
        $ivaCtaUsadas = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $this->id]);
        $plantillaNombres = [];
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->plantilla) && !in_array($iva_cta_usada->plantilla->nombre, $plantillaNombres)) {
                $plantillaNombres[] = $iva_cta_usada->plantilla->nombre;
            }
        }
        return implode(', ', $plantillaNombres) . (sizeof($plantillaNombres) ? '.' : '');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuotaVentas()
    {
        return $this->hasMany(CuotaVenta::className(), ['factura_venta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleVentas()
    {
        return $this->hasMany(DetalleVenta::className(), ['factura_venta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoFijos()
    {
        return $this->hasMany(ActivoFijo::className(), ['factura_venta_id' => 'id']);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    // extranhamente devuelve el id o los ids si no se hace ->all()
    public function getActivosBiologicos()
    {
        return ActivoBiologico::find()->alias('abio')
            ->leftJoin('cont_activo_biologico_stock_manager AS stockMan',
                'abio.especie_id = stockMan.especie_id
                 AND abio.clasificacion_id = stockMan.clasificacion_id
                 AND abio.empresa_id = stockMan.empresa_id
                 AND abio.periodo_contable_id = stockMan.periodo_contable_id')
            ->where([
                // decidi cambiar de $_SESSION[...] a $this->empresa_id y $this->periodo_contable_id
                // porque es mas coherente filtrar por activos biologicos de misma empresa y periodo
                // contable que la de compra.
                'abio.empresa_id' => $this->empresa_id,
                'abio.periodo_contable_id' => $this->periodo_contable_id,
                'stockMan.factura_venta_id' => $this->id
            ])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObligacion()
    {
        return $this->hasOne(Obligacion::className(), ['id' => 'obligacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneda()
    {
        return $this->hasOne(Moneda::className(), ['id' => 'moneda_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidad()
    {
        return $this->hasOne(Entidad::className(), ['id' => 'entidad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumento()
    {
        return $this->hasOne(TipoDocumento::className(), ['id' => 'tipo_documento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimbradoDetalle()
    {
        return $this->hasOne(TimbradoDetalle::className(), ['id' => 'timbrado_detalle_id']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getTimbrado()
    {
        return $this->timbradoDetalle->timbrado;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantilla()
    {
        return $this->hasOne(PlantillaCompraventa::className(), ['id' => 'plantilla_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRetencion()
    {
        return $this->hasOne(Retencion::className(), ['factura_venta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIvasCuentaUsadas()
    {
        return $this->hasMany(VentaIvaCuentaUsada::className(), ['factura_venta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsiento()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'factura_venta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Venta::className(), ['factura_venta_id' => 'id']);
    }
//
//    public function getCamposTotal()
//    {
//        /** @var Iva $iva */
//        foreach (Iva::find()->all() as $iva) {
//            $this->camposTotales['total_iva' . $iva->id . ''] = [
//                'type' => Form::INPUT_WIDGET,
//                'widgetClass' => NumberControl::class,
//                'options' => [
//                    'value' => 0.00,
//                    'readonly' => false,
//                    'maskedInputOptions' => [
//                        'groupSeparator' => '.',
//                        'radixPoint' => ',',
//                        'rightAlign' => false,
//                        'allowMinus' => false,
//                    ],
//
//                ]
//            ];
//            array_push($this->rules,
//                [['total_iva' . $iva->id . ''], 'number']
//            );
//        }
//        return $this->camposTotales;
//    }

    /**
     * Retorna la concatenación del ID y el nombre de la moneda para el select2.
     *
     * @return array
     */
    public static function getMonedas($onlyIdAndText = false)
    {
        $query = Moneda::find()->orderBy('id ASC');
        $query_sentence = ['id', "CONCAT((id), (' - '), (nombre)) AS text", 'tiene_decimales'];
        if ($onlyIdAndText) {
            $query_sentence = ['id', "nombre AS text"];
            $array[0] = 'Todos';
            $map = ArrayHelper::map($query->select($query_sentence)->asArray()->all(), 'id', 'text');
            foreach ($map as $key => $value) {
                $array[$key] = $value;
            }
            return $array;
        }
        $result = $query->select($query_sentence)->asArray()->all();
        return $result;
    }

    public static function getCotizacionPropiaOptions()
    {
        $result = [];
        $result[] = ['id' => 'si', 'text' => 'Si'];
        $result[] = ['id' => 'no', 'text' => 'No'];
        return $result;
    }

    public function checkNroFact($action_id = 'create')
    {
        /** @var TimbradoDetalle[] $detalles */

        $nro_sting = $this->nro_factura;
        $nro = (integer)$nro_sting;
        $detalle = TimbradoDetalle::findOne(['id' => $this->timbrado_detalle_id]);

        if (!isset($detalle)) {
            return Venta::NRO_FACTURA_NO_PREFIJO;
        }

        $venta = Venta::find()
            ->where(['nro_factura' => $nro_sting])
            ->andWhere(['prefijo' => $this->prefijo])
            ->andWhere(['timbrado_detalle_id' => $this->timbrado_detalle_id])
            ->andWhere(['!=', 'estado', 'faltante']);

        $venta->andFilterWhere(['!=', 'id', $this->id]); // excluir siemre a si mismo si tiene id; en create tambien puede tener id.

        if ($venta->exists()) {
            return Venta::NRO_FACTURA_DUPLICADO;
        }

        if ($nro >= $detalle->nro_inicio && $nro <= $detalle->nro_fin) {
            return Venta::NRO_FACTURA_CORRECTO;
        }

        return Venta::NRO_FACTURA_NO_RANGO;
    }

    /**
     * Usado por la action del deferred del NroFacturaVentaValidator.php
     * @param $nro
     * @param $prefijo
     * @param $timbrado_detalle_id
     * @param $action_id
     * @param null $idToExclude
     * @return int
     */
    public static function checkNroFactura($nro, $prefijo, $timbrado_detalle_id, $action_id, $idToExclude = null)
    {
        /** @var TimbradoDetalle[] $detalles */

        $nro_sting = $nro;
        $nro = (integer)$nro_sting;
        $detalle = TimbradoDetalle::findOne(['id' => $timbrado_detalle_id]);

        if (!isset($detalle)) {
            return Venta::NRO_FACTURA_NO_PREFIJO;
        }

        $venta = Venta::find()
            ->where(['nro_factura' => $nro_sting])
            ->andWhere(['prefijo' => $prefijo])
            ->andWhere(['timbrado_detalle_id' => $timbrado_detalle_id])
            ->andWhere(['!=', 'estado', 'faltante']);

        $venta->andFilterWhere(['!=', 'id', $idToExclude]); // excluir a sí mismo siempre que tenga id.

        if ($venta->exists()) {
            return Venta::NRO_FACTURA_DUPLICADO;
        }

        if ($nro >= $detalle->nro_inicio && $nro <= $detalle->nro_fin) {
            return Venta::NRO_FACTURA_CORRECTO;
        }

        return Venta::NRO_FACTURA_NO_RANGO;
    }

    /**
     * Retorna la concatenación del prefijo y nro_factura.
     *
     * @return string Concatenación del prefijo y nro_factura.
     */
    public function getNroFacturaCompleto()
    {
        return $this->prefijo . '-' . $this->nro_factura;
    }

    /**
     * Retorna estados para select2.
     *
     * @return array
     */
    public static function getEstados($filterMode = false, $estado_excluir = "")
    {
        $array = [];
        $i = 1;
        if ($filterMode) $array['todos'] = 'Todos';
        $array[''] = '';
        $array['vigente'] = 'Vigente';
        $array['anulada'] = 'Anulada';
        $array['faltante'] = 'Faltante';
//        !$filterMode || $array['todos'] = 'Todos';
        // more states to add goes here...

        unset($array[$estado_excluir]);
        return $array;
    }

    public function getFormattedTotal()
    {
        return ValorHelpers::numberFormatMonedaSensitive($this->total, 2, $this->moneda);
    }

    public function getFormattedSaldo()
    {
        return ValorHelpers::numberFormatMonedaSensitive($this->saldo, 2, $this->moneda);
    }

    public static function getCondiciones($filterMode = false)
    {
        $array = [];
        $i = 1;
        if ($filterMode) $array['todos'] = ($i++) . ' - Todos';
        $array['contado'] = ($i++) . ' - Contado';
        $array['credito'] = ($i++) . ' - Crédito';
        // more conditions to add goes here...
        return $array;
    }

    /**
     * @param $ruc
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getEntidadByRuc($ruc)
    {
        $concat = "CONCAT((ruc), (' - ' ), (razon_social))";
        $resultado = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($ruc)) {
            $consulta = new Query();
            $consulta->select('id, ' . $concat . ' AS text')
                ->from('cont_entidad')
                ->where(['ruc' => $ruc]);

            $comando = $consulta->createCommand();
            $datos = $comando->queryAll();
            $resultado['results'] = array_values($datos);
        }

        return $resultado;
    }

    /**
     * Usado en index de venta.
     *
     * @return int|string
     */
    public function getNroTimbrado()
    {
        return ($this->timbrado_detalle_id != '' ? $this->timbradoDetalle->timbrado->nro_timbrado : "");
    }

    public static function getTimbradoNroLista()
    {
        $entidad = Entidad::findOne(['razon_social' => Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])]);
        $query = Timbrado::find()->where(['entidad_id' => $entidad->id])
            ->select(['id', 'entidad_id as text'])
            ->asArray()->all();
        $map = ArrayHelper::map($query, 'id', 'text');
        return $map;
    }


    /**
     * @param DetalleVenta[] $array Detalles de venta
     * @return array Detalles de venta agrupados en debe / haber
     */
    public static function agruparDebeHaber($array)
    {
        $debes = [];
        $haberes = [];

        foreach ($array as $item) {
            if ($item->cta_contable == 'debe') {
                $debes[] = $item;
            } elseif ($item->cta_contable == 'haber') {
                $haberes[] = $item;
            }
        }

        return array_values(array_merge($debes, $haberes));
    }

    public static function getEntidades()
    {
        $tname = Venta::tableName();
        $query = Venta::find();
        $query->joinWith('entidad as entidad');
        $query->select([
            $tname . '.entidad_id',
            'entidad.id AS id',
            'entidad.razon_social AS text'
        ]);
        return ArrayHelper::map($query->asArray()->all(), 'id', 'text');
    }

    public static function getEntidadesEmpresaPeriodoActual()
    {
        $tname = self::tableName();
        $query = self::find();
        $query->joinWith('entidad as entidad');
        $query->select([
            "MIN($tname.entidad_id) as entidad_id",
            "MIN(entidad.id) AS id",
            "CONCAT(MIN(entidad.ruc), (' - '), MIN(entidad.razon_social)) AS text"
        ]);
        $query->where([
            "{$tname}.empresa_id" => Yii::$app->session->get(SessionVariables::empresa_actual),
            "{$tname}.periodo_contable_id" => Yii::$app->session->get('core_empresa_actual_pc'),
            "{$tname}.estado" => 'vigente',
        ]);
        $query->groupBy('entidad_id');
        $query->orderBy(['CAST(entidad.ruc AS UNSIGNED)' => SORT_ASC]);
        return $query->asArray()->all();
    }

    /**
     * @param bool $doMap
     * @param array $ids Lista de ids de facturas a excluir. Usado en form del recibo: la n-esima fila de factura
     * excluye todos los ids de facturas de las filas 1 hasta n-1.     *
     * @param string $condicion
     * @param bool $concatRazonSocial
     * @param bool $sinRetenciones
     * @param bool $saldoGreaterThanZero
     * @param string $entidad_id Usado en form de recibo: para traer facturas de cierta entidad nada mas.
     * @param null $fecha_hasta
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFacturas(
        $doMap = false, $ids = [], $condicion = null/*el cliente quiso que no predetermine a credito*/, $concatRazonSocial = false, $sinRetenciones = false,
        $saldoGreaterThanZero = false, $entidad_id = "", $fecha_hasta = null)
    {
        $tname = Venta::tableName();
        $query = Venta::find();
        $concat = "CONCAT((" . $tname . ".prefijo), ('-'), (" . $tname . ".nro_factura))";
        if ($concatRazonSocial) $concat = "CONCAT((" . $tname . ".prefijo), ('-'), (" . $tname . ".nro_factura), (' \('), (entidad.razon_social), ('\)'))";
        $query->joinWith('moneda as moneda');
        $query->leftJoin('core_cotizacion AS cotizacion', 'cotizacion.moneda_id = moneda.id');
        $query->joinWith('entidad as entidad');
        $query->leftJoin('cont_recibo_detalle as recibo_detalle', 'recibo_detalle.factura_venta_id = ' . $tname . '.id');
        $query->select([
            'MIN(' . $tname . '.id) as id',
            $concat . ' as text',
            'MIN(' . $tname . '.moneda_id) as moneda_id',
            'MIN(' . $tname . '.entidad_id) as entidad_id',
            'MIN(moneda.nombre) as moneda_nombre',
            'MIN(' . $tname . '.saldo) as saldo',
            'MIN(' . $tname . '.valor_moneda) as valor_moneda',
            'MIN(entidad.razon_social) AS razon_social',
            'MIN(entidad.ruc) AS ruc',
            'MIN(cotizacion.compra) as compra',
            'MIN(cotizacion.venta) as venta',
            'MIN(' . $tname . '.fecha_emision) as fecha_emision',
        ]);

        // cotizacion de 1 dia anterior a la fecha_hasta o de ayer
        $fecha_cotizacion = isset($fecha_hasta) ?
            date('Y-m-d', strtotime('-1 day', strtotime($fecha_hasta))) : date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $query->where(
            ['OR',
                ['moneda.id' => 1],
                ['AND',
                    ['!=', 'moneda.id', 1],
                    ['=', 'cotizacion.fecha', date('Y-m-d', strtotime($fecha_cotizacion))]
                ]
            ]
        );
        $query->andWhere([
            $tname . '.empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            $tname . '.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
            $tname . '.estado' => 'vigente',
        ]);

        if ($saldoGreaterThanZero) $query->andWhere(['>', $tname . '.saldo', 0]);
        $query->andFilterWhere([$tname . '.condicion' => $condicion]);
        $query->andFilterWhere(['<=', $tname . '.fecha_emision', $fecha_hasta]);
        if (sizeof($ids) > 0) {
            $query->andFilterWhere(['NOT IN', $tname . '.id', $ids]);
        }
        if ($sinRetenciones) {
            $ventaConRetencionIDs = [];
            $retenciones = Retencion::findAll(['!=', 'factura_venta_id', null]);
            foreach ($retenciones as $retencion) {
                $ventaConRetencionIDs[] = $retencion->id;
            }
            $query->andWhere(['NOT IN', 'id', $ventaConRetencionIDs]);
        }
        $query->andFilterWhere(['entidad_id' => $entidad_id]);
        $query->groupBy($tname . '.id');
        return $doMap ? ArrayHelper::map($query->asArray()->all(), 'id', 'text') : $query->asArray()->all();
    }

    public static function getTipoDocumentos()
    {
        $query = TipoDocumento::find()->orderBy('id ASC');
        $query_sentence = ['id', "CONCAT((id), (' - '), (nombre)) AS text"];
        $array[0] = 'Todos';
        $map = ArrayHelper::map($query->select($query_sentence)->asArray()->all(), 'id', 'text');
        foreach ($map as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    public static function getFacturasListaByRucForNotas($ruc, $q = null, $tipo = 'factura')
    {
        $entidad = Entidad::findOne(['ruc' => $ruc]);
        $resultado = ['results' => ['id' => '', 'text' => '']];

        if (!isset($entidad)) {
            return $resultado;
        }

        $columnas = [
            'factura.`id`',
            '`text`' => "CONCAT(factura.prefijo, '-', factura.nro_factura)",
            '`fecha_emision`' => 'DATE_FORMAT(factura.`fecha_emision`, \'%d-%m-%Y\')',
            '`moneda_nombre`' => '`moneda`.`nombre`',
            '`moneda_id`' => '`moneda`.`id`',
            '`moneda`.`tiene_decimales`',
            '`factura`.`valor_moneda`',
            '`factura`.`tipo_documento_id`'
        ];
        $consulta = self::find()
            ->alias('factura')
            ->select([])
            ->joinWith('moneda AS moneda', false)
            ->where([
                'factura.`empresa_id`' => Yii::$app->session->get('core_empresa_actual'),
                'factura.`periodo_contable_id`' => Yii::$app->session->get('core_empresa_actual_pc'), // TODO: preguntar si tambien puede hacer notas por facturas de otro periodo.
                'factura.`entidad_id`' => $entidad->id,
                'factura.`tipo`' => $tipo,
                'factura.estado' => 'vigente'
            ])
//            ->andWhere(['>', 'factura.`saldo`', '0']) // no solo facturas credito.
            ->andFilterWhere(['LIKE', 'factura.`nro_factura`', $q]);
        if ($tipo == 'factura') {
            $consulta->andFilterWhere(['=', 'factura.condicion', 'credito']);
        } elseif ($tipo == 'nota_credito') {
            $consulta->joinWith('facturaVenta AS fac_original', false);
            $columnas['`fecha_emision_factura_original`'] = 'DATE_FORMAT(fac_original.`fecha_emision`, \'%d-%m-%Y\')';
        }
        $consulta->select($columnas);

        $filas = $consulta->asArray()->all();
        if (!empty($filas)) {
            $resultado['results'] = array_values($filas);

            Yii::warning(Json::encode($resultado['results']));
            // quitar del array aquellas facturas cuyo ivacuentausada tiene null en la columna de plantillas.
            // este problema es debido a la nueva implementacion de facturas multiplantillas, donde al agregar la columna
            // de plantilla en ivaCtaUsada, los registros que ya estaban quedaron con null en esa columna.
            foreach ($resultado['results'] as $index => $factura) {
                $query = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $factura['id']]);
                foreach ($query as $ivaCtaUsada) {
                    if (!isset($ivaCtaUsada->plantilla)) {
                        unset($resultado['results'][$index]);
                    }
                }
            }
        }
        $resultado['results'] = array_values($resultado['results']);
        return $resultado;

    }

    public static function getDatosFacturasByIdForNotas($factura_id = null, $nota_id = null, $action_id = 'create')
    {
        $respuesta = [];

        // Miercoes 13, abril 2019: Segun correo de Magali, se desea poder modificar la factura asociada.
        // Se va a dejar en comentario, la implementacion previa al nuevo, debajo de esta nueva implementacion.
        $factura = self::findOne($factura_id);
        if (!empty($factura)) {
            foreach ($factura->ivasCuentaUsadas as $_ivaCta)
                $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] = $_ivaCta->monto;
        }

        //Asumiendo que hay una sola nota de credito/debido por documento
        if (!empty($factura->notas)) {
            foreach ($factura->notas as $_nota) {
                foreach ($_nota->ivasCuentaUsadas as $_ivaCta) {
                    $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] -= (float)$_ivaCta->monto;
                }
            }
        }

        if ($action_id == 'update') {
            $factura_nota_id = self::findOne(['id' => $nota_id])->factura_venta_id;
            if ($factura_id == $factura_nota_id) #casualmente ocurre el id de factura recibido por ajax es 'null' y el id obtenido mediante consulta al model de nota tambien es '', cuando se elimina la factura seleccionada.
                foreach (self::findOne($nota_id)->ivasCuentaUsadas as $_ivaCta) {
                    $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] = $_ivaCta->monto;
                }
        }

        return $respuesta;
    }

    /* SE MANTIENE PARA PERMITIR COMPATIBILIDAD CON NOTA DE VENTA
    /* TODO: implementar caso de update, cuando se cambia de prefijo (y número de factura) */
    /* acá siempre se aumenta, no se aumenta/descuenta en caso de un cambio en el update */
    public static function updateNroActualInTimDet($prefijo_usado, $nro, $tipo_doc_set)
    {
        /** @var TimbradoDetalle $model */

        $_respuesta = ['resultado' => true];
        $nro = (integer)$nro;
        // Siempre encuentra un timbradodetalle.
        $tname = TimbradoDetalle::tableName() . '.';
        $empresa = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')]);
        $entidad = Entidad::findOne(['ruc' => $empresa->ruc]);
        $model = TimbradoDetalle::find()
            ->joinWith('timbrado as timbrado')
            ->select([
                $tname . 'id',
                $tname . 'prefijo',
                $tname . 'nro_inicio',
                $tname . 'nro_fin',
                $tname . 'timbrado_id',
                $tname . 'tipo_documento_set_id',
                'timbrado.entidad_id',
            ])
            ->where([
                'timbrado.entidad_id' => $entidad->id,
                'tipo_documento_set_id' => $tipo_doc_set
            ])
            ->andWhere(['=', 'prefijo', $prefijo_usado])
            ->andWhere(['<=', 'nro_inicio', $nro])
            ->andWhere(['>=', 'nro_fin', $nro])
            ->one();

        $newNro = 1 + $nro;
        $model->nro_actual = $newNro;
        if (!$model->save()) {
            $_respuesta['resultado'] = false;
            $_respuesta['errores'] = $model->getErrorSummaryAsString();
        }

        return $_respuesta;
//
//        if (!$isExistentRecord) {
//            $svar = Yii::$app->session->get('cont_factura_venta_nro_actual');
//            foreach ($svar as $prefijo => $array) {
//                if ($prefijo == $prefijo_usado) {
//                    \Yii::$app->session->get('cont_factura_venta_nro_actual')[$prefijo]['nro_actual'] = $newNro;
//                    $t->commit();
//                    return true;
//                }
//            }
//        } else {
//            $t->commit();
//            return true;
//        }
//
//        $t->rollBack();
//        return false;
    }

    /* TODO: implementar caso de update, cuando se cambia de prefijo (y número de factura) */
    /* acá siempre se aumenta, no se aumenta/descuenta en caso de un cambio en el update */
    /**
     * Actualiza el nro_actual del timbrado detalle y actualiza el cache de sesion si y solo si
     * el parametro [[$action_id]] es [[create]].
     *
     * @param bool $isDeleting
     * @param string $action_id Es solamente para determinar si se va a actualizar en la sesion los datos
     * @return array
     */
    public function updateNroActualSession($isDeleting = false, $action_id = 'create')
    {
        ini_set('memory_limit', '-1');
        $_respuesta = ['resultado' => true, 'errores' => ""];
        $nro = (integer)$this->nro_factura;

        // Localizar timbradoDetalle usado por la venta actual.
        // TODO: Tal vez se pueda buscar por $this->timbrado_detalle_id nada mas y verificar si el (int)$this->nro_factura esta entre sus rangos.
        /** @var TimbradoDetalle $t_det */
        $t_det = TimbradoDetalle::findOne(['id' => $this->timbrado_detalle_id_prefijo]);

        // Calcular todos los numeros del rango del timbradoDetalle. range() devuelve un rango completamente cerrado.
        $all_nros = range($t_det->nro_inicio, $t_det->nro_fin);
        $nros_usados = [];

        // Obtener todos los nro_factura utilizados por ventas  con el mismo prefijo y timbrado.
        // Se quita el filtro por tipo de documento, porque es posible existen de diferentes tipos de documentos
        //  pero usan el mismo talonario o timbrado.
        $ventas_existentes = Venta::find()->alias('venta')
            ->leftJoin('cont_timbrado_detalle timdet', 'venta.timbrado_detalle_id = timdet.id')
//            ->where(['OR', ['venta.tipo_documento_id' => $this->tipo_documento_id], ['IS', 'venta.tipo_documento_id', null]])
            ->andWhere(['venta.prefijo' => $t_det->prefijo])
            ->andWhere(['!=', 'venta.estado', 'faltante'])
            ->andWhere(['timdet.id' => $t_det->id])
            ->orderBy(['venta.nro_factura' => SORT_ASC]);

        /** Si es create/update, el id de la venta debe formar parte de nros_usados. */
        /** Si es delete, el id de la venta debe formar parte de nros_libres. */

        if ($isDeleting)
            $ventas_existentes->andFilterWhere(['!=', 'venta.id', $this->id]); // desde create tambien puede tener id

        $ventas_existentes = $ventas_existentes->all();
        foreach ($ventas_existentes as $venta) {
            if (!in_array((int)$venta['nro_factura'], $nros_usados))
                $nros_usados[] = (int)$venta['nro_factura'];
        }

        // Para asegurar que si NO ES delete, es decir, es create/update, el nro de la venta forme parte de nros_usados.
        if (!in_array($nro, $nros_usados) && !$isDeleting) $nros_usados[] = $nro;

        // Obtener lista de numeros aun no utilizados. En teoria, la nva lista esta ordenada con respecto a los valores.
        $nros_libres = array_values(array_diff($all_nros, $nros_usados));

        // $nros_libres contiene todos los numeros que no aparecen en $nros_usados pero que estan en $all_nros.
        // $nros_libres es vacio si todos los valores de $all_nros estan en $nros_usados, es decir, se usaron todos los numeros.
        // Si $nros_libres es vacio, entonces el nro_actual del timDetalle deberia valer nro_fin + 1.
        // $nro es el nro_factura usado por la venta actual, donde se permite utilizar hasta el nro_fin del timDetalle.
        // Por eso, es equivalente hacer $nro + 1 que $t_det->nro_fin + 1 cuando $nros_libres es vacio.
        $next_nro = empty($nros_libres) ? ($nro + 1) : $nros_libres[0];

//        var_dump($all_nros);
//        echo '<br/>';
//        echo '<br/>';
//        var_dump($nros_usados);
//        echo '<br/>';
//        echo '<br/>';
//        var_dump($nros_libres);
//        exit();

        // Actualizar nro_actual del timDetalle solamente si el nro de la factura en cuestion es el mayor entre
        // todos los numeros de facturas que estan siendo cargandos/modificandos actualmente por varios usuarios y
        // si el nro_actual no es igual a nro_fin+1. Ahora se necesita contemplar rangos de numeros que no se cargaron
        // pero que aun estan disponibles para usarse. Entonces, el nro_actual va a seguir siendo nro_fin+1 pero
        // se va a permitir usar los rangos faltantes, sin actualizar el nro_actual a +1.
        if (($nro + 1) >= $t_det->nro_actual && $t_det->nro_actual != $t_det->nro_fin + 1) {
            $t_det->nro_actual = $nro + 1;
            if (!$t_det->save()) {
                $_respuesta['resultado'] = false;
                $_respuesta['errores'] = implode(', ', $t_det->getErrorSummary(true));

                return $_respuesta;
            }
        }

        // Actualizar en la sesion el sistema de cache de nro_factura si es create
        if ($action_id == 'create') {
            $data = [];
            $svar = 'cont_next_nro_factura';
            $sesion = Yii::$app->session;

            if (!$sesion->has($svar)) {
                $sesion->set($svar, $data);
            }

            $data = $sesion->get($svar);

            if ($t_det->nro_fin < $next_nro && array_key_exists($t_det->id, $data))
                unset($data[$t_det->id]);
            elseif ($t_det->nro_fin >= $next_nro) // solo poner si el next_nro es utilizable.
                $data[$t_det->id] = ($nro + 1) . '|' . $t_det->timbrado->nro_timbrado; // si existe clave $t_det->id lo reemplaza, sino se crea nueva entrada con esa clave
            $sesion->set($svar, $data);
        }

        return $_respuesta;
    }

    /** Usado en la retencion de valores usados para guardar y siguiente. */
    public function getNextNroFactura()
    {
        $svar = 'cont_next_nro_factura';
        $sesion = Yii::$app->session;

        $content = $sesion->get($svar);
        $nro = "";
        if (array_key_exists($this->timbrado_detalle_id_prefijo, $content)) {
            $data = $content[$this->timbrado_detalle_id_prefijo];
            $nro = explode('|', $data)[0];
        }

        return $nro;


        /** Implementacion anterior. Se dejara por un tiempo como backup. */
        $nro = (integer)$this->nro_factura;
        // Siempre encuentra un timbradodetalle.
        // Localizar timbrado utilizado por la factura actual.
        $tname = TimbradoDetalle::tableName() . '.';
        $empresa = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')]);
        $entidad = Entidad::findOne(['ruc' => $empresa->ruc]);
        $model = TimbradoDetalle::find()
            ->joinWith('timbrado as timbrado')
            ->select([
                $tname . 'id',
                $tname . 'prefijo',
                $tname . 'nro_inicio',
                $tname . 'nro_fin',
                $tname . 'timbrado_id',
                $tname . 'tipo_documento_set_id',
                'timbrado.entidad_id',
            ])
            ->where([
                'timbrado.entidad_id' => $entidad->id,
                'tipo_documento_set_id' => $this->tipoDocumento->tipo_documento_set_id
            ])
            ->andWhere(['=', 'prefijo', $this->prefijo])
            ->andWhere(['<=', 'nro_inicio', $nro])
            ->andWhere(['>=', 'nro_fin', $nro])->one();
        return $model->nro_actual;
    }

    public function beforeValidate()
    {
        if ($this->estado == 'faltante') {

            $this->fecha_emision = null;
        }

        if ($this->bloqueado == null)
            $this->bloqueado = 'no';

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    /** CREADO DESDE LA RETOMADA DE NOTAS DE CREDITO DEBITO */

    /**
     * @return TipoDocumentoSet
     * @throws Exception
     */
    public static function getTipodocSetNotaCredito()
    {
        // MIGUEL METIO EL ID DE TIPO DE DOCUMENTO PARA NOTA CREDITO/DEBITO DIRECTAMENTE EN LA MIGRACION DE SU TAREA.
        try {
            $nombre = 'nota_credito_set_id';
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parmsys)) throw new Exception("Falta definir a nivel de sistema (contabilidad) el
             parámetro por Tipo de Documento SET para Notas de crédito. Utilice 'nota_credito_set_id' como nombre.");
            return TipoDocumentoSet::findOne($parmsys->valor);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return TipoDocumentoSet|null
     * @throws Exception
     */
    public static function getTipodocSetNotaDebito()
    {
        // MIGUEL METIO EL ID DE TIPO DE DOCUMENTO PARA NOTA CREDITO/DEBITO DIRECTAMENTE EN LA MIGRACION DE SU TAREA.
        try {
            $nombre = 'nota_debito_set_id';
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parmsys)) throw new Exception("Falta definir a nivel de sistema (contabilidad) el
             parámetro por Tipo de Documento SET para Notas de débito. Utilice 'nota_debito_set_id' como nombre.");
            return TipoDocumentoSet::findOne($parmsys->valor);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return TipoDocumento|null
     * @throws Exception
     */
    public static function getTipodocNotaCredito()
    {
        try {
            if (($tdocNC = TipoDocumento::findOne(['asociacion' => 'cliente', 'para' => 'nota_credito'])) != null) {
                return $tdocNC;
            }
            $nombre = 'tipodoc_notacredito_venta_id';
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parmsys)) throw new Exception("Falta definir a nivel de sistema (contabilidad) el
             parámetro por Tipo de Documento para Notas de crédito de ventas.
              Utilice 'tipodoc_notacredito_venta_id' como nombre.");
            return TipoDocumento::findOne($parmsys->valor);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return TipoDocumento|null
     * @throws Exception
     */
    public static function getTipodocNotaDebito()
    {
        try {
            if (($tdocNC = TipoDocumento::findOne(['asociacion' => 'cliente', 'para' => 'nota_debito'])) != null) {
                return $tdocNC;
            }
            $nombre = 'tipodoc_notadebito_venta_id';
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parmsys)) throw new Exception("Falta definir a nivel de sistema (contabilidad) el
             parámetro por Tipo de Documento para Notas de débito de ventas.
              Utilice 'tipodoc_notadebito_venta_id' como nombre.");
            return TipoDocumento::findOne($parmsys->valor);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function formatDateForSave()
    {
        $slices = explode('-', $this->fecha_emision);
        if (strlen($slices[0]) == 2)
            $this->fecha_emision = implode('-', array_reverse($slices));
    }

    public function formatDateForViews()
    {
        $slices = explode('-', $this->fecha_emision);
        if (strlen($slices[0]) == 4)
            $this->fecha_emision = implode('-', array_reverse($slices));
    }

    public function isNotaCredito()
    {
        return $this->tipo == 'nota_credito';
    }

    public function isNotaDebito()
    {
        return $this->tipo == 'nota_debito';
    }

    public function isFactura()
    {
        return $this->tipo == 'factura';
    }

    /**
     * Verifica si entre los activos fijos que se retornaron por esta nota de credito
     * hay al menos uno que fue devuelto con nota de credito de compra.
     *
     * @return bool Retorna [[true]] si hay al menos un activo fijo retornado por esta nota de credito que fue devuelto
     * por otra nota de credito de compra. Retorna [[false]] en caso contrario o si este modelo no es nota de credito.
     */
    public function hayActFijosRetornadosDevueltos()
    {
        if (!$this->isNotaCredito()) return false;      // esta comprobacion debe estar antes de la evaluacion de los
        // managers porque solamente corresponde a notas de credito.

        $stockManagers = ActivoFijoStockManager::findAll(['venta_nota_credito_id' => $this->id]);
        foreach ($stockManagers as $stockManager) {
            $query = ActivoFijoStockManager::find()
                ->where(['activo_fijo_id' => $stockManager->activo_fijo_id])
                ->andWhere(['IS NOT', 'compra_nota_credito_id', null]);
            if ($query->exists())
                return true;
        }

        return false;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isNotaCreditoEditable()
    {
        if ($this->hayActFijosRetornadosDevueltos())
            throw new Exception("Uno o más activos fijos retornados por esta nota de crédito 
                fueron devueltos por notas de crédito de compra, por lo tanto no puede modificar.");

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isNotaCreditoDeleteable()
    {
        if ($this->hayActFijosRetornadosDevueltos())
            throw new Exception("Uno o más activos fijos retornados por esta nota de crédito 
                fueron devueltos por notas de crédito de compra, por lo tanto no puede eliminar.");

        return true;
    }

    /**
     * @return bool
     */
    public function isNotaDebitoDeleteable()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isNotaDebitoEditable()
    {
        return true;
    }

    /**
     * @param string|null $empresa_id
     * @param Venta|null $venta
     * @return array|\yii\db\ActiveRecord[]
     * @throws \Exception
     */
    public static function getObligacionesEmpresaActual($empresa_id = null, $venta = null)
    {
        $query = EmpresaObligacion::find()->alias('empresaO')
            ->joinWith('obligacion as ob')
            ->select(['id' => 'empresaO.obligacion_id', 'obligacion_id' => 'empresaO.obligacion_id', 'text' => 'ob.nombre'])
            ->where([
                'empresaO.empresa_id' => $empresa_id,
                'ob.para_factura' => 'si',
                'ob.estado' => 'activo'
            ]);
        if (isset($venta))
            $query->orFilterWhere(['empresaO.obligacion_id' => $venta->obligacion_id]);

        if (!$query->exists()) {
            throw new \Exception("La empresa actual no tiene asociado ninguna obligación ACTIVA o se han inactivado las obligaciones asociadas.");
        }

        $array = $query->asArray()->all();
        return $array;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboDetalles()
    {
        return $this->hasMany(ReciboDetalle::className(), ['factura_venta_id' => 'id']);
    }

    /**
     * @return PlantillaCompraventa[]
     */
    public function getPlantillasUsadas()
    {
        $plantillas = [];
        foreach ($this->ivasCuentaUsadas as $ivasCuentaUsada) {
            $plantillas[] = $ivasCuentaUsada->plantilla;
        }
        return $plantillas;
    }

    public function isPartidaCorrect()
    {
        if ($this->estado != 'vigente') return true;

        $return = true;
        if ($this->tipoDocumento->tipo_documento_set_id == self::getTipodocSetNotaCredito()->id) {
            $return = false;
            foreach ($this->detalleVentas as $detalle) {
                if ($detalle->cta_contable == 'haber' && $detalle->plan_cuenta_id == $this->tipoDocumento->cuenta_id) {
                    $return = true;
                    break;
                }
            }
        }
        return $return;
    }

    /**
     * @param null|string $empresa_id
     * @param null|string $periodo_id
     * @return string
     */
    public static function getMotivoNoVigente($empresa_id = null, $periodo_id = null)
    {
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-motivo_no_vigencia_venta";
        $parametroEmpresa = ParametroSistema::findOne(['nombre' => $nombre]);
        return isset($parametroEmpresa) ? $parametroEmpresa->valor : '';
    }

    public static function getEntidades2($params = [])
    {
        if (empty($params))
            return Entidad::find()->alias('entidad')
                ->innerJoin('cont_factura_venta venta', 'venta.entidad_id = entidad.id')
                ->all();
        else {
            $query = Entidad::find()->alias('entidad')
                ->innerJoin('cont_factura_venta venta', 'venta.entidad_id = entidad.id')
                ->select([
                    'id' => 'entidad.id',
                    'razon_social' => 'entidad.razon_social',
                ]);

            if (array_key_exists('empresa_id', $params))
                $query->where(['venta.empresa_id' => $params['empresa_id']]);

            if (array_key_exists('periodo_contable_id', $params))
                $query->andWhere(['venta.periodo_contable_id' => $params['periodo_contable_id']]);

            if (in_array('forSelect2', $params)) {
                $query = ArrayHelper::map($query->asArray()->all(), 'id', 'razon_social');
            } else {
                $query = $query->all();
            }
        }

        return isset($query) ? $query : null;
    }

    /**
     * @param bool $resolveSilently Whether throw exception or not when $saldo is inconsistent.
     * @throws \Exception
     */
    public function actualizarSaldo($resolveSilently = false)
    {
        if ($this->condicion == 'contado' || $this->tipoDocumento->condicion == 'contado') {
            if ($this->saldo < 0) {
                $this->saldo = 0;
                if (!$this->save(false)) {
                    throw new \Exception("Error interno corrigiendo saldo de factura {$this->getNroFacturaCompleto()}: {$this->getErrorSummaryAsString()}");
                }
            } elseif ($this->saldo > $this->total) {
                $this->saldo = $this->total;
                if (!$this->save(false)) {
                    throw new \Exception("Error interno corrigiendo saldo de factura {$this->getNroFacturaCompleto()}: {$this->getErrorSummaryAsString()}");
                }
            }
            return;
        }

        if ($this->condicion == 'credito') {
            $saldo = round($this->total, 2);
            $total = round($this->total, 2);

            foreach (ReciboDetalle::findAll(['factura_venta_id' => $this->id]) as $recDet) {
                $saldo -= round($recDet->monto, 2);
            }

            foreach (Retencion::findAll(['factura_venta_id' => $this->id]) as $ret) {
                $saldo -= round($ret->getTotalRetencion(), 2);
            }

            foreach (self::findAll(['factura_venta_id' => $this->id]) as $notaCred) {
                $saldo -= round($notaCred->total, 2);
                foreach (self::findAll(['factura_venta_id' => $notaCred->id]) as $notaDeb) {
                    $saldo += round($notaDeb->total, 2);
                }
            }

            if ($saldo < 0) {
                if ($resolveSilently)
                    $saldo = 0;
                else {
                    $saldo = number_format($saldo, 2, ',', '.');
                    throw new \Exception("No se pudo actualizar saldo de la factura {$this->getNroFacturaCompleto()}: El nuevo saldo es negativo: $saldo");
                }
            } elseif ($saldo > $total) {
                if ($resolveSilently)
                    $saldo = $total;
                else {
                    $saldo = number_format($saldo, 2, ',', '.');
                    throw new \Exception("No se pudo actualizar saldo de la factura {$this->getNroFacturaCompleto()}: El nuevo saldo es superior al total original: $saldo");
                }
            }
            $this->saldo = $saldo;
            if (!$this->save(false)) {
                throw new \Exception("No se pudo actualizar saldo de la factura {$this->getNroFacturaCompleto()}: {$this->getErrorSummaryAsString()}");
            }
        }
    }
}