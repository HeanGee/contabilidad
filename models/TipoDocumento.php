<?php

namespace backend\modules\contabilidad\models;

use yii\helpers\ArrayHelper;
use backend\models\BaseModel;
use backend\modules\contabilidad\helpers\ParametroSistemaHelpers;

/**
 * This is the model class for table "cont_tipo_documento".
 *
 * @property int $id
 * @property string $nombre
 * @property string $detalle
 * @property string $asociacion
 * @property string $estado
 * @property int $cuenta_id
 * @property string $condicion
 * @property int $tipo_documento_set_id
 * @property string $para_importacion
 * @property string $asiento_independiente
 * @property string $concepto_asiento_independiente
 * @property string $autofactura
 * @property string $solo_exento
 * @property string $para
 *
 * @property Venta[] $facturasVenta
 * @property Compra[] $facturasCompra
 * @property PlanCuenta $cuenta
 * @property TipoDocumentoSet $tipoDocumentoSet
 */
class TipoDocumento extends BaseModel
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'cont_tipo_documento';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['nombre', 'asociacion', 'estado', 'tipo_documento_set_id', 'para_importacion', 'condicion', 'autofactura',
				'solo_exento'], 'required'],
			[['asociacion', 'estado', 'condicion', 'para_importacion', 'asiento_independiente', 'concepto_asiento_independiente',
				'autofactura', 'solo_exento', 'para'], 'string'],
			[['cuenta_id', 'tipo_documento_set_id'], 'integer'],
			[['nombre'], 'string', 'max' => 45],
			[['detalle'], 'string', 'max' => 255],
			[['cuenta_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['cuenta_id' => 'id']],
			[['tipo_documento_set_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumentoSet::className(), 'targetAttribute' => ['tipo_documento_set_id' => 'id']],

			#Default values:
			[['estado'], 'default', 'value' => 'activo'],
			[['para_importacion'], 'default', 'value' => 'no'],
			[['asiento_independiente'], 'default', 'value' => 'no'],

			#Validators
			[['concepto_asiento_independiente'], 'required', 'when' => function ($model) {
				return $model->asiento_independiente == 'si';
			}, 'whenClient' => "function (attribute, value) {
                return $('#tipodocumento-asiento_independiente').val() === 'si';
            }"],

			//[['asociacion', 'para'], 'checkUniqueness'],
		];
	}

	/*public function checkUniqueness($attribute, $params)
	{
		if ($this->para != '' && $this->asociacion != '') {
			if (self::find()->where(['asociacion' => $this->asociacion, 'para' => $this->para])->exists())
				$this->addError($attribute,
					"La combinación de \"{$this->asociacion}\"-\"{$this->para}\" de {$this->getAttributeLabel('asociacion')} y {$this->getAttributeLabel('para')} ya ha sido utilizada.");
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'nombre' => 'Nombre',
			'detalle' => 'Detalle',
			'asociacion' => 'Asociacion',
			'estado' => 'Estado',
			'cuenta_id' => 'Cuenta',
			'condicion' => 'Condición',
			'tipo_documento_set_id' => 'Tipo Documento SET',
			'para' => "ES PARA",
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFacturasVenta() {
		return $this->hasMany(Venta::className(), ['tipo_documento_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCuenta() {
		return $this->hasOne(PlanCuenta::className(), ['id' => 'cuenta_id']);
	}


	public static function getValoresEnum($columna, $paraForm = false) {
		$_retornar = [];
		try {
			$valores_enum = TipoDocumento::getTableSchema()->columns[$columna]->enumValues;
			if ($paraForm) {
				$_retornar[] = ['id' => '', 'text' => ''];
				foreach ($valores_enum as $_v)
					$_retornar[] = ['id' => $_v, 'text' => strtoupper(str_replace('_', ' ', trim($_v)))];
			} else {
				$_retornar[''] = '';
				foreach ($valores_enum as $_v)
					$_retornar[$_v] = strtoupper(str_replace('_', ' ', trim($_v)));
			}

		} catch (\Exception $e) {
			/* TODO: manejar la excepción */
		}
		return $_retornar;
	}

	public static function getTiposDocumento($asociacion = '') {
		$query = TipoDocumento::find()->filterWhere(['asociacion' => $asociacion]);
		return ArrayHelper::map($query->asArray()->all(), 'id', 'nombre');
	}

	public static function getTiposDocumentoConId($asociacion = '', $id = null, $include_all = false) {
		$query = TipoDocumento::find()->alias('tipodoc')
			->leftJoin('cont_tipo_documento_set as docset', 'docset.id = tipodoc.tipo_documento_set_id')
			->filterWhere(['asociacion' => $asociacion])
			->select([
				'tipodoc.id as id',
				'tipodoc.autofactura as autofactura',
				'concat((tipodoc.id), (\' - \'), (tipodoc.nombre)) as text',
				'tipodoc.condicion as condicion',
				'tipodoc.tipo_documento_set_id as tipo_documento_set_id',
				'tipodoc.para_importacion as para_importacion',
				'docset.nombre as tipodocsetnombre',
				'docset.id as tipodocsetid',
				'tipodoc.solo_exento as solo_exento',
			]);
//        return ArrayHelper::map($query->asArray()->all(), 'id', 'text');
		$query->andFilterWhere(['tipodoc.estado' => 'activo']);
		if (!$include_all) $query->andWhere(['IS', 'tipodoc.para', null]);
		$query->orFilterWhere(['tipodoc.id' => $id]);
		return $query->asArray()->all();
	}

	public static function getTipoDocumentoParaNota($asociacion = '', $id = null, $para = 'nota_credito', $ajax = false) {
		\Yii::warning($asociacion);
		$query = self::find()->alias('tipodoc')
			->leftJoin('cont_tipo_documento_set as docset', 'docset.id = tipodoc.tipo_documento_set_id')
			->select([
				'tipodoc.id as id',
				'tipodoc.autofactura as autofactura',
				'concat((tipodoc.id), (\' - \'), (tipodoc.nombre)) as text',
				'tipodoc.condicion as condicion',
				'tipodoc.tipo_documento_set_id as tipo_documento_set_id',
				'tipodoc.para_importacion as para_importacion',
				'docset.nombre as tipodocsetnombre',
				'docset.id as tipodocsetid',
				'tipodoc.solo_exento as solo_exento',
			]);
//        return ArrayHelper::map($query->asArray()->all(), 'id', 'text');
		$query->filterWhere(['tipodoc.asociacion' => $asociacion]);
		$query->andFilterWhere(['tipodoc.para' => $para]);
		$query->andFilterWhere(['tipodoc.estado' => 'activo']);
		$query->orFilterWhere(['tipodoc.id' => $id]);
		return ($ajax ? ['results' => $query->asArray()->all()] : $query->asArray()->all());
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFacturasCompra() {
		return $this->hasMany(Venta::className(), ['tipo_documento_id' => 'id']);
	}

	public function getCondicion() {
		return $this->condicion;
	}

	public static function getCondiciones($forFilter = false) {
		$array = [];
		$i = 1;

		!$forFilter || $array['todos'] = (!$forFilter ? ($i++) . ' - ' : '') . 'Todos';
		$array['contado'] = (!$forFilter ? ($i++) . ' - ' : '') . 'Contado';
		$array['credito'] = (!$forFilter ? ($i++) . ' - ' : '') . 'Crédito';

		// more conditions to add goes here...
		return $array;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTipoDocumentoSet() {
		return $this->hasOne(TipoDocumentoSet::className(), ['id' => 'tipo_documento_set_id']);
	}

	public static function getLista($solo_activo = true, $asociacion) {
		$query = TipoDocumento::find();
		$query->select(['id', 'text' => 'nombre']);
		if ($solo_activo)
			$query->andWhere([
				'estado' => 'activo',
				'asociacion' => $asociacion,
			]);
		$query->orderBy(['nombre' => SORT_ASC]);
		return $query->asArray()->all();
	}

	/**
	 * @throws \Exception
	 */
	public function isDeleteable() {
		$compra = Compra::find()->where(['tipo_documento_id' => $this->id]);
		$venta = Venta::find()->where(['tipo_documento_id' => $this->id]);

		if ($compra->exists()) {
			throw new \Exception("Existen facturas de COMPRA que utiliza este tipo de documento.
                 Por tanto no puede eliminar");
		} elseif ($venta->exists()) {
			throw new \Exception("Existen facturas de VENTA que utiliza este tipo de documento.
                 Por tanto no puede eliminar");
		}

		return true;
	}

	public static function getIDsAsociadoSetNotaCredito() {
		$TDS = TipoDocumentoSet::findOne(['nombre' => 'NOTA DE CRÉDITO']);
		if (!isset($TDS)) return [];

		return ArrayHelper::map(
			TipoDocumento::find()->where(['IN', 'tipo_documento_set_id', $TDS->id])->asArray()->all(),
			'id', 'id'
		);
	}

	public function isNullTipoSet() {
		return $this->tipo_documento_set_id == '';
	}

	public function isTipoSetNinguno() {
		$tipoSetNinguno_id = ParametroSistemaHelpers::getValorByNombre('tipo_documento_set_ninguno');
		return !$this->isNullTipoSet() && $this->tipo_documento_set_id == $tipoSetNinguno_id;
	}
}
