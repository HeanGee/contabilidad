<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cont_empresa_obligacion".
 *
 * @property int $id
 * @property int $obligacion_id
 * @property string $empresa_id
 *
 * @property Obligacion $obligacion
 * @property Empresa $empresa
 */
class EmpresaObligacion extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_empresa_obligacion';
    }


    // TODO: borrar en el futuro si realmente no se necesita
    /**
     * @param $needle EmpresaObligacion|ActiveRecord
     * @param $array mixed
     * @return bool
     */
    public static function existObligacion($needle, $array)
    {
        /** @var EmpresaObligacion[] $obligacionesPorEmpresa */

        $obligacionesPorEmpresa = [];

        if ($array instanceof ActiveQuery) {
            $obligacionesPorEmpresa = $array->all();
        } elseif (is_array($array) && $array[0] instanceof EmpresaObligacion) {
            $obligacionesPorEmpresa = $array;
        } elseif (is_array($array)) { // no eficiente pero temporal
            foreach ($array as $key => $value) {
                $query = EmpresaObligacion::findOne(['obligacion_id' => $value]);
                if ($query != null) array_push($obligacionesPorEmpresa, $query);
            }
        }

        foreach ($obligacionesPorEmpresa as $obligacion) {
            if ($needle->empresa_id == $obligacion->empresa_id && $needle->obligacion_id == $obligacion->obligacion_id)
                return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['obligacion_id', 'empresa_id'], 'required'],
            [['obligacion_id', 'empresa_id'], 'integer'],
            [['obligacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Obligacion::className(), 'targetAttribute' => ['obligacion_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'obligacion_id' => 'Obligacion ID',
            'empresa_id' => 'Empresa ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObligacion()
    {
        return $this->hasOne(Obligacion::className(), ['id' => 'obligacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @param $obligacionesA EmpresaObligacion[]
     * @param $obligacionesB EmpresaObligacion[]
     * @return array El primer elemento es el conjunto de Obligaciones comunes y el segundo el de los no comunes.
     * El tercer elemento es el conjunto de Obligaciones no comunes de A y el cuarto el de los no comunes de B.
     */
    public static function getIntersectionsAndDivergents($obligacionesA, $obligacionesB)
    {
        /** @var EmpresaObligacion[] $commons */
        /** @var EmpresaObligacion[] $uncommons */
        /** @var EmpresaObligacion[] $uncommonsOfA */
        /** @var EmpresaObligacion[] $uncommonsOfB */

        $commons = [];
        $uncommons = [];
        $uncommonsOfA = [];
        $uncommonsOfB = [];

        foreach ($obligacionesA as $obligacionA) {
            if (EmpresaObligacion::findElement($obligacionA, $obligacionesB) != -1) {
                $commons[] = $obligacionA;
            } else {
                $uncommons[] = $obligacionA;
                $uncommonsOfA[] = $obligacionA;
            }
        }

        foreach ($obligacionesB as $obligacionB) {
            if (EmpresaObligacion::findElement($obligacionB, $obligacionesA) == -1) {
                $uncommons[] = $obligacionB;
                $uncommonsOfB[] = $obligacionB;
            }
        }

        return [$commons, $uncommons, $uncommonsOfA, $uncommonsOfB];
    }

    /**
     * @param $needle EmpresaObligacion
     * @param $obligaciones EmpresaObligacion[]
     * @return int|mixed|string
     */
    public static function findElement($needle, $obligaciones)
    {
        foreach ($obligaciones as $arrayIndex => $obligacion) {
            if ($needle->empresa_id == $obligacion->empresa_id && $needle->obligacion_id == $obligacion->obligacion_id)
                return $arrayIndex;
        }

        return -1;
    }
}
