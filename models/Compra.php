<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use backend\models\Moneda;
use backend\models\SessionVariables;
use backend\modules\contabilidad\helpers\ParametroSistemaHelpers;
use backend\modules\contabilidad\models\validators\CompraCotizacionValidator;
use backend\modules\contabilidad\models\validators\CompraNotaFechaValidator;
use backend\modules\contabilidad\models\validators\CompraTimbradoValidator;
use backend\modules\contabilidad\models\validators\FechaValidator;
use backend\modules\contabilidad\models\validators\NroFacturaValidator;
use bedezign\yii2\audit\models\AuditTrail;
use common\helpers\ValorHelpers;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cont_factura_compra".
 *
 * @property int $id
 * @property string $tipo_documento_id
 * @property string $fecha_emision
 * @property string $condicion
 * @property string $fecha_vencimiento
 * @property int $entidad_id
 * @property int $obligaciones
 * @property int $timbrado_id
 * @property string $nota_remision
 * @property string $total
 * @property string $saldo
 * @property string $nro_factura
 * @property int $moneda_id
 * @property int $empresa_id
 * @property float $cotizacion
 * @property int $cant_cuotas
 * @property string $nombre_documento
 * @property string $estado
 * @property string $ruc;
 * @property string $nombre_entidad;
 * @property int $timbrado_detalle_id
 * @property string $timbrado
 * @property string $asiento_id
 * @property int $periodo_contable_id
 * @property string $tipo
 * @property string $cotizacion_propia
 * @property int $factura_compra_id
 * @property int $importacion_id
 * @property string $creado
 * @property string $modificado
 * @property int $obligacion_id
 * @property string $para_iva
 * @property string $cedula_autofactura
 * @property string $nombre_completo_autofactura
 * @property string $bloqueado
 * @property string $creado_por
 * @property string $observaciones
 *
 * Para la vista:
 * @property string $exenta;
 * @property string $impuesto5;
 * @property string $impuesto10;
 * @property string $gravada5;
 * @property string $gravada10;
 * @property string $plantillas;
 * @property string $cotizacion_factura_asociada;
 *
 * @property DetalleCompra[] $detallesCompra
 * @property EmpresaPeriodoContable $periodoContable
 * @property Entidad $entidad
 * @property TipoDocumento $tipoDocumento
 * @property Moneda $moneda
 * @property Empresa $empresa
 * @property TimbradoDetalle $timbradoDetalle
 * @property Asiento $asiento
 * @property CompraIvaCuentaUsada[] $ivasCuentaUsadas
 * @property Retencion $retencion
 * @property Compra $facturaCompra
 * @property Compra[] $notas
 * @property Poliza $poliza
 * @property ActivoFijo[] $activoFijos
 * @property Prestamo[] $prestamos
 * @property ActivoBiologico[] $activosBiologicos
 * @property Importacion $importacion
 * @property Obligacion $obligacion
 * @property ReciboDetalle[] $reciboDetalles
 * @property PlantillaCompraventa[] $plantillasUsadas
 */
class Compra extends BaseModel
{
    public $nombre_documento;
    public $timbrado;
    public $ruc;
    public $nombre_entidad;
    const NRO_FACTURA_CORRECTO = 1;
    const NRO_FACTURA_DUPLICADO = 2;
    const NRO_FACTURA_NO_PREFIJO = 3;
    const NRO_FACTURA_NO_RANGO = 4;
    public $timbrado_id;
    public $_total_ivas;
    public $_total_gravadas;
    public $_iva_ctas_usadas = [];
    public $guardarycerrar = [];
    public $emision_factura;
    public $moneda_nombre;
    public $dv;
    public $razon_social;
    public $fecha;
    public $timbrado_vencido;
    public $cotizacion_factura_asociada; // mostrar cotizacion de la factura asociada

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_factura_compra';
    }

    public function behaviors()
    {
        $parentBehabiors = parent::behaviors();
        $behabiors = [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'creado',
                'updatedAtAttribute' => 'modificado',
                'value' => new Expression('NOW()'),
            ],
        ];

        return array_merge($parentBehabiors, $behabiors);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $controller = Yii::$app->controller->id;
        $esNota = isset($this->facturaCompra);
        return [
            [['tipo_documento_id', 'fecha_emision', 'condicion', 'entidad_id', 'total', 'moneda_id', 'empresa_id',
                'estado', 'ruc', 'nombre_entidad', 'para_iva', 'bloqueado'], 'required'],
            [['id', 'tipo_documento_id', 'entidad_id', 'obligaciones', 'cant_cuotas', 'empresa_id', 'timbrado_detalle_id', 'asiento_id', 'periodo_contable_id', 'factura_compra_id', 'importacion_id'], 'integer'],
            [['fecha_emision', 'fecha_vencimiento', 'nombre_documento', '_total_ivas', 'emision_factura',
                'moneda_nombre', 'guardarycerrar', 'cotizacion_propia', 'obligacion_id', 'para_iva', 'cedula_autofactura', 'nombre_completo_autofactura', 'creado_por', 'observaciones'], 'safe'],
            [['total', 'cotizacion', 'timbrado_vencido'], 'number'],
            [['condicion'], 'string', 'max' => 7],
//            [['nro_factura'], 'string', 'min' => 15, 'max' => 15],
            [['nota_remision'], 'string', 'min' => 15, 'max' => 15],
            [['estado', 'tipo', 'bloqueado', 'observaciones'], 'string',],
            [['id'], 'unique'],
            [['entidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entidad::className(), 'targetAttribute' => ['entidad_id' => 'id']],
            [['tipo_documento_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['tipo_documento_id' => 'id']],
            [['moneda_id'], 'exist', 'skipOnError' => false, 'targetClass' => Moneda::className(), 'targetAttribute' => ['moneda_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => false, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['timbrado_detalle_id'], 'exist', 'skipOnError' => true, 'targetClass' => TimbradoDetalle::className(), 'targetAttribute' => ['timbrado_detalle_id' => 'id']],
            [['asiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['importacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Importacion::className(), 'targetAttribute' => ['importacion_id' => 'id']],
//            [['retencion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Retencion::className(), 'targetAttribute' => ['retencion_id' => 'id']],
            [['cant_cuotas'], 'required', 'when' => function ($model) {
                return $model->condicion == 'credito';
            }, 'whenClient' => "function (attribute, value) {
                return $('#compra-condicion').val() == 'credito';
            }"],
            [['fecha_vencimiento'], 'required', 'when' => function ($model) {
                return $model->condicion == 'credito';
            }, 'whenClient' => "function (attribute, value) {
                return $('#compra-condicion').val() == 'credito';
            }"],
            ['fecha_emision', FechaValidator::className()],
            [['fecha_vencimiento'], 'fechaVenceValidator', 'when' => function ($model) {
                return $model->condicion == 'credito';
            }, 'skipOnEmpty' => false, 'skipOnError' => false],
            ['nro_factura', NroFacturaValidator::className()],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],
            [['timbrado_detalle_id', 'timbrado_id'], CompraTimbradoValidator::className()],

            [['creado', 'modificado'], 'safe'],
            [['cotizacion'], CompraCotizacionValidator::className()],
            [['fecha_emision'], CompraNotaFechaValidator::className(), 'when' => function ($model) {
                return Yii::$app->controller->id == 'compra-nota' && isset($model->factura_compra_id);
            }, 'whenClient' => "function (attribute, value) {
                return ('{$controller}') === 'compra-nota' && '{$esNota}';
            }"],
            [['cedula_autofactura', 'nombre_completo_autofactura'], 'required', 'when' => function ($model) {
                /** @var Compra $compra */
                $compra = $model;
                return $compra->tipo_documento_id != '' && $compra->tipoDocumento->autofactura == 'si';
            }, 'whenClient' => "function(attribute, value) {
                let tipoDocField = $('#compra-tipo_documento_id');
                if (tipoDocField.length) {
                    return (tipoDocField.val() !== '' && tipoDocField.select2('data')[0]['autofactura'] =='si');
                } else return false;
            }"],
            [['observaciones'], 'required', 'when' => function ($model) {
                return $model->estado != 'vigente';
            }, 'whenClient' => "function(attribute, value) {
                let estado = $(input[id$='estado']).val();
                return (estado !== 'vigente');
            }"],
            [['obligacion_id'], 'required', 'when' => function ($model) {
                return $model->factura_compra_id == '';
            }, 'whenClient' => "function(attribute, value) {
                return ($('#compra-factura_compra_id').val() === '');
            }"],
            [['saldo'], 'saldoValidator'],

            // documento unico
            [['entidad_id', 'tipo_documento_id', 'timbrado_detalle_id', 'nro_factura'], 'facturaUnicoValidator'],
        ];
    }
//
//    /**
//     * @return bool|string
//     * @throws \Exception
//     */
//    public static function getIDTipoDocumentoAutoFactura()
//    {
//        $queryTDoc = TipoDocumento::find()->where(['autofactura' => 'si']);
//        if ($queryTDoc->exists()) {
//            if ($queryTDoc->count('id') > 1)
//                throw new \Exception("Hay más de un Tipo de documento para AUTOFACTURA y es ambiguo");
//
//            return $queryTDoc->one()->id;
//        }
//
//        return '';
//    }

    public function facturaUnicoValidator($attribute, $params, $validator)
    {
        $timbrado = ($this->timbrado_detalle_id == '' ? "" : $this->timbradoDetalle->timbrado_id);
        $combined_val = [];
        $combined_attr = [];
        $query = self::find()->alias('factura')
            ->leftJoin('cont_timbrado_detalle timdet', 'timdet.id = factura.timbrado_detalle_id')
            ->leftJoin('cont_timbrado tim', 'tim.id = timdet.timbrado_id');

        if ($this->entidad_id == '') {
            $query->where(['IS', 'factura.entidad_id', null]);
        } else {
            $query->where(['=', 'factura.entidad_id', $this->entidad_id]);
            $combined_val[] = $this->ruc;
            $combined_attr[] = 'RUC';
        }

        if ($this->tipo_documento_id == '') {
            $query->andWhere(['IS', 'factura.tipo_documento_id', null]);
        } else {
            $query->andWhere(['=', 'factura.tipo_documento_id', $this->tipo_documento_id]);
            $combined_val[] = $this->tipoDocumento->nombre;
            $combined_attr[] = 'Tipo de Documento';
        }

        if ($timbrado == '') {
            $query->andWhere(['IS', 'factura.timbrado_detalle_id', null]);
        } else {
            $query->andWhere(['=', 'tim.id', $timbrado]);
            $combined_val[] = $this->timbradoDetalle->timbrado->nro_timbrado;
            $combined_attr[] = "Timbrado";
        }

        if ($this->nro_factura == '') {
            $query->andWhere(['IS', 'factura.nro_factura', null]);
        } else {
            $query->andWhere(['=', 'factura.nro_factura', $this->nro_factura]);
            $combined_val[] = $this->nro_factura;
            $combined_attr[] = 'Nro Factura';
        }

        $query->andFilterWhere(['!=', 'factura.id', $this->id]);
        if ($query->exists()) {
            $combined_val = "\"" . implode('", "', $combined_val) . '"';
            $combined_attr = "\"" . implode('", "', $combined_attr) . '"';
            $this->addError('nro_factura', "La combinación de $combined_val para $combined_attr ya fue utilizado");
        }
    }

    public static function parametrosAutofacturaExists()
    {
        try {
            $empresa_id = Yii::$app->session->get('core_empresa_actual');
            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');

            $salarioMin = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-salario_minimo";
            $cantSalMin = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cantidad_salario_min";

            $parm = ParametroSistema::findOne(['nombre' => $salarioMin]);
            if (isset($parm)) {
//                $parm = ParametroSistema::findOne(['nombre' => $cantSalMin]);
//                if (isset($parm))
                return true;
            } else {
                return false;
            }
        } catch (\Exception $exception) {
            Yii::warning($exception->getMessage());
        }
        return false;
    }

    public static function getSalarioMinimoEmpresaActual()
    {
        if (!self::parametrosAutofacturaExists()) return false;

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

        $salarioMin = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-salario_minimo";

        $parm = ParametroSistema::findOne(['nombre' => $salarioMin]);
        return $parm->valor;
    }

    public static function getCantSalarioMinAutofactura()
    {
        if (!self::parametrosAutofacturaExists()) return false;

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

        $cantSalMin = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cantidad_salario_min";

        $parm = ParametroSistema::findOne(['nombre' => $cantSalMin]);
        return $parm->valor;
    }

    public function nroFacturaValidator($attribute, $params)
    {
        $value = $this->$attribute;
        if (strlen(str_replace('_', '', $value)) < 15) {
            $this->addError($attribute, 'Nro de factura inválido');
        }
    }

    public function fechaVenceValidator($attribute, $params)
    {
        $emision = $this->fecha_emision;
        $vencimi = $this->$attribute;

        $slicesEmision = explode('-', $emision);
        $slicesVencimi = explode('-', $vencimi);

        if (strlen($slicesEmision[2]) > 2)
            $emision = implode('-', array_reverse($slicesEmision));
        if ($this->$attribute != '' && strlen($slicesVencimi[2]) > 2)
            $vencimi = implode('-', array_reverse($slicesVencimi));

        if (strtotime($vencimi) < strtotime($emision)) {
            Yii::warning("emi: $emision, ven: $vencimi");
            $this->addError($attribute, $this->getAttributeLabel($attribute) . ' no puede ser una fecha pasada.');
        }
    }

    public function saldoValidator($attribute, $params)
    {
        if (round($this->saldo, 2) < 0)
            $this->addError($attribute, "{$this->getAttributeLabel($attribute)} no puede ser negativo");
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return self::getAttributesLabelText();
    }

    public static function getAttributesLabelText()
    {

        return [
            'id' => 'ID',
            'tipo_documento_id' => 'Tipo de Documento',
            'fecha_emision' => 'Fecha Emisión',
            'condicion' => 'Condicion',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'entidad_id' => 'Proveedor',
            'empresa_id' => 'Empresa',
            'obligaciones' => 'Obligaciones',
            'nro_factura' => 'Nro. de Factura',
            'total' => 'Total Factura',
            'saldo' => 'Saldo Factura',
            'moneda_id' => 'Moneda',
            'cotizacion' => 'Cotización',
            'nota_remision' => 'Nota de Remisión Nro',
            'timbrado' => 'Timbrado',
            'estado' => 'Estado',
            'nombre_entidad' => 'Entidad (Proveedor)',
            'timbrado_id' => 'Timbrado',
            '_total_ivas' => 'Total I.V.A.',
            'tipo' => 'Tipo',
            'factura_compra_id' => 'Factura',
            'emision_factura' => 'Emisión Factura',
            'moneda_nombre' => 'Moneda',
            'cotizacion_propia' => 'Cotizacion Propia?',
            'periodo_contable_id' => "Periodo Contable Id",
            'timbrado_detalle_id' => "Timbrado Detalle Id",
            'cant_cuotas' => "Cantidad de Cuotas",
            'prestamo_detalle_id' => 'Prestamo Detalle ID',
            'asiento_id' => "Asiento ID",
            'creado' => "Creado",
            'modificado' => "Última modificación",
            'obligacion_id' => "Obligación",
            'para_iva' => 'Para I.V.A.?',
            'cotizacion_factura_asociada' => "Cotización Factura",
        ];
    }

    public function getAuditTrails()
    {
        return AuditTrail::find()
            ->orOnCondition([
                'audit_trail.model_id' => $this->id,
                'audit_trail.model' => get_class($this),
            ])
            ->orOnCondition([
                'audit_trail.model_id' => ArrayHelper::map($this->getDetallesCompra()->all(), 'id', 'id'),
                'audit_trail.model' => get_class(new DetalleCompra()),
            ]);
    }

    public function getDetallesId()
    {
        $ids = [];
        foreach ($this->detallesCompra as $item) {
            $ids[] = $item->id;
        }
        return $ids;
    }

    public static function manejarTextoAuditoria($campo, &$viejo, &$nuevo)
    {
        switch ($campo) {
            case 'tipo_documento_id':
                $viejo[0] = !empty($viejo[0]) ? TipoDocumento::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? TipoDocumento::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'entidad_id':
                $viejo[0] = !empty($viejo[0]) ? Entidad::findOne($viejo[0])->razon_social : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Entidad::findOne($nuevo[0])->razon_social : $nuevo[0];
                return true;
            case 'moneda_id':
                $viejo[0] = !empty($viejo[0]) ? Moneda::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Moneda::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'empresa_id':
                $viejo[0] = !empty($viejo[0]) ? Empresa::findOne($viejo[0])->razon_social : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Empresa::findOne($nuevo[0])->razon_social : $nuevo[0];
                return true;
            case 'timbrado_detalle_id':
                $viejo[0] = !empty($viejo[0]) ? TimbradoDetalle::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? TimbradoDetalle::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'asiento_id':

                if (!empty($viejo[0])) {
                    $asiento = Asiento::findOne($viejo[0]);
                    if (isset($asiento)) {
                        $viejo[0] = $asiento->id;
                    }
                }

                if (!empty($nuevo[0])) {
                    $asiento = Asiento::findOne($nuevo[0]);
                    if (isset($asiento)) {
                        $nuevo[0] = $asiento->id;
                    }
                }
                return true;
            case 'periodo_contable_id':
                $viejo[0] = !empty($viejo[0]) ? EmpresaPeriodoContable::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? EmpresaPeriodoContable::findOne($nuevo[0])->id : $nuevo[0];
                return true;
            case 'factura_compra_id':
                $viejo[0] = !empty($viejo[0]) ? self::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? self::findOne($nuevo[0])->id : $nuevo[0];
                return true;
        }
        return false;
    }

    public function getExenta()
    {
        $exenta = 0.0;
        $ivaCtaUsadas = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (!isset($iva_cta_usada->ivaCta)) {
                $exenta += (float)$iva_cta_usada->monto;
            }
        }
        return $exenta;
    }

    public function getGravada5()
    {
        $gravada = 0.0;
        $ivaCtaUsadas = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->ivaCta) && $iva_cta_usada->ivaCta->iva->porcentaje == 5) {
                $gravada += (float)$iva_cta_usada->monto / 1.05;
            }
        }
        return $gravada;
    }

    public function getGravada10()
    {
        $gravada = 0.0;
        $ivaCtaUsadas = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->ivaCta) && $iva_cta_usada->ivaCta->iva->porcentaje == 10) {
                $gravada += (float)$iva_cta_usada->monto / 1.1;
            }
        }
        return $gravada;
    }

    public function getImpuesto5()
    {
        $impuesto5 = 0.0;
        $ivaCtaUsadas = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->ivaCta) && $iva_cta_usada->ivaCta->iva->porcentaje == 5) {
                $impuesto5 += (float)$iva_cta_usada->monto - (float)$this->gravada5;
            }
        }
        return $impuesto5;
    }

    public function getImpuesto10()
    {
        $impuesto10 = 0.0;
        $ivaCtaUsadas = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $this->id]);
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->ivaCta) && $iva_cta_usada->ivaCta->iva->porcentaje == 10) {
                $impuesto10 += (float)$iva_cta_usada->monto - (float)$this->gravada10;
            }
        }
        return $impuesto10;
    }

    public function getPlantillas()
    {
        $ivaCtaUsadas = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $this->id]);
        $plantillaNombres = [];
        foreach ($ivaCtaUsadas as $iva_cta_usada) {
            if (isset($iva_cta_usada->plantilla) && !in_array($iva_cta_usada->plantilla->nombre, $plantillaNombres)) {
                $plantillaNombres[] = $iva_cta_usada->plantilla->nombre;
            }
        }
        return implode(', ', $plantillaNombres) . (sizeof($plantillaNombres) ? '.' : '');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetallesCompra()
    {
        return $this->hasMany(DetalleCompra::className(), ['factura_compra_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoFijos()
    {
        return $this->hasMany(ActivoFijo::className(), ['factura_compra_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidad()
    {
        return $this->hasOne(Entidad::className(), ['id' => 'entidad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestamos()
    {
        return Prestamo::find()
            ->alias('prestamo')
            ->joinWith('prestamoDetalles as detalle')
            ->leftJoin('cont_prestamo_detalle_compra as cuota_compra', 'detalle.id = cuota_compra.prestamo_detalle_id')
            ->where(['cuota_compra.factura_compra_id' => $this->id]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    // extranhamente devuelve el id o los ids si no se hace ->all()
    public function getActivosBiologicos()
    {
        return ActivoBiologico::find()->alias('abio')
            ->leftJoin('cont_activo_biologico_stock_manager AS stockMan',
                'abio.especie_id = stockMan.especie_id
                 AND abio.clasificacion_id = stockMan.clasificacion_id
                 AND abio.empresa_id = stockMan.empresa_id
                 AND abio.periodo_contable_id = stockMan.periodo_contable_id')
            ->where([
                // decidi cambiar de $_SESSION[...] a $this->empresa_id y $this->periodo_contable_id
                // porque es mas coherente filtrar por activos biologicos de misma empresa y periodo
                // contable que la de compra.
                'abio.empresa_id' => $this->empresa_id,
                'abio.periodo_contable_id' => $this->periodo_contable_id,
                'stockMan.factura_compra_id' => $this->id
            ])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRetencion()
    {
        return $this->hasOne(Retencion::className(), ['factura_compra_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneda()
    {
        return $this->hasOne(Moneda::className(), ['id' => 'moneda_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumento()
    {
        return $this->hasOne(TipoDocumento::className(), ['id' => 'tipo_documento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObligacion()
    {
        return $this->hasOne(Obligacion::className(), ['id' => 'obligacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsiento()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Compra::className(), ['factura_compra_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoliza()
    {
        return Poliza::find()->where(['factura_compra_id' => $this->id]);
    }

    /**
     * @return string
     */
    public function getTotal()
    {
        return ValorHelpers::numberFormat($this->total, $this->moneda->tieneDecimales() ? 2 : 0);
    }

    /**
     * @return string
     */
    public function getSaldo()
    {
        return ValorHelpers::numberFormat($this->saldo, $this->moneda->tieneDecimales() ? 2 : 0);
    }

    public static function getCondiciones()
    {
        return ['todos' => 'Todos', 'contado' => 'Contado', 'credito' => 'Crédito'];
    }

	public static function getMonedas($onlyIdAndText = false, $forFilter = true)
    {
        $query = Moneda::find()->orderBy('id ASC');
        $query_sentence = ['id', "CONCAT((id), (' - '), (nombre)) AS text", 'tiene_decimales'];
        if ($onlyIdAndText) {
            $query_sentence = ['id', "nombre AS text"];
	        if ($forFilter) $array['todos'] = 'Todos';
            $map = ArrayHelper::map($query->select($query_sentence)->asArray()->all(), 'id', 'text');
            foreach ($map as $key => $value) {
                $array[$key] = $value;
            }
            return $array;
        }
        return $query->select($query_sentence)->asArray()->all();
    }

    public static function getEstados($filterMode = false)
    {
        $array = [];
        $i = 1;
        if ($filterMode) $array['todos'] = ($i++) . ' - Todos';
        $array['vigente'] = ($i++) . ' - Vigente';
        // more states to add goes here...
        return $array;
    }

    public static function checkNroFactura($nro, $idToExclude = null, $tipo = 'factura', $entidad_id = null)
    {
        /** @var TimbradoDetalle[] $detalles */
        $nro_factura = $nro;
        $prefijo = substr($nro, 0, 7);
        $nro_string = substr($nro, 8, 15);
        $nro = (integer)$nro_string;
        $detalles = TimbradoDetalle::findAll(['prefijo' => $prefijo]);

        if (sizeof($detalles) == 0) {
            return Compra::NRO_FACTURA_NO_PREFIJO;
        }

        $compra = Compra::find()->where([
            'nro_factura' => $nro_factura,
            'tipo' => $tipo
        ])->andWhere(['entidad_id' => $entidad_id]);

        $compra->andFilterWhere(['!=', 'id', $idToExclude]); // excluir a sí mismo si es compra/update

        if ($compra->exists()) {
            return Compra::NRO_FACTURA_DUPLICADO;
        }

        $noExiste = true;
        $detalle_id = -1;
        foreach ($detalles as $d) {
            if ($nro >= $d->nro_inicio && $nro <= $d->nro_fin) {
                $noExiste = false;
                $detalle_id = $d->id;
                break;
            }
        }
        if ($noExiste) {
            return Compra::NRO_FACTURA_NO_RANGO;
        }

        return Compra::NRO_FACTURA_CORRECTO;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimbradoDetalle()
    {
        return $this->hasOne(TimbradoDetalle::className(), ['id' => 'timbrado_detalle_id']);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getFechaEmision()
    {
        return Yii::$app->formatter->asDate($this->fecha_emision, 'd-MM-Y');
    }

    public function getTimbrado()
    {
        return Timbrado::findOne(['id' => $this->timbrado_id])->nro_timbrado;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIvasCuentaUsadas()
    {
        return $this->hasMany(CompraIvaCuentaUsada::className(), ['factura_compra_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboDetalles()
    {
        return $this->hasMany(ReciboDetalle::className(), ['factura_compra_id' => 'id']);
    }

    public static function getEntidades()
    {
        $tname = Compra::tableName();
        $query = Compra::find();
        $query->joinWith('entidad as entidad');
        $query->select([
            $tname . '.entidad_id',
            'entidad.id AS id',
            'entidad.razon_social AS text'
        ]);
        return ArrayHelper::map($query->asArray()->all(), 'id', 'text');
    }

    public static function getEntidadesEmpresaPeriodoActual()
    {
        $tname = Compra::tableName();
        $query = Compra::find();
        $query->joinWith('entidad as entidad');
        $query->select([
            "MIN($tname.entidad_id) as entidad_id",
            "MIN(entidad.id) AS id",
            "CONCAT(MIN(entidad.ruc), (' - '), MIN(entidad.razon_social)) AS text"
        ]);
        $query->where([
            "{$tname}.empresa_id" => Yii::$app->session->get(SessionVariables::empresa_actual),
            "{$tname}.periodo_contable_id" => Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $query->groupBy('entidad_id');
        $query->orderBy(['CAST(entidad.ruc AS UNSIGNED)' => SORT_ASC]);
        return $query->asArray()->all();
    }

    /**
     * @param bool $doMap
     * @param array $ids
     * @param string $condicion
     * @param bool $concatRazonSocial
     * @param bool $sinRetenciones
     * @param bool $saldoGreaterThanZero
     * @param string $entidad_id
     * @param null $fecha Si está definido, lo usa en el filtro de cotizacion
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFacturas(
        $doMap = false, $ids = [], $condicion = null /*el cliente quiso que no predetermine a credito*/, $concatRazonSocial = false, $sinRetenciones = false,
        $saldoGreaterThanZero = false, $entidad_id = "", $fecha_hasta = null)
    {
        $tname = Compra::tableName();
        $query = Compra::find();
        $concat = $tname . '.nro_factura';
        if ($concatRazonSocial) $concat = "CONCAT((" . $tname . ".nro_factura), ('-'), (' \('), (entidad.razon_social), ('\)'))";
        $query->joinWith('moneda as moneda');
        $query->leftJoin('core_cotizacion AS cotizacion', 'cotizacion.moneda_id = moneda.id');
        $query->joinWith('entidad as entidad');
        $query->leftJoin('cont_recibo_detalle as recibo_detalle', 'recibo_detalle.factura_compra_id = ' . $tname . '.id');
        $query->select([
            'MIN(' . $tname . '.id) as id',
            $concat . ' as text',
            'MIN(' . $tname . '.moneda_id) as moneda_id',
            'MIN(' . $tname . '.entidad_id) as entidad_id',
            'MIN(moneda.nombre) as moneda_nombre',
            'MIN(' . $tname . '.saldo) as saldo',
            'MIN(' . $tname . '.cotizacion) as valor_moneda',
            'MIN(entidad.razon_social) AS razon_social',
            'MIN(entidad.ruc) AS ruc',
            'MIN(cotizacion.compra) as compra',
            'MIN(cotizacion.venta) as venta',
            'MIN(' . $tname . '.fecha_emision) as fecha_emision',
        ]);

        // cotizacion de 1 dia anterior a la fecha_hasta o de ayer
        $fecha_cotizacion = isset($fecha_hasta) ?
            date('Y-m-d', strtotime('-1 day', strtotime($fecha_hasta))) : date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $query->where(
            ['OR',
                ['moneda.id' => 1],
                ['AND',
                    ['!=', 'moneda.id', 1],
                    ['=', 'cotizacion.fecha', date('Y-m-d', strtotime($fecha_cotizacion))]
                ]
            ]
        );
        $query->andWhere([
            $tname . '.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            $tname . '.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            $tname . '.estado' => 'vigente',
        ]);

        if ($saldoGreaterThanZero) $query->andWhere(['>', $tname . '.saldo', 0]);
        $query->andFilterWhere([$tname . '.condicion' => $condicion,]);
        $query->andFilterWhere(['<=', $tname . '.fecha_emision', $fecha_hasta]);
        if (sizeof($ids) > 0) {
            $query->andWhere(['NOT IN', $tname . '.id', $ids]);
        }
        if ($sinRetenciones) {
            $compraConRetencionesIDs = [];
            $retenciones = Retencion::findAll(['!=', 'factura_compra_id', null]);
            foreach ($retenciones as $retencion) {
                $compraConRetencionesIDs[] = $retencion->id;
            }
            $query->andWhere(['NOT IN', 'id', $compraConRetencionesIDs]);
        }
        $query->andFilterWhere(['entidad_id' => $entidad_id]);
        $query->groupBy($tname . '.id');
        return $doMap ? ArrayHelper::map($query->asArray()->all(), 'id', 'text') : $query->asArray()->all();
    }

    public static function getFacturasListaByRucForNotas($ruc, $q = null, $tipo = 'factura')
    {
        $entidad = Entidad::findOne(['ruc' => $ruc]);
        $resultado = ['results' => ['id' => '', 'text' => '']];

        if (!isset($entidad)) {
            return $resultado;
        }

        $columnas = [
            'factura.`id`',
            '`text`' => "CONCAT((factura.`nro_factura`), (' ('), (tipodoc.nombre), (')'))",
            '`fecha_emision`' => 'DATE_FORMAT(factura.`fecha_emision`, \'%d-%m-%Y\')',
            '`moneda_nombre`' => '`moneda`.`nombre`',
            '`moneda_id`' => '`moneda`.`id`',
            '`moneda`.`tiene_decimales`',
            '`factura`.`cotizacion`',
            '`factura`.`nota_remision`',
            '`factura`.`tipo_documento_id`',
            '`factura`.`obligacion_id`'
        ];
        $tidoset_notacredito = self::getTipodocSetNotaCredito();
        $tidoset_notadebito = self::getTipodocSetNotaDebito();
        $consulta = self::find()
            ->alias('factura')
            ->joinWith('moneda AS moneda', false)
            ->leftJoin('cont_tipo_documento tipodoc', "tipodoc.id = factura.tipo_documento_id")
            ->where([
                'factura.`empresa_id`' => Yii::$app->session->get('core_empresa_actual'),
                'factura.`periodo_contable_id`' => Yii::$app->session->get('core_empresa_actual_pc'), // TODO: preguntar si tambien puede hacer notas por facturas de otro periodo.
                'factura.`entidad_id`' => $entidad->id,
                'factura.`tipo`' => $tipo
            ])
//            ->andWhere(['>', 'factura.`saldo`', '0']) // no solo facturas de credito
            ->andFilterWhere(['LIKE', 'factura.`nro_factura`', $q]);
        if ($tipo == 'nota_credito') {
            $consulta->joinWith('facturaCompra AS fac_original', false);
            $consulta->andFilterWhere(['=', 'tipodoc.tipo_documento_set_id', $tidoset_notacredito->id]); // solo notas de credito, segun tipo doc set
            $columnas['`fecha_emision_factura_original`'] = 'DATE_FORMAT(fac_original.`fecha_emision`, \'%d-%m-%Y\')';
        } else {
            $consulta->andFilterWhere(['!=', 'tipodoc.tipo_documento_set_id', $tidoset_notacredito->id]); // excluir notas de credito
        }
        $consulta->select($columnas);

        $filas = $consulta->asArray()->all();
        if (!empty($filas))
            $resultado['results'] = array_values($filas);
        // quitar del array aquellas facturas cuyo ivacuentausada tiene null en la columna de plantillas.
        // este problema es debido a la nueva implementacion de facturas multiplantillas, donde al agregar la columna
        // de plantilla en ivaCtaUsada, los registros que ya estaban quedaron con null en esa columna.
        if (!empty($filas))
            foreach ($resultado['results'] as $index => $factura) {
                Yii::error(print_r($factura, true));
                $query = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $factura['id']]);
                foreach ($query as $ivaCtaUsada) {
                    if (!isset($ivaCtaUsada->plantilla)) {
                        unset($resultado['results'][$index]);
                    }
                }
            }
        $resultado['results'] = array_values($resultado['results']);
        return $resultado;

    }

    public static function actionGetDatosFacturasByIdForNotas($factura_id, $tipo = 'nota_credito', $action = 'create', $nota_id = null)
    {
        $respuesta = [];

        // Miercoes 13, abril 2019: Segun correo de Magali, se desea poder modificar la factura asociada.
        // Se va a dejar en comentario, la implementacion previa al nuevo, debajo de esta nueva implementacion.
        $factura = self::findOne($factura_id);
        if (!empty($factura)) {
            foreach ($factura->ivasCuentaUsadas as $_ivaCta)
                $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] = $_ivaCta->monto;
        }

        //Asumiendo que hay una sola nota de credito/debido por documento
        if (!empty($factura->notas)) {
            foreach ($factura->notas as $_nota) {
                foreach ($_nota->ivasCuentaUsadas as $_ivaCta) {
                    $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] -= (float)$_ivaCta->monto;
                }
            }
        }

        if ($action == 'update') {
            $factura_nota_id = Compra::findOne(['id' => $nota_id])->factura_compra_id;
            if ($factura_id == $factura_nota_id) #casualmente ocurre el id de factura recibido por ajax es 'null' y el id obtenido mediante consulta al model de nota tambien es '', cuando se elimina la factura seleccionada.
                foreach (Compra::findOne($nota_id)->ivasCuentaUsadas as $_ivaCta) {
                    $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] = $_ivaCta->monto;
                }
        }

        // DEJANDO COMO BACKUP LA IMPLEMENTACION ANTERIOR AL MIERCOLES 13 ABRIL 2019
//        if ($action == 'create') {
//            $factura = self::findOne($factura_id);
//            if (!empty($factura)) {
//                foreach ($factura->ivasCuentaUsadas as $_ivaCta)
//                    $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] = $_ivaCta->monto;
//            }
//            if (!empty($factura->notas)) {
//                foreach ($factura->notas as $_nota) {
//                    foreach ($_nota->ivasCuentaUsadas as $_ivaCta) {
//                        $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] -= (float)$_ivaCta->monto;
//                    }
//                }
//            }
//        } elseif ($action == 'update') {
//            foreach (Compra::findOne($nota_id)->ivasCuentaUsadas as $_ivaCta)
//                $respuesta[$_ivaCta->plantilla_id][(!empty($_ivaCta->ivaCta) ? $_ivaCta->ivaCta->iva->porcentaje : 0)] = $_ivaCta->monto;
//        }

        return $respuesta;
    }

    public function hasPlantilla($id_plantilla)
    {
        return PlantillaCompraventa::find()->where(['id' => $id_plantilla])->exists();
    }

    public function hasCuenta($id_plantilla, $id_cuenta)
    {
        return PlantillaCompraventaDetalle::find()->where(['plantilla_id' => $id_plantilla, 'p_c_gravada_id' => $id_cuenta])->exists();
    }

    public static function getCotizacionPropiaOptions()
    {
        $result = [];
        $result[] = ['id' => 'si', 'text' => 'Si'];
        $result[] = ['id' => 'no', 'text' => 'No'];
        return $result;
    }

    /**
     * @param bool $local //Indica si se quieren las facturas locales o las del exterior
     * @param bool $doMap
     * @param array $ids
     * @param $importacionId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFacturasByImportacion($local, $importacionId, $doMap = false, $ids = [])
    {
        //TODO por ahora las facturas son contado y credito
        /*if ($local)
            $condicion = 'contado';
        else
            $condicion = 'credito';*/

        $tname = Compra::tableName();
        $query = Compra::find();
        $concat = "CONCAT((" . $tname . ".nro_factura), ('-'), (' \('), (entidad.razon_social), ('\)'), ' - ', moneda.simbolo)";
        $query->joinWith('moneda as moneda');
        $query->leftJoin('core_cotizacion AS cotizacion', 'cotizacion.moneda_id = moneda.id');
        $query->joinWith('entidad as entidad');
        $query->leftJoin('cont_recibo_detalle as recibo_detalle', 'recibo_detalle.factura_compra_id = ' . $tname . '.id');
        $query->joinWith('tipoDocumento as tipo_documento');
        $query->select([
            'MIN(' . $tname . '.id) as id',
            $concat . ' as text',
            'MIN(' . $tname . '.moneda_id) as moneda_id',
            'MIN(' . $tname . '.entidad_id) as entidad_id',
            'MIN(' . $tname . '.tipo_documento_id) as tipo_documento_id',
            'MIN(moneda.nombre) as moneda_nombre',
            'MIN(' . $tname . '.saldo) as saldo',
            'MIN(' . $tname . '.cotizacion) as cotizacion',
            'MIN(entidad.razon_social) AS razon_social',
            'MIN(entidad.ruc) AS ruc',
            'MIN(cotizacion.compra) as compra',
            'MIN(cotizacion.venta) as venta',
            'MIN(' . $tname . '.fecha_emision) as fecha_emision',
            'MIN(' . $tname . '.total) as monto',
        ]);

        $query->andWhere([
            $tname . '.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            $tname . '.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            $tname . '.estado' => 'vigente',
        ]);

        //$query->andFilterWhere([$tname . '.condicion' => $condicion,]);
        if (sizeof($ids) > 0) {
            $query->andWhere(['NOT IN', $tname . '.id', $ids]);
        }

        if (!$local) {
            $query->andWhere(['tipo_documento.para_importacion' => 'si']);
        }

        if ($importacionId != null)
            $query->andWhere([
                    'OR',
                    ['IS', $tname . '.importacion_id', null],
                    ['AND',
                        ['IS NOT', $tname . '.importacion_id', null],
                        ['=', $tname . '.importacion_id', $importacionId]
                    ]
                ]
            );

        $query->groupBy($tname . '.id');
        $query->orderBy(['entidad.razon_social' => SORT_ASC]);

        return $doMap ? ArrayHelper::map($query->asArray()->all(), 'id', 'text') : $query->asArray()->all();
    }

    public static function getPlantillaFacturasByImportacion($local, $importacionId, $doMap = false, $seleccionados = [])
    {
        $query = PlantillaCompraventa::find();
        $query->leftJoin('cont_factura_compra_iva_cuenta_usada as usada', 'usada.plantilla_id = cont_plantilla_compraventa.id');
        $query->leftJoin('cont_factura_compra as compra', 'compra.id = usada.factura_compra_id');
        $query->leftJoin('cont_entidad as entidad', 'compra.entidad_id = entidad.id');
        $query->leftJoin('core_moneda as moneda', 'compra.moneda_id = moneda.id');
        $query->select([
            'concat(\'plantilla_\', min(usada.plantilla_id), \'_compra_\', min(compra.id)) as id',
            'min(usada.plantilla_id) as plantilla_id',
            'sum(usada.monto) as monto',
            'min(compra.nro_factura) as nro_factura',
            'min(CONCAT((compra.nro_factura), \' - \', (entidad.razon_social), \' - \', moneda.simbolo, \' - \', cont_plantilla_compraventa.nombre)) as text',
            'min(entidad.razon_social) as razon_social',
            'min(compra.moneda_id) as moneda_id',
            'min(compra.id) as factura_compra_id',
            'min(compra.cotizacion) as cotizacion',
            'min(moneda.tiene_decimales) as tiene_decimales',
            'cont_plantilla_compraventa.para_importacion as para_importacion'
        ]);

        $query->andWhere([
            'compra.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'compra.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'compra.estado' => 'vigente',
        ]);

        if (sizeof($seleccionados) > 0) {
            $query->andWhere(['NOT IN', 'concat(\'plantilla_\', usada.plantilla_id, \'_compra_\', compra.id)', $seleccionados]);
        }

        if (!$local) {
            $query->andWhere(['like', 'cont_plantilla_compraventa.para_importacion', 'exterior']);
        } else {
            $query->andWhere(['<>', 'cont_plantilla_compraventa.para_importacion', 'no']);
            $query->andWhere(['like', 'cont_plantilla_compraventa.para_importacion', 'local']);
        }

        if ($importacionId != null)
            $query->andWhere([
                    'OR',
                    ['IS', 'compra.importacion_id', null],
                    ['AND',
                        ['IS NOT', 'compra.importacion_id', null],
                        ['=', 'compra.importacion_id', $importacionId]
                    ]
                ]
            );
        else
            $query->andWhere(['IS', 'compra.importacion_id', null]);


        $query->orderBy(['razon_social' => SORT_ASC]);
        $query->groupBy('plantilla_id, compra.id');

        return $doMap ?
            ArrayHelper::map($query->asArray()->all(), 'id', 'text') :
            $query->asArray()->all();
    }
    /** CREADO DESDE LA RETOMADA DE NOTAS DE CREDITO DEBITO */

    /**
     * @return TipoDocumentoSet
     * @throws Exception
     */
    public static function getTipodocSetNotaCredito()
    {
        // MIGUEL METIO EL ID DE TIPO DE DOCUMENTO PARA NOTA CREDITO/DEBITO DIRECTAMENTE EN LA MIGRACION DE SU TAREA.
        try {
            $nombre = 'nota_credito_set_id';
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parmsys)) throw new Exception("Falta definir a nivel de sistema (contabilidad) el
             parámetro por Tipo de Documento SET para Notas de crédito. Utilice 'nota_credito_set_id' como nombre.");
            return TipoDocumentoSet::findOne($parmsys->valor);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return TipoDocumentoSet|null
     * @throws Exception
     */
    public static function getTipodocSetNotaDebito()
    {
        // MIGUEL METIO EL ID DE TIPO DE DOCUMENTO PARA NOTA CREDITO/DEBITO DIRECTAMENTE EN LA MIGRACION DE SU TAREA.
        try {
            $nombre = 'nota_debito_set_id';
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parmsys)) throw new Exception("Falta definir a nivel de sistema (contabilidad) el
             parámetro por Tipo de Documento SET para Notas de débito. Utilice 'nota_debito_set_id' como nombre.");
            return TipoDocumentoSet::findOne($parmsys->valor);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return TipoDocumento|null
     * @throws Exception
     */
    public static function getTipodocNotaCredito()
    {
        try {
            if (($tdocNC = TipoDocumento::findOne(['asociacion' => 'proveedor', 'para' => 'nota_credito'])) != null) {
                return $tdocNC;
            }
            $nombre = 'tipodoc_notacredito_compra_id';
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parmsys)) throw new Exception("Falta definir a nivel de sistema (contabilidad) el
             parámetro por Tipo de Documento para Notas de crédito de compras.
              Utilice 'tipodoc_notacredito_compra_id' como nombre.");
            return TipoDocumento::findOne($parmsys->valor);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return TipoDocumento|null
     * @throws Exception
     */
    public static function getTipodocNotaDebito()
    {
        try {
            if (($tdocNC = TipoDocumento::findOne(['asociacion' => 'proveedor', 'para' => 'nota_debito'])) != null) {
                return $tdocNC;
            }
            $nombre = 'tipodoc_notadebito_compra_id';
            $parmsys = ParametroSistema::findOne(['nombre' => $nombre]);
            if (!isset($parmsys)) throw new Exception("Falta definir a nivel de sistema (contabilidad) el
             parámetro por Tipo de Documento para Notas de débito de compras.
              Utilice 'tipodoc_notadebito_compra_id' como nombre");
            return TipoDocumento::findOne($parmsys->valor);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function formatDateForSave()
    {
        $slices = explode('-', $this->fecha_emision);
        if (strlen($slices[0]) == 2)
            $this->fecha_emision = implode('-', array_reverse($slices));
    }

    public function formatDateForViews()
    {
        $slices = explode('-', $this->fecha_emision);
        if (strlen($slices[0]) == 4)
            $this->fecha_emision = implode('-', array_reverse($slices));
    }

    public function isNotaCredito()
    {
        return $this->tipo == 'nota_credito';
    }

    public function isNotaDebito()
    {
        return $this->tipo == 'nota_debito';
    }

    public function isFactura()
    {
        return $this->tipo == 'factura';
    }

    /**
     * @param null|string $empresa_id
     * @param null|Compra $compra
     * @return array|\yii\db\ActiveRecord[]
     * @throws \Exception
     */
    public static function getObligacionesEmpresaActual($empresa_id = null, $compra = null)
    {
        $query = EmpresaObligacion::find()->alias('empresaO')
            ->joinWith('obligacion as ob')
            ->select(['id' => 'empresaO.obligacion_id', 'obligacion_id' => 'empresaO.obligacion_id', 'text' => 'ob.nombre'])
            ->where([
                'empresaO.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                'ob.para_factura' => 'si',
                'ob.estado' => 'activo',
            ]);
        if (isset($compra))
            $query->orFilterWhere(['empresaO.obligacion_id' => $compra->obligacion_id]);

        if (!$query->exists())
            throw new \Exception("La empresa actual no tiene asociado ninguna obligación ACTIVA o se han inactivado las obligaciones asociadas.");

        $array = $query->asArray()->all();
        return $array;
    }

    public function isForImportacion()
    {
        $sql = "SELECT compra.id, plantilla_id, plantilla.para_importacion
FROM cont_factura_compra compra
       left join cont_factura_compra_iva_cuenta_usada ivacta on compra.id = ivacta.factura_compra_id
       left join cont_plantilla_compraventa plantilla on ivacta.plantilla_id = plantilla.id
WHERE compra.id = {$this->id} AND plantilla.para_importacion != 'no'
";
        $sql = "SELECT EXISTS ({$sql}) AS exist";
        $command = \Yii::$app->getDb()->createCommand($sql);
        $resultSet = $command->queryAll();
        return $resultSet[0]['exist'] == '1';
    }

    /**
     * Obtiene un array de ID's o de ActiveRecords de Compra para importacion de la empresa actual.
     *
     * @param bool $onlyID Si se devuelve solo los ids de compras o no.
     * @return array
     */
    public static function getFacturasOfImportacion($onlyID = false)
    {
        /** @var Compra[] $compras */
        $compras = Compra::find()->where([
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')
        ])->all();

        $forImport = [];
        foreach ($compras as $compra) {
            if ($compra->isForImportacion())
                if ($onlyID)
                    $forImport[] = $compra->id;
                else
                    $forImport[] = $compra;
        }
        return $forImport;
    }

    public function beforeValidate()
    {
        $customValidationStatus = true;
        if (isset($this->tipoDocumento) && $this->tipoDocumento->tipo_documento_set_id == ParametroSistemaHelpers::getValorByNombre('tipo_documento_set_ninguno')) {
//        if ($this->tipoDocumento->tipo_documento_set_id == ParametroSistemaHelpers::getValorByNombre('tipo_documento_set_ninguno')) {
            $this->timbrado_detalle_id = '';
        }

        if ($this->bloqueado == null)
            $this->bloqueado = 'no';

//        if (!isset($this->tipoDocumento)) {
//            Yii::error("compra {$this->nro_factura} compraId {$this->id} tipoDocumentoId {$this->tipo_documento_id}");
//        }
//
//        Yii::warning("compra {$this->nro_factura} compraId {$this->id} tipoDocumentoId {$this->tipo_documento_id}");

        return parent::beforeValidate() && $customValidationStatus; // TODO: Change the autogenerated stub
    }

    public function isPartidaCorrect()
    {
        $return = true;
        if ($this->tipoDocumento->tipo_documento_set_id == self::getTipodocSetNotaCredito()->id) {
            $return = false;
            foreach ($this->detallesCompra as $detalle) {
                if ($detalle->cta_contable == 'debe' && $detalle->plan_cuenta_id == $this->tipoDocumento->cuenta_id) {
                    $return = true;
                    break;
                }
            }
        }
        return $return;
    }

    /**
     * @return PlantillaCompraventa[]
     */
    public function getPlantillasUsadas()
    {
        $plantillas = [];
        foreach ($this->ivasCuentaUsadas as $ivasCuentaUsada) {
            $plantillas[] = $ivasCuentaUsada->plantilla;
        }
        return $plantillas;
    }

    public static function getEntidades2($params = [])
    {
        if (empty($params))
            return Entidad::find()->alias('entidad')
                ->innerJoin('cont_factura_compra compra', 'compra.entidad_id = entidad.id')
                ->all();
        else {
            $query = Entidad::find()->alias('entidad')
                ->innerJoin('cont_factura_compra compra', 'compra.entidad_id = entidad.id')
                ->select([
                    'id' => 'entidad.id',
                    'razon_social' => 'entidad.razon_social',
                ]);

            if (array_key_exists('empresa_id', $params))
                $query->where(['compra.empresa_id' => $params['empresa_id']]);

            if (array_key_exists('periodo_contable_id', $params))
                $query->andWhere(['compra.periodo_contable_id' => $params['periodo_contable_id']]);

            if (in_array('forSelect2', $params)) {
                $query = ArrayHelper::map($query->asArray()->all(), 'id', 'razon_social');
            } else {
                $query = $query->all();
            }
        }

        return isset($query) ? $query : null;
    }

    /**
     * @param bool $resolveSilently Whether throw exception or not when $saldo is inconsistent.
     * @throws \Exception
     */
    public function actualizarSaldo($resolveSilently = false)
    {
        if ($this->condicion == 'contado' || $this->tipoDocumento->condicion == 'contado') {
            if ($this->saldo < 0) {
                $this->saldo = 0;
                if (!$this->save(false)) {
                    throw new \Exception("Error interno corrigiendo saldo de factura {$this->nro_factura}: {$this->getErrorSummaryAsString()}");
                }
            } elseif ($this->saldo > $this->total) {
                $this->saldo = $this->total;
                if (!$this->save(false)) {
                    throw new \Exception("Error interno corrigiendo saldo de factura {$this->nro_factura}: {$this->getErrorSummaryAsString()}");
                }
            }
            return;
        }

        if ($this->condicion == 'credito') {
            $saldo = round($this->total, 2);
            $total = round($this->total, 2);

            foreach (ReciboDetalle::findAll(['factura_compra_id' => $this->id]) as $recDet) {
                $saldo -= round($recDet->monto, 2);
            }

            foreach (Retencion::findAll(['factura_compra_id' => $this->id]) as $ret) {
                $saldo -= round($ret->getTotalRetencion(), 2);
            }

            foreach (self::findAll(['factura_compra_id' => $this->id]) as $notaCred) {
                $saldo -= round($notaCred->total, 2);
                foreach (self::findAll(['factura_compra_id' => $notaCred->id]) as $notaDeb) {
                    $saldo += round($notaDeb->total, 2);
                }
            }

            if ($saldo < 0) {
                if ($resolveSilently)
                    $saldo = 0;
                else {
                    $saldo = number_format($saldo, 2, ',', '.');
                    throw new \Exception("No se pudo actualizar saldo de la factura {$this->nro_factura}: El nuevo saldo es negativo: $saldo");
                }
            } elseif ($saldo > $total) {
                if ($resolveSilently)
                    $saldo = $total;
                else {
                    $saldo = number_format($saldo, 2, ',', '.');
                    throw new \Exception("No se pudo actualizar saldo de la factura {$this->nro_factura}: El nuevo saldo {$saldo} es superior al total original {$total}");
                }
            }
            $this->saldo = $saldo;
            if (!$this->save(false)) {
                throw new \Exception("No se pudo actualizar saldo de la factura {$this->nro_factura}: {$this->getErrorSummaryAsString()}");
            }
        }
    }
}