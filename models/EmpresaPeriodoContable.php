<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "cont_empresa_periodo_contable".
 *
 * @property int $id
 * @property string $empresa_id
 * @property string $anho
 * @property string $activo
 * @property string $descripcion
 * @property int $periodo_anterior_id
 * @property int $asiento_cierre_id
 * @property int $asiento_apertura_id
 *
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoAnterior
 * @property Asiento $asientoCierre
 * @property Asiento $asientoApertura
 */
class EmpresaPeriodoContable extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_empresa_periodo_contable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'anho', 'activo'], 'required'],
            [['empresa_id'], 'integer'],
            [['anho'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 255],
            [['activo'], 'string', 'max' => 3],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],

            [['periodo_anterior_id', 'asiento_cierre_id', 'asiento_apertura_id'], 'safe'],
            [['periodo_anterior_id'], 'exist', 'skipOnError' => true, 'targetClass' => self::className(), 'targetAttribute' => ['periodo_anterior_id' => 'id']],
            [['asiento_cierre_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_cierre_id' => 'id']],
            [['asiento_apertura_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_apertura_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa ID',
            'anho' => 'Año',
            'activo' => 'Activo?',
            'descripcion' => 'Descripcion',
            'periodo_anterior_id' => "Periodo anterior"
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoAnterior()
    {
        return $this->hasOne(self::className(), ['id' => 'periodo_anterior_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsientoCierre()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_cierre_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsientoApertura()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_apertura_id']);
    }

    public function getAnhoDesc()
    {
        return $this->anho . ' - ' . $this->descripcion;
    }

    public static function getPcPorEmpresaLista($id = null, $q = null)
    {
        $resultado = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($id)) {
            $consulta = new Query();
            $concat = "CONCAT((cont_empresa_periodo_contable.id), (' - '), (anho), IF (descripcion='', '', ' - '), (descripcion), (' de '), (razon_social))";
//            $consulta->select('id, anho as text')
            $consulta->select([
                'cont_empresa_periodo_contable.id as id',
                $concat . ' AS text',
            ])
                ->from('cont_empresa_periodo_contable')
                ->leftJoin("core_empresa", 'cont_empresa_periodo_contable.empresa_id = core_empresa.id')
                ->where(['empresa_id' => $id])
                ->andWhere(['=', 'cont_empresa_periodo_contable.activo', 'Si'])
                ->limit(20);

            empty($q) || $consulta->andWhere(['=', 'anho', $q]);

            $comando = $consulta->createCommand();
            $datos = $comando->queryAll();
            $resultado['results'] = array_values($datos);
        }

        return $resultado;
    }

    public static function getEmpresaPeriodos($id)
    {
        $epc = EmpresaPeriodoContable::find()->select(['id' => 'id', 'text' => 'anho'])->where(['empresa_id' => $id])->asArray()->all();
        return $epc;
    }

    public static function existNextPeriodo()
    {
        $periodo_act = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'));
        return EmpresaPeriodoContable::find()->where([
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'activo' => "Si",
        ])->andWhere(['>', 'anho', $periodo_act->anho])->exists();
    }

    /**
     * Retorna todos los periodos contables de la empresa asociada
     * y distinta a la actual.
     * @param EmpresaPeriodoContable $model
     * @return array|ActiveRecord[]
     */
    public static function getOtherPeriodos($model)
    {
        $query = self::find()
            ->alias('periodo')
            ->leftJoin('core_empresa empresa', 'empresa.id = periodo.empresa_id')
            ->where(['periodo.empresa_id' => $model->empresa_id])
            ->andFilterWhere(['!=', 'periodo.id', $model->id])
            ->select(['id' => 'periodo.id', 'text' => "CONCAT(periodo.anho, ' - ', empresa.razon_social)"]);

        return $query->asArray()->all();
    }

    public function getNextPeriodos()
    {
        $query = self::find()
            ->alias('periodo')
            ->leftJoin('core_empresa empresa', 'empresa.id = periodo.empresa_id')
            ->where(['periodo.empresa_id' => $this->empresa_id])
            ->andFilterWhere(['!=', 'periodo.id', $this->id])
            ->andWhere(['>', 'periodo.anho', $this->anho])
            ->select(['id' => 'periodo.id', 'text' => "CONCAT(periodo.anho, ' - ', empresa.razon_social)"]);

        return $query->asArray()->all();
    }
}
