<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 02/10/2018
 * Time: 11:04
 */

namespace backend\modules\contabilidad\models;


use yii\base\Model;

/**
 * @property int $prestamo_id
 */
class PrestamoSelector extends Model
{
    public $prestamo_id;

    public function rules()
    {
        $array = [
            [['prestamo_id'], 'required'],
        ];

        return $array;
    }

    public function attributeLabels()
    {
        return [
            'prestamo_id' => "Prestamo",
        ];
    }
}