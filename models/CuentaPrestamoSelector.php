<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 02/10/2018
 * Time: 11:04
 */

namespace backend\modules\contabilidad\models;


use yii\base\Model;

/**
 * @property int $cuenta_id
 * @property string $concepto
 *
 * @property PlanCuenta $cuenta
 * @property string $conceptoText
 *
 */
class CuentaPrestamoSelector extends Model
{
    public $cuenta_id;
    public $concepto;

    public function rules()
    {
        $array = [
            [['cuenta_id', 'concepto'], 'required'],
            [['cuenta_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['cuenta_id' => 'id']]
        ];

        return $array;
    }

    public function attributeLabels()
    {
        return [
            'cuenta_id' => "Cuenta Contable",
            'concepto' => "Concepto"
        ];
    }

    public function getCuenta()
    {
        return PlanCuenta::find()->where(['id' => $this->cuenta_id])->one();
    }

    public function getConceptoText()
    {
        $array = self::conceptosLista();
        if ($this->concepto == 'capital')
            $this->concepto = 'monto_operacion';
        return $array[$this->concepto];
    }

    public static function conceptosLista()
    {
        return [
            'caja' => 'Caja',
            'intereses_vencer' => 'Intereses a Vencer',
            'iva_10' => 'IVA 10%',
            'gastos_bancarios' => "Gastos Bancarios",
            'gastos_no_deducibles' => "Gastos No Deducibles",
            'monto_operacion' => "Préstamo del Banco X",
            'intereses_pagados' => "Intereses Pagados",
            'intereses_a_pagar' => "Intereses a Pagar",
        ];
    }
}