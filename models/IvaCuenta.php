<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Iva;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cont_iva_cuenta".
 *
 * @property int $id
 * @property string $iva_id
 * @property string $cuenta_compra_id
 * @property string $cuenta_venta_id
 *
 * @property PlanCuenta $cuentaCompra
 * @property PlanCuenta $cuentaVenta
 * @property Iva $iva
 * @property PlantillaCompraventaDetalle[] $PlantillaCompraventaDetalles
 */
class IvaCuenta extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_iva_cuenta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iva_id',], 'required'],
            [['iva_id', 'cuenta_compra_id', 'cuenta_venta_id'], 'integer'],
            [['cuenta_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['cuenta_compra_id' => 'id']],
            [['cuenta_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['cuenta_venta_id' => 'id']],
            [['iva_id'], 'exist', 'skipOnError' => true, 'targetClass' => Iva::className(), 'targetAttribute' => ['iva_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iva_id' => 'Iva ID',
            'cuenta_compra_id' => 'Cuenta Compra',
            'cuenta_venta_id' => 'Cuenta Venta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaCompra()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'cuenta_compra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaVenta()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'cuenta_venta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIva()
    {
        return $this->hasOne(Iva::className(), ['id' => 'iva_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantillaCompraventaDetalles()
    {
        return $this->hasMany(PlantillaCompraventaDetalle::className(), ['iva_cta_id' => 'id']);
    }

    public static function getIvasCuentasVenta()
    {
        $text = "concat(('IVA '), (iva.porcentaje), ('%'), (' - '), (cta.cod_completo), (' '), (cta.nombre))";
        $tname = 'cont_iva_cuenta';
        $query = IvaCuenta::find()
            ->joinWith('iva as iva')
            ->joinWith('cuentaVenta as cta')
            ->select([
                $tname . '.id as ID',
                'iva.id',
                $tname . '.iva_id',
                'cta.id',
                $tname . '.cuenta_venta_id',
                'text' => $text,
            ])
            ->where(['!=', $tname . '.cuenta_venta_id', ''])
            // TODO: permitir crear detalles con iva_cuenta que no tiene asociado una cuenta.
            ->asArray()->all();
        return ArrayHelper::map($query, 'ID', 'text');
    }

    public static function getIvasCuentasCompra()
    {
        $text = "concat(('IVA '), (iva.porcentaje), ('%'), (' - '), (cta.cod_completo), (' '), (cta.nombre))";
        $tname = 'cont_iva_cuenta';
        $query = IvaCuenta::find()
            ->joinWith('iva as iva')
            ->joinWith('cuentaCompra as cta')
            ->select([
                $tname . '.id as ID',
                'iva.id',
                $tname . '.iva_id',
                'cta.id',
                $tname . '.cuenta_compra_id',
                'text' => $text,
            ])
            ->where(['!=', $tname . '.cuenta_compra_id', ''])
            // TODO: permitir crear detalles con iva_cuenta que no tiene asociado una cuenta.
            ->asArray()->all();
        return ArrayHelper::map($query, 'ID', 'text');
    }
}
