<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use backend\models\Moneda;
use backend\modules\contabilidad\models\validators\ActivoFijoCantidadValidator;
use Yii;

/**
 * This is the model class for table "{{%cont_activo_fijo}}".
 *
 * @property string $id
 * @property string $activo_fijo_tipo_id
 * @property string $empresa_id
 * @property int $empresa_periodo_contable_id
 * @property int $factura_venta_id
 * @property int $factura_compra_id
 * @property int $asiento_id
 * @property int $moneda_id
 * @property string $nombre
 * @property string $costo_adquisicion
 * @property string $fecha_adquisicion
 * @property int $vida_util_fiscal
 * @property int $vida_util_contable
 * @property int $vida_util_restante
 * @property string $valor_fiscal_neto
 * @property string $valor_contable_neto - Valor revaluado contable neto.
 * @property string $cuenta_id
 * @property string $cuenta_depreciacion_id
 * @property string $estado - A sugerencia de Jose el 14/12/2018 para facilitar el manejo por notas de credito.
 * @property string $observacion
 * @property int $activo_fijo_id - id del activo fijo del ejercicio anterior.
 * @property int $valor_fiscal_revaluado
 * @property int $valor_contable_revaluado
 * @property int $valor_fiscal_depreciado
 * @property int $valor_contable_depreciado
 * @property int $asiento_revaluo_id
 * @property int $asiento_depreciacion_id
 * @property string $tangible
 *
 * @property int actfijo_selector
 * @property string $cantidad
 * @property int $fila_id
 * @property string $valor_fiscal_anterior
 * @property string $valor_contable_anterior
 * @property array $tabla_depreciacion
 *
 * @property ActivoFijoTipo $activoFijoTipo
 * @property ActivoFijo $actFijoPeriodoAnterior
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $empresaPeriodoContable
 * @property PlanCuenta $cuenta
 * @property PlanCuenta $cuentaDepreciacion
 * @property Venta $venta
 * @property Compra $compra
 * @property Asiento $asiento
 * @property Asiento $asientoRevaluo
 * @property Asiento $asientoDepreciacion
 * @property Moneda $moneda
 * @property ActivoFijoAtributo[] $atributos
 */
class ActivoFijo extends BaseModel
{
    public $actfijo_selector;
    public $tangible = "si";  # Temporal. Se va a definir bien el ambito de manejo de intangibles.
    public $cantidad;
    public $fila_id;
    public $tabla_depreciacion = [];


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cont_activo_fijo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo_fijo_tipo_id', 'empresa_id', 'empresa_periodo_contable_id', 'nombre', 'costo_adquisicion', 'fecha_adquisicion', 'vida_util_fiscal', 'vida_util_restante', 'cuenta_id',], 'required'],
            [['activo_fijo_tipo_id', 'empresa_id', 'empresa_periodo_contable_id', 'vida_util_fiscal', 'vida_util_contable', 'vida_util_restante',
                'cuenta_id', 'actfijo_selector', 'fila_id', 'factura_venta_id', 'factura_compra_id', 'asiento_id', 'moneda_id', 'activo_fijo_id'], 'integer'],
//            [['costo_adquisicion', 'coeficiente_revaluo', 'valor_fiscal_neto', 'valor_fiscal_revaluo'], 'number'],
            [['valor_fiscal_neto', 'cantidad'], 'number'],
            [['fecha_adquisicion', 'actfijo_selector', 'cantidad', 'fila_id', 'observacion', 'activo_fijo_id', 'vida_util_contable',
                'valor_fiscal_revaluado', 'valor_contable_revaluado', 'valor_fiscal_depreciado', 'valor_contable_depreciado',
                'asiento_revaluo_id', 'asiento_depreciacion_id', 'cuenta_depreciacion_id'], 'safe'],
            [['nombre'], 'string', 'max' => 100],
            [['observacion'], 'string', 'max' => 256],
            [['valor_fiscal_revaluado', 'valor_contable_revaluado'], 'default', 'value' => 0],
//            [['costo_adquisicion'], 'string'],
            [['activo_fijo_tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoFijoTipo::className(), 'targetAttribute' => ['activo_fijo_tipo_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['empresa_periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['empresa_periodo_contable_id' => 'id']],
            [['cuenta_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['cuenta_id' => 'id']],
            [['cuenta_depreciacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['cuenta_depreciacion_id' => 'id']],
            [['factura_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['factura_venta_id' => 'id']],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],
            [['asiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id' => 'id']],
            [['moneda_id'], 'exist', 'skipOnError' => true, 'targetClass' => Moneda::className(), 'targetAttribute' => ['moneda_id' => 'id']],
            [['activo_fijo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoFijo::className(), 'targetAttribute' => ['activo_fijo_id' => 'id']],
            [['asiento_revaluo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_revaluo_id' => 'id']],
            [['asiento_depreciacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_depreciacion_id' => 'id']],

            [['cantidad'], ActivoFijoCantidadValidator::className()],
            [['estado'], 'safe'],
            [['estado'], 'default', 'value' => 'activo'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'activo_fijo_tipo_id' => Yii::t('app', 'Activo Fijo Tipo'),
            'empresa_id' => Yii::t('app', 'Empresa ID'),
            'empresa_periodo_contable_id' => Yii::t('app', 'Periodo Contable'),
            'nombre' => Yii::t('app', 'Nombre'),
            'costo_adquisicion' => Yii::t('app', 'Costo Adquisición Unitario'),
            'fecha_adquisicion' => Yii::t('app', 'Fecha Adquisicion'),
//            'coeficiente_revaluo' => Yii::t('app', 'Coeficiente Revaluo'),
            'vida_util_fiscal' => Yii::t('app', 'Vida Util Fiscal'),
            'vida_util_restante' => Yii::t('app', 'Vida Util Restante'),
            'valor_fiscal_neto' => Yii::t('app', 'Valor Fiscal Neto'),
//            'valor_fiscal_revaluo' => Yii::t('app', 'Valor Fiscal Revaluo'),
            'cuenta_id' => Yii::t('app', 'Cuenta'),
            'fila_id' => '',
            'cantidad' => 'Cantidad',
            'moneda_id' => 'Moneda ID',
            'estado' => 'Estado',
            'activo_fijo_id' => 'Activo Fijo Periodo Anterior',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoFijoTipo()
    {
        return $this->hasOne(ActivoFijoTipo::className(), ['id' => 'activo_fijo_tipo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneda()
    {
        return $this->hasOne(Moneda::className(), ['id' => 'moneda_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActFijoPeriodoAnterior()
    {
        return $this->hasOne(ActivoFijo::className(), ['id' => 'activo_fijo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsiento()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsientoRevaluo()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_revaluo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsientoDepreciacion()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_depreciacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresaPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'empresa_periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuenta()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'cuenta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaDepreciacion()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'cuenta_depreciacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'factura_venta_id']);
    }

    /**
     * Usado por el _facturas_relacionadas
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->getVenta();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * Usado por el _facturas_relacionadas
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->getCompra();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtributos()
    {
        return $this->hasMany(ActivoFijoAtributo::className(), ['activo_fijo_id' => 'id']);
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->empresa_periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
        $this->empresa_id = Yii::$app->session->get('core_empresa_actual');
        return parent::loadDefaultValues($skipIfSet);
    }

    public function beforeValidate()
    {
        if (strpos($this->costo_adquisicion, '.') && strlen(substr($this->costo_adquisicion, (strpos($this->costo_adquisicion, '.') + 1))) >= 3) {
            $this->costo_adquisicion = str_replace('.', '', $this->costo_adquisicion);
            $this->costo_adquisicion = str_replace(',', '.', $this->costo_adquisicion);
        } elseif (strpos($this->costo_adquisicion, ',') == strlen($this->costo_adquisicion) - 3)
            $this->costo_adquisicion = str_replace(',', '.', $this->costo_adquisicion);

        if ($this->valor_fiscal_revaluado == '')
            $this->valor_fiscal_revaluado = 0;

        if ($this->valor_contable_revaluado == '')
            $this->valor_contable_revaluado = 0;

        // se valida aqui y no con required, porque se agrego el campo despues de que se crearon varios registros de a.fijos.
        // además, se desea que los activos fijos que no tienen factura de compra, no tengan asociado una moneda.
        if ($this->moneda_id == "") {
            $this->addError('moneda_id', "Debe especificar una moneda para el Activo Fijo.");
        }
        return parent::beforeValidate();
    }

    /**
     * @param int $factura_venta_id
     */
    public function setFacturaVentaId($factura_venta_id)
    {
        $this->factura_venta_id = $factura_venta_id;
    }

    /**
     * @param int $factura_venta_id
     */
    public function setNullFacturaVentaId()
    {
        $this->factura_venta_id = null;
    }

    /**
     * @param int $factura_compra_id
     */
    public function setFacturaCompraId($factura_compra_id)
    {
        $this->factura_compra_id = $factura_compra_id;
    }

    /**
     * @param string $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @param string $costo_adquisicion
     */
    public function setCostoAdquisicion($costo_adquisicion)
    {
        $this->costo_adquisicion = $costo_adquisicion;
    }

    /**
     * @param int $moneda_id
     */
    public function setMonedaId($moneda_id)
    {
        $this->moneda_id = $moneda_id;
    }

    public function loadForTesting()
    {
        $this->nombre = 'test';
        $this->cuenta_id = PlanCuenta::find()->where(['>', 'id', 1])->one()->id;
        $this->costo_adquisicion = 10;
        $this->fecha_adquisicion = date('Y-m-d');
        $this->moneda_id = Moneda::find()->where(['>', 'id', 1])->one()->id;
        $this->valor_fiscal_neto = 10;
    }

    public function acquiredInCurrentYear()
    {
        $currentYear = EmpresaPeriodoContable::findOne(['id' => Yii::$app->session->get('core_empresa_actual_pc')])->anho; //date('Y');
        $acquiredYear = date('Y', strtotime($this->fecha_adquisicion));
        return $currentYear == $acquiredYear;
    }

    public function getValor_fiscal_anterior()
    {
        if ($this->acquiredInCurrentYear()) return $this->costo_adquisicion;

        return $this->actFijoPeriodoAnterior->valor_fiscal_neto;
    }

    public function getValor_contable_anterior()
    {
        if ($this->acquiredInCurrentYear()) return $this->costo_adquisicion;

        return $this->actFijoPeriodoAnterior->valor_contable_neto;
    }

    public function getTablaDepreciacion()
    {
        if (!empty($this->tabla_depreciacion)) return $this->tabla_depreciacion;

        $depreciacionFiscal = $this->costo_adquisicion / max(1, $this->vida_util_fiscal);
        $depreciacionContab = $this->costo_adquisicion / max(1, $this->vida_util_contable);

        $anhos = range(1, max($this->vida_util_contable, $this->vida_util_fiscal));

        $template = [
            'deprec_acum_fiscal' => 0,
            'deprec_acum_contab' => 0,
        ];
        foreach ($anhos as $anho) {

            // Si vida util es cero, no hay depreciacion acumulativa. Se aplica tanto a fiscal como contable.

            if ($this->vida_util_fiscal > 0 && $anho <= $this->vida_util_fiscal) {
                $template['deprec_acum_fiscal'] += $depreciacionFiscal;
                if ($anho == $this->vida_util_fiscal)
                    $template['deprec_acum_fiscal'] -= 1;
            }

            if ($this->vida_util_contable > 0 && $anho <= $this->vida_util_contable) {
                $template['deprec_acum_contab'] += $depreciacionContab;
                if ($anho == $this->vida_util_contable)
                    $template['deprec_acum_contab'] -= 1;
            }

            // In PHP arrays are assigned by copy.
            // Fuente: https://stackoverflow.com/questions/1532618/is-there-a-function-to-make-a-copy-of-a-php-array-to-another
            $data = $template;
            $this->tabla_depreciacion[$anho] = $data;
        }

        return $this->tabla_depreciacion;
    }
}
