<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;

/**
 * This is the model class for table "cont_rubro".
 *
 * @property string $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $estado
 * @property int $rubro_id
 *
 * @property string $formattedEstado
 *
 * @property EmpresaRubro[] $empresaRubros
 */
class Rubro extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_rubro';
    }

    public static function getEstados($isForFilteringBy = false)
    {
        $array = [];
        !$isForFilteringBy || $array['todos'] = "Todos";
        $array['activo'] = "Activo";
        $array['inactivo'] = "Inactivo";

        return $array;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['estado'], 'string'],
            [['nombre'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 256],
        ];
    }

    public function getFormattedEstado()
    {
        return ucfirst($this->estado);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresaRubros()
    {
        return $this->hasMany(EmpresaRubro::className(), ['rubro_id' => 'id']);
    }
}
