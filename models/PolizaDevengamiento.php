<?php

namespace backend\modules\contabilidad\models;

/**
 * This is the model class for table "cont_poliza_devengamiento".
 *
 * @property string $id
 * @property string $poliza_id
 * @property string $asiento_id
 * @property int $periodo_contable_id
 * @property int $dias
 * @property string $monto
 * @property string $anho_mes
 *
 * @property Asiento $asiento
 * @property Poliza $poliza
 * @property EmpresaPeriodoContable $periodoable
 */
class PolizaDevengamiento extends \backend\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_poliza_devengamiento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['poliza_id', 'periodo_contable_id', 'dias', 'anho_mes'], 'required'],
            [['poliza_id', 'asiento_id', 'periodo_contable_id', 'dias'], 'integer'],
            [['monto'], 'number'],
            [['anho_mes'], 'string', 'max' => 7],
            [['asiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id' => 'id']],
            [['poliza_id'], 'exist', 'skipOnError' => true, 'targetClass' => Poliza::className(), 'targetAttribute' => ['poliza_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'poliza_id' => 'Poliza ID',
            'asiento_id' => 'Asiento ID',
            'periodo_contable_id' => 'Periodo able ID',
            'dias' => 'Dias',
            'monto' => 'Monto',
            'anho_mes' => 'Mes - Año',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsiento()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoliza()
    {
        return $this->hasOne(Poliza::className(), ['id' => 'poliza_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }
}
