<?php

/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 18/10/2018
 * Time: 8:20
 */

namespace backend\modules\contabilidad\models;

/**
 * This is the model class for table "cont_prestamo_detalle".
 *
 * @property int $nro_cuota Como maximo tiene que valer igual al cant_cuotas del cont_prestamo.
 * @property string $vencimiento
 * @property string $monto_cuota
 * @property string $interes
 * @property string $capital
 */
class PrestamoDetalleTemp extends PrestamoDetalle
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nro_cuota'], 'integer'],
            [['vencimiento', 'monto_cuota', 'interes', 'capital'], 'safe'],
            [['vencimiento', 'monto_cuota', 'interes', 'capital'], 'string'],
        ];
    }
}
