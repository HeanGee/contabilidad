<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cont_plantilla_compraventa".
 *
 * @property string $id
 * @property string $nombre
 * @property string $tipo
 * @property string $estado
 * @property string $costo_mercad
 * @property string $para_seguro_a_devengar
 * @property string $para_cuota_prestamo
 * @property string $activo_fijo
 * @property string $activo_biologico
 * @property string $deducible
 * @property string $para_prestamo
 * @property string $para_importacion
 * @property string $para_descuento
 *
 * @property Venta[] $Ventas
 * @property PlantillaCompraventaDetalle[] $plantillaCompraventaDetalles
 * @property Rubro[] $rubros
 * @property Obligacion[] $obligaciones
 * @property PlantillaCompraventaObligacion[] $plantillaObligaciones
 * @property PlantillaCompraventaRubro[] $plantillaRubros
 */
class PlantillaCompraventa extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_plantilla_compraventa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo', 'estado', 'para_seguro_a_devengar', 'deducible', 'para_prestamo', 'para_cuota_prestamo', 'para_importacion'], 'required'],
            [['tipo', 'estado', 'costo_mercad', 'para_seguro_a_devengar', 'activo_fijo', 'para_prestamo', 'activo_biologico', 'para_descuento'], 'string'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'tipo' => 'Tipo',
            'estado' => 'Estado',
            'costo_mercad' => 'Tiene costo de venta de mercaderías?',
            'para_seguro_a_devengar' => 'Seguro A Devengar?',
            'activo_fijo' => 'Activo Fijo?',
            'para_prestamo' => 'Préstamo?',
            'activo_biologico' => 'Activo Biológico?',
            'para_cuota_prestamo' => "Cuota Prestamo?",
            'para_importacion' => "Para Importacion?",
            'para_descuento' => "Para Descuento"
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Venta::className(), ['plantilla_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantillaCompraventaDetalles()
    {
        return $this->hasMany(PlantillaCompraventaDetalle::className(), ['plantilla_id' => 'id']);
    }

    /**
     * @return Rubro[]
     */
    public function getRubros()
    {
        $rubros = [];
        foreach ($this->plantillaRubros as $plantillaRubro) {
            $rubros[] = $plantillaRubro->rubro;
        }

        return $rubros;
    }

    /**
     * @return boolean
     */
    public function esParaPrestamo()
    {
        return $this->para_prestamo == 'si';
    }

    /**
     * @return boolean
     */
    public function esParaImportacionExterior()
    {
        return explode('_', $this->para_importacion)[0] == 'exterior';
    }

    /**
     * @return boolean
     */
    public function esParaImportacion()
    {
        return $this->para_importacion != 'no';
    }

    /**
     * @return boolean
     */
    public function esParaCuotaPrestamo()
    {
        return $this->para_cuota_prestamo == 'si';
    }

    /**
     * @return boolean
     */
    public function esParaActivoBiologico()
    {
        return $this->activo_biologico == 'si';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantillaObligaciones()
    {
        return $this->hasMany(PlantillaCompraventaObligacion::className(), ['plantilla_compraventa_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlantillaRubros()
    {
        return $this->hasMany(PlantillaCompraventaRubro::className(), ['plantilla_compraventa_id' => 'id']);
    }

    /**
     * @return Obligacion[]
     */
    public function getObligaciones()
    {
        $obligaciones = [];
        foreach ($this->plantillaObligaciones as $plantillaObligacion) {
            $obligaciones[] = $plantillaObligacion->obligacion;
        }

        return $obligaciones;
    }

//    public function hasAnyAssociation($exclude_self_detalles = false)
//    {
//        $asociadoFactVenta = Venta::find()->where(['plantilla_id' => $this->id])->exists();
//        $tieneDetalles = PlantillaCompraventaDetalle::find()->where(['plantilla_id' => $this->id])->exists();
//        // more association check goes here...
//
//        $msg = [];
//        if ($asociadoFactVenta) $msg[] = 'La plantilla ' . $this->nombre . ' es utilizada por Factura Ventas.';
//        if (!$exclude_self_detalles && $tieneDetalles) $msg[] = 'La plantilla ' . $this->nombre . ' tiene detalles de plantillas.';
//        // more message to warning goes here...
//
//        foreach ($msg as $m)
//            FlashMessageHelpsers::createWarningMessage($m);
//
//        if ($exclude_self_detalles) return $asociadoFactVenta;
//        else return ($asociadoFactVenta || $tieneDetalles);
//
//    }

    public function hasOneDetallePorVentaExenta()
    {
        return $this->getPlantillaCompraventaDetalles()->where(['iva_cta_id' => null, 'tipo_asiento' => 'venta'])->exists();
    }

    public static function getPlantillasLista($tipo = null, $doMap = true, $noDeducible = false, $paraImportacion = false, $obligacion_id = null)
    {
        $tPlantillaCompraVenta = PlantillaCompraventa::tableName();
        $tPlantillaRubro = PlantillaCompraventaRubro::tableName();
        $tEmpresaRubro = EmpresaRubro::tableName();
        $concat = "CONCAT((" . $tPlantillaCompraVenta . ".id), (' - '), (" . $tPlantillaCompraVenta . ".nombre)) AS text";
        $query = PlantillaCompraventa::find()
            ->where([$tPlantillaCompraVenta . '.tipo' => $tipo == null ? 'venta' : $tipo, 'estado' => 'activo']);
        if ($noDeducible)
            $query->andWhere(['deducible' => 'no']);

        if ($paraImportacion)
            $query->andWhere(['like', 'para_importacion', 'exterior_']);
        else
            $query->andWhere(['not like', 'para_importacion', 'exterior_']);

        $query->innerJoin($tPlantillaRubro, $tPlantillaCompraVenta . '.id = ' . $tPlantillaRubro . '.plantilla_compraventa_id');
        $query->innerJoin($tEmpresaRubro, $tPlantillaRubro . '.rubro_id = ' . $tEmpresaRubro . '.rubro_id');
        $query->select([
            $tPlantillaCompraVenta . '.id as id',
            $tPlantillaCompraVenta . '.nombre',
            $concat,
            $tPlantillaCompraVenta . '.para_seguro_a_devengar',
            $tPlantillaCompraVenta . '.activo_fijo',
            $tPlantillaCompraVenta . '.para_prestamo',
            $tPlantillaCompraVenta . '.para_cuota_prestamo',
            $tPlantillaCompraVenta . '.activo_biologico',
            $tPlantillaCompraVenta . '.deducible',
            $tPlantillaCompraVenta . '.para_descuento',
        ]);

        if (isset($obligacion_id) && $obligacion_id != '') {
            $plantillasPorOblig = PlantillaCompraventaObligacion::findAll(['obligacion_id' => $obligacion_id]);
            $ids = [];
            foreach ($plantillasPorOblig as $plantillaOb) {
                $ids[] = $plantillaOb->plantilla_compraventa_id;
            }
            $query->andFilterWhere(['IN', $tPlantillaCompraVenta . '.id', $ids]);
        }

        $data = $query->asArray()->all();

        if (!$doMap) return $data;

        return ArrayHelper::map($data, 'id', 'text');
    }

    public function hasOneDetallePorCompraExenta()
    {
        return $this->getPlantillaCompraventaDetalles()->where(['iva_cta_id' => null])->andWhere(['tipo_asiento' => 'compra'])->exists();
    }

    /**
     * @param string $column_name
     * @param string $cell_value
     * @return PlantillaCompraventaDetalle[]
     */
    public function getDetalles($column_name = 'id', $cell_value = '1')
    {
        return PlantillaCompraventaDetalle::find()->where([$column_name => $cell_value, 'plantilla_id' => $this->id])->all();
    }

    public function esParaSeguroADevengar()
    {
        return $this->para_seguro_a_devengar == 'si' ? true : false;
    }

    /**
     * @return null|string Retorna id de la cuenta seguro a devengar si existe o null.
     */
    public static function getIdCuentaSeguroADevengar()
    {
        $query = PlantillaCompraventa::find()->where(['para_seguro_a_devengar' => 'si']);
        if ($query->exists()) {
            /** @var PlantillaCompraventa $plantilla */
            $plantilla = $query->one();
            return $plantilla->plantillaCompraventaDetalles[0]->p_c_gravada_id;
        }
        return null;
    }

    /**
     * @param $plantilla_id
     * @param null $p_det_campo Campo sobre el cual se desea condicionar.
     * @param null $p_det_valor Valor para el campo sobre el cual desea condicionar.
     * @return array|null|PlantillaCompraventaDetalle
     */
    public static function getFirstCuentaPrincipal($plantilla_id, $p_det_campo = null, $p_det_valor = null)
    {
        return PlantillaCompraventaDetalle::find()
            ->where(['plantilla_id' => $plantilla_id, 'cta_principal' => 'si'])
            ->andFilterWhere([$p_det_campo => $p_det_valor])
            ->orderBy(['id' => SORT_ASC])
            ->one();
    }

    public function esParaActivoFijo()
    {
        return $this->activo_fijo == 'si';
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->estado = 'activo';
        $this->costo_mercad = 'no';
        $this->deducible = 'si';
        $this->activo_fijo = 'no';
        $this->para_seguro_a_devengar = 'no';
        $this->para_prestamo = 'no';
        $this->para_cuota_prestamo = 'no';
        $this->activo_biologico = 'no';
        $this->para_importacion = 'no';

        return parent::loadDefaultValues($skipIfSet); // TODO: Change the autogenerated stub
    }

    public static function getParaImportacionValues()
    {
        $toReturnEnum = [];
        $table = self::tableName();
        $field = 'para_importacion';
        $command = \Yii::$app->db->createCommand("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'");
        try {
            $type = $command->queryAll()[0]['Type'];
            preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
            $enum = explode("','", $matches[1]);

            foreach ($enum as $e) {
                $toReturnEnum[$e] = implode(' ', array_map("ucfirst", explode('_', $e)));
                asort($toReturnEnum);
            }
        } catch (Exception $e) {

        }

        return $toReturnEnum;
    }

    public function hasCtaPrincipal()
    {
        foreach ($this->plantillaCompraventaDetalles as $plantillaCompraventaDetalle) {
            if ($plantillaCompraventaDetalle->cta_principal == 'si') {
                \Yii::warning($plantillaCompraventaDetalle->pCGravada->nombre);
                return true;
            }
        }
        return false;
    }

    public function isForDiscount()
    {
        return $this->para_descuento == 'si';
    }
}
