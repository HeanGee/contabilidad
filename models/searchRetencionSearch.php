<?php

namespace backend\modules\contabilidad\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * searchRetencionSearch represents the model behind the search form of `backend\modules\contabilidad\models\Retencion`.
 */
class searchRetencionSearch extends Retencion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'factura_compra_id', 'factura_venta_id', 'empresa_id', 'periodo_contable_id'], 'integer'],
            [['fecha_emision', 'nro_retencion', 'asiento_id', 'factura_id', 'total'], 'safe'],
            [['base_renta_porc', 'base_renta_cabezas', 'base_renta_toneladas', 'factor_renta_porc', 'factor_renta_cabezas', 'factor_renta_toneladas'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param string $operacion
     * @return ActiveDataProvider
     */
    public function search($params, $operacion = 'compra')
    {
        $query = Retencion::find()->alias('retencion')
            ->leftJoin("cont_factura_{$operacion} $operacion", "retencion.factura_{$operacion}_id = $operacion.id");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'retencion.id' => $this->id,
            'retencion.fecha_emision' => ($this->fecha_emision != '' ? implode('-', array_reverse(explode('-', trim($this->fecha_emision)))) : ""),
            'retencion.base_renta_porc' => $this->base_renta_porc,
            'retencion.base_renta_cabezas' => $this->base_renta_cabezas,
            'retencion.base_renta_toneladas' => $this->base_renta_toneladas,
            'retencion.factor_renta_porc' => $this->factor_renta_porc,
            'retencion.factor_renta_cabezas' => $this->factor_renta_cabezas,
            'retencion.factor_renta_toneladas' => $this->factor_renta_toneladas,
            'retencion.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'retencion.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ]);

        $query->andFilterWhere(['like', 'retencion.nro_retencion', $this->nro_retencion]);

        #Asentado o no
        if ($this->asiento_id != '')
            $query->andWhere([($this->asiento_id == 'si' ? "IS NOT" : "IS"), 'retencion.asiento_id', null]);

        if (\Yii::$app->request->getQueryParam('operacion') == 'venta') {
            $query->andWhere(['!=', 'retencion.factura_venta_id', ""]);
        } else
            $query->andWhere(['!=', 'retencion.factura_compra_id', ""]);

        if ($operacion == 'compra')
            $query->andFilterWhere(['like', "$operacion.nro_factura", trim($this->factura_id)]);
        elseif ($operacion == 'venta')
            $query->andFilterWhere(['like', "CONCAT($operacion.prefijo, '-', $operacion.nro_factura)", trim($this->factura_id)]);

        return $dataProvider;
    }
}
