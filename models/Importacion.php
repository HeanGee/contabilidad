<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use backend\models\Moneda;
use common\helpers\ValorHelpers;

/**
 * This is the model class for table "cont_factura_compra".
 *
 * @property int $id
 * @property string $fecha
 * @property int $moneda_id //TODO depende de si la importacion maneja una sola moneda
 * @property float $cotizacion //TODO depende de si la importacion maneja una sola moneda
 * @property string $estado
 * @property int $periodo_contable_id
 * @property int $empresa_id
 * @property string $criterio_retencion
 * @property string $medio_transporte
 * @property string $numero_despacho
 * @property int $numero
 * @property string $cal_retencion_fact_local_flete_seguro
 * @property string $indi
 * @property string $iva
 * @property string $derecho_aduanero
 * @property string $servicio_valoracion
 * @property string $iracis_generaral
 * @property string $tasa_interv_aduanera
 * @property string $repos_gtos_adm_annp
 * @property string $cdap_comis_can_annp
 * @property string $sofia_comis_can_annp
 * @property string $multas_varias
 * @property string $impuesto_selectivo_consumo
 * @property string $cannon_informatico_sistema_sofia
 * @property string $arancel_consular
 * @property string $condicion
 * @property string $exportador
 * @property string $flete_costo
 * @property string $seguro_costo
 * @property int $despacho_moneda_id
 * @property string $despacho_cotizacion
 *
 * @property ImportacionDetalleFacturaLocalExterior[] $importacionDetallesLocal
 * @property ImportacionDetalleFacturaLocalExterior[] $importacionDetallesExterior
 * @property EmpresaPeriodoContable $periodoContable
 * @property Empresa $empresa
 * @property Compra $facturaCompra
 * @property Moneda $despachoMoneda
 */
class Importacion extends BaseModel
{
    public $arrayExterior = [];
    public $arrayLocal = [];
    public $guardarycerrar;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_importacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $monedaBase = \common\models\ParametroSistema::getMonedaBaseId();
        return [
            [['periodo_contable_id', 'empresa_id', 'criterio_retencion', 'medio_transporte', 'cal_retencion_fact_local_flete_seguro', 'fecha', 'numero_despacho', 'estado', 'condicion', 'exportador'], 'required'],
            [['id', 'periodo_contable_id', 'empresa_id', 'numero'], 'integer'],
            [['iva', 'indi', 'derecho_aduanero', 'servicio_valoracion', 'iracis_generaral', 'tasa_interv_aduanera',
                'repos_gtos_adm_annp', 'cdap_comis_can_annp', 'sofia_comis_can_annp', 'multas_varias', 'impuesto_selectivo_consumo',
                'cannon_informatico_sistema_sofia', 'arancel_consular'], 'number'],
            [['guardarycerrar', 'numero_despacho', 'condicion', 'exportador'], 'string'],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => false, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],

            // Nuevos campos
            [['flete_costo', 'seguro_costo', 'despacho_moneda_id', 'despacho_cotizacion'], 'safe'],
            [['seguro_costo', 'despacho_moneda_id'], 'required', 'when' => function ($model) {
                return $model->flete_costo != '';
            }, 'whenClient' => "function (attribute, value) {
                return $('input[id$=\"flete_costo\"]').val() !== '';
            }"],
            [['flete_costo', 'despacho_moneda_id'], 'required', 'when' => function ($model) {
                return $model->seguro_costo != '';
            }, 'whenClient' => "function (attribute, value) {
                return $('input[id$=\"seguro_costo\"]').val() !== '';
            }"],
            [['despacho_cotizacion'], 'required', 'when' => function ($model) {
                $monedaBase = \common\models\ParametroSistema::getMonedaBaseId();
                return $model->despacho_moneda_id != '' && $model->despacho_moneda_id != $monedaBase;
            }, 'whenClient' => "function (attribute, value) {
                var field = $('select[id$=\"despacho_moneda_id\"]');
                return field.val() !== '' && field.val() != '$monedaBase';
            }"]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'backend\models\autonumber\Behavior',
                'attribute' => 'numero', // required
                'group' => 'importacion_empresa_' . (string)$this->empresa_id, // optional
                'value' => '?', // format auto number. '?' will be replaced with generated number
                'digit' => 0 // optional, default to null.
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return self::getAttributesLabelText();
    }

    public static function getAttributesLabelText()
    {

        return [
            'id' => 'ID',
            'cal_retencion_fact_local_flete_seguro' => 'Calcular retención de facturas locales de flete y seguro',
            'iva' => 'I.V.A',
            'indi' => 'INDI',
            'derecho_aduanero' => 'Derecho Aduanero',
            'servicio_valoracion' => 'Servicion de Valoración',
            'iracis_generaral' => 'Iracis General',
            'tasa_interv_aduanera' => 'Tasa Intervención Aduanera',
            'repos_gtos_adm_annp' => 'Repos. Gtos. Adm. ANNP',
            'cdap_comis_can_annp' => 'Cdap. Comis. Can. ANNP',
            'sofia_comis_can_annp' => 'Sofia Comis. Can. ANNP',
            'multas_varias' => 'Multas Varias',
            'impuesto_selectivo_consumo' => 'Impuesto Selectivo al Consumo',
            'condicion' => 'Condición',
            'exportador' => 'Exportador',
            'despacho_moneda_id' => "Moneda usada en Despacho",
            'despacho_cotizacion' => "Cotizacion usada en Despacho",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneda()
    {
        return $this->hasOne(Moneda::className(), ['id' => 'moneda_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespachoMoneda()
    {
        return $this->hasOne(Moneda::className(), ['id' => 'despacho_moneda_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportacionDetallesLocal()
    {
        return $this->hasMany(ImportacionDetalleFacturaLocalExterior::className(), ['importacion_id' => 'id'])->andWhere(['es_local' => 'si']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportacionDetallesExterior()
    {
        return $this->hasMany(ImportacionDetalleFacturaLocalExterior::className(), ['importacion_id' => 'id'])->andWhere(['es_local' => 'no']);
    }

    public static function getEstados($filterMode = false)
    {
        $array = [];
        $i = 1;
        if ($filterMode) $array['todos'] = ($i++) . ' - Todos';
        $array['vigente'] = ($i++) . ' - Vigente';
        // more states to add goes here...
        return $array;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaCompra()
    {
        return $this->hasOne(Compra::className(), ['importacion_id' => 'id']);
    }

    public function getValorDespacho()
    {
        //Iracis no se suma. Pedido por Sara el 21/05/2019
        $suma = (float)$this->iva + (float)$this->indi + (float)$this->derecho_aduanero + (float)$this->servicio_valoracion +
            (float)$this->tasa_interv_aduanera + (float)$this->impuesto_selectivo_consumo + (float)$this->cannon_informatico_sistema_sofia + (float)$this->arancel_consular +
            (float)$this->repos_gtos_adm_annp + (float)$this->cdap_comis_can_annp + (float)$this->sofia_comis_can_annp + (float)$this->multas_varias;

        $sumaFacturas = 0;
        foreach ($this->importacionDetallesLocal as $detalleLocal) {
            if (strpos($detalleLocal->plantilla->para_importacion, 'flete') == false && !$detalleLocal->incluir_costo &&
                strpos($detalleLocal->plantilla->para_importacion, 'seguro') == false) {
                if ($detalleLocal->facturaCompra->moneda_id != 1) {
                    $sumaFacturas += ($detalleLocal->monto * $detalleLocal->cotizacion);
                } else {
                    $sumaFacturas += $detalleLocal->monto;
                }
            }
        }

        //Impuestos en gral
        $totalImpuestos = $this->iva != null ? $this->iva : 0;
        $impuestosAbsorber = 0;
        //facturas cuyo impuesto se absorbe
        foreach ($this->importacionDetallesLocal as $detalleLocal) {
            $ivaCuentaUsadas = CompraIvaCuentaUsada::find()
                ->where(['plantilla_id' => $detalleLocal->plantilla_id, 'factura_compra_id' => $detalleLocal->factura_compra_id])
                ->all();
            if ($detalleLocal->absorber_impuesto) {
                /** @var CompraIvaCuentaUsada $ivaCuentaUsada */
                foreach ($ivaCuentaUsadas as $ivaCuentaUsada) {
                    try {
                        $monto = $ivaCuentaUsada->monto - ($ivaCuentaUsada->monto / (1.00 + ($ivaCuentaUsada->ivaCta->iva->porcentaje / 100.0)));
                    } catch (\Exception $e) {
                        $monto = (float)$ivaCuentaUsada->monto;
                    }
                    if ($ivaCuentaUsada->iva_cta_id != null && $ivaCuentaUsada->ivaCta->iva->porcentaje != 0) {
                        if ($detalleLocal->facturaCompra->moneda_id != 1)
                            $monto *= $detalleLocal->cotizacion;
                        $impuestosAbsorber += $monto;
                    }
                }
            }

            $impuestos = 0;
            /** @var CompraIvaCuentaUsada $ivaCuentaUsada */
            foreach ($ivaCuentaUsadas as $ivaCuentaUsada) {
                if ($ivaCuentaUsada->ivaCta !== null) {
                    $monto = $ivaCuentaUsada->monto;
                    if ($detalleLocal->cotizacion != null && $detalleLocal->cotizacion != 0)
                        $monto = $monto * $detalleLocal->cotizacion;

                    $impuestos += $monto - ($monto / (1.00 + ($ivaCuentaUsada->ivaCta->iva->porcentaje / 100.0)));
                }
            }

            if ($impuestos != 0) {
                $totalImpuestos += round($impuestos);
            }
        }
        return round($suma + $sumaFacturas + $impuestosAbsorber - $totalImpuestos);
    }

    public function existeFleteSeguro()
    {
        $hayFlete = $haySeguro = false;
        foreach ($this->importacionDetallesLocal as $detalleLocal) {
            if (strpos($detalleLocal->plantilla->para_importacion, 'flete'))
                $hayFlete = true;
            else if (strpos($detalleLocal->plantilla->para_importacion, 'seguro'))
                $haySeguro = true;
        }

        return [0 => $hayFlete, 1 => $haySeguro];
    }

    public function getItemsLocalExteriorTr()
    {
        $totalItemExterior = 0;
        $totalItemRecambio = 0;
        $totalItemExteriorOriginal = 0;
        $trBlock = '';
        $arrayData = [];
        $valorDespacho = $this->getValorDespacho();
        $totalItem = $valorDespacho;

        //facturas del exterior
        foreach ($this->importacionDetallesExterior as $detalleExterior) {
            $monto = strpos($detalleExterior->plantilla->para_importacion, 'descuento') !== false ? -1 * $detalleExterior->monto : $detalleExterior->monto;
            $simbolo = $detalleExterior->facturaCompra->moneda->simbolo;
            $cotizacion = $detalleExterior->cotizacion;
            $total = round($monto * $detalleExterior->cotizacion);
            $trBlock .= "
                <tr>
                    <td>Valor de factura Exterior $simbolo</td>
                    <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($total) . "</td>
                </tr>
            ";

            $arrayData [] = ['item' => 'Valor de factura Exterior ' . $simbolo, 'monto' => $monto, 'cotizacion' => $cotizacion, 'total' => $total];
            $totalItem += ($monto * $detalleExterior->cotizacion);
            if (strpos($detalleExterior->plantilla->para_importacion, 'descuento') == false && strpos($detalleExterior->plantilla->para_importacion, 'otros_gastos') == false)
                $totalItemRecambio += $monto;
            $totalItemExterior += ($monto * $detalleExterior->cotizacion);
            $totalItemExteriorOriginal += $detalleExterior->monto;
        }

        $flsarray = $this->existeFleteSeguro();
        $hayFlete = $flsarray[0];
        $haySeguro = $flsarray[1];
        //Facturas locales de flete, seguro y otros gastos
        if ($this->flete_costo != '') {

            $cotizacion = (float)$this->despacho_cotizacion;

            /**
             * Solo si no hay factura de flete y seguro se alza como costo.
             */
            if (!$hayFlete && !$haySeguro) {
                $impuestos = 0;
                $total = ((float)$this->flete_costo - $impuestos) * ($this->despacho_cotizacion == null || (float)$this->despacho_cotizacion == 0 ? 1 : (float)$this->despacho_cotizacion);

                // Por Flete en la primera pestanha.
                $monto = (float)$this->flete_costo;
                $totalMostrar = round(((float)$this->flete_costo - $impuestos) * ($this->despacho_cotizacion == null || (float)$this->despacho_cotizacion == 0 ? 1 : (float)$this->despacho_cotizacion));

                $e = "Valor Flete";
                $trBlock .= "
                    <tr>
                        <td>{$e}</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($totalMostrar) . "</td>
                    </tr>
                ";

                $arrayData [] = ['item' => $e, 'monto' => $monto, 'cotizacion' => $cotizacion, 'total' => $total];
                $totalItem += $total;

                // Por Seguro en la primera pestanha.
                $impuestos = 0;
                $cotizacion = (float)$this->despacho_cotizacion;
                $monto = (float)$this->seguro_costo;
                $total = ((float)$this->seguro_costo - $impuestos) * ($this->despacho_cotizacion == null || (float)$this->despacho_cotizacion == 0 ? 1 : (float)$this->despacho_cotizacion);
                $totalMostrar = round(((float)$this->seguro_costo - $impuestos) * ($this->despacho_cotizacion == null || (float)$this->despacho_cotizacion == 0 ? 1 : (float)$this->despacho_cotizacion));

                $e = "Valor Seguro";
                $trBlock .= "
                    <tr>
                        <td>{$e}</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($totalMostrar) . "</td>
                    </tr>
                ";

                $arrayData [] = ['item' => $e, 'monto' => $monto, 'cotizacion' => $cotizacion, 'total' => $total];
                $totalItem += $total;
            }

            // Por Otros Gastos de la pestanha de Facturas Locales. Mismo codigo que la primera implementacion pero solo por Otros Gastos.
            foreach ($this->importacionDetallesLocal as $detalleLocal) {
                if ($detalleLocal->incluir_costo) {
                    $simbolo = $detalleLocal->facturaCompra->moneda->simbolo;
                    $cotizacion = $detalleLocal->cotizacion == null || $detalleLocal->cotizacion == 0 ? 0 : $detalleLocal->cotizacion;

                    $ivaCuentaUsadas = CompraIvaCuentaUsada::find()
                        ->where(['plantilla_id' => $detalleLocal->plantilla_id, 'factura_compra_id' => $detalleLocal->factura_compra_id])
                        ->all();
                    $impuestos = 0;
                    /** @var CompraIvaCuentaUsada $ivaCuentaUsada */
                    foreach ($ivaCuentaUsadas as $ivaCuentaUsada) {
                        if ($ivaCuentaUsada->ivaCta !== null) {
                            $impuestos += $ivaCuentaUsada->monto - ($ivaCuentaUsada->monto / (1.00 + ($ivaCuentaUsada->ivaCta->iva->porcentaje / 100.0)));
                        }
                    }
                    $monto = $detalleLocal->monto - $impuestos;
                    $total = ($detalleLocal->monto - $impuestos) * ($detalleLocal->cotizacion == null || $detalleLocal->cotizacion == 0 ? 1 : $detalleLocal->cotizacion);
                    $totalMostrar = round(($detalleLocal->monto - $impuestos) * ($detalleLocal->cotizacion == null || $detalleLocal->cotizacion == 0 ? 1 : $detalleLocal->cotizacion));

                    $e = explode('_', $detalleLocal->plantilla->para_importacion);
                    unset($e[0]);
                    $e = implode(' ', array_map("ucfirst", $e));
                    $trBlock .= "
                        <tr>
                            <td>{$e}</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($totalMostrar) . "</td>
                        </tr>
                    ";

                    $arrayData [] = ['item' => $e, 'monto' => $monto, 'cotizacion' => $cotizacion, 'total' => $total];
                    $totalItem += $total;
                }
            }
        }
        if ($haySeguro && $hayFlete) {
            foreach ($this->importacionDetallesLocal as $detalleLocal) {
                if (strpos($detalleLocal->plantilla->para_importacion, 'flete') !== false || strpos($detalleLocal->plantilla->para_importacion, 'seguro') !== false ||
                    $detalleLocal->incluir_costo) {
                    $cotizacion = $detalleLocal->cotizacion == null || $detalleLocal->cotizacion == 0 ? 0 : $detalleLocal->cotizacion;

                    $ivaCuentaUsadas = CompraIvaCuentaUsada::find()
                        ->where(['plantilla_id' => $detalleLocal->plantilla_id, 'factura_compra_id' => $detalleLocal->factura_compra_id])
                        ->all();
                    $impuestos = 0;
                    /** @var CompraIvaCuentaUsada $ivaCuentaUsada */
                    foreach ($ivaCuentaUsadas as $ivaCuentaUsada) {
                        if ($ivaCuentaUsada->ivaCta !== null) {
                            $impuestos += $ivaCuentaUsada->monto - ($ivaCuentaUsada->monto / (1.00 + ($ivaCuentaUsada->ivaCta->iva->porcentaje / 100.0)));
                        }
                    }
                    $monto = $detalleLocal->monto - $impuestos;
                    $total = ($detalleLocal->monto - $impuestos) * ($detalleLocal->cotizacion == null || $detalleLocal->cotizacion == 0 ? 1 : $detalleLocal->cotizacion);
                    $totalMostrar = round(($detalleLocal->monto - $impuestos) * ($detalleLocal->cotizacion == null || $detalleLocal->cotizacion == 0 ? 1 : $detalleLocal->cotizacion));

                    $e = explode('_', $detalleLocal->plantilla->para_importacion);
                    unset($e[0]);
                    $e = implode(' ', array_map("ucfirst", $e));
                    $trBlock .= "
                        <tr>
                            <td>{$e}</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($totalMostrar) . "</td>
                        </tr>
                    ";

                    $arrayData [] = ['item' => $e, 'monto' => $monto, 'cotizacion' => $cotizacion, 'total' => $total];
                    $totalItem += $total;
                }
            }
        }

        return [
            'trblock' => $trBlock,
            'arraydata' => $arrayData,
            'totalitem' => $totalItem,
            'totalitemexterior' => $totalItemExterior,
            'totalitemexteriororiginal' => $totalItemExteriorOriginal,
            'totalItemRecambio' => $totalItemRecambio
        ];
    }

    public function getIvasTr()
    {
        $trBlock = '';
        $arrayData = [];
        $total = $this->iva != null ? $this->iva : 0;
        foreach ($this->importacionDetallesLocal as $detalleLocal) {
            $ivaCuentaUsadas = CompraIvaCuentaUsada::find()
                ->where(['plantilla_id' => $detalleLocal->plantilla_id, 'factura_compra_id' => $detalleLocal->factura_compra_id])
                ->all();
            $impuestos = 0;
            /** @var CompraIvaCuentaUsada $ivaCuentaUsada */
            foreach ($ivaCuentaUsadas as $ivaCuentaUsada) {
                if ($ivaCuentaUsada->ivaCta !== null) {
                    $monto = $ivaCuentaUsada->monto;
                    if ($detalleLocal->cotizacion != null && $detalleLocal->cotizacion != 0)
                        $monto = $monto * $detalleLocal->cotizacion;

                    $impuestos += $monto - ($monto / (1.00 + ($ivaCuentaUsada->ivaCta->iva->porcentaje / 100.0)));
                }
            }

            $e = explode('_', $detalleLocal->plantilla->para_importacion);
            unset($e[0]);
            $e = implode(' ', array_map("ucfirst", $e));
            $impuestoNombre = $detalleLocal->facturaCompra->entidad->razon_social . ' - ' . $e;
            if ($impuestos != 0) {
                $valor = round($impuestos);
                $total += $valor;
                $trBlock .= "
                    <tr>
                        <td>{$impuestoNombre}</td>
                        <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($valor) . "</td>
                    </tr>
                ";
                $arrayData [] = ['item' => $impuestoNombre, 'monto' => $valor];
            }
        }

        return [
            'trblock' => $trBlock,
            'total' => $total,
            'arraydata' => $arrayData,
        ];
    }

    public function getRetencionesTr()
    {
        $rentaTr = '';
        $ivaTr = '';
        $arrayDataRenta = [];
        $arrayDataIva = [];
        $totalPagar = 0;
        $totalRetencionRenta = 0;
        $totalRetencionIva = 0;

        $flsarray = $this->existeFleteSeguro();
        $hayFlete = $flsarray[0];
        $haySeguro = $flsarray[1];

        if ($this->flete_costo != null && $this->seguro_costo != null) {
            $cotizacion = $this->despacho_cotizacion == null || $this->despacho_cotizacion == 0 ? 0 : $this->despacho_cotizacion;

            /**
             * Retencion de renta se puede calcular para condicion cif o fob. Además de otros casos.
             * Si no hay factura local re flete se hace retencion de seguro y viceversa.
             */
            if ($this->condicion == 'cif' or $this->condicion == 'fob') {
                $porcentajeRetencioFleteSeguro = $this->medio_transporte == 'terrestre' ? 97.73 : 100;
                if (!$haySeguro) {
                    $renta = (($this->flete_costo * $porcentajeRetencioFleteSeguro) / 100) * 0.03;
                    $total = $cotizacion != 0 ? $renta * $cotizacion : $renta;
                    $rentaTr .= "
                        <tr>
                            <td>Flete</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($this->flete_costo) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($porcentajeRetencioFleteSeguro) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($renta) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive(round($total)) . "</td>
                        </tr>
                    ";
                    $totalRetencionRenta += round($total);
                    $arrayDataRenta[] = ['item' => 'Flete', 'monto' => $this->flete_costo, '%' => $porcentajeRetencioFleteSeguro, 'renta' => $renta, 'cotizacion' => $cotizacion, 'total' => round($total)];
                    $totalPagar += round($total);
                }

                if (!$hayFlete) {
                    $renta = (($this->seguro_costo * $porcentajeRetencioFleteSeguro) / 100) * 0.03;
                    $total = $cotizacion != 0 ? $renta * $cotizacion : $renta;
                    $rentaTr .= "
                        <tr>
                            <td>Seguro</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($this->seguro_costo) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($porcentajeRetencioFleteSeguro) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($renta) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive(round($total)) . "</td>
                        </tr>
                    ";
                    $totalRetencionRenta += round($total);
                    $arrayDataRenta[] = ['item' => 'Seguro', 'monto' => $this->seguro_costo, '%' => $porcentajeRetencioFleteSeguro, 'renta' => $renta, 'cotizacion' => $cotizacion, 'total' => round($total)];
                    $totalPagar += round($total);
                }

                /**
                 * Retención de IVA se puede calcular solo para condicion fob. Además de otros casos.
                 */
                if ($this->condicion == 'fob' && $this->medio_transporte != 'aereo') {
                    $porcentajeRetencionIvaFleteSeguro = 2.27;
                    if (!$haySeguro) {
                        $iva = ($this->flete_costo * $porcentajeRetencionIvaFleteSeguro) / 100;
                        $total = $cotizacion != 0 ? $iva * $cotizacion : $iva;
                        $totalPagar += $total;
                        $ivaTr .= "
                            <tr>
                                <td>Flete</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($this->flete_costo) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($porcentajeRetencionIvaFleteSeguro) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($iva) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive(round($total)) . "</td>
                            </tr>
                        ";
                        $totalRetencionIva += round($total);
                        $arrayDataIva[] = ['item' => 'Flete', 'monto' => $this->flete_costo, '%' => $porcentajeRetencionIvaFleteSeguro, 'iva' => $iva, 'cotizacion' => $cotizacion, 'total' => round($total)];
                    }

                    if (!$hayFlete && $this->medio_transporte != 'aereo') {
                        $iva = ($this->seguro_costo * $porcentajeRetencionIvaFleteSeguro) / 100;
                        $total = $cotizacion != 0 ? $iva * $cotizacion : $iva;
                        $totalPagar += $total;
                        $ivaTr .= "
                            <tr>
                                <td>Seguro</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($this->seguro_costo) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($porcentajeRetencionIvaFleteSeguro) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($iva) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive(round($total)) . "</td>
                            </tr>
                        ";
                        $totalRetencionIva += round($total);
                        $arrayDataIva[] = ['item' => 'Seguro', 'monto' => $this->seguro_costo, '%' => $porcentajeRetencionIvaFleteSeguro, 'iva' => $iva, 'cotizacion' => $cotizacion, 'total' => round($total)];
                    }
                }
            }
        } else {
            //Facturas locales de flete, seguro
            foreach ($this->importacionDetallesLocal as $detalleLocal) {
                if (strpos($detalleLocal->plantilla->para_importacion, 'flete') !== false || strpos($detalleLocal->plantilla->para_importacion, 'seguro') !== false) {
                    $cotizacion = $detalleLocal->cotizacion == null || $detalleLocal->cotizacion == 0 ? 0 : $detalleLocal->cotizacion;

                    //Para saber si es flete o seguro
                    $esFlete = strpos($detalleLocal->plantilla->para_importacion, 'flete') !== false;
                    $esSeguro = strpos($detalleLocal->plantilla->para_importacion, 'seguro') !== false;

                    $ivaCuentaUsadas = CompraIvaCuentaUsada::find()->where(['plantilla_id' => $detalleLocal->plantilla_id, 'factura_compra_id' => $detalleLocal->factura_compra_id])->all();
                    $impuestos = 0;
                    /** @var CompraIvaCuentaUsada $ivaCuentaUsada */
                    foreach ($ivaCuentaUsadas as $ivaCuentaUsada) {
                        if ($ivaCuentaUsada->ivaCta !== null) {
                            $impuestos += $ivaCuentaUsada->monto - ($ivaCuentaUsada->monto / (1.00 + ($ivaCuentaUsada->ivaCta->iva->porcentaje / 100.0)));
                        }
                    }
                    $e = explode('_', $detalleLocal->plantilla->para_importacion);
                    unset($e[0]);
                    $e = implode(' ', array_map("ucfirst", $e));
                    $monto = $detalleLocal->monto - $impuestos;

                    /**
                     * Retencion de renta se puede calcular para condicion cif o fob. Además de otros casos.
                     */
                    if (($this->condicion == 'cif' or $this->condicion == 'fob') && (($esFlete && !$haySeguro) or ($esSeguro && !$hayFlete))) {
                        $porcentajeRetencioFleteSeguro = $this->medio_transporte == 'terrestre' ? 97.73 : 100;
                        $renta = (($monto * $porcentajeRetencioFleteSeguro) / 100) * 0.03;
                        $total = $renta * $cotizacion;
                        $totalPagar += $total;
                        $rentaTr .= "
                            <tr>
                                <td>{$e}</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($porcentajeRetencioFleteSeguro) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($renta) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive(round($total)) . "</td>
                            </tr>
                        ";
                        $totalRetencionRenta += round($total);
                        $arrayDataRenta[] = ['item' => $e, 'monto' => $monto, '%' => $porcentajeRetencioFleteSeguro, 'renta' => $renta, 'cotizacion' => $cotizacion, 'total' => round($total)];
                    }

                    /**
                     * Retención de IVA se puede calcular solo para condicion fob. Además de otros casos.
                     */
                    if ($this->condicion == 'fob' && (($esFlete && !$haySeguro) or ($esSeguro && !$hayFlete)) && $this->medio_transporte != 'aereo') {
                        $porcentajeRetencionIvaFleteSeguro = 2.27;
                        $iva = ($monto * $porcentajeRetencionIvaFleteSeguro) / 100;
                        $total = $iva * $cotizacion;
                        $totalPagar += $total;
                        $ivaTr .= "
                            <tr>
                                <td>{$e}</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($porcentajeRetencionIvaFleteSeguro) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($iva) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive(round($total)) . "</td>
                            </tr>
                        ";
                        $totalRetencionIva += round($total);
                        $arrayDataIva[] = ['item' => $e, 'monto' => $monto, '%' => $porcentajeRetencionIvaFleteSeguro, 'iva' => $iva, 'cotizacion' => $cotizacion, 'total' => round($total)];
                    }
                }
            }

            //Facturas del exterior de flete, seguro
            foreach ($this->importacionDetallesExterior as $detalleExterior) {
                if (strpos($detalleExterior->plantilla->para_importacion, 'flete') !== false || strpos($detalleExterior->plantilla->para_importacion, 'seguro') !== false) {
                    $cotizacion = $detalleExterior->cotizacion == null || $detalleExterior->cotizacion == 0 ? 0 : $detalleExterior->cotizacion;

                    $e = explode('_', $detalleExterior->plantilla->para_importacion);
                    unset($e[0]);
                    $e = implode(' ', array_map("ucfirst", $e));
                    $monto = $detalleExterior->monto;
                    $porcentajeRetencioFleteSeguro = $this->medio_transporte == 'terrestre' ? 97.73 : 100;
                    $renta = (($monto * $porcentajeRetencioFleteSeguro) / 100) * 0.03;
                    $total = $renta * $cotizacion;
                    $totalPagar += $total;
                    $rentaTr .= "
                        <tr>
                            <td>{$e}</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($porcentajeRetencioFleteSeguro) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($renta) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                            <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive(round($total)) . "</td>
                        </tr>
                    ";
                    $totalRetencionRenta += round($total);
                    $arrayDataRenta[] = ['item' => $e, 'monto' => $monto, '%' => $porcentajeRetencioFleteSeguro, 'renta' => $renta, 'cotizacion' => $cotizacion, 'total' => round($total)];

                    if ($this->medio_transporte != 'aereo') {
                        $porcentajeRetencionIvaFleteSeguro = 2.27;
                        $iva = ($monto * $porcentajeRetencionIvaFleteSeguro) / 100;
                        $total = $iva * $cotizacion;
                        $totalPagar += $total;
                        $ivaTr .= "
                            <tr>
                                <td>{$e}</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($monto) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($porcentajeRetencionIvaFleteSeguro) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($iva) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive($cotizacion) . "</td>
                                <td class='monto'>" . ValorHelpers::numberFormatMonedaSensitive(round($total)) . "</td>
                            </tr>
                        ";
                        $totalRetencionIva += round($total);
                        $arrayDataIva[] = ['item' => $e, 'monto' => $monto, '%' => $porcentajeRetencionIvaFleteSeguro, 'iva' => $iva, 'cotizacion' => $cotizacion, 'total' => round($total)];
                    }
                }
            }
        }

        return [
            'ivatr' => $ivaTr,
            'rentatr' => $rentaTr,
            'totalpagar' => $totalPagar,
            'arraydataiva' => $arrayDataIva,
            'arraydatarenta' => $arrayDataRenta,
            'totalRetencionIva' => $totalRetencionIva,
            'totalRetencionRenta' => $totalRetencionRenta
        ];
    }
}