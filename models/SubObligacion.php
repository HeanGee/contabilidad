<?php

namespace backend\modules\contabilidad\models;

use Yii;

/**
 * This is the model class for table "{{%cont_sub_obligacion}}".
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $estado
 * @property int $obligacion_id
 *
 * @property Obligacion $obligacion
 * @property string $formattedEstado
 */
class SubObligacion extends \backend\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cont_sub_obligacion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'obligacion_id'], 'required'],
            [['estado'], 'string'],
            [['obligacion_id'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 256],
            [['obligacion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Obligacion::className(), 'targetAttribute' => ['obligacion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'estado' => Yii::t('app', 'Estado'),
            'obligacion_id' => Yii::t('app', 'Obligacion ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObligacion()
    {
        return $this->hasOne(Obligacion::className(), ['id' => 'obligacion_id']);
    }

    public static function getEstados($isForFilteringBy = false)
    {
        $array = [];
        !$isForFilteringBy || $array['todos'] = "Todos";
        $array['activo'] = "Activo";
        $array['inactivo'] = "Inactivo";

        return $array;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     * @throws \Exception
     */
    public static function getObligaciones()
    {
        $query = Obligacion::find()->alias('ob')
            ->select(['id' => 'ob.id', 'text' => 'ob.nombre'])
            ->orderBy(['ob.nombre' => SORT_ASC]);

        $array = $query->asArray()->all();
        return $array;
    }

    public function getFormattedEstado()
    {
        return ucfirst($this->estado);
    }
}
