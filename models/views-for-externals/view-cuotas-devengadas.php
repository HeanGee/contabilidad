<?php

use backend\modules\contabilidad\models\Asiento;
use backend\modules\contabilidad\models\Prestamo;
use kartik\grid\GridView;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yiister\adminlte\widgets\Box;

/** @var $query Query */
?>

<?php if (isset($query) && $query->exists()) {
    Box::begin([
        "header" => "Asientos de devengamientos de préstamos.",
        "type" => Box::TYPE_INFO,
        "filled" => true,
    ]);

    Pjax::begin(['id' => 'dev-prestamos']);
    try {
        echo GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $query, 'pagination' => false]),
            'rowOptions' => function ($model, $key, $index) {
//                $hoy = date('Y-m-d');
//                $url = 'index.php?ChequeClienteSearch[id]=' . $model->id . '&r=administracion/cheque-cliente/index';
//                if ($model->fecha_cobro != null) {
//                    if ($model->fecha_cobro < $hoy) {
//                        return ['class' => 'danger', 'onclick' => 'location.href="' . $url . '"'];
//                    }
//                }
//                $url = 'index.php?ChequeClienteSearch[id]=' . $model->id . '&r=administracion/cheque-cliente/index';
//                return ['onclick' => 'location.href="' . $url . '"'];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'nro_cuota',
                    'value' => function ($model, $key, $index, $column) {
                        return $model['nro_cuota'];
                    },
                    'label' => "Nro de Cuota",
                ],
                [
                    'attribute' => 'prestamo_id',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        $prestamo = Prestamo::findOne(['id' => $model['prestamo_id']]);
                        $fecha_emision = implode('-', array_reverse(explode('-', $prestamo->fecha_operacion)));
                        $url = Url::to(['contabilidad/prestamo/view', 'id' => $model['prestamo_id']]);
                        return Html::a("Préstamo <strong>#{$model['prestamo_id']}</strong> de {$prestamo->entidad->razon_social} emitido el {$fecha_emision}", $url);
                    },
                    'label' => "Préstamo",
                ],
                [
                    'attribute' => 'asiento_devengamiento_id',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        $asiento = Asiento::findOne(['id' => $model['asiento_devengamiento_id']]);
                        $fecha_emision = implode('-', array_reverse(explode('-', $asiento->fecha)));
                        $url = Url::to(['contabilidad/asiento/view', 'id' => $model['asiento_devengamiento_id']]);
                        return Html::a("Asiento <strong>Id = {$model['asiento_devengamiento_id']}</strong>", $url);
                    },
                    'label' => "Préstamo",
                ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    Pjax::end();
    Box::end();
}