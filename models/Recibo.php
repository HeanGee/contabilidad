<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;
use backend\models\Moneda;
use backend\modules\contabilidad\helpers\ParametroSistemaHelpers;
use backend\modules\contabilidad\models\validators\ReciboNroValidator;
use backend\modules\contabilidad\models\validators\ReciboRucValidator;
use common\helpers\FlashMessageHelpsers;
use common\helpers\ValorHelpers;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cont_recibo".
 *
 * En la simulacion de asiento, se puede incluir el asiento por retenciones existentes por cada factura.
 * Dicho se muestran solamente si en la empresa actual esta parametrizado la cuenta contable
 * correspondiente si es compra; si es venta, muestra siempre.
 *
 * Si existe retenciones, a la suma de los montos de las contracuentas se le suma el monto de las retenciones.
 * En el lado de la cuenta contable por factura, se inserta una cuenta independiente para retenciones con el monto
 * de las retenciones. Dicha cuenta independiente es la que trae de la empresa actual.
 *
 * La sumatoria de las contracuentas debe ser igual a la sumatoria de los montos de facturas en cancelacion.
 *
 * El campo total puede valer o la suma de los montos de facturas en cancelacion o dicha suma más un monto extra que
 * se abona demás pero que no participa en el asiento.
 *
 * En la simulación del asiento va a aparecer automáticamente sumado el monto de todas las retenciones de la factura
 * en el valor de la cuenta del tipo de documento de dicha factura.
 *
 * @property string $id
 * @property string $numero
 * @property string $fecha
 * @property int $moneda_id
 * @property int $entidad_id
 * @property int $empresa_id
 * @property int $periodo_contable_id
 * @property int $asiento_id
 * @property string $valor_moneda
 * @property string $factura_tipo
 * @property string $bloqueado
 * @property string $monto_extra
 * @property string $total
 *
 * @property string $ruc
 * @property string $razon_social
 * @property string $valorMonedaFormatted
 * @property string $monedaNombre
 * @property string $totalFormatted
 *
 * @property Moneda $moneda
 * @property ReciboDetalle[] $reciboDetalles
 * @property ReciboDetalleContracuentas[] $reciboDetalleContracuentas
 * @property Venta[] ventas
 * @property Compra[] compras
 * @property Entidad entidad
 * @property Empresa empresa
 * @property Asiento $asiento
 * @property EmpresaPeriodoContable periodoContable
 */
class Recibo extends BaseModel
{
    public $ruc;
    public $razon_social;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_recibo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['numero', 'fecha', 'moneda_id', 'ruc'], 'required'],
            [['numero', 'fecha', 'moneda_id', 'entidad_id', 'factura_tipo', 'empresa_id', 'periodo_contable_id', 'bloqueado', 'total', 'monto_extra'], 'required'],
            [['fecha', 'ruc', 'razon_social', 'total', 'factura_tipo', 'monto_extra', 'asiento_id'], 'safe'],
            [['moneda_id', 'entidad_id'], 'integer'],
            [['valor_moneda', 'total', 'monto_extra'], 'number'],
//            [['numero'], 'string', 'max' => 7],
//            [['numero'], 'string', 'min' => 7],
            [['factura_tipo', 'bloqueado'], 'string'],
            [['monto_extra'], 'montoExtraValidator'],
            [['total'], 'totalValidator'],

            //trim values
            [['numero', 'fecha', 'razon_social', 'valor_moneda'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],

            [['numero', 'entidad_id'], ReciboNroValidator::className()],
            [['ruc'], ReciboRucValidator::className()],

            [['moneda_id'], 'exist', 'skipOnError' => true, 'targetClass' => Moneda::className(), 'targetAttribute' => ['moneda_id' => 'id']],
            [['entidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entidad::className(), 'targetAttribute' => ['entidad_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['asiento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
        ];
    }

    public function montoExtraValidator($attribute, $params)
    {
        \Yii::$app->session->set('params', $params);
        if ($this->$attribute < 0)
            $this->addError($attribute, "Monto extra no puede ser negativo");
    }

    public function totalValidator($attribute, $params)
    {
        if ($this->$attribute <= 0)
            $this->addError($attribute, "Total no puede ser menor ni igual a cero");
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return self::getAttributesLabelText();
    }

    public static function getAttributesLabelText()
    {
        return [
            'id' => 'ID',
            'numero' => 'Numero',
            'fecha' => 'Fecha',
            'moneda_id' => 'Moneda ID',
            'valor_moneda' => 'Valor Moneda',
            'ruc' => 'R.U.C.',
            'entidad_id' => "Entidad",
            'factura_tipo' => "Tipo de Factura",
            'empresa_id' => "Empresa Id",
            'periodo_contable_id' => "Periodo Contable Id",
            'monto_extra' => "Monto Excedente",
        ];
    }

    public static function manejarTextoAuditoria($campo, &$viejo, &$nuevo)
    {
        switch ($campo) {
            case 'entidad_id':
                $viejo[0] = !empty($viejo[0]) ? Entidad::findOne($viejo[0])->razon_social : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Entidad::findOne($nuevo[0])->razon_social : $nuevo[0];
                return true;
            case 'moneda_id':
                $viejo[0] = !empty($viejo[0]) ? Moneda::findOne($viejo[0])->nombre : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Moneda::findOne($nuevo[0])->nombre : $nuevo[0];
                return true;
            case 'empresa_id':
                $viejo[0] = !empty($viejo[0]) ? Empresa::findOne($viejo[0])->razon_social : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? Empresa::findOne($nuevo[0])->razon_social : $nuevo[0];
                return true;
            case 'periodo_contable_id':
                $viejo[0] = !empty($viejo[0]) ? EmpresaPeriodoContable::findOne($viejo[0])->id : $viejo[0];
                $nuevo[0] = !empty($nuevo[0]) ? EmpresaPeriodoContable::findOne($nuevo[0])->id : $nuevo[0];
                return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneda()
    {
        return $this->hasOne(Moneda::className(), ['id' => 'moneda_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidad()
    {
        return $this->hasOne(Entidad::className(), ['id' => 'entidad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsiento()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboDetalles()
    {
        return $this->hasMany(ReciboDetalle::className(), ['recibo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReciboDetalleContracuentas()
    {
        return $this->hasMany(ReciboDetalleContracuentas::className(), ['recibo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        /** @var ReciboDetalle[] $recibo_detalles */
        $recibo_detalles = ReciboDetalle::find()->where(['factura_tipo' => 'venta'])->andWhere(['recibo_id' => $this->id])->all();
        $id_ventas = [];
        foreach ($recibo_detalles as $recibo_detalle) {
            $id_ventas[] = $recibo_detalle->factura_venta_id;
        }
        return Venta::find()->where(['IN', 'id', $id_ventas]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        /** @var ReciboDetalle[] $recibo_detalles */
        $recibo_detalles = ReciboDetalle::find()->where(['factura_tipo' => 'compra'])->andWhere(['recibo_id' => $this->id])->all();
        $id_ventas = [];
        foreach ($recibo_detalles as $recibo_detalle) {
            $id_ventas[] = $recibo_detalle->factura_compra_id;
        }
        return Compra::find()->where(['IN', 'id', $id_ventas]);
    }

    public static function getMonedas($onlyIdAndText = false)
    {
        $query = Moneda::find()->alias('moneda');
        $query_sentence = [
            'moneda.id as id',
            "CONCAT((moneda.id), (' - '), (moneda.nombre)) AS text",
            'moneda.tiene_decimales',
        ];
        if ($onlyIdAndText) {
            $query_sentence = ['moneda.id', "moneda.nombre AS text"];
            $array['todos'] = 'Todos';
            $map = ArrayHelper::map($query->select($query_sentence)->asArray()->all(), 'id', 'text');
            foreach ($map as $key => $value) {
                $array[$key] = $value;
            }
            return $array;
        }
        $query->select($query_sentence);
        $result = $query->asArray()->all();
        if (sizeof($result) == 0)
            FlashMessageHelpsers::createWarningMessage('Falta cargar la cotización del día');
        return $result;
    }

    public function getValorMonedaFormatted()
    {
        $valor_moneda = ValorHelpers::numberFormatMonedaSensitive($this->valor_moneda, 2, $this->moneda);
        if ($valor_moneda == 0) return "-sin cotización-";
        return $valor_moneda;
    }

    public function getMonedaNombre()
    {
        return $this->moneda->nombre;
    }

    public function getTotalFormatted()
    {
        $query = ReciboDetalle::find()->alias('detalle');
        $query->joinWith('recibo as recibo');
        $query->select([
            'MIN(detalle.recibo_id) as recibo_id',
            'SUM(detalle.monto) as monto',
        ]);
        $query->where([
            'recibo.id' => $this->id,
            'recibo.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'recibo.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $moneda_base = ParametroSistemaHelpers::getValorByNombre('moneda_base') == null ? 1 : ParametroSistemaHelpers::getValorByNombre('moneda_base');
        return number_format($query->asArray()->all()[0]['monto'], ($this->moneda_id == $moneda_base ? 0 : 2), ',', '.');
    }

    public function beforeValidate()
    {
        if ($this->bloqueado == null)
            $this->bloqueado = 'no';

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function getSumaryDetalles()
    {
        $suma = 0;
        foreach ($this->reciboDetalles as $reciboDetalle) {
            $cotizacion = ($reciboDetalle->cotizacion == 0 || $reciboDetalle->cotizacion == '') ? 1 : round($reciboDetalle->cotizacion, 2);
            $suma += round($reciboDetalle->monto, 2) * $cotizacion;
        }
        return $suma;
    }

    /**
     * @param string $operacion
     * @return bool|Venta|Compra
     */
    public function hayFacturasNoAsentadas($operacion = 'compra')
    {
        foreach ($this->reciboDetalles as $detalle) {
            if (!isset($detalle->$operacion->asiento))
                return $detalle->$operacion;
        }
        return false;
    }

    /**
     * @param null|string $empresa_id
     * @param null|string $periodo_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getReciboConExtra($empresa_id = null, $periodo_id = null)
    {
        return Recibo::find()
            ->where(['!=', 'monto_extra', 0])
            ->andFilterWhere(['empresa_id' => $empresa_id])
            ->andFilterWhere(['periodo_contable_id' => $periodo_id])
            ->all();
    }

    public function checkEditability()
    {
        $msg = '';
        if (isset($this->asiento))
            $msg = "Este recibo tiene asiento (id: {$this->asiento->id})";

        return $msg;
    }
}
