<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 09/07/2018
 * Time: 11:45
 */

namespace backend\modules\contabilidad\models\forms;

use backend\modules\contabilidad\models\Recibo;
use backend\modules\contabilidad\models\ReciboDetalle;
use Yii;
use yii\base\Model;

/**
 * @property Recibo $recibo
 * @property ReciboDetalle $reciboDetalle
 * @property ReciboDetalle[] $reciboDetalles
 */
class ReciboForm extends Model
{
    private $_recibo;
    private $_reciboDetalles;

    public function rules()
    {
        return [
            [['Recibo'], 'required'],
            [['ReciboDetalles'], 'safe'],
        ];
    }

    public function afterValidate()
    {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    private function getAllModels()
    {
        $models = [
            'Recibo' => $this->recibo,
        ];
        foreach ($this->reciboDetalles as $id => $reciboDetalle) {
            $models['ReciboDetalle.' . $id] = $this->reciboDetalles[$id];
        }
        return $models;
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->recibo->save()) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->saveReciboDetalles()) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function saveReciboDetalles()
    {
        $keep = [];
        /** @var Recibo $reciboDetalle */
        foreach ($this->reciboDetalles as $reciboDetalle) {
            $reciboDetalle->recibo_id = $this->recibo->id;
            if (!$reciboDetalle->save(false)) {
                return false;
            }
            $keep[] = $reciboDetalle->id;
        }
        $query = ReciboDetalle::find()->andWhere(['recibo_id' => $this->recibo->id]);
        if ($keep) {
            $query->andWhere(['not in', 'id', $keep]);
        }
        foreach ($query->all() as $reciboDetalle) {
            $reciboDetalle->delete();
        }
        return true;
    }

    /**
     * @return Recibo
     */
    public function getRecibo()
    {
        return $this->_recibo;
    }

    public function setRecibo($recibo)
    {
        if ($recibo instanceof Recibo) {
            $this->_recibo = $recibo;
        } else if (is_array($recibo)) {
            $this->_recibo->setAttributes($recibo);
        }
    }

    /**
     * @return ReciboDetalle[]
     */
    public function getReciboDetalles()
    {
        if ($this->_reciboDetalles === null) {
            $this->_reciboDetalles = $this->recibo->isNewRecord ? [] : $this->recibo->reciboDetalles;
        }
        return $this->_reciboDetalles;
    }

    public function setReciboDetalles($reciboDetalles)
    {
        unset($reciboDetalles['__id__']); // remove the hidden "new Parcel" row
        $this->_reciboDetalles = [];
        foreach ($reciboDetalles as $key => $reciboDetalle) {
            if (is_array($reciboDetalle)) {
                $this->_reciboDetalles[$key] = $this->getReciboDetalle($key);
                $this->_reciboDetalles[$key]->setAttributes($reciboDetalle);
            } elseif ($reciboDetalle instanceof ReciboDetalle) {
                $this->_reciboDetalles[$reciboDetalle->id] = $reciboDetalle;
            }
        }
    }

    /**
     * @param $key
     * @return ReciboDetalle|bool|null|static
     */
    private function getReciboDetalle($key)
    {
        $reciboDetalle = $key && strpos($key, 'new') === false ? ReciboDetalle::findOne($key) : false;
        if (!$reciboDetalle) {
            $reciboDetalle = new ReciboDetalle();
            $reciboDetalle->loadDefaultValues();
        }
        return $reciboDetalle;
    }

    public function errorSummary($form)
    {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }
}