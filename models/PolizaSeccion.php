<?php

namespace backend\modules\contabilidad\models;

/**
 * This is the model class for table "cont_poliza_seccion".
 *
 * @property string $id
 * @property string $nombre
 * @property string $estado
 *
 * @property Poliza[] $polizas
 */
class PolizaSeccion extends \backend\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_poliza_seccion';
    }

    public static function selectSeccion($seccion_id, $id_text_format = false)
    {
        if ($seccion_id != null) {
            $seccions = PolizaSeccion::getSecciones($id_text_format);
            foreach ($seccions as $key => $seccion) {
                if ($id_text_format) {
                    if ($seccion['id'] == $seccion_id) return $seccion['text'];
                } else
                    if ($key == $seccion_id) return $seccion;
            }
        }
        return "";
    }

    public static function getSecciones($id_text_format = false)
    {
        $result = [];
        $secciones = PolizaSeccion::find()->all();
        foreach ($secciones as $seccion) {
            if ($id_text_format) $result[] = ['id' => $seccion->id, 'text' => $seccion->nombre];
            else $result[$seccion->id] = $seccion->nombre;
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['estado'], 'string'],
            [['nombre'], 'string', 'max' => 45],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolizas()
    {
        return $this->hasMany(Poliza::className(), ['seccion_id' => 'id']);
    }
}
