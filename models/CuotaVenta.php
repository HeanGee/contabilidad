<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;

/**
 * This is the model class for table "cont_cuota_factura_venta".
 *
 * @property int $id
 * @property int $factura_venta_id
 * @property string $fecha_vencimiento
 * @property string $monto
 * @property int $nro_cuota
 * @property string $pagado
 *
 * @property Venta $facturaVenta
 */
class CuotaVenta extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_cuota_factura_venta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['factura_venta_id', 'fecha_vencimiento', 'monto', 'nro_cuota', 'pagado'], 'required'],
            [['id', 'factura_venta_id', 'nro_cuota'], 'integer'],
            [['fecha_vencimiento'], 'safe'],
//            [['monto'], 'number'],
            [['pagado'], 'string'],
            [['id'], 'unique'],
            [['factura_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['factura_venta_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'factura_venta_id' => 'Factura Venta ID',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'monto' => 'Monto',
            'nro_cuota' => 'Nro Cuota',
            'pagado' => 'Pagado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'factura_venta_id']);
    }
}
