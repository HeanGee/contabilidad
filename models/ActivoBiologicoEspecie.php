<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;
use Yii;

/**
 * This is the model class for table "cont_activo_biologico_especie".
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property int $empresa_id
 * @property int $periodo_contable_id
 *
 * @property ActivoBiologico[] $activoBiologicos
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoContable
 * @property ActivoBiologicoEspecieClasificacion[] $contActivoBiologicoEspecieClasificacions
 */
class ActivoBiologicoEspecie extends \backend\models\BaseModel
{
    public $guardar_cerrar;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_activo_biologico_especie';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'empresa_id', 'periodo_contable_id'], 'required'],
            [['empresa_id', 'periodo_contable_id'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 256],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],

            /** personal rules */
            [['nombre'], 'nombreUnico'],
            [['guardar_cerrar'], 'safe']
        ];
    }

    function nombreUnico($attribute, $params)
    {
        $query = ActivoBiologicoEspecie::find()->where([
            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
            'nombre' => $this->$attribute,
        ])->andWhere(['!=', 'id', $this->id]);

        if ($query->exists())
            $this->addError($attribute, "'{$this->$attribute}' ya ha sido utilizado para la empresa y periodo actual.");
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'empresa_id' => 'Empresa ID',
            'periodo_contable_id' => 'Periodo Contable ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoBiologicos()
    {
        return $this->hasMany(ActivoBiologico::className(), ['especie' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoBiologicoEspecieClasificacions()
    {
        return $this->hasMany(ActivoBiologicoEspecieClasificacion::className(), ['activo_biologico_especie_id' => 'id']);
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->empresa_id = Yii::$app->session->get('core_empresa_actual');
        $this->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

        return parent::loadDefaultValues($skipIfSet); // TODO: Change the autogenerated stub
    }

    public function isDeleteable()
    {
        $actbio = ActivoBiologico::find()->where(['especie_id' => $this->id]);
        $clasif = ActivoBiologicoEspecieClasificacion::find()->where(['activo_biologico_especie_id' => $this->id]);

        return !$actbio->exists() && !$clasif->exists();
    }

    public static function getEspeciesLista()
    {
        $i = 1;
        $_retornar = [];
        $_retornar[] = ['id' => '', 'text' => ''];
        $especies = ActivoBiologicoEspecie::find()->where(['IS NOT', 'id', null])
            ->andWhere([
                'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])->all();

        $hay_especies = sizeof($especies) > 0;
        foreach ($especies as $_v) {
            $_retornar[] = ['id' => $_v->id, 'text' => $i++ . '. ' . ucfirst($_v->nombre)];
        }

        if (!$hay_especies)
            return [];

        return $_retornar;
    }

    public static function getEspecieNombre($id)
    {
        $array = self::getEspeciesLista();

        foreach ($array as $item) {
            if ($item['id'] == $id)
                return $item['text'];
        }

        return "";
    }

    public function beforeValidate()
    {
        $this->nombre = ucfirst($this->nombre);
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }
}
