<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use common\helpers\ValorHelpers;

/**
 * This is the model class for table "cont_detalle_factura_compra".
 *
 * @property int $id
 * @property int $factura_compra_id
 * @property float $subtotal
 * @property string $plan_cuenta_id
 * @property string $cta_contable
 * @property int $periodo_contable_id
 * @property int $prestamo_cuota_id
 * @property string $cta_principal
 * @property string $agrupar
 * @property int $asiento_id_por_cuota_prestamo
 *
 * @property Compra $facturaCompra
 * @property PlanCuenta $planCuenta
 * @property EmpresaPeriodoContable $periodoContable
 */
class DetalleCompra extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
//        return 'cont_detalle_factura_compra';
        return 'cont_factura_compra_detalle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['factura_compra_id', 'subtotal', 'plan_cuenta_id', 'cta_contable', 'cta_principal'], 'required'],
            [['id', 'factura_compra_id', 'plan_cuenta_id'], 'integer'],
            [['subtotal'], 'number'],
            [['id'], 'unique'],
            [['cta_principal'], 'string'],
            [['asiento_id_por_cuota_prestamo'], 'safe'],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],
            [['plan_cuenta_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['plan_cuenta_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['prestamo_cuota_id'], 'exist', 'skipOnError' => true, 'targetClass' => PrestamoDetalle::className(), 'targetAttribute' => ['prestamo_cuota_id' => 'id']],

//            [['subtotal'], CompraDetalleSubtotalValidator::className()],
            [['asiento_id_por_cuota_prestamo'], 'exist', 'skipOnError' => true, 'targetClass' => Asiento::className(), 'targetAttribute' => ['asiento_id_por_cuota_prestamo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'factura_compra_id' => 'Factura Compra ID',
            'subtotal' => 'Subtotal',
            'plan_cuenta_id' => 'Plan Cuenta ID',
            'agrupar' => "Agrupar",
            'cta_contable' => 'Debe/Haber',
            'cta_principal' => 'Cuenta Principal',
            'periodo_contable_id' => 'Periodo Contable ID',
            'empresa_id' => 'Empresa ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsientoPorCuotaPrestamo()
    {
        return $this->hasOne(Asiento::className(), ['id' => 'asiento_id_por_cuota_prestamo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestamoCuota()
    {
        return $this->hasOne(PrestamoDetalle::className(), ['id' => 'prestamo_cuota_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanCuenta()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'plan_cuenta_id']);
    }

    public function getSubtotalFormat()
    {
        return $this::nf($this->subtotal);
    }

    public function getErrorsEnString()
    {
        $errorMsg = '';
        foreach ($this->getErrorSummary(true) as $error) {
            $errorMsg = $errorMsg . $error . PHP_EOL;
        }
        return $errorMsg;
    }

    private function nf($n)
    {
        $n = number_format($n, '2', '.', '');
        $n = ($n - ((int)$n)) > 0 ? ValorHelpers::numberFormat($n, 2)
            : ValorHelpers::numberFormat((int)$n, 0);

        return $n;
    }

    /**
     *  Verifica si el actual detalle se encuentra en $array_detalles.
     *  El criterio es que entre algun detalle en $array_detalles y el detalle actual deben coindicir en el id de cuenta
     *  y el tipo de saldo, para determinar que ya existe.
     *
     * @param $array_detalles DetalleCompra[] Array de DetalleCompra en donde se desea buscar.
     * @return mixed Retorna el detalle de $array_detalles que satisface los criterios mencionados con el detalle actual. Falso en caso contrario.
     */
    public function isIn($array_detalles = null)
    {
        /** @var DetalleCompra $element_detalle */
        foreach ($array_detalles as $element_detalle) {
            if ($this->plan_cuenta_id === $element_detalle->plan_cuenta_id && $this->cta_contable === $element_detalle->cta_contable)
                return $element_detalle;
        }
        return false;
    }

    public static function agruparCuentas($detalles)
    {
        /** @var DetalleCompra[] $resultado */
        /** @var DetalleCompra $detalle */
        $resultado = [];
        foreach ($detalles as $detalle) {
            if ($detalle->agrupar == 'si') {
                $index = DetalleCompra::searchForId($resultado, $detalle->plan_cuenta_id, $detalle->cta_contable);
                if ($index > -1) {
                    $resultado[$index]->subtotal = round($resultado[$index]->subtotal, 2) + round($detalle->subtotal, 2);
                } else {
                    $resultado[] = $detalle;
                }
            } else
                $resultado[] = $detalle;
        }
        return $resultado;
    }

    static function searchForId($array, $codigo, $saldo)
    {
        /**
         * @var int $key
         * @var DetalleCompra $detalle
         */
        foreach ($array as $key => $detalle) {
            if (isset($detalle) && $detalle->plan_cuenta_id === $codigo && $detalle->cta_contable === $saldo && $detalle->agrupar == 'si') {
                return $key;
            }
        }
        return -1;
    }

    public function getSubtotalDebe()
    {
        $result = $this->cta_contable == 'debe' ? $this->subtotal : '';
        if ($result != '')
            $result = $this->nf($result);
        return $result;
    }

    public function getSubtotalHaber()
    {
        $result = $this->cta_contable == 'haber' ? $this->subtotal : '';
        if ($result != '')
            $result = $this->nf($result);
        return $result;
    }
}
