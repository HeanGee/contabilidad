<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Empresa;

/**
 * This is the model class for table "cont_empresa_rubro".
 *
 * @property string $id
 * @property string $empresa_id
 * @property string $rubro_id
 *
 * @property Rubro $rubro
 * @property Empresa $empresa
 */
class EmpresaRubro extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_empresa_rubro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'rubro_id'], 'required'],
            [['empresa_id', 'rubro_id'], 'integer'],
            [['rubro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubro::className(), 'targetAttribute' => ['rubro_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa ID',
            'rubro_id' => 'Rubro ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubro()
    {
        return $this->hasOne(Rubro::className(), ['id' => 'rubro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @param $rubrosA EmpresaRubro[]
     * @param $rubrosB EmpresaRubro[]
     * @return array El primer elemento es el conjunto de Rubros comunes. El segundo elemento es el conjunto de Rubros no comunes.
     * El tercer elemento es el conjunto de Rubros no comunes de A, y el tercero es el conjunto de Rubros no comunes de B.
     */
    public static function getIntersectionsAndDivergents($rubrosA, $rubrosB)
    {
        /** @var EmpresaRubro[] $commons */
        /** @var EmpresaRubro[] $uncommons */
        /** @var EmpresaRubro[] $uncommonsOfA */
        /** @var EmpresaRubro[] $uncommonsOfB */

        $commons = [];
        $uncommons = [];
        $uncommonsOfA = [];
        $uncommonsOfB = [];

        foreach ($rubrosA as $rubroA) {
            if (EmpresaRubro::findElement($rubroA, $rubrosB) != -1) {
                $commons[] = $rubroA;
            } else {
                $uncommons[] = $rubroA;
                $uncommonsOfA[] = $rubroA;
            }
        }

        foreach ($rubrosB as $rubroB) {
            if (EmpresaRubro::findElement($rubroB, $rubrosA) == -1) {
                $uncommons[] = $rubroB;
                $uncommonsOfB[] = $rubroB;
            }
        }

        return [$commons, $uncommons, $uncommonsOfA, $uncommonsOfB];
    }

    /**
     * @param $needle EmpresaRubro
     * @param $rubros EmpresaRubro[]
     * @return int|mixed|string
     */
    public static function findElement($needle, $rubros)
    {
        foreach ($rubros as $arrayIndex => $rubro) {
            if ($needle->empresa_id == $rubro->empresa_id && $needle->rubro_id == $rubro->rubro_id)
                return $arrayIndex;
        }

        return -1;
    }
}
