<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use common\helpers\ValorHelpers;

/**
 * This is the model class for table "cont_detalle_factura_venta".
 *
 * @property int $id
 * @property int $factura_venta_id
 * @property int $periodo_contable_id
 * @property float $subtotal
 * @property string $plan_cuenta_id
 * @property string $cta_contable
 * @property string $cta_principal
 *
 * @property Venta $venta
 * @property PlanCuenta $planCuenta
 * @property EmpresaPeriodoContable $periodoContable
 */
class DetalleVenta extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
//        return 'cont_detalle_factura_venta';
        return 'cont_factura_venta_detalle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['factura_venta_id', 'subtotal', 'plan_cuenta_id', 'cta_contable'], 'required'],
            [['id', 'factura_venta_id', 'plan_cuenta_id', 'periodo_contable_id'], 'integer'],
            [['subtotal'], 'number'],
            [['id'], 'unique'],
            [['cta_principal'], 'string'],
            [['factura_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['factura_venta_id' => 'id']],
            [['plan_cuenta_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanCuenta::className(), 'targetAttribute' => ['plan_cuenta_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'factura_venta_id' => 'Factura Venta ID',
            'subtotal' => 'Monto',
            'plan_cuenta_id' => 'Plan Cuenta ID',
            'cta_contable' => 'Debe / Haber',
            'periodo_contable_id' => 'Periodo Contable ID',
            'cta_principal' => 'Cuenta Principal',
            'empresa_id' => 'Empresa ID',

        ];
    }

    public static function getAttributesLabelText()
    {

        return [
            'id' => 'ID Venta',
            'factura_venta_id' => 'Venta ID',
            'periodo_contable_id' => 'Perido contable ID',
            'subtotal' => 'Subtotal',
            'plan_cuenta_id' => 'Cuenta Contable ID',
            'cta_principal' => 'Es o no cuenta principal',
            'cta_contable' => 'Debe o Haber',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'factura_venta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanCuenta()
    {
        return $this->hasOne(PlanCuenta::className(), ['id' => 'plan_cuenta_id']);
    }

    public function getSubtotalFormat()
    {
//        return $this->nf($this->subtotal);
        return ValorHelpers::numberFormatMonedaSensitive($this->subtotal, 2, $this->venta->moneda);
    }

    public function getErrorsEnString() {
        $errorMsg = '';
        foreach($this->getErrorSummary(true) as $error)
        {
            $errorMsg = $errorMsg.$error.PHP_EOL;
        }
        return $errorMsg;
    }


    /**
     *  Verifica si el actual detalle se encuentra en $array_detalles.
     *  El criterio es que entre algun detalle en $array_detalles y el detalle actual deben coindicir en el id de cuenta
     *  y el tipo de saldo, para determinar que ya existe.
     *
     * @param $array_detalles DetalleVenta[] Array de DetalleVenta en donde se desea buscar.
     * @return mixed Retorna el detalle de $array_detalles que satisface los criterios mencionados con el detalle actual. Falso en caso contrario.
     */
    public function isIn($array_detalles = null) {
        /** @var DetalleVenta $element_detalle */
        foreach ($array_detalles as $element_detalle) {
            if($this->plan_cuenta_id === $element_detalle->plan_cuenta_id && $this->cta_contable === $element_detalle->cta_contable)
                return $element_detalle;
        }
        return false;
    }

    private function nf($n)
    {
        $n = number_format($n, '2', '.', '');
//        echo "decimals: " . ($n - ((int)$n)) . '<br/>';
        $n = $n - ((int)$n) > 0 ? number_format($n, '2', ',', '.')
            : number_format((int)$n, '0', ',', '.');
//        echo "como sale: " . $n . '<br/><br/>';

        return $n;
    }

    /**
     * Suma todas las cuentas iguales de una partida y muestra la sumatoria de las mismas.
     * @param $detalles DetalleVenta[] cada uno de los detalles de una factura.
     * @return $resultado[] los detalles de la factura con cuentas iguales en la misma partida agrupadas.
     */
    public static function agruparCuentas($detalles)
    {
        /** @var DetalleVenta[] $resultado */
        /** @var DetalleVenta $detalle */

        $resultado = [];
        foreach ($detalles as $detalle)
        {
            $index = DetalleVenta::searchForId($resultado, $detalle->planCuenta->cod_completo, $detalle->cta_contable);
            if($index > -1) {
                $resultado[$index]->subtotal = (float)$resultado[$index]->subtotal + (float)$detalle->subtotal;
            }
            else {
                $resultado[] = $detalle;

            }
        }

        return $resultado;
    }

    static function searchForId($array, $codigo, $saldo)
    {
        /**
         * @var int $key
         * @var DetalleVenta $detalle
         */
        foreach ($array as $key => $detalle) {
            if (isset($detalle) && $detalle->planCuenta->cod_completo === $codigo && $detalle->cta_contable === $saldo) {
                return $key;
            }
        }
        return -1;
    }

    public function subtotalFormat() {
        return ValorHelpers::numberFormatMonedaSensitive($this->subtotal, 2, $this->venta->moneda);
    }

    public function getSubtotalDebe() {
        $result = $this->cta_contable == 'debe' ? $this->subtotal : '';
        if ($result != '')
            $result = $this->nf($result);
        return $result;
    }

    public function getSubtotalHaber() {
        $result = $this->cta_contable == 'haber' ? $this->subtotal : '';
        if ($result != '')
            $result = $this->nf($result);
        return $result;
    }
}
