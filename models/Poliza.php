<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;
use DateTime;
use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "cont_poliza".
 *
 * @property string $id
 * @property string $nro_poliza
 * @property string $fecha_desde
 * @property string $fecha_hasta
 * @property string $importe_diario
 * @property string $importe_gravado
 * @property int $seccion_id
 * @property string $forma_devengamiento
 * @property int $factura_compra_id
 * @property string $empresa_id
 * @property int $periodo_contable_id
 *
 * @property string $importeDiarioFormatted
 *
 * @property Empresa $empresa
 * @property Compra $compra
 * @property PolizaSeccion $seccion
 * @property EmpresaPeriodoContable $periodoContable
 * @property PolizaDevengamiento[] $devengamientos
 */
class Poliza extends \backend\models\BaseModel
{
    public $poliza_id_selector;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_poliza';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nro_poliza', 'fecha_desde', 'fecha_hasta', 'empresa_id', 'periodo_contable_id', 'forma_devengamiento', 'seccion_id'], 'required'],
            [['fecha_desde', 'fecha_hasta', 'poliza_id_selector', 'seccion_id'], 'safe'],
            [['nro_poliza'], 'unique'],
            [['factura_compra_id', 'empresa_id', 'periodo_contable_id', 'seccion_id'], 'integer'],
            [['forma_devengamiento'], 'string'],
            [['importe_diario', 'importe_gravado'], 'string'],
            [['nro_poliza'], 'string', 'max' => 13],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['seccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => PolizaSeccion::className(), 'targetAttribute' => ['seccion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nro_poliza' => 'Nro Poliza',
            'fecha_desde' => 'Fecha Desde',
            'fecha_hasta' => 'Fecha Hasta',
            'importe_diario' => 'Importe Diario',
            'importe_gravado' => 'Importe Gravado',
            'forma_devengamiento' => 'Forma Devengamiento',
            'factura_compra_id' => 'Factura Compra ID',
            'empresa_id' => 'Empresa ID',
            'periodo_contable_id' => 'Periodo Contable ID',
            'seccion_id' => 'Sección',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * Usado en el view _facturas_relacionadas en la carpeta facturas-relacionadas.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->getCompra();
    }

    /**
     * Usado en el view _facturas_relacionadas en la carpeta facturas-relacionadas.
     * Se compara con id = -1 para que si o si devuelva un ActiveQuery.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return Poliza::find()->where(['id' => -1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeccion()
    {
        return $this->hasOne(PolizaSeccion::className(), ['id' => 'seccion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return bool
     */
    public function esDelPeriodoYEmpresa()
    {
        return $this->periodo_contable_id == \Yii::$app->session->get('core_empresa_actual_pc') &&
            $this->empresa_id == \Yii::$app->session->get('core_empresa_actual');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevengamientos()
    {
        return $this->hasMany(PolizaDevengamiento::className(), ['poliza_id' => 'id']);
    }

    public function getImporteDiarioFormatted()
    {
        return number_format($this->importe_diario, 2, ',', '.');
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public static function getFormaDevengamientoLista($idTextFormat = false)
    {
        $valores_enum = Poliza::getTableSchema()->columns['forma_devengamiento']->enumValues;
        $result = [];
        $i = 0;
        foreach ($valores_enum as $_v) {
            if ($idTextFormat) $result[] = ['id' => $_v, 'text' => ++$i . ' - ' . str_replace('_', " ", ucfirst($_v))];
            else $result[$_v] = ++$i . ' - ' . str_replace('_', " ", ucfirst($_v));
        }
        return $result;
    }

    /**
     * @param $forma_devengamiento
     * @param bool $idTextFormat
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public static function selectFormaDevengamiento($forma_devengamiento, $idTextFormat = false)
    {
        if ($forma_devengamiento != null) {
            $deven_formas = Poliza::getFormaDevengamientoLista($idTextFormat);
            foreach ($deven_formas as $key => $deven_forma) {
                if ($idTextFormat) {
                    if ($deven_forma['id'] == $forma_devengamiento) return $deven_forma['text'];
                } else {
                    if ($key == $forma_devengamiento) return $deven_forma;
                }

            }
        }
        return "";
    }

    public static function getPolizasLibres($idTextFormat = false)
    {
        $result = [];
        $polizas = Poliza::findAll(['factura_compra_id' => null]);
        foreach ($polizas as $poliza) {
            if ($idTextFormat)
                $result[] = ['id' => $poliza->id, 'text' => $poliza->id . ' - ' . $poliza->nro_poliza];
            else
                $result[$poliza->id] = $poliza->id . ' - ' . $poliza->nro_poliza;
        }
        return $result;
    }

    public function generarImporteDiario()
    {
        $cant_dias = date_diff(
            DateTime::createFromFormat('Y-m-d', $this->fecha_hasta),
            DateTime::createFromFormat('Y-m-d', $this->fecha_desde), true
        )->days;
        $this->importe_diario = (double)$this->importe_gravado / $cant_dias;
    }

    /**
     * @param null $fecha_emision_factura
     * @return array
     * @throws Exception
     * @throws \Throwable
     */
    public function generarDevengamientos($fecha_emision_factura = null)
    {
        $model = $this;

        if ($model->importe_diario == 0) {
            return ['result' => false, 'error' => "Importe diario de la poliza {$model->nro_poliza} es cero."];
        }

        if (!isset($model->compra)) {
            return ['result' => false, 'error' => "Póliza Nº {$model->nro_poliza} no está asociado a una factura."];
        }

        foreach ($model->devengamientos as $devengamiento) {
            if (isset($devengamiento->asiento)) {
                return ['result' => false, 'error' => "El devengamiento para {$devengamiento->anho_mes} de la Póliza {$model->nro_poliza} está asentado."];
            }
        }

        $fecha_inicio = isset($model->fecha_desde) ? $model->fecha_desde : $fecha_emision_factura;
        $fecha_fin = isset($model->fecha_hasta) ? $model->fecha_hasta : date('Y-m-d', strtotime('+1 year', strtotime($fecha_inicio)));
        $importe_diario = $model->importe_diario;
        $mes_nombre = [
            '01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio',
            '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre',
        ];
        $array = [];
        $fecha = $fecha_inicio;
        while ($fecha <= $fecha_fin) {
            $fin_de_mes = date('Y-m-t', strtotime($fecha));
            $comienzo_de_mes = date('Y-m-01', strtotime($fecha));
            $final_de_mes = $fin_de_mes;
            $es_primer_mes = false;
            if (date('Y-m', strtotime($fin_de_mes)) == date('Y-m', strtotime($fecha_inicio))) {
                $es_primer_mes = true;
            }
            if (date('Y-m', strtotime($fin_de_mes)) == date('Y-m', strtotime($fecha_fin))) {
                $fin_de_mes = $fecha_fin;
            }
            $diff = date_diff(DateTime::createFromFormat('Y-m-d', $fin_de_mes), DateTime::createFromFormat('Y-m-d', $fecha), true);
            $cant_dias_mes = (int)$diff->days + ($es_primer_mes ? 0 : 1);
            $mes = date('m', strtotime($fecha));
            $array[] = [
                'monto' => (double)$importe_diario * $cant_dias_mes,
                'mes' => $mes_nombre["{$mes}"],
                'fecha_desde' => $fecha,
                'fecha_hasta' => $fin_de_mes,
//                'cant' => $cant_dias_mes,
                'cant' => (int)date_diff(DateTime::createFromFormat('Y-m-d', $final_de_mes), DateTime::createFromFormat('Y-m-d', $comienzo_de_mes), true)->days + 1,
            ];
            $fecha = date('Y-m-d', strtotime('+1 day', strtotime($fin_de_mes)));
        }
        $total = 0.0;
        foreach ($array as $item) {
            $total += (float)$item['monto'];
        }

        return $this->generarDevengamientoDetalles($array);
    }


    /**
     * @param $array
     * @return array
     * @throws Exception
     * @throws \Throwable
     */
    private function generarDevengamientoDetalles($array)
    {
        try {
            // Borrar las devengaciones calculadas anteriormente
            $transaccion = Yii::$app->db->beginTransaction();
            foreach ($this->devengamientos as $deveng) {
                if (!$deveng->delete()) {
                    throw new Exception('Error borrando devengamientos de la póliza ' . $deveng->poliza_id);
                }
            }

            // Crear nuevas devengaciones
            foreach ($array as $item) {
                $poliza_dev = new PolizaDevengamiento();
                $poliza_dev->poliza_id = $this->id;
                $poliza_dev->asiento_id = null;
                $poliza_dev->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $poliza_dev->dias = $item['cant'];
                $poliza_dev->monto = $item['monto'];
                $poliza_dev->anho_mes = substr($item['fecha_desde'], 0, 7); // Y-m nada mas
                if (!$poliza_dev->save(false)) {
                    $msg = "";
                    foreach ($poliza_dev->getErrorSummary(true) as $model_error) {
                        $msg .= $model_error . ', ';
                    }
                    throw new Exception('No se pudo guardar poliza_devengamiento: ' . substr($msg, strlen($msg) - 3) . '.');
                }
            }

        } catch (Exception $exception) {
            $transaccion->rollBack();
            throw new Exception($exception->getMessage());
        }

        $transaccion->commit();
        return ['result' => true, 'error' => false];
    }

    public function tieneAsiento()
    {
        return $this->getDevengamientos()->where(['!=', 'asiento_id', ''])->exists();
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $this->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $this->empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $this->fecha_desde = date('Y-m-d');
        $this->fecha_hasta = date('Y-m-d', strtotime('+1 year'));
        $this->importe_diario = 0;
        $this->poliza_id_selector = $this->id;
        return parent::loadDefaultValues($skipIfSet); // TODO: Change the autogenerated stub
    }
}
