<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 25/02/19
 * Time: 01:16 PM
 */

namespace backend\modules\contabilidad\models\auxiliar;

use yii\base\Model;
use yii\web\UploadedFile;

class ArchivosMultiple extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;
    /**
     * @var UploadedFile[]
     */
    public $imageFiles2;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx', 'maxFiles' => 1],
            [['imageFiles2'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx', 'maxFiles' => 1],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);
            }
            foreach ($this->imageFiles2 as $file) {
                $file->saveAs('uploads/' . $file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }
}
