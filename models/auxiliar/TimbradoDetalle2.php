<?php

namespace backend\modules\contabilidad\models\auxiliar;

use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\validators\NumberValidator;
use backend\modules\contabilidad\models\validators\NumeroActualValidator;
use backend\modules\contabilidad\models\validators\RangoValidator;
use backend\modules\contabilidad\models\validators\TimbradoDetalle2Validator;

/**
 * Class TimbradoDetalle2
 *
 * Viernes 14 junio, 2019: Magali solicita que desde compra, al crear timbrados se verifique que
 * no exista otro timbrado del mismo proveedor que tenga las mismas fechas y al menos un detalle con el mismo prefijo,
 * sobre todo al usar el lapiz para editar el timbrado.
 *
 * Esta validacion lo realiza la clase TimbradoDetalle2Validator. La misma no se realiza desde abm de Timbrado porque
 * solicitó que lo que se haga desde allí ya es más concienzudamente y con mayor control. Menciona que puede ocurrir
 * que proveedor "se olvidó" que su timbrado con talonario 00x-00y llegaba hasta, por ej, 5000 y sólo imprimió hasta
 * 4000. Entonces va a tener que haber necesariamente un segundo timbrado (con otro nro_timbrado) pero con las mismas
 * fechas y prefijos. Este caso es solamente abordable desde el panel de ABM de timbrados y no desde factura de compras
 * puesto que puede relegar a errores de carga.
 *
 * @property string $nro_completo
 */
class TimbradoDetalle2 extends TimbradoDetalle
{
    public $nro_completo;

    public function rules()
    {
        return [
            [['nro_completo'], 'safe'],
            [['timbrado_id', 'tipo_documento_set_id', 'prefijo'], 'required'],
            [['nro_inicio', 'nro_fin'], 'integer'],
            [['nro_inicio'], NumberValidator::className()],
            [['nro_fin'], NumberValidator::className()],
            [['nro_actual'], NumeroActualValidator::className()],
            [['nro_inicio'], RangoValidator::className()],
            [['nro_fin'], RangoValidator::className()],

            [['timbrado_id'], TimbradoDetalle2Validator::class],
        ];
    }
}