<?php
namespace backend\modules\contabilidad\models\auxiliar;

use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\Timbrado;
use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\validators\NroTimbrado2Validator;
use backend\modules\contabilidad\models\validators\TimbradoFechaValidator;
use backend\modules\contabilidad\models\validators\TimbradoVigenciaMinimaValidator;

class Timbrado2 extends Timbrado
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entidad_id'], 'required'],
            [['nro_timbrado', 'entidad_id'], 'integer'],
            [['fecha_inicio', 'fecha_fin', 'entidad_tmp'], 'safe'],
            [['entidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entidad::className(), 'targetAttribute' => ['entidad_id' => 'id']],
            ['nro_timbrado', NroTimbrado2Validator::className()],
//            [['nro_timbrado'], 'unique'],
            [['fecha_fin'], TimbradoFechaValidator::className()],

            #21 Junio 2019: Magali quiere que valide que la vigencia minima sea el fin del 3er mes a partir del mes de la fecha_inicio, si existe.
            [['fecha_inicio', 'fecha_fin'], TimbradoVigenciaMinimaValidator::class],
        ];
    }
}

class TimbradoDetalleUpdate extends TimbradoDetalle
{
    public function rules()
    {
        return [
            [['timbrado_id', 'tipo_documento_set_id', 'prefijo'], 'required'],
            [['nro_inicio', 'nro_fin'], 'integer'],
        ];
    }
}