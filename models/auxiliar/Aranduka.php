<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 25/02/19
 * Time: 01:16 PM
 */

namespace backend\modules\contabilidad\models\auxiliar;

use yii\base\Model;

/**
 * Class Aranduka
 *
 * @property array $tipoEgresoDesc
 * @property array $tipodocIngresoDesc
 * @property array $tipodocEgresoDesc
 * @property array $clasifEgresoDesc
 * @property array $tipoVinculoDesc
 * @property array $regimenMatrimDesc
 * @property array $personaFisica
 *
 * @package backend\modules\contabilidad\models\auxiliar
 */
class Aranduka extends Model
{
    /** TIPO DE DOCUMENTO INGRESOS. */
    const TDI_FACTURA = 1;
    const TDI_NOTA_DE_CREDITO = 4;
    const TDI_LIQUIDACION_DE_SALARIO = 5;
    const TDI_EXTRACTO_DE_CUENTA = 8;
    const TDI_OTROS_DOCUMENTOS_RESPALDO_INGRESOS = 14;
    public $TIPO_DOCUMENTO_INGRESO_DESCRIPCION = null;


    /** TIPO DE DOCUMENTO EGRESOS. */
    const TDE_FACTURA = 1;
    const TDE_AUTOFACTURA = 2;
    const TDE_BOLETA_DE_VENTA = 3;
    const TDE_NOTA_DE_CREDITO = 4;
    const TDE_LIQUIDACION_DE_SALARIO = 5;
    const TDE_EXTRACTO_CUENTA_IPS = 6;
    const TDE_EXTRACTO_TARGETA_CREDITO_DEBITO = 7;
    const TDE_TRANSF_O_GIROS_BANCAR_BOLETA_DE_DEPTO = 9;
    const TDE_COMPROBANTE_EXTERIOR_LEGALIZADO = 10;
    const TDE_COMPROBANTE_INGRESO_ENT_PUBLICAS = 11;
    const TDE_TICKET = 12;
    const TDE_DESPACHO_DE_IMPORTACION = 13;
    const TDE_OTROS_COMPROBANTES_VENTA_RESPALDO_EGRESO = 14;
    public $TIPO_DOCUMENTO_EGRESO_DESCRIPCION = null;


    /** TIPOS DE EGRESOS. */
    const TE_GASTO = 'gasto';
    const TE_INVERSION_ACTIVIDAD = 'inversion_actividad';
    const TE_INVERSION_PERSONAS = 'inversion_personas';
    private $TIPO_EGRESO_DESCRIPCION = null;


    /** CLASIFICACIoN DEL EGRESO. */
    const GPERS = 'GPERS';
    const GPERSSINCV = 'GPERSSINCV';
    const GACT = 'GACT';
    const DONAC = 'DONAC';
    const PREST = 'PREST';
    const CUOTA = 'CUOTA';
    const MEH = 'MEH';
    const INM = 'INM';
    const EDU = 'EDU';
    const COLOC = 'COLOC';
    const DESCJBPN = 'DESCJBPN';
    const REMDEP = 'REMDEP'; // tambien en TTPO DE INGRESO
    const APRTSS = 'APRTSS';
    const GPERSEXT = 'GPERSEXT';
    const GACTEXT = 'GACTEXT';
    const GSTADM = 'GSTADM';
    const RECPOS = 'RECPOS';
    const CMPOF = 'CMPOF';
    const GSTACT = 'GSTACT';
    const REMINDEP = 'REMINDEP';
    const GSTACTEXT = 'GSTACTEXT';
    const OG = 'OG';
    const INVLF = 'INVLF';
    const INVLFEXT = 'INVLFEXT';
    const IMPBIENES = 'IMPBIENES';
    const ACCIONES = 'ACCIONES';
    const CAPITAL = 'CAPITAL';
    const SALUD = 'SALUD';
    public $CLASIFICACION_EGRESO_DESCRIPCION = null;


    /** TIPO DE INGRESO */
    const HPRSP = 'HPRSP';
    const HPR = 'HPR';
    const DU = 'DU';
    const VO = 'VO';
    const IPCMI = 'IPCMI';
    const OI = 'OI';
    const IPCM = 'IPCM';
    const VIAT = 'VIAT';
    const AGUI = 'AGUI';
    const RET = 'RET';
    const INDEM = 'INDEM';
    const PREMIO = 'PREMIO';
    const LH = 'LH';
    const BIENDIS = 'BIENDIS';
    const EXCEDE = 'EXCEDE';


    /** TIPOS DE VINCULOS */
    const CONYUGE = 1;
    const HIJOS = 2;
    const PADRES = 3;
    const HERMANOS = 4;
    const ABUELOS = 5;
    const SUEGROS = 6;
    const OTRAS_PERSONAS = 7;
    private $TIPO_VINCULO_DESCRIPCION = null;


    /** REGIMEN MATRIMONIAL */
    const RM_ADM_CONJUNTA = 1;
    const RM_PART_DIFERIDA = 2;
    const RM_SEPARAC_BIENES = 3;
    const RM_UNION_DE_HECHO = 4;
    private $REGIMEN_MATRIMONIAL_DESCRIPCION = null;


    /** ARRAY DE JSON */
    public $_CONYUGE_O_DEPENDIENTE = [
        "identificacion" => '',
        "nombre" => '',
        "regimen" => '',
        "regimenTexto" => '',
        "vinculo" => '',
        "vinculoTexto" => '',
        "ruc" => '',
        "periodo" => '',
        "fechaNacimiento" => '',
    ];
    public $_HIJO = [
        "identificacion" => '',
        "nombre" => '',
        "regimen" => '',
        "regimenTexto" => '',
        "vinculo" => '',
        "vinculoTexto" => '',
        "ruc" => '',
        "periodo" => '',
        "fechaNacimiento" => '',
    ];
    public $_INGRESO = [
        'tipo' => '',
        "periodo" => '',
        "tipoTexto" => '',
        "fecha" => '',
        "ruc" => '',
        "tipoIngreso" => '',
        "tipoIngresoTexto" => '',
        "id" => '',
        "ingresoMontoGravado" => '',
        "ingresoMontoNoGravado" => '',
        "ingresoMontoTotal" => '',
        "timbradoCondicion" => '',
        "timbradoDocumento" => '',
        "timbradoNumero" => '',
        "relacionadoTipoIdentificacion" => '',
        "mes" => '',
        "relacionadoNumeroIdentificacion" => '',
        "relacionadoNombres" => ''
    ];
    public $_EGRESO = [
        "periodo" => '',
        "tipo" => '',
        "relacionadoTipoIdentificacion" => '',
        "fecha" => '',
        "id" => '',
        "ruc" => "",
        "egresoMontoTotal" => '',
        "relacionadoNombres" => '',
        "relacionadoNumeroIdentificacion" => '',
        "timbradoCondicion" => '',
        "timbradoDocumento" => '',
        "timbradoNumero" => '',
        "tipoEgreso" => '',
        "tipoEgresoTexto" => '',
        "tipoTexto" => '',
        "subtipoEgreso" => '',
        "subtipoEgresoTexto" => ''
    ];
    private $PERSONA_FISICA = [
        'informante' => [
            'ruc' => ''
        ],
        'identificacion' => [
            'periodo' => ''
        ],
        'ingresos' => [],
        'egresos' => [],
        'familiares' => [],
    ];
    private $SOCIEDAD_SIMPLE = [
        'informante' => [
            'ruc' => ''
        ],
        'identificacion' => [
            'periodo' => ''
        ],
        'ingresos' => '',
        'egresos' => '',
    ];

    /** CONSTRUCTOR */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        self::setTipoEgresoDesc();
        self::setTipodocEgresoDesc();
        self::setTipodocIngresoDesc();
        self::setClasifEgresoDesc();
        self::setTipoVinculoDesc();
        self::setRegimenMatrimDesc();
//        $this->PERSONA_FISICA['ingresos'] = [$this->_INGRESO, $this->_EGRESO];
//        $this->PERSONA_FISICA['egresos'] = [$this->_EGRESO, $this->_EGRESO];
//        $this->PERSONA_FISICA['familiares'] = [$this->_CONYUGE_O_DEPENDIENTE, $this->_HIJO, $this->_CONYUGE_O_DEPENDIENTE];
    }

    /**
     * @param null $index
     * @return array|mixed
     * @throws \Exception
     */
    public static function getTipoEgresoDesc($index = null)
    {
        $TIPO_EGRESO_DESCRIPCION = [
            self::TE_GASTO => "Gasto",
            self::TE_INVERSION_ACTIVIDAD => "Inversiones Relacionadas a la Actividad Gravada",
            self::TE_INVERSION_PERSONAS => "Inversiones Personales y de familiares a Cargo"
        ];

        if (isset($index) && !array_key_exists($index, $TIPO_EGRESO_DESCRIPCION))
            throw new \Exception("Tipo de egreso Nº $index no existe.");

        return (isset($index)) ? $TIPO_EGRESO_DESCRIPCION[$index] : $TIPO_EGRESO_DESCRIPCION;
    }

    public function setTipoEgresoDesc()
    {
        $this->TIPO_EGRESO_DESCRIPCION = [
            self::TE_GASTO => "Gasto",
            self::TE_INVERSION_ACTIVIDAD => "Inversiones Relacionadas a la Actividad Gravada",
            self::TE_INVERSION_PERSONAS => "Inversiones Personales y de familiares a Cargo"
        ];
    }

    /**
     * @param null $index
     * @return array|mixed
     * @throws \Exception
     */
    public static function getTipodocEgresoDesc($index = null)
    {
        $TIPO_DOCUMENTO_EGRESO_DESCRIPCION = [
            self::TDE_FACTURA => "Factura",
            self::TDE_AUTOFACTURA => "Autofactura",
            self::TDE_BOLETA_DE_VENTA => "Boleta de Venta",
            self::TDE_NOTA_DE_CREDITO => "Nota de Credito",
            self::TDE_LIQUIDACION_DE_SALARIO => "Liquidacion de Salarios",
            self::TDE_EXTRACTO_CUENTA_IPS => "Extracto de Cuenta IPS",
            self::TDE_EXTRACTO_TARGETA_CREDITO_DEBITO => "",
            self::TDE_TRANSF_O_GIROS_BANCAR_BOLETA_DE_DEPTO => "Extracto de Tarjeta de Credito/Tarjeta de Debito",
            self::TDE_COMPROBANTE_EXTERIOR_LEGALIZADO => "Comprobante del Exterior Legalizado",
            self::TDE_COMPROBANTE_INGRESO_ENT_PUBLICAS => "Comprobante de Ingreso de Entidades Públicas",
            self::TDE_TICKET => "Ticket (Maquina Registradora)",
            self::TDE_DESPACHO_DE_IMPORTACION => "Despacho de Importación",
            self::TDE_OTROS_COMPROBANTES_VENTA_RESPALDO_EGRESO => "Otros comprobantes de venta que respaldan los egresos
             (pasaje aereos, entradas a espectaculos públicos, boletos de transporte público) o cuando no exista la
              obligación de emitir comprobantes de venta",
        ];

        if (isset($index) && !array_key_exists($index, $TIPO_DOCUMENTO_EGRESO_DESCRIPCION))
            throw new \Exception("Tipo de documento Nº $index no existe para Egresos");

        return (isset($index)) ? $TIPO_DOCUMENTO_EGRESO_DESCRIPCION[$index] : $TIPO_DOCUMENTO_EGRESO_DESCRIPCION;
    }

    public function setTipodocEgresoDesc()
    {
        $this->TIPO_DOCUMENTO_EGRESO_DESCRIPCION = [
            self::TDE_FACTURA => "Factura",
            self::TDE_AUTOFACTURA => "Autofactura",
            self::TDE_BOLETA_DE_VENTA => "Boleta de Venta",
            self::TDE_NOTA_DE_CREDITO => "Nota de Credito",
            self::TDE_LIQUIDACION_DE_SALARIO => "Liquidacion de Salarios",
            self::TDE_EXTRACTO_CUENTA_IPS => "Extracto de Cuenta IPS",
            self::TDE_EXTRACTO_TARGETA_CREDITO_DEBITO => "",
            self::TDE_TRANSF_O_GIROS_BANCAR_BOLETA_DE_DEPTO => "Extracto de Tarjeta de Credito/Tarjeta de Debito",
            self::TDE_COMPROBANTE_EXTERIOR_LEGALIZADO => "Comprobante del Exterior Legalizado",
            self::TDE_COMPROBANTE_INGRESO_ENT_PUBLICAS => "Comprobante de Ingreso de Entidades Públicas",
            self::TDE_TICKET => "Ticket (Maquina Registradora)",
            self::TDE_DESPACHO_DE_IMPORTACION => "Despacho de Importación",
            self::TDE_OTROS_COMPROBANTES_VENTA_RESPALDO_EGRESO => "Otros comprobantes de venta que respaldan los egresos
             (pasaje aereos, entradas a espectaculos públicos, boletos de transporte público) o cuando no exista la
              obligación de emitir comprobantes de venta",
        ];
    }

    /**
     * @param null $index
     * @return array|mixed
     * @throws \Exception
     */
    public static function getTipodocIngresoDesc($index = null)
    {
        $TIPO_DOCUMENTO_INGRESO_DESCRIPCION = [
            self::TDI_FACTURA => "Factura",
            self::TDI_NOTA_DE_CREDITO => "Nota de Credito",
            self::TDI_LIQUIDACION_DE_SALARIO => "Liquidacion de Salarios",
            self::TDI_EXTRACTO_DE_CUENTA => "Extracto de Cuenta (cuando no exista la obligación de emitir comprobantes de venta)",
            self::TDI_OTROS_DOCUMENTOS_RESPALDO_INGRESOS => "Otros Documentos que respaldan los ingresos (cuando no exista la obligación de emitir comprobantes de venta)"
        ];

        if (isset($index) && !array_key_exists($index, $TIPO_DOCUMENTO_INGRESO_DESCRIPCION))
            throw new \Exception("Tipo de documento Nº $index no existe para Ingresos");

        return (isset($index)) ? $TIPO_DOCUMENTO_INGRESO_DESCRIPCION[$index] : $TIPO_DOCUMENTO_INGRESO_DESCRIPCION;
    }

    public function setTipodocIngresoDesc()
    {
        $this->TIPO_DOCUMENTO_INGRESO_DESCRIPCION = [
            self::TDI_FACTURA => "Factura",
            self::TDI_NOTA_DE_CREDITO => "Nota de Credito",
            self::TDI_LIQUIDACION_DE_SALARIO => "Liquidacion de Salarios",
            self::TDI_EXTRACTO_DE_CUENTA => "Extracto de Cuenta (cuando no exista la obligación de emitir comprobantes de venta)",
            self::TDI_OTROS_DOCUMENTOS_RESPALDO_INGRESOS => "Otros Documentos que respaldan los ingresos (cuando no exista la obligación de emitir comprobantes de venta)"
        ];
    }

    /**
     * @param null $index
     * @return array|mixed
     * @throws \Exception
     */
    public static function getClasifEgresoDesc($index = null)
    {
        $CLASIFICACION_EGRESO_DESCRIPCION = [
            self::GPERS => "Gastos personales y de familiares a cargo realizados en el país",
            self::GPERSSINCV => "Gastos personales y de familiares a cargo realizados en el país, cuando no exista obligación de contar con comprobantes de venta",
            self::GACT => "Gastos relacionados a la actividad gravada realizados en el país",
            self::DONAC => "Donaciones",
            self::PREST => "Amortización o cancelación de préstamos obtenidos antes de ser contribuyente del IRP, así como sus intereses, comisiones y otros recargos",
            self::CUOTA => "Cuotas de capital de las financiaciones, así como los intereses, las comisiones y otros recargos pagados por la adquisición de bienes o servicios",
            self::MEH => "Muebles, Equipos y Herramientas",
            self::INM => "Adquisicion de inmuebles, construcción o mejoras de inmuebles",
            self::EDU => "Educación",
            self::COLOC => "Colocaciones de dinero",
            self::DESCJBPN => "Descuentos legales por Aporte al Regimen de Jubilaciones y Pensiones en carácter de trabajador dependiente",
            self::REMDEP => "Salarios y otras remuneraciones pagados a trabajadores dependientes",
            self::APRTSS => "Aportes al régimen de seguridad social en carácter de empleador",
            self::GPERSEXT => "Gastos personales y de familiares a cargo realizados en el exterior",
            self::GACTEXT => "Gastos relacionados a la actividad gravada realizados en el exterior",
            self::GSTADM => "Intereses, comisiones y demás gastos administrativos",
            self::RECPOS => "Intereses, comisiones y otros recargos pagados por los préstamos obtenidos, con posterioridad a ser contribuyentes del IRP",
            self::CMPOF => "Compra de útiles de oficina, gastos de limpieza y mantenimiento",
            self::GSTACT => "Otros gastos realizados relacionados a la actividad gravada",
            self::REMINDEP => "Honorarios y otras remuneraciones pagados al personal independiente",
            self::GSTACTEXT => "Gastos realizados en el exterior relacionados a la actividad gravada",
            self::OG => "Otros gastos realizados en el ejercicio",
            self::INVLF => "Inversión en licencias, franquicias y otros similares",
            self::INVLFEXT => "Inversión en licencias, franquicias y otros similares, adquiridos del exterior",
            self::IMPBIENES => "Importación ocasional de bienes",
            self::ACCIONES => "Compra de acciones o cuotas partes de sociedades constituidas en el país",
            self::CAPITAL => "Aporte de capital realizado en sociedades constituidas en el país",
            self::SALUD => "Salud",
        ];

        if (isset($index) && !array_key_exists($index, $CLASIFICACION_EGRESO_DESCRIPCION))
            throw new \Exception("Clasificación de Egreso `{$index}` no existe.");

        return isset($index) ? $CLASIFICACION_EGRESO_DESCRIPCION[$index] : $CLASIFICACION_EGRESO_DESCRIPCION;
    }

    public function setClasifEgresoDesc()
    {
        $this->CLASIFICACION_EGRESO_DESCRIPCION = [
            self::GPERS => "Gastos personales y de familiares a cargo realizados en el país",
            self::GPERSSINCV => "Gastos personales y de familiares a cargo realizados en el país, cuando no exista obligación de contar con comprobantes de venta",
            self::GACT => "Gastos relacionados a la actividad gravada realizados en el país",
            self::DONAC => "Donaciones",
            self::PREST => "Amortización o cancelación de préstamos obtenidos antes de ser contribuyente del IRP, así como sus intereses, comisiones y otros recargos",
            self::CUOTA => "Cuotas de capital de las financiaciones, así como los intereses, las comisiones y otros recargos pagados por la adquisición de bienes o servicios",
            self::MEH => "Muebles, Equipos y Herramientas",
            self::INM => "Adquisicion de inmuebles, construcción o mejoras de inmuebles",
            self::EDU => "Educación",
            self::COLOC => "Colocaciones de dinero",
            self::DESCJBPN => "Descuentos legales por Aporte al Regimen de Jubilaciones y Pensiones en carácter de trabajador dependiente",
            self::REMDEP => "Salarios y otras remuneraciones pagados a trabajadores dependientes",
            self::APRTSS => "Aportes al régimen de seguridad social en carácter de empleador",
            self::GPERSEXT => "Gastos personales y de familiares a cargo realizados en el exterior",
            self::GACTEXT => "Gastos relacionados a la actividad gravada realizados en el exterior",
            self::GSTADM => "Intereses, comisiones y demás gastos administrativos",
            self::RECPOS => "Intereses, comisiones y otros recargos pagados por los préstamos obtenidos, con posterioridad a ser contribuyentes del IRP",
            self::CMPOF => "Compra de útiles de oficina, gastos de limpieza y mantenimiento",
            self::GSTACT => "Otros gastos realizados relacionados a la actividad gravada",
            self::REMINDEP => "Honorarios y otras remuneraciones pagados al personal independiente",
            self::GSTACTEXT => "Gastos realizados en el exterior relacionados a la actividad gravada",
            self::OG => "Otros gastos realizados en el ejercicio",
            self::INVLF => "Inversión en licencias, franquicias y otros similares",
            self::INVLFEXT => "Inversión en licencias, franquicias y otros similares, adquiridos del exterior",
            self::IMPBIENES => "Importación ocasional de bienes",
            self::ACCIONES => "Compra de acciones o cuotas partes de sociedades constituidas en el país",
            self::CAPITAL => "Aporte de capital realizado en sociedades constituidas en el país",
            self::SALUD => "Salud",
        ];
    }

    public function getTipoVinculoDesc()
    {
        return $this->TIPO_VINCULO_DESCRIPCION;
    }

    public function setTipoVinculoDesc()
    {
        $this->TIPO_VINCULO_DESCRIPCION = [
            self::CONYUGE => "Conyuge",
            self::HIJOS => "Hijos",
            self::PADRES => "Padres",
            self::HERMANOS => "Hermanos",
            self::ABUELOS => "Abuelos",
            self::SUEGROS => "Suegros",
            self::OTRAS_PERSONAS => "Otras Personas respecto a las cuales exista la obligación legal de prestar alimentos",
        ];
    }

    public function getRegimenMatrimDesc()
    {
        return $this->REGIMEN_MATRIMONIAL_DESCRIPCION;
    }

    public function setRegimenMatrimDesc()
    {
        $this->REGIMEN_MATRIMONIAL_DESCRIPCION = [
            self::RM_ADM_CONJUNTA => "Comunidad de gananciales bajo administracion conjunta",
            self::RM_PART_DIFERIDA => "Regimen de participacion diferida",
            self::RM_SEPARAC_BIENES => "Regimen de separacion de bienes",
            self::RM_UNION_DE_HECHO => "Union de hecho",
            self::OTRAS_PERSONAS => "Otras Personas respecto a las cuales exista la obligación legal de prestar alimentos",
        ];
    }

    public function getPersonaFisica()
    {
        return $this->PERSONA_FISICA;
    }
}