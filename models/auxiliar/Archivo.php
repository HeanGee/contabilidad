<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 25/02/19
 * Time: 01:16 PM
 */

namespace backend\modules\contabilidad\models\auxiliar;

use yii\base\Model;

/**
 * Class Archivo
 * @package backend\modules\contabilidad\controllers
 *
 * @property string $reemplazar
 * @property string $crear_tipo
 */
class Archivo extends Model
{
    /* valores auxiliares */
    public $archivo;
    public $reemplazar;
    public $crear_tipo;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['archivo', 'reemplazar', 'crear_tipo'], 'required'],
            [['archivo', 'reemplazar', 'crear_tipo'], 'safe'],
            [['fecha'], 'safe'],
            [['fecha'], 'required'],
            ['archivo', 'file', 'extensions' => ['xlsx', 'xls'], 'skipOnEmpty' => false, 'maxSize' => 1024 * 1024],
            [['nombreArchivo'], 'match', 'pattern' => '/^\d+-\d{1}_\d{6}']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Archivo a subir',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->archivo->saveAs('uploads/' . $this->archivo->baseName . '.' . $this->archivo->extension);
            return true;
        } else {
            return false;
        }
    }

}