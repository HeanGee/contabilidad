<?php

namespace backend\modules\contabilidad\models\auxiliar;

use Exception;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class ArchivoEstadoFinanciero
 *
 * @property UploadedFile $file
 */
class ArchivoEstadoFinanciero extends Model
{

    private $directory = "";

    public $file;

    public function __construct($config = [])
    {
        $this->directory = self::getPreferedDirectory();
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => ['xlsx', 'xls'], 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024],  # 1 mbyte.
        ];
    }

    public static function getPreferedDirectory()
    {
        return Yii::$app->basePath . '/web/uploads/contabilidad/files/estado-financiero/';
    }

    /**
     * @return string
     * @throws Exception
     */
    public function upload()
    {
        if (!self::mkdir($this->directory))
            throw new Exception("No se pudo crear carpeta en `$this->directory`");

        $absFileName = "$this->directory{$this->file->baseName}.{$this->file->extension}";
        if ($this->file->saveAs($absFileName)) {
            return $absFileName;
        }

        throw new Exception("No se pudo guardar en `$absFileName`");
    }

    public function deleteUploaded()
    {
        $absFileName = "$this->directory{$this->file->baseName}.{$this->file->extension}";
        if (file_exists($this->directory))  # Es archivo pero no un directorio.
            return unlink($absFileName);
        return true;
    }

    private function mkdir(string $dir)
    {
        if (!file_exists($dir) && !is_dir($dir))
            return @mkdir($dir, 0777, true);

        return true;
    }
}
