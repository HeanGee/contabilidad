<?php

namespace backend\modules\contabilidad\models;

use backend\models\Empresa;

/**
 * This is the model class for table "cont_activo_fijo_stock_manager".
 *
 * @property string $id
 * @property string $activo_fijo_id
 * @property int $factura_compra_id
 * @property int $compra_nota_credito_id
 * @property int $factura_venta_id
 * @property int $venta_nota_credito_id
 * @property string $empresa_id
 * @property int $periodo_contable_id
 *
 * @property ActivoFijo $activoFijo
 * @property Compra $facturaCompra
 * @property Compra $compraNotaCredito
 * @property Empresa $empresa
 * @property EmpresaPeriodoContable $periodoContable
 * @property Venta $facturaVenta
 * @property Venta $ventaNotaCredito
 */
class ActivoFijoStockManager extends \backend\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_activo_fijo_stock_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo_fijo_id', 'empresa_id', 'periodo_contable_id'], 'required'],
            [['activo_fijo_id', 'factura_compra_id', 'compra_nota_credito_id', 'factura_venta_id', 'venta_nota_credito_id', 'empresa_id', 'periodo_contable_id'], 'integer'],
            [['activo_fijo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivoFijo::className(), 'targetAttribute' => ['activo_fijo_id' => 'id']],
            [['factura_compra_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compra::className(), 'targetAttribute' => ['factura_compra_id' => 'id']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id']],
            [['periodo_contable_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['periodo_contable_id' => 'id']],
            [['factura_venta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['factura_venta_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activo_fijo_id' => 'Activo Fijo ID',
            'factura_compra_id' => ' Compra ID',
            'compra_nota_credito_id' => 'Compra Nota Credito ID',
            'factura_venta_id' => ' Venta ID',
            'venta_nota_credito_id' => 'Venta Nota Credito ID',
            'empresa_id' => 'Empresa ID',
            'periodo_contable_id' => 'Periodo able ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivoFijo()
    {
        return $this->hasOne(ActivoFijo::className(), ['id' => 'activo_fijo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompra()
    {
        return $this->hasOne(Compra::className(), ['id' => 'factura_compra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraNotaCredito()
    {
        return $this->hasOne(Compra::className(), ['id' => 'compra_nota_credito_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'empresa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoContable()
    {
        return $this->hasOne(EmpresaPeriodoContable::className(), ['id' => 'periodo_contable_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVenta()
    {
        return $this->hasOne(Venta::className(), ['id' => 'factura_venta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentaNotaCredito()
    {
        return $this->hasOne(Venta::className(), ['id' => 'venta_nota_credito_id']);
    }

    /**
     * @param string $activo_fijo_id
     */
    public function setActivoFijoId($activo_fijo_id)
    {
        $this->activo_fijo_id = $activo_fijo_id;
    }

    /**
     * @param int $factura_compra_id
     */
    public function setFacturaCompraId($factura_compra_id)
    {
        $this->factura_compra_id = $factura_compra_id;
    }

    /**
     * @param int $compra_nota_credito_id
     */
    public function setCompraNotaCreditoId($compra_nota_credito_id)
    {
        $this->compra_nota_credito_id = $compra_nota_credito_id;
    }

    /**
     * @param int $factura_venta_id
     */
    public function setFacturaVentaId($factura_venta_id)
    {
        $this->factura_venta_id = $factura_venta_id;
    }

    /**
     * @param int $venta_nota_credito_id
     */
    public function setVentaNotaCreditoId($venta_nota_credito_id)
    {
        $this->venta_nota_credito_id = $venta_nota_credito_id;
    }

    /**
     * @param string $empresa_id
     */
    public function setEmpresaId($empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @param int $periodo_contable_id
     */
    public function setPeriodoContableId($periodo_contable_id)
    {
        $this->periodo_contable_id = $periodo_contable_id;
    }

    /**
     * @param $empresa_id
     * @param $periodo_id
     */
    public function setEmpresaPeriodoActuales($empresa_id, $periodo_id)
    {
        $this->setEmpresaId($empresa_id);
        $this->setPeriodoContableId($periodo_id);
    }
}
