<?php

namespace backend\modules\contabilidad\models;

use backend\models\BaseModel;
use backend\models\Iva;

/**
 * This is the model class for table "cont_retencion_detalle_base".
 *
 * @property string $id
 * @property string $retencion_id
 * @property string $base
 * @property string $iva_id
 *
 * @property Retencion $retencion
 * @property Iva $iva
 */
class RetencionDetalleBaseIvas extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cont_retencion_detalle_base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['retencion_id', 'iva_id'], 'required'],
            [['retencion_id', 'iva_id'], 'integer'],
//            [['base'], 'number'],
            [['retencion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Retencion::className(), 'targetAttribute' => ['retencion_id' => 'id']],
            [['iva_id'], 'exist', 'skipOnError' => true, 'targetClass' => Iva::className(), 'targetAttribute' => ['iva_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'retencion_id' => 'Retencion ID',
            'base' => 'Base',
            'iva_id' => 'Iva ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRetencion()
    {
        return $this->hasOne(Retencion::className(), ['id' => 'retencion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIva()
    {
        return $this->hasOne(Iva::className(), ['id' => 'iva_id']);
    }
}
