<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 14/02/19
 * Time: 10:56 AM
 */

namespace backend\modules\contabilidad\models;

use yii\base\Model;

/**
 * Modelo utilizado para el reporte de hechauka de compra y venta.
 *
 * @property string $id
 * @property string $condicion
 * @property string $total
 * @property string $ruc
 * @property string $nro_factura
 * @property string $dv
 * @property string $razon_social
 * @property string $fecha
 * @property string $cant_cuotas
 * @property string $moneda_id
 * @property string $cotizacion
 * @property string $fecha_emision
 * @property string $timbrado_detalle_id
 * @property string $nro_timbrado
 * @property string $operacion
 */
class Factura extends Model
{
    public $id;
    public $condicion;
    public $total;
    public $ruc;
    public $nro_factura;
    public $dv;
    public $razon_social;
    public $fecha;
    public $cant_cuotas;
    public $moneda_id;
    public $cotizacion;
    public $fecha_emision;
    public $timbrado_detalle_id;
    public $nro_timbrado;
    public $operacion;

    public function rules()
    {
        $rules = [
            [['id', 'condicion', 'total', 'ruc', 'nro_factura', 'dv', 'razon_social', 'fecha', 'cant_cuotas', 'moneda_id',
                'cotizacion', 'fecha_emision', 'timbrado_detalle_id', 'nro_timbrado', 'operacion'], 'safe'],
        ];

        return $rules;
    }

    public function getAttributes($names = null, $except = [])
    {
        return parent::getAttributes($names, $except); // TODO: Change the autogenerated stub
    }
}
