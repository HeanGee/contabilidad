<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\models\Iva;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\RetencionArchivo;
use backend\modules\contabilidad\models\RetencionDetalleIvas;
use backend\modules\contabilidad\models\searchRetencionSearch;
use backend\modules\contabilidad\models\Timbrado;
use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\TipoDocumentoSet;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\FlashMessageHelpsers;
use kartik\form\ActiveForm;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

//use backend\modules\contabilidad\models\RetencionDetalleBaseIvas;
//use backend\modules\contabilidad\models\RetencionDetalleFactorIvas;

/**
 * RetencionController implements the CRUD actions for Retencion model.
 */
class RetencionController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view', 'bloquear', 'desbloquear'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Retencion $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Retencion $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['bloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Retencion $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['desbloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Retencion $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'si';
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Retencion models.
     * @return mixed
     */
    public function actionIndex($operacion)
    {
        $searchModel = new searchRetencionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $operacion);

        $skey = 'cont_retencion_id';
        Yii::$app->session->remove($skey);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Retencion model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $operacion)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Retencion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Retencion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Retencion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Retencion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $operacion
     * @return mixed
     */
    public function actionCreate($operacion)
    {
        try {
            Retencion::isCreable($operacion);
        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index', 'operacion' => $operacion]);
        }
        $model = new Retencion();
        $model->tipo_factura = $operacion;
        $model->fecha_emision = date('Y-m-d');
        $model->empresa_id = Yii::$app->session->get('core_empresa_actual');
        $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
        $detalles_base_ivas = [];
        $detalles_factor_ivas = [];

        if ($model->load(Yii::$app->request->post())) {
            foreach ($_POST['Retencion'] as $attribute => $value) {
                if (array_key_exists($attribute, $model->attributes))
                    $model->$attribute = $value;
            }
            $retencion_base_ivas = [];
            $retencion_factor_ivas = [];
            $total_retenciones = 0.0;
            $pattern_base = '/retencion-base_iva-([0-9]+)$/';
            $pattern_factor = '/retencion-factor_iva-([0-9]+)$/';
            $pattern_totales_iva = '/total-total-iva-([0-9]+)$/';
            // $matches: array: [0] => texto completo que matcheó, [1] => primer subgrupo que matcheó (en este caso el numero porcentaje iva)
            // Acumular retencion de RENTA
            $total_retenciones += round(round($model->base_renta_porc, 2) * (round($model->factor_renta_porc, 2) / 100), 2);
            // Acumular retenciones de IVA
            foreach ($_POST as $key => $value) {
                if (preg_match($pattern_base, $key, $matches) && $value != "") {
                    $retencion_base_ivas[$matches[1]] = $value;
                }
                if (preg_match($pattern_factor, $key, $matches) && $value != "") {
                    $retencion_factor_ivas[$matches[1]] = $value;
                }
                if (preg_match($pattern_totales_iva, $key, $matches) && $value != "") {
                    $total_retenciones += (float)$value;
                }
            }

            $trans = Yii::$app->db->beginTransaction();

            try {
                $compra = null;
                $venta = null;
                switch ($operacion) {
                    case 'compra':
                        $compra = Compra::findOne(['id' => $model->factura_id]);
//                        $compra->saldo -= ($compra->condicion == 'credito') ? (float)$total_retenciones : 0;
                        $model->factura_compra_id = $compra->id;
                        break;
                    case 'venta':
                        $venta = Venta::findOne(['id' => $model->factura_id]);
//                        $venta->saldo -= ($venta->condicion == 'credito') ? (float)$total_retenciones : 0;
                        $model->factura_venta_id = $venta->id;
                        break;
                }

                // obtener timbrado detalle correspondiente al nro de retencion.
                $timbradoDetalle = $model->manageNroRetencion($operacion);
                $timbradoDetalle->refresh();

                $model->timbrado_detalle_id = $timbradoDetalle->id;
                if (!$model->save()) {
                    $msg = implode(', ', $model->getErrorSummary(true));
                    throw new \Exception(-'Error validando cabecera: ' . $msg);
                }
                $model->refresh();

//                if ($compra != null) {
//                    $nuevo_saldo = number_format($compra->saldo, 2, ',', '.');
//                    if (round($compra->saldo, 2) < 0)
//                        throw new \Exception("El saldo de la factura queda en negativo. Nuevo valor: {$nuevo_saldo}");
//                    $compra->save(false);
//                }
//                if ($venta != null) {
//                    $nuevo_saldo = number_format($venta->saldo, 2, ',', '.');
//                    if (round($venta->saldo, 2) < 0)
//                        throw new \Exception("El saldo de la factura queda en negativo. Nuevo valor: {$nuevo_saldo}");
//                    $venta->save(false);
//                }
                $i = 0;
                foreach ($retencion_base_ivas as $iva_porcentaje => $valor_base_iva) {
                    $newDetalleIva = new RetencionDetalleIvas();
                    $newDetalleIva->iva_id = Iva::findOne(['porcentaje' => $iva_porcentaje])->id;
                    $newDetalleIva->base = $valor_base_iva;
                    $newDetalleIva->factor = $retencion_factor_ivas[$iva_porcentaje];
                    $newDetalleIva->retencion_id = $model->id;
                    if (!$newDetalleIva->save()) {
                        $msg = implode(', ', $model->getErrorSummary(true));
                        throw new \Exception('Error validando detalles: ' . substr($msg, 0, (strlen($msg) - 3)));
                    }

//                $newDetalleBase = new RetencionDetalleBaseIvas();
//                $newDetalleBase->iva_id = Iva::findOne(['porcentaje' => $iva_porcentaje])->id;
//                $newDetalleBase->base = $valor_base_iva;
//                $newDetalleBase->retencion_id = $model->id;
//                if (!$newDetalleBase->save()) {
//                    $trans->rollBack();
//                    $msg = '';
//                    foreach ($newDetalleBase->getErrorSummary(true) as $item) {
//                        $msg .= $item . ', ';
//                    }
//                    FlashMessageHelpsers::createWarningMessage('Error validando detalles: ' . substr($msg, 0, (strlen($msg) - 3)));
//                    goto retorno;
//                }
                }
//            foreach ($retencion_factor_ivas as $iva_porcentaje => $valor_factor_iva) {
//                $newDetalleFactor = new RetencionDetalleFactorIvas();
//                $newDetalleFactor->iva_id = Iva::findOne(['porcentaje' => $iva_porcentaje])->id;
//                $newDetalleFactor->factor = $valor_factor_iva;
//                $newDetalleFactor->retencion_id = $model->id;
//                if (!$newDetalleFactor->save()) {
//                    $trans->rollBack();
//                    $msg = '';
//                    foreach ($newDetalleFactor->getErrorSummary(true) as $item) {
//                        $msg .= $item . ', ';
//                    }
//                    FlashMessageHelpsers::createWarningMessage('Error validando detalles: ' . substr($msg, 0, (strlen($msg) - 3)));
//                    goto retorno;
//                }
//            }
                $model->total = $total_retenciones;
                if (!$model->save())
                    throw new \Exception("No se pudo guardar retención: {$model->getErrorSummaryAsString()}");
                $model->refresh();

                $factura = (isset($model->facturaCompra) ? $model->facturaCompra : $model->facturaVenta);
                $factura->actualizarSaldo();

                $trans->commit();
                FlashMessageHelpsers::createSuccessMessage('Retencion Nro: ' . $model->nro_retencion . ' se ha creado exitosamente.');
                return $this->redirect(['index', 'operacion' => $operacion]);

            } catch (\Exception $exception) {
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
                $model->id = null;
            }

        }

        return $this->render('create', [
            'model' => $model,
            // Estos dos de abajo quizas no sea util, pero si util en update. quizas.
            'detalles_base' => $detalles_base_ivas,
            'detalles_factor' => $detalles_factor_ivas
        ]);
    }

    /**
     * Updates an existing Retencion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param $operacion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\db\Exception
     * @throws \Throwable
     */
    public function actionUpdate($id, $operacion)
    {
        $model = $this->findModel($id);
        $old_retencion_total = $model->getTotalRetencion();
        Yii::warning("old total: {$old_retencion_total}");
        $old_compra = $model->facturaCompra;
        $old_venta = $model->facturaVenta;

        if (($msg = $model->checkEditability()) != '') {
            FlashMessageHelpsers::createWarningMessage("No se puede modificar esta retención: {$msg}");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        $model->factura_id = $operacion == 'compra' ? $model->factura_compra_id : $model->factura_venta_id;
        $model->timbrado_nro = $model->timbrado->nro_timbrado;
//        $detalles_base_ivas = $model->retencionDetalleBases;
//        $detalles_factor_ivas = $model->retencionDetalleFactors;
        $timbradoDetalleAnterior = $model->timbrado_detalle_id;

        if ($model->load(Yii::$app->request->post())) {
            foreach ($_POST['Retencion'] as $attribute => $value) {
                if (array_key_exists($attribute, $model->attributes))
                    $model->$attribute = $value;
            }

            $retencion_base_ivas = [];
            $retencion_factor_ivas = [];
            $total_retenciones = 0.0;
            $pattern_base = '/retencion-base_iva-([0-9]+)$/';
            $pattern_factor = '/retencion-factor_iva-([0-9]+)$/';
            $pattern_totales_iva = '/total-total-iva-([0-9]+)$/';

            // $matches: array: [0] => texto completo que matcheó, [1] => primer subgrupo que matcheó (en este caso el numero porcentaje iva)
            // Acumular retenciones de RENTA
            $total_retenciones = round(round($model->base_renta_porc, 2) * (round($model->factor_renta_porc, 2) / 100), 2);
            // Acumular retenciones de IVA
            foreach ($_POST as $key => $value) {
                if (preg_match($pattern_base, $key, $matches) && $value != "") {
                    $retencion_base_ivas[$matches[1]] = $value;
                }
                if (preg_match($pattern_factor, $key, $matches) && $value != "") {
                    $retencion_factor_ivas[$matches[1]] = $value;
                }
                if (preg_match($pattern_totales_iva, $key, $matches) && $value != "") {
                    $total_retenciones += round($value, 2);
                }
            }
            Yii::$app->session->set('total_retenciones', $total_retenciones);
            Yii::$app->session->set('model()->getTotalRetencion()', $model->getTotalRetencion());

            $trans = Yii::$app->db->beginTransaction();

            try {
                $compra = null;
                $venta = null;

                $campos_modificados = $model->getDirtyAttributes();

//                if (isset($old_compra)) {
//                    $old_compra->saldo += ($old_compra->condicion == 'credito' ? $old_retencion_total : 0);
//                    if (!$old_compra->save(false))
//                        throw new \Exception("No se pudo restaurar saldo de compra {$old_compra->nro_factura}");
//                    $old_compra->refresh();
//                } elseif (isset($old_venta)) {
//                    $old_venta->saldo += ($old_venta->condicion == 'credito' ? $old_retencion_total : 0);
//                    if (!$old_venta->save(false))
//                        throw new \Exception("No se pudo restaurar saldo de venta {$old_venta->getNroFacturaCompleto()}");
//                    $old_venta->refresh();
//                }

                switch ($operacion) {
                    case 'compra':
                        $compra = Compra::findOne(['id' => $model->factura_id]);
                        $model->factura_compra_id = $compra->id;
                        break;
                    case 'venta':
                        $venta = Venta::findOne(['id' => $model->factura_id]);
                        $model->factura_venta_id = $venta->id;
                        break;
                }

                $timbradoDetalle = $model->manageNroRetencion($operacion); // devuelve un timbrado detalle nuevo o uno que tiene rango y slot.
                $model->timbrado_detalle_id = $timbradoDetalle->id;
                if (!$model->save()) {
                    throw new \Exception("Error validando cabecera: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                // borrar detalles anteriores
                foreach ($model->detalles as $detalle) {
                    if (!$detalle->delete()) {
                        throw new \Exception('Error borrando detalles bases anteriores.');
                    }
                }

                // guardar detalles nuevos
                foreach ($retencion_base_ivas as $iva_porcentaje => $valor_base_iva) {
                    $newDetalleIva = new RetencionDetalleIvas();
                    $newDetalleIva->iva_id = Iva::findOne(['porcentaje' => $iva_porcentaje])->id;
                    $newDetalleIva->base = $valor_base_iva;
                    $newDetalleIva->factor = $retencion_factor_ivas[$iva_porcentaje];
                    $newDetalleIva->retencion_id = $model->id;
                    if (!$newDetalleIva->save()) {
                        $msg = implode(', ', $newDetalleIva->getErrorSummary(true));
                        throw new \Exception('Error validando detalles: ' . $msg);
                    }
                }

                $model->refresh();
                $model->total = $model->getTotalRetencion();
                if (!$model->save())
                    throw new \Exception("No se pudo actualizar retención: {$model->getErrorSummaryAsString()}");
                $model->refresh();

//                if ($compra != null) {
//                    $compra->saldo -= ($compra->condicion == 'credito' ? $model->total : 0);
//                    $nuevo_saldo = number_format($compra->saldo, 2, ',', '.');
//                    if (round($compra->saldo, 2) < 0)
//                        throw new \Exception("El saldo de la factura queda en negativo. Nuevo valor: {$nuevo_saldo}");
//                    $compra->save(false);
//                }
//                if ($venta != null) {
//                    $venta->saldo -= ($venta->condicion == 'credito' ? $model->total : 0);
//                    $nuevo_saldo = number_format($venta->saldo, 2, ',', '.');
//                    if (round($venta->saldo, 2) < 0)
//                        throw new \Exception("El saldo de la factura queda en negativo. Nuevo valor: {$nuevo_saldo}");
//                    $venta->save(false);
//                }

                // Borrar detalle de timbrado si no tiene asociado mas retenciones.
                // Este borrado es seguro debido a que los timbrados son unicamente para retenciones. No es compartido
                // por facturas o notas por ej. Asi que si 1 detalle ya no es utilizado por ninguna retencion, se puede
                // borrar con total seguridad.
                $retencionesAsociadas = Retencion::findAll(['timbrado_detalle_id' => $timbradoDetalleAnterior]);
                if (empty($retencionesAsociadas)) {
                    $timbradoDetalleAnterior = TimbradoDetalle::findOne(['id' => $timbradoDetalleAnterior]);
                    if (isset($timbradoDetalleAnterior)) {
                        $timbradoDetalleAnterior->delete();
                    }
                }

                // restaurar saldo de factura anterior y
                // actualizar saldo de factura actual
                $model->refresh();
                $factura = (isset($model->facturaCompra) ? $model->facturaCompra : $model->facturaVenta);
                if (isset($old_venta)) {
                    $old_venta->refresh();
                    $old_venta->actualizarSaldo();
                    if ($old_venta->id != $factura->id)
                        $factura->actualizarSaldo();
                } elseif (isset($old_compra)) {
                    $old_compra->refresh();
                    $old_compra->actualizarSaldo();
                    if ($old_compra->id != $factura->id)
                        $factura->actualizarSaldo();
                }

                $trans->commit();
                FlashMessageHelpsers::createSuccessMessage('Retencion Nro: ' . $model->nro_retencion . ' se ha creado exitosamente.');
                return $this->redirect(['index', 'operacion' => $operacion]);

            } catch (\Exception $exception) {
                //throw $exception;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        retorno:;
        return $this->render('update', [
            'model' => $model,
            // Estos dos de abajo quizas no sea util, pero si util en update. quizas.
//            'detalles_base' => $detalles_base_ivas,
//            'detalles_factor' => $detalles_factor_ivas
        ]);
    }

    /**
     * Deletes an existing Retencion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id, $operacion)
    {
        $model = $this->findModel($id);
        if (($msg = $model->checkEditability()) != '') {
            FlashMessageHelpsers::createWarningMessage("No se puede eliminar esta retención: {$msg}");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }
        $nro_retencion = $model->nro_retencion;
        $factura = (isset($model->facturaVenta) ? $model->facturaVenta : $model->facturaCompra);

        // Verificar si hay asiento por esta retencion
        if ($model->asiento != null) {
            FlashMessageHelpsers::createWarningMessage('Existen uno o más asientos generados sobre esta retención. Elimínelos primero y vuelva a intentar.');
            goto retorno;
        }

        $transaccion = Yii::$app->db->beginTransaction();
        try {
            /*$compra = Compra::findOne(['id' => $model->factura_compra_id]);
            $venta = Venta::findOne(['id' => $model->factura_venta_id]);*/
            $detalles = RetencionDetalleIvas::findAll(['retencion_id' => $model->id]);
            // Restaurar saldos de las facturas
            /*if (isset($compra)) {
                $compra->saldo += ($compra->condicion == 'credito') ? $model->getTotalRetencion() : 0;
                $compra->save(false);
            }
            if (isset($venta)) {
                $venta->saldo += ($venta->condicion == 'credito') ? $model->getTotalRetencion() : 0;
                $venta->save(false);
            }*/
            // Borrar las bases y factores
            foreach ($detalles as $detalle) {
                if (!$detalle->delete()) {
                    throw new \Exception("Error borrando detalles de retención: {$detalle->getErrorSummaryAsString()}");
                }
            }

            $timbradoDetalle = $model->timbradoDetalle;

            // Borrar la retencion
            if (!$model->delete()) {
                throw new Exception("Error al intentar eliminar la retención: {$model->getErrorSummaryAsString()}");
            }
            $model->refresh();

            $factura->refresh();
            $factura->actualizarSaldo(true);

            // Si no existe retenciones ni parametro de contabilidad asociada al timbradoDetalle, borra.
            $empresa_id = Yii::$app->session->get('core_empresa_actual');
            $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
            $parmName = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-timbrado_para_retenciones";
            $parmCont = ParametroSistema::findOne(['nombre' => $parmName]);
            if (!$timbradoDetalle->getRetenciones()->exists() && (!isset($parmCont) || $parmCont->valor == '')) {
                $timbrado = $timbradoDetalle->timbrado;
                if ($timbradoDetalle->delete()) {
                    $timbrado->refresh();
                    if (!$timbrado->getTimbradoDetalles()->exists())
                        $timbrado->delete();
                }
            }

            $transaccion->commit();
            FlashMessageHelpsers::createSuccessMessage('La retención ' . $nro_retencion . ' se ha eliminado exitosamnte.');
        } catch (\Exception $exception) {
            $transaccion->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        retorno:;
        return $this->redirect(['index', 'operacion' => $operacion]);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionGetTotalesFields($operacion)
    {
        $result = [];
        $factura_id = $_GET['factura_id'];
        $factura_total = $operacion == 'compra' ? Compra::findOne(['id' => $factura_id])->total : Venta::findOne(['id' => $factura_id])->total;
        $factura_saldo = $operacion == 'compra' ? Compra::findOne(['id' => $factura_id])->saldo : Venta::findOne(['id' => $factura_id])->saldo;
        $factura_gravada = 0;

        $ivactas = ($operacion == 'compra') ?
            CompraIvaCuentaUsada::find()->where(['factura_compra_id' => $factura_id])->all() :
            VentaIvaCuentaUsada::find()->where(['factura_venta_id' => $factura_id])->all();
        foreach ($ivactas as $ivaCtaUsada) {
            if (isset($ivaCtaUsada->ivaCta)) {
                $factura_gravada += $ivaCtaUsada->monto / (100 + $ivaCtaUsada->ivaCta->iva->porcentaje) * 100;
            } else {
                $factura_gravada += $ivaCtaUsada->monto;
            }
        }

        $iva_ctas_usadas = $operacion == 'compra' ? CompraIvaCuentaUsada::findAll(['factura_compra_id' => $factura_id]) :
            VentaIvaCuentaUsada::findAll(['factura_venta_id' => $factura_id]);
        foreach ($iva_ctas_usadas as $iva_ctas_usada) {
            if (isset($iva_ctas_usada->ivaCta)) {
                $result[] = '#total-iva-' . $iva_ctas_usada->ivaCta->iva->porcentaje . '';
            } else {
                $result[] = '#total-iva-0';
            }
        }
        $result[] = $factura_saldo; // ahora que las retenciones son aplicables a facturas contado y credito, saldo no tiene sentido
        $result[] = round($factura_gravada);  // es para el campo `retencion base renta %` por lo que no hay que calcular su 30%
        $result[] = $factura_total;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionGetTotalIvaMonto($operacion)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $factura_id = $_GET['factura_id'];
        $porcentaje_iva = $_GET['porcentaje_iva'];
        $retencion_id = Yii::$app->request->get('retencion_id');
        $action_id = Yii::$app->request->get('action_id');
        $factura_id_attribute = "factura_{$operacion}_id";

        $iva = Iva::findOne(['porcentaje' => $porcentaje_iva]);
        $ivaCta = IvaCuenta::findOne(['iva_id' => $iva->id]);
        if ($porcentaje_iva != '0') {
            $iva_cta_usada = $operacion == 'compra' ?
                CompraIvaCuentaUsada::findOne(['factura_compra_id' => $factura_id, 'iva_cta_id' => $ivaCta->id]) :
                VentaIvaCuentaUsada::findOne(['factura_venta_id' => $factura_id, 'iva_cta_id' => $ivaCta->id]);
        } else {
            $iva_cta_usada = $operacion == 'compra' ?
                CompraIvaCuentaUsada::find()->where(['factura_compra_id' => $factura_id])->andWhere(['IS', 'iva_cta_id', null])->one() :
                VentaIvaCuentaUsada::find()->where(['factura_venta_id' => $factura_id])->andWhere(['IS', 'iva_cta_id', null])->one();
        }
        return $iva_cta_usada->monto;
    }

    public function actionCheckNroRetencion() // es el metodo usado por defered del validador.
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $error = '';

        $operacion = $_GET['operacion'];
        $factura_id = $_GET['factura_id'];
        $nro_retencion = $_GET['nro_retencion'];
        $action_id = $_GET['action_id'];
        $retencion_id = $_GET['retencion_id'];

        return Retencion::checkNroRetencion($nro_retencion, $retencion_id);

//        $query = Retencion::find()->where([
//            'nro_retencion' => $nro_retencion,
//            'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
//            'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
//        ])->andFilterWhere(['!=', 'id', $retencion_id]);
//
//        if ($query->exists()) {
//            return ['result' => '', 'error' => "Nro de Retención {$nro_retencion} ya fue utilizado.", 'attribute' => 'nro_retencion'];
//        }
//
//        if (strlen(str_replace('_', '', $nro_retencion)) <15)
//            return ['result' => '', 'error' => "Error en el formato del número de retención.", 'attribute' => 'nro_retencion'];
//
//        return ['result' => '', 'error' => ""];

//        if ($factura_id != "" && $nro_retencion != "") {
//
//            $prefijo = substr($nro_retencion, 0, 7);
//            $siete_digitos = substr($nro_retencion, 8, strlen($nro_retencion));
//            if (strlen(str_replace('_', '', $siete_digitos)) < 7) {
//                if (strlen($error) == 0)
//                    $error .= 'Formato no válido para Nro de retención';
//                else
//                    $error .= ' y ' . 'Formato no válido para Nro de retención';
//                goto retorno;
//            }
//            $siete_digitos = (int)$siete_digitos;
//
//            $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-tipodoc_set_retencion";
//            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
//
//            if ($parametro_sistema == null) {
//                if (strlen($error) == 0)
//                    $error .= 'Falta definir el tipo de documento para retenciones en el parámetro del sistema.';
//                else
//                    $error .= ' y ' . 'Falta definir el tipo de documento para retenciones en el parámetro del sistema.';
//                goto retorno;
//            }
//
//            $tipodoc_set_retencion = TipoDocumentoSet::findOne(['id' => $parametro_sistema->valor]);
//
//            if ($tipodoc_set_retencion == null) {
//                if (strlen($error) == 0)
//                    $error .= 'Falta definir el tipo de documento set para retenciones.';
//                else
//                    $error .= ' y ' . 'Falta definir el tipo de documento set para retenciones.';
//                goto retorno;
//            }
//
//            $entidad = null;
//            if ($operacion == 'compra') {
//                $ruc = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->ruc;
//                $entidad = Entidad::findOne(['ruc' => $ruc]);
//            } else {
//                $venta = Venta::findOne(['id' => $factura_id]);
//                $entidad = $venta->entidad;
//            }
//
//            $timbrado_detalles = TimbradoDetalle::find()->alias('det')
//                ->leftJoin('cont_timbrado AS tim', 'det.timbrado_id = tim.id')
//                ->where(['tim.entidad_id' => $entidad->id])
//                ->andWhere(['=', 'det.tipo_documento_set_id', $tipodoc_set_retencion->id])
//                ->andWhere(['=', 'det.prefijo', $prefijo])
//                ->andWhere(['<=', 'det.nro_inicio', $siete_digitos])
//                ->andWhere(['>=', 'det.nro_fin', $siete_digitos])->all();
//
//            if (sizeof($timbrado_detalles) == 0) {
//                if (strlen($error) == 0)
//                    $error .= 'Este número no pertenece a ningún timbrado.';
//                else
//                    $error .= ' y ' . 'Este número no pertenece a ningún timbrado.';
//            }
//
//            $retencion = Retencion::find()->where(['nro_retencion' => $nro_retencion]);
//
//            if ($action_id != 'create' || $retencion_id != "") {
//                $retencion->andWhere(['!=', 'id', $retencion_id]);
//            }
//
//            if ($retencion->exists()) {
//                if (strlen($error) == 0)
//                    $error .= "Este número ya fue utilizado por la retención (id = {$retencion->one()->id}).";
//                else
//                    $error .= ' y ' . "Este número ya fue utilizado por la retención (id = {$retencion->one()->id}).";
//            }
//        }
//
//        retorno:;
//        return ['result' => '', 'error' => $error];
    }

    // TODO : Deprecar luego de haber adaptado el modal de retencion desde recibo.
    public function actionCheckNroRetencion2($operacion)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->checkNroRetencion($_GET['nro_retencion'], $operacion);

    }

    public function actionGetTimbradoDetalleId($nro_retencion, $operacion)
    {
        $operacion = $_GET['operacion'];
        $factura_id = $_GET['factura_id'];
        $nro_retencion = $_GET['nro_retencion'];
        $action_id = $_GET['action_id'];
        $retencion_id = $_GET['retencion_id'];

        try {

            if ($factura_id != "" && $nro_retencion != "") {

                $prefijo = substr($nro_retencion, 0, 7);
                $siete_digitos = substr($nro_retencion, 8, strlen($nro_retencion));
                if (strlen(str_replace('_', '', $siete_digitos)) < 7) {
                    return false;
                }
                $siete_digitos = (int)$siete_digitos;
                $empresa_id = Yii::$app->session->get('core_empresa_actual');
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-tipodoc_set_retencion";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);

                if ($parametro_sistema == null) {
                    return false;
                }

                $tipodoc_set_retencion = TipoDocumentoSet::findOne(['id' => $parametro_sistema->valor]);

                if ($tipodoc_set_retencion == null) {
                    return false;
                }

                $entidad = null;
                if ($operacion == 'compra') {
                    $ruc = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->ruc;
                    $entidad = Entidad::findOne(['ruc' => $ruc]);
                } else {
                    $venta = Venta::findOne(['id' => $factura_id]);
                    $entidad = $venta->entidad;
                }

                $timbrado_detalles = TimbradoDetalle::find()->alias('det')
                    ->leftJoin('cont_timbrado AS tim', 'det.timbrado_id = tim.id')
                    ->where(['tim.entidad_id' => $entidad->id])
                    ->andWhere(['=', 'det.tipo_documento_set_id', $tipodoc_set_retencion->id])
                    ->andWhere(['=', 'det.prefijo', $prefijo])
                    ->andWhere(['<=', 'det.nro_inicio', $siete_digitos])
                    ->andWhere(['>=', 'det.nro_fin', $siete_digitos])->all();

                if (sizeof($timbrado_detalles) == 0) {
                    return false;
                }

                $retencion = Retencion::find()->where(['nro_retencion' => $nro_retencion]);

                if ($action_id == 'update' || $action_id != 'create') {
                    $retencion->andWhere(['!=', 'id', $retencion_id]);
                }

                if ($retencion->exists()) {
                    return false;
                }

                return $timbrado_detalles[0]->id;
            }
        } catch (Exception $exception) {
            Yii::warning($exception->getMessage());
        }
        return false;
    }

    public function actionGetPorcentajeIvasFactura($operacion)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $factura_id = $_GET['factura_id'];
        $factura = $operacion == 'compra' ? Compra::findOne(['id' => $factura_id]) : Venta::findOne(['id' => $factura_id]);
        if ($factura == null) {
            FlashMessageHelpsers::createInfoMessage('Seleccione una factura.');
            return [];
        }
        $ivaCuentas = $factura->ivasCuentaUsadas;
        $array_iva_porcentajes = [];
        foreach ($ivaCuentas as $ivaCuenta) {
            if ($ivaCuenta->ivaCta != null)
                $array_iva_porcentajes[$ivaCuenta->ivaCta->iva->porcentaje] = $ivaCuenta->ivaCta->iva->porcentaje;
        }
        ksort($array_iva_porcentajes); // ordenar por indice, que en este caso es el porcentaje iva que maneja.
        $result = [];
        foreach ($array_iva_porcentajes as $item) {
            $result[] = $item;
        }
        return $result;
    }

    public function actionGetFactor($operacion)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelID = $_GET['retencion_id'];
        $iva_porcentaje = $_GET['iva_porcentaje']; // siempre se recibe los porcentajes de todos los ivas que existe en el sistema, aun cuando el detalleBase no este relacionado a tal porcentaje en especifico...
        $iva = Iva::findOne(['porcentaje' => $iva_porcentaje]);
        if ($iva == null) return false; // ... por eso no se realiza Flashmessage
        $factor = RetencionDetalleIvas::find()->where(['retencion_id' => $modelID, 'iva_id' => $iva->id])->one();
        if ($factor == null) return "0"; // por defecto, si no existe detallefactor asociado al iva cuyo porcentaje se especifica en GET
        else return $factor->factor;
    }

    /**
     * @param $operacion
     * @param $factura_id
     * @param bool $submit
     * @param string $retencion_id
     * @param string $quiere_crear_nuevo
     * @return array|bool|string|Response
     * @throws \Throwable
     */
    public function actionManejarRetenciones($operacion, $factura_id, $submit = false, $retencion_id = "", $quiere_crear_nuevo = 'no')
    {
        if ($factura_id == '') {
            FlashMessageHelpsers::createWarningMessage('Debe seleccionar primero una factura.');
            return false;
        }
        $model = null;
        $campo_factura_id = $operacion == 'compra' ? "factura_compra_id" : 'factura_venta_id';
        // Se eligio una retencion desde el modal.
        if ($retencion_id != "") {
            $model = Retencion::findOne(['id' => $retencion_id]);
        } else {
            // Se eliminó con la X la retención seleccionada, indicando que quiere crear uno nuevo.
            if ($quiere_crear_nuevo == 'si') {
                $model = new Retencion();
                $model->fecha_emision = date('Y-m-d');
//                $model->$campo_factura_id = $factura_id;
                $model->empresa_id = Yii::$app->session->get('core_empresa_actual');
                $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
            } // Acaba de abrir el modal.
            else {
                $model = Retencion::findOne([$campo_factura_id => $factura_id]);
                if (!isset($model)) {
                    $model = new Retencion();
                    $model->fecha_emision = date('Y-m-d');
//                    $model->$campo_factura_id = $factura_id;
                    $model->empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                }
            }
        }
        $model->factura_id = $factura_id; // factura_id usado en update/create, y esta configurado como required.
        $model->retencion_id_selector = $model->id; // si el model se seteo mediante un findOne(), se asigna el id, sino null o vacio.

        $detalles_base_ivas = [];
        $detalles_factor_ivas = [];

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            return ActiveForm::validate($model);
        }

        // Aqui no hace falta controlar la excepcion que pueda lanzar porque
        // el modal de retenciones se abre solamente si existen los parametros necesarios
        // para retenciones correctamente en la empresa actual.
        if ($operacion == 'compra') {
            $model->timbrado_nro = Retencion::getTimbradoRetencion()->nro_timbrado;
        } else {
            $model->$campo_factura_id = $factura_id;
            $entidad_id = $model->facturaVenta->entidad_id;
            $tipoDocSet = Retencion::getTipodocSetRetencion();
            $timbrado = Timbrado::find()->alias('tim')
                ->leftJoin('cont_timbrado_detalle as det', 'tim.id = det.timbrado_id')
                ->where(['tim.entidad_id' => $entidad_id, 'det.tipo_documento_set_id' => $tipoDocSet->id]);
            if ($timbrado->exists()) {
                $timbrado = $timbrado->one();
                /** @var Timbrado $timbrado */
                $model->timbrado_nro = $timbrado->nro_timbrado;
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            foreach ($_POST['Retencion'] as $attribute => $value) {
                if (array_key_exists($attribute, $model->attributes))
                    $model->$attribute = $value;
            }

            $retencion_base_ivas = [];
            $retencion_factor_ivas = [];
            $total_retenciones = 0.0;
            $total_retenciones += round(round($model->base_renta_porc, 2) * (round($model->factor_renta_porc, 2) / 100), 2);
            $pattern_base = '/retencion-base_iva-([0-9]+)$/';
            $pattern_factor = '/retencion-factor_iva-([0-9]+)$/';
            $pattern_totales_iva = '/total-total-iva-([0-9]+)$/';

            // $matches: array: [0] => texto completo que matcheó, [1] => primer subgrupo que matcheó (en este caso el numero porcentaje iva)
            foreach ($_POST as $key => $value) {
                if (preg_match($pattern_base, $key, $matches) && $value != "") {
                    $value = str_replace('.', '', $value);
                    $value = str_replace(',', '.', $value);
                    $retencion_base_ivas[$matches[1]] = (float)$value;
                }
                if (preg_match($pattern_factor, $key, $matches) && $value != "") {
                    $value = str_replace('.', '', $value);
                    $value = str_replace(',', '.', $value);
                    $retencion_factor_ivas[$matches[1]] = (float)$value;
                }
                if (preg_match($pattern_totales_iva, $key, $matches) && $value != "") {
                    $value = str_replace('.', '', $value);
                    $value = str_replace(',', '.', $value);
                    $total_retenciones += (float)$value;
                }
            }
            Yii::$app->session->set('total_retenciones', $total_retenciones);

            $trans = Yii::$app->db->beginTransaction();
            try {
                Retencion::isCreable($operacion);

//                $compra = null;
//                $venta = null;
                switch ($operacion) {
                    case 'compra':
                        $compra = Compra::findOne(['id' => $model->factura_id]);
                        $model->factura_compra_id = $compra->id;
                        break;
                    case 'venta':
                        $venta = Venta::findOne(['id' => $model->factura_id]);
                        $model->factura_venta_id = $venta->id;
                        break;
                }
//                // Calcular la diferencia en el total de retenciones y actualizar el saldo
//                if ($compra != null) {
//                    $compra->saldo -= ($compra->condicion == 'credito') ? (float)$total_retenciones - (float)$model->getTotalRetencion() : 0;
//                }
//                if ($venta != null) {
//                    $venta->saldo -= ($venta->condicion == 'credito') ? (float)$total_retenciones - (float)$model->getTotalRetencion() : 0;
//                }
                $model->tipo_factura = $operacion;

                $timbradoDetalle = $model->manageNroRetencion($operacion); // devuelve un timbrado detalle nuevo o uno que tiene rango y slot.
                $model->timbrado_detalle_id = $timbradoDetalle->id;
                $model->total = $total_retenciones;

                // guardar retencion para generar id. si es update, no tiene efecto
                if (!$model->save()) {
                    throw new Exception("Error validando cabecera: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

//                // guardar compra venta
//                if ($compra != null) {
//                    $nuevo_saldo = number_format($compra->saldo, 2, ',', '.');
//                    if (round($compra->saldo, 2) < 0)
//                        throw new \Exception("El saldo de la factura queda en negativo. Nuevo valor: {$nuevo_saldo}");
//                    $compra->save(false);
//                }
//                if ($venta != null) {
//                    $nuevo_saldo = number_format($venta->saldo, 2, ',', '.');
//                    if (round($venta->saldo, 2) < 0)
//                        throw new \Exception("El saldo de la factura queda en negativo. Nuevo valor: {$nuevo_saldo}");
//                    $venta->save(false);
//                }
                // borrar detalles anteriores de la retencion
                foreach ($model->detalles as $detalle) {
                    if (!$detalle->delete()) {
                        throw new Exception('Error borrando detalles anteeriores.');
                    }
                }

                // guardar los nuevos detalles de la retencion
                foreach ($retencion_base_ivas as $iva_porcentaje => $valor_base_iva) {
                    $newDetalleIva = new RetencionDetalleIvas();
                    $newDetalleIva->iva_id = Iva::findOne(['porcentaje' => $iva_porcentaje])->id;
                    $newDetalleIva->base = $valor_base_iva;
                    $newDetalleIva->factor = $retencion_factor_ivas[$iva_porcentaje];
                    $newDetalleIva->retencion_id = $model->id;
                    if (!$newDetalleIva->save()) {
                        throw new Exception("Error validando detalles: {$newDetalleIva}");
                    }
                    $newDetalleIva->refresh();
                }

                // actualizar saldo de factura
                $model->refresh();
                $model->total = $model->getTotalRetencion();
                if (!$model->save())
                    throw new Exception("Error validando cabecera: {$model->getErrorSummaryAsString()}");
                $model->refresh();
                $factura = (isset($model->facturaCompra) ? $model->facturaCompra : $model->facturaVenta);
                $factura->actualizarSaldo();

                $trans->commit();
                $skey = 'cont_retencion_id';
                $session = Yii::$app->session;
                if ($session->has($skey)) {
                    $sval = $session->get($skey);
                    $index = array_search($model->id, array_column($sval, 'retencion_id'));
                    if (!$index) {
                        $sval[] = ['retencion_id' => $model->id];
                        $session->set($skey, $sval);
                    }
                } else {
                    $sval[] = ['retencion_id' => $model->id];
                    $session->set($skey, $sval);
                }
                FlashMessageHelpsers::createSuccessMessage('Retencion Nro: ' . $model->nro_retencion . ' se estableció correctamente.');

            } catch (\Exception $exception) {
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        retorno:;
        return $this->renderAjax('recibo-retencion/_form_modal_retencion', [
            'model_retencion' => $model,
            // Estos dos de abajo quizas no sea util, pero si util en update. quizas.
            'bases' => $detalles_base_ivas,
            'factors' => $detalles_factor_ivas,
            'factura_id' => $factura_id,
            'retencion_id' => $retencion_id,
        ]);
    }

    public function actionGetRetencionesByFactura($operacion, $factura_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $retenciones = Retencion::find()
            ->where([$operacion == 'compra' ? 'factura_compra_id' : 'factura_venta_id' => $factura_id])
            ->select(['id', "CONCAT((id), (' - Retención Nª '), (nro_retencion), (' '), (DATE_FORMAT(fecha_emision,' del %d-%m-%Y')), ('.')) AS text"])->asArray()->all();
        return ['results' => array_values($retenciones)];
    }

    /**
     * @param $operacion
     * @return string
     * @throws \Throwable
     */
    public function actionCreateFile($operacion)
    {
        if (!$this->verificarSession()) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar empresa actual y periodo contable.');
            return $this->redirect(['index', 'opperacion' => $operacion]);
        }

        // verificar que exista entidad correspondiente a empresa actual
        $empresa_actual = Empresa::findOne(Yii::$app->session->get('core_empresa_actual'));
        $entidad = Entidad::find()->where(['ruc' => $empresa_actual->ruc])->one(); // de la empresa actual.

        if (!isset($entidad)) {
            FlashMessageHelpsers::createWarningMessage("Falta registrar Entidad correspondiente para Empresa Actual primero.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        // verificar que exista tipo_documento_set_id como parametro de empresa
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $periodo_model = EmpresaPeriodoContable::findOne(['id' => $periodo_id]);
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-tipodoc_set_retencion";
        $param_sys_tipodocuSetRetencion = ParametroSistema::findOne(['nombre' => $nombre]);
        if (!isset($param_sys_tipodocuSetRetencion)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir primero el Tipo de documento SET para retenciones en el parámetro de la Empresa Actual.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        $model = new RetencionArchivo();
        $mensaje = "";
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);//5 min
        if ($model->load(Yii::$app->request->post())) {
            // cargar el archivo en el atributo 'archivo' del modelo.
            $model->archivo = UploadedFile::getInstance($model, 'archivo');
            $transaction = Yii::$app->getDb()->beginTransaction();
            try {
                // obtener la hoja del excel en $sheetData
                $objPHPExcel = IOFactory::load($model->archivo->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $huboError = false;
                $empresa_no_existe = [];
                $empresa_no_actual = [];
                $facturas_noexiste = [];
                $retenciones_novalidas = [];
                $retenciones_existentes = [];
                $retencion_periodo_error = [];
                $factura_nro_formatoerroeno = [];
                $factura_timbrado_nocorresponde = [];
                $error_entidad_emp_actual_noexiste = [];
                $retencion_fecha_malformat = [];
                $retencion_timbrado_noexiste = [];

                foreach ($sheetData as $rownro => $row) {
                    $map = [
                        'nro_retencion' => 'D', 'ruc_informate' => 'E', 'nro_factura' => 'N', 'timbrado_factura' => 'P',
                        'fecha_emision' => 'L', 'retencion_total' => 'V', 'total_cabezas' => 'W', 'total_toneladas' => 'X'
                    ];
                    /**
                     * D: Número de retención
                     * E: RUC Informate: Empresa actual.
                     * L: Fecha de emisión de la retención
                     * M: Retencion de RENTA.
                     * N: Número de Factura
                     * P: Número de Timbrado de la Factura
                     * V: Retención total del IVA
                     * W: Total cabezas (para sector agropecuario)
                     * X: Total toneladas (para sector agropecuario)
                     */
                    if ($rownro > 1 && isset($row['A'])) {
                        #verificar fecha de retencion
                        #se asume que el formato de la fecha de la columna L es dd/mm/yyyy
                        if (explode('/', $row['L'])[2] != $periodo_model->anho) {
                            $huboError = true;
                            $retencion_periodo_error[$rownro] = "La fecha `{$row['L']}` de retención de la fila {$rownro} no corresponde con el año del periodo contable actual.";
                            continue;
                        }

                        $factura_id = $operacion == 'compra' ? 'factura_compra_id' : 'factura_venta_id';

                        $retencion = Retencion::findOne(['nro_retencion' => $row[$map['nro_retencion']], $factura_id => $row[$map['nro_factura']]]);
                        if ($retencion != null) {
                            $huboError = true;
                            $retenciones_existentes[$rownro] = "La Retención {$row[$map['nro_retencion']]} asociado a la factura {$row[$map['nro_factura']]} ya existe.";
                            continue;
                        }
                        // TODO: Definir si hay que:
                        // TODO: determinar si RUC Informate del archivo coincide con la empresa actual o,
                        // TODO: guardar directamente las retenciones con el id de empresa actual
                        // TODO: o guardar las retenciones con el id de la empresa indicada en el archivo.
//                        $empresa = Empresa::findOne(['ruc' => $row[$columnas['ruc_informate']]]);
//                        if ($empresa == null) {
//                            $huboError = true;
//                            $empresa_no_existe[$rownro] = "No existe ninguna empresa con el RUC {$row[$columnas['ruc_informate']]}.";
//                            continue;
//                        }
//                        if (\Yii::$app->session->get('core_empresa_actual') == $empresa->id) {
//                            $huboError = true;
//                            $empresa_no_actual[$rownro] = "El RUC de la empresa actual no es igual al RUC {$row[$columnas['ruc_informate']]} de la empresa informante.";
//                            continue;
//                        }

                        $fecha_recepcion = date_create_from_format('d/m/Y', $row[$map['fecha_emision']]);
                        if (!$fecha_recepcion) {
                            $huboError = true;
                            $retencion_fecha_malformat[$rownro] = "El formato de la fecha de retención {$row[$map['fecha_emision']]} es incorrecto.";
                            continue;
                        }

                        if (strlen($row[$map['nro_factura']]) != 15) {
                            $huboError = true;
                            $factura_nro_formatoerroeno[$rownro] =
                                'El número de Factura, ' . $row[$map['nro_factura']] . ', tiene formato incorrecto.';
                            continue;
                        }

                        $factura = $operacion == 'compra' ?
                            Compra::find()->where(['nro_factura' => $row[$map['nro_factura']], 'empresa_id' => Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]) :
                            Venta::find()->where(['estado' => 'vigente', 'prefijo' => substr($row[$map['nro_factura']], 0, 7), 'nro_factura' => substr($row[$map['nro_factura']], 8, 7), 'empresa_id' => Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
                        if (!$factura->exists()) {
                            $huboError = true;
                            $facturas_noexiste[$rownro] = "Factura " . ucfirst($operacion) . " {$row[$map['nro_factura']]} no existe en la empresa/periodo actual.";
                            continue;
                        }
                        $factura = $factura->one();
                        /** @var Venta|Compra $factura */
                        $cotizacion = ($operacion == 'compra') ? ($factura->cotizacion == '' ? 1 : round($factura->cotizacion, 2)) : ($factura->valor_moneda == '' ? 1 : round($factura->valor_moneda));

                        $tipodoc_set_retencion = TipoDocumentoSet::findOne(['id' => $param_sys_tipodocuSetRetencion->valor]);
                        $nro_splitted = explode('-', $row[$map['nro_retencion']]);
                        $prefijo = implode('-', [$nro_splitted[0], $nro_splitted[1]]);
                        $last_gr = $nro_splitted[2];
                        $query = TimbradoDetalle::find()->alias('det');
                        $query->joinWith('timbrado as tim');
                        $entidad = null;
                        // determinar la entidad segun compra/venta.
                        if ($operacion == 'compra') {
                            $entidad = Entidad::find()->where(['ruc' => $empresa_actual->ruc])->one(); // de la empresa actual.
                        } else {
                            $entidad = $factura->entidad; // de la factura de venta.
                        }
                        $query->where(['tim.entidad_id' => $entidad->id]);      // filtrar por entidad.
                        $query->andWhere(['<=', 'tim.fecha_fin', $fecha_recepcion->format('Y-m-d')]);
                        $query->andWhere(['<=', 'det.prefijo', $prefijo]);
                        $query->andWhere(['<=', 'det.nro_inicio', $last_gr]);
                        $query->andWhere(['>=', 'det.nro_fin', $last_gr]);
                        $query->andWhere(['=', 'det.tipo_documento_set_id', $tipodoc_set_retencion->id]);
                        if (!$query->exists()) {
                            if ($operacion == 'compra') {
                                $huboError = true;
                                $retencion_timbrado_noexiste[$rownro] = "No se pudo encontrar ningún Timbrado que corresponda al Nro de Retención {$row[$map['nro_retencion']]}.";
                                continue;
                            } else {
                                #Crear un timbrado nuevo y permitir continuar como flujo normal.
                                #Para la entidad vendedora, no le importa en realidad si la retencion emitida por su cliente
                                # es o no correcta y contemplada bajo algun timbrado vigente, así que creamos por bypass un timbrado.
                                $newTimbrado = new Timbrado(['scenario' => Timbrado::SCENARIO_RETENCION]);
                                $newTimbrado->nro_timbrado = 4294967295;
                                while (Timbrado::find()->where(['nro_timbrado' => $newTimbrado->nro_timbrado])->exists())
                                    $newTimbrado->nro_timbrado--;
                                $newTimbrado->fecha_inicio = date('01-01-Y');
                                $newTimbrado->fecha_fin = date('t-m-Y', strtotime($newTimbrado->fecha_inicio . ' +1 year'));
                                $newTimbrado->entidad_id = $entidad->id;
                                $newTimbrado->ruc = $entidad->ruc;
                                $newTimbrado->nombre_entidad = $entidad->razon_social;
                                $newTimbradoDet = new TimbradoDetalle();
                                $newTimbradoDet->tipo_documento_set_id = $tipodoc_set_retencion->id;
                                $newTimbradoDet->prefijo = $prefijo;
                                $newTimbradoDet->nro_inicio = 1;
                                $newTimbradoDet->nro_fin = 9999999;
                                if (!$newTimbrado->save())
                                    throw new \Exception("Error generando timbrado para retención (recibida): {$newTimbrado->getErrorSummaryAsString()}");
                                $newTimbrado->refresh();
                                $newTimbradoDet->timbrado_id = $newTimbrado->id;
                                if (!$newTimbradoDet->save())
                                    throw new \Exception("Error creando detalle del timbrado autogenerado para retención (recibida): {$newTimbradoDet->getErrorSummaryAsString()}");
                                $newTimbradoDet->refresh();
                                $query = TimbradoDetalle::find()->where(['id' => $newTimbradoDet->id]);
                            }
                        }

                        #verificar si factura y timbrado corresponden
                        $timbrado = $factura->timbradoDetalle->timbrado;
                        $factura->ivasCuentaUsadas;
                        if ($timbrado->nro_timbrado != $row[$map['timbrado_factura']]) {
                            $huboError = true;
                            $factura_timbrado_nocorresponde[$rownro] = 'La Factura ' . $row[$map['nro_factura']] . ' no está asociada al Timbrado ' . $row[$map['timbrado_factura']] . '.';
                            continue;
                        }

                        #crear retencion
                        $retencion = new Retencion();
                        $retencion->empresa_id = Yii::$app->session->get('core_empresa_actual');
                        $retencion->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                        $retencion->nro_retencion = $row[$map['nro_retencion']];
                        $retencion->$factura_id = $factura->id;
                        $retencion->factura_id = $factura->id; # campo virtual
                        $retencion->fecha_emision = $fecha_recepcion->format('Y-m-d');
                        $retencion->total = str_replace(',', '', $row[$map['retencion_total']]);
                        $retencion->tipo_factura = $operacion;
                        $retencion->timbrado_detalle_id = $query->one()->id;


                        # CALCULO DE RETENCION DE RENTA

                        $total_retencion_renta = round($row['M'], 2);
                        $retencion->base_renta_porc = round($total_retencion_renta / (30 / 100), 2);
                        $retencion->factor_renta_porc = ($retencion->base_renta_porc > 0 ? 30 : 0);

                        if (!$retencion->save()) {
                            $huboError = true;
                            $msg = "La retención de la fila {$rownro} no se pudo validar {$factura->id}: {$retencion->getErrorSummaryAsString()}.";
                            FlashMessageHelpsers::createWarningMessage($msg);
                            $retenciones_novalidas[$rownro] = $msg;
                            continue;
                        }
                        $retencion->refresh();
                        $retencion_iva_total = round($retencion->base_renta_porc * ($retencion->factor_renta_porc / 100), 2);


                        # CALCULO Y VERIFICACION DE RETENCION DE IVA.

                        static $factor = 30;
                        $total_retencion = $row['V'];
                        $total_retencion = str_replace(',', '', $total_retencion);
                        $total_retencion = $total_retencion / $cotizacion; # valorizar: el monto en el excel es en guaranies pero la retencion se guarda en moneda de factura.
                        $base = round($total_retencion / ($factor / 100), 4);

                        #determinar los ivas usados por la factura (excepto exenta)
                        $ivas_factura = [];
                        foreach ($factura->ivasCuentaUsadas as $ivasCuentaUsada) {
                            if (isset($ivasCuentaUsada->ivaCta) && !in_array($ivasCuentaUsada->ivaCta->iva->porcentaje, $ivas_factura))
                                $ivas_factura[$ivasCuentaUsada->ivaCta->iva->porcentaje] = round($ivasCuentaUsada->monto, 2);  // se crea el array asi por si en alguna modificacion futura se llegue a necesitar..., por ahora solo necesitamos saber la cantidad de ivas usados: o 5 o 10 o ambos.
                        }

                        # si hay dos ivas 5 y 10, verificar si la base total calculada a partir de excel (dividiendo entre 0.3)
                        #  es igual a la suma de los impuestos de la factura, con una diferencia maxima tolerada de 10 unidades de moneda.
                        if (sizeof($ivas_factura) == 2) {
                            $sumIvas = 0;
                            $sumIva5 = 0;
                            $sumIva10 = 0;
                            foreach ($ivas_factura as $porcentaje => $monto) {
                                $sumIvas += round($monto / ($porcentaje == 5 ? 21 : 11), 4);
                                if ($porcentaje == 5)
                                    $sumIva5 += round($monto / 21, 4);
                                else
                                    $sumIva10 += round($monto / 11, 4);
                            }
                            $diff = abs(round($sumIvas, 2) - round($base, 2));
                            if ($diff > 10) {
                                $huboError = true;
                                $nro_factura = ($operacion == 'compra' ? $factura->nro_factura : $factura->getNroFacturaCompleto());
                                $nro_factura = Html::a($nro_factura, ["/contabilidad/$operacion/view", 'id' => $factura->id], ['target' => '_blank']);
                                $retenido = $base * 0.3;
                                $msg = "La retención de la fila {$rownro} no puede crearse:&nbsp;
                                La suma de los impuestos de la factura {$nro_factura} es " . number_format(round($sumIvas, 5), 5, ',', '.') . ", la base de la retención calculada a partir del valor de la columna V (Retenido IVA = " . number_format($retenido, 4, ',', '.') . ") dividiendo entre 0.3 es " . number_format(round($base, 4), 4, ',', '.') . " y la diferencia es " . number_format(round($diff, 5), 5, ',', '.');
                                if ($factura->moneda_id != 1)
                                    $msg .= ". La factura está en {$factura->moneda->nombre} con la cotización  de {$cotizacion}";
                                $retenciones_novalidas[$rownro] = $msg;
                                continue;
                            }
                            // crear detalle por retencion de iva 5
                            $base_usar = $sumIva5;
                            $iva_id = Iva::findOne(['porcentaje' => '5'])->id;
                            $newRetencionDetalle = new RetencionDetalleIvas();
                            $newRetencionDetalle->retencion_id = $retencion->id;
                            $newRetencionDetalle->iva_id = $iva_id;
                            $newRetencionDetalle->base = round($base_usar, 2);
                            $newRetencionDetalle->factor = $factor;
                            $retencion_iva_total += round($newRetencionDetalle->base * ($newRetencionDetalle->factor / 100), 2);
                            if (!$newRetencionDetalle->save()) {
                                $huboError = true;
                                $msg = "Error interno creando detalle para retención de la fila {$rownro}: {$newRetencionDetalle->getErrorSummaryAsString()}";
                                $retenciones_novalidas[$rownro] = $msg;
                                continue;
                            }
                            $newRetencionDetalle->refresh();
                            // crear detalle por retencion de iva 10
                            $base_usar = $base - $sumIva5;
                            $iva_id = Iva::findOne(['porcentaje' => '10'])->id;
                            $newRetencionDetalle = new RetencionDetalleIvas();
                            $newRetencionDetalle->retencion_id = $retencion->id;
                            $newRetencionDetalle->iva_id = $iva_id;
                            $newRetencionDetalle->base = round($base_usar, 2);
                            $newRetencionDetalle->factor = $factor;
                            $retencion_iva_total += round($newRetencionDetalle->base * ($newRetencionDetalle->factor / 100), 2);
                            if (!$newRetencionDetalle->save()) {
                                $huboError = true;
                                $msg = "Error interno creando detalle para retención de la fila {$rownro}: {$newRetencionDetalle->getErrorSummaryAsString()}";
                                $retenciones_novalidas[$rownro] = $msg;
                                continue;
                            }
                            $newRetencionDetalle->refresh();
                        } elseif (sizeof($ivas_factura) == 1) {
                            $base = round($total_retencion / ($factor / 100), 2);
                            $iva_id = Iva::findOne(['porcentaje' => key($ivas_factura)])->id;  // key(): devuelve el indide del array al que esta apuntando el mismo array actualmente. Normalmente la variable del array apunta al primer elemento.
                            $newRetencionDetalle = new RetencionDetalleIvas();
                            $newRetencionDetalle->retencion_id = $retencion->id;
                            $newRetencionDetalle->iva_id = $iva_id;
                            $newRetencionDetalle->base = $base;
                            $newRetencionDetalle->factor = 30;
                            $retencion_iva_total += round($newRetencionDetalle->base * ($newRetencionDetalle->factor / 100), 2);
                            if (!$newRetencionDetalle->save()) {
                                $huboError = true;
                                $msg = "Error interno creando detalle para retención de la fila {$rownro}: {$newRetencionDetalle->getErrorSummaryAsString()}";
                                $retenciones_novalidas[$rownro] = $msg;
                                continue;
                            }
                            $newRetencionDetalle->refresh();
                        } else {
                            $huboError = true;
                            $nro_factura = ($operacion == 'compra' ? $factura->nro_factura : $factura->getNroFacturaCompleto());
                            $nro_factura = Html::a($nro_factura, ["/contabilidad/$operacion/view", 'id' => $factura->id], ['target' => '_blank']);
                            $msg = "La retención de la fila {$rownro} no se pudo crear: La factura {$nro_factura} no usa IVA 5% ni IVA 10%, pero la retención de la fila {$rownro} indica un monto de retención de IVA.";
                            $retenciones_novalidas[$rownro] = $msg;
                            continue;
                        }

                        // actualizar total de retenciones
                        $retencion->refresh();
                        $retencion->total = $retencion->getTotalRetencion();
                        if (!$retencion->save()) {
                            $huboError = true;
                            $msg = "La retención de la fila {$rownro} no se pudo validar {$factura->id}: {$retencion->getErrorSummaryAsString()}.";
                            $retenciones_novalidas[$rownro] = $msg;
                            continue;
                        }
                        $retencion->refresh();

                        // actualizar saldo de factura.
                        try {
                            $factura->refresh();
                            $factura->actualizarSaldo();
                        } catch (\Exception $exception) {
                            $huboError = true;
                            $msg = "La retención de la fila {$rownro} no se pudo guardar: {$exception->getMessage()}.";
                            $retenciones_novalidas[$rownro] = $msg;
                            continue;
                        }
                    }
                }

                $html = <<<HTML
<div class="panel panel-danger">
    <div class="panel-heading"><strong>Panel Heading</strong></div>
    <div class="panel-body">Panel Content</div>
    <div class="panel-footer">Panel Footer</div>
</div>
HTML;

                $mensaje = "";
                if ($huboError) {
                    $html_li = '<ul style="list-style-type:circle">';
//                    $retencion_timbrado_noexiste = [];

                    if (sizeof($error_entidad_emp_actual_noexiste) > 0) {
                        foreach ($error_entidad_emp_actual_noexiste as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($error_entidad_emp_actual_noexiste);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por Errores Generales', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }

                    if (sizeof($retencion_fecha_malformat) > 0) {
                        foreach ($retencion_fecha_malformat as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($retencion_fecha_malformat);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por Formato erróneo en Fecha de retención', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }

                    if (sizeof($retencion_timbrado_noexiste) > 0) {
                        foreach ($retencion_timbrado_noexiste as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($retencion_timbrado_noexiste);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por Nros de Retenciones no correspodientes a ningún timbrado.', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }

                    if (sizeof($retenciones_novalidas) > 0) {
                        foreach ($retenciones_novalidas as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($retenciones_novalidas);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por no ser validadas', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }

                    if (sizeof($retenciones_existentes) > 0) {
                        foreach ($retenciones_existentes as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($retenciones_existentes);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por ser duplicadas', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }
//                    foreach ($empresa_no_existe as $retencion_nro => $item) {
//                        $mensaje .= '<strong>Error Retención de la Fila ' . $retencion_nro . ': </strong>' . $item . '<br/>';
//                    }
//                    foreach ($empresa_no_actual as $retencion_nro => $item) {
//                        $mensaje .= '<strong>Error Retención de la Fila ' . $retencion_nro . ': </strong>' . $item . '<br/>';
//                    }


                    if (sizeof($facturas_noexiste) > 0) {
                        foreach ($facturas_noexiste as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($facturas_noexiste);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por Nro de Factura no existentes', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }

                    if (sizeof($factura_nro_formatoerroeno) > 0) {
                        foreach ($factura_nro_formatoerroeno as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($factura_nro_formatoerroeno);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por Error en el formato de Nro de Facturas', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }

                    if (sizeof($factura_timbrado_nocorresponde) > 0) {
                        foreach ($factura_timbrado_nocorresponde as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($factura_timbrado_nocorresponde);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por Facturas y Timbrados no asociados', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }

                    if (sizeof($retencion_periodo_error) > 0) {
                        foreach ($retencion_periodo_error as $rowNro => $msg) {
                            $html_li .= "<li>Fila en el excel: {$rowNro}: {$msg}</li>";
                        }
                        $html_li .= '</ul>';

                        $cantidad = sizeof($retencion_periodo_error);
                        $mensaje .= $html;
                        $mensaje = str_replace('Panel Heading', 'Retenciones omitidas por incoherencia en el año del periodo contable.', $mensaje);
                        $mensaje = str_replace('Panel Content', $html_li, $mensaje);
                        $mensaje = str_replace('Panel Footer', "Cantidad de Retenciones: {$cantidad}", $mensaje);
                    }
                } else {
                    $transaction->commit();
                    FlashMessageHelpsers::createInfoMessage('Se han creado correctamente las retenciones.');
                }
            } catch (\Exception $e) {
                //throw $e;
                FlashMessageHelpsers::createErrorMessage($e->getMessage());
                $transaction->rollBack();
            }
        }

        retorno:;
        return $this->render('create_file', [
            'model' => $model, 'mensaje' => $mensaje
        ]);
    }

    private
    function verificarSession()
    {
        if (Yii::$app->session->has('core_empresa_actual') && Yii::$app->session->has('core_empresa_actual_pc')) return true;
        return false;
    }

    public function actionGetTimbrados($operacion)
    {
        $resultado = [
            'results' => [
                [
                    'id' => '', 'text' => ''
                ]
            ],
            'error' => "",
        ];
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-tipodoc_set_retencion";
        $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);

        if (!isset($parametro_sistema)) {
            $result['error'] = "Falta definir el tipo de documento para retención en el parámetro de la Empresa actual.";
        } else {

            $nro_retencion = $_GET['nro_retencion'];
            $nro_retencion = (int)substr($nro_retencion, 8, 7);
            $factura_id = $_GET['factura_id'];
            $entidad = null;
            $tipoDocuSet = TipoDocumentoSet::findOne(['id' => $parametro_sistema->valor]);

            // Según explicación de José el 25 setiembre a las 9:00 am:
            //  - retencion compra: yo emito retencion.
            //  - retencion venta: cliente emite retencion.

            // Por lo tanto, la entidad del timbrado es igual a la entidad que emite la retencion:
            //  - si es compra, emite la empresa actual (core_empresa_actual).
            //  - si es venta, emite la entidad asociada a la venta (cliente asosiado a la venta).

            if ($operacion == 'compra') {
                $ruc = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->ruc;
                $entidad = Entidad::findOne(['ruc' => $ruc]);
            } else {
                $venta = Venta::findOne(['id' => $factura_id]);
                $entidad = $venta->entidad;
            }

            $t_d = Timbrado::find()->alias('tim')
                ->joinWith('timbradoDetalles as detalles')
                ->select([
                    'tim.id as id',
                    'tim.nro_timbrado as text',
                    'detalles.id',
                    'detalles.nro_inicio',
                    'detalles.nro_fin'
                ])
                ->where(['=', 'tim.entidad_id', $entidad->id])
                ->andWhere(['=', 'detalles.tipo_documento_set_id', $tipoDocuSet->id])
                ->andWhere('detalles.nro_actual <= detalles.nro_fin')
                ->andWhere(['AND', ['<=', 'detalles.nro_inicio', $nro_retencion], ['>=', 'detalles.nro_fin', $nro_retencion]]);

            if ($_GET['action_id'] == 'update')
                $t_d->orWhere(['detalles.id' => Retencion::findOne(['id' => $_GET['retencion_id']])->timbrado_detalle_id]);

            $t_d = $t_d->orderBy('tim.nro_timbrado ASC')
                ->asArray()->all();

            $resultado['results'] = array_values($t_d);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $resultado;
    }

    // TODO:
    public function actionGetFacturas($operacion)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $ids = ($_GET['ids'] == "") ? [] : $_GET['ids']; // arrays vacios en ajax elimina el atributo
        $facturas = [];
        if ($operacion == 'compra') {
            $facturas = Retencion::getComprasParaRetener(
                $_GET['doMap'], $ids, $_GET['condicion'], $_GET['concatRazonSocial'], $_GET['sinRetenciones'], $_GET['saldoGreaterThanZero'], $_GET['entidad_id'], $_GET['fecha_hasta']);
        } else {
            $facturas = Retencion::getVentasParaRetener(
                $_GET['doMap'], $ids, $_GET['condicion'], $_GET['concatRazonSocial'], $_GET['sinRetenciones'], $_GET['saldoGreaterThanZero'], $_GET['entidad_id'], $_GET['fecha_hasta']);
        }

        return $facturas;
    }

    public function actionGetTimbrado($operacion = 'compra')
    {
        $data = ['result' => '', 'error' => ''];
        \Yii::$app->response->format = Response::FORMAT_JSON;

        try {
            if ($operacion == 'compra') {
                $timbrado = Retencion::getTimbradoRetencion();
                $data['result'] = $timbrado->nro_timbrado;
            } elseif ($operacion == 'venta') {
                $factura_id = Yii::$app->request->get('factura_id', null);
                if (!isset($factura_id))
                    $data['result'] = '';
                else {
                    $venta = Venta::findOne(['id' => $factura_id]);
                    $tipoDocSetRetencion = Retencion::getTipodocSetRetencion();
                    $timbrado = Timbrado::find()->alias('tim')
                        ->leftJoin('cont_timbrado_detalle as det', 'det.timbrado_id = tim.id')
                        ->where(['tim.entidad_id' => $venta->entidad_id, 'det.tipo_documento_set_id' => $tipoDocSetRetencion->id]);
                    if ($timbrado->exists()) {
                        $data['result'] = $timbrado->one()->nro_timbrado;
                    }
                }
            }
        } catch (\Exception $exception) {
            $data['result'] = '';
            $data['error'] = $exception->getMessage();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $data;
    }

    public function actionBloquear($id, $operacion)
    {
        $model = Retencion::findOne(['id' => $id]);

        if ($model->bloqueado == 'no') {
            $model->bloqueado = 'si';

            //Solo para validacion
            $model->factura_id = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Retencion ha sido bloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede bloquear retención. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index', 'operacion' => $operacion]);
    }

    public function actionDesbloquear($id, $operacion)
    {
        $model = Retencion::findOne(['id' => $id]);

        if ($model->bloqueado == 'si') {
            $model->bloqueado = 'no';

            //Solo para validacion
            $model->factura_id = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Retencion ha sido desbloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede desbloquear retención. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index', 'operacion' => $operacion]);
    }
}
