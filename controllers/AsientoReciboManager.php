<?php

use backend\models\Empresa;
use backend\models\SessionVariables;
use backend\modules\contabilidad\controllers\TemplateModel;
use backend\modules\contabilidad\models\Asiento;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\Recibo;
use backend\modules\contabilidad\models\ReciboDetalleContracuentas;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\RetencionDetalleIvas;
use common\helpers\FlashMessageHelpsers;

/**
 * @param TemplateModel $model
 * @param string|null $operacion
 * @return array|null
 * @throws Exception
 */
function generarAsientoRecibo($model, $operacion = null)
{
    $session = Yii::$app->session;
    $empresa_id = $session->get(SessionVariables::empresa_actual);
    $periodo_id = $session->get(SessionVariables::empresa_actual . "_pc");
    $empresa = Empresa::findOne(['id' => $empresa_id]);
    $razon_social = ucwords($empresa->razon_social);

    #cuentas de diferencia de cambio
    $cuentaDiffCambioGanancia = ParametroSistema::findOne(['nombre' => "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber"]);
    $cuentaDiffCambioPerdida = ParametroSistema::findOne(['nombre' => "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe"]);
    if (!isset($cuentaDiffCambioGanancia) || $cuentaDiffCambioGanancia->valor == '')
        throw new Exception("Falta parametrizar en la empresa actual la cuenta para GANANCIA POR DIFERENCIA DE CAMBIO");
    if (!isset($cuentaDiffCambioPerdida) || $cuentaDiffCambioPerdida->valor == '')
        throw new Exception("Falta parametrizar en la empresa actual la cuenta para PERDIDA POR DIFERENCIA DE CAMBIO");
    $cuentaDiffCambioGanancia = PlanCuenta::findOne(['id' => $cuentaDiffCambioGanancia->valor]);
    $cuentaDiffCambioPerdida = PlanCuenta::findOne(['id' => $cuentaDiffCambioPerdida->valor]);

    #cuentas para ganancia/perdida por error de rendondeo
    $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_error_redondeo_ganancia";
    $ctaErrRedondeoGanancia = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre]);
    if (!isset($ctaErrRedondeoGanancia)) {
        throw new Exception("Falta parametrizar en la empresa actual la cuenta para Ganancia por error de redondeo.");
    }
    $ctaErrRedondeoGanancia = PlanCuenta::findOne(['id' => $ctaErrRedondeoGanancia->valor]);
    $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_error_redondeo_perdida";
    $ctaErrRedondeoPerdida = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre]);
    if (!isset($ctaErrRedondeoPerdida)) {
        throw new Exception("Falta parametrizar en la empresa actual la cuenta para Pérdida por error de redondeo.");
    }
    $ctaErrRedondeoPerdida = PlanCuenta::findOne(['id' => $ctaErrRedondeoPerdida->valor]);

    #buscar cuenta contable en el parametro de empresa actual
    $tipo_retencion = ($operacion == 'compra') ? "cuenta_retenciones_emitidas" : "cuenta_retenciones_recibidas";
    $parmName = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-{$tipo_retencion}";
    $parmContRetenciones = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $parmName]);
    if (!isset($parmContRetenciones))
        throw new Exception("Es obligatorio tener parametrizado en la empresa actual las cuentas contables para Retenciones de IVA emitidas y recibidas.");

    #buscar cuenta contable en el parametro de empresa actual
    $tipo_retencion = ($operacion == 'compra') ? "cuenta_retenciones_renta_emitidas" : "cuenta_retenciones_renta_recibidas";
    $parmName = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-{$tipo_retencion}";
    $parmContRetencionesRenta = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $parmName]);
    if (!isset($parmContRetencionesRenta))
        throw new Exception("Es obligatorio tener parametrizado en la empresa actual las cuentas contables para Retenciones de RENTA emitidas y recibidas.");

    #procesar datos del form
    $rango = explode(' - ', $model->fecha_rango);
    $desde = implode(array_reverse(explode('-', $rango[0])));
    $hasta = implode(array_reverse(explode('-', $rango[1])));

    // Traer recibos de la empresa/periodo actual, segun fecha especificada en el formulario, sin asentarse.
    $recibos = Recibo::find()->where([
        'empresa_id' => $empresa_id,
        'periodo_contable_id' => $periodo_id,
    ])
        ->andWhere(['BETWEEN', 'fecha', $desde, $hasta])
        ->andWhere(['IS', 'asiento_id', null]);

    // Si no existe recibo, retorna.
    if (!$recibos->exists()) {
        return null;
    }
    $recibos = $recibos->all();

    // Existe recibo, crea asiento: Un solo asiento para todos los recibos obtenidos.
    $asiento = new Asiento();
    $asiento->empresa_id = $empresa_id;
    $asiento->periodo_contable_id = $periodo_id;
    $asiento->fecha = date('Y-m-t');
    $asiento->concepto = "Asiento de Recibos " . ucwords($operacion) . " de {$razon_social}, {$asiento->fecha}";
    $asiento->usuario_id = Yii::$app->user->id;
    $asiento->creado = date('Y-m-d H:i:s');
    $asiento->modulo_origen = 'contabilidad';
    $asiento->monto_debe = $asiento->monto_haber = 1;
    if (!$asiento->save()) {
        throw new Exception("No se pudo validar asiento de recibo: {$asiento->getErrorSummaryAsString()}");
    }
    $asiento->refresh();

    /** @var AsientoDetalle[] $detalles */
    $detalles = [];
    /** @var Recibo $recibo */
    foreach ($recibos as $recibo) {
        $skipped = false;
        $monto_diferencia_cambio = 0;
        foreach ($recibo->reciboDetalles as $reciboDetalle) {
            // Si la factura del reciboDetalle es null, es porque no corresponde a la operacion (compra/venta)
            if (!isset($reciboDetalle->$operacion)) {
                $skipped = true;
                break;
            }
            // `$diferenciaCotizac` es la resta aritmetica entre la cotizacion usada en la factura menos la del recibo (de la cabecera o del detalle recibo)
            list($diferenciaCotizac, $montoValorizado) = $reciboDetalle->getMontoValorizado($operacion);
            $detalle = new AsientoDetalle();
            $detalle->cuenta_id = $reciboDetalle->$operacion->tipoDocumento->cuenta_id;
            $detalle->monto_debe = ($operacion == 'compra' ? $montoValorizado : 0);
            $detalle->monto_haber = ($operacion == 'compra' ? 0 : $montoValorizado);
            $detalle->asiento_id = $asiento->id;
            $detalles[] = $detalle;
            $cuentaAsientoReciboIndex = sizeof($detalles) - 1;

            // Asiento por retenciones
            $monto_retencion_iva = 0;
            $monto_retencion_renta = 0;
            foreach (($retenciones = Retencion::findAll(["factura_{$operacion}_id" => $reciboDetalle->$operacion->id])) as $retencion) {
                $monto_renta = round(round($retencion->base_renta_porc, 2) * (round($retencion->factor_renta_porc, 2) / 100), 2);
                $monto_retencion_renta += $monto_renta;
                /** @var RetencionDetalleIvas $retencionDetalle */
                foreach ($retencion->detalles as $retencionDetalle) {
                    $monto_retencion_iva += round($retencionDetalle->base * $retencionDetalle->factor / 100, 2);
                }
                // Asociar asiento a la retencion.
                $retencion->asiento_id = $asiento->id;
                $retencion->factura_id = $reciboDetalle->$operacion->id; // campo virtual requerido.
                if (!$retencion->save())
                    throw new Exception("Error asociando asiento a la retencion ID: {$retencion->id}: {$retencion->getErrorSummaryAsString()}");
                $retencion->refresh();
            }
            // Crear asiento detalles por el valor de la retenciones de iva acumuladas.
            if ($monto_retencion_iva > 0) {
                if (isset($parmContRetenciones)) {
                    $cotizacionFact = ($operacion == 'compra' ? $reciboDetalle->$operacion->cotizacion : $reciboDetalle->$operacion->valor_moneda);
                    $cotizacionFact = ($cotizacionFact == '' ? 1 : $cotizacionFact);
                    $cuentaRetencion = PlanCuenta::findOne(['id' => $parmContRetenciones->valor]);
                    $monto_retencion_iva *= $cotizacionFact;
                    $monto_retencion_iva = round($monto_retencion_iva);
                    $detRetencion = new AsientoDetalle();
                    $detRetencion->cuenta_id = $cuentaRetencion->id;
                    $detRetencion->monto_debe = ($operacion == 'compra' ? 0 : $monto_retencion_iva);
                    $detRetencion->monto_haber = ($operacion == 'compra' ? $monto_retencion_iva : 0);
                    // Sumar al detalle de asiento con la cuenta del tipo de documento de factura.
                    $detalles[$cuentaAsientoReciboIndex]->monto_debe += ($operacion == 'compra' ? $monto_retencion_iva : 0);
                    $detalles[$cuentaAsientoReciboIndex]->monto_haber += ($operacion == 'venta' ? $monto_retencion_iva : 0);
                    $detalles[] = $detRetencion;
                } else {
                    FlashMessageHelpsers::createInfoMessage("NO se realiza asiento por retenciones debido a que falta en el parámetro de la empresa las cuentas para retenciones de IVA emitidas/recibidas", 12000);
                }
            }
            // Crear asiento detalles por el valor de la retenciones de renta acumuladas.
            if ($monto_retencion_renta > 0) {
                if (isset($parmContRetencionesRenta)) {
                    $cotizacionFact = ($operacion == 'compra' ? $reciboDetalle->$operacion->cotizacion : $reciboDetalle->$operacion->valor_moneda);
                    $cotizacionFact = ($cotizacionFact == '' ? 1 : $cotizacionFact);
                    $cuentaRetencion = PlanCuenta::findOne(['id' => $parmContRetencionesRenta->valor]);
                    $monto_retencion_renta *= $cotizacionFact;
                    $monto_retencion_renta = round($monto_retencion_renta);
                    $detRetencion = new AsientoDetalle();
                    $detRetencion->cuenta_id = $cuentaRetencion->id;
                    $detRetencion->monto_debe = ($operacion == 'compra' ? 0 : $monto_retencion_renta);
                    $detRetencion->monto_haber = ($operacion == 'compra' ? $monto_retencion_renta : 0);
                    // Sumar al detalle de asiento con la cuenta del tipo de documento de factura.
                    $detalles[$cuentaAsientoReciboIndex]->monto_debe += ($operacion == 'compra' ? $monto_retencion_renta : 0);
                    $detalles[$cuentaAsientoReciboIndex]->monto_haber += ($operacion == 'venta' ? $monto_retencion_renta : 0);
                    $detalles[] = $detRetencion;
                } else {
                    FlashMessageHelpsers::createInfoMessage("NO se realiza asiento por retenciones debido a que falta en el parámetro de la empresa las cuentas para retenciones de RENTA emitidas/recibidas", 12000);
                }
            }

            // Asiento por diferencia de cambio
            if (isset($diferenciaCotizac) && $diferenciaCotizac != 0) {
                $detDiffCotizac = new AsientoDetalle();
                $monto_diferencia_cambio = round($reciboDetalle->monto * abs($diferenciaCotizac));
                if ($operacion == 'compra' && $diferenciaCotizac > 0 || $operacion == 'venta' && $diferenciaCotizac < 0) {
                    $detDiffCotizac->monto_debe = 0;
                    $detDiffCotizac->monto_haber = $monto_diferencia_cambio;
                    $detDiffCotizac->cuenta_id = $cuentaDiffCambioGanancia->id;
                } elseif ($operacion == 'compra' && $diferenciaCotizac < 0 || $operacion == 'venta' && $diferenciaCotizac > 0) {
                    $detDiffCotizac->monto_debe = $monto_diferencia_cambio;
                    $detDiffCotizac->monto_haber = 0;
                    $detDiffCotizac->cuenta_id = $cuentaDiffCambioPerdida->id;
                }
                $detDiffCotizac->asiento_id = $asiento->id;
                $detalles[] = $detDiffCotizac;
            }
        }

        // Detalle asiento por contracuentas del recibo
        if (!$skipped) {
            /** @var ReciboDetalleContracuentas $reciboDetalle */
            foreach ($recibo->reciboDetalleContracuentas as $reciboDetalle) {
                $detalle = new AsientoDetalle();
                $detalle->cuenta_id = $reciboDetalle->plan_cuenta_id;
                $detalle->monto_debe = ($operacion == 'compra' ? 0 : $reciboDetalle->getMontoValorizado());
                $detalle->monto_haber = ($operacion == 'compra' ? $reciboDetalle->getMontoValorizado() : 0);
                $detalle->asiento_id = $asiento->id;
                $detalles[] = $detalle;
            }
            $recibo->asiento_id = $asiento->id;
            if (!$recibo->save())
                throw new Exception("No se pudo asociar asiento al recibo {$recibo->id}: {$recibo->getErrorSummaryAsString()}");
            $recibo->refresh();
        }
    }

    if (empty($detalles)) {
        return null;
    }

    $detalles = AsientoDetalle::agruparDetallesIguales($detalles);
    $detalles = AsientoDetalle::agruparDebeHaberAsientos($detalles);
    $debe = $haber = 0;
    foreach ($detalles as $detalle) {
        $detalle->asiento_id = $asiento->id;
        if (!$detalle->save())
            throw new Exception("No se pudo validar la cuenta del asiento de recibo: {$detalle->getErrorSummaryAsString()}");
        $detalle->refresh();
        $debe += round($detalle->monto_debe, 2);
        $haber += round($detalle->monto_haber, 2);
    }

    $diff_error_red = round($debe - $haber, 2);
    $newDetErrorRedondeo = new AsientoDetalle();
    $newDetErrorRedondeo->asiento_id = $asiento->id;
    if ($diff_error_red > 0 && $operacion == 'compra' || $diff_error_red < 0 && $operacion == 'venta') {
        $newDetErrorRedondeo->cuenta_id = $ctaErrRedondeoGanancia->id;
        $newDetErrorRedondeo->monto_haber = abs($diff_error_red);
        $newDetErrorRedondeo->monto_debe = 0;
        if (!$newDetErrorRedondeo->save())
            throw new Exception("Error guardando asiento por Ganancia por error de redondeo: {$newDetErrorRedondeo->getErrorSummaryAsString()}");
        $newDetErrorRedondeo->refresh();
        $detalles[] = $newDetErrorRedondeo;
        $detalles = AsientoDetalle::agruparDetallesIguales($detalles);
        $detalles = AsientoDetalle::agruparDebeHaberAsientos($detalles);
    } elseif ($diff_error_red < 0 && $operacion == 'compra' || $diff_error_red > 0 && $operacion == 'venta') {
        $newDetErrorRedondeo->cuenta_id = $ctaErrRedondeoPerdida->id;
        $newDetErrorRedondeo->monto_debe = abs($diff_error_red);
        $newDetErrorRedondeo->monto_haber = 0;
        if (!$newDetErrorRedondeo->save())
            throw new Exception("Error guardando asiento por Ganancia por error de redondeo: {$newDetErrorRedondeo->getErrorSummaryAsString()}");
        $newDetErrorRedondeo->refresh();
        $detalles[] = $newDetErrorRedondeo;
        $detalles = AsientoDetalle::agruparDetallesIguales($detalles);
        $detalles = AsientoDetalle::agruparDebeHaberAsientos($detalles);
    }

    $asiento->refresh();
    $asiento->setDebeHaberFromDetalles();
    if (!$asiento->save())
        throw new Exception("Error validando asiento recibo: {$asiento->getErrorSummaryAsString()}");
    $asiento->refresh();

    return $detalles;
}