<?php

namespace backend\modules\contabilidad\controllers;

use backend\modules\contabilidad\models\ActivoFijoTipo;
use backend\modules\contabilidad\models\ActivoFijoTipoAtributo;
use backend\modules\contabilidad\models\search\ActivoFijoTipoSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ActivoFijoTipoController implements the CRUD actions for ActivoFijoTipo model.
 */
class ActivoFijoTipoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    private function clearSession()
    {
        $sesion = Yii::$app->session;

        $sesion->remove('cont_afijotipo_atributos');
    }

    /**
     * Lists all ActivoFijoTipo models.
     * @return mixed
     */
    public function actionIndex()
    {
        self::clearSession();

        $searchModel = new ActivoFijoTipoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ActivoFijoTipo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ActivoFijoTipo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ActivoFijoTipo();
        $model->loadDefaultValues();
        $sesion = Yii::$app->session;

        if (Yii::$app->request->isGet) {
            self::clearSession();
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();

            try {
                $atributos = [];
                if (array_key_exists('ActivoFijoTipoAtributo', $_POST)) {
                    $postData = $_POST['ActivoFijoTipoAtributo'];
                    foreach ($postData as $atributo) {
                        Yii::warning('entra');
                        $modelAtributo = new ActivoFijoTipoAtributo();
                        $modelAtributo->loadEmpresaPeriodo();
                        $load['ActivoFijoTipoAtributo'] = $atributo;
                        $modelAtributo->load($load);
                        $atributos[] = $modelAtributo;
                    }
                }
                $sesion->set('cont_afijotipo_atributos', $atributos);

                if (!$model->save()) {
                    throw new \Exception("Error validando formulario: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                $processed = [];
                /** @var ActivoFijoTipoAtributo[] $atributos */
                foreach ($atributos as $index => $atributo) {
                    $atributos[$index]->activo_fijo_tipo_id = $model->id;
                    if (!in_array($atributos[$index]->atributo, $processed)) {
                        if (!$atributos[$index]->validate()) {
                            throw new \Exception("Error validando el atributo `{$atributos[$index]->atributo}`:
                         {$atributos[$index]->getErrorSummaryAsString()}");
                        }
                        $atributos[$index]->save(false);
                        $processed[] = $atributos[$index]->atributo;
                    } else {
                        $atributos[$index]->addError('atributo', "Este atributo está duplicado.");
                        throw new \Exception("Error procesando atributo `{$atributos[$index]->atributo}`:
                         {$atributos[$index]->getErrorSummaryAsString()}");
                    }
                }

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("`$model->nombre` creado correctamente.");
                return $this->redirect(['index']);
            } catch (\Exception $exception) {
                $model->id = "";
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ActivoFijoTipo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $sesion = Yii::$app->session;

        if (Yii::$app->request->isGet) {
            self::clearSession();
            $sesion->set('cont_afijotipo_atributos',
                ActivoFijoTipoAtributo::find()->where(['activo_fijo_tipo_id' => $id])->orderBy('atributo ASC')->all());
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();

            try {
                $atributos = [];
                if (array_key_exists('ActivoFijoTipoAtributo', $_POST)) {
                    $postData = $_POST['ActivoFijoTipoAtributo'];
                    foreach ($postData as $atributo) {
                        $modelAtributo = null;
                        $query = ActivoFijoTipoAtributo::find()->where([
                            'atributo' => $atributo['atributo'],
                            'activo_fijo_tipo_id' => $id,
                        ]);
                        if (!$query->exists()) {
                            $modelAtributo = new ActivoFijoTipoAtributo();
                            $modelAtributo->loadEmpresaPeriodo();
                        } else {
                            $modelAtributo = $query->one();
                        }
                        $load['ActivoFijoTipoAtributo'] = $atributo;
                        $modelAtributo->load($load);
                        $atributos[] = $modelAtributo;
                    }
                }
                $sesion->set('cont_afijotipo_atributos', $atributos);

                if (!$model->save()) {
                    throw new \Exception("Error validando formulario: {$model->getErrorSummaryAsString()}");
                }

                $processed = [];
                /** @var ActivoFijoTipoAtributo[] $atributos */
                foreach ($atributos as $index => $atributo) {
                    $atributos[$index]->activo_fijo_tipo_id = $id;

                    if (in_array($atributos[$index]->atributo, $processed)) {
                        $atributos[$index]->addError('atributo', "Este atributo está duplicado.");
                        throw new \Exception("Error validando atributo `{$atributos[$index]->atributo}`:
                         {$atributos[$index]->getErrorSummaryAsString()}");
                    }

                    if ($atributos[$index]->obligatorio == '') {
                        $atributos[$index]->addError('obligatorio', "Es Obligatorio? no puede estar vacio.");
                        throw new \Exception("Error validando atributo `{$atributo->atributo}`:
                         {$atributos[$index]->getErrorSummaryAsString()}");
                    }

                    if (!$atributos[$index]->validate()) {
                        throw new \Exception("Error validando el atributo `{$atributos[$index]->atributo}`:
                         {$atributos[$index]->getErrorSummaryAsString()}");
                    }
                    $atributos[$index]->save(false);
                    $processed[] = $atributos[$index]->atributo;
                }

                foreach ($model->activoFijos as $activoFijo) {
                    $activoFijo->cuenta_id = $model->cuenta_id;
                    $activoFijo->cuenta_depreciacion_id = $model->cuenta_depreciacion_id;
                    if (!$activoFijo->save(false))
                        throw new \Exception("No se pudo actualizar las cuentas de revaluo y depreciacion del activo fijo {$activoFijo->nombre}");
                }

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("`$model->nombre` modificado correctamente.");
                return $this->redirect(['index']);
            } catch (\Exception $exception) {
                $model->id = "";
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ActivoFijoTipo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = self::findModel($id);
        try {

            if ($model->getActivosFijos()->exists())
                throw new \Exception("Error eliminando Tipo de activo fijo: Existen uno o más activos fijos asociados.");
            foreach ($model->atributos as $atributo) {
                if (!$atributo->delete()) {
                    throw new \Exception("Error eliminando atributos del Tipo de activo fijo: {$atributo->getErrorSummaryAsString()}");
                }
            }

            $model->delete();
            FlashMessageHelpsers::createSuccessMessage('Tipo de Activo Fijo eliminado con éxito.');

        } catch (\Exception $e) {
//            throw $e;
            FlashMessageHelpsers::createErrorMessage($e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ActivoFijoTipo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActivoFijoTipo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ActivoFijoTipo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
