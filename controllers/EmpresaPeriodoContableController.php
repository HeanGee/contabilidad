<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\Asiento;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\search\EmpresaPeriodoContableSearch;
use backend\modules\contabilidad\models\Venta;
use common\helpers\FlashMessageHelpsers;
use kartik\form\ActiveForm;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmpresaPeriodoContableController implements the CRUD actions for EmpresaPeriodoContable model.
 */
class EmpresaPeriodoContableController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmpresaPeriodoContable models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Ver TODOS los periodos contables de TODAS las empresas.
        $searchModel = new EmpresaPeriodoContableSearch();
//        $core_empresa_actual = \Yii::$app->session->has('core_empresa_actual') ? \Yii::$app->session->get('core_empresa_actual') : null;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);//, $core_empresa_actual);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmpresaPeriodoContable model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmpresaPeriodoContable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->session->has('core_empresa_actual') || strlen(\Yii::$app->session->get('core_empresa_actual')) == 0) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar una empresa.');
            return $this->redirect(['index',]);
        }

        $model = new EmpresaPeriodoContable();
        $model->empresa_id = \Yii::$app->session->get('core_empresa_actual');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                FlashMessageHelpsers::createSuccessMessage('Periodo contable modificado correctamente.');
                return $this->redirect(['index',]);
            } else {
                FlashMessageHelpsers::createWarningMessage('Algunos datos están incorrectos. VERIFIQUE.');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing EmpresaPeriodoContable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            FlashMessageHelpsers::createSuccessMessage('Periodo contable modificado correctamente.');
            return $this->redirect(['index',]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing EmpresaPeriodoContable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete())
            FlashMessageHelpsers::createSuccessMessage('Periodo contable eliminado correctamente.');

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmpresaPeriodoContable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmpresaPeriodoContable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmpresaPeriodoContable::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false; // TODO: modificar mas tarde
    }

    /**
     * @param array $post
     * @param $empresa_id
     * @return array|bool
     */
    public static function gestionarPeriodo($post = [], $empresa_id)
    {

        if (array_key_exists('EmpresaPeriodoContable', $post) && $post['EmpresaPeriodoContable']['anho']) {
            $query = EmpresaPeriodoContable::find()->where(['empresa_id' => $empresa_id, 'anho' => $post['EmpresaPeriodoContable']['anho']]);

            try {
                $trans = Yii::$app->db->beginTransaction();

                if (!$query->exists()) {
                    $model = new EmpresaPeriodoContable();
                    $model->load($post);
                    $model->empresa_id = $empresa_id;
                    $model->activo = $model->activo == 1 ? 'Si' : 'No';

                    if ($model->validate()) {
                        $estado = $model->save();
                        if ($estado) {
                            $trans->commit();
                        } else {
                            $trans->rollBack();
                        }
                        return $estado;
                    } else {
                        $trans->rollBack();
                        return $model->getErrors();
                    }
                } else {
                    $model = $query->one();
                    $model->anho = $post['EmpresaPeriodoContable']['anho'];
                    $model->activo = $post['EmpresaPeriodoContable']['activo'] == 1 ? 'Si' : 'No';
                    $model->descripcion = $post['EmpresaPeriodoContable']['descripcion'] ? $post['EmpresaPeriodoContable']['descripcion'] : $model->descripcion;
                    if ($model->validate()) {
                        $estado = $model->save();
                        if ($estado) {
                            $trans->commit();
                        } else {
                            $trans->rollBack();
                        }
                        return $estado;
                    } else {
                        $trans->rollBack();
                        return $model->getErrors();
                    }
                }
            } catch (\Exception $e) {
            }
        } else {
            return true; // no se especifica ningún periodo contable.
        }

        return false;
    }

    public function setOperaciones()
    {
        $operaciones = [];
        array_push($operaciones, 'contabilidad-empresa-periodo-contable-view');
        array_push($operaciones, 'contabilidad-empresa-periodo-contable-update');
        array_push($operaciones, 'contabilidad-empresa-periodo-contable-delete');

        return $operaciones;
    }

    // TODO: Eliminar en una prox ocasion, si es que el nuevo metodo funciona bien.
//    public static function setPeriodoContableEnSession($post)
//    {
//        $attrib = 'id';
//        $key_name = 'EmpresaPeriodoContable';
//
//        if (array_key_exists($key_name, $post) && array_key_exists($attrib, $post[$key_name]))
//            Yii::$app->getSession()->set('core_empresa_actual_pc', $post[$key_name][$attrib]);
//    }

    public function actionGetPcLista($id = null, $q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return EmpresaPeriodoContable::getPcPorEmpresaLista($id, $q);
    }

    public function actionAddPeriodoContable($empresa_id, $submit = false)
    {
        $model = new EmpresaPeriodoContable();
        $model->activo = 'Si';
        $model->empresa_id = $empresa_id;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->empresa_id = $empresa_id;  # por seguridad.
                if (!$model->save()) {
                    throw new \Exception("Error guardando periodo contable: {$model->getErrorSummaryAsString()}");
                }

                // Parametros automaticos.
                // Abrir archivo base de parametros
                $my_file = Yii::$app->basePath . '/views/empresa/' . 'parametro_contab.sql';
                $handle = fopen($my_file, 'r');
                $counter = 0;
                while (!feof($handle)) {
                    $counter++;
                    $file_line = trim(fgets($handle));
                    $slices = explode(',', $file_line);
                    $nombre = str_replace('\'', '', $slices[0]);
                    $valor = str_replace('\'', '', $slices[1]);
                    $nuevo_nombre = "";

                    if ($counter < 10) {
                        $query = ParametroSistema::find()->where(['nombre' => $nombre]);
                        if (!$query->exists()) {
                            $cont_parm = new ParametroSistema();
                            $cont_parm->nombre = $nombre;
                            $cont_parm->valor = $valor;
                            $cont_parm->save(false);
                        }
                    } else {
                        $pattern = '/(core_empresa)-([0-9]+)-(periodo)-([0-9]+)-([^"]+)/';
                        $result = preg_match($pattern, $nombre, $matches);
                        if ($result && isset($matches)) {
                            $matches[2] = $empresa_id;
                            $matches[4] = $model->id;
                            unset($matches[0]);
                            $nuevo_nombre = implode('-', $matches);
                        } else {
                            $pattern = '/(core_empresa)-([0-9]+)-([^"]+)/';
                            $result = preg_match($pattern, $nombre, $matches);
                            if ($result && isset($matches)) {
                                $matches[2] = $empresa_id;
                                unset($matches[0]);
                                $nuevo_nombre = implode('-', $matches);
                            }
                        }
                        if ($nuevo_nombre != '') {
                            $query = ParametroSistema::find()->where(['nombre' => $nuevo_nombre]);
                            if (!$query->exists()) {
                                $cont_parm = new ParametroSistema();
                                $cont_parm->nombre = $nuevo_nombre;
                                $cont_parm->valor = $valor;
                                $cont_parm->save(false);
                            }
                        }
                    }
                }

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("El periodo contable {$model->anho} se ha creado correctamente.");
            } catch (\Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createErrorMessage($exception->getMessage());
            }
        };

        return $this->renderAjax('detalle/create-detalle-periodo', [
            'model' => $model,
            'disable_periodo_anterior' => false,
        ]);
    }

    public function actionUpdatePeriodoEmpresa($periodo_id, $submit = false)
    {
        $model = EmpresaPeriodoContable::findOne($periodo_id);
        $periodo_anterior_id = $model->periodo_anterior_id;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->save()) {
                FlashMessageHelpsers::createSuccessMessage('Periodo modificado correctamente');
                return true;
            } else {
                FlashMessageHelpsers::createErrorMessage('Error en la modificación del Periodo');
                return false;
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('detalle/create-detalle-periodo', [
            'model' => $model,
            'disable_periodo_anterior' => self::disablePeriodoAnterior($model)
        ]);
    }

    /**
     * @param EmpresaPeriodoContable $model
     * @return bool
     */
    private function disablePeriodoAnterior($model)
    {
        return Asiento::find()->where(['periodo_contable_id' => $model->periodo_anterior_id])->exists()
            || Compra::find()->where(['periodo_contable_id' => $model->periodo_anterior_id])->exists()
            || Venta::find()->where(['periodo_contable_id' => $model->periodo_anterior_id])->exists();
    }

    public function actionDeletePeriodoEmpresa($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = EmpresaPeriodoContable::findOne($id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($model) {
                $empresa_id = $model->empresa_id;
                $periodo_id = $id;

                // Borrar parametros
                $nombreLike = "core_empresa-{$empresa_id}-periodo-{$periodo_id}";
                foreach (ParametroSistema::find()->where(['like', 'nombre', $nombreLike])->all() as $parametro) {
                    $parametro->delete();
                }

                $model->delete();
                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage('Se ha eliminado correctamente el periodo');
                return true;
            }
        } catch (\Exception $exception) {
            $transaction->rollBack();
            FlashMessageHelpsers::createErrorMessage($exception->getMessage());
        }
        return false;
    }
}
