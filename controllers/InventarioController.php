<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\FlashMessageHelpsers;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class InventarioController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'reporte' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $this->verificarSesion();

        return $this->render('index', [
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    private function verificarSesion()
    {
        if (!\Yii::$app->session->has('core_empresa_actual') || \Yii::$app->session->get('core_empresa_actual') == "") {
            $msg = 'Falta especificar Empresa Actual.';
            FlashMessageHelpsers::createWarningMessage($msg);
            throw new NotFoundHttpException($msg);
        }
        if (!\Yii::$app->session->has('core_empresa_actual_pc') || \Yii::$app->session->get('core_empresa_actual_pc') == "") {
            $msg = 'Falta especificar Periodo Contable Actual para la Empresa Actual.';
            FlashMessageHelpsers::createWarningMessage($msg);
            throw new NotFoundHttpException($msg);
        }
        return true;
    }


    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    private function getActivosFijos($cuenta_id, $empresa_id)
    {
        return ActivoFijo::find()->where(['cuenta_id' => $cuenta_id, 'empresa_id' => $empresa_id])->asArray()->all();
    }

    private function getSumaActivosFijoHijo($cuenta_id, $empresa_id){
        $query = ActivoFijo::find();            
        $query->where(['cuenta_id' => $cuenta_id, 'empresa_id' => $empresa_id]);
        return $query->sum('valor_fiscal_neto');
    }

    private function getSumaDeLasCuentasHijo($cuenta_id, $empresa_id)
    {
        Yii::info($cuenta_id,'#### id de cuenta');
        $suma = $this->getSumaActivosFijoHijo($cuenta_id, $empresa_id);
        $cuenta = PlanCuenta::findOne($cuenta_id);
        $cuentas_hijo = $cuenta->getPlanCuentas()->all();
        Yii::info(count($cuentas_hijo),'#### cuentas count');
        if(count($cuentas_hijo) > 0 ) {
            foreach( $cuentas_hijo as $cuenta_hijo ){
                Yii::info($cuenta_hijo->nombre,'#### hijo');
                $suma = $suma + $this->getSumaDeLasCuentasHijo($cuenta_hijo->id, $empresa_id);
            } 
        }
        return $suma;
        
        
    }

    /** -------------------------------------------
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws NotFoundHttpException
     * @throws \Exception
     */

    public function actionReporte()
    {
        $data = PlanCuenta::getCuentaLista();

        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $empresa = Empresa::findOne($empresa_id);
        $empresaNombre = 'Sin empresa seleccionada';
        $empresa_id = null;
        if ($empresa) {
            $empresaNombre = $empresa->nombre;
            $empresa_id = $empresa->id;
        }
        if (sizeof($data) == 0) {
            FlashMessageHelpsers::createWarningMessage('Cargue plan de cuentas.');
            return $this->redirect(['index']);
        }

        // Crear objetoExcel.
        /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        date_default_timezone_set('America/Asuncion');
        
        /** Create a new Spreadsheet Object **/
        $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/inventario/templates/';
        $objPHPExcel = IOFactory::load($carpeta . 'inventario_template.xlsx');

        
        //Rellenar Excel.
        $fila_titulo = 2;
        $fila = 3;
        $columna = 2;
        $columnaValor = 3;

        $max_fila = 0;
        $max_colum = 0;
        
        $currentDate = date('d-m-Y');
        $titulo = 'INVENTARIO AL '.$currentDate;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo, $titulo);    
        
        foreach ($data as $currentCuenta) {
            $currentCuentaNombre = $currentCuenta['nombre'];
            $currentCuentaId = $currentCuenta['id'];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, ++$fila, $currentCuentaNombre);
            if($currentCuenta['asentable'] == 'no'){
                $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
                ->getFont()->setBold(true);
                $suma_cuentas_hijo = $this->getSumaDeLasCuentasHijo($currentCuentaId, $empresa_id);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 3, $fila, $suma_cuentas_hijo);
            }
            $activos_fijos = $this->getActivosFijos($currentCuentaId, $empresa_id);
            $suma = 0;
            foreach ($activos_fijos as $currentActivoFijo) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, ++$fila, $currentActivoFijo['nombre']);    
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnaValor, $fila, $currentActivoFijo['valor_fiscal_neto']);
                $suma = $suma + $currentActivoFijo['valor_fiscal_neto'];
            }
            if($suma > 0){
                $currentActivoFijoNombre = $currentActivoFijo['nombre'];
                $titulo = 'SUMAS TOTALES '.$currentCuentaNombre;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, ++$fila, $titulo);    
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnaValor + 1, $fila, $suma);
                $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('b9c0cc');
                $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna + 1, $fila)->getCoordinate())
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('b9c0cc');
                $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna + 2, $fila)->getCoordinate())
                ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('b9c0cc');
            }
          
            
        }

        //Versión excel
        $carpeta = Yii::$app->basePath . '/web/uploads/';
        if (!is_dir($carpeta)) {
            if (!@mkdir($carpeta, 0777, true)) {
                throw new \Exception('No se puede crear carpeta.');
            }
        }
        Yii::$app->session->set('carpeta', $carpeta);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Exportar en navegador
        $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
       
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . 'Inventario_al_' .$currentDate .'_'.$empresaNombre.'.' . $fi['extension']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx"));
            ob_clean();
            flush();
            readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
    

        //borramos el fichero temporal
        unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Al index pero con el modelo
        $this->verificarSesion();


        return $this->redirect(['index']);

    }

   
    private function setNumberFormat(&$objPHPExcel, $columna, $fila)
    {
        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
            ->getNumberFormat()
            ->setFormatCode('#,##0');
    }

}