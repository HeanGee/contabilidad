<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\helpers\ExcelHelpers;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\Importacion;
use backend\modules\contabilidad\models\ImportacionDetalleFacturaLocalExterior;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\search\ImportacionSearch;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Font;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ImportacionController extends BaseController
{
    public function actionCreate($_pjax = null)
    {
        $model = new Importacion();
        $model->cal_retencion_fact_local_flete_seguro = 0;
        $model->estado = 'vigente';
        $model->empresa_id = Yii::$app->session->get(SessionVariables::empresa_actual);
        $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
        $tabReporte = 0;

        if ($_pjax == null && !Yii::$app->request->isPost) {

        }

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            $model->cal_retencion_fact_local_flete_seguro = $model->cal_retencion_fact_local_flete_seguro == 0 ? 'no' : 'si';
            if ($model->guardarycerrar == 'no')
                $tabReporte = 1;

            try {
                if (array_key_exists('exterior', $_POST)) {
                    foreach ($_POST['exterior'] as $new_keys => $new) {
                        if ($new['plantilla_factura_id'] == "") {
                            unset($_POST['exterior'][$new_keys]); // eliminar items que tienen plantilla_id vacio.
                            Yii::$app->request->setBodyParams($_POST);
                        }
                    }
                } else
                    throw new \Exception('Debe agregar facturas del exterior.');

                if (array_key_exists('local', $_POST)) {
                    foreach ($_POST['local'] as $new_keys => $new) {
                        if ($new['plantilla_factura_id'] == "") {
                            unset($_POST['local'][$new_keys]); // eliminar items que tienen plantilla_id vacio.
                            Yii::$app->request->setBodyParams($_POST);
                        }
                    }
                } else
                    throw new \Exception('Debe agregar facturas locales.');

                $local_tabla = $_POST['local'];
                $exterior_tabla = $_POST['exterior'];
                $model->fecha = implode('-', array_reverse(explode('-', $model->fecha)));

                foreach ($exterior_tabla as $index => $detalleExterior) {
                    $importacionDetalleExterior = new ImportacionDetalleFacturaLocalExterior();
                    $importacionDetalleExterior->es_local = 'no';
                    $importacionDetalleExterior->factura_compra_id = explode('_', $detalleExterior['plantilla_factura_id'])[3];
                    $importacionDetalleExterior->plantilla_id = explode('_', $detalleExterior['plantilla_factura_id'])[1];
                    $importacionDetalleExterior->cotizacion = isset($detalleExterior['cotizacion']) ? $detalleExterior['cotizacion'] : null;
                    $importacionDetalleExterior->plantilla_factura_id = $detalleExterior['plantilla_factura_id'];
                    $importacionDetalleExterior->cotizacion = floatval(str_replace([".", ","], ["", "."], $importacionDetalleExterior->cotizacion));
                    $model->arrayExterior[] = $importacionDetalleExterior;
                }

                foreach ($local_tabla as $index => $detalleLocal) {
                    $importacionDetalleLocal = new ImportacionDetalleFacturaLocalExterior();
                    $importacionDetalleLocal->es_local = 'si';
                    $importacionDetalleLocal->factura_compra_id = explode('_', $detalleLocal['plantilla_factura_id'])[3];
                    $importacionDetalleLocal->plantilla_id = explode('_', $detalleLocal['plantilla_factura_id'])[1];
                    $importacionDetalleLocal->cotizacion = isset($detalleLocal['cotizacion']) ? $detalleLocal['cotizacion'] : null;
                    $importacionDetalleLocal->plantilla_factura_id = $detalleLocal['plantilla_factura_id'];
                    $importacionDetalleLocal->cotizacion = floatval(str_replace([".", ","], ["", "."], $importacionDetalleLocal->cotizacion));
                    $importacionDetalleLocal->absorber_impuesto = $detalleLocal['absorber_impuesto'];
                    $importacionDetalleLocal->incluir_costo = $detalleLocal['incluir_costo'];
                    $model->arrayLocal[] = $importacionDetalleLocal;
                }

                if (!$model->validate())
                    throw new \Exception('Error al validar la importación. ' . $model->getErrorSummary(true)[0]);

                //Generamos ID
                $model->save();

                /** @var ImportacionDetalleFacturaLocalExterior $importacionDetalleLocal */
                foreach ($model->arrayLocal as $importacionDetalleLocal) {
                    $importacionDetalleLocal->importacion_id = $model->id;
                    if (!$importacionDetalleLocal->validate())
                        throw new \Exception('Error al validar las facturas locales. ' . $importacionDetalleLocal->getErrorSummary(true)[0]);

                    $importacionDetalleLocal->save(false);

                    $facturaCompra = $importacionDetalleLocal->facturaCompra;
                    $facturaCompra->importacion_id = $model->id;
                    $facturaCompra->ruc = '4545';//Solo para que pase la validación
                    $facturaCompra->nombre_entidad = 'asfsdf';//Solo para que pase la validación
                    $facturaCompra->timbrado_id = 1;//Solo para que pase la validación
                    $facturaCompra->save();
                }

                /** @var ImportacionDetalleFacturaLocalExterior $importacionDetalleExterior */
                foreach ($model->arrayExterior as $importacionDetalleExterior) {
                    $importacionDetalleExterior->importacion_id = $model->id;
                    if (!$importacionDetalleExterior->validate())
                        throw new \Exception('Error al validar las facturas del exterior.');

                    $importacionDetalleExterior->save(false);

                    $facturaCompra = $importacionDetalleExterior->facturaCompra;
                    $facturaCompra->importacion_id = $model->id;
                    $facturaCompra->ruc = '4545';//Solo para que pase la validación
                    $facturaCompra->nombre_entidad = 'asdasf';//Solo para que pase la validación
                    $facturaCompra->timbrado_id = 1;//Solo para que pase la validación
                    $facturaCompra->save();
                }

                FlashMessageHelpsers::createSuccessMessage('Guardado exitósamente.');
                $trans->commit();
                if ($model->guardarycerrar == 'si')
                    return $this->redirect(['index']);
                else
                    return $this->redirect(['update', 'id' => $model->id]);

            } catch (\Exception $e) {
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($e->getMessage());
                $model->fecha = implode('-', array_reverse(explode('-', $model->fecha)));
                $model->cal_retencion_fact_local_flete_seguro = $model->cal_retencion_fact_local_flete_seguro == 'si' ? 1 : 0;
            }
        }

        return $this->render('create',
            [
                'model' => $model,
                'tabReporte' => $tabReporte
            ]
        );
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            $facturaCompra = $model->facturaCompra;
            if ($facturaCompra != null) {
                $facturaCompra->importacion_id = null;
                $facturaCompra->ruc = '4545';//Solo para que pase la validación
                $facturaCompra->nombre_entidad = 'asfsdf';//Solo para que pase la validación
                $facturaCompra->timbrado_id = 1;//Solo para que pase la validación
                $facturaCompra->save();
            }

            foreach ($model->importacionDetallesLocal as $local) {
                $local->delete();
            }

            foreach ($model->importacionDetallesExterior as $exterior) {
                $exterior->delete();
            }
            $model->delete();
        } catch (\Exception $e) {
            FlashMessageHelpsers::createErrorMessage($e->getMessage());
        }

        return $this->redirect(['index']);
    }

    public function actionIndex()
    {
        $searchModel = new ImportacionSearch();
        $searchModel->empresa_id = Yii::$app->session->get(SessionVariables::empresa_actual);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 20];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @param null $_pjax
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id, $_pjax = null)
    {
        $model = $this->findModel($id);
        $model->fecha = implode('-', array_reverse(explode('-', $model->fecha)));
        $model->cal_retencion_fact_local_flete_seguro = $model->cal_retencion_fact_local_flete_seguro == 'si' ? 1 : 0;
        $tabReporte = 0;

        if ($_pjax == null && !Yii::$app->request->isPost) {
            foreach ($model->importacionDetallesExterior as $index => $importacionDetalleExterior) {
                $importacionDetalleExterior->plantilla_factura_id = 'plantilla_' . $importacionDetalleExterior->plantilla_id . '_compra_' . $importacionDetalleExterior->factura_compra_id;
                $model->arrayExterior[] = $importacionDetalleExterior;
            }

            foreach ($model->importacionDetallesLocal as $index => $importacionDetalleLocal) {
                $importacionDetalleLocal->plantilla_factura_id = 'plantilla_' . $importacionDetalleLocal->plantilla_id . '_compra_' . $importacionDetalleLocal->factura_compra_id;
                $importacionDetalleLocal->ivas = $this->getImpuestos($importacionDetalleLocal->factura_compra_id, $importacionDetalleLocal->plantilla_id, 'iva_');
                $model->arrayLocal[] = $importacionDetalleLocal;
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->guardarycerrar == 'no')
                $tabReporte = 1;

            try {
                if (array_key_exists('exterior', $_POST))
                    foreach ($_POST['exterior'] as $new_keys => $new) {
                        if ($new['plantilla_factura_id'] == "") {
                            unset($_POST['exterior'][$new_keys]); // eliminar items que tienen plantilla_id vacio.
                            Yii::$app->request->setBodyParams($_POST);
                        }
                    }
                else
                    throw new \Exception('Debe agregar facturas del exterior.');

                if (array_key_exists('local', $_POST))
                    foreach ($_POST['local'] as $new_keys => $new) {
                        if ($new['plantilla_factura_id'] == "") {
                            unset($_POST['local'][$new_keys]); // eliminar items que tienen plantilla_id vacio.
                            Yii::$app->request->setBodyParams($_POST);
                        }
                    }
                else
                    throw new \Exception('Debe agregar facturas locales.');

                $local_tabla = $_POST['local'];
                $exterior_tabla = $_POST['exterior'];
                $model->fecha = implode('-', array_reverse(explode('-', $model->fecha)));
                $model->cal_retencion_fact_local_flete_seguro = $model->cal_retencion_fact_local_flete_seguro == 1 ? 'si' : 'no';
                $facturasExteriorDB = $model->importacionDetallesExterior;
                $facturasLocalDB = $model->importacionDetallesLocal;

                foreach ($exterior_tabla as $index => $detalleExterior) {
                    $importacionDetalleExterior = null;
                    $array = explode('_', $detalleExterior['plantilla_factura_id']);
                    /** @var ImportacionDetalleFacturaLocalExterior $importacionDetalleDB */
                    foreach ($facturasExteriorDB as $i => $importacionDetalleDB) {
                        if ($array[3] == $importacionDetalleDB->factura_compra_id &&
                            $array[1] == $importacionDetalleDB->plantilla_id) {
                            $importacionDetalleExterior = $importacionDetalleDB;
                            break;
                        }
                    }

                    if ($importacionDetalleExterior == null) {
                        $importacionDetalleExterior = new ImportacionDetalleFacturaLocalExterior();
                        $importacionDetalleExterior->es_local = 'no';
                        $importacionDetalleExterior->factura_compra_id = $array[3];
                        $importacionDetalleExterior->plantilla_id = $array[1];
                    }
                    $importacionDetalleExterior->cotizacion = isset($detalleExterior['cotizacion']) ? $detalleExterior['cotizacion'] : null;
                    $importacionDetalleExterior->plantilla_factura_id = $detalleExterior['plantilla_factura_id'];
                    $importacionDetalleExterior->cotizacion = floatval(str_replace([".", ","], ["", "."], $importacionDetalleExterior->cotizacion));
                    $model->arrayExterior[] = $importacionDetalleExterior;
                }
                foreach ($local_tabla as $index => $detalleLocal) {
                    $importacionDetalleLocal = null;
                    $array = explode('_', $detalleLocal['plantilla_factura_id']);
                    /** @var ImportacionDetalleFacturaLocalExterior $importacionDetalleDB */
                    foreach ($facturasLocalDB as $i => $importacionDetalleDB) {
                        if ($array[3] == $importacionDetalleDB->factura_compra_id &&
                            $array[1] == $importacionDetalleDB->plantilla_id) {
                            $importacionDetalleLocal = $importacionDetalleDB;
                            break;
                        }
                    }
                    if ($importacionDetalleLocal == null) {
                        $importacionDetalleLocal = new ImportacionDetalleFacturaLocalExterior();
                        $importacionDetalleLocal->es_local = 'si';
                        $importacionDetalleLocal->factura_compra_id = $array[3];
                        $importacionDetalleLocal->plantilla_id = $array[1];
                    }
                    $importacionDetalleLocal->cotizacion = isset($detalleLocal['cotizacion']) ? $detalleLocal['cotizacion'] : null;
                    $importacionDetalleLocal->cotizacion = floatval(str_replace([".", ","], ["", "."], $importacionDetalleLocal->cotizacion));
                    $importacionDetalleLocal->plantilla_factura_id = $detalleLocal['plantilla_factura_id'];
                    $importacionDetalleLocal->absorber_impuesto = $detalleLocal['absorber_impuesto'];
                    $importacionDetalleLocal->incluir_costo = $detalleLocal['incluir_costo'];
                    $importacionDetalleLocal->ivas = $this->getImpuestos($importacionDetalleLocal->factura_compra_id, $importacionDetalleLocal->plantilla_id, 'iva_');
                    $model->arrayLocal[] = $importacionDetalleLocal;
                }

                if (!$model->validate())
                    throw new \Exception('Error al validar la importación. ' . $model->getErrorSummary(true)[0]);

                $model->save();

                /** @var ImportacionDetalleFacturaLocalExterior $importacionDetalleLocal */
                foreach ($model->arrayLocal as $importacionDetalleLocal) {
                    $importacionDetalleLocal->importacion_id = $model->id;
                    if (!$importacionDetalleLocal->validate())
                        throw new \Exception('Error al validar las facturas locales. ' . $importacionDetalleLocal->getErrorSummary(true)[0]);

                    $importacionDetalleLocal->save(false);
                }
                //Borrar los que correspondan
                foreach ($facturasLocalDB as $facturaLocalOld) {
                    $encontrado = false;
                    foreach ($model->arrayLocal as $facturaLocalNew) {
                        if ($facturaLocalOld->id == $facturaLocalNew->id) {
                            $encontrado = true;
                            break;
                        }
                    }
                    if (!$encontrado || sizeof($model->arrayLocal) == 0) {
                        $facturaCompraOld = $facturaLocalOld->facturaCompra;
                        $facturaCompraOld->importacion_id = null;
                        $facturaCompraOld->ruc = '4545';//Solo para que pase la validación
                        $facturaCompraOld->nombre_entidad = 'asfsdf';//Solo para que pase la validación
                        $facturaCompraOld->timbrado_id = 1;//Solo para que pase la validación
                        $facturaCompraOld->save();

                        /** @var Compra $facturaCompra */
                        $facturaCompra = Compra::find()->where(['importacion_id' => $facturaLocalOld->id])->one();
                        if ($facturaCompra != null) {
                            $facturaCompra->importacion_id = $model->id;
                            $facturaCompra->ruc = '4545';//Solo para que pase la validación
                            $facturaCompra->nombre_entidad = 'asfsdf';//Solo para que pase la validación
                            $facturaCompra->timbrado_id = 1;//Solo para que pase la validación
                            $facturaCompra->save();
                        }

                        $facturaLocalOld->delete();
                    }
                }

                /** @var ImportacionDetalleFacturaLocalExterior $importacionDetalleExterior */
                foreach ($model->arrayExterior as $importacionDetalleExterior) {
                    $importacionDetalleExterior->importacion_id = $model->id;
                    if (!$importacionDetalleExterior->validate())
                        throw new \Exception('Error al validar las facturas del exterior.');

                    $importacionDetalleExterior->save(false);
                }
                //Borrar los que correspondan
                foreach ($facturasExteriorDB as $facturaExteriorOld) {
                    $encontrado = false;
                    foreach ($model->arrayExterior as $facturaExteriorNew) {
                        if ($facturaExteriorOld->id == $facturaExteriorNew->id) {
                            $encontrado = true;
                            break;
                        }
                    }
                    if (!$encontrado || sizeof($model->arrayExterior) == 0) {
                        $facturaCompraOld = $facturaExteriorOld->facturaCompra;
                        $facturaCompraOld->importacion_id = null;
                        $facturaCompraOld->ruc = '4545';//Solo para que pase la validación
                        $facturaCompraOld->nombre_entidad = 'asfsdf';//Solo para que pase la validación
                        $facturaCompraOld->timbrado_id = 1;//Solo para que pase la validación
                        $facturaCompraOld->save();

                        /** @var Compra $facturaCompra */
                        $facturaCompra = Compra::find()->where(['importacion_id' => $facturaExteriorOld->id])->one();
                        if ($facturaCompra != null) {
                            $facturaCompra->importacion_id = $model->id;
                            $facturaCompra->ruc = '4545';//Solo para que pase la validación
                            $facturaCompra->nombre_entidad = 'asfsdf';//Solo para que pase la validación
                            $facturaCompra->timbrado_id = 1;//Solo para que pase la validación
                            $facturaCompra->save();
                        }

                        $facturaExteriorOld->delete();
                    }
                }

                FlashMessageHelpsers::createSuccessMessage('Guardado exitósamente.');
                $trans->commit();
                if ($model->guardarycerrar == 'si')
                    return $this->redirect(['index']);
            } catch (\Exception $e) {
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($e->getMessage());
                $model->fecha = implode('-', array_reverse(explode('-', $model->fecha)));
                $model->cal_retencion_fact_local_flete_seguro = $model->cal_retencion_fact_local_flete_seguro == 'si' ? 1 : 0;
            }
        }

        return $this->render('update', [
            'model' => $model,
            'tabReporte' => $tabReporte
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionEnableCamposTotales()
    {
        $campos_a_habilitar = [];
        $a_retornar = [];
        $plantilla_id = $_POST['plantilla_id'];
        $prefijo_campo = $_POST['prefijo_campo'];
        $plantilla_detalles = PlantillaCompraventaDetalle::findAll([
            'plantilla_id' => $plantilla_id, 'tipo_asiento' => 'compra']);

        foreach ($plantilla_detalles as $detalle) {
            /** @var IvaCuenta $iva_cta */
            $iva_cta = $detalle->ivaCta;

            switch ($iva_cta) {
                case (null):
//                    $campos_a_habilitar[] = '#' . $prefijo_campo . '0';
                    $campos_a_habilitar[0] = '#' . $prefijo_campo . '0';
                    break;
                case (!null):
//                    $campos_a_habilitar[] = '#' . $prefijo_campo . $iva_cta->iva->porcentaje;
                    $campos_a_habilitar[$iva_cta->iva->porcentaje] = '#' . $prefijo_campo . $iva_cta->iva->porcentaje;
                    break;
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        foreach ($campos_a_habilitar as $item)
            $a_retornar[] = $item;
        return $a_retornar;
    }

    protected function getImpuestos($facturaCompraId, $plantillaId, $prefijoIndice)
    {
        $a_retornar = [];
        $factCompra = Compra::findOne($facturaCompraId);
        if ($factCompra != null) {
            foreach ($factCompra->ivasCuentaUsadas as $ivaCuentaUsada) {
                if ($ivaCuentaUsada->plantilla_id == $plantillaId) {
                    /** @var IvaCuenta $iva_cta */
                    $iva_cta = $ivaCuentaUsada->ivaCta;
                    switch ($iva_cta) {
                        case (null):
                            $a_retornar[$prefijoIndice . '0'] = $ivaCuentaUsada->monto;
                            break;
                        case (!null):
                            $a_retornar[$prefijoIndice . $iva_cta->iva->porcentaje] = round(
                                $ivaCuentaUsada->monto - ($ivaCuentaUsada->monto / (1.00 + ($ivaCuentaUsada->ivaCta->iva->porcentaje / 100.0))));
                            break;
                    }
                }
            }
        }

        return $a_retornar;
    }

    public function actionGetIvasTotales()
    {
        $idCompuesta = $_POST['id_compuesta'];
        $prefijo_campo = $_POST['prefijo_campo'];
        $a_retornar = [];

        $array = explode('_', $idCompuesta);
        if (sizeof($array) == 4) {
            $a_retornar = $this->getImpuestos($array[3], $array[1], '#' . $prefijo_campo);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $a_retornar;
    }

    public function actionGetDataFactura($local, $importacionId = null)
    {
        $seleccionados = [];

        if (isset($_POST['seleccionados']) && $_POST['seleccionados'] != '')
            $seleccionados = $_POST['seleccionados'];

        $facturas = Compra::getPlantillaFacturasByImportacion(($local == 'true' || $local == 1) ? true : false, $importacionId, false, $seleccionados);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $facturas;
    }

    public static function getAcceso($action)
    {
        return PermisosHelpers::getAcceso('contabilidad-importacion-' . $action);
    }

    /**
     * Finds the Compra model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Importacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Importacion::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionReporte($id)
    {
        $model = Importacion::findOne($id);
        $cssInline = Yii::$app->view->renderFile("@app/modules/contabilidad/views/importacion/reporte/pdf.css");
        $contenido = Yii::$app->view->renderFile("@app/modules/contabilidad/views/importacion/reporte/pdf.php", ['model' => $model]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'content' => $contenido,
            'cssInline' => $cssInline,
            'destination' => Pdf::DEST_BROWSER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'format' => Pdf::FORMAT_A4,
            //'marginTop' => 17,
            'filename' => "repote_importacion_" . $id . ".pdf",
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    /**
     * @param $id
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \Exception
     */
    public function actionReporteExcel($id)
    {
        $model = Importacion::findOne($id);

        // Crear objetoExcel.
        /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        date_default_timezone_set('America/Asuncion');

        /** Create a new Spreadsheet Object **/
        $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/importacion/reporte/xls/';
        $objPHPExcel = IOFactory::load($carpeta . 'template.xlsx');

        $empresa = $model->empresa;
        if ($empresa->tipo == 'juridica') {
            $razonSocial = $empresa->razon_social;
        } else {
            $razonSocial = $empresa->primer_apellido . ' ' . $empresa->segundo_apellido . ', ' . $empresa->primer_nombre . ' ' . $empresa->segundo_nombre;
        }

        ///////////////////////////////////////////////////////////////////////////////
        ///                                 CONTENIDO                               ///
        ///////////////////////////////////////////////////////////////////////////////
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, 'EMPRESA: ' . strtoupper($razonSocial));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, 'EXPORTADOR: ' . strtoupper($model->exportador));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 4, 'VIA: ' . strtoupper($model->medio_transporte));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 5, 'CONDICIÓN: ' . strtoupper($model->condicion));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 7, 'DESPACHO Nº ' . $model->numero_despacho);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 9, Yii::$app->formatter->asDate($model->fecha, 'd-MM-Y'));

        $result = $model->getItemsLocalExteriorTr();

        $valorDespacho = $model->getValorDespacho();
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . 11 . ':C' . 11);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 11, 'Valor Despacho');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D' . 11 . ':E' . 11);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 11, round($valorDespacho));
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F' . 11 . ':G' . 11);

        $fila = 12;
        foreach ($result['arraydata'] as $item) {
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':C' . $fila);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, $item['item']);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D' . $fila . ':E' . $fila);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $fila)->setValueExplicit($item['monto'], DataType::TYPE_NUMERIC);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F' . $fila . ':G' . $fila);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $fila)->setValueExplicit($item['cotizacion'], DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $fila)->setValueExplicit($item['total'], DataType::TYPE_NUMERIC);
            $fila++;
        }
        $objPHPExcel->getActiveSheet()->getStyle('D' . ($fila - sizeof($result['arraydata'])) . ':G' . ($fila - 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objPHPExcel->getActiveSheet()->getStyle('H' . ($fila - sizeof($result['arraydata'])) . ':H' . $fila)->getNumberFormat()->setFormatCode('#,##0');

        $totalItem = $result['totalitem'];
        $totalItemRecambio = $result['totalItemRecambio'];
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F' . $fila . ':G' . $fila);
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $fila)->setValueExplicit('TOTAL:', DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':H' . $fila)->getBorders()->getBottom()->setBorderStyle(Border::BORDER_DOUBLE);
        $objPHPExcel->getActiveSheet()->getStyle('F' . $fila . ':G' . $fila)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $fila)->setValueExplicit($totalItem, DataType::TYPE_NUMERIC);
        $fila += 2;

        $objPHPExcel->getActiveSheet()->getStyle('C' . $fila . ':C' . $fila)->getNumberFormat()->setFormatCode('#,##0.00');
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $fila)->setValueExplicit('RECAMBIO:', DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':B' . $fila)->getFont()->setBold(true);
        $recambio = $totalItem / $totalItemRecambio;
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $fila)->setValueExplicit($recambio, DataType::TYPE_NUMERIC);

        $fila += 3;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':F' . $fila);
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $fila)->setValueExplicit('IVA', DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':F' . $fila)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':F' . $fila)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':F' . $fila)->getBorders()->getBottom()->setBorderStyle(Border::BORDER_MEDIUM);
        $fila++;

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':D' . $fila);
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $fila)->setValueExplicit('Ítem', DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':D' . $fila)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':D' . $fila)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E' . $fila . ':F' . $fila);
        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $fila)->setValueExplicit('Total', DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $fila . ':F' . $fila)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $fila . ':F' . $fila)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $fila++;

        //Total IVA
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':D' . $fila);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, 'IMPUESTO');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E' . $fila . ':F' . $fila);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $fila, $model->iva);
        $fila++;

        $resultIva = $model->getIvasTr();
        $totalIva = $resultIva['total'];
        foreach ($resultIva['arraydata'] as $item) {
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':D' . $fila);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, $item['item']);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E' . $fila . ':F' . $fila);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $fila, $item['monto']);
            $fila++;
        }
        $objPHPExcel->getActiveSheet()->getStyle('E' . ($fila - sizeof($resultIva['arraydata']) - 1) . ':F' . ($fila - 1))->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('E' . $fila . ':E' . $fila)->getNumberFormat()->setFormatCode('#,##0');

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $fila, 'TOTAL:');
        $objPHPExcel->getActiveSheet()->getStyle('C' . $fila . ':D' . $fila)->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E' . $fila . ':F' . $fila);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $fila, $totalIva);

        $resultRetenciones = $model->getRetencionesTr();
        if (($model->condicion == 'cif' or $model->condicion == 'fob') && !empty($resultRetenciones['arraydatarenta'])) {
            $fila += 3;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':H' . ($fila + 1));
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . ($fila + 1))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . $fila)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . $fila)->getFont()->setUnderline(Font::UNDERLINE_SINGLE);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, 'Retención renta');

            $fila += 2;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':C' . $fila);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $fila)->setValueExplicit('Ítem', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $fila)->setValueExplicit('Monto', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $fila)->setValueExplicit('%', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $fila)->setValueExplicit('Renta 3%', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $fila)->setValueExplicit('Cotización', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $fila)->setValueExplicit('Total', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . $fila)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . $fila)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fila++;

            $totalRenta = 0;
            $totalIva = 0;
            foreach ($resultRetenciones['arraydatarenta'] as $item) {
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':C' . $fila);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, $item['item']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $fila, $item['monto']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $fila, $item['%']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $fila, $item['renta']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $fila, $item['cotizacion']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $fila, $item['total']);
                $fila++;
                $totalRenta += $item['total'];
            }
            $objPHPExcel->getActiveSheet()->getStyle('D' . ($fila - sizeof($resultRetenciones['arraydatarenta'])) . ':G' . ($fila - 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('H' . ($fila - sizeof($resultRetenciones['arraydatarenta'])) . ':H' . $fila)->getNumberFormat()->setFormatCode('#,##0');

            //Total renta
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $fila, $totalRenta);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':H' . $fila)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
        }
        if ($model->condicion == 'fob' && !empty($resultRetenciones['arraydataiva'])) {
            $fila += 3;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':H' . ($fila + 1));
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . ($fila + 1))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . $fila)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . $fila)->getFont()->setUnderline(Font::UNDERLINE_SINGLE);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, 'Retención IVA');

            $fila += 2;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':C' . $fila);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $fila)->setValueExplicit('Ítem', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $fila)->setValueExplicit('Monto', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $fila)->setValueExplicit('%', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $fila)->setValueExplicit('Iva 10%', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $fila)->setValueExplicit('Cotización', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $fila)->setValueExplicit('Total', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . $fila)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':H' . $fila)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $fila++;
            foreach ($resultRetenciones['arraydataiva'] as $item) {
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B' . $fila . ':C' . $fila);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, $item['item']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $fila, $item['monto']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $fila, $item['%']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $fila, $item['iva']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $fila, $item['cotizacion']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $fila, $item['total']);
                $fila++;
                $totalIva += $item['total'];
            }
            $objPHPExcel->getActiveSheet()->getStyle('D' . ($fila - sizeof($resultRetenciones['arraydataiva'])) . ':G' . ($fila - 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objPHPExcel->getActiveSheet()->getStyle('H' . ($fila - sizeof($resultRetenciones['arraydataiva'])) . ':H' . $fila)->getNumberFormat()->setFormatCode('#,##0');

            //Total renta IVA
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $fila, $totalIva);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':H' . $fila)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
        }

        if (!empty($resultRetenciones['totalpagar'])) {
            $fila += 2;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F' . $fila . ':G' . $fila);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $fila, 'TOTAL A PAGAR:');
            $objPHPExcel->getActiveSheet()->getStyle('F' . $fila . ':G' . $fila)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $fila, round($resultRetenciones['totalpagar']));
            $objPHPExcel->getActiveSheet()->getStyle('F' . $fila . ':H' . $fila)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
            $objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':H' . $fila)->getNumberFormat()->setFormatCode('#,##0');
        }

        //Si tiene retencion de IRACIS se agrega al final - Pedido del Sr. Griño
        if ($model->iracis_generaral != null && $model->iracis_generaral != 0) {
            $fila += 2;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, 'Retención IRACIS');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $fila, $model->iracis_generaral);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':C' . $fila)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('B' . $fila . ':C' . $fila)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_MEDIUM);
            $objPHPExcel->getActiveSheet()->getStyle('C' . $fila . ':C' . $fila)->getNumberFormat()->setFormatCode('#,##0');
        }

        //////////////////////////////////// FIN //////////////////////////////////////

        ExcelHelpers::exportToBrowser($objPHPExcel, 'importacion_' . $model->numero_despacho);
    }
}