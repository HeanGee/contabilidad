<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\helpers\ExcelHelpers;
use backend\helpers\Helpers;
use backend\models\Empresa;
use backend\models\Iva;
use backend\models\Moneda;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\PrestamoDetalle;
use backend\modules\contabilidad\models\PrestamoDetalleCompra;
use backend\modules\contabilidad\models\ReciboDetalle;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\Timbrado;
use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\validators\ReporteValidator;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\FlashMessageHelpsers;
use common\models\ParametroSistema;
use Exception;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DataType as ExcelDataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * VentaController implements the CRUD actions for Venta model.
 */
class ReporteController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param string $operacion
     * @return mixed|string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionReporteLibro($operacion = 'venta')
{
    $model = new Reporte();
    $model->criterio1 = 'factura.fecha_emision';
    $model->operacion = $operacion;
    $model->filtro = 'fecha_emision__id';
    $model->moneda = '1';
    $model->valorizar = 'si';
    $model->paper_size = Pdf::FORMAT_A4;
    $model->tipo_comprobante = [6, 7];
    $model->groupByCuenta = 'no';
    $model->para_iva = '*';
    $model->resumen_plantilla = 'no';

    $nombre = 'tipo_documento_set_ninguno';
    $tipodocset_ninguno_id = \backend\modules\contabilidad\models\ParametroSistema::getValorByNombre($nombre);
    $tipodoc_set_ids = [];
    $tipodoc_models = [];
    $tipodocset_id_name = [];
    foreach (($operacion == 'compra') ?
                 Compra::findAll(['empresa_id' => Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]) :
                 Venta::findAll(['empresa_id' => Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
             as $factura) {
        if ($factura->tipo_documento_id != null) {
            if (!array_key_exists($factura->tipo_documento_id, $tipodoc_models)) {
                $tipodoc_models[$factura->tipo_documento_id] = $factura->tipoDocumento;
            }
            $tipoDoc = $tipodoc_models[$factura->tipo_documento_id];

            if (!array_key_exists($tipoDoc->tipo_documento_set_id, $tipodocset_id_name)) {
                $tipodocset_id_name[$tipoDoc->tipo_documento_set_id] = $tipoDoc->tipoDocumentoSet->nombre;
            }

            $tipodocset_id = $tipoDoc->tipo_documento_set_id;
            $tipodocset_name = $tipodocset_id_name[$tipoDoc->tipo_documento_set_id];
            if (!in_array($tipodocset_id, $tipodoc_set_ids) && $tipodocset_id != $tipodocset_ninguno_id)
                $tipodoc_set_ids["{$tipodocset_id}"] = $tipodocset_name;
        }
    }

    if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);//5 min
        ini_set('pcre.backtrack_limit', 10000000);

        if (!$model->validate()) {
            FlashMessageHelpsers::createWarningMessage($model->getErrorSummaryAsString());
            goto retorno;
        }

        if ($model->paper_size == '') {
            FlashMessageHelpsers::createWarningMessage('Indique un tamaño de hoja.');
            goto retorno;
        }

        $query = ($operacion == 'venta') ? Venta::find() : Compra::find();
        $query->alias('factura');

        #Moneda
        if ($model->moneda != "") {
            $query->where(['OR', ['moneda_id' => $model->moneda], ['AND', ['!=', 'factura.estado', 'vigente'], ['IS', 'factura.moneda_id', null]]]);
        }

        #Join con detalle factura
        $query->leftJoin(
            ($operacion == 'venta' ? 'cont_factura_venta_detalle' : 'cont_factura_compra_detalle') . ' as detalle',
            'detalle.factura_' . $operacion . '_id = factura.id'
        );

        #Join con plan de cuenta
        $query->leftJoin(
            'cont_plan_cuenta as cta',
            'detalle.plan_cuenta_id = cta.id'
        );

        #Join con tipo de documento
        $query->leftJoin('cont_tipo_documento tipodoc', 'tipodoc.id = factura.tipo_documento_id');

        // excluir facturas de importacion
        if ($operacion == 'compra') {
            $query->leftJoin(
                'cont_factura_compra_iva_cuenta_usada as iva_cta_usada',
                'factura.id = iva_cta_usada.factura_compra_id'
            );
            $query->leftJoin(
                'cont_plantilla_compraventa as plla',
                'iva_cta_usada.plantilla_id = plla.id'
            );
            $query->andWhere(['=', 'plla.para_importacion', 'no']);
        }

        // concatenar las cuentas principales.
        // A peticion de Jose: No es coherente en la usabilidad el no poder guardar facturas porque ninguno de sus
        // detalles fueran configurados como cuenta principal.
        // Ese control dejarlo en el abm de plantillas.
        // Para el reporte de libros, en vez de buscar por cta_principal en los detalles de las facturas,
        // hacer join los detalles de las plantillas usadas y buscar alli.
        // Va a ocurrir que si existieran facturas cuyos detalles no contengan cuentas contables que actualmente
        // están especificadas en las plantillas utilizadas, ésas facturas no se incluirán en el reporte.
        // 21 FEBRERO: SE des-hace esto debido a la situacion que genera el correo de Magali, del dia 21febrero2019 a las 13:24
//            $query->leftJoin(
//                "cont_factura_{$operacion}_iva_cuenta_usada ivac",
//                "ivac.factura_{$operacion}_id = factura.id");
//            $query->leftJoin(
//                "cont_plantilla_compraventa_detalle pldet",
//                "pldet.plantilla_id = ivac.plantilla_id"
//            );

        $query->select([
            'factura.*', 'detalle.id as detalle', 'GROUP_CONCAT(DISTINCT cta.nombre SEPARATOR \' | \') as ctanombre'
        ]);

        // VENTAS ANULADAS NO TIENEN DETALLES POR ESO HAY QUE HACER UN OR.
        $query->andWhere(['OR', ['=', 'detalle.cta_principal', 'si'], ['AND', ['IS', 'detalle.cta_principal', null], ['!=', 'factura.estado', 'vigente']]]);
//            $query->andWhere([
//                'OR',
//                [
//                    'AND',
//                    "pldet.p_c_gravada_id = detalle.plan_cuenta_id",
//                    ['=', 'pldet.cta_principal', 'si']
//                ],
//                ['IS', 'detalle.cta_principal', null]
//            ]);

        // clasificar si es para iva o para renta
        $query->andFilterWhere(['OR', ['factura.para_iva' => $model->para_iva == '*' ? ['si', 'no'] : $model->para_iva], ['AND', "factura.para_iva IS NULL", "factura.estado != 'vigente'"]]);
        $orderBy = [];

        $desde = '';
        $hasta = '';
        $fieldSet = ['criterio1', 'criterio2', 'criterio3'];
        foreach ($fieldSet as $attribute) {
            $criterio = $model->$attribute;
            $criterioFechaAttribute = $attribute . 'fecha';
            $criterioFecha = $model->$criterioFechaAttribute;

            if ($criterio == '') continue;

            if (in_array($criterio, ['entidad.razon_social', 'factura.nro_factura', 'factura.fecha_emision', 'factura.creado'])) {
                switch ($criterio) {
                    case 'entidad.razon_social':
                        $query->joinWith('entidad as entidad')
                            ->select([
                                'factura.*',
                                'entidad.razon_social',
                                'cta.nombre as ctanombre',
                            ]);
                        $orderBy['entidad.razon_social'] = SORT_ASC;
                        break;
                    case 'factura.nro_factura':
                        if ($operacion == 'venta') {
                            $concat = "CONCAT((factura.prefijo), ('-'), (factura.nro_factura))";
                            $query->select(['factura.*', $concat . ' AS nro_factura_completo', 'cta.nombre as ctanombre']);
                            $orderBy[$concat] = SORT_ASC;
                        } else {
                            $orderBy[$criterio] = SORT_ASC;
                        }
                        break;
                    case 'factura.fecha_emision':
                        if ($criterioFecha != '') {
                            $rango = explode(' - ', $criterioFecha);
                            $desde = implode('-', array_reverse(explode('-', $rango[0])));
                            $hasta = implode('-', array_reverse(explode('-', $rango[1])));
                            if ($desde == $hasta and $desde != '') {
                                $query->andFilterWhere(['=', "STR_TO_DATE({$criterio}, '%Y-%m-%d')", $desde]);
                            } elseif ($desde != '') {
                                $query->andFilterWhere(['between', "STR_TO_DATE({$criterio}, '%Y-%m-%d')", $desde, $hasta]);
                            }
                        }
                        $orderBy[$criterio] = SORT_ASC;
                        break;
                    case 'factura.creado':
                        $rango = explode(' - ', $criterioFecha);
                        $desde = implode('-', array_reverse(explode('-', $rango[0])));
                        $hasta = implode('-', array_reverse(explode('-', $rango[1])));
                        if ($desde == $hasta and $desde != '') {
                            $query->andFilterWhere(['=', "STR_TO_DATE({$criterio}, '%Y-%m-%d')", $desde]);
                            $orderBy[$criterio] = SORT_ASC;
                        } elseif ($desde != '') {
                            $query->andFilterWhere(['between', "STR_TO_DATE({$criterio}, '%Y-%m-%d')", $desde, $hasta]);
                            $orderBy[$criterio] = SORT_ASC;
                        }
                        break;
                }
            } else {
                $orderBy[$criterio] = SORT_ASC;
            }
        }

        $query->andFilterWhere(['=', 'entidad_id', $model->entidad_id]);
        $query->andFilterWhere(['>=', 'factura.id', $model->cod_desde])->andFilterWhere(['<=', 'factura.id', $model->cod_hasta]);
        $query->andWhere(['factura.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'), 'factura.empresa_id' => \Yii::$app->session->get('core_empresa_actual')]);

        $query->andFilterWhere([
            'OR',
            ['IN', 'factura.tipo_documento_id', $model->tipo_comprobante],
            [
                'AND',
                ['!=', 'factura.estado', 'vigente'],
                ['IS', 'factura.tipo_documento_id', null]
            ]
        ]);

        $query->andFilterWhere(['IN', 'factura.obligacion_id', $model->obligaciones]);
        if ($model->incluir_no_vigente == 'no') {
            $query->andFilterWhere(['=', 'factura.estado', 'vigente']);
        }
//            if ($model->tipo_comprobante == 'factura') {
//                $query->andWhere(['IS', "factura.factura_{$operacion}_id", null]); // explicitamente null el factura_compra/venta_id
//                $query->andWhere([
//                    'OR',
//                    ['!=', "tipodoc.tipo_documento_set_id", Compra::getTipodocSetNotaCredito()->id],
//                    [
//                        'AND',
//                        ['IS', 'factura.tipo_documento_id', null],
//                        ['!=', 'factura.estado', 'vigente']
//                    ]
//                ]);
//            } elseif ($model->tipo_comprobante == 'nota') {
//                $tipoDocIds = [];
//                $docSetNotaCredito = Compra::getTipodocSetNotaCredito();
//                $docSetNotaDebito = Compra::getTipodocSetNotaDebito();
//                /** @var TipoDocumento $tipoDoc */
//                foreach (TipoDocumento::find()->all() as $tipoDoc) {
//                    if (in_array($tipoDoc->tipo_documento_set_id, [$docSetNotaCredito->id, $docSetNotaDebito->id]))
//                        $tipoDocIds[] = $tipoDoc->id;
//                }
//                $query->andFilterWhere(['IN', 'factura.tipo_documento_id', $tipoDocIds]);
//            }

//            // Excluir autofactuas. Descomentar si piden.
//            $query->andWhere(['IS', 'cedula_autofactura', null]);

        # Agrupar por factura
        $query->groupBy('factura.id');

        // Ordenar segun filtro.
        $query->orderBy($orderBy);

        $data = $query->all();
        $cuentas_principales = ArrayHelper::map($query->asArray()->all(), 'id', 'ctanombre');
        if (empty($data)) {
            FlashMessageHelpsers::createWarningMessage('No se encontraron resultados');
            goto retorno;
        };

        # Cuando son notas de credito/debito, no quieren que aparezca los no vigentes. Solo cuando es factura.
        if ($model->incluir_no_vigente == 'si') {
            $tipo_documento_set_ids = [];
            foreach ($data as $item) {
                if ($item['timbrado_detalle_id'] == "")  # comprobantes de venta no tiene timbrado, en compras.
                    continue;

                $tipoDocSetId = $item->timbradoDetalle->tipo_documento_set_id;
                if ($item->estado == 'vigente' && !in_array($tipoDocSetId, $tipo_documento_set_ids))
                    $tipo_documento_set_ids[] = $tipoDocSetId;
            }

            foreach ($data as $key => $item) {
                if ($item['timbrado_detalle_id'] == '')
                    continue;

                if (!in_array($item->timbradoDetalle->tipo_documento_set_id, $tipo_documento_set_ids))
                    unset($data[$key]);
            }
            $data = array_values($data);
        }

//            $cuentas_principales = $this->getCuentasPrincipalesByFactura($data, $operacion); // trae todas las cuentas principales
//            $cuentas_principales = [];
//            foreach ($query->asArray()->all() as $item) {
//                $cuentas_principales[] = $item['ctanombre'];
//            }

        $tipodoc_set_ids = [];
        $tipodoc_models = [];
        foreach ($data as $factura) {
            if ($factura->tipo_documento_id != null) {
                if (!array_key_exists($factura->tipo_documento_id, $tipodoc_models)) {
                    $tipodoc_models[$factura->tipo_documento_id] = $factura->tipoDocumento;
                }
                $tipoDoc = $tipodoc_models[$factura->tipo_documento_id];

                if (!in_array($tipoDoc->tipo_documento_set_id, $tipodoc_set_ids))
                    $tipodoc_set_ids[] = $tipoDoc->tipo_documento_set_id;
            }
        }

        $count = count($data);
        if ($count > 150) {
            ini_set('memory_limit', (int)ceil($count * 0.9) . 'M');
        }

        $nro_facturas_libres = ($operacion == 'venta') ? $this->getNroFacturasLibres($data) : [];

            //para descargar en excel
            if ($model->tipo_doc == 'xls') {
                $path = '/modules/contabilidad/views/reporte/templates/';
                $name = 'reporteExcel_template.xlsx';
                $objPHPExcel = ExcelHelpers::generatePHPExcelObject($path, $name);
                $this->fillReporteLibroExcel($model, $objPHPExcel, $data, $tipodoc_set_ids, $cuentas_principales, $desde, $hasta, $operacion);
                ExcelHelpers::exportToBrowser($objPHPExcel, "Reporte Libro " . ucwords($operacion) . " ");
            } else if ($model->tipo_doc == 'pdf') {
                $path = 'modules\contabilidad\views\reporte\templates';
                $path = str_replace('\\', '/', $path);
                $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/reporte.css");
                $contenido = Yii::$app->view->renderFile("@app/" . $path . "/reporte.php", [
                    'data' => $data,
                    'cuentas_principales' => $cuentas_principales,
                    'valorizar' => $model->valorizar,
                    'agrupar' => $model->agrupar,
                    'nro_facturas_libres' => $nro_facturas_libres,
                    'resumen_plantilla' => $model->resumen_plantilla,
                    'tipoDocumentoSetIds' => $tipodoc_set_ids,
                    'separarVisualmente' => $model->separar_visualmente,
                    'model' => $model,
                ]);
                $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/reporte_header.php", ['data' => $data, 'del' => $desde, 'al' => $hasta]);
                $pie = Yii::$app->view->renderFile("@app/" . $path . "/reporte_pie.php", []);
                $op = ucfirst($operacion);

                // http response
                $response = Yii::$app->response;
                $response->format = Response::FORMAT_RAW;
                $headers = Yii::$app->response->headers;
                $headers->add('Content-Type', 'application/pdf');
                $requestedMonthFrom = explode('-', $desde)[1];
                $requestedMonthTo = explode('-', $hasta)[1];
                $yearsFrom = explode('-', $desde)[0];
                $yearsTo = explode('-', $hasta)[0];
                if ($requestedMonthFrom == $requestedMonthTo)
                    $requestedMonths = Helpers::monthToText($requestedMonthFrom) . ' ' . $yearsFrom;
                else {
                    $from = Helpers::monthToText($requestedMonthFrom);
                    $to = Helpers::monthToText($requestedMonthTo);
                    $requestedMonths = "{$from} {$yearsFrom} ~ {$to} {$yearsTo}";
                }
                $filename = "{$op} - {$requestedMonths}.pdf";

                $pdf = new Pdf([
                    'mode' => Pdf::DEST_BROWSER,
                    'orientation' => Pdf::ORIENT_LANDSCAPE,
                    'format' => $model->paper_size,
                    'marginTop' => 47,
                    'marginLeft' => 7,
                    'marginBottom' => 18,
                    'marginRight' => 7,
                    'content' => $contenido,
                    'cssInline' => $cssInline,
                    'filename' => $filename,
                    'options' => [
                        'title' => $filename,
                        'subject' => "",
                        'defaultheaderline' => false
                    ],
                    'methods' => [
                        'SetHeader' => [$encabezado],
                        'SetFooter' => [$pie],
                        'SetTitle' => [$filename]
                    ]
                ]);

                Yii::$app->response->format = Response::FORMAT_RAW;
                Yii::$app->response->headers->add('Content-Type', 'application/pdf');
                return $pdf->render();
            } else if ($model->tipo_doc == 'import-marangatu') {
                if ($operacion == 'compra') {
                    $nro_facturas = [];
                    foreach ($data as $key => $fila) {
                        array_push($nro_facturas, $fila->nro_factura);
                    }
                }

                $nro_facturas = "'" . join("','", $nro_facturas) . "'";
                $sql = <<<SQL
                select
                    /*compra.id,*/
                    2 as tipo_registro,
                    11 as tipo_id_proveedor,
                    entidad.ruc as ruc,
                    entidad.razon_social as razon_social,
                    109 as tipo_comprobante,
                    DATE_FORMAT(fecha_emision, "%d/%m/%Y"),
                    timbrado.nro_timbrado as timbrado,
                    nro_factura,
                    FLOOR(sum(case when iva.porcentaje = 10 then iva_compra.monto else 0 end)) as gravada_10,
                    FLOOR(sum(case when iva.porcentaje = 5 then iva_compra.monto else 0 end)) as gravada_5,
                    FLOOR(sum(case when iva_compra.iva_cta_id IS NULL then iva_compra.monto else 0 end)) as exenta,
                    total,
                    /*IF(
                        sum(case when iva.porcentaje = 10 then iva_compra.monto else 0 end) +
                        sum(case when iva.porcentaje = 5 then iva_compra.monto else 0 end) +
                        sum(case when iva_compra.iva_cta_id IS NULL then (case when iva_compra.plantilla_id = 6 then -iva_compra.monto else iva_compra.monto end) else 0 end) = total, 'SI', 'NO'
                    ) as verificador,*/
                    (case when condicion = "contado" then 1 else 2 end) as condicion,
                    'N' as operacion_moneda_extrangera,
                    'S' as imputa_al_iva,
                    'N' as imputa_al_ire,
                    'N' as imputa_al_irp,
                    'N' as no_imputa,
                    '' as nro_venta_asociado,
                    '' as timbrado_venta_asociado
                from cont_factura_compra as compra
                left join cont_entidad as entidad on compra.entidad_id = entidad.id
                left join cont_timbrado_detalle as td on compra.timbrado_detalle_id = td.id
                left join cont_timbrado as timbrado on td.timbrado_id = timbrado.id
                left join cont_factura_compra_iva_cuenta_usada as iva_compra on compra.id = iva_compra.factura_compra_id
                left join cont_iva_cuenta as iva_cta on iva_compra.iva_cta_id = iva_cta.id
                left join core_iva as iva on iva_cta.iva_id = iva.id
                where
                    /*compra.id IN (
                        636,
                        491,
                        292,
                        490
                    ) and */
                    iva_compra.monto != 0 and
                    compra.nro_factura in ($nro_facturas)
                group by compra.id;
                SQL;

                $resultSet = Yii::$app->db->createCommand($sql)->queryAll();
                Yii::error($resultSet);


                $carpeta = Yii::$app->basePath . '/web/uploads/';
                $filename_date = date('mY');
                $month = strtoupper(date('M'));
                $day = date('d');
                $filename = "{$carpeta}/5214801_REG_{$filename_date}_{$month}{$day}.csv";
                $fp = fopen($filename,  "w");
                // Loop through file pointer and a line
                foreach ($resultSet as $fields) {
                    fputcsv($fp, $fields);
                }
                fclose($fp);
                return Yii::$app->response->sendFile($filename);
            }
        }

        retorno:;
        return $this->render('reporte-libro-compraventa', ['model' => $model, 'tipoDocumentoSetIds' => $tipodoc_set_ids]);
    }

    /**
     * @param $model Reporte
     * @param $objPHPExcel Spreadsheet
     * @param $data Venta[]|Compra
     * @param $tipodoc_set_ids array
     * @param $cuentas_principales array
     * @param $desde string
     * @param $hasta string
     * @param $operacion string
     * @return Spreadsheet|null
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function fillReporteLibroExcel($model, &$objPHPExcel, $data, $tipodoc_set_ids, $cuentas_principales, $desde, $hasta, $operacion)
{
    try {
        // Rellenar Excel.
        $reporte = [];
        $filas = 9; // la primera fila vacía del template para datos.
        $dataSize = sizeof($data);
        $codigo_desde = $dataSize ? $data[0]->id : null;
        $codigo_hasta = $dataSize ? $data[0]->id : null;
        $list = array_slice($data, 1, ($dataSize - 1));
        foreach ($list as $factura) {
            if ($factura->id > $codigo_hasta) $codigo_hasta = $factura->id;
            elseif ($factura->id < $codigo_desde) $codigo_desde = $factura->id;
        }
        $tipo_libro = strtoupper($operacion);
        $emp_actual = Yii::$app->session->get('core_empresa_actual');
        $empresa = Empresa::findOne(['id' => $emp_actual]);
        Yii::error("{$desde}, {$hasta}");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, "LIBRO {$tipo_libro} ({$empresa->razon_social})");
        $objPHPExcel->getActiveSheet()->setCellValueExplicit("C4", implode('-', array_reverse(explode('-', $desde))), DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit("C5", implode('-', array_reverse(explode('-', $hasta))), DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit("G4", $codigo_desde, DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit("G5", $codigo_hasta, DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit("M4", $empresa->ruc, DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit("M5", $empresa->telefonos, DataType::TYPE_STRING);

        $plantillas_descuento = [];
        foreach (PlantillaCompraventa::findAll(['tipo' => $operacion, 'para_descuento' => 'si']) as $item) {
            $plantillas_descuento[] = $item->id;
        }

        $array_resumen = [
            'gr10' => 0,
            'im10' => 0,
            'gr5' => 0,
            'im5' => 0,
            'ex' => 0,
            'to' => 0,
            'totals_10' => 0,
            'totals_5' => 0
        ];
        $facturas_ya_procesadas = [];
        $resumenPorCuenta = false;
        foreach ($tipodoc_set_ids as $documentoSetId) {
            $total_gravs = 0.0;
            $total_imps = 0.0;
            $total_excs = 0.0;
            $totals = 0.0;
            $items = 0;
            $tt_gr10 = 0.0;
            $tt_gr5 = 0.0;
            $tt_im5 = 0.0;
            $tt_im10 = 0.0;
            $totals_10 = 0.0;
            $totals_5 = 0.0;

            $cta_principal = $cuentas_principales[$data[0]->id];
            $tipodoc_models = [];
            $moneda_models = [];
            $resumen_por_cuenta = [  # usado cuando se agrupa primero por cuenta principal
                'gr10' => 0,
                'iv10' => 0,
                'gr5' => 0,
                'iv5' => 0,
                'exent' => 0,
                'total' => 0,
                'total_10' => 0,
                'total_5' => 0
            ];

            $nextTdoc = true;
            $facturaPrintedExist = false;
            foreach ($data as $key => $fila) {

                # En el primer ciclo de tipo de documento set, ya se debe imprimir todas las facturas anuladas y faltantes.
                # Se espera que el primer tipo de documento set sea facturas emitidas.
                # A partir del 2do tipo de documento set y en adelante, no deben aparecer las no vigentes.

                if ($fila->tipo_documento_id != null) {
                    if (!array_key_exists($fila->tipo_documento_id, $tipodoc_models))
                        $tipodoc_models[$fila->tipo_documento_id] = $fila->tipoDocumento;

                    $tipoDoc = $tipodoc_models[$fila->tipo_documento_id];
                    if ($tipoDoc->tipo_documento_set_id != $documentoSetId) {
                        $facturaPrintedExist = false;
                        continue;
                    }
                } elseif ($documentoSetId == Compra::getTipodocSetNotaCredito()->id) { # tipo de documento de la factura es null y el tipo de documento set en proceso es nota de credito.
                    $facturaPrintedExist = false;
                    continue;
                }

                if (in_array($fila->id, $facturas_ya_procesadas)) continue;

                $facturas_ya_procesadas[] = $fila->id;

                if ($nextTdoc) {
                    $nextTdoc = false;
                    $prefijo = ($fila instanceof Venta) ? $fila->getNroFacturaCompleto() : $fila->nro_factura;
                    $prefijo = substr($prefijo, 0, 7);
                }

                $current_pref = ($fila instanceof Venta) ? $fila->getNroFacturaCompleto() : $fila->nro_factura;
                $current_pref = substr($current_pref, 0, 7);
                $fecha = (isset($fila->fecha_emision) && $fila->fecha_emision != '') ? date('d-m-Y', strtotime($fila->fecha_emision)) : '';
                if ($model->resumen_plantilla == 'no' && $model->separar_visualmente == 'prefijo' && $key > 1 && $current_pref != $prefijo) {
                    $filas++;
                }
                $current_cta_principal = $cuentas_principales[$fila->id];
                if ($model->resumen_plantilla == 'no' && $model->separar_visualmente == 'cuenta_principal' && $key > 1 && $current_cta_principal != $cta_principal && $facturaPrintedExist) {
                    /*$resumen_por_cuenta['gr10'] = number_format($resumen_por_cuenta['gr10'], 0, '.', ',');
                        $resumen_por_cuenta['iv10'] = number_format($resumen_por_cuenta['iv10'], 0, '.', ',');
                        $resumen_por_cuenta['gr5'] = number_format($resumen_por_cuenta['gr5'], 0, '.', ',');
                        $resumen_por_cuenta['iv5'] = number_format($resumen_por_cuenta['iv5'], 0, '.', ',');
                        $resumen_por_cuenta['exent'] = number_format($resumen_por_cuenta['exent'], 0, '.', ',');
                        $resumen_por_cuenta['total'] = number_format($resumen_por_cuenta['total'], 0, '.', ',');*/

                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("E$filas", 'SUBTOTAL', DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getStyle("D$filas")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getBorders()->getTop()->setBorderStyle(Border::BORDER_DOTTED);
                    $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_DOTTED);
                    $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFill()->setFillType(Fill::FILL_SOLID);
                    $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFill()->setStartColor(new Color('ffff6d'));
                    $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFont()->setBold(true);

                    $map = ['F' => 'gr10', 'G' => 'iv10', 'I' => 'gr5', 'J' => 'iv5', 'L' => 'exent', 'M' => 'total',
                        'H' => 'total_10', 'K' => 'total_5'];
                    foreach (['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'] as $col) {
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("$col$filas", $resumen_por_cuenta[$map[$col]], DataType::TYPE_NUMERIC);
                    }

                    $filas++;
                    $filas++;
                    $resumen_por_cuenta = [  //resetear
                        'gr10' => 0,
                        'iv10' => 0,
                        'gr5' => 0,
                        'iv5' => 0,
                        'exent' => 0,
                        'total' => 0,
                        'total_10' => 0,
                        'total_5' => 0
                    ];
                    if (!$resumenPorCuenta)
                        $resumenPorCuenta = true;
                }

                if (($fila instanceof Venta || $fila instanceof Compra) /*&& !in_array($fila->estado, ['anulada', 'faltante'])*/) {
                    $items++;
                    $facturaPrintedExist = true;

                    $isVenta = $fila instanceof Venta;
                    $isCompra = $fila instanceof Compra;

                    /** @var VentaIvaCuentaUsada[] $query */
                    $excs = 0.0;
                    $gravs5 = 0.0;
                    $gravs10 = 0.0;
                    $imps5 = 0.0;
                    $imps10 = 0.0;
                    $total_10 = 0.0;
                    $total_5 = 0.0;
                    $query = $isVenta ? VentaIvaCuentaUsada::findAll(['factura_venta_id' => $fila->id]) : CompraIvaCuentaUsada::findAll(['factura_compra_id' => $fila->id]);
                    foreach ($query as $q) {
                        if ($q->iva_cta_id != null) {
                            if ($q->ivaCta->iva->porcentaje == 5) {
                                $gravs5 += round((float)$q->monto / (1.05) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1), 2);
                                $imps5 += round(((float)$q->monto - ((float)$q->monto / (1.05))) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1), 2);
                                $total_5 += $gravs5 + $imps5;
                            } elseif ($q->ivaCta->iva->porcentaje == 10) {
                                $gravs10 += round((float)$q->monto / (1.10) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1), 2);
                                $imps10 += round(((float)$q->monto - ((float)$q->monto / (1.10))) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1), 2);
                                $total_10 += $gravs10 + $imps10;
                            }
                        } else
                            $excs += (float)$q->monto * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1);
                    }
                    if ($fila->estado == 'vigente') {
                        $cotizacion = ($fila instanceof Venta) ? $fila->valor_moneda : $fila->cotizacion;
                        if ($fila->moneda_id != null && $fila->moneda_id != 1 && $cotizacion) { // si no es guaranies
                            $ayer = date('Y-m-d', strtotime('-1 day', strtotime($fila->fecha_emision)));
                            $valor_moneda_factura = $model->valorizar == 'si' ? (float)$cotizacion : 1.0;
                            $gravs5 = (float)$gravs5 * $valor_moneda_factura;
                            $gravs10 = (float)$gravs10 * $valor_moneda_factura;
                            $imps5 = (float)$imps5 * $valor_moneda_factura;
                            $imps10 = (float)$imps10 * $valor_moneda_factura;
                            $excs = (float)$excs * $valor_moneda_factura;
                            $fila->total = (float)$fila->total * $valor_moneda_factura;
                            $total_10 = (float)$gravs10 + (float)$imps10;
                            $total_5 = (float)$gravs5 + (float)$imps5;
                        }
                    }
                    $total_gravs += (float)$gravs10 + (float)$gravs5;
                    $total_imps += (float)$imps10 + (float)$imps5;
                    $total_excs += (float)$excs;
                    $tt_gr5 += (float)$gravs5;
                    $tt_gr10 += (float)$gravs10;
                    $tt_im5 += (float)$imps5;
                    $tt_im10 += (float)$imps10;
                    $totals += (float)$fila->total;
                    $totals_10 += (float)$total_10;
                    $totals_5 += (float)$total_5;

                    if (!array_key_exists(1, $moneda_models))
                        $moneda_models[1] = Moneda::findOne(['id' => 1]);
                    $monedaModel = $moneda_models[1];

                    // acumular resumen por cuenta
                    $resumen_por_cuenta['gr10'] += round($gravs10, 2);
                    $resumen_por_cuenta['gr5'] += round($gravs5, 2);
                    $resumen_por_cuenta['iv10'] += round($imps10, 2);
                    $resumen_por_cuenta['iv5'] += round($imps5, 2);
                    $resumen_por_cuenta['exent'] += round($excs, 2);
                    $resumen_por_cuenta['total'] += round($fila->total, 2);
                    $resumen_por_cuenta['total_10'] += round($total_10, 2);
                    $resumen_por_cuenta['total_5'] += round($total_5, 2);

                    if (!array_key_exists($fila->tipo_documento_id, $tipodoc_models))
                        $tipodoc_models[$fila->tipo_documento_id] = $fila->tipoDocumento;

                    $tipoDoc = $tipodoc_models[$fila->tipo_documento_id];

                    $excelRow = [
                        'A' => ($isVenta ? $fila->getNroFacturaCompleto() : $fila->nro_factura),
                        'B' => $fecha,
                        'C' => (isset($fila->timbradoDetalle) ? $fila->timbradoDetalle->timbrado->nro_timbrado : ''),
                        'D' => (($fila->estado == 'vigente') ? $fila->entidad->razon_social : ''),
                        'E' => ($fila->estado == 'vigente') ? "{$fila->entidad->ruc}-{$fila->entidad->digito_verificador}" : '',
                        'F' => $gravs10,
                        'G' => $imps10,
                        'H' => $total_10,
                        'I' => $gravs5,
                        'J' => $imps5,
                        'K' => $total_5,
                        'L' => $excs,
                        'M' => $fila->total,
                        'N' => (($fila->estado == 'vigente') ? (($fila instanceof Venta) ? $fila->valor_moneda : $fila->cotizacion) : 0),
                        'O' => (($fila->estado == 'vigente') ? $cuentas_principales[$fila->id] : '-'),
                        'P' => (($fila->estado == 'vigente') ? $tipoDoc->nombre : (($fila->tipo_documento_id != '') ? $tipoDoc->nombre : '')),
                    ];

                    # rellenar las columnas de la N-esima fila (N-esima factura)
                    foreach ($excelRow as $col => $value) {
                        Yii::warning($total_10);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("$col$filas", $value, (($col >= 'F' && $col <= 'M') ? DataType::TYPE_NUMERIC : DataType::TYPE_STRING));
                    }
                }

                if ($model->resumen_plantilla == 'si') {
                    $cotizacion = ($fila instanceof Venta) ? $fila->valor_moneda : $fila->cotizacion;
                    $cotizacion = (float)$cotizacion;
                    if ($cotizacion == '' || $cotizacion == 0.0) $cotizacion = 1.1;

                    $filas++;
                    $objPHPExcel->getActiveSheet()->mergeCells("D$filas:E$filas");
                    $objPHPExcel->getActiveSheet()->mergeCells("F$filas:G$filas");
                    $objPHPExcel->getActiveSheet()->mergeCells("H$filas:I$filas");
                    $objPHPExcel->getActiveSheet()->mergeCells("J$filas:K$filas");
                    $objPHPExcel->getActiveSheet()->getStyle("D$filas:K$filas")->getFill()->setFillType(Fill::FILL_SOLID);
                    $objPHPExcel->getActiveSheet()->getStyle("D$filas:K$filas")->getFill()->setStartColor(new Color('FFFFDC'));
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("D$filas", 'PLANTILLAS', DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("F$filas", 'TOTAL 10%', DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("H$filas", 'TOTAL 5%', DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("J$filas", 'EXENTA', DataType::TYPE_STRING);

                    $filas++;
                    foreach ($query as $ivaCtaUsada) {
                        $objPHPExcel->getActiveSheet()->getStyle("D$filas:K$filas")->getFill()->setFillType(Fill::FILL_SOLID);
                        $objPHPExcel->getActiveSheet()->getStyle("D$filas:K$filas")->getFill()->setStartColor(new Color('e9f4ff'));
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("D$filas", $ivaCtaUsada->plantilla->nombre, DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("F$filas", ((isset($ivaCtaUsada->ivaCta) && $ivaCtaUsada->ivaCta->iva->porcentaje == 10) ? number_format(round($ivaCtaUsada->monto), 0, ',', '.') : (0)), DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("H$filas", ((isset($ivaCtaUsada->ivaCta) && $ivaCtaUsada->ivaCta->iva->porcentaje == 5) ? number_format(round($ivaCtaUsada->monto), 0, ',', '.') : (0)), DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("J$filas", (!isset($ivaCtaUsada->ivaCta) ? number_format(round($ivaCtaUsada->monto), 0, ',', '.') : (0)), DataType::TYPE_STRING);
                        $filas++;
                    }
                }
                $prefijo = $current_pref;
                $cta_principal = $current_cta_principal;
                $filas++;
            } //Fin facturas

            if ($model->resumen_plantilla == 'no' && $model->separar_visualmente == 'cuenta_principal' && $resumenPorCuenta) {
                $filas++;
                /*$resumen_por_cuenta['gr10'] = number_format($resumen_por_cuenta['gr10'], 0, ',', '.');
                    $resumen_por_cuenta['iv10'] = number_format($resumen_por_cuenta['iv10'], 0, ',', '.');
                    $resumen_por_cuenta['gr5'] = number_format($resumen_por_cuenta['gr5'], 0, ',', '.');
                    $resumen_por_cuenta['iv5'] = number_format($resumen_por_cuenta['iv5'], 0, ',', '.');
                    $resumen_por_cuenta['exent'] = number_format($resumen_por_cuenta['exent'], 0, ',', '.');
                    $resumen_por_cuenta['total'] = number_format($resumen_por_cuenta['total'], 0, ',', '.');*/

                $objPHPExcel->getActiveSheet()->setCellValueExplicit("E$filas", 'SUBTOTAL', DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getStyle("D$filas")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getBorders()->getTop()->setBorderStyle(Border::BORDER_DOTTED);
                $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_DOTTED);
                $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFill()->setFillType(Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFill()->setStartColor(new Color('ffff6d'));
                $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFont()->setBold(true);

                $map = ['F' => 'gr10', 'G' => 'iv10', 'I' => 'gr5', 'J' => 'iv5', 'L' => 'exent', 'M' => 'total',
                    'H' => 'total_10', 'K' => 'total_5'];
                foreach (['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'] as $col) {
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("$col$filas", $resumen_por_cuenta[$map[$col]], DataType::TYPE_NUMERIC);
                }

                $filas++;
            }
            ?>
            <!-- GENERACION DE ULTIMA FILA, SUMATORIA DE COLUMNAS RESPECTIVAS. -->
            <!-- Los totales son comunes a compra y venta. -->
            <?php
            $factor = ($documentoSetId == Compra::getTipodocSetNotaCredito()->id) ? -1 : 1;
            $array_resumen['gr10'] += (float)$tt_gr10 * $factor;
            $array_resumen['im10'] += (float)$tt_im10 * $factor;
            $array_resumen['gr5'] += (float)$tt_gr5 * $factor;
            $array_resumen['im5'] += (float)$tt_im5 * $factor;
            $array_resumen['ex'] += (float)$total_excs * $factor;
            $array_resumen['to'] += (float)$totals * $factor;
            $array_resumen['totals_10'] += (float)$totals_10 * $factor;
            $array_resumen['totals_5'] += (float)$totals_5 * $factor;

//                    $filas++;
//                    $objPHPExcel->getActiveSheet()->mergeCells("D$filas:E$filas");
//                    $objPHPExcel->getActiveSheet()->setCellValueExplicit("D$filas", 'Totales', DataType::TYPE_STRING);
//
//                    $filas++;
//                    $map = ['F' => 'gr10', 'G' => 'im10', 'H' => 'gr5', 'I' => 'im5', 'J' => 'ex', 'K' => 'to'];
//                    foreach (['F', 'G', 'H', 'I', 'J', 'K'] as $col) {
//                        $objPHPExcel->getActiveSheet()->setCellValueExplicit("$col$filas", $array_resumen[$map[$col]], DataType::TYPE_NUMERIC);
//                    }

            $array_resumen['gr10'] = $tt_gr10;
            $array_resumen['im10'] = $tt_im10;
            $array_resumen['gr5'] = $tt_gr5;
            $array_resumen['im5'] = $tt_im5;
            $array_resumen['ex'] = $total_excs;
            $array_resumen['to'] = $totals;
            $array_resumen['totals_10'] = $totals_10;
            $array_resumen['totals_5'] = $totals_5;
            $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFill()->setFillType(Fill::FILL_SOLID); //ffff6d
            $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFill()->setStartColor(new Color('d4ea6b'));
            $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle("A$filas:N$filas")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_DOTTED);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit("A$filas", 'Total Items', DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit("B$filas", $items, DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit("E$filas", 'Totales', DataType::TYPE_STRING);
            $map = ['F' => 'gr10', 'G' => 'im10', 'I' => 'gr5', 'J' => 'im5', 'L' => 'ex', 'M' => 'to',
                'H' => 'totals_10', 'K' => 'totals_5'];
            foreach (['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'] as $col) {
                $objPHPExcel->getActiveSheet()->setCellValueExplicit("$col$filas", $array_resumen[$map[$col]], DataType::TYPE_NUMERIC);
            }
            $filas++;
            $filas++;
        }

        $filas++;
        if (!empty($nro_facturas_libres)) {
            $filas++;
            $msg = [];
            foreach ($nro_facturas_libres as $nro_libre_timbrado) {
                $subsecuencias = [];
                foreach ($nro_libre_timbrado['cortes'] as $key => $corte) {
                    if ($key % 2 == 0) {
                        if ($nro_libre_timbrado['nros_libres'][$corte] != $nro_libre_timbrado['nros_libres'][$nro_libre_timbrado['cortes'][$key + 1]]) {
                            $del = str_pad($nro_libre_timbrado['nros_libres'][$corte], 7, '0', STR_PAD_LEFT);
                            $al = str_pad($nro_libre_timbrado['nros_libres'][$nro_libre_timbrado['cortes'][$key + 1]], 7, '0', STR_PAD_LEFT);
                            $subsecuencias[] = "del {$nro_libre_timbrado['prefijo']}-{$del} al {$nro_libre_timbrado['prefijo']}-{$al}";
                        } else {
                            $nro = str_pad($nro_libre_timbrado['nros_libres'][$corte], 7, '0', STR_PAD_LEFT);
                            $subsecuencias[] = "{$nro_libre_timbrado['prefijo']}-{$nro}";
                        }
                    }
                }
                $msg[] = ['timbrado' => $nro_libre_timbrado['timbrado'], 'subsecuencias' => implode(', ', $subsecuencias)];
            }

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $filas + 11, "Timbrado");
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $filas + 11, "Faltantes");
            foreach ($msg as $_ => $item) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $filas + 12 + $_, "Timbrado {$item['timbrado']}");
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $filas + 12 + $_, "{$item['subsecuencias']}");
            }
        }
    } catch (Exception $exception) {
        !isset($objPHPExcel) || $objPHPExcel->getActiveSheet()->setCellValueExplicit("A$filas", $exception, DataType::TYPE_STRING);
    }

    return isset($objPHPExcel) ? $objPHPExcel : null;
}

    /**
     * @param $data Venta[]
     * @return array
     */
    private function getNroFacturasLibres($data)
{
//        return [];

    $t_det_ids = [];
    $nro_facturas_libres = [];
    $t_det_id = null;

    foreach ($data as $venta) {
        if (!in_array($venta->timbrado_detalle_id, $t_det_ids)) {
            /** @var Venta[] $ventas_existentes */

            $t_det_ids[] = $venta->timbrado_detalle_id;

            $concat = "CONCAT((prefijo), ('-'), (nro_factura))";
            $all_nros = range($venta->timbradoDetalle->nro_inicio, $venta->timbradoDetalle->nro_fin);
            $ventas_existentes = Venta::find()
                ->where(['timbrado_detalle_id' => $venta->timbrado_detalle_id])
                ->andWhere(['!=', 'estado', 'faltante'])// una faltante es como una que no existe, por lo que su nro debe estar disponible para su uso.
                ->andWhere(['empresa_id' => \Yii::$app->session->get('core_empresa_actual')])
                ->andWhere(['periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])
                ->orderBy([$concat => SORT_ASC])
                ->all();

            $nros_usados = [];
            foreach ($ventas_existentes as $venta_existente) {
                $nros_usados[] = (int)$venta_existente->nro_factura;
            }

            $nros_libres = array_values(array_diff($all_nros, $nros_usados));

            $nros_libres_timbrado = [
                'timbrado' => $venta->timbradoDetalle->timbrado->nro_timbrado,
                'prefijo' => $venta->prefijo,
                'nros_libres' => $nros_libres,
                'cortes' => $this->getContigsSubsecsIndexes($nros_libres)
            ];

            foreach ($nro_facturas_libres as $key => $item) {
                if ($item['timbrado'] == $nros_libres_timbrado['timbrado'] && $item['prefijo'] == $nros_libres_timbrado['prefijo']) {
                    $data = $item['nros_libres'];
                    $data = array_merge($data, $nros_libres_timbrado['nros_libres']);
                    sort($data);
                    $item['nros_libres'] = $data;
                    $nro_facturas_libres[$key] = $item;
                }
            }

            $nro_facturas_libres[] = $nros_libres_timbrado;
        } else {
            continue;
        }
    }

    return $nro_facturas_libres;
}

    private function getContigsSubsecsIndexes(&$nros_libres)
{

    $indexes = [];

    foreach ($nros_libres as $i => $nro_libre) {
        $comienza = -1;
        $termina = -1;
        if (!in_array(($nro_libre - 1), $nros_libres)) {
            $comienza = $i;
            $termina = $comienza; // por darle un valor inicial, mas no tiene un significado radical.
            $scanner = $nro_libre + 1;

            while (in_array($scanner, $nros_libres)) {
                $termina = array_search($scanner, $nros_libres);
                $scanner++;
            }

            $indexes[] = $comienza;
            $indexes[] = $termina;
        }
    }

    return $indexes;
}

    private function getCuentasPrincipalesByFactura($data, $operacion)
{

    $cuentas_principales = [];
    foreach ($data as $key => $compra_o_vent) {
        /** Obtiene todos los ids de plantilla usados por la compra_o_vent $key-esima.
         *
         *  Retorna un array de la forma:
         *  $q_result = [
         *      0 => [
         *          'plantilla_id' => nº
         *      ],
         *      1 => [
         *          'plantilla_id' => nº
         *      ],
         *      ...
         *  ]
         */
        $factura_fk = $operacion == 'venta' ? "factura_venta_id" : "factura_compra_id";
        $query = new Query();
        $query->select(['plantilla_id'])->from($operacion == 'venta' ? VentaIvaCuentaUsada::tableName() : CompraIvaCuentaUsada::tableName())
            ->where([$factura_fk => $compra_o_vent->id])->groupBy(['plantilla_id']);
        $comando = $query->createCommand();
        $q_result = $comando->queryAll();

        /** @var  $plantilla_ids
         *
         *  $plantilla_ids tiene la forma:
         *  $plantilla_ids = [
         *      0 => nro,
         *      1 => nro,
         *      ...
         *  ];
         */
        $plantilla_ids = [];
        foreach ($q_result as $result) {
            $plantilla_ids[] = $result['plantilla_id'];
        }

        /** @var  $q_result
         *
         *  Tiene la forma:
         *  $q_result = [
         *      0 => [
         *          'nombre' => 'Venta de Mercaderias gravadas por 10%...'
         *      ],
         *      1 => [...],
         *      ...
         *  ];
         */

        $query = new Query();
        $query->select(['cuenta.nombre'])->from(($operacion == 'venta' ? DetalleVenta::tableName() : DetalleCompra::tableName()) . ' AS facturad')
            ->leftJoin('cont_plan_cuenta AS cuenta', 'facturad.plan_cuenta_id = cuenta.id')
            ->leftJoin(PlantillaCompraventaDetalle::tableName() . ' AS plantillad', 'facturad.plan_cuenta_id = plantillad.p_c_gravada_id')
            ->where(['facturad.' . $factura_fk => $compra_o_vent->id])
            ->andWhere(['IN', 'plantillad.plantilla_id', $plantilla_ids])
            ->andWhere(['plantillad.cta_principal' => 'si']);
        $comando = $query->createCommand();
        $q_result = $comando->queryAll();

        /** @var  $query_result
         *
         *  Cambia a la forma:
         *  $q_result = [
         *      0 => 'Venta de Mercaderias gravadas por 10%...',
         *      1 => " ... ",
         *      ...
         *  ];
         */
        foreach ($q_result as $key2 => $value) {
            $q_result[$key2] = $q_result[$key2]['nombre'];
        }

        /** $cuentas_principales es un array de textos donde cada texto es la concatenacion de nombres de
         *      las cuentas contables (principales segun plantilla) mediante coma y espacio
         */
        $cuentas_principales[] = implode(', ', $q_result) . '.';
    }
    return $cuentas_principales;
}

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /**
     * @return mixed|string
     * @throws \yii\base\InvalidConfigException
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function actionBalance()
{
    $model = new ReporteBalance();
    $model->mostrar_codigo_cuenta = 'no';
    $periodoAnho = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'))->anho;
    $model->fecha_desde = $periodoAnho . '-01-01';
    $model->fecha_hasta = $periodoAnho . '-12-31';

    if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
        //Obteniendo datos
        //Cuentas de Asientos del año, periodo, rango de fechas y nivel
        $data = $this->getCuentasFromAsientos($model->fecha_desde, $model->fecha_hasta);

        if ($model->tipo_doc == 'xls') {
            // Crear objetoExcel.
            /** Error reporting */
            error_reporting(E_ALL);
            ini_set('display_errors', true);
            ini_set('display_startup_errors', true);
            define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
            date_default_timezone_set('America/Asuncion');

            /** Create a new Spreadsheet Object **/
            $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/reporte/balance/impositivo/xls/';
            $objPHPExcel = IOFactory::load($carpeta . 'balance_template.xlsx');

            $empresa = Empresa::findOne(\Yii::$app->session->get('core_empresa_actual'));
            if ($empresa->tipo == 'juridica') {
                $razonSocial = $empresa->razon_social;
            } else {
                $razonSocial = $empresa->primer_apellido . ' ' . $empresa->segundo_apellido . ', ' . $empresa->primer_nombre . ' ' . $empresa->segundo_nombre;
            }
            $ruc = $empresa->ruc . '-' . $empresa->digito_verificador;
            $desde = Yii::$app->formatter->asDate($model->fecha_desde, 'dd-MM-y');
            $hasta = Yii::$app->formatter->asDate($model->fecha_hasta, 'dd-MM-y');
            $currentDate = strftime('%d-%m-%Y a las %H:%M:%S', time());

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, strtoupper($razonSocial));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 4, strtoupper($ruc));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 4, $desde);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, 4, $hasta);

            $totalDebe = 0;
            $totalHaber = 0;
            $totalDeudor = 0;
            $totalAcreedor = 0;
            $totalInventarioActivo = 0;
            $totalInventarioPasivo = 0;
            $totalResultadoDebe = 0;
            $totalResultadoHaber = 0;
            $fila = 10;
            foreach ($data as $item) {
                $totalDebe += $item['total_debe'];
                $totalHaber += $item['total_haber'];
                $totalDeudor += $item['total_deudor'];
                $totalAcreedor += $item['total_acreedor'];
                $totalInventarioActivo += $item['total_inventario_activo'];
                $totalInventarioPasivo += $item['total_inventario_pasivo'];
                $totalResultadoDebe += $item['total_resultado_debe'];
                $totalResultadoHaber += $item['total_resultado_haber'];

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $fila, strtoupper($item['nombre'] . (($model->mostrar_codigo_cuenta == 'si') ? ' - ' . $item['cod_completo'] : '')));
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, $item['total_debe']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $fila, $item['total_haber']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $fila, $item['total_deudor']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $fila, $item['total_acreedor']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $fila, $item['total_inventario_activo']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $fila, $item['total_inventario_pasivo']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $fila, $item['total_resultado_debe']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $fila, $item['total_resultado_haber']);
                $fila++;
            }

            //Perdida y Ganancia
            $complementoDebe = 0;
            $complementoHaber = 0;
            $diff1 = $totalDebe - $totalHaber;
            if ($diff1 < 0) {
                $complementoDebe = -1 * $diff1;
                $totalDebe += $complementoDebe;
            } else {
                $complementoHaber = -1 * $diff1;
                $totalHaber += $complementoHaber;
            }

            $complementoDeudor = 0;
            $complementoAcreedor = 0;
            $diff2 = $totalDeudor - $totalAcreedor;
            if ($diff1 < 0) {
                $complementoDeudor = -1 * $diff2;
                $totalDeudor += $complementoDeudor;
            } else {
                $complementoAcreedor = -1 * $diff2;
                $totalAcreedor += $complementoAcreedor;
            }

            $complementoInventarioActivo = 0;
            $complementoInventarioPasivo = 0;
            $diff3 = $totalInventarioActivo - $totalInventarioPasivo;
            if ($diff1 < 0) {
                $complementoInventarioActivo = -1 * $diff3;
                $totalInventarioActivo += $complementoInventarioActivo;
            } else {
                $complementoInventarioPasivo = -1 * $diff3;
                $totalInventarioPasivo += $complementoInventarioPasivo;
            }

            $complementoResultadoDebe = 0;
            $complementoResultadoHaber = 0;
            $diff4 = $totalResultadoDebe - $totalResultadoHaber;
            if ($diff1 < 0) {
                $complementoResultadoDebe = $diff4;
                $totalResultadoDebe += $complementoResultadoDebe;
            } else {
                $complementoResultadoHaber = $diff4;
                $totalResultadoHaber += $complementoResultadoHaber;
            }
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $fila, strtoupper('PERDIDAS Y GANANCIAS'));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, $complementoDebe);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $fila, $complementoHaber);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $fila, $complementoDeudor);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $fila, $complementoAcreedor);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $fila, $complementoInventarioActivo);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $fila, $complementoInventarioPasivo);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $fila, $complementoResultadoDebe);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $fila, $complementoResultadoHaber);

            $fila++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $fila, strtoupper('TOTAL'));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $fila, $totalDebe);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $fila, $totalHaber);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $fila, $totalDeudor);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $fila, $totalAcreedor);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $fila, $totalInventarioActivo);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $fila, $totalInventarioPasivo);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $fila, $totalResultadoDebe);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $fila, $totalResultadoHaber);

            $objPHPExcel->getActiveSheet()->getStyle('B10:I' . $fila)->getNumberFormat()->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle('A' . $fila . ':I' . $fila)->getFont()->setBold(true);

            //Versión excel
            $carpeta = Yii::$app->basePath . '/web/uploads/';
            if (!is_dir($carpeta)) {
                if (!@mkdir($carpeta, 0777, true)) {
                    throw new Exception('No se puede crear carpeta.');
                }
            }
            Yii::$app->session->set('carpeta', $carpeta);
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
            $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

            // Exportar en navegador
            $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
            $OS = strtolower(empty($_SERVER['HTTP_USER_AGENT']) ? '' : $_SERVER['HTTP_USER_AGENT']);
            if (!$OS)
                $OS_IS_MAC = true;
            else
                $OS_IS_MAC = (strpos($OS, 'mac') !== false)
                    || (strpos($OS, 'macintosh') !== false)
                    || (strpos($OS, 'iphone') !== false)
                    || (strpos($OS, 'ipad') !== false);

            if ($OS_IS_MAC) {
//            if(!file_exists($target_relative)){
//                copy($filename,$target_relative);
//                sleep(1);
//            }
//            header('Location: ' . $target);
            } else {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . 'balance_impositivo_' . $currentDate . '.' . $fi['extension']);
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx"));
                ob_clean();
                flush();
                readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
            }
            //borramos el fichero temporal
            unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
        } else {
            $path = 'modules\contabilidad\views\reporte\balance\impositivo\pdf';
            $path = str_replace('\\', '/', $path);
            $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/pdf.css");
            $contenido = Yii::$app->view->renderFile("@app/" . $path . "/pdf.php", ['data' => $data, 'model' => $model]);
            $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/header.php", ['model' => $model, 'fecha_desde' => $model->fecha_desde, 'fecha_hasta' => $model->fecha_hasta]);

            $pdf = new Pdf([
                'mode' => Pdf::DEST_BROWSER,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'format' => Pdf::FORMAT_FOLIO,
                'marginTop' => 62.5,
                'marginLeft' => 7,
                'marginBottom' => 10,
                'marginRight' => 7,
                'content' => $contenido,
                'cssInline' => $cssInline,
//                    'destination' => Pdf::DEST_BROWSER,
                'filename' => "Reporte Balance Impositivo.pdf",
                'options' => [
                    'title' => "",
                    'subject' => "",
                ],
                'methods' => [
                    'SetHeader' => [$encabezado],
                ]
            ]);
            setlocale(LC_TIME, 'es_ES.UTF-8');
            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');
            return $pdf->render();
        }
    }

    retorno:;
    return $this->render('balance/impositivo/reporte', [
        'model' => $model,
    ]);
}

    protected function getCuentasFromAsientos($fecha_desde, $fecha_hasta)
{
    $empresa_id = Yii::$app->session->get('core_empresa_actual');
    $periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

    $cuentasTotal = AsientoDetalle::find()->alias('detalle')
        ->select([
            'min(cuenta.id) as id',
            'min(cuenta.nombre) as nombre',
            'min(cuenta.cod_completo) as cod_completo',
            'sum(detalle.monto_debe) as total_debe',
            'sum(abs(detalle.monto_haber)) as total_haber',
        ])
        ->leftJoin('cont_asiento as asiento', 'asiento.id = detalle.asiento_id')
        ->leftJoin('cont_plan_cuenta as cuenta', 'cuenta.id = detalle.cuenta_id')
        ->where(['asiento.empresa_id' => $empresa_id])
        ->andWhere(['asiento.periodo_contable_id' => $periodo_contable_id])
        ->andWhere(['>=', 'asiento.fecha', $fecha_desde])
        ->andWhere(['<=', 'asiento.fecha', $fecha_hasta])
        ->groupBy('detalle.cuenta_id')
        ->orderBy('cuenta.cod_ordenable')
        ->asArray()
        ->all();

    $cuentasFinal = [];

    foreach ($cuentasTotal as $cuenta) {
        if (isset($cuentasFinal[$cuenta['id']]))
            $cuentasFinal[$cuenta['id']] = [
                'nombre' => $cuenta['nombre'],
                'cod_completo' => $cuenta['cod_completo'],
                'total_debe' => $cuentasFinal[$cuenta['id']]['total_debe'] + $cuenta['total_debe'],
                'total_haber' => $cuentasFinal[$cuenta['id']]['total_haber'] + $cuenta['total_haber']
            ];
        else
            $cuentasFinal[$cuenta['id']] = [
                'nombre' => $cuenta['nombre'],
                'cod_completo' => $cuenta['cod_completo'],
                'total_debe' => $cuenta['total_debe'],
                'total_haber' => $cuenta['total_haber']
            ];

        if (isset($cuentasFinal[$cuenta['id']])) {
            $diferencia = $cuentasFinal[$cuenta['id']]['total_debe'] - $cuentasFinal[$cuenta['id']]['total_haber'];
            $primerCod = explode('.', $cuentasFinal[$cuenta['id']]['cod_completo'])[0];
            $cuentasFinal[$cuenta['id']]['total_inventario_activo'] = 0;
            $cuentasFinal[$cuenta['id']]['total_inventario_pasivo'] = 0;
            $cuentasFinal[$cuenta['id']]['total_resultado_debe'] = 0;
            $cuentasFinal[$cuenta['id']]['total_resultado_haber'] = 0;

            if ($diferencia > 0) {
                if (in_array((int)$primerCod, PlanCuenta::INIT_COD_CUENTA_HABER)) {
                    $cuentasFinal[$cuenta['id']]['total_deudor'] = 0;
                    $cuentasFinal[$cuenta['id']]['total_acreedor'] = -$diferencia;  # debe figurar forzadamente en el haber pero con signo negativo.

                    if ((int)$primerCod == 1 || (int)$primerCod == 2 || (int)$primerCod == 3)
                        $cuentasFinal[$cuenta['id']]['total_inventario_pasivo'] = -$diferencia;
                    else
                        $cuentasFinal[$cuenta['id']]['total_resultado_haber'] = $diferencia;  #Perdidas y Ganancias no tiene posicion fija, depende del $diferecia.
                } else {
                    $cuentasFinal[$cuenta['id']]['total_deudor'] = $diferencia;
                    $cuentasFinal[$cuenta['id']]['total_acreedor'] = 0;

                    if ((int)$primerCod == 1 || (int)$primerCod == 2 || (int)$primerCod == 3)
                        $cuentasFinal[$cuenta['id']]['total_inventario_activo'] = $diferencia;
                    else
                        $cuentasFinal[$cuenta['id']]['total_resultado_debe'] = $diferencia;  #Perdidas y Ganancias no tiene posicion fija, depende del $diferecia.
                }

//                    if ((int)$primerCod == 1 || (int)$primerCod == 2 || (int)$primerCod == 3 || (int)$primerCod == 4)
//                    if ((int)$primerCod == 1 || (int)$primerCod == 2 || (int)$primerCod == 3)
//                        $cuentasFinal[$cuenta['id']]['total_inventario_activo'] = $diferencia;
//                    else
//                        $cuentasFinal[$cuenta['id']]['total_resultado_debe'] = $diferencia;
            } else {
                {
                    if (in_array((int)$primerCod, PlanCuenta::INIT_COD_CUENTA_DEBE)) {
                        $cuentasFinal[$cuenta['id']]['total_deudor'] = $diferencia;  # debe figurar forzadamente en el debe pero con signo negativo.
                        $cuentasFinal[$cuenta['id']]['total_acreedor'] = 0;

                        if ((int)$primerCod == 1 || (int)$primerCod == 2 || (int)$primerCod == 3)
                            $cuentasFinal[$cuenta['id']]['total_inventario_activo'] = $diferencia;
                        else
                            $cuentasFinal[$cuenta['id']]['total_resultado_debe'] = -$diferencia;  #Perdidas y Ganancias no tiene posicion fija, depende del $diferecia.
                    } else {
                        $cuentasFinal[$cuenta['id']]['total_deudor'] = 0;
                        $cuentasFinal[$cuenta['id']]['total_acreedor'] = -1 * $diferencia;

                        if ((int)$primerCod == 1 || (int)$primerCod == 2 || (int)$primerCod == 3)
                            $cuentasFinal[$cuenta['id']]['total_inventario_pasivo'] = -1 * $diferencia;
                        else
                            $cuentasFinal[$cuenta['id']]['total_resultado_haber'] = -1 * $diferencia;  #Perdidas y Ganancias no tiene posicion fija, depende del $diferecia.
                    }
                }

//                    if ((int)$primerCod == 1 || (int)$primerCod == 2 || (int)$primerCod == 3 || (int)$primerCod == 4)
//                    if ((int)$primerCod == 1 || (int)$primerCod == 2 || (int)$primerCod == 3)
//                        $cuentasFinal[$cuenta['id']]['total_inventario_pasivo'] = -1 * $diferencia;
//                    else
//                        $cuentasFinal[$cuenta['id']]['total_resultado_haber'] = -1 * $diferencia;
            }
        }
    }

    return $cuentasFinal;
}

    protected function getDataEstadoCuentaCompleto($model, $desde, $hasta, $cliente)
{
    if (!$cliente) {
        $facturas = Compra::find()
            ->alias('compra')
            ->select([
                'entidad.id as entidad_id',
                'entidad.razon_social as razon_social',
                'entidad.ruc as ruc',
                'compra.fecha_emision fecha_emision',
                'compra.nro_factura nro_factura',
                '(compra.total * IF(compra.cotizacion is null or compra.cotizacion = 0, 1, compra.cotizacion)) total',
                'compra.entidad_id',
                'compra.id as factura_id',
                'compra.tipo compra_tipo'
            ])
            ->joinWith('entidad as entidad')
//                ->where(['or', ['condicion' => 'credito'], ['and', ['condicion' => 'contado'], ['<>', 'compra.tipo', 'factura']]])
            ->andWhere(['tipo' => 'factura'])//Se obtienen solamente facturas
            ->andWhere(['condicion' => 'credito'])
            ->andWhere(['compra.empresa_id' => Yii::$app->session->get(SessionVariables::empresa_actual)])
            ->andWhere(['compra.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->andWhere(['>', 'fecha_emision', $desde])
            ->andWhere(['<', 'fecha_emision', $hasta])
            ->andFilterWhere(['IN', 'compra.entidad_id', $model->entidad_id])
            ->groupBy(['compra.id'])
            ->orderBy(['entidad.razon_social' => SORT_ASC, 'compra.fecha_emision' => SORT_ASC])
            ->asArray()->all();

        $factura_ids = [];
        foreach ($facturas as $factura)
            $factura_ids[] = $factura['factura_id'];

        $notas = [];
        $recibos = [];
        $retenciones = [];
        foreach ($factura_ids as $factura_id) {
            $recibos[$factura_id] = ReciboDetalle::find()->alias('reciboDetalle')
                ->select([
                    '(reciboDetalle.factura_compra_id) as factura_id',
                    '(recibo.fecha) as fecha_recibo',
                    '(recibo.numero) as numero_recibo',
                    '(reciboDetalle.recibo_id) as recibo_id',
                    '(reciboDetalle.monto * IF(reciboDetalle.cotizacion is null or reciboDetalle.cotizacion = 0, 1, reciboDetalle.cotizacion)) total_recibo',
                ])
                ->joinWith('recibo as recibo')
                ->where(['reciboDetalle.factura_compra_id' => $factura_id])
                ->asArray()->all();

            $notas[$factura_id] = Compra::find()->alias('nota')
                ->select([
                    'nota.id as nota_id',
                    'nota.factura_compra_id as factura_id',
                    'nota.total',
                    'nota.nro_factura as nro_nota',
                ])
                ->where(['=', 'factura_compra_id', $factura_id])
                ->asArray()->all();

            $retenciones[$factura_id] = Retencion::find()->alias('retencion')
                ->select([
                    'retencion.nro_retencion',
                    'retencion.id as retencion_id',
                    'retencion.factura_compra_id as factura_id',
                    'retencion.total as total_retencion'
                ])
                ->where(['=', 'factura_compra_id', $factura_id])
                ->asArray()->all();
        }

        #notas sin factura
        $TDSNotaCredito = Compra::getTipodocSetNotaCredito();
        $TDIds = [];
        foreach (TipoDocumento::findAll(['tipo_documento_set_id' => $TDSNotaCredito->id]) as $tdoc)
            $TDIds[] = $tdoc->id;
        $notasSinFactura = Compra::find()
            ->alias('compra')
            ->select([
                'entidad.id as entidad_id',
                'entidad.razon_social as razon_social',
                'entidad.ruc as ruc',
                'compra.fecha_emision fecha_emision',
                'compra.nro_factura nro_factura',
                '(compra.total * IF(compra.cotizacion is null or compra.cotizacion = 0, 1, compra.cotizacion)) total',
                'compra.entidad_id',
                'compra.id as factura_id',
                'compra.tipo compra_tipo'
            ])
            ->joinWith('entidad as entidad')
            ->where(['IN', 'compra.tipo_documento_id', $TDIds])
            ->andWhere(['IS', 'compra.factura_compra_id', null])
            ->andWhere(['compra.empresa_id' => Yii::$app->session->get(SessionVariables::empresa_actual)])
            ->andWhere(['compra.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->andWhere(['>', 'fecha_emision', $desde])
            ->andWhere(['<', 'fecha_emision', $hasta])
            ->andFilterWhere(['IN', 'compra.entidad_id', $model->entidad_id])
            ->groupBy(['compra.id'])
            ->orderBy(['entidad.razon_social' => SORT_ASC, 'compra.fecha_emision' => SORT_ASC])
            ->asArray()->all();

        #resultado
        $resultQuery = [
            'facturas' => $facturas,
            'recibos' => $recibos,
            'notas' => $notas,
            'notasSFactura' => $notasSinFactura,
            'retenciones' => $retenciones
        ];
    } else {
        $facturas = Venta::find()
            ->alias('venta')
            ->select([
                'entidad.id as entidad_id',
                'entidad.razon_social as razon_social',
                'entidad.ruc as ruc',
                'venta.fecha_emision fecha_emision',
                "CONCAT(venta.prefijo, '-', venta.nro_factura) AS nro_factura",
                '(venta.total * IF(venta.valor_moneda is null or venta.valor_moneda = 0, 1, venta.valor_moneda)) total',
                'venta.entidad_id',
                'venta.id as factura_id',
                'venta.tipo venta_tipo'
            ])
            ->joinWith('entidad as entidad')
            ->andWhere(['tipo' => 'factura'])//Se obtienen solamente facturas
            ->andWhere(['condicion' => 'credito'])
            ->andWhere(['venta.empresa_id' => Yii::$app->session->get(SessionVariables::empresa_actual)])
            ->andWhere(['venta.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->andWhere(['>', 'fecha_emision', $desde])
            ->andWhere(['<', 'fecha_emision', $hasta])
            ->andWhere(['venta.estado' => 'vigente'])
            ->andFilterWhere(['IN', 'venta.entidad_id', $model->entidad_id])
            ->groupBy(['venta.id'])
            ->orderBy(['entidad.razon_social' => SORT_ASC, 'venta.fecha_emision' => SORT_ASC])
            ->asArray()->all();

        $factura_ids = [];
        foreach ($facturas as $factura)
            $factura_ids[] = $factura['factura_id'];

        $notas = [];
        $recibos = [];
        $retenciones = [];
        foreach ($factura_ids as $factura_id) {
            $recibos[$factura_id] = ReciboDetalle::find()->alias('reciboDetalle')
                ->select([
                    '(reciboDetalle.factura_venta_id) as factura_id',
                    '(recibo.fecha) as fecha_recibo',
                    '(recibo.numero) as numero_recibo',
                    '(reciboDetalle.recibo_id) as recibo_id',
                    '(reciboDetalle.monto * IF(reciboDetalle.cotizacion is null or reciboDetalle.cotizacion = 0, 1, reciboDetalle.cotizacion)) total_recibo',
                ])
                ->joinWith('recibo as recibo')
                ->where(['reciboDetalle.factura_venta_id' => $factura_id])
                ->asArray()->all();

            $notas[$factura_id] = Venta::find()->alias('nota')
                ->select([
                    'nota.id as nota_id',
                    'nota.factura_venta_id as factura_id',
                    'nota.total',
                    'nota.nro_factura as nro_nota',
                ])
                ->where(['=', 'factura_venta_id', $factura_id])
                ->asArray()->all();

            $retenciones[$factura_id] = Retencion::find()->alias('retencion')
                ->select([
                    'retencion.nro_retencion',
                    'retencion.id as retencion_id',
                    'retencion.factura_venta_id as factura_id',
                    'retencion.total as total_retencion'
                ])
                ->where(['=', 'factura_venta_id', $factura_id])
                ->asArray()->all();
        }

        #notas sin factura
        $TDSNotaCredito = Venta::getTipodocSetNotaCredito();
        $TDIds = [];
        foreach (TipoDocumento::findAll(['tipo_documento_set_id' => $TDSNotaCredito->id]) as $tdoc)
            $TDIds[] = $tdoc->id;
        $notasSinFactura = Venta::find()
            ->alias('venta')
            ->select([
                'entidad.id as entidad_id',
                'entidad.razon_social as razon_social',
                'entidad.ruc as ruc',
                'venta.fecha_emision fecha_emision',
                "CONCAT(venta.prefijo, '-', venta.nro_factura) AS nro_factura",
                '(venta.total * IF(venta.valor_moneda is null or venta.valor_moneda = 0, 1, venta.valor_moneda)) total',
                'venta.entidad_id',
                'venta.id as factura_id',
                'venta.tipo venta_tipo'
            ])
            ->joinWith('entidad as entidad')
            ->where(['IN', 'venta.tipo_documento_id', $TDIds])
            ->andWhere(['IS', 'venta.factura_venta_id', null])
            ->andWhere(['venta.empresa_id' => Yii::$app->session->get(SessionVariables::empresa_actual)])
            ->andWhere(['venta.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->andWhere(['>', 'fecha_emision', $desde])
            ->andWhere(['<', 'fecha_emision', $hasta])
            ->andFilterWhere(['IN', 'venta.entidad_id', $model->entidad_id])
            ->groupBy(['venta.id'])
            ->orderBy(['entidad.razon_social' => SORT_ASC, 'venta.fecha_emision' => SORT_ASC])
            ->asArray()->all();

        #resultado
        $resultQuery = [
            'facturas' => $facturas,
            'recibos' => $recibos,
            'notas' => $notas,
            'notasSFactura' => $notasSinFactura,
            'retenciones' => $retenciones
        ];
    }
    foreach ($resultQuery['recibos'] as $id => $_) {
        unset($resultQuery['recibos'][$id]['recibo']);
    }
    foreach ($resultQuery['facturas'] as $id => $_) {
        unset($resultQuery['facturas'][$id]['entidad']);
    }

    return $resultQuery;
}

    /**
     * @return mixed|string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionEstadoCuentaProveedor()
{
    $model = new ReporteEstadoCuenta();
    $model->tipo = 'completo';
    $model->paper_size = Pdf::FORMAT_A4;
    $model->orientation = Pdf::ORIENT_PORTRAIT;
    $periodoAnho = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'))->anho;
    $model->fecha_rango = '01-01-' . $periodoAnho . ' - ' . '31-12-' . $periodoAnho;
    ini_set('pcre.backtrack_limit', 9999999000);
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes


    if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
        try {
            $model->separarEnDesdeHasta();

            $path = 'modules\contabilidad\views\reporte\estado-cuenta-proveedor\pdf';
            $path = str_replace('\\', '/', $path);

            $data = $this->getDataEstadoCuentaCompleto($model, $model->fecha_desde, $model->fecha_hasta, false);

            //Completo
            if ($model->tipo == 'completo') {
                $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/completo/pdf.css");
                $contenido = Yii::$app->view->renderFile("@app/" . $path . "/completo/pdf.php", ['data' => $data]);
                $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/completo/header.php", ['model' => $model]);
                $marginTop = 65;
            } else { //Simplificado
                $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/simplificado/pdf.css");
                $contenido = Yii::$app->view->renderFile("@app/" . $path . "/simplificado/pdf.php", ['data' => $data]);
                $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/simplificado/header.php", ['model' => $model]);
                $marginTop = 42;
            }

            $pdf = new Pdf([
                'mode' => Pdf::DEST_BROWSER,
                'orientation' => $model->orientation,
                'format' => $model->paper_size,
                'marginTop' => $marginTop,
                'marginLeft' => 7,
                'marginBottom' => 13,
                'marginRight' => 7,
                'content' => $contenido,
                'cssInline' => $cssInline,
                'filename' => "Reporte Estado de Cuenta " . ucfirst($model->tipo) . ".pdf",
                'options' => [
                    'title' => "",
                    'subject' => "",
                    'defaultheaderline' => 0
                ],
                'methods' => [
                    'SetHeader' => [$encabezado],
                    'SetFooter' => ['| Página {PAGENO} de {nb} |']
                ]
            ]);

//            setlocale(LC_TIME, 'es_ES.UTF-8');
//            Yii::$app->response->format = Response::FORMAT_RAW;
//            Yii::$app->response->headers->add('Content-Type', 'application/pdf');
            return $pdf->render();
        } catch (Exception $exception) {
            throw $exception;
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }
    }

    retorno:;
    return $this->render('estado-cuenta-proveedor/reporte', [
        'model' => $model,
    ]);
}

    /**
     * @return mixed|string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionEstadoCuentaCliente()
{
    $model = new ReporteEstadoCuenta();
    $model->tipo = 'completo';
    $model->paper_size = Pdf::FORMAT_A4;
    $model->orientation = Pdf::ORIENT_PORTRAIT;
    $periodoAnho = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'))->anho;
    $model->fecha_rango = '01-01-' . $periodoAnho . ' - ' . '31-12-' . $periodoAnho;

    if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
        $model->separarEnDesdeHasta();

        $path = 'modules\contabilidad\views\reporte\estado-cuenta-cliente\pdf';
        $path = str_replace('\\', '/', $path);

        $data = $this->getDataEstadoCuentaCompleto($model, $model->fecha_desde, $model->fecha_hasta, true);

        //Completo
        if ($model->tipo == 'completo') {
            $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/completo/pdf.css");
            $contenido = Yii::$app->view->renderFile("@app/" . $path . "/completo/pdf.php", ['data' => $data]);
            $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/completo/header.php", ['model' => $model]);
            $marginTop = 65;
        } else { //Simplificado
            $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/simplificado/pdf.css");
            $contenido = Yii::$app->view->renderFile("@app/" . $path . "/simplificado/pdf.php", ['data' => $data]);
            $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/simplificado/header.php", ['model' => $model]);
            $marginTop = 42;
        }

        $pdf = new Pdf([
            'mode' => Pdf::DEST_BROWSER,
            'orientation' => $model->orientation,
            'format' => $model->paper_size,
            'marginTop' => $marginTop,
            'marginLeft' => 7,
            'marginBottom' => 13,
            'marginRight' => 7,
            'content' => $contenido,
            'cssInline' => $cssInline,
            'filename' => "Reporte Estado de Cuenta " . ucfirst($model->tipo) . ".pdf",
            'options' => [
                'title' => "",
                'subject' => "",
                'defaultheaderline' => 0
            ],
            'methods' => [
                'SetHeader' => [$encabezado],
                'SetFooter' => ['| Página {PAGENO} de {nb} |']
            ]
        ]);

        setlocale(LC_TIME, 'es_ES.UTF-8');
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    retorno:;
    return $this->render('estado-cuenta-cliente/reporte', [
        'model' => $model,
    ]);
}

    /** <<<<<<<<<<<<<<<<<< REPORTE POR CUENTAS Y POR MESES >>>>>>>>>>>>>>>>>>> */

    public function actionReporteLibroPorCuentas($operacion)
{
    $a = microtime(true);
    $model = new Reporte();
    $model->resumido = 'no';
    $model->moneda = 0; //todos.

    if ($model->load(Yii::$app->request->post())) {
        try {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 300);//5 min
            ini_set('pcre.backtrack_limit', 10000000);
            if ($model->resumido == 'si') $model->resumido = true;
            else $model->resumido = false;

            // Procesar datos del post
            $id_desde = $model->cod_desde;
            $id_hasta = $model->cod_hasta;
            $desde = '';
            $hasta = '';
            if ($model->fecha_rango != '') {
                $rango = explode(' - ', $model->fecha_rango);
                $desde = implode('-', array_reverse(explode('-', $rango[0])));
                $hasta = implode('-', array_reverse(explode('-', $rango[1])));
            }

            // Generar array de datos
            $b = microtime(true);
            $data = self::generateReporteByCuenta(
                $operacion, $id_desde, $id_hasta, $desde, $hasta, $model);
            if (!sizeof($data))
                throw new Exception("No se han encontrado resultados.");

            $arrayDataProvider = $data[0];
            $facturas_id = $data[1];
            $fechas = $data[2];
            $c = microtime(true);

            /** ========================= TEST DE IMPRESION ========================== */
//                foreach ($arrayDataProvider as $detalle) {
//                    print_r($detalle);
//                    echo "<br/>";
//                    echo "<br/>";
//                }
//                exit();
            /** ========================= TEST DE IMPRESION ========================== */

            // Crear archivos para exportar.
            if ($model->tipo_doc == 'xls') {
                $encabezado = [
                    'tipoLibro' => ucfirst($operacion),
                    'id_desde' => ($id_desde != '') ? $id_desde : $facturas_id[0],
                    'id_hasta' => ($id_hasta != '') ? $id_hasta : $facturas_id[sizeof($facturas_id) - 1],
                    'desde' => ($model->fecha_rango != '') ? $rango[0] : implode('-', array_reverse(explode('-', $fechas[0]))),
                    'hasta' => ($model->fecha_rango != '') ? $rango[1] : implode('-', array_reverse(explode('-', $fechas[sizeof($fechas) - 1]))),
                ];

                // Crear objeto excel
                $path = '/modules/contabilidad/views/reporte/libro-por-cuenta/';
                $name = (!$model->resumido) ? 'template.xls' : 'template_resumido.xls';
                $objPHPExcel = ExcelHelpers::generatePHPExcelObject($path, $name);

                // Rellenar cabecera del excel
                $empresa = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')]);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, $encabezado['desde']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, $encabezado['hasta']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, $encabezado['id_desde']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, $encabezado['id_hasta']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow((!$model->resumido) ? 10 : 7, 2, $empresa->razon_social);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow((!$model->resumido) ? 10 : 7, 3, "{$empresa->ruc}-{$empresa->digito_verificador}");

                // Rellenar cuerpo excel
                self::createExcel($objPHPExcel, $arrayDataProvider, $model->resumido);

                // Exportar
                $name = 'Reporte de ' . ucfirst($operacion) . ' por cuenta' . ($model->resumido ? ' - resumido ' : ' ');
                ExcelHelpers::exportToBrowser($objPHPExcel, $name);
            } else {
                $d = microtime(true);
                $path = 'modules\contabilidad\views\reporte\libro-por-cuenta';
                $path = str_replace('\\', '/', $path);
                $cssInline = Yii::$app->view->renderFile("@app/" . $path . ((!$model->resumido) ? "/reporte.css" : "/reporte_resumido.css"));
                $contenido = Yii::$app->view->renderFile("@app/" . $path . ((!$model->resumido) ? "/reporte.php" : "/reporte_resumido.php"), [
                    'dataProvider' => $arrayDataProvider,
                ]);
                $encabezado = Yii::$app->view->renderFile("@app/" . $path . ((!$model->resumido) ? "/reporte_header.php" : "/reporte_header_resumido.php"), [
                    'tipoLibro' => ucfirst($operacion),
                    'id_desde' => ($id_desde != '') ? $id_desde : $facturas_id[0],
                    'id_hasta' => ($id_hasta != '') ? $id_hasta : $facturas_id[sizeof($facturas_id) - 1],
                    'desde' => ($model->fecha_rango != '') ? $rango[0] : implode('-', array_reverse(explode('-', $fechas[0]))),
                    'hasta' => ($model->fecha_rango != '') ? $rango[1] : implode('-', array_reverse(explode('-', $fechas[sizeof($fechas) - 1]))),
                ]);
                $pie = Yii::$app->view->renderFile("@app/" . $path . "/reporte_pie.php", []);
                $op = ucfirst($operacion);

                // http response
                $response = Yii::$app->response;
                $response->format = Response::FORMAT_RAW;
                $headers = Yii::$app->response->headers;
                $headers->add('Content-Type', 'application/pdf');
                $name = "Reporte Libro {$op} por Cuenta" . ($model->resumido ? ' - resumido.pdf' : '.pdf');

                $pdf = new Pdf([
                    'mode' => Pdf::MODE_CORE,
                    'destination' => Pdf::DEST_BROWSER,
                    'orientation' => Pdf::ORIENT_LANDSCAPE,
                    'format' => $model->paper_size,
                    'marginTop' => 36,
                    'marginBottom' => 14,
                    'marginLeft' => 5,
                    'marginRight' => 5,
                    'content' => $contenido,
                    'cssInline' => $cssInline,
                    'filename' => $name,
                    'options' => [
                        'title' => "",
                        'subject' => "",
                        'defaultheaderline' => false
                    ],
                    'methods' => [
                        'SetHeader' => [$encabezado],
                        'SetFooter' => [$pie],
                        'SetTitle' => [$name]
                    ]
                ]);

                $e = microtime(true);
                Yii::$app->response->format = Response::FORMAT_RAW;
                Yii::$app->response->headers->add('Content-Type', 'application/pdf');
                $response = $pdf->render();
                $f = microtime(true);
                Yii::warning(($f - $a) . " whole action.");
                Yii::warning(($c - $b) . " data provider generator.");
                Yii::warning(($e - $d) . " pdf construction.");
                Yii::warning(($f - $e) . " response construction");
                return $response;
//                    echo "<style>" . $cssInline . "</style>";
//                    echo $encabezado;
//                    echo $contenido;
//                    $g = microtime(true);
//                    Yii::warning(($g - $f) . " raw print time");
//                    die();
            }
        } catch (Exception $exception) {
            throw $exception;
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

    }

    return $this->render('libro-por-cuenta/_form_reporte_por_cuenta', [
        'model' => $model,
    ]);
}

    public function actionReporteLibroPorMes($operacion)
{
    $model = new Reporte();

    if ($model->load(Yii::$app->request->post())) {
        try {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 300);//5 min
            ini_set('pcre.backtrack_limit', 10000000);
            // Procesar datos del post
            $id_desde = $model->cod_desde;
            $id_hasta = $model->cod_hasta;
            $desde = '';
            $hasta = '';
            if ($model->fecha_rango != '') {
                $rango = explode(' - ', $model->fecha_rango);
                $desde = implode('-', array_reverse(explode('-', $rango[0])));
                $hasta = implode('-', array_reverse(explode('-', $rango[1])));
            }

            // Generar array de datos
            $data = self::generateReporteByMes(
                $operacion, $id_desde, $id_hasta, $desde, $hasta, $model);
            $arrayDataProvider = $data[0];
            $facturas_id = $data[1];
            $fechas = $data[2];

////                 ----------------- TEST PARA IMPRESAION ------------------
//                foreach ($arrayDataProvider as $mes => $item) {
//                    echo "$mes || {$item['total']} || {$item['gravada10']} || {$item['iva10']} || {$item['gravada5']} || {$item['iva5']} || {$item['exenta']}<br/>";
//                }
//                exit();
////                 ----------------- TEST PARA IMPRESAION ------------------

            // Crear archivos para exportar.
            if ($model->tipo_doc == 'xls') {
                $encabezado = [
                    'tipoLibro' => ucfirst($operacion),
                    'id_desde' => ($id_desde != '') ? $id_desde : $facturas_id[0],
                    'id_hasta' => ($id_hasta != '') ? $id_hasta : $facturas_id[sizeof($facturas_id) - 1],
                    'desde' => ($model->fecha_rango != '') ? $rango[0] : implode('-', array_reverse(explode('-', $fechas[0]))),
                    'hasta' => ($model->fecha_rango != '') ? $rango[1] : implode('-', array_reverse(explode('-', $fechas[sizeof($fechas) - 1]))),
                ];

                // Crear objeto excel
                $path = '/modules/contabilidad/views/reporte/libro-por-mes/';
                $name = 'template.xls';
                $objPHPExcel = ExcelHelpers::generatePHPExcelObject($path, $name);

                // Rellenar cabecera del excel
                $empresa = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')]);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, $encabezado['desde']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, $encabezado['hasta']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, $encabezado['id_desde']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, $encabezado['id_hasta']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 2, $empresa->razon_social);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 3, "{$empresa->ruc}-{$empresa->digito_verificador}");

                // Rellenar cuerpo excel
                self::createExcelMes($objPHPExcel, $arrayDataProvider);

                // Exportar
                $name = 'Reporte de ' . ucfirst($operacion) . ' por mes ';
                ExcelHelpers::exportToBrowser($objPHPExcel, $name);
            } else {
                $path = 'modules\contabilidad\views\reporte\libro-por-mes';
                $path = str_replace('\\', '/', $path);
                $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/reporte.css");
                $contenido = Yii::$app->view->renderFile("@app/" . $path . "/reporte.php", [
                    'dataProvider' => $arrayDataProvider,
                ]);
                $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/reporte_header.php", [
                    'tipoLibro' => ucfirst($operacion),
                    'id_desde' => ($id_desde != '') ? $id_desde : $facturas_id[0],
                    'id_hasta' => ($id_hasta != '') ? $id_hasta : $facturas_id[sizeof($facturas_id) - 1],
                    'desde' => ($model->fecha_rango != '') ? $rango[0] : implode('-', array_reverse(explode('-', $fechas[0]))),
                    'hasta' => ($model->fecha_rango != '') ? $rango[1] : implode('-', array_reverse(explode('-', $fechas[sizeof($fechas) - 1]))),
                ]);
                $pie = Yii::$app->view->renderFile("@app/" . $path . "/reporte_pie.php", []);
                $op = ucfirst($operacion);

                // http response
                $response = Yii::$app->response;
                $response->format = Response::FORMAT_RAW;
                $headers = Yii::$app->response->headers;
                $headers->add('Content-Type', 'application/pdf');

                $pdf = new Pdf([
                    'mode' => Pdf::DEST_BROWSER,
                    'orientation' => Pdf::ORIENT_LANDSCAPE,
                    'format' => $model->paper_size,
                    'marginTop' => 31,
                    'marginBottom' => 14,
                    'marginLeft' => 5,
                    'marginRight' => 5,
                    'content' => $contenido,
                    'cssInline' => $cssInline,
                    'filename' => "Reporte Libro {$op} por Mes.pdf",
                    'options' => [
                        'title' => "",
                        'subject' => "",
                        'defaultheaderline' => false
                    ],
                    'methods' => [
                        'SetHeader' => [$encabezado],
                        'SetFooter' => [$pie],
                    ]
                ]);

                Yii::$app->response->format = Response::FORMAT_RAW;
                Yii::$app->response->headers->add('Content-Type', 'application/pdf');
                return $pdf->render();
            }
        } catch (Exception $exception) {
//                throw $exception;
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

    }

    return $this->render('libro-por-mes/_form_reporte_por_mes', [
        'model' => $model,
    ]);
}

    /**
     * @param Spreadsheet $objPHPExcel
     * @param array $arrayDataProvider
     */
    private function createExcelMes(&$objPHPExcel, $arrayDataProvider)
{
    $mesToText = ['01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre',
        '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre'];
    $total = $grav10 = $grav5 = $iva10 = $iva5 = $exenta = 0;
    $fila = 5;
    foreach ($arrayDataProvider as $mes => $item) {
        try {
            $total += (float)$item['total'];
            $grav10 += (float)$item['gravada10'];
            $grav5 += (float)$item['gravada5'];
            $exenta += (float)$item['exenta'];
            $iva5 += (float)$item['iva5'];
            $iva10 += (float)$item['iva10'];

            $fila++;
            $column = 1;
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($mesToText[$mes], ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($item['total']) ? number_format(round($item['total']), 0, ',', '.') : 0, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($item['gravada10']) ? number_format(round($item['gravada10']), 0, ',', '.') : 0, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($item['iva10']) ? number_format(round($item['iva10']), 0, ',', '.') : 0, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($item['gravada5']) ? number_format(round($item['gravada5']), 0, ',', '.') : 0, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($item['iva5']) ? number_format(round($item['iva5']), 0, ',', '.') : 0, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($item['exenta']) ? number_format(round($item['exenta']), 0, ',', '.') : 0, ExcelDataType::TYPE_STRING);

        } catch (Exception $exception) {
            Yii::error($exception->getMessage());
        }
    }
}

    /**
     * @param Spreadsheet $objPHPExcel
     * @param $arrayDataProvider
     * @param bool $resumido
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function createExcel(&$objPHPExcel, $arrayDataProvider, $resumido = false)
{
    $cuenta_contable = "";
    if (!$resumido) {
        $cantTotal = 0;
        $cant = 0;

        $fila = 5;

        $gravadas = 0;
        $impuestos = 0;
        $exentas = 0;
        $ret_ivas = 0;
        $ret_rentas = 0;
        $totales = 0;
        $base_imps = 0;
        foreach ($arrayDataProvider as $index => $data) {
            $fila++;
            $column = 1;

            $cta_contable_actual = $data['codigo_completo'];

            // Fila de cuenta
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
            ];

            if ($cuenta_contable != $cta_contable_actual) {
                // fila de resumen por cuenta
                if ($cantTotal > 1) {
                    $column = 1;

                    $gravadas = number_format($gravadas, 0, ',', '.');
                    $impuestos = number_format($impuestos, 0, ',', '.');
                    $exentas = number_format($exentas, 0, ',', '.');
                    $ret_ivas = number_format($ret_ivas, 0, ',', '.');
                    $ret_rentas = number_format($ret_rentas, 0, ',', '.');
                    $totales = number_format($totales, 0, ',', '.');
                    $base_imps = number_format($base_imps, 0, ',', '.');

                    // Fila de resumen por cuenta
//            $fila++;
                    $column = 1;
                    $styleArray = [
                        'borders' => [
                            'top' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED,
                            ],
                            'bottom' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ],
                    ];
                    $objPHPExcel->getActiveSheet()->mergeCells("A{$fila}:B{$fila}");
                    $objPHPExcel->getActiveSheet()->getStyle("A{$fila}:L{$fila}")->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle("A{$fila}:L{$fila}")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit("Items", ExcelDataType::TYPE_STRING);
                    $column++;
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit($cant, ExcelDataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getStyle("C{$fila}:D{$fila}")->getAlignment()->setHorizontal('center');
                    $objPHPExcel->getActiveSheet()->getStyle("C{$fila}:D{$fila}")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit("Sumas", ExcelDataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit($gravadas, ExcelDataType::TYPE_STRING);
                    $column++;
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit($impuestos, ExcelDataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit($exentas, ExcelDataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit($ret_ivas, ExcelDataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit($ret_rentas, ExcelDataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit($totales, ExcelDataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()
                        ->getCellByColumnAndRow($column++, $fila)
                        ->setValueExplicit($base_imps, ExcelDataType::TYPE_STRING);
                    $fila++;
                }

                $column = 1;
                // fila de nombre de cuenta.
                $cuenta_contable = $cta_contable_actual;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit("CUENTA:", ExcelDataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->mergeCells("B{$fila}:C{$fila}");
                $objPHPExcel->getActiveSheet()
                    ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $fila)->getCoordinate())
                    ->getFont()->getColor()->setARGB('CE181E');
                $objPHPExcel->getActiveSheet()
                    ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $fila)->getCoordinate())
                    ->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()
                    ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $fila)->getCoordinate())
                    ->getFont()->setSize(8);
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column + 1, $fila)
                    ->setValueExplicit("$cta_contable_actual - {$data['cuenta_nombre']}", ExcelDataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()
                    ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column + 1, $fila)->getCoordinate())
                    ->getAlignment()->setHorizontal('left');
                $objPHPExcel->getActiveSheet()
                    ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column + 1, $fila)->getCoordinate())
                    ->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()
                    ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column + 1, $fila)->getCoordinate())
                    ->getFont()->setSize(8);
                // incrementa una fila.
                $fila++;
                $column = 1;
                $cant = 0;
                $gravadas = 0;
                $impuestos = 0;
                $exentas = 0;
                $ret_ivas = 0;
                $ret_rentas = 0;
                $totales = 0;
                $base_imps = 0;
            }

            $nro = $data['nro_factura'];

            $fecha = $data['fecha_emision'];
            $gravada = ($data['gravada'] == '-') ? '-' : (float)$data['gravada'];
            $gravadas += ($data['gravada'] == '-') ? 0 : $gravada;
            $impuesto = ($data['impuesto'] == '-') ? '-' : (float)$data['impuesto'];
            $impuestos += ($data['impuesto'] == '-') ? 0 : $impuesto;
            $exenta = ($data['exenta'] == '-') ? '-' : (float)$data['exenta'];
            $exentas += ($data['exenta'] == '-') ? 0 : $exenta;
            $ret_iva = ($data['gravada'] == '-') ? '-' : 0;
            $ret_ivas += ($data['gravada'] == '-') ? 0 : $ret_iva;
            $ret_renta = ($data['gravada'] == '-') ? '-' : 0;
            $ret_rentas += ($data['gravada'] == '-') ? 0 : $ret_renta;
            $total = (float)$data['total'] * (float)$data['cotizacion'];
            $totales += $total;
            $base_imp = ($data['gravada'] == '-') ? '-' : 0;
            $base_imps += ($data['gravada'] == '-') ? 0 : $base_imp;
            $porcentaje = $data['porcentaje'];

            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($nro, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($fecha, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($data['razon_social'], ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($data['ruc'], ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getStyle("D{$fila}")->getAlignment()->setHorizontal('center');
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit((($gravada == '-') ? '-' : $gravada), ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($data['porcentaje'], ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit((($impuesto == '-') ? '-' : $impuesto), ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit((($exenta == '-') ? '-' : $exenta), ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($data['gravada'] == '-') ? '-' : 0, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($data['gravada'] == '-') ? '-' : 0, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit((($total == '-') ? '-' : $total), ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(($data['gravada'] == '-') ? '-' : 0, ExcelDataType::TYPE_NUMERIC);
            $cant++;
            $cantTotal++;
        }

        $column = 1;
        $fila++;
        $gravadas = number_format($gravadas, 0, ',', '.');
        $impuestos = number_format($impuestos, 0, ',', '.');
        $exentas = number_format($exentas, 0, ',', '.');
        $ret_ivas = number_format($ret_ivas, 0, ',', '.');
        $ret_rentas = number_format($ret_rentas, 0, ',', '.');
        $totales = number_format($totales, 0, ',', '.');
        $base_imps = number_format($base_imps, 0, ',', '.');

        // Fila de resumen por cuenta
        $column = 1;
        $styleArray = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $objPHPExcel->getActiveSheet()->mergeCells("A{$fila}:B{$fila}");
        $objPHPExcel->getActiveSheet()->getStyle("A{$fila}:L{$fila}")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle("A{$fila}:L{$fila}")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit("Items", ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($cant, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getStyle("C{$fila}:D{$fila}")->getAlignment()->setHorizontal('center');
        $objPHPExcel->getActiveSheet()->getStyle("C{$fila}:D{$fila}")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit("Sumas", ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($gravadas, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($impuestos, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($exentas, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($ret_ivas, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($ret_rentas, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($totales, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($base_imps, ExcelDataType::TYPE_STRING);
    } else {
        $cantTotal = 0;
        $fila = 5;

        $cant = 0;
        $gravadas5 = 0;
        $gravadas10 = 0;
        $impuestos5 = 0;
        $impuestos10 = 0;
        $exentas = 0;
        $totales = 0;
        $base_imps = 0;
        $cuenta_contable = '';
        $cuenta_contable_n = '';
        $cta_contable_actual = '';
        foreach ($arrayDataProvider as $index => $data) {
            $column = 1;

            $cta_contable_actual = $data['codigo_completo'];

            // Fila de cuenta
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
            ];

            if ($cuenta_contable != $cta_contable_actual && $cuenta_contable != '') {
                $fila++;
                $column = 1;

                $gravadas10 = number_format($gravadas10, 0, ',', '.');
                $gravadas5 = number_format($gravadas5, 0, ',', '.');
                $impuestos10 = number_format($impuestos10, 0, ',', '.');
                $impuestos5 = number_format($impuestos5, 0, ',', '.');
                $exentas = number_format($exentas, 0, ',', '.');
                $totales = number_format($totales, 0, ',', '.');
                $base_imps = number_format($base_imps, 0, ',', '.');

                $styleArray = [
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED,
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($cta_contable_actual, ExcelDataType::TYPE_STRING);
                $column++;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($cuenta_contable_n, ExcelDataType::TYPE_STRING);
                $column++;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($exentas, ExcelDataType::TYPE_STRING);
                $column++;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($gravadas10, ExcelDataType::TYPE_STRING);
                $column++;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($gravadas5, ExcelDataType::TYPE_STRING);
                $column++;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($impuestos10, ExcelDataType::TYPE_STRING);
                $column++;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($impuestos5, ExcelDataType::TYPE_STRING);
                $column++;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($base_imps, ExcelDataType::TYPE_STRING);
                $column++;
                $objPHPExcel->getActiveSheet()
                    ->getCellByColumnAndRow($column, $fila)
                    ->setValueExplicit($totales, ExcelDataType::TYPE_STRING);

                $cant = 0;
                $gravadas5 = 0;
                $gravadas10 = 0;
                $impuestos5 = 0;
                $impuestos10 = 0;
                $exentas = 0;
                $totales = 0;
                $base_imps = 0;
            }

            $porcentaje = $data['porcentaje'];

            $gravada10 = ($data['gravada'] == '-') ? '-' : (($porcentaje == 10) ? (float)$data['gravada'] : 0);
            $gravadas10 += ($data['gravada'] == '-') ? 0 : $gravada10;
            $impuesto10 = ($data['impuesto'] == '-') ? '-' : (($porcentaje == 10) ? (float)$data['impuesto'] : 0);
            $impuestos10 += ($data['impuesto'] == '-') ? 0 : $impuesto10;

            $gravada5 = ($data['gravada'] == '-') ? '-' : (($porcentaje == 5) ? (float)$data['gravada'] : 0);
            $gravadas5 += ($data['gravada'] == '-') ? 0 : $gravada5;
            $impuesto5 = ($data['impuesto'] == '-') ? '-' : (($porcentaje == 5) ? (float)$data['impuesto'] : 0);
            $impuestos5 += ($data['impuesto'] == '-') ? 0 : $impuesto5;

            $exenta = ($data['exenta'] == '-') ? '-' : (float)$data['exenta'];
            $exentas += ($data['exenta'] == '-') ? 0 : $exenta;

            $total = (float)$data['total'] * (float)$data['cotizacion'];
            $totales += $total;

            $base_imp = ($data['gravada'] == '-') ? '-' : 0;
            $base_imps += ($data['gravada'] == '-') ? 0 : $base_imp;

            $cant++;
            $cantTotal++;

            $cuenta_contable = $cta_contable_actual;
            $cuenta_contable_n = $data['cuenta_nombre'];
        }

        $fila++;
        $column = 1;

        $gravadas10 = number_format($gravadas10, 0, ',', '.');
        $gravadas5 = number_format($gravadas5, 0, ',', '.');
        $impuestos10 = number_format($impuestos10, 0, ',', '.');
        $impuestos5 = number_format($impuestos5, 0, ',', '.');
        $exentas = number_format($exentas, 0, ',', '.');
        $totales = number_format($totales, 0, ',', '.');
        $base_imps = number_format($base_imps, 0, ',', '.');

        $styleArray = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($cta_contable_actual, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($cuenta_contable_n, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($exentas, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($gravadas10, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($gravadas5, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($impuestos10, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($impuestos5, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($base_imps, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit($totales, ExcelDataType::TYPE_STRING);
    }
}

    /**
     * @param string $operacion
     * @param null $id_desde
     * @param null $id_hasta
     * @param null $fechaDesde
     * @param null $fechaHasta
     * @param Reporte $model
     * @return array
     */
    private function generateReporteByCuenta(
    $operacion = 'compra', $id_desde = null, $id_hasta = null, $fechaDesde = null, $fechaHasta = null, $model = null)
{
    $a = microtime(true);
    $empresa_id = Yii::$app->session->get('core_empresa_actual');
    $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
    $iva5 = Iva::findOne(['porcentaje' => 5]);
    $iva10 = Iva::findOne(['porcentaje' => 10]);
    $ivaCta5 = IvaCuenta::findOne(['iva_id' => $iva5->id]);
    $ivaCta10 = IvaCuenta::findOne(['iva_id' => $iva10->id]);
    $cotizacFie = ($operacion == 'compra') ? 'cotizacion' : 'valor_moneda';
    $nroFactura = ($operacion == 'compra') ? 'fa.nro_factura' : "CONCAT(fa.prefijo, ('-'), fa.nro_factura) as nro_factura";
    $tablaFactu = ($operacion == 'compra') ? Compra::tableName() : Venta::tableName();
    $facturaDet = ($operacion == 'compra') ? DetalleCompra::tableName() : DetalleVenta::tableName();
    $ivaCtaTabl = ($operacion == 'compra') ? CompraIvaCuentaUsada::tableName() : VentaIvaCuentaUsada::tableName();
    $entidadTab = Entidad::tableName();
    $planCtaTab = PlanCuenta::tableName();
    $plantillaD = PlantillaCompraventaDetalle::tableName();

    $select = [
        'fa.id',
        'fa.moneda_id',
        "IF(fa.moneda_id = 1, 1, fa.{$cotizacFie}) as cotizacion",
        "$nroFactura",
        'fa.fecha_emision',
        'en.razon_social',
        'en.ruc',
        'icu.monto as total',
        'fade.subtotal as fade_total',
        "IF(icu.iva_cta_id = {$ivaCta5->id}, (icu.monto / 1.05), IF(icu.iva_cta_id = {$ivaCta10->id}, (icu.monto / 1.1), 0)) * IF(fa.moneda_id = 1, 1, fa.{$cotizacFie}) as gravada",
        "IF(icu.iva_cta_id = {$ivaCta5->id}, '5', IF(icu.iva_cta_id = {$ivaCta10->id}, '10', '0')) as porcentaje",
        "IF(icu.iva_cta_id = {$ivaCta5->id}, icu.monto - (icu.monto / 1.05), IF(icu.iva_cta_id = {$ivaCta10->id}, icu.monto - (icu.monto / 1.1), icu.monto - icu.monto)) * IF(fa.moneda_id = 1, 1, fa.{$cotizacFie}) as impuesto",
        "IF(icu.iva_cta_id is null, icu.monto, 0) * IF(fa.moneda_id = 1, 1, fa.{$cotizacFie}) AS exenta",
        "cta.cod_completo as codigo_completo",
        "cta.cod_ordenable as codigo_ordenable",
        "cta.nombre as cuenta_nombre",
    ];
    $query = new Query();
    $query->select($select);
    $query->from("$ivaCtaTabl as icu");
    $query->leftJoin("$tablaFactu as fa", "icu.factura_{$operacion}_id = fa.id");
    $query->leftJoin("$plantillaD as plantillad", "((plantillad.plantilla_id = icu.plantilla_id and ((plantillad.iva_cta_id is not null and plantillad.iva_cta_id = icu.iva_cta_id) or (plantillad.iva_cta_id is null and icu.iva_cta_id is null))))");
    $query->leftJoin("$facturaDet as fade", "(fade.factura_{$operacion}_id = icu.factura_{$operacion}_id and fade.plan_cuenta_id = plantillad.p_c_gravada_id)");
    $query->leftJoin("$entidadTab as en", "fa.entidad_id = en.id");
    $query->leftJoin("$planCtaTab as cta", "cta.id = fade.plan_cuenta_id");
    $query->where(['fa.empresa_id' => $empresa_id, 'fa.periodo_contable_id' => $periodo_id]);
    $query->filterWhere(['BETWEEN', 'fa.fecha_emision', $fechaDesde, $fechaHasta]);
    $query->andFilterWhere(['>=', 'fa.id', $id_desde]);
    $query->andFilterWhere(['<=', 'fa.id', $id_hasta]);
    $query->andFilterWhere(['IN', 'fa.entidad_id', $model->_entidades]);
    $query->andFilterWhere(['!=', 'icu.monto', 0]);
    $query->andWhere(['IS NOT', 'cta.cod_completo', null]);
    $query->andWhere(['=', 'fa.estado', 'vigente']);

    if ($model->moneda != 0)
        $query->andWhere(['fa.moneda_id' => $model->moneda]);

    // Cuentas gravadas esperadas
    $select = [
        'fa.id as factura_id',
        'plantillad.p_c_gravada_id gravada_id',
    ];
    $query_esperadas = new Query();
    $query_esperadas->select($select);
    $query_esperadas->from("$ivaCtaTabl as icu");
    $query_esperadas->leftJoin("$tablaFactu as fa", "icu.factura_{$operacion}_id = fa.id");
    $query_esperadas->leftJoin("$plantillaD as plantillad", "((plantillad.plantilla_id = icu.plantilla_id and ((plantillad.iva_cta_id is not null and plantillad.iva_cta_id = icu.iva_cta_id) or (plantillad.iva_cta_id is null and icu.iva_cta_id is null))))");
    $query_esperadas->where(['fa.empresa_id' => $empresa_id, 'fa.periodo_contable_id' => $periodo_id]);
    $query_esperadas->filterWhere(['BETWEEN', 'fa.fecha_emision', $fechaDesde, $fechaHasta]);
    $query_esperadas->andFilterWhere(['>=', 'fa.id', $id_desde]);
    $query_esperadas->andFilterWhere(['<=', 'fa.id', $id_hasta]);
    $query_esperadas->andFilterWhere(['IN', 'fa.entidad_id', $model->_entidades]);
    $query_esperadas->andWhere(['=', 'fa.estado', 'vigente']);
    $query_esperadas->orderBy(['factura_id' => SORT_ASC]);

    if ($model->moneda != 0)
        $query_esperadas->andWhere(['fa.moneda_id' => $model->moneda]);

    $ctaGravEsperadas = $query_esperadas->all();
    $ctasEsperadasGrp = [];
    foreach ($ctaGravEsperadas as $ctaGravEsperada) {
        $ctasEsperadasGrp[$ctaGravEsperada['factura_id']][] = $ctaGravEsperada['gravada_id'];
    }

    // Cuentas gravadas usadas
    $select = [
        'fa.id as factura_id',
        'fade.plan_cuenta_id gravada_id',
        'fade.id as fade_id'
    ];
    $query_usadas = new Query();
    $query_usadas->select($select);
    $query_usadas->from("$tablaFactu as fa");
    $query_usadas->leftJoin("$facturaDet fade", "fade.factura_{$operacion}_id = fa.id");
    $query_usadas->where(['fa.empresa_id' => $empresa_id, 'fa.periodo_contable_id' => $periodo_id]);
    $query_usadas->filterWhere(['BETWEEN', 'fa.fecha_emision', $fechaDesde, $fechaHasta]);
    $query_usadas->andFilterWhere(['>=', 'fa.id', $id_desde]);
    $query_usadas->andFilterWhere(['<=', 'fa.id', $id_hasta]);
    $query_usadas->andFilterWhere(['IN', 'fa.entidad_id', $model->_entidades]);
    $query_usadas->andWhere(['=', 'fa.estado', 'vigente']);
    $query_usadas->orderBy(['factura_id' => SORT_ASC]);

    if ($model->moneda != 0)
        $query_usadas->andWhere(['fa.moneda_id' => $model->moneda]);

    $ctaGravUsadas = $query_usadas->all();
    $ctasUsadasGrp = [];
    foreach ($ctaGravUsadas as $ctaGravUsada) {
        $ctasUsadasGrp[$ctaGravUsada['factura_id']][] = ['gravada' => $ctaGravUsada['gravada_id'], 'fade' => $ctaGravUsada['fade_id']];
    }
    $b = microtime(true);

    // Dejar solamente cuentas usadas no esperadas.
    foreach ($ctasUsadasGrp as $factura_id => $tuplas) {
        $ctas_esperadas = $ctasEsperadasGrp[$factura_id];
        foreach ($tuplas as $key => $tupla) {
            $cta_usada = $tupla['gravada'];
            if (in_array($cta_usada, $ctas_esperadas)) {
                unset($ctasUsadasGrp[$factura_id][$key]);
            }
        }
    }
    $c = microtime(true);
    $detalle_ids = [];
    foreach ($ctasUsadasGrp as $factura_id => $tuplas) {
        foreach ($tuplas as $tupla) {
            $detalle_ids[] = $tupla['fade'];//echo "&nbsp;&nbsp;&nbsp;&nbsp;Gravada: {$tupla['gravada']}, Detalle id: {$tupla['fade']}<br/>";
        }
    }
    $select = [
        'fa.id',
        'fa.moneda_id',
        "IF(fa.moneda_id = 1, 1, fa.{$cotizacFie}) as cotizacion",
        "$nroFactura",
        'fa.fecha_emision',
        'en.razon_social',
        'en.ruc',
        'fade.subtotal as total',
        'fade.subtotal as fade_total',
        "('-') as gravada",
        "('-') as porcentaje",
        "('-') as impuesto",
        "('-') AS exenta",
        "cta.cod_completo as codigo_completo",
        "cta.cod_ordenable as codigo_ordenable",
        "cta.nombre as cuenta_nombre",
    ];
    $query_no_esperadas = new Query();
    $query_no_esperadas->select($select);
    $query_no_esperadas->from("$tablaFactu as fa");
    $query_no_esperadas->leftJoin("$facturaDet as fade", "fade.factura_{$operacion}_id = fa.id");
    $query_no_esperadas->leftJoin("$entidadTab as en", "fa.entidad_id = en.id");
    $query_no_esperadas->leftJoin("$planCtaTab as cta", "cta.id = fade.plan_cuenta_id");
    $query_no_esperadas->where(['fa.empresa_id' => $empresa_id, 'fa.periodo_contable_id' => $periodo_id]);
    $query_no_esperadas->filterWhere(['BETWEEN', 'fa.fecha_emision', $fechaDesde, $fechaHasta]);
    $query_no_esperadas->andFilterWhere(['>=', 'fa.id', $id_desde]);
    $query_no_esperadas->andFilterWhere(['<=', 'fa.id', $id_hasta]);
    $query_no_esperadas->andFilterWhere(['IN', 'fa.entidad_id', $model->_entidades]);
    $query_no_esperadas->andWhere(['IS NOT', 'cta.cod_completo', null]);
    $query_no_esperadas->andFilterWhere(['IN', 'fade.id', $detalle_ids]);
    $query_no_esperadas->andWhere(['=', 'fa.estado', 'vigente']);

    if ($model->moneda != 0)
        $query_no_esperadas->andWhere(['fa.moneda_id' => $model->moneda]);

    $query->union($query_no_esperadas->createCommand()->rawSql);

    // verificar si el array final es vacio.
    if (!$query->exists()) return [];

    $arrayDataProvider = $query->all();

    // ordenar por codigo de cuenta.
    usort($arrayDataProvider, function ($item1, $item2) {
        return $item1['codigo_ordenable'] <=> $item2['codigo_ordenable'];
    });

    $factura_ids = array_values(array_unique(array_map(function ($elem) {
        return $elem['id'];
    }, $arrayDataProvider)));
    $fechas = array_values(array_unique(array_map(function ($elem) {
        return $elem['fecha_emision'];
    }, $arrayDataProvider)));

    return [$arrayDataProvider, $factura_ids, $fechas];
}

    /**
     * @param array|DetalleCompraReporte[]|DetalleVentaReporte[] $detalles
     * @param array $cuentas
     */
    private function filterByCuentas(&$detalles = [], $cuentas = [])
{
    if (empty($cuentas)) return;

    foreach ($detalles as $index => $detalle) {
        if (!in_array($detalle->plan_cuenta_id, $cuentas)) {
            unset($detalles[$index]);
        }
    }

    $detalles = array_values($detalles);
}

    /** Obtiene los detalles de facturas basandose en sus respectivas tablas de iva_cuenta_usada.
     *
     * @param Compra[]|Venta[]|ActiveRecord[] $facturas
     * @param string $operacion
     * @return array
     */
    private function getDetalles($facturas, $operacion)
{
    $detalles = [];
    foreach ($facturas as $factura) {
        foreach ($factura->ivasCuentaUsadas as $ivaCtaUsada) {
            if (empty($detalles)) $detalles = self::generateDetalles($ivaCtaUsada, $operacion);
            else $detalles = array_merge($detalles, self::generateDetalles($ivaCtaUsada, $operacion));
        }
    }

    return $detalles;
}

    /** Genera los detalles de factura correspondiente a la tabla de iva_cuenta_usada, con un indicador de si es para
     * iva5, iva10 o excenta, con las cuentas correspondientes a la plantilla asociada.
     *
     * @param CompraIvaCuentaUsada|VentaIvaCuentaUsada|ActiveRecord $ivaCtaUsada
     * @param string $operacion
     * @return array
     */
    private function generateDetalles($ivaCtaUsada, $operacion)
{
    $detalles = [];
    $plantilla = $ivaCtaUsada->plantilla;
    $plantillaDetalles = $plantilla->plantillaCompraventaDetalles;

    if ($plantilla->esParaPrestamo()) {
        // TODO: determinar que cuentas y con que valor deben aparecer
//            $detalles = self::generateDetallesCompraPrestamo($ivaCtaUsada); // determinar que cuentas y con que valor deben aparecer
    } elseif ($plantilla->esParaCuotaPrestamo()) {
        // TODO: falta determinar que cuentas y con que valores (+/-) deben aparecer
//            $detalles = self::generateDetallesCompraCuotaPrestamo($ivaCtaUsada); // falta determinar que cuentas y con que valores (+/-) deben aparecer
    } else {
        if ($operacion == 'compra') {
            $detalles = self::generateDetallesCompra($ivaCtaUsada, $plantillaDetalles);
        } else {
            $detalles = self::generateDetallesVenta($ivaCtaUsada, $plantillaDetalles);
        }
    }

    return $detalles;
}

    /**
     * @param CompraIvaCuentaUsada $ivaCtaUsada
     * @return DetalleCompraReporte[] array
     */
    private function generateDetallesCompraPrestamo($ivaCtaUsada)
{
    /** @var DetalleCompraReporte[] $compra_detalles */
    $prestamos = Prestamo::findAll(['factura_compra_id' => $ivaCtaUsada->factura_compra_id]);
    $compra_detalles = [];

    foreach ($prestamos as $prestamo) {
        $usar_iv_10 = $prestamo->usar_iva_10;
        $cuentas = [];
        foreach ($prestamo->getCuentasFields() as $cuenta_field) {
            if (array_key_exists($cuenta_field, $prestamo->attributes)) {
                $cuentas[] = ['atributo' => $cuenta_field, 'valor' => $prestamo->$cuenta_field];
            }
        }

        foreach ($cuentas as $cuenta) {
            $concepto = str_replace('cuenta_', '', $cuenta['atributo']);
            $compra_detalle = new DetalleCompraReporte();
            $compra_detalle->plan_cuenta_id = $cuenta['valor'];
            $compra_detalle->cta_principal = 'no';
            $compra_detalle->subtotal = 0;
            $compra_detalle->factura_compra_id = $ivaCtaUsada->factura_compra_id;

            if ($concepto == 'intereses_pagados') continue;

            if ($usar_iv_10 == 'si') {
                if ($concepto == 'gastos_bancarios') {
                    $compra_detalle->subtotal = round($prestamo->$concepto / 1.1);
                    $compra_detalle->cta_contable = 'debe';
                    $compra_detalle->iva_cuenta_id = IvaCuenta::findOne(['iva_id' => Iva::findOne(['porcentaje' => 10])]);
                } elseif (in_array($concepto, ['intereses_vencer'])) {
                    $compra_detalle->subtotal = ($prestamo->incluye_iva == 'si') ? round($prestamo->$concepto / 1.1) : $prestamo->$concepto;
                    $compra_detalle->cta_contable = 'debe';
                    $compra_detalle->iva_cuenta_id = IvaCuenta::findOne(['iva_id' => Iva::findOne(['porcentaje' => 10])]);
                } /*elseif ($concepto != 'gastos_no_deducibles') {
                        $compra_detalle->subtotal = $concepto == 'caja' ? $prestamo->$concepto : 0.0;
                        $compra_detalle->subtotal = $concepto == 'caja' ? 0.0 : (($prestamo->iva_interes_cobrado == 'si') ? ($prestamo->$concepto) : ((int)$prestamo->$concepto + $iva_interes));
                        $compra_detalle->cta_contable = ($concepto == 'caja') ? 'debe' : 'haber';
                        $compra_detalle->iva_cuenta_id = null;
                    }*/
            } else {
                if ($concepto == 'gastos_no_deducibles') {
//                        $iva_10 = $gastos_bancarios - round($gastos_bancarios / 1.1);
//                        if ($incluye_iva == 'si')
//                            $iva_10 += $interes - round($interes / 1.1);
//                        else
//                            $iva_10 += round($interes / 1.1);
//                        $compra_detalle->subtotal = $iva_10;
//                        $compra_detalle->cta_contable = 'debe';
//                        $compra_detalle->iva_cuenta_id = null;
                } elseif ($concepto == 'gastos_bancarios') {
                    $compra_detalle->subtotal = round($prestamo->$concepto / 1.1);
                    $compra_detalle->cta_contable = 'debe';
                    $compra_detalle->iva_cuenta_id = IvaCuenta::findOne(['iva_id' => Iva::findOne(['porcentaje' => 10])]);
                } elseif (in_array($concepto, ['intereses_vencer'])) {
                    $compra_detalle->subtotal = ($prestamo->incluye_iva == 'si') ? round($prestamo->$concepto / 1.1) : $prestamo->$concepto;
                    $compra_detalle->cta_contable = 'debe';
                    $compra_detalle->iva_cuenta_id = IvaCuenta::findOne(['iva_id' => Iva::findOne(['porcentaje' => 10])]);
                } /*elseif ($concepto != 'iva_10') {
                        $compra_detalle->subtotal = $concepto == 'caja' ? $prestamo->$concepto : 0.0;
                        $compra_detalle->subtotal = $concepto == 'caja' ? 0.0 : (($prestamo->iva_interes_cobrado == 'si') ? ($prestamo->$concepto) : ((int)$prestamo->$concepto + $iva_interes));
                        $compra_detalle->cta_contable = ($concepto == 'caja') ? 'debe' : 'haber';
                    }*/
            }

            if ((int)$compra_detalle->subtotal)
                $compra_detalles[] = $compra_detalle;
        }
    }

    return $compra_detalles;
}

    /**
     * @param CompraIvaCuentaUsada $ivaCtaUsada
     * @return array
     */
    private function generateDetallesCompraCuotaPrestamo($ivaCtaUsada)
{
    $cuotasFacturadas = PrestamoDetalleCompra::findAll(['factura_compra_id' => $ivaCtaUsada->factura_compra_id]);
    $prestamoDataProvider = [];
    foreach ($cuotasFacturadas as $cuota) {
        $prestamoDetalle = PrestamoDetalle::findOne(['id' => $cuota->prestamo_detalle_id]);
        if (!array_key_exists($prestamoDetalle->prestamo_id, $prestamoDataProvider))
            $prestamoDataProvider[$prestamoDetalle->prestamo_id] = [];
        $prestamoDataProvider[$prestamoDetalle->prestamo_id][] = $cuota;
    }

    $detalles = [];
    /**
     * @var int $prestamo_id
     * @var PrestamoDetalleCompra $prestamo_detalle
     */
    foreach ($prestamoDataProvider as $prestamo_id => $prestamo_detalles) {
        foreach ($prestamo_detalles as $prestamo_detalle) {
//                $_pCapital = (float)$prestamo_detalle->capital;
            $_pInteres = (float)$prestamo_detalle->interes;
            $_pCuota = (float)$prestamo_detalle->monto_cuota;

            // Crear detalles de compra
            /** @var Prestamo $prestamo */
            $prestamo = Prestamo::findOne(['id' => $prestamo_id]);
            foreach ($prestamo->getCuentasFields() as $cuenta) {
                if (in_array($cuenta, ['cuenta_caja', 'cuenta_intereses_vencer', 'cuenta_intereses_pagados', 'cuenta_monto_operacion'])) {
                    $detalle = null;
                    switch ($cuenta) {
                        case 'cuenta_caja':
//                                $found = false;
//                                if (!$found) {
//                                    $detalle = new DetalleCompraReporte();
//                                    $detalle->plan_cuenta_id = $prestamo->$cuenta;
//                                    // en CAJA incluye el iva si es que el interes es sin iva
//                                    $interes = 0;
//                                    $interes += (($prestamo->incluye_iva == 'no') ? round($_pInteres / 10) : round($_pInteres / 11));
//                                    $cuota = $_pCuota + $interes;
//                                    $detalle->subtotal = (int)$cuota;
//                                    $detalle->cta_contable = 'haber';
//                                    $detalle->agrupar = 'no';
//                                    $detalle->cta_principal = 'no';
//                                    $detalle->prestamo_cuota_id = $prestamo_detalle->id;
//                                }
                            break;
                        case 'cuenta_intereses_vencer':
                            $detalle = new DetalleCompraReporte();
                            $detalle->plan_cuenta_id = $prestamo->$cuenta;
                            // en INTERESES A DEVENGAR excluye el iva
                            $interes = 0;
                            $interes += (($prestamo->incluye_iva == 'no') ? 0 : round($_pInteres / 11));
                            $interes = $_pInteres - $interes;
                            $detalle->subtotal = -$interes;
                            $detalle->cta_contable = 'haber';
                            $detalle->agrupar = 'no';
                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                            $detalle->iva_cuenta_id = IvaCuenta::findOne(['iva_id' => Iva::findOne(['porcentaje' => 10])]);
                            break;
                        case 'cuenta_intereses_pagados':
                            if ($prestamo->iva_interes_cobrado == 'si') {
                                $detalle = new DetalleCompraReporte();
                                $detalle->plan_cuenta_id = $prestamo->$cuenta;
                                $detalle->subtotal = ($prestamo->incluye_iva == 'no') ? $_pInteres : round($_pInteres / 1.1);
                                $detalle->cta_contable = 'debe';
                                $detalle->agrupar = 'no';
                                $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                $detalle->iva_cuenta_id = IvaCuenta::findOne(['iva_id' => Iva::findOne(['porcentaje' => 10])]);
                            } else {
                                $detalle = new DetalleCompraReporte();
                                $detalle->plan_cuenta_id = $prestamo->$cuenta;
                                $detalle->subtotal = ($prestamo->incluye_iva == 'no') ? $_pInteres : round($_pInteres / 1.1);
                                $detalle->cta_contable = 'debe';
                                $detalle->agrupar = 'no';
                                $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                $detalle->iva_cuenta_id = IvaCuenta::findOne(['iva_id' => Iva::findOne(['porcentaje' => 10])]);

//                                    $_detalleIva = new DetalleCompraReporte();
//                                    $_detalleIva->plan_cuenta_id = ($prestamo->usar_iva_10 == 'si') ? $prestamo->cuenta_iva_10 : $prestamo->cuenta_gastos_no_deducibles;
//                                    $_detalleIva->subtotal = ($prestamo->incluye_iva == 'no') ? round($_pInteres / 10) : round($_pInteres / 11);
//                                    $_detalleIva->cta_contable = 'debe';
//                                    $_detalleIva->agrupar = 'no';
//                                    $_detalleIva->cta_principal = 'no';
//                                    $_detalleIva->prestamo_cuota_id = $prestamo_detalle->id;
//                                    $detalles[] = $_detalleIva;
                            }
                            break;
                        case 'cuenta_monto_operacion':
//                                $found = false;
//                                if (!$found) {
//                                    $detalle = new DetalleCompraReporte();
//                                    $detalle->plan_cuenta_id = $prestamo->$cuenta;
//                                    $detalle->subtotal = $_pCuota;
//                                    $detalle->cta_contable = 'debe';
//                                    $detalle->agrupar = 'no';
//                                    $detalle->cta_principal = 'no';
//                                    $detalle->prestamo_cuota_id = $prestamo_detalle->id;
//                                }
                            break;
                    }
                    if (isset($detalle)) {
                        $detalle->factura_compra_id = $ivaCtaUsada->factura_compra_id;
                        $detalles[] = $detalle;
                    }
                }
            }
        }
    }

//        $tm = sizeof($detalles);
//        Yii::warning("tamano de detalles: {$tm}");
    return $detalles;
}

    /** Crea detalles de facturas de compra con indicador de a que iva corresponde, en base a iva_cuenta_usada y los
     * detalles de plantilla.
     *
     * @param CompraIvaCuentaUsada $ivaCtaUsada
     * @param PlantillaCompraventaDetalle[] $plantillaDetalles
     * @return DetalleCompraReporte[]
     */
    private function generateDetallesCompra($ivaCtaUsada, $plantillaDetalles)
{
    /** Se observa segun ejemplo que no hay reporte de las cuentas del iva, ni de caja/banco/proveedores, esas
     *  cuentas que se ingresan mediante el tipo de documento.
     */
    $detalles = [];
    foreach ($plantillaDetalles as $p_det) {
        $model = new DetalleCompraReporte();
        $model->factura_compra_id = $ivaCtaUsada->factura_compra_id;
        $model->cta_contable = $p_det->tipo_saldo;
        $model->iva_cuenta_id = $p_det->iva_cta_id;
        $model->plan_cuenta_id = $p_det->p_c_gravada_id;
        $moneda_extrangera = $ivaCtaUsada->compra->moneda_id != \common\models\ParametroSistema::getMonedaBaseId();
        $cotizacion = $moneda_extrangera ? $ivaCtaUsada->compra->cotizacion : 1;
        $paraDescuento = $p_det->plantilla->isForDiscount();

        if (isset($ivaCtaUsada->ivaCta) && ($ivaCtaUsada->iva_cta_id == $p_det->iva_cta_id)) {
            $subtotal = (float)$ivaCtaUsada->monto;
            $porcentaje = (int)$ivaCtaUsada->ivaCta->iva->porcentaje;
            $gravada = ($subtotal / ((100 + $porcentaje) / (100)));
            $gravada *= $cotizacion;
            $gravada = round($gravada);
            $model->subtotal = (float)$gravada * ($paraDescuento ? -1 : 1);
            $detalles[] = $model;

        } elseif (!isset($ivaCtaUsada->ivaCta) && !isset($p_det->ivaCta)) {
            $subtotal = $ivaCtaUsada->monto;
            $subtotal *= $cotizacion;
            $subtotal = round($subtotal);
            $model->subtotal = (float)$subtotal * ($paraDescuento ? -1 : 1);
            $detalles[] = $model;
        }
    }

    return $detalles;
}

    /** Crea detalles de facturas de venta con indicador de a que iva corresponde, en base a iva_cuenta_usada y los
     * detalles de plantilla.
     *
     * @param VentaIvaCuentaUsada $ivaCtaUsada
     * @param PlantillaCompraventaDetalle[] $plantillaDetalles
     * @return DetalleVentaReporte[]
     */
    private function generateDetallesVenta($ivaCtaUsada, $plantillaDetalles)
{
//        echo "-Procesando factura de venta id {$ivaCtaUsada->factura_venta_id} y usa plantilla {$ivaCtaUsada->plantilla->nombre}<br/>";
    $detalles = [];
    foreach ($plantillaDetalles as $p_det) {
        $model = new DetalleVentaReporte();
        $model->factura_venta_id = $ivaCtaUsada->factura_venta_id;
        $model->cta_contable = $p_det->tipo_saldo;
        $model->iva_cuenta_id = $p_det->iva_cta_id;
        $model->plan_cuenta_id = $p_det->p_c_gravada_id;
        $moneda_extrangera = $ivaCtaUsada->venta->moneda_id != \common\models\ParametroSistema::getMonedaBaseId();
        $cotizacion = $moneda_extrangera ? $ivaCtaUsada->venta->valor_moneda : 1;
        $paraDescuento = $p_det->plantilla->isForDiscount();

        if (isset($ivaCtaUsada->ivaCta) && ($ivaCtaUsada->iva_cta_id == $p_det->iva_cta_id)) {
            $subtotal = (float)$ivaCtaUsada->monto;
            $porcentaje = (int)$ivaCtaUsada->ivaCta->iva->porcentaje;
            $gravada = ($subtotal / ((100 + $porcentaje) / (100)));
            $gravada *= $cotizacion;
            $gravada = round($gravada);
            $model->subtotal = (float)$gravada * ($paraDescuento ? -1 : 1);
            $detalles[] = $model;

        } elseif (!isset($ivaCtaUsada->ivaCta) && !isset($p_det->ivaCta)) {
            $subtotal = $ivaCtaUsada->monto;
            $subtotal *= $cotizacion;
            $subtotal = round($subtotal);
            $model->subtotal = (float)$subtotal * ($paraDescuento ? -1 : 1);
            $detalles[] = $model;
        }

    }

    return $detalles;
}

    /**
     * @param string $operacion
     * @param null $id_desde
     * @param null $id_hasta
     * @param null $fechaDesde
     * @param null $fechaHasta
     * @param Reporte $model
     * @return array
     * @throws \yii\db\Exception
     */
    private function generateReporteByMes(
    $operacion = 'compra', $id_desde = null, $id_hasta = null, $fechaDesde = null, $fechaHasta = null, $model = null)
{
    $empresa_id = Yii::$app->session->get('core_empresa_actual');
    $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
    $query = ($operacion == 'compra') ? Compra::find() : Venta::find();
    $query->alias('factura');
    $query->filterWhere(['>=', 'factura.fecha_emision', $fechaDesde]);
    $query->filterWhere(['<=', 'factura.fecha_emision', $fechaHasta]);
    $query->andFilterWhere(['>=', 'factura.id', $id_desde]);
    $query->andFilterWhere(['<=', 'factura.id', $id_hasta]);
    $query->andFilterWhere(['AND', ['factura.empresa_id' => $empresa_id], ['factura.periodo_contable_id' => $periodo_id]]);
    $query->andFilterWhere(['IN', 'factura.entidad_id', $model->_entidades]);

    if (!$query->exists())
        return [];

    /** @var Compra[]|Venta[] $facturas */
    $facturas = $query->all();
    $factura_ids = [];
    $fechas = [];
    foreach ($facturas as $factura) {
        $factura_ids[] = $factura->id;
        $fechas[] = $factura->fecha_emision;
    }
    sort($factura_ids);
    sort($fechas);

    /** @var Compra[] | Venta[] $facturas */
    $iva5 = Iva::findOne(['porcentaje' => 5]);
    $iva10 = Iva::findOne(['porcentaje' => 10]);
    $ivaCta5 = IvaCuenta::findOne(['iva_id' => $iva5->id]);
    $ivaCta10 = IvaCuenta::findOne(['iva_id' => $iva10->id]);

    $fieldCotizacion = ($operacion == 'compra') ? 'cotizacion' : 'valor_moneda';
    $facturas = implode(',', $factura_ids);
    $ivasCtas = "SELECT ivaCta.*,
       factura.id,
       factura.fecha_emision,
       factura.entidad_id,
       factura.moneda_id,
       factura.obligacion_id,
       factura.para_iva,
       factura.{$fieldCotizacion} AS cotizacion
FROM cont_factura_{$operacion}_iva_cuenta_usada AS ivaCta
  LEFT JOIN cont_factura_{$operacion} AS factura
ON factura.id = ivaCta.factura_{$operacion}_id
WHERE factura.id in ({$facturas})";

    $query = Yii::$app->db->createCommand($ivasCtas);
    $resultSet = $query->queryAll();

    $arrayDataProvider = [];
    $meses_procesados = [];
    $plantillasDescuento = [];
    foreach (PlantillaCompraventa::find()->where(['tipo' => $operacion, 'para_descuento' => 'si'])->all() as $item) {
        $plantillasDescuento[] = $item->id;
    }

    foreach ($resultSet as $index => $data) {
        $cotizacion = $data['cotizacion'];
        $cotizacion = ($cotizacion == '') ? 1 : $cotizacion;
        Yii::warning("Cotizacion para factura {$data['id']}: {$cotizacion}");
        $mes = substr($data['fecha_emision'], 5, 2);
        if (!in_array($mes, $meses_procesados)) $meses_procesados[] = $mes;
        if (!array_key_exists($mes, $arrayDataProvider)) {
            $arrayDataProvider[$mes] = [
                'total' => 0,
                'gravada10' => 0,
                'gravada5' => 0,
                'iva10' => 0,
                'iva5' => 0,
                'exenta' => 0,
            ];
        }
        $monto = (float)$data['monto'] * $cotizacion;
        $gravada10 = $monto * ($data['iva_cta_id'] == $ivaCta10->id ? (float)(1 / 1.1) : 0);
        $gravada05 = $monto * ($data['iva_cta_id'] == $ivaCta5->id ? (float)(1 / 1.05) : 0);
        $iva10 = ($data['iva_cta_id'] == $ivaCta10->id) ? $monto - $gravada10 : 0;
        $iva5 = ($data['iva_cta_id'] == $ivaCta5->id) ? $monto - $gravada05 : 0;
        $excenta = ($data['iva_cta_id'] == '') ? $monto : 0;
        $arrayDataProvider[$mes]['total'] += $monto * (in_array($data['plantilla_id'], $plantillasDescuento) ? -1 : 1);
        $arrayDataProvider[$mes]['gravada10'] += $gravada10 * (in_array($data['plantilla_id'], $plantillasDescuento) ? -1 : 1);
        $arrayDataProvider[$mes]['gravada5'] += $gravada05 * (in_array($data['plantilla_id'], $plantillasDescuento) ? -1 : 1);
        $arrayDataProvider[$mes]['iva10'] += $iva10 * (in_array($data['plantilla_id'], $plantillasDescuento) ? -1 : 1);
        $arrayDataProvider[$mes]['iva5'] += $iva5 * (in_array($data['plantilla_id'], $plantillasDescuento) ? -1 : 1);
        $arrayDataProvider[$mes]['exenta'] += $excenta * (in_array($data['plantilla_id'], $plantillasDescuento) ? -1 : 1);
    }

    foreach (range(1, 12, 1) as $mes) {
        if ($mes < 10) $mes = "0{$mes}";
        else $mes = "$mes";
        if (!array_key_exists($mes, $arrayDataProvider)) {
            $arrayDataProvider[$mes] = [
                'total' => 0,
                'gravada10' => 0,
                'gravada5' => 0,
                'iva10' => 0,
                'iva5' => 0,
                'exenta' => 0,
            ];
        }
    }
//
//        foreach ($arrayDataProvider as $mes => $data) {
//            echo "Mes: $mes<br/>";
//            echo print_r($data);
//            echo "<br/><br/>";
//        }

    return [$arrayDataProvider, $factura_ids, $fechas];
}

    /**
     * @param DetalleCompraReporte[]|DetalleVentaReporte[]|ActiveRecord[] $detalles
     * @return array
     */
    private function generateDataForReporteByCuenta($detalles)
{
    $arrayDataProvider = [];
    foreach ($detalles as $detalle) {
        $cuenta_id = $detalle->plan_cuenta_id;
        if (!array_key_exists($cuenta_id, $arrayDataProvider)) {
            self::insertCuentaToArray($detalle, $arrayDataProvider);
        } else {
            self::addFacturaToArrayDataProvider($detalle, $arrayDataProvider);
        }
    }

    return $arrayDataProvider;
}

    /**
     * @param DetalleCompraReporte|DetalleVentaReporte|ActiveRecord $detalle
     * @param array $arrayDataProvider
     */
    private function insertCuentaToArray(&$detalle, &$arrayDataProvider)
{
    $detalles = [];
    $detalles[] = $detalle;
    $arrayDataProvider[$detalle->plan_cuenta_id] = [
        'detalles' => $detalles
    ];
}

    /**
     * @param DetalleCompraReporte|DetalleVentaReporte|ActiveRecord $detalle
     * @param array $arrayDataProvider
     */
    private function addFacturaToArrayDataProvider(&$detalle, &$arrayDataProvider)
{
    $arrayDataProvider[$detalle->plan_cuenta_id]['detalles'][] = $detalle;
}


    /** <<<<<<<<<<<<<<<<<< REPORTE POR ENTIDAD >>>>>>>>>>>>>>>>>>> */

    /**
     * Reporte por Entidad.
     *
     * Se cotiza en la vista.
     *
     * @param $operacion
     * @return mixed|string
     */

    public function actionReporteLibroPorEntidad($operacion)
{
    $model = new Reporte();
    $model->ascDesc = SORT_ASC;
    $model->filtro = 'id';
    $model->moneda = 0; //todos.
    $model->valorizar = 'si';

    if ($model->load(Yii::$app->request->post())) {
        try {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 300);//5 min
            ini_set('pcre.backtrack_limit', 10000000);

            // Procesar datos del post
            $id_desde = $model->cod_desde;
            $id_hasta = $model->cod_hasta;
            $desde = '';
            $hasta = '';
            if ($model->fecha_rango != '') {
                $rango = explode(' - ', $model->fecha_rango);
                $desde = implode('-', array_reverse(explode('-', $rango[0])));
                $hasta = implode('-', array_reverse(explode('-', $rango[1])));
            }

            // Generar array de datos
            $data = self::generateReporteByEntidad(
                $operacion, $id_desde, $id_hasta, $desde, $hasta, $model);
            $arrayDataProvider = $data[0];
            $facturas_id = $data[1];
            $fechas = $data[2];
            /** ========================= TEST DE IMPRESION ========================== */
//                foreach ($arrayDataProvider as $cuenta_id => $detalles) {
//                    $entidad = Entidad::findOne(['id' => $cuenta_id]);
//                    if ($cuenta_id == 'anulada') continue;
//                    echo ($operacion == 'compra') ? "PROVEEDOR:" : "CLIENTE:" . " {$entidad->ruc} {$entidad->razon_social}<br/>";
//                    foreach ($detalles as $detalle) {
//                        echo "&nbsp;&nbsp;&nbsp;-Gravada: {$detalle['gravada10']}, Impuesto: {$detalle['iva10']}, Excenta: {$detalle['exenta']}<br/>";
//                    }
//                }
//                exit();
            /** ========================= TEST DE IMPRESION ========================== */


            // Crear archivos para exportar.
            if ($model->tipo_doc == 'xls') {
                $encabezado = [
                    'tipoLibro' => ucfirst($operacion),
                    'id_desde' => ($id_desde != '') ? $id_desde : $facturas_id[0],
                    'id_hasta' => ($id_hasta != '') ? $id_hasta : $facturas_id[sizeof($facturas_id) - 1],
                    'desde' => ($model->fecha_rango != '') ? $rango[0] : implode('-', array_reverse(explode('-', $fechas[0]))),
                    'hasta' => ($model->fecha_rango != '') ? $rango[1] : implode('-', array_reverse(explode('-', $fechas[sizeof($fechas) - 1]))),
                ];

                // Crear objeto excel
                $path = '/modules/contabilidad/views/reporte/libro-por-entidad/';
                $name = (!$model->resumido) ? 'template.xls' : 'template_resumido.xls';
                $objPHPExcel = ExcelHelpers::generatePHPExcelObject($path, $name);

                // Rellenar cabecera del excel
                $empresa = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')]);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, $encabezado['desde']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, $encabezado['hasta']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 2, $encabezado['id_desde']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 3, $encabezado['id_hasta']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow((!$model->resumido) ? 10 : 7, 2, $empresa->razon_social);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow((!$model->resumido) ? 10 : 7, 3, "{$empresa->ruc}-{$empresa->digito_verificador}");

                // Rellenar cuerpo excel
                self::createExcelEntidad($objPHPExcel, $arrayDataProvider);

                // Exportar
                $name = 'Reporte de ' . ucfirst($operacion) . ' por entidad' . ($model->resumido ? ' - resumido ' : ' ');
                ExcelHelpers::exportToBrowser($objPHPExcel, $name);
            } else {
                $path = 'modules\contabilidad\views\reporte\libro-por-entidad';
                $path = str_replace('\\', '/', $path);
                $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/reporte.css");
                $contenido = Yii::$app->view->renderFile("@app/" . $path . "/reporte.php", [
                    'dataProvider' => $arrayDataProvider,
                    'model' => $model,
                ]);
                $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/reporte_header.php", [
                    'tipoLibro' => ucfirst($operacion),
                    'id_desde' => ($id_desde != '') ? $id_desde : $facturas_id[0],
                    'id_hasta' => ($id_hasta != '') ? $id_hasta : $facturas_id[sizeof($facturas_id) - 1],
                    'desde' => ($model->fecha_rango != '') ? $rango[0] : implode('-', array_reverse(explode('-', $fechas[0]))),
                    'hasta' => ($model->fecha_rango != '') ? $rango[1] : implode('-', array_reverse(explode('-', $fechas[sizeof($fechas) - 1]))),
                ]);
                $pie = Yii::$app->view->renderFile("@app/" . $path . "/reporte_pie.php", []);
                $op = ucfirst($operacion);

                // http response
                $response = Yii::$app->response;
                $response->format = Response::FORMAT_RAW;
                $headers = Yii::$app->response->headers;
                $headers->add('Content-Type', 'application/pdf');
                $name = "Reporte Libro {$op} por Entidad.pdf";

                $pdf = new Pdf([
                    'mode' => Pdf::DEST_BROWSER,
                    'orientation' => Pdf::ORIENT_LANDSCAPE,
                    'format' => $model->paper_size,
                    'marginTop' => 36,
                    'marginBottom' => 14,
                    'marginLeft' => 5,
                    'marginRight' => 5,
                    'content' => $contenido,
                    'cssInline' => $cssInline,
                    'filename' => $name,
                    'options' => [
                        'title' => "",
                        'subject' => "",
                        'defaultheaderline' => false
                    ],
                    'methods' => [
                        'SetHeader' => [$encabezado],
                        'SetFooter' => [$pie],
                        'SetTitle' => [$name]
                    ]
                ]);

                Yii::$app->response->format = Response::FORMAT_RAW;
                Yii::$app->response->headers->add('Content-Type', 'application/pdf');
                return $pdf->render();
            }
        } catch (Exception $exception) {
            throw $exception;
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

    }

    return $this->render('libro-por-entidad/_form', [
        'model' => $model,
    ]);
}

    /**
     * @param string $operacion
     * @param null $id_desde
     * @param null $id_hasta
     * @param null $fechaDesde
     * @param null $fechaHasta
     * @param Reporte $model
     * @return array
     * @throws \yii\db\Exception
     */
    private function generateReporteByEntidad(
    $operacion = 'compra', $id_desde = null, $id_hasta = null, $fechaDesde = null, $fechaHasta = null, $model = null)
{
    $empresa_id = Yii::$app->session->get('core_empresa_actual');
    $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
    $query = ($operacion == 'compra') ? Compra::find() : Venta::find();
    $query->alias('factura');
    $query->joinWith('entidad as entidad');
    $query->select([
        'factura.*', 'entidad.ruc', 'entidad.razon_social'
    ]);
    $query->where(['=', 'factura.estado', 'vigente']);
    $query->andfilterWhere(['BETWEEN', 'factura.fecha_emision', $fechaDesde, $fechaHasta]);
    $query->andFilterWhere(['>=', 'factura.id', $id_desde]);
    $query->andFilterWhere(['<=', 'factura.id', $id_hasta]);
    $query->andFilterWhere(['AND', ['factura.empresa_id' => $empresa_id], ['factura.periodo_contable_id' => $periodo_id]]);
    $query->andFilterWhere(['IN', 'factura.entidad_id', $model->_entidades]);

    if ($model->moneda != 0)
        $query->andWhere(['factura.moneda_id' => $model->moneda]);

    // Unir con ventas anuladas.
    if ($operacion == 'venta') {
        $query2 = "SELECT factura.*, entidad.ruc, entidad.razon_social
FROM cont_factura_{$operacion} AS factura
LEFT JOIN cont_entidad AS entidad ON factura.entidad_id = entidad.id
WHERE factura.empresa_id = {$empresa_id} AND factura.periodo_contable_id = {$periodo_id} AND factura.estado = 'anulada'";
        $query->union($query2);
    }

    // Unir con ventas faltantes. Aun no pidieron.

    if (!$query->exists())
        return [];

    $orderBy = null;
    switch ($model->filtro) {
        case 'ruc':
            $orderBy = ['entidad.ruc' => $model->ascDesc];
            break;
        case 'razon_social':
            $orderBy = ['entidad.razon_social' => $model->ascDesc];
            break;
        case 'id':
            $orderBy = ['factura.id' => $model->ascDesc];
            break;
        case 'modificado':
            $orderBy = ['factura.modificado' => $model->ascDesc];
            break;
        case 'creado':
            $orderBy = ['factura.creado' => $model->ascDesc];
            break;
        case 'fecha_emision':
            $orderBy = ['factura.fecha_emision' => $model->ascDesc];
            break;
        case 'nro_factura':
            if ($operacion == 'compra')
                $orderBy = ['factura.nro_factura' => $model->ascDesc];
            else
                $orderBy = ["CONCAT(factura.prefijo, ('-'), factura.nro_factura)" => $model->ascDesc];
            break;
    }

    $query->orderBy(array_merge(['factura.entidad_id' => SORT_ASC], $orderBy));

    /** @var Compra[] | Venta[] $facturas */
    $cant = $query->count();
    $sql = $query->createCommand()->rawSql;
    Yii::warning("Cantidad de registros: $cant");
    Yii::warning("SQL: $sql");
    $facturas = $query->asArray()->all();
    $factura_ids = [];
    $fechas = [];
    foreach ($facturas as $factura) {
        $factura_ids[] = $factura['id'];
        $fechas[] = $factura['fecha_emision'];
    }
    sort($factura_ids);
    sort($fechas);

    $iva5 = Iva::findOne(['porcentaje' => 5]);
    $iva10 = Iva::findOne(['porcentaje' => 10]);
    $ivaCta5 = IvaCuenta::findOne(['iva_id' => $iva5->id]);
    $ivaCta10 = IvaCuenta::findOne(['iva_id' => $iva10->id]);
    $tableName = "cont_factura_{$operacion}";
    $arrayDataProvider = [];
    $plantillas_descuento = [];
    foreach (PlantillaCompraventa::find()->where(['tipo' => $operacion, 'para_descuento' => 'si'])->all() as $item) {
        $plantillas_descuento[] = $item->id;
    }
    $plantillas_descuento = implode(', ', $plantillas_descuento);
    foreach ($facturas as $factura) {
        $facturas_id = [];
        $facturas_id[] = $factura['id'];
        $facturas_id = implode(', ', $facturas_id);

        $total = "SELECT total from {$tableName} where id in ({$facturas_id})";
        if (strlen($plantillas_descuento) != 0) {
            $gravada10 = "SELECT SUM(IF(iva_cta_id = {$ivaCta10->id} AND (plantilla_id NOT IN ({$plantillas_descuento})), (monto / 1.1), IF(iva_cta_id = {$ivaCta10->id} AND (plantilla_id IN ({$plantillas_descuento})), (monto / -1.1), 0))) from {$tableName}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
            $iva10 = "SELECT SUM(IF(iva_cta_id = {$ivaCta10->id} AND (plantilla_id NOT IN ({$plantillas_descuento})), (monto / 11), IF(iva_cta_id = {$ivaCta10->id} AND (plantilla_id IN ({$plantillas_descuento})), (monto / -11), 0))) from cont_factura_{$operacion}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
            $gravada5 = "SELECT SUM(IF(iva_cta_id = {$ivaCta5->id} AND (plantilla_id not in ({$plantillas_descuento})), (monto / 1.05), IF(iva_cta_id = {$ivaCta5->id} AND (plantilla_id in ({$plantillas_descuento})), (monto / -1.05), 0))) from cont_factura_{$operacion}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
            $iva5 = "SELECT SUM(IF(iva_cta_id = {$ivaCta5->id} AND (plantilla_id NOT IN ({$plantillas_descuento})), (monto / 21), IF(iva_cta_id = {$ivaCta5->id} AND (plantilla_id IN ({$plantillas_descuento})), (monto / -21), 0))) from cont_factura_{$operacion}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
            $exenta = "SELECT SUM(IF(iva_cta_id IS NULL AND (plantilla_id NOT IN ({$plantillas_descuento})), monto, IF(iva_cta_id IS NULL AND (plantilla_id IN ({$plantillas_descuento})), -monto, 0))) from cont_factura_{$operacion}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
        } else {
            $gravada10 = "SELECT SUM(IF(iva_cta_id = {$ivaCta10->id}, (monto / 1.1), 0)) from {$tableName}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
            $iva10 = "SELECT SUM(IF(iva_cta_id = {$ivaCta10->id} , (monto / 11), 0)) from cont_factura_{$operacion}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
            $gravada5 = "SELECT SUM(IF(iva_cta_id = {$ivaCta5->id} , (monto / 1.05), 0)) from cont_factura_{$operacion}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
            $iva5 = "SELECT SUM(IF(iva_cta_id = {$ivaCta5->id} , (monto / 21), 0)) from cont_factura_{$operacion}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
            $exenta = "SELECT SUM(IF(iva_cta_id IS NULL , monto, 0)) from cont_factura_{$operacion}_iva_cuenta_usada where factura_{$operacion}_id in ({$facturas_id})";
        }

        // No se necesita separar gravadas e impuestos en 10 y 5 para este reporte pero se hizo asi para construir
        // inline-queries mas simples y poder calcular a la vez la gravada y su impuesto y generar como columnas
        // en el resultSet. Desde la vista se suman las gravadas y los impuestos correspondientes entre si
        // en una sola gravada e impuesto.
        $select = [
            'total' => "({$total})",
            'gravada10' => "({$gravada10})",
            'iva10' => "({$iva10})",
            'gravada5' => "({$gravada5})",
            'iva5' => "({$iva5})",
            'exenta' => "({$exenta})",
        ];
        $query = new Query();
        $query->select($select);
        $data = $query->createCommand()->queryAll()[0];
        $data = array_merge($data, ['factura' => $factura]);
        if ($factura['estado'] == 'vigente')
            $arrayDataProvider[$factura['entidad_id']][] = $data;
        else
            $arrayDataProvider['anulada'][] = $data;
    }

//        $count = 1;
//        foreach ($arrayDataProvider as $entidad_id => $facturas) {
//            echo "$count. Entidad id: $entidad_id<br/>";
//            echo "-- " . print_r($facturas) . '<br/><br/>';
//            $count++;
//        }
//
//        die();
    return [$arrayDataProvider, $factura_ids, $fechas];
}

    /**
     * @param Spreadsheet $objPHPExcel
     * @param $arrayDataProvider
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function createExcelEntidad(&$objPHPExcel, $arrayDataProvider)
{
    $fila = 5;
    $operacion = Yii::$app->request->getQueryParam('operacion');
    foreach ($arrayDataProvider as $entidad_id => $facturas) {
        if ($entidad_id == 'anulada') continue;

        $fila++;
        $column = 1;

        $cant = 0;
        $gravadas = 0;
        $impuestos = 0;
        $exentas = 0;
        $ret_ivas = 0;
        $ret_rentas = 0;
        $totales = 0;
        $base_imps = 0;
        $entidad = Entidad::findOne(['id' => $entidad_id]);

        // Fila de cuenta
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
        ];
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column, $fila)
            ->setValueExplicit(($operacion == 'compra') ? "PROVEEDOR:" : "CLIENTE:", ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->mergeCells("B{$fila}:C{$fila}");
        $objPHPExcel->getActiveSheet()
            ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $fila)->getCoordinate())
            ->getFont()->getColor()->setARGB('CE181E');
        $objPHPExcel->getActiveSheet()
            ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $fila)->getCoordinate())
            ->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()
            ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $fila)->getCoordinate())
            ->getFont()->setSize(8);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column + 1, $fila)
            ->setValueExplicit("{$entidad->ruc} - {$entidad->razon_social}", ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column + 1, $fila)->getCoordinate())
            ->getAlignment()->setHorizontal('left');
        $objPHPExcel->getActiveSheet()
            ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column + 1, $fila)->getCoordinate())
            ->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()
            ->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column + 1, $fila)->getCoordinate())
            ->getFont()->setSize(8);
        $fila++;

//            echo ($operacion == 'compra') ? "PROVEEDOR:" : "CLIENTE:" . " {$entidad->ruc} {$entidad->razon_social}<br/>";

        // Filas de facturas
        foreach ($facturas as $key => $facturaData) {
            $factura = $facturaData['factura'];
            $cotizacion = ($operacion == 'compra') ? $factura['cotizacion'] : $factura['valor_moneda'];
            $cotizacion = ($cotizacion == '') ? $cotizacion = 1 : $cotizacion;

            $nro = ($operacion == 'compra') ? $factura['nro_factura'] : "{$factura['prefijo']}-{$factura['nro_factura']}";
            $fecha = $factura['fecha_emision'];
            $fecha = implode('-', array_reverse(explode('-', $fecha)));
            $gravada10 = $facturaData['gravada10'] * $cotizacion;
            $gravada5 = $facturaData['gravada5'] * $cotizacion;
            $iva10 = $facturaData['iva10'] * $cotizacion;
            $iva5 = $facturaData['iva5'] * $cotizacion;
            $gravada = $gravada10 + $gravada5;
            $gravadas += $gravada;
            $porcentaje = ($iva10 > 0) ? '10' . (($iva5 > 0) ? ', 5' : '') : (($iva5 > 0) ? '5' : '');
            $impuesto = $iva5 + $iva10;
            $impuestos += $impuesto;
            $exenta = $facturaData['exenta'] * $cotizacion;
            $exentas += $exenta;
            $ret_iva = 0;
            $ret_ivas += $ret_iva;
            $ret_renta = 0;
            $ret_rentas += $ret_renta;
            $total = $facturaData['total'] * $cotizacion;
            $totales += $total;
            $base_imp = 0;
            $base_imps += $base_imp;

            {
                // FORMAT NUMBERS FOR VIEW
                $precision = ($factura['moneda_id'] != \common\models\ParametroSistema::getMonedaBaseId()) ? 2 : 0;

                $gravada = number_format($gravada, $precision, ',', '.');
                $impuesto = number_format($impuesto, $precision, ',', '.');
                $exenta = number_format($exenta, $precision, ',', '.');
                $total = number_format($total, $precision, ',', '.');
                $ret_iva = number_format($ret_iva, $precision, ',', '.');
                $ret_renta = number_format($ret_renta, $precision, ',', '.');
                $base_imp = number_format($base_imps, $precision, ',', '.');
            }

//                echo "&nbsp;&nbsp;&nbsp;-Gravada: $gravada, Impuesto: $impuesto, Porcentaje: $porcentaje, Excenta: $exenta<br/>";

            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($nro, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($fecha, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($entidad->razon_social, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($entidad->ruc, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getStyle("D{$fila}")->getAlignment()->setHorizontal('center');
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($gravada, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($porcentaje, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($impuesto, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($exenta, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(0, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(0, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit($total, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()
                ->getCellByColumnAndRow($column++, $fila)
                ->setValueExplicit(0, ExcelDataType::TYPE_STRING);

            $cant++;
            $fila++;
            $column = 1;
        }
        $gravadas = number_format($gravadas, 0, ',', '.');
        $impuestos = number_format($impuestos, 0, ',', '.');
        $exentas = number_format($exentas, 0, ',', '.');
        $ret_ivas = number_format($ret_ivas, 0, ',', '.');
        $ret_rentas = number_format($ret_rentas, 0, ',', '.');
        $totales = number_format($totales, 0, ',', '.');
        $base_imps = number_format($base_imps, 0, ',', '.');

        // Fila de resumen por entidad
        $column = 1;
        $styleArray = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $objPHPExcel->getActiveSheet()->mergeCells("A{$fila}:B{$fila}");
        $objPHPExcel->getActiveSheet()->getStyle("A{$fila}:L{$fila}")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle("A{$fila}:L{$fila}")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit("Items", ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($cant, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getStyle("C{$fila}:D{$fila}")->getAlignment()->setHorizontal('center');
        $objPHPExcel->getActiveSheet()->getStyle("C{$fila}:D{$fila}")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit("Sumas", ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($gravadas, ExcelDataType::TYPE_STRING);
        $column++;
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($impuestos, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($exentas, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($ret_ivas, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($ret_rentas, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($totales, ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()
            ->getCellByColumnAndRow($column++, $fila)
            ->setValueExplicit($base_imps, ExcelDataType::TYPE_STRING);

//            echo "<br/>";
    }
}

    /** <<<<<<<<<<<<<<<<<< AUDITORIA VENTAS >>>>>>>>>>>>>>>>>>> */

    public function actionAuditoriaVenta()
{
    $model = new Reporte();

    if (!Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        try {
            $a = microtime(true);
            $ventas = self::auditarVenta($model);
            $b = microtime(true);
//                Yii::warning(($b - $a) . ' data provider function');
            $data = self::rellenarNrosNoUsados($ventas);

            if (!sizeof($data)) {
                throw new Exception("No se han encontrado resultados.");
            }

            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 300);//5 min
            ini_set('pcre.backtrack_limit', 100000000);

            $path = 'modules\contabilidad\views\reporte\auditoria-venta';
            $path = str_replace('\\', '/', $path);
            $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/css.css");
            $contenido = Yii::$app->view->renderFile("@app/" . $path . "/pdf.php", [
                'dataProvider' => $data[0],
                'userByFactura' => $data[1],
            ]);
            $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/header.php", [

            ]);
            $pie = Yii::$app->view->renderFile("@app/" . $path . "/pie.php", []);

            // http response
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_RAW;
            $headers = Yii::$app->response->headers;
            $headers->add('Content-Type', 'application/pdf');
            $name = "Auditoria venta.pdf";

            $pdf = new Pdf([
                'mode' => Pdf::DEST_FILE,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'format' => Pdf::FORMAT_A4,
                'marginTop' => 23,
                'marginBottom' => 15,
                'marginLeft' => 5,
                'marginRight' => 5,
                'content' => $contenido,
                'cssInline' => $cssInline,
                'filename' => $name,
                'options' => [
                    'title' => "",
                    'subject' => "",
                    'defaultheaderline' => false
                ],
                'methods' => [
                    'SetHeader' => [$encabezado],
                    'SetFooter' => [$pie],
                    'SetTitle' => [$name]
                ]
            ]);

            Yii::$app->response->format = Response::FORMAT_RAW;
            Yii::$app->response->headers->add('Content-Type', 'application/pdf');
            $pdf->getApi()->useSubstitutions = false;
            $pdf->getApi()->simpleTables = true;
            $c = microtime(true);
            $response = $pdf->render();
            $d = microtime(true);
//                echo ($c - $b) . " action<br/><br/><br/>";
//                Yii::warning(($c - $b) . ' pdf construccion.');
//                Yii::warning(($d - $c) . ' response construcction.');
            return $response;
        } catch (Exception $exception) {
//                throw $exception;
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }
    }

    return $this->render('auditoria-venta/_form', [
        'model' => $model,
    ]);
}

    /**
     * @param Reporte $model
     * @return array
     */
    private function auditarVenta($model)
{
    $empresa = Yii::$app->session->get('core_empresa_actual');
    $periodo = Yii::$app->session->get('core_empresa_actual_pc');

    // Procesar datos del post
    $id_desde = $model->cod_desde;
    $id_hasta = $model->cod_hasta;
    $desde = '';
    $hasta = '';
    if ($model->fecha_rango != '') {
        $rango = explode(' - ', $model->fecha_rango);
        $desde = implode('-', array_reverse(explode('-', $rango[0])));
        $hasta = implode('-', array_reverse(explode('-', $rango[1])));
    }

    #Obtener en formato de filas, todas las ventas de la empresa y periodo actuales.
    #Se añaden columnas calculadas/elaboradas para facilitar su post-procesamiento.
    $nroFactura = "CONCAT(fa.prefijo, ('-'), fa.nro_factura)";
    $monedaBase = ParametroSistema::getMonedaBaseId();
    $select = [
        "fa.id",
        "$nroFactura as nro_factura",
        'fa.timbrado_detalle_id',
        'ti.nro_timbrado',
        'fa.fecha_emision',
        'en.razon_social',
        "(fa.total * IF(fa.moneda_id != {$monedaBase}, fa.valor_moneda, 1)) AS total",
        "('no') as agregado",
    ];
    $table = Venta::tableName();
    $query = new Query();
    $query->select($select);
    $query->from("$table as fa");
    $query->leftJoin('cont_entidad en', 'en.id = fa.entidad_id');
    $query->leftJoin('cont_timbrado_detalle tide', 'tide.id = fa.timbrado_detalle_id');
    $query->leftJoin('cont_timbrado ti', 'tide.timbrado_id = ti.id');
    $query->where(['fa.empresa_id' => $empresa, 'fa.periodo_contable_id' => $periodo, 'tipo' => 'factura']);
    $query->andFilterWhere(['>=', 'fa.id', $id_desde]);
    $query->andFilterWhere(['<=', 'fa.id', $id_hasta]);
    $query->andFilterWhere(['>=', 'fa.fecha_emision', $desde]);
    $query->andFilterWhere(['<=', 'fa.fecha_emision', $hasta]);
    $query->orderBy([$nroFactura => SORT_ASC]);

    $result = $query->all();
    return $result;
}

    private function deleteFromUniverse($numberUsed, &$universe)
{
    if (($key = array_search($numberUsed, $universe)) !== false) {
        unset($universe[$key]);
    }
}

    private function rellenarNrosNoUsados($ventas)
{
    if (!sizeof($ventas)) return [];

    $a = microtime(true);
    #Listar todos los numeros de timbrados utilizados.
    $timbradoNros = array_values(array_unique(array_map(function ($elem) {
        return $elem['nro_timbrado'];
    }, $ventas)));

    #Listar todos los IDs de ventas.
    $factura_ids = array_values(array_unique(array_map(function ($elem) {
        return $elem['id'];
    }, $ventas)));
    $factura_ids = implode(', ', $factura_ids);

    #Listar todos los usuarios que crearon estas ventas
    $sql = "SELECT model_id, core_usuario.username from audit_trail left join core_usuario on audit_trail.user_id = core_usuario.id where audit_trail.model like 'backend_\modules_\contabilidad_\models_\Venta' and CAST(audit_trail.model_id as UNSIGNED) in ({$factura_ids}) AND audit_trail.action = 'CREATE' and field = 'id'";
    $ventaCreators = Yii::$app->db->createCommand($sql)->queryAll();
    $b = microtime(true);

    #Obtener el universo de numeros por timbrado y detalle (talonario). No importa el prefijo.
    $talonarioUniverse = [];
    foreach ($timbradoNros as $timbradoNro) {
        foreach (TimbradoDetalle::findAll(['timbrado_id' => Timbrado::findOne(['nro_timbrado' => $timbradoNro])->id]) as $detalle) {
            if ($detalle->nro_inicio == $detalle->nro_fin) continue; // posiblemente es un timbrado detalle para compras.

            #Crear universo del numero de talonario por timbrado.
            $talonarioUniverse[$timbradoNro][$detalle->id] = range($detalle->nro_inicio, $detalle->nro_fin);
        }
    }

    #Remover el nro de factura utilizado del universo de talonario correspondiente.
    # `$ventaUniverse` servira como universo de ventas (existentes y ficticias con numeros aun no utilizados).
    $ventaUniverse = [];
    foreach ($ventas as $venta) {
        $nro_factura = $venta['nro_factura'];
        $timbradoNro = $venta['nro_timbrado'];
        $timbrado_detalle_id = $venta['timbrado_detalle_id'];
        $slices = explode('-', $nro_factura);
        $prefijo = "{$slices[0]}-{$slices[1]}";
        $last7 = (int)$slices[2];

        #Añadir ventas existentes al universo.
        $ventaUniverse[] = $venta;

        #Remover nro usado y actualizar universo.
        self::deleteFromUniverse($last7, $talonarioUniverse[$timbradoNro][$timbrado_detalle_id]);
    }

    #En este punto, en el universo de talonarios hay solamente numeros aun no utilizados.

    #Anhadir ventas ficticias con nros aun no usados al universo de ventas.
    #Se incrustan filas virtuales de ventas.
    foreach ($talonarioUniverse as $timbradoNro => $timbDetalleIDs) {
        foreach ($timbDetalleIDs as $timDetId => $range) {
            $prefijo = TimbradoDetalle::findOne(['id' => $timDetId])->prefijo;

            foreach ($range as $nro_no_usado) {
                #Construir nro de venta en formato completo.
                $nro_factura = "$prefijo-" . str_pad($nro_no_usado, 7, '0', STR_PAD_LEFT);

                #Construir e insertar venta virtual. El numero de esta venta aun no se ha utilizado.
                $ventaUniverse[] = [
                    "id" => "",
                    "nro_factura" => $nro_factura,
                    'timbrado_detalle_id' => $timDetId,
                    'nro_timbrado' => $timbradoNro,
                    'fecha_emision' => 'NO-ENCONTRADO',
                    'razon_social' => 'NO-ENCONTRADO',
                    "total" => 'NO-ENCONTRADO',
                    "agregado" => "si",
                ];
            }
        }
    }

    #Ordenar el universo de ventas segun nro_factura.
    usort($ventaUniverse, function ($item1, $item2) {
        return $item1['nro_factura'] <=> $item2['nro_factura'];
    });

    #Mapear los ids de ventas existentes a sus usuarios creadores.
    $ventaCreators = ArrayHelper::map($ventaCreators, 'model_id', 'username');
    $c = microtime(true);
//        echo ($b - $a) . " audit trail<br/><br/><br/>";
//        echo ($c - $b) . " despues de audittrail y antes del return<br/><br/><br/>";
//        Yii::warning(($b - $a) . " audit consult query time");
//        Yii::warning(($c - $b) . " rest of the rellenarNrosNoUsados()");
    return [$ventaUniverse, $ventaCreators];
}
}

/**
 * Class DetalleCompraReporte
 * @package backend\modules\contabilidad\controllers
 *
 * @property int $iva_cuenta_id
 * @property array $porcentajes
 * @property IvaCuenta $ivaCuenta
 * @property Compra $facturaCompra
 * @property PlanCuenta $planCuenta
 */
class DetalleCompraReporte extends DetalleCompra
{
    public $iva_cuenta_id;
    public $porcentajes = [];

    /**
     * @return IvaCuenta|null
     */
    public function getIvaCuenta()
    {
        if ($this->iva_cuenta_id == '' or !isset($this->iva_cuenta_id)) return null;

        return IvaCuenta::findOne(['id' => $this->iva_cuenta_id]);
    }

    /**
     * @return Compra|\yii\db\ActiveQuery|null
     */
    public function getFacturaCompra()
    {
        if (!isset($this->factura_compra_id) || $this->factura_compra_id == '') return null;

        return Compra::findOne(['id' => $this->factura_compra_id]);
    }

    public function getPlanCuenta()
    {
        if (!isset($this->plan_cuenta_id) || $this->plan_cuenta_id == '') return null;

        return PlanCuenta::findOne(['id' => $this->plan_cuenta_id]);
    }
}

/**
 * Class DetalleVentaReporte
 * @package backend\modules\contabilidad\controllers
 *
 * @property int $iva_cuenta_id
 * @property array $porcentajes
 * @property IvaCuenta $ivaCuenta
 * @property Venta $venta
 * @property PlanCuenta $planCuenta
 */
class DetalleVentaReporte extends DetalleVenta
{
    public $iva_cuenta_id;
    public $porcentajes = [];

    /**
     * @return IvaCuenta|null
     */
    public function getIvaCuenta()
    {
        if ($this->iva_cuenta_id == '' or !isset($this->iva_cuenta_id)) return null;

        return IvaCuenta::findOne(['id' => $this->iva_cuenta_id]);
    }

    /**
     * @return Venta|\yii\db\ActiveQuery|null
     */
    public function getVenta()
    {
        if ($this->factura_venta_id == '' or !isset($this->factura_venta_id)) return null;

        return Venta::findOne(['id' => $this->factura_venta_id]);
    }

    public function getPlanCuenta()
    {
        if (!isset($this->plan_cuenta_id) || $this->plan_cuenta_id == '') return null;

        return PlanCuenta::findOne(['id' => $this->plan_cuenta_id]);
    }
}

/**
 * @property string $filtro
 * @property string $operacion
 * @property string $paper_size
 * @property string $fecha_rango
 * @property string $moneda
 * @property string $cod_desde
 * @property string $cod_hasta
 * @property string $valorizar
 * @property string $agrupar
 * @property string $creado
 * @property string $modificado // se usa dos campos diferentes por si se quiera filtrar por los dos en un futuro.
 * @property string $ascDesc
 * @property string $resumen_plantilla
 * @property boolean $resumido
 * @property array $obligaciones
 *
 * @property string $entidad_id;
 * @property string $cuentas;
 * @property array $tipo_comprobante;
 * @property string $groupByIva;
 * @property string $groupByCuenta;
 * @property string $para_iva;
 * @property string $incluir_no_vigente;
 */
class Reporte extends Model
{
    public $filtro;
    public $operacion;
    public $paper_size;
    public $fecha_rango;
    public $moneda;
    public $cod_desde;
    public $cod_hasta;
    public $valorizar;
    public $agrupar;
    public $tipo_doc;
    public $creado;
    public $modificado;
    public $ascDesc;
    public $resumen_plantilla;
    public $incluir_no_vigente = 'si';

    // segun observaciones JMG 28/11/2018
    public $entidad_id;
    public $cuentas;
    public $groupByCuenta; // true, false
    public $groupByIva;
    public $tipo_comprobante; // factura, nota de credito, ambos, ambos agrupados (primero facturas y luego notas).
    public $para_iva; // desde 26 de diciembre, por tema de impuesto a la renta, las facturas pueden ser para iva o para renta.

    // Usado en el reporte por cuenta
    public $resumido = false;
    public $_cuentas = [];
    public $_entidades = [];

    //usado en el reporte por factura
    public $obligaciones = [];

    #######################

    public $criterio1;
    public $criterio2;
    public $criterio3;

    public $criterio1fecha;
    public $criterio2fecha;
    public $criterio3fecha;

    public $separar_visualmente;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['filtro', 'operacion', 'paper_size', 'fecha_rango', 'moneda', 'cod_desde', 'cod_hasta', 'valorizar',
                    'agrupar', 'tipo_doc', 'entidad_id', 'cuentas', 'groupByCuenta', 'groupByIva', 'para_iva', 'resumen_plantilla', 'incluir_no_vigente'], 'string'
            ],
            [['tipo_comprobante'], 'required'],
            [['creado', 'modificado', 'ascDesc', 'para_iva', 'resumido', '_cuentas', '_entidades', 'obligaciones', 'incluir_no_vigente'], 'safe'],

            [['criterio1', 'criterio2', 'criterio3', 'separar_visualmente'], 'string'],
            [['criterio1', 'criterio2', 'criterio3', 'separar_visualmente'], ReporteValidator::class],
            [['criterio1fecha', 'criterio2fecha', 'criterio3fecha'], ReporteValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'filtro' => 'Ordenar por',
            'paper_size' => 'Tamaño de hoja',
            'fecha_rango' => 'Filtrar por Fecha Rango',
            'moneda' => 'Moneda',
            'cod_desde' => "Cod. Desde",
            'cod_hasta' => "Cod. Hasta",
            'valorizar' => "Valorizar?",
            'agrupar' => "Agrupar por prefijo?",
            'tipo_doc' => "Tipo de Documento",
            'entidad_id' => "Entidad",
            'cuentas' => "Cuentas",
            'groupByCuenta' => "Separar por Cuenta Principal?",
            'groupByIva' => "Agrupar por Iva?",
            'tipo_comprobante' => "Tipos de Comprobante (admite varios)",
            'creado' => 'Fecha de Creación',
            'modificado' => 'Fecha de Última Modificación',
            'ascDesc' => 'Ascendente / Descendente',
            'para_iva' => "Para I.V.A. ?",
            'criterio1' => 'Criterio 1',
            'criterio2' => 'Criterio 2',
            'criterio3' => 'Criterio 3',
            'criterio1fecha' => 'Rango Fecha Criterio 1',
            'criterio2fecha' => 'Rango Fecha Criterio 2',
            'criterio3fecha' => 'Rango Fecha Criterio 3',
        ];
    }

    public static function getPaperSizes()
    {
        $array = [];
        $i = 1;
        // (a4, oficio, oficiopy)
        $array[Pdf::FORMAT_A4] = ($i++) . " - A4";
        $array[Pdf::FORMAT_FOLIO] = ($i++) . " - Oficio";
        return $array;
    }

    public static function getMonedas($idTextFormat)
    {
        $result = [];
//        if ($idTextFormat) $result[] = ['id' => 0, 'text' => 'Todos'];
//        else $result[] = 'Todos';
        /** @var Moneda $moneda */
        foreach (Moneda::find()->all() as $moneda) {
            if ($idTextFormat) $result[] = ['id' => $moneda->id, 'text' => $moneda->nombre];
            else $result[$moneda->id] = $moneda->nombre;
        }
        return $result;
    }

    public function getErrorSummaryAsString()
    {
        return implode(', ', $this->getErrorSummary(true));
    }
}

class ReporteBalance extends Model
{
    public $fecha_desde;
    public $fecha_hasta;
//    public $nivel;
    public $tipo_doc;
    public $mostrar_codigo_cuenta;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_desde', 'fecha_hasta', 'tipo_doc', 'mostrar_codigo_cuenta'], 'string'],
            [['fecha_desde', 'fecha_hasta'], 'required'],
        ];
    }
}

class ReporteEstadoCuenta extends Model
{
    public $fecha_desde;
    public $fecha_hasta;
    public $fecha_rango;
    public $paper_size;
    public $tipo_doc;
    public $orientation;
    public $tipo;
    public $entidad_id = [];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_desde', 'fecha_hasta', 'cuentas', 'paper_size', 'fecha_rango', 'tipo_doc', 'orientation', 'tipo', 'entidad_id'], 'safe'],
            [['fecha_desde', 'fecha_hasta', 'paper_size', 'fecha_rango', 'tipo_doc', 'orientation', 'tipo'], 'string'],
            [['paper_size', 'orientation', 'tipo', 'fecha_rango'], 'required'],
        ];
    }

    public static function getPaperSizes()
    {
        $array = [];
        $i = 1;

        $array[Pdf::FORMAT_A4] = ($i++) . " - A4";
        $array[Pdf::FORMAT_FOLIO] = ($i++) . " - Oficio";
        return $array;
    }

    public static function getTipos()
    {
        $array = [];
        $i = 1;

        $array['completo'] = ($i++) . " - Completo";
        $array['resumido'] = ($i++) . " - Resumido";
        return $array;
    }

    public function separarEnDesdeHasta()
    {
        if ($this->fecha_rango != '') {
            $rango = explode(' - ', $this->fecha_rango);
            $desde = $rango[0];
            $hasta = $rango[1];

            $desde = date_create_from_format('d-m-Y', $desde);
            $hasta = date_create_from_format('d-m-Y', $hasta);

            if ($desde && $hasta) {
                $this->fecha_desde = $desde->format('Y-m-d');
                $this->fecha_hasta = $hasta->format('Y-m-d');
            }
        } else {
            $primero_enero = date('Y-m-d', strtotime("01-01-" . EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')])->anho));
            $ayer = date('Y-m-d', strtotime('-1 day'));
            $this->fecha_desde = $primero_enero;
            $this->fecha_hasta = $ayer;
        }
    }
}