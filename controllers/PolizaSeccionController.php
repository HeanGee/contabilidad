<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\PolizaSeccion;
use backend\modules\contabilidad\models\search\PolizaSeccionSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * PolizaSeccionController implements the CRUD actions for PolizaSeccion model.
 */
class PolizaSeccionController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PolizaSeccion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PolizaSeccionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PolizaSeccion model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the PolizaSeccion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PolizaSeccion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PolizaSeccion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new PolizaSeccion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($guardar_cerrar = false)
    {
        $model = new PolizaSeccion();
        $model->estado = 'activo';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            FlashMessageHelpsers::createSuccessMessage('Sección ' . $model->nombre . ' se ha guardado exitosamente.');
            if ($guardar_cerrar) return $this->redirect(['index',]);
            return $this->redirect(['create']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PolizaSeccion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index',]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PolizaSeccion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (sizeof($model->polizas) > 0)
            throw new ForbiddenHttpException('Esta sección está siendo utilizada por una o más pólizas existentes.');

        if ($model->delete())
            FlashMessageHelpsers::createSuccessMessage('Sección ' . $model->nombre . ' eliminado exitosamente.');
        else
            FlashMessageHelpsers::createWarningMessage('No se ha podido borrar la sección.');

        return $this->redirect(['index']);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }
}
