<?php

namespace backend\modules\contabilidad\controllers;

use yii\web\Controller;

/**
 * Default controller for the `contabilidad` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
