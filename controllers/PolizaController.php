<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\Poliza;
use backend\modules\contabilidad\models\PolizaDevengamientoDetalle;
use backend\modules\contabilidad\models\PolizaSeccion;
use backend\modules\contabilidad\models\search\PolizaSearch;
use common\helpers\FlashMessageHelpsers;
use kartik\form\ActiveForm;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * PolizaController implements the CRUD actions for Poliza model.
 */
class PolizaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poliza models.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $this->verificarSesion();

        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new TempModel();
        $model->fecha_hasta = array_key_exists('PolizaSearch', $_GET) ? $_GET['PolizaSearch']['fecha_hasta'] : "";

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    private function verificarSesion()
    {
        if (!\Yii::$app->session->has('core_empresa_actual') || \Yii::$app->session->get('core_empresa_actual') == "") {
            $msg = 'Falta especificar Empresa Actual.';
            FlashMessageHelpsers::createWarningMessage($msg);
            throw new NotFoundHttpException($msg);
        }
        if (!\Yii::$app->session->has('core_empresa_actual_pc') || \Yii::$app->session->get('core_empresa_actual_pc') == "") {
            $msg = 'Falta especificar Periodo Contable Actual para la Empresa Actual.';
            FlashMessageHelpsers::createWarningMessage($msg);
            throw new NotFoundHttpException($msg);
        }
        return true;
    }

    /**
     * Displays a single Poliza model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->verificarSesion();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Poliza model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Poliza the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poliza::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Poliza model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate($formodal = false)
    {
        $this->verificarSesion();
        if (!PolizaSeccion::find()->exists()) {
            FlashMessageHelpsers::createWarningMessage('Debe crear primero secciones que utilizará para las pólizas.');
            return $this->redirect(['index']);
        }
        $model = new Poliza();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            $model->importe_diario = '';
            if (!$model->validate()) {
                $msg = "";
                foreach ($model->getErrorSummary(true) as $item) {
                    $msg .= substr($item, 0, strlen($item) - 1) . ', ';
                }
                FlashMessageHelpsers::createWarningMessage(substr($msg, 0, strlen($msg) - 2) . '.');
                goto retorno;
            }
            $model->importe_diario = str_replace('.', '', $model->importe_diario);
            $model->importe_diario = (float)str_replace(',', '.', $model->importe_diario);
            $model->save(false);
            return $this->redirect(['index']);
        }

        retorno:;
        return $this->render('create', [
            'model' => $model,
            'compra_id' => null,
        ]);
    }

    /**
     * Updates an existing Poliza model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param bool $formodal
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id, $formodal = false)
    {
        $this->verificarSesion();
        $model = $this->findModel($id);
        if (!$model->esDelPeriodoYEmpresa())
            throw new NotFoundHttpException('The requested page doesn\'t exist.');

        if ($model->tieneAsiento()) {
            FlashMessageHelpsers::createWarningMessage('La poliza tiene asientos generados.');
            return $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $trans = Yii::$app->db->beginTransaction();
            try {
                $model->save(false);

                $result = ['result' => true];

                // TODO: Programar generarDevengamiento independiente del que esta en el modelo.
//                if (isset($model->factura_compra_id))
//                {
//                    $result = $model->generarDevengamientos();
//                }

                if ($result['result']) {
                    $trans->commit();
                    FlashMessageHelpsers::createSuccessMessage('La Póliza se ha borrado exitosamente.');
                    return $this->redirect(['index']);
                }
            } catch (\Exception $exception) {
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
            'compra_id' => null,
        ]);
    }

    /**
     * Deletes an existing Poliza model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->verificarSesion();
        $model = $this->findModel($id);
        $trans = Yii::$app->db->beginTransaction();

        // Verificar periodo contable
        if (!$model->esDelPeriodoYEmpresa()) {
            $trans->rollBack();
            throw new NotFoundHttpException('The requested page doesn\'t exist.');
        }

        // Verificar si existe poliza_devengamiento asentado.
        if ($model->tieneAsiento()) {
            $trans->rollBack();
            throw new ForbiddenHttpException("Existen uno o más asientos generados por esta póliza. Elimínelos primero.");
        }

        // Borrar detalles del poliza_devengamiento
        foreach ($model->devengamientos as $deveng) {
            if (!$deveng->delete()) {
                $trans->rollBack();
                throw new \Exception("Error interno: No se pudo borrar poliza_devengamiento id: " . $deveng->id);
            }
        }

        if (!$model->delete()) {
            $trans->rollBack();
            FlashMessageHelpsers::createWarningMessage('La Póliza no se pudo borrar.');
        } else {
            $trans->commit();
            FlashMessageHelpsers::createSuccessMessage('La Póliza se ha borrado exitosamente.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /** ------------------------- [INICIO] MANEJO DE POLIZA DESDE FACTURA DE COMPRA ------------------------- */
    public function actionManejarPolizaDesdeCompra($formodal, $submit = false, $compra_id = null, $poliza_id = null, $quiere_crear_nuevo = 'no')
    {
        $compra = Compra::findOne(['id' => $compra_id]);
        $model = null;
        // Si se elige una poliza de la lista en el modal.
        if ($poliza_id != null) {
            $model = Poliza::findOne(['id' => $poliza_id]);
        } else {
            $model = new Poliza();
            $model->loadDefaultValues();

            if ($quiere_crear_nuevo == 'no') {
                if (Yii::$app->session->has('cont_poliza_desde_compra')) {
                    $dataProvider = Yii::$app->session->get('cont_poliza_desde_compra');
                    /** @var Poliza $model */
                    $model = $dataProvider->allModels[0];
                }
            }
        }
        $model->poliza_id_selector = $model->id;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $submit == true) {
            $model->importe_diario = "";

            if (!$model->validate()) {
                FlashMessageHelpsers::createWarningMessage('No se pudo almacenar en la sesion temporalmente: ' . implode(', ', $model->getErrorSummary(true)));
            } else {

                // Refresca la sesion
                Yii::$app->session->remove('cont_poliza_desde_compra');
                Yii::$app->session->set('cont_poliza_desde_compra', new ArrayDataProvider([
                    'allModels' => [$model,],
                    'pagination' => false,
                ]));
            }
        }

        retorno:;
        return $this->renderAjax('create', [
            'model' => $model,
            'compra_id' => $compra_id,
        ]);
    }

    public function actionGetPolizasLibresAjax($compra_id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $empresa_id = Yii::$app->session->get("core_empresa_actual");
        $periodo_id = Yii::$app->session->get("core_empresa_actual_pc");
        $poliza = Poliza::find()
            ->where(['IS', 'factura_compra_id', null])
            ->andWhere(['empresa_id' => $empresa_id, 'periodo_contable_id' => $periodo_id])
            ->orWhere(['factura_compra_id' => $compra_id])
            ->select(['id', "CONCAT((id), (' - '), (nro_poliza)) AS text"])->asArray()->all();
        return ['results' => array_values($poliza)];
    }

    /** ------------------------- [FIN] MANEJO DE POLIZA DESDE FACTURA DE COMPRA ------------------------- */


    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws NotFoundHttpException
     * @throws \Exception
     */

    public function actionReporte()
    {
        $model = new TempModel();
        $model->load(Yii::$app->request->post());

        $fecha_hasta = date_create_from_format('d-m-Y', $model->fecha_hasta); // DateTime|false
        $fecha_hasta = $fecha_hasta ? $fecha_hasta->format('Y-m-d') : null; // string|null

        $cta_seguro_a_devengar = PlanCuenta::findOne(['id' => PlantillaCompraventa::getIdCuentaSeguroADevengar()]);
        if (!isset($cta_seguro_a_devengar)) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar o definir una plantilla marcando \'SI\' en SEGURO A DEVENGAR.');
            return $this->redirect(['index']);
        }

        $query = Poliza::find()->alias('poliza');
//        $query->leftJoin('cont_poliza_devengamiento devengamientos', 'poliza.id = devengamientos.poliza_id');
        $query->joinWith('devengamientos as devengamientos');
        $query->joinWith('seccion as seccion');
        $query->joinWith('compra as compra');
        $query->joinWith('compra.entidad as entidad');
        $query->joinWith('compra.detallesCompra as detalleCompra');
        $query->select([
            'id' => 'poliza.id',
            'seccion_id' => 'seccion.id',
            'factura_compra_id' => 'compra.id',
            'poliza_id' => 'devengamientos.poliza_id',

            'nro_poliza' => 'poliza.nro_poliza',
            'importe_diario' => 'poliza.importe_diario',
            'nro_factura' => 'compra.nro_factura',
            'fecha_de_factura' => 'compra.fecha_emision',
            'proveedor' => 'entidad.razon_social',
            'seccion' => 'seccion.nombre',
            'importe_gravado' => 'detalleCompra.subtotal', // where detalleCompra.cta_contable = 'debe'
            'desde' => 'poliza.fecha_desde',
            'hasta' => 'poliza.fecha_hasta',
            'vigencia' => "DATEDIFF(poliza.fecha_hasta, poliza.fecha_desde)",
            'devengamientos.*'
        ]);
        $query->filterWhere(['<=', 'poliza.fecha_hasta', $fecha_hasta]); // añade where si $fecha_hasta no es false/null
        $query->andWhere([
            'plan_cuenta_id' => PlantillaCompraventa::getIdCuentaSeguroADevengar(),
        ]);
        $query->andWhere(['poliza.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        $query->orderBy('devengamientos.poliza_id, devengamientos.anho_mes');

        $data = $query->asArray()->all();
        if (sizeof($data) == 0) {
            FlashMessageHelpsers::createWarningMessage('No existe ninguna póliza asociada a una factura.');
            return $this->redirect(['index']);
        }

        // Crear objetoExcel.
        /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        date_default_timezone_set('America/Asuncion');
        /** Create a new Spreadsheet Object **/
        $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/poliza/templates/';
        $objPHPExcel = IOFactory::load($carpeta . 'poliza_template.xlsx');

        // Rellenar Excel.
        $fila = 4;
        $columna = 1;
        $fila_titulo = 4;
        $max_fila = 0;
        $max_colum = 0;
        $poliza_id = 0;
        foreach ($data as $item) {
            if ($item['poliza_id'] != $poliza_id) {
                $fila++;
                $columna = 1;
                $poliza_id = $item['poliza_id']; // para completar datos del seguro una sola vez por poliza.

                // Rellenar parte estatica
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['nro_poliza']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['nro_factura']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['fecha_de_factura']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['proveedor']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['seccion']['nombre']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['importe_gravado']);
                $this->setNumberFormat($objPHPExcel, $columna - 1, $fila);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['desde']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['hasta']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['vigencia']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['importe_diario']);
                $this->setNumberFormat($objPHPExcel, $columna - 1, $fila);
            }

            if ($fila != 5) {
                $columna = 11;
                if ($item['anho_mes'] < $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila_titulo)->getValue()) {
                    $objPHPExcel->getActiveSheet()->insertNewColumnBeforeByIndex($columna);
                    $max_colum++;
                } elseif ($item['anho_mes'] > $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($max_colum, $fila_titulo)->getValue()) {
                    $columna = $max_colum + 1;
                } else {
                    while ($columna <= $max_colum) {
                        if ($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila_titulo)->getValue() == $item['anho_mes']) {
                            break;
                        }
                        if ($columna < $max_colum) {
                            if ($item['anho_mes'] > $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila_titulo)->getValue() &&
                                $item['anho_mes'] < $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna + 1, $fila_titulo)->getValue()) {
                                $objPHPExcel->getActiveSheet()->insertNewColumnBeforeByIndex($columna + 1);
                                $columna++;
                                $max_colum++;

                                break;
                            }
                        }
                        $columna++;
                    }
                }
            }

            // TODO: por devengamientos.
            if ($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila_titulo - 1)->getValue() == "")
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo - 1, $item['dias']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo, $item['anho_mes']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['monto']);
            $this->setNumberFormat($objPHPExcel, $columna - 1, $fila);

            $last_column = $columna - 1;
            if ($last_column > $max_colum) $max_colum = $last_column;
            iF ($fila > $max_fila) $max_fila = $fila;
        }

        // Colores.
        $verde = 'FF92D050';
        $naranja = 'FFF4B083';
        $celeste = "FF9CC2E5";

        // Poner borde a la tabla.
        $this->setBorder($objPHPExcel, 1, 4, $max_colum + 1, $max_fila, true);

        // Ultima columna de total
        $fila = $fila_titulo;
        $columna = $max_colum + 1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo, "TOTAL");
        $this->setBackground($objPHPExcel, $columna, $fila_titulo, $celeste);
        $fila++;
        $poliza_id = 0;
        foreach ($data as $item) {
            if ($poliza_id != $item['poliza_id']) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila, $item['importe_gravado']);
                $this->setNumberFormat($objPHPExcel, $columna, $fila);
                $fila++;
                $poliza_id = $item['poliza_id'];
            }
        }
//        // Si se quiere calcular los subtotales por fila mediante fórmulas, descomentar esta linea y comentar el algoritmo previo.
//        $fila = 5;
//        $columna = 11;
//        while ($fila <= $max_fila) {
//            $desde = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(11, $fila)->getCoordinate();
//            $hasta = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($max_colum, $fila)->getCoordinate();
//            $target = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($max_colum+1, $fila)->getCoordinate();
//            $objPHPExcel->getActiveSheet()->setCellValue($target, "=SUM({$desde}:{$hasta})");
//            $fila++;
//        }

        // Pintar fondo diferenciado del diciembre y enero
        $columna = 12;
        $fila = $fila_titulo;
        $flag = true;
        $color = $verde;
        $this->setBackground($objPHPExcel, $columna - 1, $fila_titulo, $verde);
        while ($columna <= $max_colum) {
            $actual = substr($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila_titulo)->getValue(), 5, 2);
            $anterior = substr($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna - 1, $fila_titulo)->getValue(), 5, 2);

            if ($actual == "01" && $anterior == "12") {
                $flag = !$flag;
            }

            if ($flag) {
                $this->setBackground($objPHPExcel, $columna, $fila_titulo, $verde);
            } else {
                $this->setBackground($objPHPExcel, $columna, $fila_titulo, $naranja);
            }

            $columna++;
        }

        // Generar la ultima fila de sumatoria
        $columna = 11;
        $fila = $max_fila + 1;
        while ($columna <= $max_colum + 1) {
            $desde = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, 5)->getCoordinate();
            $hasta = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $max_fila)->getCoordinate();
            $target = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $max_fila + 1)->getCoordinate();
            $objPHPExcel->getActiveSheet()->setCellValue($target, "=SUM({$desde}:{$hasta})");
            if ($columna <= $max_colum) $this->setBackground($objPHPExcel, $columna, $max_fila + 1, $naranja);
            else $this->setBackground($objPHPExcel, $columna, $max_fila + 1, $celeste);
            $this->setBorder($objPHPExcel, $columna, $max_fila + 1);
            $this->setNumberFormat($objPHPExcel, $columna, $max_fila + 1);
            $columna++;
        }

        // Cambiar los numeros de los meses a textos.
        $columna = 11;
        $fila = $fila_titulo;
        while ($columna <= $max_colum) {
            $anho_mes = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getValue();
            $texto = $this->dateToStr($anho_mes, true);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->setValue($texto);
            $columna++;
        }

        //Versión excel
        $carpeta = Yii::$app->basePath . '/web/uploads/';
        if (!is_dir($carpeta)) {
            if (!@mkdir($carpeta, 0777, true)) {
                throw new \Exception('No se puede crear carpeta.');
            }
        }
        Yii::$app->session->set('carpeta', $carpeta);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Exportar en navegador
        $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
        $OS = strtolower(empty($_SERVER['HTTP_USER_AGENT']) ? '' : $_SERVER['HTTP_USER_AGENT']);
        if (!$OS)
            $OS_IS_MAC = true;
        else
            $OS_IS_MAC = (strpos($OS, 'mac') !== false)
                || (strpos($OS, 'macintosh') !== false)
                || (strpos($OS, 'iphone') !== false)
                || (strpos($OS, 'ipad') !== false);

        if ($OS_IS_MAC) {
//            if(!file_exists($target_relative)){
//                copy($filename,$target_relative);
//                sleep(1);
//            }
//            header('Location: ' . $target);
        } else {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . 'Devengamientos.' . $fi['extension']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx"));
            ob_clean();
            flush();
            readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
        }

        //borramos el fichero temporal
        unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Al index pero con el modelo
        $this->verificarSesion();

        $searchModel = new PolizaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * @param $objPHPExcel Spreadsheet
     * @param $columna
     * @param $fila
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function setBorder(&$objPHPExcel, $columna, $fila, $columna_hasta = null, $fila_hasta = null, $rango = false)
    {
        // Borde superior, derecho, inferior.
        if ($rango) {
            while ($columna <= $columna_hasta) {
                $fila = 4;
                while ($fila <= $fila_hasta) {
                    $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
                        ->getBorders()->getTop()->setBorderStyle(Border::BORDER_THIN);
                    $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
                        ->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                    $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
                        ->getBorders()->getRight()->setBorderStyle(Border::BORDER_THIN);
                    $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
                        ->getBorders()->getLeft()->setBorderStyle(Border::BORDER_THIN);
                    $fila++;
                }
                $columna++;
            }
            return;
        }

        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
            ->getBorders()->getTop()->setBorderStyle(Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
            ->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
            ->getBorders()->getRight()->setBorderStyle(Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
            ->getBorders()->getLeft()->setBorderStyle(Border::BORDER_THIN);
    }

    private function setNumberFormat(&$objPHPExcel, $columna, $fila)
    {
        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
            ->getNumberFormat()
            ->setFormatCode('#,##0');
    }

    private function setBackground(&$objPHPExcel, $columna, $fila, $colour)
    {
        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
            ->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()->setARGB($colour);
    }

    private function dateToStr($anho_mes, $short = false)
    {
        $mes_nombre = [
            '01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio',
            '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre',
        ];

        return $short ?
            substr($mes_nombre[substr($anho_mes, 5, 2)], 0, 3) . '-' . substr($anho_mes, 0, 4) :
            $mes_nombre[substr($anho_mes, 5, 2)] . '-' . substr($anho_mes, 0, 4);

    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRecalcularDevengamientos()
    {
        $polizas = Poliza::find()->where([
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')
        ])->all();
        $cant_polizas = sizeof($polizas);

        $polizas_no_devengadas = [];
        $msgs = [];
        /** @var Poliza $poliza */
        foreach ($polizas as $poliza) {
            $poliza->generarImporteDiario();
            $poliza->refresh();
            Yii::warning($poliza->importe_diario);
            $result = $poliza->generarDevengamientos();
            if ($result['result']) {
                $cant_polizas--;
            } else {
                $polizas_no_devengadas[] = "#{$poliza->nro_poliza}";
                $msgs[] = $result['error'];
            }
        }
        if ($cant_polizas == 0)
            FlashMessageHelpsers::createSuccessMessage('Devengamientos recalculados correctamente.');
        else {
            $msg = implode(', ', $polizas_no_devengadas) . ": " . implode(', ', $msgs);
            FlashMessageHelpsers::createWarningMessage("Polizas no devengadas {$msg}");
        }
        return $this->redirect(['index']);
    }

    /**
     * @throws NotFoundHttpException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \yii\db\Exception
     * @throws \Throwable
     */
    public function actionCalcular($id)
    {
        $model = $this->findModel($id);
        if (!$model->esDelPeriodoYEmpresa()) {
            throw new NotFoundHttpException('The requested pages doesn´t exist');
        }

        $model = new TempModel();
        $model->load(Yii::$app->request->post());

        $cta_seguro_a_devengar = PlanCuenta::findOne(['id' => PlantillaCompraventa::getIdCuentaSeguroADevengar()]);
        if (!isset($cta_seguro_a_devengar)) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar o definir una plantilla marcando \'SI\' en SEGURO A DEVENGAR.');
            return $this->redirect(['index']);
        }

        $query = Poliza::find()->alias('poliza');
        $query->joinWith('devengamientos as devengamientos');
        $query->joinWith('seccion as seccion');
        $query->joinWith('compra as compra');
        $query->joinWith('compra.entidad as entidad');
        $query->joinWith('compra.detallesCompra as detalleCompra');
        $query->select([
            'id' => 'poliza.id',
            'seccion_id' => 'seccion.id',
            'factura_compra_id' => 'compra.id',
            'poliza_id' => 'devengamientos.poliza_id',

            'nro_poliza' => 'poliza.nro_poliza',
            'importe_diario' => 'poliza.importe_diario',
            'nro_factura' => 'compra.nro_factura',
            'fecha_de_factura' => 'compra.fecha_emision',
            'proveedor' => 'entidad.razon_social',
            'seccion' => 'seccion.nombre',
            'importe_gravado' => 'detalleCompra.subtotal', // where detalleCompra.cta_contable = 'debe'
            'desde' => 'poliza.fecha_desde',
            'hasta' => 'poliza.fecha_hasta',
            'vigencia' => "DATEDIFF(poliza.fecha_hasta, poliza.fecha_desde)",
            'devengamientos.*'
        ]);
        $query->where(['poliza.id' => $id]);
        $query->andWhere([
            'plan_cuenta_id' => PlantillaCompraventa::getIdCuentaSeguroADevengar(),
        ]);
        $query->orderBy('devengamientos.poliza_id, devengamientos.anho_mes');

        $data = $query->asArray()->all();

        // Crear objetoExcel.
        /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        date_default_timezone_set('America/Asuncion');
        /** Create a new Spreadsheet Object **/
        $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/poliza/templates/';
        $objPHPExcel = IOFactory::load($carpeta . 'poliza_template.xlsx');

        // Rellenar Excel.
        $fila = 4;
        $columna = 1;
        $fila_titulo = 4;
        $max_fila = 0;
        $max_colum = 0;
        $poliza_id = 0;
        foreach ($data as $item) {
            if ($item['poliza_id'] != $poliza_id) {
                $fila++;
                $columna = 1;
                $poliza_id = $item['poliza_id']; // para completar datos del seguro una sola vez por poliza.

                // Rellenar parte estatica
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['nro_poliza']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['nro_factura']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['fecha_de_factura']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['proveedor']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['seccion']['nombre']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['importe_gravado']);
                $this->setNumberFormat($objPHPExcel, $columna - 1, $fila);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['desde']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['hasta']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['vigencia']);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['importe_diario']);
                $this->setNumberFormat($objPHPExcel, $columna - 1, $fila);
            }

            // TODO: por devengamientos.
            if ($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila_titulo - 1)->getValue() == "")
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo - 1, $item['dias']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo, $item['anho_mes']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna++, $fila, $item['monto']);
            $this->setNumberFormat($objPHPExcel, $columna - 1, $fila);

            $last_column = $columna - 1;
            if ($last_column > $max_colum) $max_colum = $last_column;
            iF ($fila > $max_fila) $max_fila = $fila;
        }

        // Colores.
        $verde = 'FF92D050';
        $naranja = 'FFF4B083';
        $celeste = "FF9CC2E5";

        // Poner borde a la tabla.
        $this->setBorder($objPHPExcel, 1, 4, $max_colum + 1, $max_fila, true);

        // Ultima columna de total
        $fila = $fila_titulo;
        $columna = $max_colum + 1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo, "TOTAL");
        $this->setBackground($objPHPExcel, $columna, $fila_titulo, $celeste);
        $fila++;
        $poliza_id = 0;
        foreach ($data as $item) {
            if ($poliza_id != $item['poliza_id']) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila, $item['importe_gravado']);
                $this->setNumberFormat($objPHPExcel, $columna, $fila);
                $fila++;
                $poliza_id = $item['poliza_id'];
            }
        }
//        // Si se quiere calcular los subtotales por fila mediante fórmulas, descomentar esta linea y comentar el algoritmo previo.
//        $fila = 5;
//        $columna = 11;
//        while ($fila <= $max_fila) {
//            $desde = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(11, $fila)->getCoordinate();
//            $hasta = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($max_colum, $fila)->getCoordinate();
//            $target = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($max_colum+1, $fila)->getCoordinate();
//            $objPHPExcel->getActiveSheet()->setCellValue($target, "=SUM({$desde}:{$hasta})");
//            $fila++;
//        }

        // Pintar fondo diferenciado del diciembre y enero
        $columna = 12;
        $fila = $fila_titulo;
        $flag = true;
        $color = $verde;
        $this->setBackground($objPHPExcel, $columna - 1, $fila_titulo, $verde);
        while ($columna <= $max_colum) {
            $actual = substr($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila_titulo)->getValue(), 5, 2);
            $anterior = substr($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna - 1, $fila_titulo)->getValue(), 5, 2);

            if ($actual == "01" && $anterior == "12") {
                $flag = !$flag;
            }

            if ($flag) {
                $this->setBackground($objPHPExcel, $columna, $fila_titulo, $verde);
            } else {
                $this->setBackground($objPHPExcel, $columna, $fila_titulo, $naranja);
            }

            $columna++;
        }

        // Cambiar los numeros de los meses a textos.
        $columna = 11;
        $fila = $fila_titulo;
        while ($columna <= $max_colum) {
            $anho_mes = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getValue();
            $texto = $this->dateToStr($anho_mes, true);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->setValue($texto);
            $columna++;
        }

        //Versión excel
        $carpeta = Yii::$app->basePath . '/web/uploads/';
        if (!is_dir($carpeta)) {
            if (!@mkdir($carpeta, 0777, true)) {
                throw new \Exception('No se puede crear carpeta.');
            }
        }
        Yii::$app->session->set('carpeta', $carpeta);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Exportar en navegador
        $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
        $OS = strtolower(empty($_SERVER['HTTP_USER_AGENT']) ? '' : $_SERVER['HTTP_USER_AGENT']);
        if (!$OS)
            $OS_IS_MAC = true;
        else
            $OS_IS_MAC = (strpos($OS, 'mac') !== false)
                || (strpos($OS, 'macintosh') !== false)
                || (strpos($OS, 'iphone') !== false)
                || (strpos($OS, 'ipad') !== false);

        if ($OS_IS_MAC) {
//            if(!file_exists($target_relative)){
//                copy($filename,$target_relative);
//                sleep(1);
//            }
//            header('Location: ' . $target);
        } else {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . 'Devengamientos.' . $fi['extension']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx"));
            ob_clean();
            flush();
            readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
        }

        //borramos el fichero temporal
        unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
        return $this->redirect(['index']);
    }
}

/**
 * Class TempModel
 * @package backend\modules\contabilidad\controllers
 *
 * @property string $fecha_hasta
 */
class TempModel extends Model
{
    public $fecha_hasta;

    public function rules()
    {
        return [
            [['fecha_hasta'], 'string'],
            [['fecha_hasta'], 'safe'],
        ];
    }
}