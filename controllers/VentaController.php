<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Cotizacion;
use backend\models\Iva;
use backend\models\Moneda;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\ReciboDetalle;
use backend\modules\contabilidad\models\search\VentaSearch;
use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * VentaController implements the CRUD actions for Venta model.
 */
class VentaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view', 'bloquear', 'desbloquear'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Venta $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Venta $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['bloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Venta $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['desbloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Venta $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'si';
                        }
                    ],
                ],
            ],
        ];
    }

    private function clearSession()
    {
        Yii::$app->session->remove('cont_detalleventa-provider');
        Yii::$app->session->remove('cont_monto_totales');
        Yii::$app->session->remove('cont_detalleventa_costo_sim-provider');
        Yii::$app->session->remove('cont_detalleventa_sim-provider');
        Yii::$app->session->remove('cont_activofijo_ids');
        Yii::$app->session->remove('cont_activo_bio_stock_manager');
        Yii::$app->session->remove('test');
        Yii::$app->session->remove('cont_act_bio_venta_deducible');
        Yii::$app->session->remove('cont_act_bio_valor_para_plantilla');
        Yii::$app->session->remove('cont_act_bio_venta_campo_iva_activo');
        Yii::$app->session->remove('cont_act_bio_venta_fila_id');
        Yii::$app->session->remove('$venta_id');
    }

    /**
     * Lists all Venta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VentaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 20];

//        Yii::$app->session->remove('cont_factura_venta_nro_actual');
        self::clearSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Venta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Calcula las columnas del detalle en base al monto total de la factura.
     */
    public function actionCalcDetalle()
    {
        /** @var DetalleVenta[] $detalles_debe */
        /** @var DetalleVenta[] $detalles */

        $total_factura = $_POST['cont_factura_total'];
        $porcentajes_iva = $_POST['ivas'];
        $tipo_docu_id = $_POST['tipo_docu_id'];
        $estado = Yii::$app->request->post('estado_comprobante', '');
        $moneda_id = $_POST['moneda_id'];

        // Si la variable del post recibe un array y si ese array es vacio, directamente no viene la variable.
        if (!array_key_exists('plantillas', $_POST)) {
            $detalles = [];
            goto retorno;
        }

        if (in_array($estado, ['anulada', 'faltante'])) {
            $detalles = [];
            goto retorno;
        }

        // Verificar tipo de documento
        $tipoDoc = TipoDocumento::findOne(['id' => $tipo_docu_id]);
        if ($tipoDoc == null) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar Tipo de Documento');
            return false;
        }

        $plantillas = $_POST['plantillas'];
        $plantillasModel = [];
        $totales_valor = [];
        $ids_previos = [];
        foreach ($plantillas as $pl) {
            $plantilla = PlantillaCompraventa::findOne(['id' => $pl['plantilla_id']]);

            if (in_array($plantilla->id, $ids_previos)) {
                FlashMessageHelpsers::createWarningMessage('No puede elegir dos plantillas iguales.');
                return false;
            }

            if ($plantilla != null) {
                // Valores iva por plantilla
                $valores_iva = [];
                foreach ($porcentajes_iva as $iva) {
                    $valores_iva[] = $pl['iva_' . $iva];
                }

                $plantillasModel[] = $plantilla;
                $totales_valor[$plantilla->id] = $valores_iva;
            }

            $ids_previos[] = $plantilla->id;
        }

//        Yii::$app->session->set('totales_valor', $totales_valor);

        // Verificar que se ha seleccionado una plantilla
        if (!isset($plantillasModel)) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar PLANTILLA');
            return false;
        }

        // Verificar que se haya seleccionado una moneda
        $moneda = Moneda::findOne(['id' => $moneda_id]);
        if ($moneda == null) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar una MONEDA');
            return false;
        }

        // Crear detalles por Plantilla e IVA's usadas.
        $detalles = [];
        $detalles = $this->genDetallesByDefault($detalles, $tipoDoc, $total_factura, $totales_valor, $porcentajes_iva, $plantillasModel, $moneda_id);

        // Actualizar indices en la sesion
        $detalles = array_values($detalles);

        // Agrupar cuentas iguales
        $detalles = DetalleVenta::agruparCuentas($detalles);

        // Agrupar debe/haber
        $detalles = $this->agruparDebeHaber($detalles);

        // invertir si es nota de credito
        self::invertirPartidas($detalles, $tipoDoc);

        // guarda en la sesión
        retorno:;
        Yii::$app->getSession()->set('cont_detalleventa-provider', new ArrayDataProvider([
            'allModels' => $detalles,
            'pagination' => false,
        ]));

        return true;
    }

    /**
     * @param DetalleVenta[] $detalles
     * @param TipoDocumento $tipoDocumento
     */
    private function invertirPartidas(&$detalles, $tipoDocumento)
    {
        try {
            $TDS_NotaCredito = Venta::getTipodocSetNotaCredito();
            if ($tipoDocumento->tipo_documento_set_id == $TDS_NotaCredito->id) {
                $map = ['debe' => 'haber', 'haber' => 'debe'];
                foreach ($detalles as $key => $_) {
                    $detalles[$key]->cta_contable = $map[$_->cta_contable];
                }
                $detalles = $this->agruparDebeHaber($detalles);
            }
        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }
    }

    /**
     * @param $new_detalle DetalleVenta
     * @param $old_detalles DetalleVenta[]
     * @return int|string
     */
    private function findItem($new_detalle, $old_detalles)
    {
        foreach ($old_detalles as $index => $old) {
            if ($new_detalle->cta_contable == $old->cta_contable &&
                $new_detalle->planCuenta->cod_completo == $old->planCuenta->cod_completo)
                return $index;
        }

        return -1;
    }

    /**
     * @param $detalles DetalleVenta[]
     * @param $tipoDoc TipoDocumento
     * @param $total_factura
     * @param $array_totales_por_iva
     * @param $porcentajes_iva
     * @param $plantillas PlantillaCompraventa[]
     * @param $moneda_id
     * @return mixed
     */
    private function genDetallesByDefault($detalles, $tipoDoc, $total_factura, $totales_valor, $porcentajes_iva, $plantillas, $moneda_id)
    {
        /** @var PlantillaCompraventaDetalle[] $plantilla_detalles */
        /** @var IvaCuenta $iva_cta */
        $es_nota_credito = Yii::$app->request->post('es_nota') == 'nota_credito';
        $moneda = Moneda::findOne(['id' => $moneda_id]);

//        if ($total_factura == 0) {
//            $detalles = [];
//            goto retorno;
//        }

        // Crear detalle por Tipo de Documento
        $cuenta_debe = $tipoDoc != null ? $tipoDoc->cuenta : null;
        if (isset($cuenta_debe)) {
            /* Al elegir tipo_documento, SIEMPRE se trae 1 sola cuenta. */
            $detalle = new DetalleVenta();
            $detalle->cta_contable = !$es_nota_credito ? 'debe' : 'haber';
            $detalle->cta_principal = 'no';
            $detalle->plan_cuenta_id = $cuenta_debe->id;
            $detalle->subtotal = $this->numberFormatUseDotByMoneda($total_factura, $moneda); // TODO: averiguar que valor debe tomar, dependiendo del tipo_documento.

            array_push($detalles, $detalle);
        }

        $total = 0;
        foreach ($plantillas as $plantilla) {
            // Crear detalles segun detalles de la plantilla.
            $plantilla_detalles = $plantilla->getPlantillaCompraventaDetalles()->all();
            $gravada_total = 0.0;
            $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
            $descuento = $plantilla->isForDiscount();
            // TODO: Aclarar con el cliente sobre como desea ver los costos de venta. Suma de costos o por venta gravada.
            foreach ($plantilla_detalles as $p_detalle) {
                if ($p_detalle->tipo_asiento == 'venta') {
                    $i = 0;
                    foreach ($totales_valor[$plantilla->id] as $total_iva) {
                        if ($p_detalle->ivaCta != null && $total_iva !== '' && round($total_iva, 2) > 0.0 &&
                            $p_detalle->ivaCta->iva->porcentaje == $porcentajes_iva[$i]) {
                            // detalle por cuenta mas importante (ventas de merca, ingresos varios, etc)
                            $detalle = new DetalleVenta();
                            $detalle->plan_cuenta_id = $p_detalle->pCGravada->id;
                            $detalle->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
                            $detalle->cta_principal = $p_detalle->cta_principal;
                            $detalle->subtotal = round($total_iva / (1.00 + ($p_detalle->ivaCta->iva->porcentaje / 100.0)), 2);
                            $detalle->subtotal = $this->numberFormatUseDotByMoneda($detalle->subtotal, $moneda);
                            $detalle->subtotal *= ($descuento ? -1 : 1);
                            $gravada_total += round($detalle->subtotal, 2);
                            // detalle por iva x%
                            $detalle2 = new DetalleVenta();
                            $detalle2->plan_cuenta_id = $p_detalle->ivaCta->cuentaVenta->id;
                            $detalle2->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
//                            $detalle2->cta_principal = $p_detalle->cta_principal;
                            $detalle2->cta_principal = 'no'; // normalmente los ivas asociados a la cta gravada no son principales.
                            $detalle2->subtotal = round($total_iva * ($descuento ? -1 : 1), 2) - round($detalle->subtotal, 2);
                            $detalle2->subtotal = $this->numberFormatUseDotByMoneda($detalle2->subtotal, $moneda);
                            array_push($detalles, $detalle);
                            array_push($detalles, $detalle2);
                            $total += round($detalle->subtotal + $detalle2->subtotal, 2);
                            break;
                        } elseif ($p_detalle->ivaCta == null && $total_iva !== '' && round($total_iva, 2) > 0.0 &&
                            $porcentajes_iva[$i] == '0') // TODO: aqui falta definir si habra mas detalles de plantilla para asiento de venta con ivacta = null
                        {
                            $detalle = new DetalleVenta();
                            $detalle->plan_cuenta_id = $p_detalle->pCGravada->id;
                            $detalle->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
                            $detalle->cta_principal = $p_detalle->cta_principal;
                            $detalle->subtotal = round($total_iva, 2);
                            $detalle->subtotal = $this->numberFormatUseDotByMoneda($detalle->subtotal, $moneda);
                            $detalle->subtotal *= ($descuento ? -1 : 1);
                            array_push($detalles, $detalle);
                            $total += round($detalle->subtotal, 2);
                            break;
                        }
                        $i++;
                    }
                }
            }
        }

        $detalles[0]->subtotal = $total; // cuenta de entrada, unica.
        $detalles = array_values($detalles);

        retorno:;
        return $detalles;
    }

    /**
     * @param $value
     * @param $moneda Moneda|int
     * @return string
     */
    private function numberFormatUseDotByMoneda($value, $moneda)
    {
        if (!($moneda instanceof Moneda)) {
            $moneda = Moneda::findOne(['id' => $moneda]);
        }

        if ($moneda->id == 1) {
            return number_format((float)$value, '0', '.', '');
        }

        return $value;
    }

    public function actionSimular()
    {
        /** @var Cotizacion $cotizacion */
        /** @var DetalleVenta[] $detalles */
        /** @var DetalleVenta[] $detalles_para_sim */

        $hoy = date_create_from_format('d-m-Y', $_POST['hoy']);
        $moneda_id = $_POST['moneda_id'];
        $es_nota_credito = Yii::$app->request->post('es_nota') == 'nota_credito';
        $estado = Yii::$app->request->post('estado_comprobante', '');
        $total_factura = $_POST['cont_factura_total'];
        $detalles = Yii::$app->getSession()->get('cont_detalleventa-provider')->allModels;

        if ($total_factura == 0 || in_array($estado, ['anulada', 'faltante']) || sizeof($detalles) == 0) {
            $detalles_para_sim = [];
            $detalles_para_sim_costo = [];
            goto retorno;
        }

        $monedaBaseId = \common\models\ParametroSistema::getMonedaBaseId();
        // Cuando cambia de anulada/faltante a vigente, el campo fecha_emision es vacia y llega en POST vacio.
        if ($moneda_id != null && $moneda_id != $monedaBaseId && $hoy == false) {
            FlashMessageHelpsers::createInfoMessage('Debe especificar una fecha para poder obtener la cotización a utilizar.');
            return false;
//            $detalles_para_sim = [];
//            $detalles_para_sim_costo = [];
//            goto retorno;
        }

        $cotizacion = null;
        if ($moneda_id != null && $moneda_id != $monedaBaseId)
            $cotizacion = Cotizacion::find()
                ->where(['moneda_id' => $_POST['moneda_id']])
                ->andFilterWhere(['!=', 'moneda_id', Moneda::findOne(['nombre' => 'Guaraní'])->id])
                ->andFilterWhere(['fecha' => date('Y-m-d', strtotime($hoy->format('Y-m-d') . ' - 1 days'))])->one();
        $valor_moneda = Yii::$app->request->post('valor_moneda', '');
        $detalles = DetalleVenta::agruparCuentas($detalles);
        $detalles_para_sim = [];
        $plantillas = $_POST['plantillas'];

        // Convertir a guaranies si la moneda es extrangera.
        if (isset($cotizacion) && $valor_moneda != '') {
            $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
            $diff = (float)$valor_moneda - (float)$cotizacion->compra;
            $newDetalleDiffCambio = null;
            if ($diff != 0.0) {
                $newDetalleDiffCambio = new DetalleVenta();
                $newDetalleDiffCambio->cta_contable = $diff > 0.0 ? 'haber' : 'debe'; // TODO: parametro del modulo
                if ($es_nota_credito)
                    $newDetalleDiffCambio->cta_contable = $mapeo_inverso[$newDetalleDiffCambio->cta_contable];
                // 8.05 = resultado por diferencia de cambio como ingresos
                // 13.04 = resultado por diferencia de cambio como pérdida

                $empresa_id = \Yii::$app->session->get('core_empresa_actual');
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                $newDetalleDiffCambio->plan_cuenta_id = PlanCuenta::findOne(['id' => $diff > 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor])->id; // TODO: parametro del modulo
                $newDetalleDiffCambio->subtotal = (float)$diff * (float)$total_factura;
                $diff < 0.0 ? $newDetalleDiffCambio->subtotal = ((float)$newDetalleDiffCambio->subtotal * -1.0) : true;
            }
            foreach ($detalles as $detalle) {
                $newDetalle = new DetalleVenta();
                $newDetalle->subtotal = (float)$detalle->subtotal * ($detalle->planCuenta->nombre == 'CAJA' ? (float)$valor_moneda : (float)$cotizacion->compra);
                $newDetalle->plan_cuenta_id = $detalle->plan_cuenta_id;
                $newDetalle->cta_contable = $detalle->cta_contable;
                $detalles_para_sim[] = $newDetalle;
            }

            // insertar el asiento por diferencia de costo
            if (isset($newDetalleDiffCambio)) {
                $detalles_para_sim[] = $newDetalleDiffCambio;
            }
        } elseif ($valor_moneda != '') {
            foreach ($detalles as $detalle) {
                $newDetalle = new DetalleVenta();
                $newDetalle->subtotal = (float)$detalle->subtotal * (float)$valor_moneda;
                $newDetalle->plan_cuenta_id = $detalle->plan_cuenta_id;
                $newDetalle->cta_contable = $detalle->cta_contable;
                $detalles_para_sim[] = $newDetalle;
            }
        } else {
            // se usa moneda nacional, solo copiar los detalles al array para simulacion
            foreach ($detalles as $detalle) {
                $detalles_para_sim[] = $detalle;
            }
        }

        $detalles_para_sim = $this->agruparDebeHaber($detalles_para_sim);


        /* *********************** ASIENTO DE COSTO *********************** */

        $coef_costo = (float)ParametroSistema::getEmpresaActualCoefCosto();
        if ($coef_costo == 0) {
            $detalles_para_sim_costo = [];
//            FlashMessageHelpsers::createInfoMessage("La simulación de asiento por Costo de Venta no se muestra porque el coef. de costo en la empresa actual es cero.");
            goto retorno;
        }

        $detalles_para_sim_costo = [];
        if (sizeof($detalles_para_sim) > 0) {
            $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
            foreach ($plantillas as $key => $pl) {
                $plantilla = PlantillaCompraventa::findOne(['id' => $pl['plantilla_id']]);
                $_total_por_plantilla = $this->sumAllIvas($pl);

                if ($plantilla->costo_mercad == 'si') {
                    /** @var PlantillaCompraventaDetalle[] $p_detalles_costo_merca */
                    $p_detalles_costo_merca = $plantilla->getDetalles('tipo_asiento', 'costo_venta');
                    foreach ($p_detalles_costo_merca as $p_d) {
                        $d = new DetalleVenta();
                        $d->cta_contable = !$es_nota_credito ? $p_d->tipo_saldo : $mapeo_inverso[$p_d->tipo_saldo];
                        $d->plan_cuenta_id = $p_d->p_c_gravada_id;
                        $d->subtotal = $coef_costo * round($_total_por_plantilla, 2);
                        if ($cotizacion != null)
                            $d->subtotal *= round($cotizacion->compra, 2);
                        $detalles_para_sim_costo[] = $d;
                    }
                }
            }
        }

        Yii::warning($detalles_para_sim_costo);
        $detalles_para_sim_costo = $this->agruparDebeHaber($detalles_para_sim_costo);

        retorno:;
        // poner en sesion
        Yii::$app->getSession()->set('cont_detalleventa_sim-provider', new ArrayDataProvider([
            'allModels' => $detalles_para_sim,
            'pagination' => false,
        ]));
        Yii::$app->getSession()->set('cont_detalleventa_costo_sim-provider', new ArrayDataProvider([
            'allModels' => $detalles_para_sim_costo,
            'pagination' => false,
        ]));
        return true;
    }

    /**
     * @param $pl array
     * @return float
     */
    private function sumAllIvas($pl)
    {
        $sum = 0.0;
        foreach ($pl as $key => $value) {
            if ($key != 'plantilla_id') $sum = (float)$sum + (float)$value;
        }
        return $sum;
    }

    /**
     * @param DetalleVenta[] $array Detalles de venta
     * @return array Detalles de venta agrupados en debe / haber
     */
    private function agruparDebeHaber($array) // TAMBIEN ESTA EN Venta.php PARA USARLO DESDE ASIENTOCONTROLLER. ES EXACTAMENTE IGUAL
    {
        $debes = [];
        $haberes = [];

        foreach ($array as $item) {
            if ($item->cta_contable == 'debe') {
                $debes[] = $item;
            } elseif ($item->cta_contable == 'haber') {
                $haberes[] = $item;
            }
        }

        return array_values(array_merge($debes, $haberes));
    }

    /**
     * @param null $oldModel
     * @return Venta
     * @throws \Exception
     */
    private function newModel($oldModel = null)
    {
        if ($oldModel != null) unset($oldModel); // $oldModel = null;
        $model = new Venta();
        $model->moneda_id = 1; // ParametroSistemaHelpers::getValorByNombre('moneda_base');
        $model->estado = 'vigente';
        $model->tipo = 'factura';
        $model->para_iva = 'si';
        $model->empresa_id = Yii::$app->session->get('core_empresa_actual');
        $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
        $model->empresa_id = Yii::$app->getSession()->get('core_empresa_actual');
        $model->obligacion_id = Venta::getObligacionesEmpresaActual($model->empresa_id)[0]['id'];  #No es necesario pero es para controlar que haya obligacion activa en la empresa actual.
        if (Yii::$app->session->has('cont_valores_usados')) {
            $valoresUsados = Yii::$app->session->get('cont_valores_usados');
            foreach ($valoresUsados as $key => $value) {
                if ($key != 'id') $model->$key = $value;
            }
            $model->total = '';
            $tim_det = TimbradoDetalle::findOne(['id' => $model->timbrado_detalle_id_prefijo]);
            $model->nro_timbrado = isset($tim_det) ? $tim_det->timbrado->nro_timbrado : '';
        }

        $skey = 'cont_venta_plantillas_usadas';
        if (Yii::$app->session->has($skey)) {
            $_ivas_ctas_usadas = Yii::$app->session->get($skey);
            foreach ($_ivas_ctas_usadas as $key1 => $ivas_ctas_usada) {
                foreach ($ivas_ctas_usada as $key2 => $item) {
                    $_ivas_ctas_usadas[$key1][$key2] = 0;
                }
            }
            $model->_iva_ctas_usadas = $_ivas_ctas_usadas;
        }
        return $model;
    }

    /**
     * Creates a new Venta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     * @throws \Throwable
     */
    // todo: si la moneda es extrangera, realizar los cambios en los montos. En update y en create.
    public function actionCreate($_pjax = null, $guardar_salir = false)
    {
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
        $parmsys_debe = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_debe]);

        $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
        $parmsys_haber = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_haber]);

        if (!isset($parmsys_debe)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Ganancia.");
            return $this->redirect(['index']);
        }
        if (!isset($parmsys_haber)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Pérdida.");
            return $this->redirect(['index']);
        }

        $coef = ParametroSistema::getEmpresaActualCoefCosto();
        if (!isset($coef)) {
            FlashMessageHelpsers::createWarningMessage("Falta especificar en el parámetro de la empresa el coeficiente de costo.");
            return $this->redirect(['index']);
        }

        try {
            $model = $this->newModel();
            $model->empresa_id = Yii::$app->session->get('core_empresa_actual');
            $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }
        $model->usar_tim_vencido = 0;
        $session = Yii::$app->session;

        // Inicializar variable de sesion para detalles con array vacio
        if ($_pjax == null && !Yii::$app->request->isPost) {
            $session->set('cont_detalleventa-provider', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
        }

        // eliminar el blue print y eliminar items que tienen plantilla_id vacio.
        if (array_key_exists('VentaIvaCuentaUsada', $_POST)) {
            if (!empty($_POST['VentaIvaCuentaUsada']['new'])) {
                unset($_POST['VentaIvaCuentaUsada']['new']);
                Yii::$app->request->setBodyParams($_POST);
                foreach ($_POST['VentaIvaCuentaUsada'] as $new_keys => $new) {
                    if ($new['plantilla_id'] == "") {
                        unset($_POST['VentaIvaCuentaUsada'][$new_keys]);
                        Yii::$app->request->setBodyParams($_POST);
                    }
                }
            }
        }

        if ($model->load(Yii::$app->request->post())) {

            $trans = Yii::$app->db->beginTransaction();

            try {
                $model->creado_por = Yii::$app->user->identity->username;

                /** @var IvaCuenta $iva_cta */
                /** @var DetalleVenta[] $detalleVenta */

                if ($model->tipo_documento_id != '') {
                    $tipodoc = TipoDocumento::findOne(['id' => $model->tipo_documento_id]);
                    if (isset($tipodoc)) {
                        $model->condicion = $tipodoc->condicion;
                    }
                }
                $timbradoD = TimbradoDetalle::findOne(['id' => $model->timbrado_detalle_id_prefijo]);
                $model->prefijo = $timbradoD->prefijo;
                $model->timbrado_detalle_id = $timbradoD->id;
                $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                $model->entidad_id = $entidad != null ? $entidad->id : null;
                if ($model->fecha_emision != "" && $model->fecha_emision != "00-00-0000")
                    $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

                if ($model->condicion == 'credito')
                    $model->fecha_vencimiento = implode('-', array_reverse(explode('-', $model->fecha_vencimiento)));

                if (!$model->validate()) {
                    $msg = implode(', ', $model->getErrorSummary(true));
                    throw new \Exception("Error al validar la cabecera de factura: {$msg}.");
                }

                $venta_de_activofijo = false; // si se salta por force_save, la variable en ese punto queda indefinida.
                $venta_de_activobiol = false;

                $_tipo_doc_ob = $model->getTipoDocumento()->one();
                // Desde create, solo se puede declarar facturas faltantes.
                if (in_array($model->estado, ['anulada', 'faltante'])) {

                    $check_ok = $model->checkNroFact('create');

                    if ($check_ok != Venta::NRO_FACTURA_CORRECTO) {

                        switch ($check_ok) {
                            case Venta::NRO_FACTURA_DUPLICADO:
                                throw new \Exception('Nro Factura Duplicado.');
                            case Venta::NRO_FACTURA_NO_RANGO:
                                throw new \Exception('Nro Factura no se encuentra en el rango del timbrado.');
                            case Venta::NRO_FACTURA_NO_PREFIJO:
                                throw new \Exception('La factura no tiene prefijo.');
                        }

                        throw new \Exception("Error no identificado. \$check_ok = {$check_ok}.");
                    }

                    $isDeleting = $model->estado == 'faltante';
                    $result = $model->updateNroActualSession($isDeleting, 'create');

                    if (!$result['resultado']) {
                        throw new \Exception("Error en updateNroActualSession(): {$result['errores']}");
                    }

                    /*
                     * Si se 'disabled' en js, directamente no envia nada, asi que hay que nullear manualmente aqui porque se carga con el newModel()
                     * Si se usa 'readonly', los select2 no se deshabilitan.
                     */
                    $model->total = 0;
                    $model->saldo = 0;
                    $model->condicion = '';
                    $model->cant_cuotas = '';
                    $model->valor_moneda = '';
                    $model->obligaciones = '';
                    $model->fecha_vencimiento = '';
                    $model->para_iva = null;
                    $model->moneda_id = null;
                    $model->entidad_id = null;
                    $model->obligacion_id = null;
                    $model->cotizacion_propia = null;
//                    $model->tipo_documento_id = null;
                    # Miercoles 19 junio 2019: Magali "solicita/se pone de acuerdo en" que el tdoc sea siempre requerido porque otros tdos pueden tambien ser anulados, por ej, nota de credito.

                    if (!$model->validate()) {
                        throw new \Exception('Error validando factura: ' . implode(', ', $model->getErrorSummary(true)));
                    }

                    goto force_save;
                }

                if (!$model->save()) { // generar id para poder asociarle a los detalles.
                    throw new \Exception("No se pudo guardar venta: {$model->getErrorSummaryAsString()}");
                }

                // Verificar cotizaciones de la set y valor_moneda.
                $fecha = date('Y-m-d', strtotime('-1 day', strtotime($model->fecha_emision)));
                $moneda_id = $model->moneda_id;

                /** @var Cotizacion $query */
                $query = Cotizacion::find()
                    ->where(['fecha' => $fecha, 'moneda_id' => $moneda_id])
                    ->andWhere(['!=', 'moneda_id', Moneda::findOne(['nombre' => 'Guaraní'])->id])
                    ->one();

                if ($model->moneda_id != 1 && ($query == null || $model->valor_moneda == '')) {
                    if ($query == null) throw new \Exception('Falta cargar cotización de la SET.');
                    elseif ($model->valor_moneda == '') throw new \Exception('Falta especificar valor de la moneda');
                } elseif ($model->moneda_id == \common\models\ParametroSistema::getMonedaBaseId()) {
                    $model->valor_moneda = '';
                }

                // Registrar cada uno de los montos de los ivas, junto con las cuentas usadas.
                $ningun_total = true;                               // flag para controlar que haya al menos 1 detalle.
                $ivacuenta_guardar = [];
                $ivacuenta_usadas = $_POST['VentaIvaCuentaUsada'];
                $plantilla_id = null;
                $obligacionCheck = ['match' => true, 'msg' => ""];
                foreach ($ivacuenta_usadas as $ivacuenta_usada) {
                    foreach ($ivacuenta_usada['ivas'] as $iva_x => $iva_x_value) // x es un numero que vale uno de los porcentajes de iva.
                    {
                        $iva_x_value = str_replace(',', '.', str_replace('.', '', $iva_x_value));
                        if ($iva_x_value != 0 && $iva_x_value != "" && $iva_x_value != "0") { // solo si hay monto indicado
                            $ningun_total = false;
                            $ivacuenta_venta = new VentaIvaCuentaUsada();
                            $ivacuenta_venta->monto = $model->moneda_id == 1 ? round($iva_x_value) : $iva_x_value;
                            $ivacuenta_venta->factura_venta_id = $model->id;
                            $ivacuenta_venta->plantilla_id = $ivacuenta_usada['plantilla_id'];
                            $ivacuenta_venta->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

                            $regex = preg_match('/iva_([0-9]+)/', $iva_x, $matches); // matches[1] es el numero capturado.
                            if ($regex != false && $regex != 0) {
                                if ($matches[1] == 0 || $matches[1] == "0") { // significa que el iva es cero
                                    $ivacuenta_venta->iva_cta_id = "";
                                    $ivacuenta_venta->plan_cuenta_id = "";
                                } else {
                                    $iva = Iva::findOne(['porcentaje' => $matches[1]]);
                                    $ivacuenta = IvaCuenta::findOne(['iva_id' => $iva->id]);
                                    $ivacuenta_venta->iva_cta_id = $ivacuenta->id;
                                    $ivacuenta_venta->plan_cuenta_id = $ivacuenta->cuenta_venta_id;
                                }
                            }

                            // Verificar motivo de gnd
                            $plantilla = PlantillaCompraventa::findOne(['id' => $ivacuenta_venta->plantilla_id]);
                            if (isset($plantilla) && $plantilla->deducible == 'no') {
                                if (!isset($ivacuenta_usada['gnd_motivo']))
                                    throw new \Exception("Error en la fila de plantilla {$plantilla->nombre}: Motivo por el que se envia a GND no puede estar vacio.");
                                $ivacuenta_venta->gnd_motivo = $ivacuenta_usada['gnd_motivo'];
                            }

                            if (!$ivacuenta_venta->validate()) {
                                $msg = implode(', ', $ivacuenta_venta->getErrorSummary(true));
                                throw new \Exception("ERROR INTERNO al guardar factura: $msg");
                            }

                            $ivacuenta_guardar[] = $ivacuenta_venta;
                            $plantilla_id = $ivacuenta_usada['plantilla_id'];
                        }
                    }

                    if (isset($plantilla_id)) {
                        $plantilla = PlantillaCompraventa::findOne($plantilla_id);
                        if ($plantilla->esParaActivoFijo()) {
                            $venta_de_activofijo = true;
                        } elseif ($plantilla->esParaActivoBiologico()) {
                            $venta_de_activobiol = true;
                        }

                        // si hasta ahora no hubo discordancia entre obligacion de venta y de plantillas.
                        if ($obligacionCheck['match']) {
                            $idsObls = [];
                            foreach ($plantilla->obligaciones as $obligacion) {
                                in_array($obligacion->id, $idsObls) || $idsObls[] = $obligacion->id;
                            }
                            if (!in_array($model->obligacion_id, $idsObls)) {
                                $msg = ("La plantilla {$plantilla->nombre} no pertenece a la obligación {$model->obligacion->nombre} seleccionada.");
                                $obligacionCheck['match'] = false;
                                $obligacionCheck['msg'] = $msg;
                                // No se lanza la excepcion directamente aqui porque de lo contrario, las plantillas
                                // utilizadas no se van a ver al retornar al formulario
                            }
                        }
                    }
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Para poder seguir viendo las plantillas elegidas al retornar al form.
                $model->_iva_ctas_usadas = [];
                foreach ($ivacuenta_guardar as $monto_iva_usada) {

                    //Para cargar tabla de plantillas ya existentes
                    if (array_key_exists($monto_iva_usada->plantilla_id, $model->_iva_ctas_usadas)) {
                        if ($monto_iva_usada->ivaCta != null) {
                            if (array_key_exists($monto_iva_usada->ivaCta->iva->porcentaje, $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id])) {
                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] += $monto_iva_usada->monto;
                            } else {
                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
                            }
                        } else {
                            if (array_key_exists(0, $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id])) {
                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] += $monto_iva_usada->monto;
                            } else {
                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
                            }
                        }
                    } else {
                        if ($monto_iva_usada->ivaCta != null) {
                            $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
                        } else {
                            $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
                        }
                    }
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////////

                if ($ningun_total) {
                    throw new \Exception('Falta completar al menos un campo del Total Iva o Total Exenta.');
                }

                if (!$obligacionCheck['match']) {
                    $model->_iva_ctas_usadas = [];
                    throw new \Exception($obligacionCheck['msg']);
                }

                // No permitir guradar factura sin detalle
                $detalleVenta = $session['cont_detalleventa-provider']->allModels;
                if (sizeof($detalleVenta) == 0) {
                    throw new \Exception('Faltan los detalles de la factura');
                }

                $hayCtaPrincipal = false;
                $saldo = ['debe' => 0.0, 'haber' => 0.0];
                foreach ($detalleVenta as $detalle) {
                    $saldo[$detalle->cta_contable] += round($detalle->subtotal, 2);

                    $detalle->factura_venta_id = $model->id;
                    $detalle->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

                    if (!$detalle->validate()) {
                        throw new \Exception("Error en detalle de factura: {$detalle->planCuenta->nombre}: {$detalle->getErrorSummaryAsString()}.");
                    }

                    if ($detalle->cta_principal == 'si')
                        $hayCtaPrincipal = true;
                }

                if (!$hayCtaPrincipal) {
                    throw new \Exception("Se necesita establecer al menos una cuenta como principal.");
                }

                // TODO: Verificar balance entre saldo acreedor y deudor (debe y haber). Asignar total factura.
                if (round($saldo['debe'], 2) !== round($saldo['haber'], 2)) {
                    throw new \Exception('El DEBE y el HABER NO son iguales. Se recomienda verificar cómo está guardada la plantilla utilizada y también revisar los detalles y las simulaciones generados automáticamente.');
                }
                $model->total = $saldo['debe']; // para evitar trampa

                $check_ok = $model->checkNroFact('create');

                if ($check_ok != Venta::NRO_FACTURA_CORRECTO) {
                    switch ($check_ok) {
                        case Venta::NRO_FACTURA_DUPLICADO:
                            throw new \Exception('Nro Factura Duplicado.');
                        case Venta::NRO_FACTURA_NO_RANGO:
                            throw new \Exception('Nro Factura no se encuentra en el rango del timbrado.');
                        case Venta::NRO_FACTURA_NO_PREFIJO:
                            throw new \Exception('La factura no tiene prefijo.');
                    }

                    throw new \Exception("Error desconocido. \$check_ok: $check_ok");
                }

                // Actualizar el detalle timbrado corresp. con el nro_actual adecuado.
                $result = $model->updateNroActualSession();
                if (!$result['resultado']) {
                    throw new \Exception("Error actualizando Nro Factura en Sesion: {$result['errores']}.");
                }

                // asignar saldo = total factura
                $model->saldo = ($model->condicion == 'credito') ? $model->total : 0;

                // Guardar los models y comprometer transaccion
                foreach ($detalleVenta as $detalle) {
                    if (!$detalle->save()) {
                        throw new \Exception("Error guardando detalle de venta: {$detalle->getErrorSummaryAsString()}");
                    }
                }
                foreach ($ivacuenta_guardar as $ivacta) {
                    if (!$ivacta->save()) {
                        throw new \Exception("Error interno guardand iva cuenta: {$ivacuenta->getErrorSummaryAsString()}");
                    }
                }

                force_save:;
                //$model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                if (!$model->save(false)) {
                    throw new \Exception("Error guardando venta: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                # Unificar talonarios
                try {
                    $timbrado = $model->timbradoDetalle->timbrado;
                    $timbrado->unificarTalonarios();
                } catch (\Exception $exception) {
                    throw new \Exception("Error interno unificando talonarios: {$timbrado->getErrorSummaryAsString()}");
                }

                if (!in_array($model->estado, ['anulada', 'faltante'])) {
                    /** ---------------------- [inicio] Manejo de Activos fijos ---------------------- */
                    $skey = 'cont_activofijo_ids';
                    $activos_f = [];
                    if ($venta_de_activofijo && Yii::$app->session->has($skey)) {
                        foreach (Yii::$app->session->get($skey) as $id) {
                            $activofijo = ActivoFijo::findOne(['id' => $id]);
                            $activofijo->setFacturaVentaId($model->id);
                            $activofijo->setEstado('vendido');

                            $activos_f[] = $activofijo;
                        }
                        foreach ($activos_f as $item) {
                            if (!$item->save(false)) {
                                throw new \Exception("Error guardando activo fijo: {$item->getErrorSummaryAsString()}");
                            }
                        }
                    } elseif ($venta_de_activofijo && !Yii::$app->session->has($skey)) {
                        throw new \Exception('Esta Factura registra un detalle para Venta de uno o más Activo Fijo, pero Ud. no ha especificado ninguna. Especifique primero.');
                    }
                    /** ------------------------ [fin] Manejo de Activos fijos ------------------------ */

                    /** ------------------------ [inicio] Manejo de Activos biologicos ------------------------ */
                    if ($venta_de_activobiol) {
                        ActivoBiologico::isCreable();
                        $session_key = 'cont_activo_bio_stock_manager';
                        $sessionData = Yii::$app->session->get($session_key);

                        if (!Yii::$app->session->has($session_key) || !isset($sessionData) ||
                            (isset($sessionData) && empty($sessionData['dataProvider']->allModels))) {
                            throw new \Exception("Falta especificar la cantidad de A. Biológicos a vender..");
                        } else {

                            /** @var ActivoBiologicoStockManager[] $stock_managers */
                            $stock_managers = $sessionData['dataProvider']->allModels;

                            foreach ($stock_managers as $stock_manager) {
                                $stock_manager->factura_venta_id = $model->id;

                                if (!$stock_manager->datosValidos()) {
                                    throw new \Exception("Error interno validando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                                }

                                // guardar manager
                                if (!$stock_manager->save()) {
                                    throw  new \Exception("Error interno guardando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                                }
                                $stock_manager->refresh();

                                $actbio = ActivoBiologico::actualizarStock_O_CrearNuevo($stock_manager);
                                // No se por que falto este bloque, agrego por si.
                                // Pero si no es necesario, remover posteriormente.
                                if ($actbio->isNewRecord)
                                    ActivoBiologico::isCreable();
                                else
                                    if ($actbio->getInventario()->exists()) {
                                        throw new \yii\base\Exception("Ya se generó el cierre de inventario de activos 
                                    biológicos vendidos por esta factura, por lo tanto no se puede modificar más.");
                                    }
                                if (!$actbio->save()) {
                                    throw new Exception("Error actualizando stock del Activo Biológico: {$actbio->getErrorSummaryAsString()}");
                                }
                                $actbio->refresh();
                            }
                        }
                    }
                    /** ------------------------ [fin] Manejo de Activos biologicos ------------------------ */
                }

                #Verificar si se invirtio si el tipo de documento set es nota de credito
                if (!$model->isPartidaCorrect())
                    throw new \Exception("Error validando venta: El tipo de documento SET es NOTA DE CREDITO pero las partidas no se han invertido.");

                $trans->commit();
                FlashMessageHelpsers::createSuccessMessage("La factura {$model->getNroFacturaCompleto()} se ha generado correctamente.");
                self::clearSession();

                // Guardar ciertos campos en session para el siguiente create.
                $valoresUsados = [];
                $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));
                foreach ($model->attributes() as $attr) {
                    $valoresUsados[$attr] = $model->$attr;
                }

//                $valoresUsados['ruc'] = $model->ruc; // ya no retener porque se pidio asi el 14 de febrero del 2019.
                $valoresUsados['nombre_entidad'] = $model->nombre_entidad;
                $valoresUsados['nro_timbrado'] = '';//$model->nro_timbrado; // no quiere que reaparezca dice
                $valoresUsados['timbrado_detalle_id_prefijo'] = '';//$model->timbrado_detalle_id_prefijo; // no quiere que reaparezca dice
                $valoresUsados['estado'] = 'vigente';
                $valoresUsados['nro_factura'] = $model->getNextNroFactura(); // TODO: revisar

                if (strlen($valoresUsados['nro_factura']) > 0)
                    $valoresUsados['nro_factura'] = str_repeat('0', (7 - strlen($valoresUsados['nro_factura']))) . $valoresUsados['nro_factura'];

                Yii::$app->session->set('cont_valores_usados', $valoresUsados);
                Yii::$app->session->set('cont_venta_plantillas_usadas', $model->_iva_ctas_usadas);
                Yii::$app->session->remove('campos_totales_nombre_valor'); // borrar totales iva

                if ($guardar_salir) return $this->redirect(['index']);

                return $this->redirect(['create']);

            } catch (\Exception $exception) {
                //throw $exception;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
                $model->id = null;

                if ($model->fecha_emision != "" && $model->fecha_emision != "00-00-0000")
                    $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

                if ($model->condicion == 'credito')
                    $model->fecha_vencimiento = implode('-', array_reverse(explode('-', $model->fecha_vencimiento)));
            }
        }

        retorno:;
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    private function getTimDetIdPrefijo($prefijo, $nro)
    {
        return TimbradoDetalle::getTimDetIdPrefijo($prefijo, $nro);
    }

    /**
     * @param $model Venta
     * @return bool
     * @throws ForbiddenHttpException
     */
    private function isEditable(&$model)
    {
        if (isset($model->asiento)) {
            FlashMessageHelpsers::createWarningMessage('Esta Factura tiene Asientos asociados. Elimínelos primero.');
            return false;
        }
        if (ReciboDetalle::findOne(['factura_venta_id' => $model->id]) != null) {
            FlashMessageHelpsers::createWarningMessage("Esta Factura tiene asociado uno o varios Recibos.");
            return false;
        }
        if ($model->periodo_contable_id != Yii::$app->session->get('core_empresa_actual_pc')) {
            FlashMessageHelpsers::createWarningMessage('Falta definir periodo contable actual.');
            return false;
        }
        if ($model->empresa_id != Yii::$app->session->get('core_empresa_actual')) {
            FlashMessageHelpsers::createWarningMessage('Falta definir empresa actual.');
            return false;
        }
        $actbios = $model->activosBiologicos;
        foreach ($actbios as $actbio) {
            // si el activo biologico esta en el cierre de inventario, no puede modificar.
            if ($actbio->getInventario()->exists()) {
                FlashMessageHelpsers::createWarningMessage("Los activos biológicos comprados por esta factura fueron registrados en el cierre de inventario.");
                return false;
            }
        }
        if ($model->getNotas()->exists()) {
            /** @var Venta $nota_ */
            FlashMessageHelpsers::createWarningMessage("Hay notas asociadas a esta factura.");
            return false;
        }
        if ($model->getRetencion()->exists()) {
            FlashMessageHelpsers::createWarningMessage("Hay retencion asociada esta factura.");
            return false;
        }
        return true;
    }

    /**
     * Updates an existing Venta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\db\Exception
     * @throws \Throwable
     */
    public function actionUpdate($id, $_pjax = null)
    {
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
        $parmsys_debe = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_debe]);

        $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
        $parmsys_haber = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_haber]);

        if (!isset($parmsys_debe)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Ganancia.");
            return $this->redirect(['index']);
        }
        if (!isset($parmsys_haber)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Pérdida.");
            return $this->redirect(['index']);
        }

        $coef = ParametroSistema::getEmpresaActualCoefCosto();
        if (!isset($coef)) {
            FlashMessageHelpsers::createWarningMessage("Falta especificar en el parámetro de la empresa el coeficiente de costo.");
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);
        $model->usar_tim_vencido = 0;
        if ($model->timbrado->fecha_fin < $model->fecha_emision) {
            $model->usar_tim_vencido = 1;
        }
        $detalles_bd = $model->detalleVentas;
        $ivaCtas_bd = $model->ivasCuentaUsadas;

        if (!$this->isEditable($model)) {
            return $this->redirect(['index']);
        }

        $entidad = Entidad::findOne(['id' => $model->entidad_id]);

        $model->ruc = $entidad != null ? $entidad->ruc : null;
        $model->nombre_entidad = $entidad != null ? $entidad->razon_social : null;
        $model->nro_timbrado = $model->timbradoDetalle->timbrado->nro_timbrado;
        $model->timbrado_detalle_id_prefijo = $model->timbrado_detalle_id;

        if ($model->fecha_emision != "" && $model->fecha_emision != "00-00-0000")
            $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

        $session = Yii::$app->session;
        $detalles = DetalleVenta::find()->where(['factura_venta_id' => $model->id])->all();

        if ($_pjax == null && !Yii::$app->request->isPost) {
            $session->set('cont_detalleventa-provider', new ArrayDataProvider([
                'allModels' => $detalles,
                'pagination' => false,
            ]));

            $session->remove('cont_activo_bio_stock_manager');
        }

        /** @var VentaIvaCuentaUsada[] $vta_iva_cta_used */
        $vta_iva_cta_used = VentaIvaCuentaUsada::find()
            ->where(['factura_venta_id' => $model->id, 'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->select('SUM(monto) as monto, iva_cta_id, plantilla_id, MIN(gnd_motivo) as gnd_motivo')
            ->groupBy('plantilla_id, iva_cta_id')
            ->all();

        foreach ($vta_iva_cta_used as $monto_iva_usada) {
            $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id]['gnd_motivo'] = $monto_iva_usada->gnd_motivo;
            if (isset($monto_iva_usada->ivaCta)) {
                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
            } else {
                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
            }
        }

        if (array_key_exists('VentaIvaCuentaUsada', $_POST)) {
            if (!empty($_POST['VentaIvaCuentaUsada']['new'])) { // eliminar el blue print
                unset($_POST['VentaIvaCuentaUsada']['new']);
                Yii::$app->request->setBodyParams($_POST);
                foreach ($_POST['VentaIvaCuentaUsada'] as $new_keys => $new) {
                    // la variable plantilla_id no viene en el post, si el select2 esta disabled.
                    // ocurre cuando se cambia de estado de una factura con detalles a faltante o anulada y el select2 de plantillas se deshabilita.
                    if (array_key_exists('plantilla_id', $new) && $new['plantilla_id'] == "") {
                        unset($_POST['VentaIvaCuentaUsada'][$new_keys]); // eliminar items que tienen plantilla_id vacio.
                        Yii::$app->request->setBodyParams($_POST);
                    }
                }
            }
        }

        // Verificar obligaciones de plantillas con obligaciones del modelo.

        // La verificacion se realiza si el modelo tiene asignado obligacion, sino no se hace.
        // Se hace asi porque en devel hay facturas que no tienen obligacion debido a que esto fue implementado
        // en produccion con un hotfix. En el FlashMessage, cuando se invoca $model->obligacion->nombre,
        // lanza excepcion "getting property of non-object".
        if ($_pjax == null && !Yii::$app->request->isPost && isset($model->obligacion)) {
            // Warning si la plantilla utilizada ya no pertenece a la obligacion de la compra
            /** @var VentaIvaCuentaUsada[] $ivaCtaUsadas */
            $ivaCtaUsadas = VentaIvaCuentaUsada::find()->where(['factura_venta_id' => $id])->all();
            $plantillas = [];
            foreach ($ivaCtaUsadas as $ivaCtaUsada) {
                $pertenece = false;
                foreach ($ivaCtaUsada->plantilla->plantillaObligaciones as $plantillaObligacion) {
                    if ($plantillaObligacion->obligacion_id == $model->obligacion_id) {
                        $pertenece = true;
                        break;
                    }
                }
                if (!$pertenece) {
                    if (!in_array($ivaCtaUsada->plantilla_id, $plantillas))
                        $plantillas[] = $ivaCtaUsada->plantilla_id;
                }
            }
            if (sizeof($plantillas)) {
                foreach ($plantillas as $key => $plantilla_id) {
                    $plantillas[$key] = PlantillaCompraventa::findOne(['id' => $plantilla_id])->nombre;
                }
                $submsg = "";
                if (sizeof($plantillas) == 1) {
                    $submsg = "La plantilla `{$plantillas[0]}` usada anteriormente ya no pertenece";
                } else {
                    $plantillas = implode(', ', $plantillas);
                    $submsg = "Las plantillas `{$plantillas}` usadas anteriormente ya no pertenecen";
                }
                FlashMessageHelpsers::createWarningMessage("{$submsg} a la obligación `{$model->obligacion->nombre}` de esta factura.");
            }
        }

        if ($model->load(Yii::$app->request->post())) {

            $trans = Yii::$app->db->beginTransaction();

            try {

                if (!empty($model->notas)) {
                    throw new \Exception('No se puede editar una factura que ya tiene una nota asociada.');
                }

                /** @var IvaCuenta $iva_cta */
                /** @var DetalleVenta[] $detalleVenta */

                if ($model->tipo_documento_id != '') {
                    $tipodoc = TipoDocumento::findOne(['id' => $model->tipo_documento_id]);
                    if (isset($tipodoc)) {
                        $model->condicion = $tipodoc->condicion;
                    }
                }
                $timbradoD = TimbradoDetalle::findOne(['id' => $model->timbrado_detalle_id_prefijo]);
                $model->prefijo = $timbradoD->prefijo;
                $model->timbrado_detalle_id = $timbradoD->id;
                $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                $model->entidad_id = $entidad != null ? $entidad->id : null;

                if ($model->fecha_emision != "" && $model->fecha_emision != "00-00-0000")
                    $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

                if ($model->condicion == 'credito') {
                    $model->fecha_vencimiento = implode('-', array_reverse(explode('-', $model->fecha_vencimiento)));
                }

                // alambrado: para aquellas ventas malformadas, cuyo saldo fuera negativo, lo corrige para que pase la validacion
                if ($model->saldo < 0) $model->saldo = 0;
                if (!$model->validate()) {
                    $msg = implode(', ', $model->getErrorSummary(true));
                    throw new \Exception("Error al validar la cabecera de factura: {$msg}.");
                }

                $venta_de_activofijo = false;
                $venta_de_activobiol = false;

                if (in_array($model->estado, ['anulada', 'faltante'])) {

                    $estado = $model->estado;
                    $fecha = $model->fecha_emision;
                    $nro_factura = $model->nro_factura;
                    $observaciones = $model->observaciones;
                    $tipoDocumento = $model->tipo_documento_id;
                    $timbradoDetalle = $model->timbrado_detalle_id;
                    $model = $this->findModel($id);
                    // al hacer findModel(), timbrado_detalle_id_prefijo es vacio por ser campo a nivel del modelo nada mas
                    //   y como es required, al validar se queja.
                    $model->timbrado_detalle_id_prefijo = $model->timbrado_detalle_id = $timbradoDetalle;
                    $model->estado = $estado;
                    $model->fecha_emision = $fecha; // no hace falta cambiarle de formato porque antes de la validacion (lineas mas arriba) ya se hizo
                    // se entiende que si es anulada o faltante, tal como se acordo con Jose, es igual que una anulada, por eso vencimiento y cantidad de cuotas podria bien valer "".
                    $model->fecha_vencimiento = "";
                    $model->cant_cuotas = "";
                    $model->condicion = "";
                    $model->para_iva = null;
                    $model->obligacion_id = "";
                    $model->nro_factura = $nro_factura;
                    $model->observaciones = $observaciones;
                    $model->tipo_documento_id = $tipoDocumento;
                    $model->total = 0;
                    $model->saldo = 0;
                    $model->valor_moneda = '';
                    $model->obligaciones = '';
                    $model->moneda_id = null;
                    $model->entidad_id = null;
                    $model->cotizacion_propia = null;

                    $check_ok = $model->checkNroFact('update');

                    if ($check_ok != Venta::NRO_FACTURA_CORRECTO) {

                        switch ($check_ok) {
                            case Venta::NRO_FACTURA_DUPLICADO:
                                throw new \Exception('Nro Factura Duplicado.');
                            case Venta::NRO_FACTURA_NO_RANGO:
                                throw new \Exception('Nro Factura no se encuentra en el rango del timbrado.');
                            case Venta::NRO_FACTURA_NO_PREFIJO:
                                throw new \Exception('La factura no tiene prefijo.');
                        }

                        throw new \Exception("Error desconocido. \$check_ok: {$check_ok}");
                    }

                    // rev.2: Revision del 14 set 2018
                    // rev.2: creo que ya no hace falta porque nuevamente, anulada == faltante excepto fecha_emision requerido.
                    if (!$model->save()) {
                        throw new \Exception('Error validando factura: ' . implode(', ', $model->getErrorSummary(true)));
                    }
                    $model->refresh();

//                    if ($model->estado == 'faltante') { // los nros de anuladas no se pueden reciclar, es como haber utilizado
                    $result = $model->updateNroActualSession($model->estado == 'faltante', 'update'); // si es faltante, hay que redisponibilizar el nro.
                    if (!$result['resultado']) {
                        throw new \Exception($result['errores']);
                    }
//                    }

                    // Des-asociar activos fijos.
                    foreach ($model->activoFijos as $activoFijo) {
                        $activoFijo->setFacturaVentaId(null);
                        $activoFijo->setEstado('activo');
                        if (!$activoFijo->save(false)) {
                            throw new \Exception("Error guardando activo fijo: {$activoFijo->getErrorSummaryAsString()}");
                        }
                    }

                    // Borrar ivas cuentas
                    foreach ($model->ivasCuentaUsadas as $ivasCuentaUsada) {
                        if (!$ivasCuentaUsada->delete()) {
                            throw new \Exception("Error interno: Error borrando iva cuenta usada: " . implode(', ', $ivasCuentaUsada->getErrorSummary(true)));
                        }
                    }

                    // Borrar detalles.
                    foreach ($model->detalleVentas as $detalleVenta) {
                        if (!$detalleVenta->delete()) {
                            throw new \Exception("Error borrando detalles de venta: " . implode(', ', $detalleVenta->getErrorSummary(true)));
                        }
                    }
//                    }

                    // restaurar stock de activo biologico:
                    // borrar todos los stockManagers generados por esta factura.
                    /** @var ActivoBiologicoStockManager[] $stock_managers */
                    $stock_managers = ActivoBiologicoStockManager::find()->where(['factura_venta_id' => $id])->andWhere(['IS', 'nota_venta_id', null])->all();
                    foreach ($stock_managers as $stock_manager) {
                        $actbio = ActivoBiologico::actualizarStock_O_CrearNuevo($stock_manager, $stock_manager->id);
                        if ($actbio->isNewRecord)
                            ActivoBiologico::isCreable();
                        else
                            if ($actbio->getInventario()->exists()) {
                                throw new \yii\base\Exception("Ya se generó el cierre de inventario de activos 
                                biológicos vendidos por esta factura, por lo tanto no se puede modificar más.");
                            }
                        if (!$actbio->save()) {
                            throw new Exception("Error restableciendo stock del Activo Biológico: {$actbio->getErrorSummaryAsString()}");
                        }
                        $actbio->refresh();
                        //echo "ahora stock actual es: {$actbio->stock_actual}(id: {$actbio->id})<br/>";

                        if (!$stock_manager->delete()) {
                            throw new  Exception("Error interno borrando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                        }
                        $stock_manager->refresh();
                    }

//                    if (!$model->save()) {
//                        throw new \Exception("Error guardando factura: {$model->getErrorSummaryAsString()}");
//                    }
//                    $model->refresh();

                    # Unificar talonarios
                    try {
                        $timbrado = $model->timbradoDetalle->timbrado;
                        $timbrado->unificarTalonarios();
                    } catch (\Exception $exception) {
                        throw new \Exception("Error interno unificando talonarios: {$timbrado->getErrorSummaryAsString()}");
                    }

                    $trans->commit();
                    FlashMessageHelpsers::createSuccessMessage("La factura {$model->getNroFacturaCompleto()} se ha modificado correctamente.");
                    return $this->redirect(['index']);
                }

                if (!$model->save(false)) {
                    throw new \Exception("Error guardando factura: {$model->getErrorSummaryAsString()}");
                } // para generar id.

                // Verificar cotizaciones de la set y valor_moneda.
                $fecha = date('Y-m-d', strtotime('-1 day', strtotime($model->fecha_emision)));
                $moneda_id = $model->moneda_id;
                /** @var Cotizacion $query */
                $query = Cotizacion::find()
                    ->where(['fecha' => $fecha, 'moneda_id' => $moneda_id])
                    ->andWhere(['!=', 'moneda_id', Moneda::findOne(['nombre' => 'Guaraní'])->id])
                    ->one();
                if ($model->moneda_id != 1 && ($query == null || $model->valor_moneda == '')) {
                    if ($query == null) throw new \Exception('Falta cargar cotización de la SET.');
                    elseif ($model->valor_moneda == '') throw new \Exception('Falta especificar valor de la moneda');
                } elseif ($model->moneda_id == \common\models\ParametroSistema::getMonedaBaseId()) {
                    $model->valor_moneda = '';
                }

                // Registrar cada uno de los montos de los ivas, junto con las cuentas usadas.
                $ningun_total = true;
                $ivacuenta_guardar = [];
                $ivacuenta_usadas = $_POST['VentaIvaCuentaUsada'];
                $plantilla_id = null;
                $obligacionCheck = ['match' => true, 'msg' => ""];
                foreach ($ivacuenta_usadas as $ivacuenta_usada) {
                    foreach ($ivacuenta_usada['ivas'] as $iva_x => $iva_x_value) // x es un numero que vale uno de los porcentajes de iva.
                    {
                        $iva_x_value = str_replace(',', '.', str_replace('.', '', $iva_x_value));
                        if ($iva_x_value != 0 && $iva_x_value != "" && $iva_x_value != "0") { // solo si hay monto indicado
                            $ningun_total = false;
                            $ivacuenta_venta = new VentaIvaCuentaUsada();
                            $ivacuenta_venta->monto = $model->moneda_id == 1 ? round($iva_x_value) : $iva_x_value;
                            $ivacuenta_venta->factura_venta_id = $model->id;
                            $ivacuenta_venta->plantilla_id = $ivacuenta_usada['plantilla_id'];
                            $ivacuenta_venta->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

                            $regex = preg_match('/iva_([0-9]+)/', $iva_x, $matches); // matches[1] es el numero capturado.
                            if ($regex != false && $regex != 0) {
                                if ($matches[1] == 0 || $matches[1] == "0") { // significa que el iva es cero
                                    $ivacuenta_venta->iva_cta_id = "";
                                    $ivacuenta_venta->plan_cuenta_id = "";
                                } else {
                                    $iva = Iva::findOne(['porcentaje' => $matches[1]]);
                                    $ivacuenta = IvaCuenta::findOne(['iva_id' => $iva->id]);
                                    $ivacuenta_venta->iva_cta_id = $ivacuenta->id;
                                    $ivacuenta_venta->plan_cuenta_id = $ivacuenta->cuenta_venta_id;
                                }
                            }

                            // Verificar motivo de gnd
                            $plantilla = PlantillaCompraventa::findOne(['id' => $ivacuenta_venta->plantilla_id]);
                            if (isset($plantilla) && $plantilla->deducible == 'no') {
                                if (!isset($ivacuenta_usada['gnd_motivo']))
                                    throw new \Exception("Error en la fila de plantilla {$plantilla->nombre}: Motivo por el que se envia a GND no puede estar vacio.");
                                $ivacuenta_venta->gnd_motivo = $ivacuenta_usada['gnd_motivo'];
                            }

                            if (!$ivacuenta_venta->validate()) {
                                $msg = implode(', ', $ivacuenta_venta->getErrorSummary(true));
                                throw new \Exception("Error interno con ivactaventa: {$msg}");
                            }

                            $ivacuenta_guardar[] = $ivacuenta_venta;
                            $plantilla_id = $ivacuenta_usada['plantilla_id'];
                        }
                    }

                    if (isset($plantilla_id)) {
                        $plantilla = PlantillaCompraventa::findOne($plantilla_id);
                        if ($plantilla->esParaActivoFijo())
                            $venta_de_activofijo = true;
                        elseif ($plantilla->esParaActivoBiologico())
                            $venta_de_activobiol = true;

                        // si hasta ahora no hubo discordancia entre obligacion de venta y de plantillas.
                        if ($obligacionCheck['match']) {
                            $idsObls = [];
                            foreach ($plantilla->obligaciones as $obligacion) {
                                in_array($obligacion->id, $idsObls) || $idsObls[] = $obligacion->id;
                            }
                            if (!in_array($model->obligacion_id, $idsObls)) {
                                $msg = ("La plantilla {$plantilla->nombre} no pertenece a la obligación {$model->obligacion->nombre} seleccionada.");
                                $obligacionCheck['match'] = false;
                                $obligacionCheck['msg'] = $msg;
                                // No se lanza la excepcion directamente aqui porque de lo contrario, las plantillas
                                // utilizadas no se van a ver al retornar al formulario
                            }
                        }
                    }
                }
                if ($ningun_total) {
                    throw new \Exception('Falta completar al menos un campo del Total Iva o Total Exenta.');
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Para poder seguir viendo las plantillas elegidas al retornar al form.
                $model->_iva_ctas_usadas = [];
                foreach ($ivacuenta_guardar as $monto_iva_usada) {

                    //Para cargar tabla de plantillas ya existentes
                    if (array_key_exists($monto_iva_usada->plantilla_id, $model->_iva_ctas_usadas)) {
                        if ($monto_iva_usada->ivaCta != null) {
                            if (array_key_exists($monto_iva_usada->ivaCta->iva->porcentaje, $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id])) {
                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] += $monto_iva_usada->monto;
                            } else {
                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
                            }
                        } else {
                            if (array_key_exists(0, $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id])) {
                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] += $monto_iva_usada->monto;
                            } else {
                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
                            }
                        }
                    } else {
                        if ($monto_iva_usada->ivaCta != null) {
                            $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
                        } else {
                            $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
                        }
                    }
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////////

                if (!$obligacionCheck['match']) {
                    $model->_iva_ctas_usadas = [];
                    throw new \Exception($obligacionCheck['msg']);
                }

                // No permitir guardar factura sin detalle
                $detalleVenta = $session['cont_detalleventa-provider']->allModels;
                if (sizeof($detalleVenta) == 0) {
                    throw new \Exception('Faltan los detalles de la factura');
                }

                // borrar detalles anteriores.
                foreach ($detalles_bd as $detalle) {
                    if (!$detalle->delete()) {
                        throw new \Exception('Error Borrando detalles anteriores...');
                    }
                    //echo "Borrando {$detalle->planCuenta->nombre}, partida {$detalle->cta_contable}, subtotal:{$detalle->subtotal}<br/>";
                }
                echo "<br/>";

                // borramos ivas cuentas anteriores
                foreach ($ivaCtas_bd as $ivasCuentaUsada) {
                    if (!$ivasCuentaUsada->delete()) {
                        throw new \Exception('ERROR INTERNO: No se pudieron borrar ivactas usadas...');
                    }
                }

                $saldo = ['debe' => 0.0, 'haber' => 0.0];
                $hayCtaPrincipal = false;
                foreach ($detalleVenta as $_detalle) {
                    $detalle = new DetalleVenta();
                    $detalle->setAttributes($_detalle->attributes);
                    unset($detalle->id);
                    $saldo[$detalle->cta_contable] += round($detalle->subtotal, 2);
                    $detalle->factura_venta_id = $model->id;
                    $detalle->periodo_contable_id = ($detalle->periodo_contable_id == null) ? Yii::$app->session->get('core_empresa_actual_pc') : $detalle->periodo_contable_id;
                    if (!$detalle->save()) {
                        throw new \Exception('Error en detalle de factura: ' . implode(', ', $detalle->getErrorSummary(true)));
                    }

                    if ($detalle->cta_principal == 'si')
                        $hayCtaPrincipal = true;
                }

                if (!$hayCtaPrincipal) {
                    throw new \Exception("Se necesita establecer al menos una cuenta como principal.");
                }

                // TODO: VERIFICAR BALANCE DE SALDOS
                if (round($saldo['debe'], 2) !== round($saldo['haber'], 2)) {
                    throw new \Exception("El DEBE y el HABER NO son iguales. Verifique: Debe = {$saldo['debe']}, Haber = {$saldo['haber']}. Se recomienda verificar cómo está guardada la plantilla utilizada y también revisar los detalles y las simulaciones generados automáticamente.");
                }
                $model->total = $saldo['debe']; // para evitar trampa

//            $result = Venta::checkNroFactura($model->nro_factura, $model->prefijo, $model->id);
                $result = $model->checkNroFact('update');
                if ($result != Venta::NRO_FACTURA_CORRECTO) {
                    switch ($result) {
                        case Venta::NRO_FACTURA_DUPLICADO:
                            throw new \Exception("Este Nro de factura ya fue utilizado.");
                        case Venta::NRO_FACTURA_NO_RANGO:
                            throw new \Exception('Este Nro de factura no corresponde a ningún rango de ningún timbrado.');
                        case Venta::NRO_FACTURA_NO_PREFIJO:
                            throw new \Exception('Este prefijo de factura no existe. DEBE CREAR desde el menú de Timbrado primero.');
                    }

                    throw new \Exception("Error desconocido - result of checkNroFact('update'): {$result}");
                }

                // Actualizar el detalle timbrado corresp. con el nro_actual adecuado.
                $result = $model->updateNroActualSession(false, 'update');
                if (!$result['resultado']) {
                    throw new \Exception("Error actualizando Nro Factura en Sesion: {$result['errores']}.");
                }

                // No hace falta preguntar si tiene o no asociado recibo porque haber podido entrar hasta aqui significa que isEditable() fue true, metodo dentro del cual se verifica si tiene o no recibo asociado.
                $model->saldo = ($model->condicion == 'credito') ? $model->total : 0;

//                // guardamos los detalles
//                foreach ($detalleVenta as $detalle) {
//                    if (!$detalle->save(false)) {
//                        throw new \Exception("Error guardando detalle: {$detalle->getErrorSummaryAsString()}");
//                    }
//                }

                // guardar ivas cuentas
                foreach ($ivacuenta_guardar as $ivacta) {
                    if (!$ivacta->save(false)) {
                        throw new \Exception("Error interno guardando iva cuenta: {$ivacta->getErrorSummaryAsString()}");
                    }
                }

                force_save:;
                if (!$model->save()) {
                    throw new \Exception("Error guardando venta: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();
                /** ---------------------- [inicio] Manejo de Activos fijos ---------------------- */
                if (!in_array($model->estado, ['anulada', 'faltante'])) {
                    $skey = 'cont_activofijo_ids';
                    $activos_f = [];
                    if ($venta_de_activofijo && Yii::$app->session->has($skey)) {
                        foreach ($model->activoFijos as $activoFijo) {
                            $activoFijo->setFacturaVentaId(null);
                            $activoFijo->setEstado('activo');
                            if (!$activoFijo->save(false)) {
                                throw new \Exception("Error deshaciendo activo fijo anterior: {$activoFijo->getErrorSummaryAsString()}");
                            }
                        }
                        foreach (Yii::$app->session->get($skey) as $id) {
                            $activofijo = ActivoFijo::findOne(['id' => $id]);
                            $activofijo->setFacturaVentaId($model->id);
                            $activofijo->setEstado('vendido');
                            $activos_f[] = $activofijo;
                        }
                        foreach ($activos_f as $item) {
                            if (!$item->save(false)) {
                                throw new \Exception("Error guardando activo fijo: {$item->getErrorSummaryAsString()}");
                            }
                        }
                    } elseif ($venta_de_activofijo && !Yii::$app->session->has($skey)) {
                        throw new \Exception('Esta Factura registra un detalle por venta de uno o más Activo Fijo, pero Ud. no ha especificado ninguna. Especifique primero.');
                    } elseif (!$venta_de_activofijo) {
                        foreach ($model->activoFijos as $activoFijo) {
                            $activoFijo->setFacturaVentaId(null);
                            $activoFijo->setEstado('activo');
                            if (!$activoFijo->save(false)) {
                                throw new \Exception("Error deshaciendo activo fijo anterior: {$activoFijo->getErrorSummaryAsString()}");
                            }
                        }
                    }
                } else {
                    // TODO: Tal vez no sea necesario si se ejecuta lo mismo antes.
                    foreach ($model->activoFijos as $activoFijo) {
                        $activoFijo->setFacturaVentaId(null);
                        $activoFijo->setEstado('activo');
                        if (!$activoFijo->save(false)) {
                            throw new \Exception("Error deshaciendo activo fijo anterior: {$activoFijo->getErrorSummaryAsString()}");
                        }
                    }
                }
                /** ------------------------ [fin] Manejo de Activos fijos ------------------------ */

                /** ------------------------ [inicio] Manejo de Activos biologicos ------------------------ */
                if (!in_array($model->estado, ['anulada', 'faltante']) && $venta_de_activobiol) {
                    $session_key = 'cont_activo_bio_stock_manager';
                    $sessionData = Yii::$app->session->get($session_key);

                    if (!Yii::$app->session->has($session_key) || !isset($sessionData) ||
                        (isset($sessionData) && empty($sessionData['dataProvider']->allModels))) {
                        throw new \Exception("Falta especificar la cantidad de A. Biológicos a comprar.");
                    } else {
                        // eliminar stock managers anteriores
                        /** @var ActivoBiologicoStockManager $stockManager */
                        foreach (ActivoBiologicoStockManager::find()
                                     ->where(['factura_venta_id' => $id])
                                     ->andWhere(['IS', 'nota_venta_id', null])->all() as $stockManager) {

                            $actbio = $stockManager->activoBiologico;
                            if (!$stockManager->delete()) {
                                throw new \yii\base\Exception("Error interno eliminando stock managers
                                de activos biológicos anteriores: {$stockManager->getErrorSummaryAsString()}");
                            }
                            $stockManager->refresh();
                            $actbio->refresh();
                            $actbio->recalcularStockActual();
                            if (!$actbio->save())
                                throw new \yii\base\Exception("Error interno restableciendo stock actual del
                                activo biologico {$actbio->nombre}: {$actbio->getErrorSummaryAsString()}");
                            $actbio->refresh();
                        }

                        /** @var ActivoBiologicoStockManager[] $stock_managers */
                        $stock_managers = $sessionData['dataProvider']->allModels;

                        if (!isset($stock_managers) || empty($stock_managers))
                            throw new \Exception("Falta especificar la cantidad de A. Biológicos a comprar.");

                        foreach ($stock_managers as $stock_manager) {
                            $stock_manager->factura_venta_id = $model->id;

                            if (!$stock_manager->datosValidos()) {
                                throw new \Exception("Error interno validando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                            }

                            // guardar manager
                            if (!$stock_manager->save()) {
                                throw  new \Exception("Error interno guardando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                            }
                            $stock_manager->refresh();

                            $actbio = ActivoBiologico::actualizarStock_O_CrearNuevo($stock_manager);
                            if ($actbio->isNewRecord)
                                ActivoBiologico::isCreable();
                            else
                                if ($actbio->getInventario()->exists()) {
                                    throw new \yii\base\Exception("Ya se generó el cierre de inventario de activos 
                                    biológicos vendidos por esta factura, por lo tanto no se puede modificar más.");
                                }
                            if (!$actbio->save()) {
                                throw new Exception("Error actualizando stock del Activo Biológico: {$actbio->getErrorSummaryAsString()}");
                            }
                            $actbio->refresh();
                        }
                    }
                } elseif (!$venta_de_activobiol) {
                    $query = ActivoBiologicoStockManager::find()
                        ->where(['=', 'nota_venta_id', $id]);

                    /** @var ActivoBiologicoStockManager $stock_manager */
                    foreach ($query->all() as $stock_manager) {
                        $actbio = ActivoBiologico::actualizarStock_O_CrearNuevo($stock_manager, $stock_manager->id);
                        if ($actbio->isNewRecord)
                            ActivoBiologico::isCreable();
                        else
                            if ($actbio->getInventario()->exists()) {
                                throw new \yii\base\Exception("Ya se generó el cierre de inventario de activos 
                                biológicos vendidos por esta factura, por lo tanto no se puede modificar más.");
                            }
                        if (!$actbio->save()) {
                            throw new Exception("Error restaurando stock del Activo Biológico: {$actbio->getErrorSummaryAsString()}");
                        }
                        $actbio->refresh();

                        if (!$stock_manager->delete()) {
                            throw new \Exception("Error interno borrando StockManager Id={$stock_manager->id}: {$stock_manager->getErrorSummaryAsString()}");
                        }
                        $stock_manager->refresh();
                    }
                }
                /** ------------------------ [fin] Manejo de Activos biologicos ------------------------ */

                #Verificar si se invirtio si el tipo de documento set es nota de credito
                if (!$model->isPartidaCorrect())
                    throw new \Exception("Error validando venta: El tipo de documento SET es NOTA DE CREDITO pero las partidas no se han invertido.");

                $trans->commit();
                $msg_cambio_estado = "";
                if (array_key_exists('estado', $model->dirtyAttributes)) {
                    $estado = ucfirst($model->estado);
                    $msg_cambio_estado = " El estado ha cambiado a {$estado}";
                }
                FlashMessageHelpsers::createSuccessMessage('La factura ' . $model->getNroFacturaCompleto() . ' se ha modificado correctamente.' . $msg_cambio_estado);
                return $this->redirect(['index',]);
            } catch (\Exception $exception) {
//                throw $exception;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 20000);
                if ($model->fecha_emision != "" && $model->fecha_emision != "00-00-0000")
                    $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

                if ($model->condicion == 'credito')
                    $model->fecha_vencimiento = implode('-', array_reverse(explode('-', $model->fecha_vencimiento)));
            }
        }

        retorno:;
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        throw new ForbiddenHttpException('The requested page doesn\'t exist');

        // No eliminar codigo de eliminado, por si se requiera mas tarde.

//        /** @var DetalleVenta[] $model_detalle */
//        /** @var VentaIvaCuentaUsada[] $iva_cta_usada */
//
//        $model_venta = $this->findModel($id);
//        $nro_factura = $model_venta->getNroFacturaCompleto();
//        $model_detalle = DetalleVenta::findAll(['factura_venta_id' => $id]);
//        $iva_cta_usada = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $model_venta->id]);
//        $retencion = $model_venta->retencion;
//        // TODO: Hay de determinar bajo que condiciones sobre los activosfijos, una factura venta no se puede borrar.
//
//        try {
//            $t = Yii::$app->db->beginTransaction();
//            try {
//                foreach ($model_detalle as $item) {
//                    $item->delete();
//                }
//            } catch (\Exception $e) {
//                throw new Exception('Error borrando detalles: ' . $item->getErrorSummary(true)[0]);
//            }
//
//            try {
//                foreach ($iva_cta_usada as $item) {
//                    $item->delete();
//                }
//            } catch (\Exception $e) {
//                throw new Exception('Error Interno: NO se pudo borrar IvaCuentaUsada.');
//            }
//
//            if ($retencion != null) {
//                throw new Exception('Esta factura tiene asociada la retención ' . $retencion->nro_retencion . '. Elimínelo primero.');
//            }
//
//            if ($model_venta->asiento_id != null) {
//                throw new Exception('Esta factura tiene asociado asientos. Elimínelos primero.');
//            }
//
//            // Desvincular activos fijos.
//            foreach ($model_venta->activoFijos as $activoFijo) {
//                $activoFijo->setFacturaVentaId(null);
//                $activoFijo->setEstado('activo');
//                $activoFijo->save(false);
//            }
//            $result = $model_venta->updateNroActualSession(true, 'update');
//            if ($result['resultado']) {
//                $model_venta->delete();
//                $t->commit();
//                FlashMessageHelpsers::createSuccessMessage("La factura {$nro_factura} se ha borrado exitosamente.");
//            } else
//                throw new \Exception('Erro actualizando nro_actual en TimbradoDetalle: ' . $result['errores']);
//        } catch (\Exception $e) {
//            $t->rollBack();
//            FlashMessageHelpsers::createWarningMessage('Error al intentar borrara la factura ' . $model_venta->getNroFacturaCompleto() . ": " . $e->getMessage());
//        }
//
//        retorno:;
//        return $this->redirect(['index']);
    }

    /**
     * Finds the Venta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Venta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Venta::findOne(['id' => $id, 'tipo' => 'factura'])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('El registro solicitado no está disponible.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /**
     * Retorna el siguiente nro de factura venta disponible.
     *
     * La primera vez retorna el menor nro_inicio de la tabla cont_timbrado_detalle. Luego de haberse usado una vez,
     * el siguiente número consecutivo se almacena en la sesión y retorna ésto.
     *
     * @return int|mixed
     * @throws Exception
     */
    public function actionGetNextNro(/*$isExistentRecord*/)
    {
        return $this->obtenerNextNro(/*$isExistentRecord*/);
    }

    /** Sistema de cache de nro_factura.
     *
     * Se maneja en la sesion, un array de la forma:
     *      $key = <id del timbradoDetalle>, $value = <concatenacion del nro_actual, '|' y nro_timbrado>.
     *
     *  Si es update, realiza busqueda en la BD y retorna <concatenacion del nro_actual, '|' y nro_timbrado>.
     *  Si es create, realiza busqueda en la sesion y retorna <concatenacion del nro_actual, '|' y nro_timbrado>.
     *
     *  Si no encuentra de la sesion, realiza busqueda en BD, lo agrega en el array de la sesion y retorna
     *      <concatenacion del nro_actual, '|' y nro_timbrado> buscando en el array por $timbradoDetalle->id.
     *
     * @return string
     * @throws Exception
     */
    private function obtenerNextNro(/*$isExistentRecord*/)
    {
        /** @var TimbradoDetalle $query */

        $action_id = $_POST['action_id'];
        $detalle_id = $_POST['id_detalle'];

        if ($action_id == 'update') {
            $timbradoDetalle = TimbradoDetalle::findOne(['id' => $detalle_id]);

            if ($timbradoDetalle != null) {
                $nro_actual = $timbradoDetalle->nro_actual;
                $nro_timbrado = $timbradoDetalle->timbrado->nro_timbrado;

                // cuando vuelva a seleccionar el prefijo original,
                // no tiene que devolver el siguiente nro del timbradoDetalle.
                $venta_model = Venta::findOne(['id' => $_POST['model_id']]);
                if ($venta_model->timbrado_detalle_id == $detalle_id)
                    $nro_actual = (integer)$venta_model->nro_factura;

                return $nro_actual . '|' . $nro_timbrado;
            }
        }

        $data = [];
        $svar = 'cont_next_nro_factura';
        $sesion = Yii::$app->session;

        if (!$sesion->has($svar)) {
            $sesion->set($svar, $data);
        }

        $data = $sesion->get($svar);

        if (!array_key_exists($detalle_id, $data)) {
            $timbradoDetalle = TimbradoDetalle::findOne(['id' => $detalle_id]);
            $nro_actual = "";
            $nro_timbrado = "";

            if ($timbradoDetalle != null) {
                $nro_actual = $timbradoDetalle->nro_actual;
                $nro_timbrado = $timbradoDetalle->timbrado->nro_timbrado;
            }

            $data[$timbradoDetalle->id] = $nro_actual . '|' . $nro_timbrado;
            $sesion->set($svar, $data);
        }

        return $sesion->get($svar)[$detalle_id];


        /** Implementarion anterior, se dejara como backup por un tiempo. */
        $timbradoDetalle = TimbradoDetalle::findOne(['id' => $detalle_id]);
        $nro_actual = "";
        $nro_timbrado = "";
        if ($timbradoDetalle != null) {
            $nro_actual = $timbradoDetalle->nro_actual;
            $nro_timbrado = $timbradoDetalle->timbrado->nro_timbrado;

            // cuando vuelva a seleccionar el prefijo original, no tiene que devolver el siguiente nro del timbradoDetalle.
            if ($_POST['action_id'] == 'update') {
                $venta_model = Venta::findOne(['id' => $_POST['model_id']]);
                if ($venta_model->timbrado_detalle_id == $detalle_id)
                    $nro_actual = (integer)$venta_model->nro_factura;
            }
        }

//        if (!$isExistentRecord) {
//            $svar = [];
//            if (Yii::$app->session->has($skey)) {
//                $svar = Yii::$app->session->get($skey);
//                if (array_key_exists($prefijo, $svar)) {
//                    $nro_actual = $svar[$prefijo]['nro_actual'];
//                } else {
//                    $svar[$prefijo] = ['nro_actual' => $nro_actual, 'detalle_id' => $query->id];
//                }
//            } else {
//                $svar[$prefijo] = ['nro_actual' => $nro_actual, 'detalle_id' => $query->id];
//            }
//            Yii::$app->session->set($skey, $svar);
//        }

        return $nro_actual . '|' . $nro_timbrado;
    }

    /**
     * Verifica si el nro de factura corresponde a algún cont_timbrado_detalle.
     *
     * @param string $nro El último número con ceros a la izquierda del formulario factura venta.
     * @param string $prefijo El prefijo seleccionado en el formulario de factura venta.
     * @param null $idToExclude
     * @return array 1: nro de factura correcto, 2: nro de factura duplicado, 3: prefijo de factura no existe,
     * 4: nro de factura no existe en ningun timbrado_detalle
     */
    public function actionCheckNroFactura($nro, $prefijo, $timbrado_detalle_id, $action_id, $idToExclude = null /*$isNewRecord = 1*/)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $estado = Venta::checkNroFactura($nro, $prefijo, $timbrado_detalle_id, $action_id, $idToExclude);

        switch ($estado) {
            case Venta::NRO_FACTURA_NO_RANGO:
                return ['result' => '', 'error' => 'Este Nro no pertenece a ningún timbrado.'];
            case Venta::NRO_FACTURA_DUPLICADO:
                return ['result' => '', 'error' => 'Este Nro ya fue utilizado.'];
            case Venta::NRO_FACTURA_NO_PREFIJO:
                return ['result' => '', 'error' => 'El prefijo no es válido o no existe.'];
        }

        return ['result' => '', 'error' => ''];
    }

    /**
     * Guardar en la sesión los valores de los campos totales por iva.
     *
     * @return bool
     */
    public function actionHoldTotalesEnSession()
    {
        $skey = 'campos_totales_nombre_valor';
        $session = Yii::$app->session;
        if ($session->has($skey)) {
            $svalue = $session->get($skey);
            $svalue[$_POST['campo_nombre']] = $_POST['campo_valor'];
            $session->set($skey, $svalue);
        } else
            Yii::$app->session->set($skey, [$_POST['campo_nombre'] => $_POST['campo_valor']]);
        return true;
    }

    /**
     * Borrar variables de la sessión.
     *
     * @return bool
     */
    public function actionDeleteTotalesFromSession()
    {
        foreach ($_POST['campos_totales_nombre'] as $key => $value) {
            Yii::$app->session->remove($value . '-disp');
        }
        return true;
    }

    /**
     * Retornar condición a utilizar en el formulario, dependiendo del tipo de documento elegido.
     *
     * @return string
     */
    public function actionCondicionFromTipoDoc()
    {
        $query = TipoDocumento::findOne(['id' => $_POST['tipo_doc_id']]);

//        Lanzar mensaje en caso de no existir prefijos
//        $resultado = Timbrado::getPrefsEmpActualAjax($_POST['tipo_doc_id']);
//        if (empty($resultado) || empty($resultado['results'])) {
//            FlashMessageHelpsers::createWarningMessage('No tiene ningún timbrado vigente.');
//        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $query->condicion;
    }

    /**
     * Guardar en sesión valores usados en formulario.
     *
     * @return bool
     */
    public function actionCamposUsadosEnsesion()
    {
        $skey = 'cont_valores_usados';
        $session = Yii::$app->session;
        if (!$session->has($skey)) $session->set($skey, []);

        $cont_valores_usados = $session->get($skey);
        if (sizeof($cont_valores_usados) == 0) {
            $cont_valores_usados = [];
        }
        $cont_valores_usados[$_POST['campo_id']] = $_POST['campo_valor'];

        $session->set($skey, $cont_valores_usados);
        return true;
    }

    public function actionCamposUsadosDesesion()
    {
        $skey = 'cont_valores_usados';
        $session = Yii::$app->session;
        if (!$session->has($skey)) return false;

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $session->get($skey);
    }

    /**
     * @param $ruc
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionGetEntidadByRuc($ruc)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Venta::getEntidadByRuc($ruc);
    }

    public function actionGetRazonSocialByRuc()
    {
        $ruc = $_POST['ruc'];
        $entidad = Entidad::findOne(['ruc' => $_POST['ruc']]);
        if ($entidad != null)
            return $entidad->razon_social;

        return '';
    }

    public function actionCheckRuc()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $ruc = Yii::$app->request->get('ruc');
        $error = '';

        $entidad = Entidad::findOne(['ruc' => $ruc]);
        if (!$entidad) {
            $error = 'Este R.U.C. no corresponde a ninguna entidad registrada.';
        }

        return ['result' => '', 'error' => $error];
    }

    /** -------------- CREADAS JUNTO CON MULTIPLES PLANTILLAS -------------- */

    public function actionGetIvasDePlantilla()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $plantilla_id = $_GET['plantilla_id'];
        $pds = PlantillaCompraventaDetalle::findAll(['plantilla_id' => $plantilla_id, 'tipo_asiento' => 'venta']);
        $ivas_p = [];

        /** @var PlantillaCompraventaDetalle $pd */
        foreach ($pds as $pd) {
            if ($pd->ivaCta == null)
                $ivas_p[] = 0;
            else
                $ivas_p[] = $pd->ivaCta->iva->porcentaje;
        }
        return $ivas_p;
    }

    /** -------------- [FIN] CREADAS JUNTO CON MULTIPLES PLANTILLAS -------------- */

    /** -------------- PARA BOTON FORWARD/REWIND --------------  */

    public function actionNavegate($id, $goto)
    {
        $op_rels = [
            'next' => '<',
            'prev' => '>'
        ];

        $venta = $this->findModel($id);
        $concat = "CONCAT((prefijo), ('-'), (nro_factura))";

        $ventas = Venta::find()
            ->where([$op_rels[$goto], $concat, $venta->getNroFacturaCompleto()])
            ->andWhere(['empresa_id' => Yii::$app->session->get('core_empresa_actual')])
            ->andWhere(['periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->orderBy($concat . ($goto == 'next' ? ' DESC' : ' ASC'))->all();

        /** @var Venta $venta */
        foreach ($ventas as $venta) {
            if ($this->isEditable($venta))
                return $this->redirect(['update', 'id' => $venta->id]);
        }

        FlashMessageHelpsers::createInfoMessage('Ha llegado al tope.');
        return $this->redirect(['update', 'id' => $id]);
    }

    public function actionNavegateFromCreate($goto, $prefijo = null, $nro_fac = null)
    {
        $op_rels = [
            'next' => '<',
            'prev' => '>'
        ];

        $concat = "CONCAT((prefijo), ('-'), (nro_factura))";

        $nro_completo = "{$prefijo}-{$nro_fac}";

        if (strlen($nro_completo) < 15)
            return $this->redirect(['create']);

        $venta = Venta::find()
            ->where([$op_rels[$goto], $concat, $nro_completo])
            ->andWhere(['empresa_id' => Yii::$app->session->get('core_empresa_actual')])
            ->andWhere(['periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->orderBy($concat . ($goto == 'prev' ? ' ASC' : ' DESC'))->one();

        $url = "";

        if (!isset($venta)) {
            return $this->redirect(['create']);
        } else
            return $this->redirect(['update', 'id' => $venta->id]);
    }

    /** -------------- [FIN] PARA BOTON FORWARD/REWIND --------------  */

    public function actionBloquear($id)
    {
        $model = Venta::findOne(['id' => $id]);

        if ($model->bloqueado == 'no') {
            $model->bloqueado = 'si';

            //Solo para validacion
            $model->timbrado_detalle_id_prefijo = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Factura ha sido bloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede bloquear factura. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionDesbloquear($id)
    {
        $model = Venta::findOne(['id' => $id]);

        if ($model->bloqueado == 'si') {
            $model->bloqueado = 'no';

            //Solo para validacion
            $model->timbrado_detalle_id_prefijo = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Factura ha sido desbloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede desbloquear factura. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionGetDefaultDetalles()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->get('venta_id');
        if (!isset($id)) {
            $msg = "Esta venta no tiene detalles.";
            FlashMessageHelpsers::createWarningMessage($msg);
            return ['success' => false, 'error' => true];
        }
        $session = Yii::$app->session;

        $detalles = DetalleVenta::find()->where(['factura_venta_id' => $id])->all();

        $session->set('cont_detalleventa-provider', new ArrayDataProvider([
            'allModels' => $detalles,
            'pagination' => false,
        ]));
        return ['success' => true, 'error' => false];
    }

    public function actionGetMotivoNoVigencia()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
        return Venta::getMotivoNoVigente($empresa_id, $periodo_id);
    }
}