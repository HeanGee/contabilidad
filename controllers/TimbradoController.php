<?php

namespace backend\modules\contabilidad\controllers;

use backend\models\Empresa;
use backend\modules\contabilidad\models\auxiliar\TimbradoDetalle2;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\search\TimbradoSearch;
use backend\modules\contabilidad\models\Timbrado;
use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\TipoDocumentoSet;
use backend\modules\contabilidad\models\validators\NroTimbrado2Validator;
use backend\modules\contabilidad\models\validators\TimbradoFechaValidator;
use backend\modules\contabilidad\models\Venta;
use common\helpers\FlashMessageHelpsers;
use Exception;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TimbradoController implements the CRUD actions for Timbrado model.
 */
class TimbradoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Timbrado models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimbradoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $skey = 'cont_detalletimbrado-provider';
        Yii::$app->session->has($skey) ? Yii::$app->session->remove($skey) : true;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Timbrado model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Timbrado model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $_pjax
     * @return mixed
     * @throws \Throwable
     */
    public function actionCreate($_pjax = null)
    {
        $model = new Timbrado();
        $session = Yii::$app->session;
        $skey = 'cont_detalletimbrado-provider';

        if ($_pjax == null && !Yii::$app->request->isPost) {
            $session->set($skey, new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaccion = Yii::$app->db->beginTransaction();
            try {
                $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                $model->entidad_id = $entidad != null ? $entidad->id : null;

                if (!$model->validate()) {
                    throw new Exception("Error al validar timbrado: {$model->getErrorSummaryAsString()}");
                }
                $model->save(false);

                /** @var TimbradoDetalle[] $detalles */
                $detalles = $session->get($skey)->allModels;
                foreach ($detalles as $detalle) {
                    $detalle->timbrado_id = $model->id;
                    if ($detalle->validate()) continue;
                    else {
                        throw new Exception("Error al validar detalle: {$detalle->getErrorSummaryAsString()}");
                    }
                }

                foreach ($detalles as $detalle) {
                    if (!$detalle->save(false)) {
                        throw new Exception("Error al guardar detalle: {$detalle->getErrorSummaryAsString()}");
                    }

                    // al crear, no se realiza la absorcion.
                }

                $transaccion->commit();
                FlashMessageHelpsers::createSuccessMessage('Timbrado guardado exitosamente.');
//            unset($session[$skey]);
                return $this->redirect(['index']);
            } catch (Exception $exception) {
                $transaccion->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        retorno:;
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Absorve detalles que quedan dentro del rango.
     * Es utilizado desde create/update/createNewTimbrado (usado desde venta y venta-nota)
     *
     * @param TimbradoDetalle $detalle
     * @param Timbrado $model
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function absorverDetalles(&$detalle, &$model)
    {
        if ($detalle->nro_inicio < $detalle->nro_fin) {
            $detalle->refresh();
            $detallesExistentes = TimbradoDetalle::find()->where([
                'timbrado_id' => $model->id,
                'tipo_documento_set_id' => $detalle->tipo_documento_set_id,
                'prefijo' => $detalle->prefijo,
            ]);

            $detallesExistentes->andWhere("nro_inicio = nro_fin");
            $detallesExistentes->andWhere(['BETWEEN', 'nro_inicio', $detalle->nro_inicio, $detalle->nro_fin]);
            $detallesExistentes->andWhere(['!=', 'id', $detalle->id]);
            $detallesExistentes->orderBy(['nro_inicio' => SORT_ASC]);
            if ($detallesExistentes->exists()) {
                ini_set('memory_limit', '-1');
                $detallesExistentes = $detallesExistentes->all();

                $universe = range($detalle->nro_inicio, $detalle->nro_fin); // array de valores consecutivos
                $base = array_combine($universe, $universe); // index == value

                /** @var TimbradoDetalle[] $detallesExistentes */
                foreach ($detallesExistentes as $key => $_detalle) {
                    Yii::warning("{$_detalle->nro_inicio}, ");
                    unset($base[$_detalle->nro_inicio]); // eliminar nros ya usados (recordar: index == value)
                }
                Yii::warning(implode(', ', $base));
                $base = array_values($base); // index en orden consecutivo desde 0
                $desde = $base[0];
                $nvos_detalles = [];
                $creado = false;
                $load['TimbradoDetalle'] = [
                    'tipo_documento_set_id' => $detalle->tipo_documento_set_id,
                    'prefijo' => $detalle->prefijo,
                ];
                if (!$detalle->delete()) {
                    throw new Exception("Error interno borrando detalle rango: {$detalle->getErrorSummaryAsString()}");
                }
                foreach ($base as $index => $item) {
                    $creado = false;
                    if ($index == 0) continue;
                    if ($item == $base[$index - 1]) continue;
                    elseif ($item > ($base[$index - 1] + 1)) {
                        $hasta = $base[$index - 1];
//                        Yii::warning("crea detalle con {$desde} al {$hasta}");
                        $nvo_detalle = new TimbradoDetalle();
                        $nvo_detalle->timbrado_id = $model->id;
                        $nvo_detalle->load($load);
                        $nvo_detalle->nro_inicio = $desde;
                        $nvo_detalle->nro_fin = $hasta;
                        $nvo_detalle->nro_actual = $desde;
                        if (!$nvo_detalle->save())
                            throw new Exception("Error validando nuevo detalle: {$nvo_detalle->getErrorSummaryAsString()}");
                        $nvos_detalles[] = $nvo_detalle;
                        $desde = $item;
                        $creado = true;
                    }
                }
                if (!$creado) {
                    $hasta = $base[sizeof($base) - 1];
//                    Yii::warning(echo "-crea detalle con {$desde} al {$hasta}");
                    $nvo_detalle = new TimbradoDetalle();
                    $nvo_detalle->timbrado_id = $model->id;
                    $nvo_detalle->load($load);
                    $nvo_detalle->nro_inicio = $desde;
                    $nvo_detalle->nro_fin = $hasta;
                    $nvo_detalle->nro_actual = $desde;
                    if (!$nvo_detalle->save())
                        throw new Exception("Error validando nuevo detalle: {$nvo_detalle->getErrorSummaryAsString()}");
                    $nvos_detalles[] = $nvo_detalle;
                } else {
                    $desde = $hasta = $base[sizeof($base) - 1];
//                    Yii::warning(echo "--crea detalle con {$desde} al {$hasta}");
                    $nvo_detalle = new TimbradoDetalle();
                    $nvo_detalle->timbrado_id = $model->id;
                    $nvo_detalle->load($load);
                    $nvo_detalle->nro_inicio = $desde;
                    $nvo_detalle->nro_fin = $hasta;
                    $nvo_detalle->nro_actual = $desde;
                    if (!$nvo_detalle->save())
                        throw new Exception("Error validando nuevo detalle: {$nvo_detalle->getErrorSummaryAsString()}");
                    $nvos_detalles[] = $nvo_detalle;
                }
                foreach ($nvos_detalles as $index => $_detalle) {
                    $msg = "({$index}) {$_detalle->id} - crea detalle con {$_detalle->nro_inicio} al {$_detalle->nro_fin}";
                    Yii::warning($msg);
                }
            }
        }
    }

    /**
     * Updates an existing Timbrado model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param null $_pjax
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionUpdate($id, $_pjax = null)
    {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $skey = 'cont_detalletimbrado-provider';
        $entidad = Entidad::findOne(['id' => $model->entidad_id]);
        $model->ruc = $entidad != null ? $entidad->ruc : null;
        $model->nombre_entidad = $entidad != null ? $entidad->razon_social : null;

        if ($_pjax == null && !Yii::$app->request->isPost) {
            $session->set($skey, new ArrayDataProvider([
                'allModels' => TimbradoDetalle::findAll(['timbrado_id' => $model->id]),
                'pagination' => false,
            ]));
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaccion = Yii::$app->db->beginTransaction();
            try {
                $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                $model->entidad_id = $entidad != null ? $entidad->id : null;

                if (!$model->validate()) {
                    throw new Exception("Error validando timbrado: {$model->getErrorSummaryAsString()}");
                }
                $model->save(false);

                /** @var TimbradoDetalle[] $detalles */
                $detalles = $session->get($skey)->allModels;
                foreach ($detalles as $detalle) {
                    $detalle->timbrado_id = $model->id;
                    if ($detalle->validate()) continue;
                    else {
                        throw new Exception("Error validando detalle: {$detalle->getErrorSummaryAsString()}");
                    }
                }

                foreach ($detalles as $detalle) {
                    if (!$detalle->save(false)) {
                        throw new Exception("Error guardando detalle: {$detalle->getErrorSummaryAsString()}");
                    }

                    // absorver y actualizar facturas
                    self::absorverDetalles($detalle, $model);
                }
                $transaccion->commit();
                FlashMessageHelpsers::createSuccessMessage("Timbrado {$model->nro_timbrado} guardado exitosamente.");
                return $this->redirect(['index']);
            } catch (Exception $exception) {
//                throw $exception;
                $transaccion->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        retorno:;
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Timbrado model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $timbrado_en_uso = $model->getCompras()->exists() || $model->getVentas()->exists();
        if ($timbrado_en_uso) {
            FlashMessageHelpsers::createWarningMessage('Este timbrado está siendo utilizado por una o más facturas.');
            goto retorno;
        }

        $trans = Yii::$app->db->beginTransaction();
        foreach ($model->timbradoDetalles as $timbradoDetalle) {
            if (!$timbradoDetalle->delete()) {
                $msg = '';
                foreach ($timbradoDetalle->getErrorSummary(true) as $item) {
                    $msg .= $item . ', ';
                }
                FlashMessageHelpsers::createWarningMessage('No se pudieron borrar los detalles: ' . substr($msg, strlen($msg) - 3) . '.');
                $trans->rollBack();
                goto retorno;
            }
        }
        $model->delete();
        $trans->commit();
        FlashMessageHelpsers::createSuccessMessage('El timbrado se ha borrado correctamente.');

        retorno:;
        return $this->redirect(['index']);
    }

    /**
     * [2019-06-10] Comunicado de Jose: Magali quiere que se unifique todo.
     *
     * @param null $id
     * @return Response
     * @throws \Throwable
     */
    public function actionUnificarTalonario($id = null)
    {
        if (!isset($id)) {
            FlashMessageHelpsers::createWarningMessage("Error en la petición de solicitud.");
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->isPost)
            try {
                $timbrado = $this->findModel($id);

                if (!isset($timbrado))
                    throw new Exception("El timbrado con id `{$id}` no existe.");

                if (empty($timbrado->timbradoDetalles))
                    throw new Exception("Este timbrado no tiene detalles.");

                $transaction = Yii::$app->db->beginTransaction();

                # Ejecuta la unificacion.
                $timbrado->unificarTalonarios();

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage('La unificación se ha realizado correctamente.', 100);
            } catch (Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 7000);
            }

        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Finds the Timbrado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timbrado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Timbrado::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $prefijo
     * @param $tipoDocumentoId
     * @param null $esTipoSet
     * @param bool $submit
     * @return array|string
     * @throws \Throwable
     */
    public function actionCreateNewTimbrado($prefijo, $tipoDocumentoId, $esTipoSet = null, $submit = false)
    {
        $model = new Timbrado2();
        $model_detalle = new TimbradoDetalle();
        $model_detalle->prefijo = $prefijo;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model, $model_detalle);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post())) {
            $t = Yii::$app->db->beginTransaction();
            try {
                $empresa = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')]);
                $entidad = Entidad::findOne(['ruc' => $empresa->ruc]);
                if (!isset($entidad)) {
                    throw new Exception("Falta crear la Entidad asociada a la Empresa actual, con el mismo R.U.C.");
                }
                $model->entidad_id = $entidad->id;

                if (!empty($esTipoSet)) {
                    $model_detalle->tipo_documento_set_id = $tipoDocumentoId;
                } else {
                    /** @var TipoDocumento $tipoDoc */
                    $tipoDoc = TipoDocumento::find()->where(['id' => $tipoDocumentoId])->one();
                    $model_detalle->tipo_documento_set_id = $tipoDoc->tipo_documento_set_id;
                }

                if ($model->validate()) {
                    $query = Timbrado::findOne([
                        'nro_timbrado' => $model->nro_timbrado,
                        'entidad_id' => $model->entidad_id,
                    ]);
                    // Verificar si existe cabecera timbrado
                    if ($query == null)
                        $model->save(false);
                    else
                        $model = $query;
                    $model_detalle->timbrado_id = $model->id;
                    $model_detalle->nro_actual = $model_detalle->nro_inicio;
                    if ($model_detalle->save()) {
                        self::absorverDetalles($model_detalle, $model);
                    } else {
                        throw new Exception("Error validando detalle: {$model_detalle->getErrorSummaryAsString()}");
                    }

                    # Unificar talonario
                    try {
                        $timbrado = $model_detalle->timbrado;
                        $timbrado->unificarTalonarios();
                    } catch (Exception $exception) {
                        throw new Exception("Error interno unificando talonario: {$timbrado->getErrorSummaryAsString()}");
                    }

                    $t->commit();
                    FlashMessageHelpsers::createSuccessMessage('Detalle timbrado agregado correctamente.');
                } else {
                    throw new Exception("Error validando timbrado: {$model->getErrorSummaryAsString()}");
                }
            } catch (Exception $exception) {
                $t->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        };
        retorno:;
        return $this->renderAjax('_modalform_facturaventa_addtimbrado', [
            'model' => $model,
            'model_detalle' => $model_detalle,
        ]);
    }

    /**
     * @param bool $submit
     * @return array|string
     * @throws \yii\db\Exception
     */
    public function actionCreateNewTimbradoRetencion($prefijo, $nro, $operacion, $factura_id, $submit = false)
    {
        $model = new Timbrado2();
        $model_detalle = new TimbradoDetalle();
        $model_detalle->prefijo = $prefijo;
        $model_detalle->nro_actual = (integer)$nro;
        $model_detalle->nro_fin = (integer)$nro;
        $model_detalle->nro_inicio = (integer)$nro;
        $model_detalle->nro_completo = $prefijo . '-' . $nro;

        try {
            $nro = str_replace('-', '', $nro);
            if (strlen($nro) < 7) {
                throw new Exception("El formato de los ultimos 7 dígitos están incorrectos.");
            }

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post()) && $submit == false) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model, $model_detalle);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post())) {
                $t = Yii::$app->db->beginTransaction();

                $entidad = null;

                // El timbrado creado desde retencion, debe seguir el siguiente mecanismo:

                //  - Si es creado desde retencion de venta, la entidad a asociarse debe ser igual a la entidad asociada a la venta.
                //  - Si es creado desde retencion de compra, la entidad debe ser igual a la de la empresa actual.

                if ($operacion == 'compra') {
                    $ruc = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->ruc;
                    $entidad = Entidad::findOne(['ruc' => $ruc]);
                } else {
                    $venta = Venta::findOne(['id' => $factura_id]);
                    $entidad = $venta->entidad;
                }

                $model->entidad_id = $entidad->id;
                $empresa_id = \Yii::$app->session->get('core_empresa_actual');
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-tipodoc_set_retencion";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                $tipodoc_set_retencion = null;

                if ($parametro_sistema == null) {
                    throw new Exception('Antes de crear un nuevo Timbrado, debe definir el tipo de documento para retenciones en el parámetro del sistema.');
                }

                $tipodoc_set_retencion = TipoDocumentoSet::findOne(['id' => $parametro_sistema->valor]);
                $model_detalle->tipo_documento_set_id = $tipodoc_set_retencion->id;

                if ($model->validate()) {

                    $query = Timbrado::findOne([
                        'nro_timbrado' => $model->nro_timbrado,
                        'entidad_id' => $model->entidad_id,
                    ]);

                    // Verificar si existe cabecera timbrado
                    if ($query == null)
                        $model->save(false);
                    else
                        $model = $query;

                    $model_detalle->timbrado_id = $model->id;
                    $model_detalle->nro_actual = $model_detalle->nro_inicio;

                    if ($model_detalle->validate()) {
                        $model_detalle->save(false);
                    } else {
                        throw new Exception('Error en los detalles del timbrado: ' . implode(', ', $model->getErrorSummary(true)));
                    }

                    $t->commit();
                    FlashMessageHelpsers::createSuccessMessage('Detalle timbrado agregado correctamente.');

                } else {
                    throw new Exception('Error validando cabecera de timbrado: ' . implode(', ', $model->getErrorSummary(true)));
                }
            };

        } catch (Exception $exception) {
            $t->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        retorno:;
        return $this->renderAjax('_modalform_retencion_addtimbrado', [
            'model' => $model,
            'model_detalle' => $model_detalle,
        ]);
    }

    /**
     * @param bool $submit
     * @return array|string
     * @throws \yii\db\Exception
     * @throws \Throwable
     */
    public function actionCreateNewTimbradoCompra($entidadRuc, $prefijo, $nro, $tipoDocumentoId, $submit = false, $esNota = false, $timbrado_id = false)
    {
        $model = new \backend\modules\contabilidad\models\auxiliar\Timbrado2();
        $timbrado = $timbrado_id ? Timbrado::findOne($timbrado_id) : null;
        //$model->nro_timbrado = $timbrado != null ? $timbrado->nro_timbrado : null;
        //$model->fecha_inicio = $timbrado != null ? implode('-', array_reverse(explode('-', $timbrado->fecha_inicio))) : null;
        //$model->fecha_fin = $timbrado != null ? implode('-', array_reverse(explode('-', $timbrado->fecha_fin))) : null;

        $model_detalle = new TimbradoDetalle2();
        $model_detalle->prefijo = $prefijo;
        $model_detalle->nro_inicio = (int)$nro;
        $model_detalle->nro_fin = (int)$nro;
        $model_detalle->timbrado_id = $timbrado_id ? $timbrado_id : null;
        if ($esNota) {
            $model_detalle->tipo_documento_set_id = $tipoDocumentoId;
        } else {
            /** @var TipoDocumento $tipoDoc */
            $tipoDoc = TipoDocumento::find()->where(['id' => $tipoDocumentoId])->one();
            $model_detalle->tipo_documento_set_id = $tipoDoc->tipo_documento_set_id;
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model_detalle->timbrado_id = $timbrado_id ? $timbrado_id : null;
            return ActiveForm::validate($model, $model_detalle);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post())) {

            if (Entidad::find()->where(['ruc' => $entidadRuc])->exists()) {
                $model->entidad_id = Entidad::find()->where(['ruc' => $entidadRuc])->one()->id;
            }
            $t = Yii::$app->db->beginTransaction();
            if ($model->validate()) {
                $query = Timbrado::findOne([
                    'nro_timbrado' => $model->nro_timbrado,
                ]);

                //El timbrado pertenece a otra entidad
                if ($query != null && $query->entidad_id != $model->entidad_id) {
                    $t->rollBack();
                    FlashMessageHelpsers::createErrorMessage('Error. El nro de timbrado pertenece a otra entidad.');
                    return false;
                }

                // Verificar si existe cabecera timbrado
                if ($query == null)
                    $model->save(false);
                else
                    $model = $query;

                $model_detalle->timbrado_id = $model->id;
                $model_detalle->nro_actual = $model_detalle->nro_inicio;

                //En caso de que los numero de inicio y fin estén vacíos
                //crea un detalle con el numero de factura
                if ($model_detalle->nro_inicio == null && $model_detalle->nro_fin == null) {
                    $model_detalle->nro_inicio = $nro;
                    $model_detalle->nro_fin = $nro;
                    $model_detalle->nro_actual = $nro;
                } elseif ($model_detalle->nro_inicio == null && $model_detalle->nro_fin != null) {
                    $model_detalle->nro_inicio = $model_detalle->nro_fin;
                    $model_detalle->nro_actual = $model_detalle->nro_fin;
                } elseif ($model_detalle->nro_inicio != null && $model_detalle->nro_fin == null) {
                    $model_detalle->nro_fin = $model_detalle->nro_inicio;
                    $model_detalle->nro_actual = $model_detalle->nro_inicio;
                }

                if ($model_detalle->validate()) {
                    $model_detalle->save(false);
                } else {
                    $t->rollBack();
                    FlashMessageHelpsers::createErrorMessage('Error en los detalles del timbrado. Verifique.');
                    return false;
                }

                # Unificar talonario
                try {
                    $timbrado = $model_detalle->timbrado;
                    $timbrado->unificarTalonarios();
                } catch (Exception $exception) {
                    throw new Exception("Error interno unificando talonario: {$timbrado->getErrorSummaryAsString()}");
                }

                $t->commit();
                FlashMessageHelpsers::createSuccessMessage('Detalle timbrado agregado correctamente.');
                return ['id' => $model_detalle->timbrado_id, 'timbrado' => $model_detalle->timbrado->nro_timbrado, 'nro_inicio' => $model_detalle->nro_inicio, 'nro_fin' => $model_detalle->nro_fin];
            } else {
                $model->fecha_inicio = implode('-', array_reverse(explode('-', $model->fecha_inicio)));
                $model->fecha_fin = implode('-', array_reverse(explode('-', $model->fecha_fin)));

                $t->rollBack();
                FlashMessageHelpsers::createErrorMessage('Error validando cabecera de timbrado. -- ' . $model->getErrorSummary(true)[0]);
                return false;
            }
        };
        retorno:;
        return $this->renderAjax('_modalform_facturacompra_addtimbrado', [
            'model' => $model,
            'model_detalle' => $model_detalle,
        ]);
    }

    /**
     * @param $entidadRuc
     * @param $prefijo
     * @param $nro
     * @param $tipoDocumentoId
     * @param bool $submit
     * @param bool $esNota
     * @param bool $timbrado_id
     * @return array|string
     * @throws \yii\db\Exception
     */
    public function bk_actionCreateNewTimbradoCompra($entidadRuc, $prefijo, $nro, $tipoDocumentoId, $submit = false, $esNota = false, $timbrado_id = false)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        try {
            $transaction = Yii::$app->db->beginTransaction();

            $model = new \backend\modules\contabilidad\models\auxiliar\Timbrado2();
            $model_detalle = new TimbradoDetalle2();
            $model_detalle->prefijo = $prefijo;
            $model_detalle->nro_inicio = (int)$nro;
            $model_detalle->nro_fin = (int)$nro;
            if ($esNota) {
                $model_detalle->tipo_documento_set_id = $tipoDocumentoId;
            } else {
                /** @var TipoDocumento $tipoDoc */
                $tipoDoc = TipoDocumento::find()->where(['id' => $tipoDocumentoId])->one();
                $model_detalle->tipo_documento_set_id = $tipoDoc->tipo_documento_set_id;
            }

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post()) && $submit == false) {
                return ActiveForm::validate($model, $model_detalle);
            }

            if ($model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post())) {

                if (Entidad::find()->where(['ruc' => $entidadRuc])->exists()) {
                    $model->entidad_id = Entidad::find()->where(['ruc' => $entidadRuc])->one()->id;
                }

                $timbradoBD = Timbrado::find()->where(['nro_timbrado' => $model->nro_timbrado]);
                if ($timbradoBD->exists()) {
                    $timbradoBD = $timbradoBD->one();
                    if ($timbradoBD->entidad_id != $model->entidad_id)
                        throw new Exception('Error. El nro de timbrado pertenece a otra entidad.');

                    $model = $timbradoBD;
                }

                if (!$model->save()) {
//                    return ActiveForm::validate($model);
                    throw new Exception("No se puede guardar timbrado: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                $model_detalle->timbrado_id = $model->id;
                $model_detalle->nro_actual = $model_detalle->nro_inicio;

                if (!$model_detalle->save()) {
//                    return ActiveForm::validate($model_detalle);
                    throw new Exception("No se puede guardar timbrado: {$model_detalle->getErrorSummaryAsString()}");
                }
                $model_detalle->refresh();

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("Timbrado {$model->nro_timbrado} creado correctamente.");
                return ['id' => $model_detalle->timbrado_id, 'timbrado' => $model_detalle->timbrado->nro_timbrado, 'nro_inicio' => $model_detalle->nro_inicio, 'nro_fin' => $model_detalle->nro_fin];
            }
        } catch (Exception $exception) {
            (!isset($transaction)) || $transaction->rollBack();
            FlashMessageHelpsers::createErrorMessage($exception->getMessage());
            return false;
        }
        retorno:;
        return $this->renderAjax('_modalform_facturacompra_addtimbrado', [
            'model' => $model,
            'model_detalle' => $model_detalle,
        ]);
    }

    public function actionRangeValidator()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $nro_inicio = Yii::$app->request->get('nro_inicio');
        $nro_fin = Yii::$app->request->get('nro_fin');
        $timbrado = Yii::$app->request->get('nro_timbrado');
        $prefijo = Yii::$app->request->get('prefijo');
        $model_id = Yii::$app->request->get('model_id');
        $error = '';

        $detallesExistentes = TimbradoDetalle::find()
            ->joinWith('timbrado as timbrado')
            ->where([
                'timbrado.nro_timbrado' => $timbrado,
                'prefijo' => $prefijo
            ]);
        if ($model_id != 'false') {//En caso de update no se debe validar contra sí misma
            $detallesExistentes->andWhere(['<>', 'cont_timbrado_detalle.id', $model_id]);
        }

        if (($nro_inicio == 'false' && $nro_fin != 'false') || ($nro_inicio != 'false' && $nro_fin == 'false')) {
            $nro = $nro_inicio == 'false' ? $nro_fin : $nro_inicio;
            // Verificar si los rangos no se solapan para un mismo prefijo
            if ($detallesExistentes->exists()) {
                /** @var TimbradoDetalle $detalle */
                foreach ($detallesExistentes->all() as $detalle) {
                    if ($nro >= $detalle->nro_inicio && $nro <= $detalle->nro_fin &&
                        !($detalle->nro_inicio == $detalle->nro_fin)) {
                        $error = 'Se solapa con uno de los detalles';
                        break;
                    }
                }
            }
        }

        if ($nro_inicio != 'false' && $nro_fin != 'false') {
            // Verificar si los rangos no se solapan para un mismo prefijo
            if ($detallesExistentes->exists()) {
                /** @var TimbradoDetalle $detalle */
                foreach ($detallesExistentes->all() as $detalle) {
                    if ((($nro_inicio >= $detalle->nro_inicio && $nro_inicio <= $detalle->nro_fin) ||
                            ($nro_fin >= $detalle->nro_inicio && $nro_fin <= $detalle->nro_fin)) &&
                        !($detalle->nro_inicio == $detalle->nro_fin)) {
                        $error = 'Se solapa con uno de los detalles';
                        break;
                    }

                    if ((($nro_inicio == $detalle->nro_inicio && $nro_inicio == $detalle->nro_fin) ||
                            ($nro_fin == $detalle->nro_inicio && $nro_fin == $detalle->nro_fin)) &&
                        $detalle->nro_inicio == $detalle->nro_fin) {
                        $error = 'Se solapa con uno de los detalles';
                        break;
                    }
                }
            }

            $query_2 = TimbradoDetalle::find()
                ->joinWith('timbrado as timbrado')
                ->where([
                    'timbrado.nro_timbrado' => $timbrado,
                    'prefijo' => $prefijo,
                ])
                ->andWhere(['>', 'nro_inicio', $nro_inicio])
                ->andWhere(['<', 'nro_fin', $nro_fin]);
            if ($model_id != 'false') {//En caso de update no se debe validar contra sí misma
                $query_2->andWhere(['<>', 'cont_timbrado_detalle.id', $model_id]);
            }
            if ($query_2->exists()) {
                foreach ($query_2->all() as $detalle) {
                    if (!($detalle->nro_inicio == $detalle->nro_fin)) {
                        $error = 'El rango se solapa con uno de los detalles.';
                        break;
                    }
                }
            }
        }

        return ['result' => '', 'error' => $error];
    }

    public function actionGetFechasTimbrado()
    {
        $nro_tim = $_POST['nro_timbrado'];
        $timbrado = Timbrado2::findOne(['nro_timbrado' => $nro_tim]);
        if ($timbrado != null) {
            $date = date_create_from_format('Y-m-d', $timbrado->fecha_inicio);
            $fecha_i = '';
            if ($date)
                $fecha_i = $date->format('d-m-Y');
            $date = date_create_from_format('Y-m-d', $timbrado->fecha_fin);
            $fecha_f = '';
            if ($date)
                $fecha_f = $date->format('d-m-Y');
        }
        return ($timbrado != null) ? $fecha_i . '|' . $fecha_f : "";
//        return ($timbrado != null) ? $timbrado->fecha_inicio.'|'.$timbrado->fecha_fin : "";
    }

    public function actionGetRazonSocialByRuc()
    {
        $ruc = $_POST['ruc'];
        return ($ruc != '') ? (Entidad::find()->where(['ruc' => $_POST['ruc']])->exists() ? Entidad::findOne(['ruc' => $_POST['ruc']])->razon_social : null) : null;
    }

    public function actionGetPrefsEmpActual($q = null, $id = null, $es_tipo_set = null, $fecha_factura = null, $vencido = '0')
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return Timbrado::getPrefsEmpActualAjax($id, Yii::$app->request->get('model_id'), Yii::$app->request->get('action_id'), $es_tipo_set, $fecha_factura, $vencido);
    }

    public function actionGetTimbradosVigentes($ruc, $nro_factura, $fecha, $tipo_documento_id, $vencidos, $q = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return Timbrado::getTimbradosVigentesAjax($ruc, $fecha, $nro_factura, $tipo_documento_id, $vencidos);
    }

    public function actionValidateAttributeFecha()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        try {
            $desde = implode('-', array_reverse(explode('-', $_GET['desde'])));
            $hasta = implode('-', array_reverse(explode('-', $_GET['hasta'])));
            $d1 = new \DateTime($desde);
            $d2 = new \DateTime($hasta);

            $diff = $d2->diff($d1);
            if (!(($diff->y == 1 && $diff->m == 0) || ($diff->y == 0))) {
                return "El periodo de vigencia es de $diff->y año(s) $diff->m mes(es) $diff->d día(s).";
            }

            if ($desde == null && $hasta == null) {
                return "Fecha Fin es requerido si Fecha Inicio es vacío.";
            }
        } catch (Exception $exception) {
            return "Algo está mal en la fecha";
        }

        return ""; // false es interpretado como cadena vacia desde javascript.
    }
}

class Timbrado2 extends Timbrado
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entidad_id',], 'required'],
            [['nro_timbrado', 'entidad_id'], 'integer'],
//            [['nro_timbrado'], 'unique'], // voy a comentar con la esperanza de que si el nro de timbrado es igual haga un update.
            [['fecha_inicio', 'fecha_fin', 'entidad_tmp'], 'safe'],
            [['entidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entidad::className(), 'targetAttribute' => ['entidad_id' => 'id']],
            ['nro_timbrado', NroTimbrado2Validator::className()],
            [['fecha_fin'], TimbradoFechaValidator::className()],
        ];
    }
}