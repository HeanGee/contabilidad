<?php

namespace backend\modules\contabilidad\controllers;

use backend\modules\contabilidad\models\auxiliar\Aranduka;
use backend\modules\contabilidad\models\auxiliar\ArchivosMultiple;
use common\helpers\FlashMessageHelpsers;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\web\UploadedFile;

class ArandukaController extends \backend\controllers\BaseController
{
    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionParseToJson()
    {
        $model = new ArchivoAranduka();
        $model->agregar_familiares = 'no';

        if ($model->load(Yii::$app->request->post())) {
            try {
                ini_set('memory_limit', '-1');
                $model->imageFiles = UploadedFile::getInstance($model, 'imageFiles');
                $model->imageFiles2 = UploadedFile::getInstance($model, 'imageFiles2');

                $objPHPExcel = IOFactory::load($model->imageFiles->tempName);
                $sheetDataIngresos = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $objPHPExcel = IOFactory::load($model->imageFiles2->tempName);
                $sheetDataEgresos = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $json = self::getJsonToFile($sheetDataIngresos, $sheetDataEgresos, $model);

                if (Yii::$app->session->has('aranduka_errors')) {
                    throw new Exception("Se encontraron errores en el excel. Siga los mensajes de error y corrija el archivo correspondiente.");
                }

                // Crear file temporalmente.
                $filePath = "uploads/JSON_Ingresos_Egresos_Para_SET.json";
                $handle = fopen($filePath, "w");
                fwrite($handle, $json);
                fclose($handle);

                // Enviar al navegador.
                Yii::$app->response->sendFile($filePath);

                // Borrar file temporal.
                unlink($filePath);
            } catch (Exception $exception) {
//                throw $exception;
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('parse-to-json', [
            'model' => $model,
        ]);
    }

    /**
     * @param array $ingresoFila
     * @param ArchivoAranduka $model
     * @throws Exception
     */
    private function preprocIngreso(&$ingresoFila, $nro_fila, &$model)
    {
        $tipo = (int)$ingresoFila['tipo'];

        if (!array_key_exists($tipo, Aranduka::getTipodocIngresoDesc())) {
            $col = 'A';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            $msg2 = "Tipo de documento $tipo no existe para Ingresos";
            throw new Exception("$msg $msg2");
        }

        $tipotexto = Aranduka::getTipodocIngresoDesc($tipo);

        if (strtolower($tipotexto) != strtolower(trim($ingresoFila['tipoTexto']))) {
            $ingresoFila['tipoTexto'] = $tipotexto;
        }

        if (strlen($ingresoFila['fecha']) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $ingresoFila['fecha'])) {
            $col = 'E';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            throw new Exception("{$msg} Formato de fecha requerido AAAA-MM-DD, se encontró {$ingresoFila['fecha']}");
        }

        if ($tipo == Aranduka::TDI_FACTURA && $ingresoFila['timbradoCondicion'] == '') {
            $col = 'L';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            throw new Exception("$msg La columna $col está vacía pero es requerido porque el tipo de documento es Factura");
        }

        if (in_array($tipo, [Aranduka::TDI_FACTURA, Aranduka::TDI_NOTA_DE_CREDITO]) && $ingresoFila['timbradoDocumento'] == '') {
            $col = 'K';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if (in_array($tipo, [Aranduka::TDI_FACTURA, Aranduka::TDI_NOTA_DE_CREDITO]) && $ingresoFila['timbradoNumero'] == '') {
            $col = 'J';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if (!in_array($ingresoFila['relacionadoTipoIdentificacion'], ['RUC', 'CEDULA', 'CEDULA_EXTRANJERA', 'IDENTIFICACION_TRIBUTARIA', 'PASAPORTE'])) {
            $col = 'J';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            $msg2 = "La columna $col está vacía pero es requerida obligatoriamente";
            throw new Exception("$msg $msg2");
        }

        if ($tipo == Aranduka::TDI_LIQUIDACION_DE_SALARIO && $ingresoFila['mes'] == '') {
            $col = 'F';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if ($tipo == Aranduka::TDI_EXTRACTO_DE_CUENTA && $ingresoFila['cuentaNumero'] == '') {
            $col = 'P';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        } elseif ($tipo != Aranduka::TDI_EXTRACTO_DE_CUENTA) {
            unset($ingresoFila['cuentaNumero']);
        }

        if ($tipo == Aranduka::TDI_EXTRACTO_DE_CUENTA && $ingresoFila['cuentaRazonSocial'] == '') {
            $col = 'Q';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        } elseif ($tipo != Aranduka::TDI_EXTRACTO_DE_CUENTA) {
            unset($ingresoFila['cuentaRazonSocial']);
        }

        if ($tipo == Aranduka::TDI_OTROS_DOCUMENTOS_RESPALDO_INGRESOS && $ingresoFila['tipoDocumentoOtros'] == '') {
            $col = 'R';
            $msg = "Error en la fila {$nro_fila}, columna $col de ingreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }
    }

    /**
     * @param $filaEgreso
     * @param $nro_fila
     * @param $model
     * @throws Exception
     */
    private function preprocEgreso(&$filaEgreso, $nro_fila, &$model)
    {
        $tipo = (int)$filaEgreso['tipo'];

        if (!array_key_exists($tipo, Aranduka::getTipodocEgresoDesc())) {
            $col = 'A';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "Tipo de documento $tipo no existe para Egresos";
            throw new Exception("$msg $msg2");
        }

        $tipotexto = Aranduka::getTipodocEgresoDesc($tipo);

        if ($tipo != Aranduka::TDE_EXTRACTO_CUENTA_IPS && !in_array($filaEgreso['relacionadoTipoIdentificacion'], ['RUC', 'CEDULA', 'CEDULA_EXTRANJERA', 'IDENTIFICACION_TRIBUTARIA', 'PASAPORTE'])) {
            $col = 'E';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if (strlen($filaEgreso['fecha']) && !preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $filaEgreso['fecha'])) {
            $col = 'C';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            throw new Exception("{$msg} Formato de fecha requerido AAAA-MM-DD, se encontró {$filaEgreso['fecha']}");
        }

        if ($tipo != Aranduka::TDE_EXTRACTO_CUENTA_IPS && $filaEgreso['relacionadoNombres'] == '') {
            $col = 'G';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if ($tipo != Aranduka::TDE_EXTRACTO_CUENTA_IPS && $filaEgreso['relacionadoNumeroIdentificacion'] == '') {
            $col = 'F';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if ($tipo == Aranduka::TDE_FACTURA && $filaEgreso['timbradoCondicion'] == '') {
            $col = 'J';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if (in_array($tipo, [Aranduka::TDE_FACTURA, Aranduka::TDE_AUTOFACTURA, Aranduka::TDE_NOTA_DE_CREDITO, Aranduka::TDE_TICKET])
            && $filaEgreso['timbradoDocumento'] == '') {
            $col = 'I';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if (in_array($tipo, [Aranduka::TDE_FACTURA, Aranduka::TDE_AUTOFACTURA, Aranduka::TDE_NOTA_DE_CREDITO, Aranduka::TDE_TICKET])
            && $filaEgreso['timbradoNumero'] == '') {
            $col = 'H';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if (!array_key_exists($filaEgreso['tipoEgreso'], Aranduka::getTipoEgresoDesc())) {
            $col = 'S';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "El tipo de egreso nº {$filaEgreso['tipoEgreso']} no existe";
            throw new Exception("$msg $msg2");
        }

        if (!array_key_exists($filaEgreso['subtipoEgreso'], Aranduka::getClasifEgresoDesc())) {
            $col = 'U';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La clasificación de egreso nº {$filaEgreso['subtipoEgreso']} no existe";
            throw new Exception("$msg $msg2");
        }

        if ($tipo == Aranduka::TDE_DESPACHO_DE_IMPORTACION && $filaEgreso['numeroDespacho'] == '') {
            $col = 'P';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if (in_array($tipo, [Aranduka::TDE_LIQUIDACION_DE_SALARIO, Aranduka::TDE_EXTRACTO_CUENTA_IPS])
            && $filaEgreso['mes'] == '') {
            $col = 'D';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if ($tipo == Aranduka::TDE_EXTRACTO_CUENTA_IPS && $filaEgreso['empleadorIdentificacion'] == '') {
            $col = 'W';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if ($tipo == Aranduka::TDE_TRANSF_O_GIROS_BANCAR_BOLETA_DE_DEPTO && $filaEgreso['cuentaNumero'] == '') {
            $col = 'L';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        } elseif ($tipo != Aranduka::TDE_TRANSF_O_GIROS_BANCAR_BOLETA_DE_DEPTO) {
            unset($filaEgreso['cuentaNumero']);
        }

        if ($tipo == Aranduka::TDE_TRANSF_O_GIROS_BANCAR_BOLETA_DE_DEPTO && $filaEgreso['cuentaRazonSocial'] == '') {
            $col = 'M';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        } elseif ($tipo != Aranduka::TDE_TRANSF_O_GIROS_BANCAR_BOLETA_DE_DEPTO) {
            unset($filaEgreso['cuentaRazonSocial']);
        }

        if ($tipo == Aranduka::TDE_OTROS_COMPROBANTES_VENTA_RESPALDO_EGRESO && $filaEgreso['tipoDocumentoOtros'] == '') {
            $col = 'N';
            $msg = "Error en la fila {$nro_fila}, columna $col de egreso: ";
            $msg2 = "La columna $col está vacía pero es requerida porque el tipo de documento es $tipo: $tipotexto";
            throw new Exception("$msg $msg2");
        }

        if (!in_array($tipo, [
            Aranduka::TDE_OTROS_COMPROBANTES_VENTA_RESPALDO_EGRESO,
            Aranduka::TDE_COMPROBANTE_EXTERIOR_LEGALIZADO, Aranduka::TDE_COMPROBANTE_INGRESO_ENT_PUBLICAS
        ])) {
            unset($filaEgreso['numeroDocumento']);
        }
    }

    /**
     * @param array $sheetDataIngresos
     * @param array $sheetDataEgresos
     * @param ArchivoAranduka $model
     * @return false|string
     * @throws Exception
     */
    private function getJsonToFile(&$sheetDataIngresos, &$sheetDataEgresos, &$model)
    {
        $aranduka = new Aranduka();
        $persona_fisica = $aranduka->personaFisica;
        Yii::$app->session->remove('aranduka_errors');

        // Cabecera.
        $informante = [
            'ruc' => $model->ruc
        ];
        $identificacion = [
            'periodo' => $model->periodo
        ];
        $persona_fisica['informante'] = $informante;
        $persona_fisica['identificacion'] = $identificacion;

        $errores = [];

        // Ingresos.
        foreach ($sheetDataIngresos as $nro_fila => $fila) {
            if ($nro_fila == 1) continue;
            if (trim($fila['A']) == '') break;

            $ingreso = [
                "id" => $nro_fila - 1,
                "ruc" => $model->ruc,
                "periodo" => $model->periodo,
                'tipo' => "{$fila['A']}",
                "tipoTexto" => strlen($fila['B']) ? $fila['B'] : null,
                "tipoIngreso" => $fila['C'],
                "tipoIngresoTexto" => strlen($fila['D']) ? $fila['D'] : null,
                "fecha" => (strlen($fila['E'])) ? $fila['E'] : null,
                "mes" => strlen($fila['F']) ? "{$fila['F']}" : null,
                "relacionadoTipoIdentificacion" => strlen($fila['G']) ? strtoupper($fila['G']) : null,
                "relacionadoNumeroIdentificacion" => "{$fila['H']}",
                "relacionadoNombres" => strlen($fila['I']) ? $fila['I'] : null,
                "timbradoNumero" => strlen($fila['J']) ? "{$fila['J']}" : null,
                "timbradoDocumento" => strlen($fila['K']) ? "{$fila['K']}" : null,
                "timbradoCondicion" => (strlen($fila['L'])) ? strtolower($fila['L']) : null,
                "ingresoMontoGravado" => (int)$fila['M'],
                "ingresoMontoNoGravado" => (int)$fila['N'],
                "ingresoMontoTotal" => (int)$fila['O'],
                "cuentaNumero" => strlen($fila['P']) ? "{$fila['P']}" : null,
                "cuentaRazonSocial" => strlen($fila['Q']) ? "{$fila['Q']}" : null,
                "tipoDocumentoOtros" => strlen($fila["R"]) ? trim($fila['R']) : null,
            ];

            try {
                self::preprocIngreso($ingreso, $nro_fila, $model);
                $persona_fisica['ingresos'][] = $ingreso;
            } catch (Exception $exception) {
                $errores[] = $exception->getMessage();
            }
        }

        // Egresos.
        foreach ($sheetDataEgresos as $nro_fila => $fila) {
            if ($nro_fila == 1) continue;
            if (trim($fila['A']) == '') break;

            $egreso = [
                "periodo" => $model->periodo,
                "tipo" => "{$fila['A']}",
                "tipoTexto" => strlen($fila['B']) ? $fila['B'] : null,
                "fecha" => (strlen($fila['C'])) ? $fila['C'] : null, //implode('-', array_reverse(explode('/', $fila['C']))) : null,
                "mes" => strlen($fila['D']) ? "{$fila['D']}" : null,
                "relacionadoTipoIdentificacion" => strlen($fila['E']) ? strtoupper($fila['E']) : null,
                "id" => $nro_fila - 1,
                "ruc" => $model->ruc,
                "relacionadoNumeroIdentificacion" => strlen($fila['F']) ? "{$fila['F']}" : null,
                "relacionadoNombres" => strlen($fila['G']) ? $fila['G'] : null,
                "timbradoNumero" => strlen($fila['H']) ? "{$fila['H']}" : null,
                "timbradoDocumento" => strlen($fila['I']) ? "{$fila['I']}" : null,
                "timbradoCondicion" => strlen($fila['J']) ? strtolower($fila['J']) : null,
                "egresoMontoTotal" => (int)$fila['K'],
                "cuentaNumero" => strlen($fila['L']) ? "{$fila['L']}" : "",
                "cuentaRazonSocial" => strlen($fila['M']) ? $fila['M'] : "",
                "tipoDocumentoOtros" => strlen($fila['N']) ? $fila['N'] : "",
                "numeroDocumento" => strlen($fila['O']) ? "{$fila['O']}" : "",
                "numeroDespacho" => strlen($fila['P']) ? "{$fila['P']}" : null,
                "tipoEgreso" => $fila['S'],
                "tipoEgresoTexto" => strlen($fila['T']) ? $fila['T'] : null,
                "subtipoEgreso" => $fila['U'],
                "subtipoEgresoTexto" => strlen($fila['V']) ? $fila['V'] : null,
                "empleadorIdentificacion" => strlen($fila['W']) ? "{$fila['W']}" : "",
            ];

            try {
                self::preprocEgreso($egreso, $nro_fila, $model);
                $persona_fisica['egresos'][] = $egreso;
            } catch (Exception $exception) {
                $errores[] = $exception->getMessage();
            }
        }

        // Familiares.
        $familiares = Yii::$app->request->post('ArandukaFamiliares');
        if (isset($familiares)) {
            foreach ($familiares as $familiar) {
                $_familiar = [
                    "identificacion" => "{$familiar['nro_identificacion']}",
                    "nombre" => $familiar['nombre_completo'],
                    "regimen" => $familiar['regimen_matrimonial'],
                    "regimenTexto" => ($familiar['regimen_matrimonial'] != '') ? $aranduka->regimenMatrimDesc[$familiar['regimen_matrimonial']] : '',
                    "vinculo" => $familiar['tipo_familia'],
                    "vinculoTexto" => ($familiar['tipo_familia'] != '') ? $aranduka->tipoVinculoDesc[$familiar['tipo_familia']] : "','",
                    "ruc" => $model->ruc,
                    "periodo" => $model->periodo,
                    'fechaNacimiento' => implode('-', array_reverse(explode('/', $familiar['fecha_nacimiento']))),
                ];

                if ($familiar['tipo_familia'] != Aranduka::CONYUGE) {
                    $_familiar['regimen'] = null;
                    $_familiar['regimenTexto'] = null;
                }
                if ($familiar['tipo_familia'] != Aranduka::HIJOS)
                    unset($_familiar['fechaNacimiento']);

                $persona_fisica['familiares'][] = $_familiar;
            }
        }

        if (!empty($errores)) {
            Yii::$app->session->set('aranduka_errors', $errores);
        }

        return json_encode($persona_fisica);
    }
}

class ArchivoAranduka extends ArchivosMultiple
{
    public $tipo_movimiento; // ingreso / egreso
    public $ruc; // ingreso / egreso
    public $periodo; // ingreso / egreso
    public $agregar_familiares;

    public function rules()
    {
        $parent_rules = parent::rules(); // TODO: Change the autogenerated stub

        return array_merge($parent_rules, [
            [['tipo_movimiento', 'ruc', 'periodo', 'agregar_familiares'], 'safe'],
            [['tipo_movimiento', 'ruc', 'periodo', 'agregar_familiares'], 'required']
        ]);
    }
}
