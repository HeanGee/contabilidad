<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\PeriodoContableSelector;
use backend\modules\contabilidad\models\search\ActivoBiologicoSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ActivoBiologicoController implements the CRUD actions for ActivoBiologico model.
 */
class ActivoBiologicoController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ActivoBiologico models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ActivoBiologicoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ActivoBiologico model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ActivoBiologico model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {

        try {
            ActivoBiologico::isCreable();
        } catch (\yii\base\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }

        $model = new ActivoBiologico();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            $transaccion = Yii::$app->db->beginTransaction();

            try {
                $model->stock_actual = $model->stock_inicial;
                $guardar_cerrar = $model->guardar_cerrar;

                if (!$model->save()) {
                    throw new \Exception($model->getErrorSummaryAsString());
                }

                $transaccion->commit();
                FlashMessageHelpsers::createSuccessMessage("Activo Biologico {$model->especie->nombre} creado correctamente.");

                if ($guardar_cerrar == 'si')
                    return $this->redirect(['index']);

                $especie_id = $model->especie_id;
                $clasificacion_id = $model->clasificacion_id;
                $model = new ActivoBiologico();
                $model->loadDefaultValues();
                $model->especie_id = $especie_id;
                $model->clasificacion_id = $clasificacion_id;
            } catch (\Exception $exception) {
                $transaccion->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ActivoBiologico model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws Exception
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        try {
            $model->isEditable();
        } catch (\yii\base\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaccion = Yii::$app->db->beginTransaction();

            try {
                if (!$model->validate()) {
                    throw new \Exception($model->getErrorSummaryAsString());
                }
                $model->recalcularPrecioUnitarioStockActuales();
                $model->save(false);

                $transaccion->commit();
                FlashMessageHelpsers::createSuccessMessage("Activo Biológico '{$model->especie->nombre}' modificado correctamente.");
                return $this->redirect(['index']);

            } catch (\Exception $exception) {
                $transaccion->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ActivoBiologico model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model = $this->findModel($id);

            $model->isDeleteable();

            $hijo = ActivoBiologico::find()->where(['activo_biologico_padre_id' => $id]);

            if ($hijo->exists()) {
                $hijo = $hijo->one();
                throw new Exception("Este A. Biológico está asociado como 'padre' del A. B. {$hijo->especie->nombre} del periodo {$hijo->periodoContable->anho}.");
            }

            $model->delete();

            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage("Activo Biológico {$model->especie->nombre} se ha eliminado correctamente.");
        } catch (\Exception $exception) {
            $transaction->rollBack();
//            throw $exception;
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ActivoBiologico model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActivoBiologico the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ActivoBiologico::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionTraspasarAlPeriodo($submit = false, $id = null)
    {
        if (!EmpresaPeriodoContable::existNextPeriodo()) {
            FlashMessageHelpsers::createInfoMessage('No existe periodo contable posterior al actual, al cual se pueda realizar el traspaso.');
            return $this->redirect(['index']);
        }

        $model = new PeriodoContableSelector();

        $skey = 'cont_activog_a_traspasar';
        $session = Yii::$app->session;

        if (!$session->has($skey) && isset($this->id)) {
            $session->set($skey, $id);
        }

        if ($submit != false && $model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            $p_act = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'));

            try {

                if ($model->periodo_contable_id == \Yii::$app->session->get('core_empresa_actual_pc')) {
                    throw new \Exception("No se puede traspasar al mismo periodo que el actual");
                }

                $actg_a_pasar = [];
                $id = $session->get($skey);

                if ($id == null) {
                    $actg_a_pasar = ActivoBiologico::findAll(['periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
                } else {
                    $actg_a_pasar[] = ActivoBiologico::findOne($id);
                }

                $traspaso_exists = false;
                $no_traspasados = [];
                foreach ($actg_a_pasar as $activog) {
                    if ($activog->isTraspasable($model->periodo_contable_id)) {
                        $activog_n = new ActivoBiologico();
                        $activog_n->copyFrom($activog);
                        $activog_n->periodo_contable_id = $model->periodo_contable_id;
                        $activog_n->activo_biologico_padre_id = $activog->id; // indica que se ha migrado desde tal activo
                        if (!$activog_n->save()) {
                            throw new \Exception("Error traspasando: {$activog_n->getErrorSummaryAsString()}");
                        }

                        $traspaso_exists = true;
                    } else {
                        $no_traspasados[] = $activog->id;
                    }
                }

                $trans->commit();

                if (!$traspaso_exists) {
                    $msg = isset($id) ?
                        "Este Activo ya se encuentra en el periodo {$model->periodoContable()->anho}." :
                        "Todos los activos actuales se encuentran migrados en el periodo {$model->periodoContable()->anho}.";
                    FlashMessageHelpsers::createInfoMessage($msg);
                } else {
                    FlashMessageHelpsers::createSuccessMessage("El traspaso del Periodo {$p_act->anho} al Periodo {$model->periodoContable()->anho} se ha efectuado correctamente");
                    if (sizeof($no_traspasados) > 0) {
                        $msg = implode(', ', $no_traspasados);
                        FlashMessageHelpsers::createInfoMessage("Los siguientes IDs son de activos que ya se encontraban migrados: {$msg}.");
                    }
                }

            } catch (\Exception $exception) {
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('modal/_form_traspaso_periodo', [
            'model' => $model
        ]);
    }

//    public function actionCalculateTotal()
//    {
//        $stock = Yii::$app->request->get('stock');
//        $precio_unitario = Yii::$app->request->get('precio_unitario');
//        $id = Yii::$app->request->get('id');
//
//        Yii::$app->response->format = Response::FORMAT_JSON;
//
//        if ($stock == "" || $precio_unitario == "") {
//            $activog_g = ActivoBiologico::findOne($id);
//            if (!isset($activog_g)) {
//                http_response_code(400);
//                echo "No se pudo auto-calcular. Precio Unitario y Stock Final no pueden estar vacíos.";
//                exit;
//            }
//
//            $prod = (float)$activog_g->dbFormattedPrecioUnitario * (int)$activog_g->dbFormattedStockFinal;
//
//            if ($prod == 0) {
//                http_response_code(400);
//                echo "Total no puede ser cero.";
//                exit;
//            }
//
//            return $prod;
//        }
//
//        $precio_unitario = str_replace(',', '.', str_replace('.', '', $precio_unitario));
//        $stock = str_replace('.', '', $stock);
//
//        $prod = (float)$precio_unitario * (int)$stock;
//
//        if ($prod == 0) {
//            http_response_code(400);
//            echo "Total no puede ser cero.";
//            exit;
//        }
//
//        return $prod;
//    }

    private function removeBlueprint()
    {

        if (array_key_exists('ActivoBiologicoStockManager', $_POST)) {
            if (!empty($_POST['ActivoBiologicoStockManager']['__new__'])) {
                unset($_POST['ActivoBiologicoStockManager']['__new__']);
                Yii::$app->request->setBodyParams($_POST);
            }
        }
    }

    public function actionComprarActBio($submit = false, $compra_id = null, $fila_id = null, $deducible = null, $campo_iva_activo = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (isset($fila_id)) {
            Yii::$app->session->set('cont_act_bio_compra_fila_id', $fila_id);
        }
        if (isset($deducible)) {
            Yii::$app->session->set('cont_act_bio_compra_deducible', $deducible);
        }
        if (isset($campo_iva_activo)) {
            Yii::$app->session->set('cont_act_bio_compra_campo_iva_activo', $campo_iva_activo);
        }
        if (isset($compra_id)) {
            Yii::$app->session->set('$compra_id', $compra_id);
        }

        /** @var ActivoBiologicoStockManager $allModels */
        $allModels = [];
        $session_key = 'cont_activo_bio_stock_manager';
        $compra_id = Yii::$app->session->get('$compra_id');

        if (Yii::$app->session->has($session_key)) {
            $sesionData = Yii::$app->session->get($session_key);
            $dataProvider = $sesionData['dataProvider'];
            $allModels = $dataProvider->allModels;
        } elseif (isset($compra_id)) {
            $allModels = ActivoBiologicoStockManager::find()->where(['factura_compra_id' => $compra_id])->all();
        }

        $this->removeBlueprint();
        $validate = true;

        if ($submit != false && Yii::$app->request->isPost) {
            if (sizeof($_POST['ActivoBiologicoStockManager']) == 0) {
                FlashMessageHelpsers::createWarningMessage("NO se a agregado ningún detalle.");
                $allModels = [];
            } else {

                $allModels = [];
                foreach ($_POST['ActivoBiologicoStockManager'] as $_ => $item) {

                    $post['ActivoBiologicoStockManager'] = $item;
                    $model = new ActivoBiologicoStockManager();
                    $model->loadDefaultValues();
                    $model->load($post);

                    if (!$model->datosValidos()) {
                        $validate = false;
                        FlashMessageHelpsers::createWarningMessage("Error en el formulario: {$model->getErrorSummaryAsString()}");
                    }

                    $allModels[] = $model;
                }
            }
        }

        Yii::$app->session->set('cont_activo_bio_stock_manager', ['compra_id' => $compra_id, 'dataProvider' => new ArrayDataProvider([
            'allModels' => $allModels, 'pagination' => false,
        ])]);

        $valor_para_plantilla = 0;
        if ($validate) {
            foreach ($allModels as $_model) {
                $valor_para_plantilla += (int)$_model->cantidad * (int)$_model->precio_unitario;
            }
        }

        Yii::$app->session->set('cont_act_bio_valor_para_plantilla', $valor_para_plantilla);

        return $this->renderAjax('modal/_form_stock_manager', [
            'allModels' => $allModels,
            'session_key' => $session_key,
            'fila_id' => Yii::$app->session->get('cont_act_bio_compra_fila_id'),
            'campo_iva_activo' => Yii::$app->session->get('cont_act_bio_compra_campo_iva_activo'),
        ]);
    }

    public function actionVenderActBio($submit = false, $venta_id = null, $fila_id = null, $deducible = null, $campo_iva_activo = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (isset($fila_id)) {
            Yii::$app->session->set('cont_act_bio_venta_fila_id', $fila_id);
        }

        if (isset($deducible)) {
            Yii::$app->session->set('cont_act_bio_venta_deducible', $deducible);
        }

        if (isset($campo_iva_activo)) {
            Yii::$app->session->set('cont_act_bio_venta_campo_iva_activo', $campo_iva_activo);
        }

        if (isset($venta_id)) {
            Yii::$app->session->set('$venta_id', $venta_id);
        }

        /** @var ActivoBiologicoStockManager $allModels */
        $allModels = [];
        $session_key = 'cont_activo_bio_stock_manager';
        $venta_id = Yii::$app->session->get('$venta_id');

        if (Yii::$app->session->has($session_key)) {
            $sessionData = Yii::$app->session->get($session_key);
            $dataProvider = $sessionData['dataProvider'];
            $allModels = $dataProvider->allModels;
        } elseif (isset($venta_id)) {
            $allModels = ActivoBiologicoStockManager::find()->where(['factura_venta_id' => $venta_id])->andWhere(['IS', 'nota_venta_id', null])->all();
        }

        $this->removeBlueprint();
        $validate = true;

        if ($submit != false && Yii::$app->request->isPost) {
            if (sizeof($_POST['ActivoBiologicoStockManager']) == 0) {
                FlashMessageHelpsers::createWarningMessage("NO se a agregado ningún detalle.");
                $allModels = [];
            } else {

                $allModels = [];
                foreach ($_POST['ActivoBiologicoStockManager'] as $_ => $item) {

                    $post['ActivoBiologicoStockManager'] = $item;
                    $model = new ActivoBiologicoStockManager();
                    $model->loadDefaultValues();
                    $model->load($post);

                    if (!$model->datosValidos()) {
                        $validate = false;
                        FlashMessageHelpsers::createWarningMessage("Error en el formulario: {$model->getErrorSummaryAsString()}");
                    }

                    $allModels[] = $model;
                }
            }
        }

        Yii::$app->session->set('cont_activo_bio_stock_manager', ['venta_id' => $venta_id, 'dataProvider' => new ArrayDataProvider([
            'allModels' => $allModels, 'pagination' => false,
        ])]);

        $valor_para_plantilla = 0;
        if ($validate) {
            foreach ($allModels as $_model) {
                $valor_para_plantilla += (int)$_model->cantidad * (float)$_model->precio_unitario;
            }
        }

        Yii::$app->session->set('cont_act_bio_valor_para_plantilla', $valor_para_plantilla);

        return $this->renderAjax('modal/_form_stock_manager', [
            'allModels' => $allModels,
            'session_key' => $session_key,
            'fila_id' => Yii::$app->session->get('cont_act_bio_venta_fila_id'), // para que no se queje, pero solo es usado en compra.
            'campo_iva_activo' => Yii::$app->session->get('cont_act_bio_venta_campo_iva_activo')
        ]);
    }

    public function actionGetValorParaPlantilla()
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->session->get('cont_act_bio_valor_para_plantilla');
    }

    /** ====================== para notas de credito ====================== */
    /**
     * @param bool $submit
     * @param null $nota_id
     * @param null $compra_id
     * @param null $campo_iva_activo
     * @return string
     */
    public function actionDevolverActBio($submit = false, $nota_id = null, $compra_id = null, $campo_iva_activo = null)
    {
        $allModels = [];
        $managersNota = [];
        $managersCompra = [];
        $sesion = Yii::$app->session;
        $session_key = 'cont_activo_bio_stock_manager';
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (isset($campo_iva_activo))
            $sesion->set('$campo_iva_activo', $campo_iva_activo);
        if (isset($nota_id))
            $sesion->set('$nota_id', $nota_id);
        if (isset($compra_id))
            $sesion->set('$compra_id', $compra_id);

        $nota_id = $sesion->get('$nota_id');
        $compra_id = $sesion->get('$compra_id');

        if ($sesion->has($session_key)) {
            // Si cambio de factura pero la sesion mantiene abios de otra factura, limpiar sesion.
            $sessionData = $sesion->get($session_key);
            $compra_id_asociada = $sessionData['compra_id'];
            if ($compra_id != $compra_id_asociada) {
                $sesion->remove($session_key);
            }
        }

        if (!$sesion->has($session_key)) {
            // managers de la nota.
            if (isset($nota_id) && $nota_id != '')
                $managersNota = ActivoBiologicoStockManager::findAll(['nota_compra_id' => $nota_id, 'factura_compra_id' => $compra_id]);
            // managers de la compra asociada.
            if (empty($allModels) && isset($compra_id) && $compra_id != '')
                $managersCompra = ActivoBiologicoStockManager::find()->where(['factura_compra_id' => $compra_id])->andWhere(['IS', 'nota_compra_id', null])->all();

            // agregar managers de compra que no estan en managers de nota.
            $agregar = [];
            foreach ($managersCompra as $managerCompra) {
                $hay = false;
                foreach ($managersNota as $managerNota) {
                    if ($managerNota->especie_id == $managerCompra->especie_id && $managerNota->clasificacion_id == $managerCompra->clasificacion_id) {
                        $hay = true;
                        break;
                    }
                }
                if (!$hay) {
                    $agregar[] = $managerCompra;
                }
            }
            $allModels = array_values(array_merge($managersNota, $agregar));
            // poner en la sesion
            $sesion->set($session_key, ['compra_id' => $compra_id, 'dataProvider' => new ArrayDataProvider([
                'allModels' => $allModels, 'pagination' => false])]);
        }

        $validate = true;
        $this->removeBlueprint();
        if ($submit != false && Yii::$app->request->isPost) {
            $allModels = [];
            if (sizeof($_POST['ActivoBiologicoStockManager']) == 0) {
                FlashMessageHelpsers::createWarningMessage("NO se a agregado ningún detalle.");
            } else {
                foreach ($_POST['ActivoBiologicoStockManager'] as $_ => $item) {
                    $post['ActivoBiologicoStockManager'] = $item;
                    $model = new ActivoBiologicoStockManager();
                    $model->loadDefaultValues();
                    $model->load($post);
                    Yii::warning($model->cantidad);

                    if (!$model->datosValidos()) {
                        $validate = false;
                        FlashMessageHelpsers::createWarningMessage("Error en el formulario: 
                        {$model->getErrorSummaryAsString()}");
                    }

                    $allModels[] = $model;
                }
            }
            $sesion->set($session_key, ['compra_id' => $compra_id, 'dataProvider' => new ArrayDataProvider([
                'allModels' => $allModels, 'pagination' => false])]);
        }

        $valor_para_plantilla = 0;
        if ($validate) {
            foreach ($allModels as $_model)
                $valor_para_plantilla += (int)$_model->cantidad * (int)$_model->precio_unitario;
        }
        $sesion->set('cont_act_bio_valor_para_plantilla', $valor_para_plantilla);

        return $this->renderAjax('modal/_form_stock_manager', [
            'session_key' => $session_key,
            'fila_id' => '', // para que no se queje, pero solo es usado en compra.
            'campo_iva_activo' => $sesion->get('$campo_iva_activo')
        ]);
    }

    /**
     * @param bool $submit
     * @param null $nota_id
     * @param null $compra_id
     * @param null $campo_iva_activo
     * @return string
     */
    public function actionRetornarActBio($submit = false, $nota_id = null, $venta_id = null, $campo_iva_activo = null)
    {
        $allModels = [];
        $managersNota = [];
        $managersVenta = [];
        $sesion = Yii::$app->session;
        $session_key = 'cont_activo_bio_stock_manager';
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (isset($campo_iva_activo))
            $sesion->set('$campo_iva_activo', $campo_iva_activo);
        if (isset($nota_id))
            $sesion->set('$nota_id', $nota_id);
        if (isset($venta_id))
            $sesion->set('$venta_id', $venta_id);

        $nota_id = $sesion->get('$nota_id');
        $venta_id = $sesion->get('$venta_id');

        if ($sesion->has($session_key)) {
            // Si cambio de factura pero la sesion mantiene abios de otra factura, limpiar sesion.
            $sessionData = $sesion->get($session_key);
            $venta_id_asociada = $sessionData['venta_id'];
            if ($venta_id != $venta_id_asociada) {
                $sesion->remove($session_key);
            }
        }

        if (!$sesion->has($session_key)) {
            // managers de la nota.
            if (isset($nota_id) && $nota_id != '')
                $managersNota = ActivoBiologicoStockManager::findAll(['nota_venta_id' => $nota_id, 'factura_venta_id' => $venta_id]);
            // managers de la compra asociada.
            if (empty($allModels) && isset($venta_id) && $venta_id != '')
                $managersVenta = ActivoBiologicoStockManager::find()->where(['factura_venta_id' => $venta_id])->andWhere(['IS', 'nota_venta_id', null])->all();

            // agregar managers de compra que no estan en managers de nota.
            $agregar = [];
            foreach ($managersVenta as $managerVenta) {
                $hay = false;
                foreach ($managersNota as $managerNota) {
                    if ($managerNota->especie_id == $managerVenta->especie_id && $managerNota->clasificacion_id == $managerVenta->clasificacion_id) {
                        $hay = true;
                        break;
                    }
                }
                if (!$hay) {
                    $agregar[] = $managerVenta;
                }
            }
            $allModels = array_values(array_merge($managersNota, $agregar));
            // poner en la sesion
            $sesion->set($session_key, ['venta_id' => $venta_id, 'dataProvider' => new ArrayDataProvider(['allModels' => $allModels, 'pagination' => false])]);
        }

        $validate = true;
        $this->removeBlueprint();
        if ($submit != false && Yii::$app->request->isPost) {
            $allModels = [];
            if (sizeof($_POST['ActivoBiologicoStockManager']) == 0) {
                FlashMessageHelpsers::createWarningMessage("NO se a agregado ningún detalle.");
            } else {
                foreach ($_POST['ActivoBiologicoStockManager'] as $_ => $item) {
                    $post['ActivoBiologicoStockManager'] = $item;
                    $model = new ActivoBiologicoStockManager();
                    $model->loadDefaultValues();
                    $model->load($post);
                    Yii::warning($model->cantidad);

                    if (!$model->datosValidos()) {
                        $validate = false;
                        FlashMessageHelpsers::createWarningMessage("Error en el formulario: 
                        {$model->getErrorSummaryAsString()}");
                    }

                    $allModels[] = $model;
                }
            }
            $sesion->set($session_key, ['venta_id' => $venta_id, 'dataProvider' => new ArrayDataProvider(['allModels' => $allModels, 'pagination' => false])]);
        }

        $valor_para_plantilla = 0;
        if ($validate) {
            foreach ($allModels as $_model)
                $valor_para_plantilla += (int)$_model->cantidad * (int)$_model->precio_unitario;
        }
        $sesion->set('cont_act_bio_valor_para_plantilla', $valor_para_plantilla);

        return $this->renderAjax('modal/_form_stock_manager', [
            'session_key' => $session_key,
            'fila_id' => '', // para que no se queje, pero solo es usado en compra.
            'campo_iva_activo' => $sesion->get('$campo_iva_activo')
        ]);
    }
}
