<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\search\TipoDocumentoSearch;
use backend\modules\contabilidad\models\TipoDocumento;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TipoDocumentoController implements the CRUD actions for TipoDocumento model.
 */
class TipoDocumentoController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TipoDocumento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TipoDocumentoSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single TipoDocumento model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TipoDocumento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TipoDocumento;
        $model->estado = 'activo';
        $model->para_importacion = 'no';
        $model->asiento_independiente = 'no';
        $model->solo_exento = 'no';

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                FlashMessageHelpsers::createSuccessMessage('Tipo de Documento creado exitosamente.');
                return $this->redirect(['index']);
            } else {
                FlashMessageHelpsers::createWarningMessage($model->getErrorSummaryAsString());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TipoDocumento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                FlashMessageHelpsers::createSuccessMessage('Tipo de Documento actualizado exitosamente.');
                return $this->redirect(['index']);
            } else {
                FlashMessageHelpsers::createWarningMessage($model->getErrorSummaryAsString());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TipoDocumento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model->isDeleteable();
            $model->delete();
            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage('Tipo de Documento eliminado exitosamente.');
        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the TipoDocumento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TipoDocumento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TipoDocumento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }

    public function actionGetTipodocParaNota()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $asociacion = Yii::$app->request->get('asociacion');
        $id = Yii::$app->request->get('id');
        $para = Yii::$app->request->get('para');
        $ajax = true;
        return TipoDocumento::getTipoDocumentoParaNota($asociacion, $id, $para, $ajax);
    }
}
