<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\Factura;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;
use yii2tech\csvgrid\CsvGrid;

/**
 * controller para generar reporte de compras en formato consumible por heckauka
 */
class ReporteCompraHechaukaController extends BaseController
{
    private $monto_total_rango = 0;
    private $timbrado = [];
    private $gra10 = [];
    private $gra5 = [];
    private $iva10 = [];
    private $iva5 = [];
    private $exent = [];


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param Compra|Venta|Factura $model
     * @param $campo
     * @return float
     */
    private function conversionAMonedaLocal($model, $campo)
    {
        $cotizacion = 1;
        Yii::info($model->moneda_id, "### id de moneda");
        if ($model->moneda_id != null && $model->moneda_id != 1) {
            $cotizacion = $model->cotizacion;
        }
        return round($campo * (float)$cotizacion);
    }

    /**
     * @param null $_pjax
     * @return $this|string
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function actionGetParams($_pjax = null)
    {
        $model = new ReporteCompraHechauka();

        if (!isset($_pjax) && Yii::$app->request->isGet) {
            $gridViewQuery = Compra::find()->where(['DATE_FORMAT(cont_factura_compra.fecha_emision, "%m/%Y")' => '']);
            $dataProvider = new ActiveDataProvider(['query' => $gridViewQuery, 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]]);
            $dataProvider->pagination = ['pageSize' => 20];
            Yii::$app->session->set('dataProvider', $dataProvider);
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $this->monto_total_rango = 0;

            if ($model->periodo == '') {
                FlashMessageHelpsers::createWarningMessage('Indique el período.');
                goto retorno;
            }

            $periodo = implode('-', array_reverse(explode('-', "01-" . str_replace("/", "-", $model->periodo))));
            $desde = date('Y-m-d', strtotime($periodo));
            $hasta = date('Y-m-t', strtotime($periodo));
            if ($desde && $hasta) {

                // Inspired in:
                // https://stackoverflow.com/questions/455261/how-to-create-virtual-column-using-mysql-select
                // https://stackoverflow.com/questions/849348/conditional-column-for-query-based-on-other-columns-in-mysql

                $tipoDocSetNotaCredito = Compra::getTipodocSetNotaCredito();
                $query = new Query();
                $query->select(['compra.id as id', 'compra.condicion', 'compra.total',
                    'entidad.ruc AS ruc', 'compra.nro_factura', 'entidad.digito_verificador as dv',
                    'entidad.razon_social', 'DATE_FORMAT(compra.fecha_emision, "%d/%m/%Y") as fecha',
                    'compra.cant_cuotas', 'compra.moneda_id', 'compra.cotizacion as cotizacion',
                    'compra.fecha_emision', 'compra.timbrado_detalle_id', 'compra.entidad_id',
                    'tim.nro_timbrado as nro_timbrado', "('compra') as operacion"])
                    ->from('cont_factura_compra as compra');
                $query->leftJoin('cont_entidad as entidad', 'entidad.id = compra.entidad_id');
                $query->leftJoin('cont_timbrado_detalle as timdet', 'timdet.id = compra.timbrado_detalle_id');
                $query->leftJoin('cont_timbrado as tim', 'tim.id = timdet.timbrado_id');
                $query->leftJoin('cont_tipo_documento as tipodoc', 'tipodoc.id = compra.tipo_documento_id');
                $query->leftJoin('cont_tipo_documento_set as tipodocset', 'tipodocset.id = tipodoc.tipo_documento_set_id');
                $query->where(['compra.empresa_id' => Yii::$app->session->get('core_empresa_actual')]);
                $query->andWhere(['compra.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
                $query->andWhere(['IS NOT', 'timbrado_detalle_id', null]);
                $query->andFilterWhere(['AND', ['!=', 'tipodoc.tipo_documento_set_id', $tipoDocSetNotaCredito->id], ['IS', 'compra.factura_compra_id', null]]); // solo facturas, nada de notas
                $query->andFilterWhere(['between', 'fecha_emision', $desde, $hasta]);

                #remover facturas que usan plantilla marcada como no deducible.
                #18 de julio 19: quieren que no aparezca facturas con plantilla GASTOS NO DEDUCIBLES
                # pero es mas generico preguntar por el atributo deducible de la plantilla.
                $id_facturas = ArrayHelper::map($query->all(), 'id', 'id');
                foreach ($id_facturas as $key => $id) {
                    foreach (CompraIvaCuentaUsada::findAll(['factura_compra_id' => $id]) as $ivaCta) {
                        if (isset($ivaCta->plantilla) && $ivaCta->plantilla->deducible == 'no')
                            unset($id_facturas[$key]);
                    }
                }

                #restringir los ids de facturas
                $query->andFilterWhere(['IN', 'compra.id', $id_facturas]);

                #unir las ventas credito
                $empresa_id = Yii::$app->session->get('core_empresa_actual');
                $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
                $tipoDocSetNotaCredito = Venta::getTipodocSetNotaCredito();
                $sql = "
                SELECT venta.id as id, venta.condicion as condicion, venta.total as total, entidad.ruc as ruc,
                CONCAT((venta.prefijo), ('-'), (venta.nro_factura)) AS nro_factura, entidad.digito_verificador as dv,
                entidad.razon_social as razon_social, DATE_FORMAT(venta.fecha_emision, \"%d/%m/%Y\") as fecha,
                venta.cant_cuotas as cant_cuotas, venta.moneda_id as moneda_id, venta.valor_moneda as cotizacion,
                venta.fecha_emision as fecha_emision, venta.timbrado_detalle_id as timbrado_detalle_id,
                venta.entidad_id as entidad_id, tim.nro_timbrado as nro_timbrado,
                ('venta') as operacion
                from cont_factura_venta as venta
                left join cont_timbrado_detalle as timdet on timdet.id = venta.timbrado_detalle_id
                left join cont_timbrado as tim on tim.id = timdet.timbrado_id
                left join cont_entidad as entidad on venta.entidad_id = entidad.id
                left join cont_tipo_documento ctd on venta.tipo_documento_id = ctd.id
                left join cont_tipo_documento_set ctds on ctd.tipo_documento_set_id = ctds.id
                where venta.empresa_id = {$empresa_id} and venta.periodo_contable_id = {$periodo_id}
                and venta.fecha_emision between '{$desde}' and '{$hasta}'
                and (venta.factura_venta_id is not null or ctd.tipo_documento_set_id = {$tipoDocSetNotaCredito->id})
                and venta.estado = 'vigente' /* correo deisy 19 feb 19 a las 13:30 */
                ";
                $query->union($sql);

            } else {
                FlashMessageHelpsers::createWarningMessage('Rango de fecha incorrecto.');
                goto retorno;
            }

            $count = count($query->all());
            if ($count == 0) {
                FlashMessageHelpsers::createWarningMessage('Sin datos en el periodo.');
                goto retorno;
            }

            $allModels = [];
            $command = $query->createCommand();
            foreach ($command->queryAll() as $fila) {
                $_model = new Factura();
                foreach ($fila as $attribute => $value) {
                    if (array_key_exists($attribute, $_model->attributes)) {
                        $_model->$attribute = $value;
                    }
                }
                $allModels[] = $_model;

                $gravs10 = 0.0;
                $gravs5 = 0.0;
                $imps10 = 0.0;
                $imps5 = 0.0;
                $excs = 0.0;
                if ($_model->operacion == 'compra') {
                    $query = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $_model->id]);
                } else {
                    $query = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $_model->id]);
                }
                /** @var VentaIvaCuentaUsada|CompraIvaCuentaUsada $q */
                foreach ($query as $q) {
                    if ($q->iva_cta_id != null) {
                        if ($q->ivaCta->iva->porcentaje == 10) {
                            $gravs10 += (float)$q->monto / (1.10);
                            $imps10 += (float)$q->monto - ((float)$q->monto / (1.10));
                        } elseif ($q->ivaCta->iva->porcentaje == 5) {
                            $gravs5 += (float)$q->monto / (1.05);
                            $imps5 += (float)$q->monto - ((float)$q->monto / (1.05));
                        }
                    } else {
                        $excs += (float)$q->monto;
                    }
                }
                $gravs10 = $this->conversionAMonedaLocal($_model, $gravs10);
                $this->monto_total_rango += $gravs10;
                $this->gra10[$_model->id] = $gravs10;
                $imps10 = $this->conversionAMonedaLocal($_model, $imps10);
                $this->iva10[$_model->id] = $imps10;
                $gravs5 = $this->conversionAMonedaLocal($_model, $gravs5);
                $this->monto_total_rango += $gravs5;
                $this->gra5[$_model->id] = $gravs5;
                $imps5 = $this->conversionAMonedaLocal($_model, $imps5);
                $this->iva5[$_model->id] = $imps5;
                $excs = $this->conversionAMonedaLocal($_model, $excs);
                $this->monto_total_rango += $excs;
                $this->exent[$_model->id] = $excs;
            }

            $exporter = new CsvGrid([
                'csvFileConfig' => [
                    'cellDelimiter' => ";",
                    'rowDelimiter' => "\n",
                    'enclosure' => '',
                ],
                'showHeader' => false,
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => $allModels,
                    'pagination' => false,
                ]),
                'columns' => [
                    [
                        'label' => 'TipoRegistro',
                        'value' => function () {
                            return 2;
                        },
                    ],
                    "ruc",
                    "dv",
                    "razon_social",
                    'nro_timbrado',
                    [
                        'label' => 'tipo_documento',
                        'value' => function ($model) {
                            if ($model->operacion == 'venta') return 3; // correo deisy 19 feb 19 a las 13:30
                            return 1;
                        },
                    ],
                    [
                        'label' => 'nro_documento',
                        'value' => 'nro_factura',
                    ],
                    "fecha",
                    [
                        'label' => 'Gravada 10%',
                        'value' => function ($model) {
                            return $this->gra10[$model->id];
                        },
                    ],
                    [
                        'label' => 'IVA 10%',
                        'value' => function ($model) {
                            return $this->iva10[$model->id];
                        },
                    ],
                    [
                        'label' => 'Gravada 5%',
                        'value' => function ($model) {
                            return $this->gra5[$model->id];
                        },
                    ],
                    [
                        'label' => 'IVA 5%',
                        'value' => function ($model) {
                            return $this->iva5[$model->id];
                        },
                    ],
                    [
                        'label' => 'EXENTA',
                        'value' => function ($model) {
                            return $this->exent[$model->id];
                        },
                    ],
                    [
                        'label' => 'tipo_operacion',
                        'value' => function () {
                            return 0;
                        },
                    ],
                    [
                        'label' => 'Condición',
                        'value' => function ($model) {
                            return $condicion = $model->condicion == 'contado' ? 1 : 2;
                        },
                    ],
                    [
                        'label' => 'Cuotas',
                        'value' => function ($model) {
                            return $cuotas = $model->condicion == 'credito' ? $model->cant_cuotas : 0;
                        },
                    ]
                ]

            ]);

            $carpeta = Yii::$app->basePath . '/web/uploads/';
            if (!is_dir($carpeta)) {
                if (!@mkdir($carpeta, 0777, true)) {
                    throw new \Exception('No se puede crear carpeta.');
                }
            }
            $exporter->export()->saveAs($carpeta . 'tmp/tmp_compras');
            $cantidad_registros = $count;
            Yii::info($cantidad_registros . "#" . $this->monto_total_rango, "### total general");
            $this->createCabecera($periodo, $cantidad_registros, $this->monto_total_rango)->saveAs($carpeta . 'tmp/tmp_cabecera_compras');
            $fp1 = fopen($carpeta . "tmp/tmp_cabecera_compras", 'a+');
            $file2 = file_get_contents($carpeta . "tmp/tmp_compras");
            fwrite($fp1, "\n" . $file2);
            if (file_exists($carpeta . "tmp/tmp_cabecera_compras")) {
                $file = Yii::$app->response->sendFile($carpeta . "tmp/tmp_cabecera_compras", 'reporte_compras_' . $periodo[1] . $periodo[0] . '.csv');
                FileHelper::removeDirectory($carpeta . 'tmp');
                return $file;
            }
        }

        retorno:;
        return $this->render('reporte-compra-hechauka', [
            'model' => $model,
        ]);
    }

    public function actionLoadCompras()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;

        $periodo = $request->get('periodo');
        $query = Compra::find();
        $query->where([
            'DATE_FORMAT(fecha_emision, "%m/%Y")' => $periodo,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $query->andWhere(['IS NOT', 'timbrado_detalle_id', null]);
//        $query->orderBy(['fecha_emision' => SORT_DESC]);
        $session->remove('dataProvider');
        $session->set('dataProvider', new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => false
        ]));
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /**
     * @param $periodo
     * @param $cantidad_registros
     * @param $monto_total
     * @return \yii2tech\csvgrid\ExportResult
     * @throws \yii\base\InvalidConfigException
     */
    function createCabecera($periodo, $cantidad_registros, $monto_total)
    {
        $empresa = Empresa::findOne(\Yii::$app->session->get('core_empresa_actual'));
        $slices = explode('-', $periodo);
        $exporter = new CsvGrid([
            'csvFileConfig' => [
                'rowDelimiter' => "\n",
                'enclosure' => '',
                'cellDelimiter' => ";",
            ],
            'showHeader' => false,
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [
                    [
                        'tipo' => '1',
                        'periodo' => $slices[0] . $slices[1],
                        'tipoRegistro' => '1',
                        'obligacion' => '911',
                        'formulario' => '211',
                        'ruc' => $empresa->ruc,
                        'dv' => $empresa->digito_verificador,
                        'nombre' => $empresa->razon_social,
                        'ruc_representante_legal' => $empresa->cedula,
                        'dv_representante_legal' => $empresa->dvCedula,
                        'nombre_representante_legal' => "$empresa->primer_apellido $empresa->segundo_apellido, $empresa->primer_nombre $empresa->segundo_nombre",
                        'cantidad_de_registros' => $cantidad_registros,
                        'sumatoria_del_monto_reportado' => $monto_total,
                        'exportador' => 'NO',
                        'version' => '2'
                    ],
                ],
            ]),
            'columns' => [
                [
                    'attribute' => 'tipo',
                ],
                [
                    'attribute' => 'periodo'
                ],
                [
                    'attribute' => 'tipoRegistro'
                ],
                [
                    'attribute' => 'obligacion'
                ],
                [
                    'attribute' => 'formulario'
                ],
                [
                    'attribute' => 'ruc'
                ],
                [
                    'attribute' => 'dv'
                ],
                [
                    'attribute' => 'nombre'
                ],
                [
                    'attribute' => 'ruc_representante_legal'
                ],
                [
                    'attribute' => 'dv_representante_legal'
                ],
                [
                    'attribute' => 'nombre_representante_legal'
                ],
                [
                    'attribute' => 'cantidad_de_registros'
                ],
                [
                    'attribute' => 'sumatoria_del_monto_reportado'
                ],
                [
                    'attribute' => 'exportador'
                ],
                [
                    'attribute' => 'version'
                ]

            ],
        ]);
        return $exporter->export();
    }

}

/**
 * @property string $periodo
 */
class ReporteCompraHechauka extends Model
{
    public $periodo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periodo'], 'safe'],
            [['periodo'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'periodo' => 'Corresponde al período fiscal que se informa.',
        ];
    }
}

///**
// * @property string $id
// * @property string $condicion
// * @property string $total
// * @property string $ruc
// * @property string $nro_factura
// * @property string $dv
// * @property string $razon_social
// * @property string $fecha
// * @property string $cant_cuotas
// * @property string $moneda_id
// * @property string $cotizacion
// * @property string $fecha_emision
// * @property string $timbrado_detalle_id
// * @property string $operacion
// */
//class Factura extends Model
//{
//    public $id;
//    public $condicion;
//    public $total;
//    public $ruc;
//    public $nro_factura;
//    public $dv;
//    public $razon_social;
//    public $fecha;
//    public $cant_cuotas;
//    public $moneda_id;
//    public $cotizacion;
//    public $fecha_emision;
//    public $timbrado_detalle_id;
//    public $operacion;
//
//    public function rules()
//    {
//        $rules = [
//            [['id', 'condicion', 'total', 'ruc', 'nro_factura', 'dv', 'razon_social', 'fecha', 'cant_cuotas', 'moneda_id',
//                'cotizacion', 'fecha_emision', 'timbrado_detalle_id', 'operacion'], 'safe'],
//        ];
//
//        return $rules;
//    }
//
//    public function getAttributes($names = null, $except = [])
//    {
//        return parent::getAttributes($names, $except); // TODO: Change the autogenerated stub
//    }
//}