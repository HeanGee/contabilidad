<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\search\ActivoBiologicoCierreInventarioSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ActivoBiologicoCierreInventarioController implements the CRUD actions for ActivoBiologicoCierreInventario model.
 */
class ActivoBiologicoCierreInventarioController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ActivoBiologicoCierreInventario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ActivoBiologicoCierreInventarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $session = Yii::$app->session;

        $session->remove('cont_actbio_cierre');
        $session->remove('cont_asientodetalle_abiocierre');

        return $this->render('index', [
//            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ActivoBiologicoCierreInventario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        /** @var ActivoBiologicoCierreInventario[] $detalles */
        $detalles = [];
        $query = ActivoBiologicoCierreInventario::find()
            ->where([
                'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        foreach ($query->all() as $_ => $inventarioCierre) {
            $detalles[] = $inventarioCierre;
        }

        // al cambiar de empresa y/o periodo, el formulario sigue mostrándose pero sin datos
        // entonces redirigir al index.
        if (empty($detalles)) {
            FlashMessageHelpsers::createWarningMessage("No existe ningún registro de cierre de inventario de
             activos biológicos para la empresa y periodo actuales.");
            return $this->redirect(['index']);
        }

        return $this->render('view', ['detalles' => $detalles]);
    }

    /**
     * @param $models ActivoBiologicoCierreInventario[]
     * @param $postData array
     */
    private static function loadModel($models, $postData)
    {
        foreach ($postData as $tabla => $filas) {
            foreach ($filas as $_ => $atributos) {
                $especieClasificacion = [
                    'especie' => $atributos['especie_id'],
                    'clasificacion' => $atributos['clasificacion_id'],
                ];
                $index = self::getModel($models, $especieClasificacion); // se espera que nunca devuelva null
                $dataLoader = [
                    'ActivoBiologicoCierreInventario' => $atributos,
                ];
                $models[$index]->load($dataLoader);
            }
        }
    }

    /**
     * @param $models ActivoBiologicoCierreInventario[]
     * @param $especieClasificacion array
     * @return ActivoBiologicoCierreInventario|null
     */
    private static function getModel($models, $especieClasificacion)
    {
        foreach ($models as $index => $model) {
//            Yii::warning($model->especie_id == $especieClasificacion['especie'] && $model->clasificacion_id == $especieClasificacion['clasificacion']);
            if ($model->especie_id == $especieClasificacion['especie'] && $model->clasificacion_id == $especieClasificacion['clasificacion'])
                return $index;
        }
        return null;
    }

    /**
     * Creates a new ActivoBiologicoCierreInventario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $_pjax
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate($_pjax = null)
    {
        try {
            ActivoBiologicoCierreInventario::isCreable();
        } catch (Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }

        $model = new ActivoBiologicoCierreInventario();
        $model->loadDefaultValues();
        $session = Yii::$app->session;
        $session_key = 'cont_actbio_cierre';
        $show_tab = 'datos-entrada';
        $session = Yii::$app->session;
        $session_key = 'cont_asientodetalle_abiocierre';

        /** @var ActivoBiologicoCierreInventario[] $detalles */
        $detalles = [];
        $query = ActivoBiologico::find()
            ->where([
                'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        foreach ($query->all() as $_ => $abio) {
            $_model = new ActivoBiologicoCierreInventario();
            $_model->loadDefaultValues();
            $_model->especie_id = $abio->especie_id;
            $_model->clasificacion_id = $abio->clasificacion_id;
            $detalles[] = $_model;
        }

        if ($_pjax == null && Yii::$app->request->isGet) {
            $session->set($session_key, []);
        }

        if ($_pjax == null && Yii::$app->request->isPost) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $postData = [
                    'procreo' => Yii::$app->request->post('ActivoBiologicoCierreInventarioProcreo', null),
                    'venta' => Yii::$app->request->post('ActivoBiologicoCierreInventarioCostoVenta', null),
                    'mortandad' => Yii::$app->request->post('ActivoBiologicoCierreInventarioMortandad', null),
                    'consumo' => Yii::$app->request->post('ActivoBiologicoCierreInventarioConsumo', null),
                    'datos' => Yii::$app->request->post('ActivoBiologicoCierreInventarioDatos', null),
                    'calculados' => Yii::$app->request->post('ActivoBiologicoCierreInventarioDatosCalculados', null),
                ];

                self::loadModel($detalles, $postData);
                if (!ActivoBiologicoCierreInventario::validateMultiple($detalles)) {
                    $errors = [];
                    foreach ($detalles as $detalle) {
                        if ($detalle->hasErrors()) {
                            Yii::warning($detalle->especie);
                            Yii::warning($detalle->clasificacion->nombre);
                            $errors[] = "Para Especie {$detalle->especie->nombre} y Clasificación {$detalle->clasificacion->nombre}: {$detalle->getErrorSummaryAsString()}";
                        }
                    }
                    $msg = implode(', ', $errors);
                    throw new Exception("Error validando formulario: {$msg}");
                }

                foreach ($detalles as $detalle) {
                    $abio = $detalle->activoBiologico;
                    $abio->stock_final = $detalle->stock_final;
                    if (!$abio->save()) {
                        throw new Exception("Error actualizando stock final del A. Biológico {$abio->especie->nombre}/{$abio->clasificacion->nombre}: {$abio->getErrorSummaryAsString()}");
                    }
                    $abio->refresh();
                    if (!$detalle->save(false)) {
                        throw new Exception("Error interno guardando el inventario.");
                    }
                    $detalle->refresh();
                }

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("El Cierre se ha creado correctamente.");
                return $this->redirect(['index']);
            } catch (Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => null,
            'show_tab' => $show_tab,
            'detalles' => $detalles,
        ]);
    }

    /**
     * Updates an existing ActivoBiologicoCierreInventario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param null $_pjax
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id, $_pjax = null)
    {
        try {
            ActivoBiologicoCierreInventario::isEditable();
        } catch (Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }

        $show_tab = 'datos-entrada';
        $session = Yii::$app->session;
        $session_key = 'cont_asientodetalle_abiocierre';

        /** @var ActivoBiologicoCierreInventario[] $detalles */
        $detalles = [];
        $query = ActivoBiologicoCierreInventario::find()
            ->where([
                'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        foreach ($query->all() as $_ => $inventarioCierre) {
            $detalles[] = $inventarioCierre;
        }

        // al cambiar de empresa y/o periodo, el formulario sigue mostrándose pero sin datos
        // entonces redirigir al index.
        if (empty($detalles)) {
            FlashMessageHelpsers::createWarningMessage("No existe ningún registro de cierre de inventario de
             activos biológicos para la empresa y periodo actuales.");
            return $this->redirect(['index']);
        }

        if ($_pjax == null && Yii::$app->request->isGet) {
            $session->set($session_key, []);
        }

        if ($_pjax == null && Yii::$app->request->isPost) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $postData = [
                    'procreo' => Yii::$app->request->post('ActivoBiologicoCierreInventarioProcreo', null),
                    'venta' => Yii::$app->request->post('ActivoBiologicoCierreInventarioCostoVenta', null),
                    'mortandad' => Yii::$app->request->post('ActivoBiologicoCierreInventarioMortandad', null),
                    'consumo' => Yii::$app->request->post('ActivoBiologicoCierreInventarioConsumo', null),
                    'datos' => Yii::$app->request->post('ActivoBiologicoCierreInventarioDatos', null),
                    'calculados' => Yii::$app->request->post('ActivoBiologicoCierreInventarioDatosCalculados', null),
                ];

                self::loadModel($detalles, $postData);
                if (!ActivoBiologicoCierreInventario::validateMultiple($detalles)) {
                    $errors = [];
                    foreach ($detalles as $detalle) {
                        if ($detalle->hasErrors()) {
//                            Yii::warning($detalle->especie);
//                            Yii::warning($detalle->clasificacion->nombre);
                            $errors[] = "Para Especie {$detalle->especie->nombre} y Clasificación {$detalle->clasificacion->nombre}: {$detalle->getErrorSummaryAsString()}";
                        }
                    }
                    $msg = implode(', ', $errors);
                    throw new Exception("Error validando formulario: {$msg}");
                }

                foreach ($detalles as $detalle) {
                    $abio = $detalle->activoBiologico;
                    $abio->stock_final = $detalle->stock_final;
                    if (!$abio->save()) {
                        throw new Exception("Error actualizando stock final del A. Biológico {$abio->especie->nombre}/{$abio->clasificacion->nombre}: {$abio->getErrorSummaryAsString()}");
                    }
                    $abio->refresh();
                    if (!$detalle->save(false)) {
                        throw new Exception("Error interno guardando el inventario.");
                    }
                    $detalle->refresh();
                }

                // TODO: Validar, guardar.

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("El Cierre se ha modificado correctamente.");
                return $this->redirect(['index']);
            } catch (Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('update', [
            'model' => null,
            'show_tab' => $show_tab,
            'detalles' => $detalles,
        ]);
    }

    /**
     * Deletes an existing ActivoBiologicoCierreInventario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $trasaction = Yii::$app->db->beginTransaction();
        try {
            ActivoBiologicoCierreInventario::isDeleteable();

            $models = ActivoBiologicoCierreInventario::getInventarioCurrentPeriodo();
            foreach ($models as $model) {
                $activoBiologico = $model->restoreABioStockFinal();
                if (!$activoBiologico->save()) {
                    throw new Exception("Error restaurando stock final del A. Bio
                     {$activoBiologico->especie->nombre}/{$activoBiologico->clasificacion->nombre}: {$activoBiologico->getErrorSummaryAsString()}");
                }
                $activoBiologico->refresh();
                if (!$model->delete()) {
                    throw new Exception("Error borrando inventario: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();
            }

            $trasaction->commit();
            FlashMessageHelpsers::createSuccessMessage("El cierre del {$model->periodoContable->anho} se ha eliminado correctamente");
            return $this->redirect(['index']);
        } catch (Exception $exception) {
            $trasaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ActivoBiologicoCierreInventario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActivoBiologicoCierreInventario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ActivoBiologicoCierreInventario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionSimularAsiento()
    {
        $session = Yii::$app->session;
        $session_key = 'cont_asientodetalle_abiocierre';
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;

            ActivoBiologicoCierreInventario::isCuentasForAsiento();

            $request = Yii::$app->request;
            $asientos = [
                'procreo' => [
                    'debe' => 0,
                    'haber' => 0,
                    'allModels' => [],
                ],
                'venta' => [
                    'debe' => 0,
                    'haber' => 0,
                    'allModels' => [],
                ],
                'consumo' => [
                    'debe' => 0,
                    'haber' => 0,
                    'allModels' => [],
                ],
                'mortandad' => [
                    'debe' => 0,
                    'haber' => 0,
                    'allModels' => [],
                ],
                'valorizacion' => [
                    'debe' => 0,
                    'haber' => 0,
                    'allModels' => [],
                ],
            ];

            // TODO:
            $simulationData = [];
            $simulationData['procreo'] = $request->get('procreo');
            $simulationData['venta'] = $request->get('venta');
            $simulationData['consumo'] = $request->get('consumo');
            $simulationData['mortandad'] = $request->get('mortandad');
            $simulationData['datosIniciales'] = $request->get('datosIniciales');

            foreach ($simulationData as $tablaNombre => $tabla) {
                $generarAsientos = self::getFnctNameByTable($tablaNombre);
                if (isset($generarAsientos)) {
                    if ($tablaNombre == 'datosIniciales') {
                        Yii::$app->session->remove('debug');
                    }
                    self::$generarAsientos($asientos, $tabla);
                    $session->set($session_key, $asientos);
                } else {
                    throw new Exception("Error interno mapeando funciones.");
                }
            }
        } catch (Exception $exception) {
            $session->set($session_key, []);
            FlashMessageHelpsers::createWarningMessage("Error desde simularAsiento(): {$exception->getMessage()}");
            return ['error' => true, 'message' => $exception];
        }

        return ['error' => false];
    }

    private static function getFnctNameByTable($tablaNombre)
    {
        $map = [
            'procreo' => "asientoProcreo",
            'venta' => "asientoVenta",
            'consumo' => "asientoConsumo",
            'mortandad' => "asientoMortandad",
            'datosIniciales' => "asientoValorizacion",
        ];

        if (array_key_exists($tablaNombre, $map))
            return $map[$tablaNombre];
        return null;
    }


    /**
     * @param $asientos
     * @param $datos
     * @throws Exception
     */
    private static function asientoProcreo(&$asientos, $datos)
    {
        Yii::warning('entro en procreo');
        $tablaNombre = 'procreo';
        $cantidad = 0;
        $monto = 0;
        foreach ($datos as $fila => $campos) {
            $cantidad += (int)$campos['procreo_cantidad'];
            $cantidadByAbio = (int)$campos['procreo_cantidad'];
            $monto += $cantidadByAbio * (int)$campos['precio_foro']; // para evitar trampa, se recalcula.
        }
        $debe = $monto;
        $haber = $monto;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaGanado()->id;
        $detalle->monto_debe = $monto;
        $detalle->monto_haber = 0;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaProcreo()->id;
        $detalle->monto_debe = 0;
        $detalle->monto_haber = $monto;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;
    }

    /**
     * @param $asientos
     * @param $datos
     * @throws Exception
     */
    private static function asientoVenta(&$asientos, $datos)
    {
        Yii::warning('entro en venta');
        $tablaNombre = 'venta';
        $cantidad_gd = 0;
        $cantidad_gnd = 0;
        $cantidad = 0;
        $monto_gd = 0;
        $monto_gnd = 0;
        $monto = 0;
        foreach ($datos as $fila => $campos) {
            $cantidad_gd = (int)$campos['venta_cantidad_gd'];
            $cantidad_gnd = (int)$campos['venta_cantidad_gnd'];
            $cantidad = (int)$campos['venta_cantidad'];
            $monto_gd += $cantidad_gd * (int)$campos['precio_foro']; // para evitar trampa, se recalcula.
            $monto_gnd += $cantidad_gd * (int)$campos['precio_foro']; // para evitar trampa, se recalcula.
            $monto += $cantidad * (int)$campos['precio_foro'];
        }
        $debe = $monto;
        $haber = $monto;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaCostoVentaGd()->id;
        $detalle->monto_debe = $monto_gd;
        $detalle->monto_haber = 0;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaCostoVentaGnd()->id;
        $detalle->monto_debe = $monto_gnd;
        $detalle->monto_haber = 0;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaGanado()->id;
        $detalle->monto_debe = 0;
        $detalle->monto_haber = $monto;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;
    }

    private static function asientoConsumo(&$asientos, $datos)
    {
        Yii::warning("Logro consumo");
        $tablaNombre = 'consumo';
        $cantidad_gd = 0;
        $cantidad_gnd = 0;
        $cantidad = 0;
        $monto_gd = 0;
        $monto_gnd = 0;
        foreach ($datos as $fila => $campos) {
            $cantidad_gd = (int)$campos['consumo_cantidad_gd'];
            $cantidad_gnd = (int)$campos['consumo_cantidad_gnd'];
            $monto_gd += $cantidad_gd * (int)$campos['precio_foro']; // para evitar trampa, se recalcula.
            $monto_gnd += $cantidad_gnd * (int)$campos['precio_foro']; // para evitar trampa, se recalcula.
        }
        $monto = $monto_gd + $monto_gnd;
        Yii::warning("consumo monto: {$monto}");
        $debe = $monto;
        $haber = $monto;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaConsumoGd()->id;
        $detalle->monto_debe = $monto_gd;
        $detalle->monto_haber = 0;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaConsumoGnd()->id;
        $detalle->monto_debe = $monto_gnd;
        $detalle->monto_haber = 0;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaGanado()->id;
        $detalle->monto_debe = 0;
        $detalle->monto_haber = $monto;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;
    }

    /**
     * @param $asientos
     * @param $datos
     * @throws Exception
     */
    private static function asientoMortandad(&$asientos, $datos)
    {
        Yii::warning("Logro mortandad");
        $tablaNombre = 'mortandad';
        $cantidad_gd = 0;
        $cantidad_gnd = 0;
        $monto_gd = 0;
        $monto_gnd = 0;
        foreach ($datos as $fila => $campos) {
            $cantidad_gd = (int)$campos['mortandad_cantidad_gd'];
            $cantidad_gnd = (int)$campos['mortandad_cantidad_gnd'];
            $monto_gd += $cantidad_gd * (int)$campos['precio_foro']; // para evitar trampa, se recalcula.
            $monto_gnd += $cantidad_gnd * (int)$campos['precio_foro']; // para evitar trampa, se recalcula.
        }
        $monto = $monto_gd + $monto_gnd;
        $debe = $monto;
        $haber = $monto;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaMortandadGd()->id;
        $detalle->monto_debe = $monto_gd;
        $detalle->monto_haber = 0;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaMortandadGnd()->id;
        $detalle->monto_debe = $monto_gnd;
        $detalle->monto_haber = 0;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaGanado()->id;
        $detalle->monto_debe = 0;
        $detalle->monto_haber = $monto;
        $asientos[$tablaNombre]['allModels'][] = $detalle;
        $asientos[$tablaNombre]['debe'] = $debe;
        $asientos[$tablaNombre]['haber'] = $haber;
    }

    /**
     * @param $asientos
     * @param $datos
     * @throws Exception
     */
    private static function asientoValorizacion(&$asientos, $datos)
    {
        Yii::warning("Logro valorizacion");
        Yii::$app->session->remove('debug2');

        $forAsiento = 'valorizacion';
        $monto = 0;
        foreach ($datos as $fila => $campos) {
            $total = (int)$campos['total'];
            $monto += $total;
        }

        // calcular variacion en terminos monetarios
        $deltaMonto = 0;
        foreach ($asientos as $tablaNombre => $data) {
            if (in_array($tablaNombre, ['venta', 'consumo', 'mortandad'])) {
                $deltaMonto -= (int)$data['debe']; // o haber.. igual es el mismo monto, en casos ideales.
            } else {
                $deltaMonto += (int)$data['debe']; // o haber.. igual es el mismo monto, en casos ideales.
            }
        }
        $a = $monto;
        $b = $deltaMonto;
        $monto += $deltaMonto;
        $c = $monto;
        $debe = $monto;
        $haber = $monto;
        Yii::warning("monto inicial: {$a}, monto delta: {$b}, monto final: {$c}");

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaGanado()->id;
        $detalle->monto_debe = $monto;
        $detalle->monto_haber = 0;
        $asientos[$forAsiento]['allModels'][] = $detalle;
        $asientos[$forAsiento]['debe'] = $debe;
        $asientos[$forAsiento]['haber'] = $haber;

        $detalle = new AsientoDetalle();
        $detalle->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $detalle->cuenta_id = ActivoBiologicoCierreInventario::getCuentaValorizacionHacienda()->id;
        $detalle->monto_debe = 0;
        $detalle->monto_haber = $monto;
        $asientos[$forAsiento]['allModels'][] = $detalle;
        $asientos[$forAsiento]['debe'] = $debe;
        $asientos[$forAsiento]['haber'] = $haber;
    }
}
