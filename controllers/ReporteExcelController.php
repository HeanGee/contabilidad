<?php
/**
 * Created by PhpStorm.
 * User: dev03
 * Date: 14/09/2018
 * Time: 11:43 AM
 */

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\FlashMessageHelpsers;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ReporteExcelController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $this->verificarSesion();

        return $this->render('reporte-libro-venta', [
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    private function verificarSesion()
    {
        if (!\Yii::$app->session->has('core_empresa_actual') || \Yii::$app->session->get('core_empresa_actual') == "") {
            $msg = 'Falta especificar Empresa Actual.';
            FlashMessageHelpsers::createWarningMessage($msg);
            throw new NotFoundHttpException($msg);
        }
        if (!\Yii::$app->session->has('core_empresa_actual_pc') || \Yii::$app->session->get('core_empresa_actual_pc') == "") {
            $msg = 'Falta especificar Periodo Contable Actual para la Empresa Actual.';
            FlashMessageHelpsers::createWarningMessage($msg);
            throw new NotFoundHttpException($msg);
        }
        return true;
    }


    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /**
     * @param string $operacion
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionReporteExcel($operacion = 'venta')
    {
        $model = new Reporte();
        $model->operacion = $operacion;
        $model->filtro = 'fecha_emision__id';
        $model->moneda = 'todos';
        $model->valorizar = 'si';
        //$model->paper_size = Pdf::FORMAT_A4;

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $filtro = $model->filtro;
            $moneda = $model->moneda;


            if ($filtro == '') {
                FlashMessageHelpsers::createWarningMessage('Seleccione un filtro.');
                goto retorno;
            }
            if ($model->paper_size == '') {
                FlashMessageHelpsers::createWarningMessage('Indique un tamaño de hoja.');
                goto retorno;
            }
            if ($moneda == "") {
                FlashMessageHelpsers::createWarningMessage('Requiere indicar una moneda');
                goto retorno;
            }

            $data = ($operacion == 'venta') ? Venta::find() : Compra::find();

            if ($moneda > 0) {
                $data->where(['moneda_id' => $moneda]);
            }

            switch ($filtro) {
                case 'id':
                    $data->orderBy([$filtro => SORT_ASC]);
                    break;
                case 'fecha_emision__id':
                    $params = explode('__', $filtro);
                    $fecha = $params[0];
                    $id = $params[1];
                    $data->orderBy([$fecha => SORT_ASC, $id => SORT_ASC]);
                    break;
                case 'nro_factura':
                    if ($operacion == 'venta') {
                        $concat = "CONCAT((prefijo), ('-'), (nro_factura))";
                        $data->select(['*', $concat . ' AS nro_factura_completo'])
                            ->orderBy(['nro_factura_completo' => SORT_ASC]);
                    } else {
                        $data->orderBy([$filtro => SORT_ASC]);
                    }
                    break;
                case 'entidad_id__nro_factura': // Esto solo si es compra
                    $params = explode('__', $filtro);
                    $entidad_id = $params[0];
                    $nro_factura = $params[1];
                    $tname = ($operacion == 'venta') ? Venta::tableName() . '.' : Compra::tableName() . '.';
                    $data->joinWith('entidad as entidad')
                        ->select([
                            $tname . '*',
                            'entidad.razon_social'
                        ]);
                    $data->orderBy(['entidad.razon_social' => SORT_ASC, $nro_factura => SORT_ASC]);
                    break;
            }

            $desde = '';
            $hasta = '';
            if ($model->fecha_rango != '') {
                $rango = explode(' - ', $model->fecha_rango);
                $desde = $rango[0];
                $hasta = $rango[1];

                $desde = date_create_from_format('d-m-Y', $desde);
                $hasta = date_create_from_format('d-m-Y', $hasta);

                if ($desde && $hasta) {
                    $desde = $desde->format('Y-m-d');
                    $hasta = $hasta->format('Y-m-d');

                    $data->andFilterWhere(['between', 'fecha_emision', $desde, $hasta]);
                } else {
                    FlashMessageHelpsers::createWarningMessage('Rango de fecha incorrecto.');
                    goto retorno;
                }
            }

            $data->andFilterWhere(['>=', 'id', $model->cod_desde])->andFilterWhere(['<=', 'id', $model->cod_hasta]);
            $data->andWhere(['periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'), 'empresa_id' => \Yii::$app->session->get('core_empresa_actual')]);

            $data = $data->all();
            if (empty($data)) {
                FlashMessageHelpsers::createWarningMessage('No se encontraron resultados');
                goto retorno;
            };

            $cuentas_principales = $this->getCuentasPrincipalesByFactura($data);

            $count = count($data);
            if ($count > 150) {
                ini_set('memory_limit', (int)ceil($count * 0.9) . 'M');
            }

            $nro_facturas_libres = ($operacion == 'venta') ? $this->getNroFacturasLibres($data) : [];

            $path = 'modules\contabilidad\views\reporte\templates';
            $path = str_replace('\\', '/', $path);
            $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/reporte.css");
            $contenido = Yii::$app->view->renderFile("@app/" . $path . "/reporte.php", ['data' => $data, 'cuentas_principales' => $cuentas_principales, 'valorizar' => $model->valorizar, 'agrupar' => $model->agrupar, 'nro_facturas_libres' => $nro_facturas_libres]);
//            $encabezado = '';
            $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/reporte_header.php", ['data' => $data, 'del' => $desde, 'al' => $hasta]);
            $pie = Yii::$app->view->renderFile("@app/" . $path . "/reporte_pie.php", []);
            $op = ucfirst($operacion);
            \Yii::$app->response->format = Response::FORMAT_JSON;
            \Yii::$app->response->data = $data;
            setlocale(LC_TIME, 'es_ES.UTF-8');
           // return $pdf->render();
        }

        retorno:;
        return $this->render('reporte-libro-venta', [
            'model' => $model,
        ]);
    }

    /**
     * @param $data
     * @return array
     * @throws \yii\db\Exception
     */
    private function getCuentasPrincipalesByFactura($data)
    {

        $cuentas_principales = [];
        foreach ($data as $key => $venta) {
            /** Obtiene todos los ids de plantilla usados por la venta $key-esima.
             *
             *  Retorna un array de la forma:
             *  $q_result = [
             *      0 => [
             *          'plantilla_id' => nº
             *      ],
             *      1 => [
             *          'plantilla_id' => nº
             *      ],
             *      ...
             *  ]
             */
            $query = new Query();
            $query->select(['plantilla_id'])->from(VentaIvaCuentaUsada::tableName())
                ->where(['factura_venta_id' => $venta->id])->groupBy(['plantilla_id']);
            $comando = $query->createCommand();
            $q_result = $comando->queryAll();

            /** @var  $plantilla_ids
             *
             *  $plantilla_ids tiene la forma:
             *  $plantilla_ids = [
             *      0 => nro,
             *      1 => nro,
             *      ...
             *  ];
             */
            $plantilla_ids = [];
            foreach ($q_result as $result) {
                $plantilla_ids[] = $result['plantilla_id'];
            }

            /** @var  $query_result
             *
             *  Tiene la forma:
             *  $q_result = [
             *      0 => [
             *          'nombre' => 'Venta de Mercaderias gravadas por 10%...'
             *      ],
             *      1 => [...],
             *      ...
             *  ];
             */
            $query = new Query();
            $query->select(['cuenta.nombre'])->from(DetalleVenta::tableName() . ' AS ventad')
                ->leftJoin(PlantillaCompraventaDetalle::tableName() . ' AS plantillad', 'ventad.plan_cuenta_id = plantillad.p_c_gravada_id')
                ->leftJoin('cont_plan_cuenta AS cuenta', 'plantillad.p_c_gravada_id = cuenta.id')
                ->where(['ventad.factura_venta_id' => $venta->id])
                ->andWhere(['IN', 'plantillad.plantilla_id', $plantilla_ids])
                ->andWhere(['plantillad.cta_principal' => 'si']);
            $comando = $query->createCommand();
            $q_result = $comando->queryAll();

            /** @var  $query_result
             *
             *  Cambia a la forma:
             *  $q_result = [
             *      0 => 'Venta de Mercaderias gravadas por 10%...',
             *      1 => " ... ",
             *      ...
             *  ];
             */
            foreach ($q_result as $key2 => $value) {
                $q_result[$key2] = $q_result[$key2]['nombre'];
            }

            /** $cuentas_principales es un array de textos donde cada texto es la concatenacion de nombres de
             *      las cuentas contables (principales segun plantilla) mediante coma y espacio
             */
            $cuentas_principales[] = implode(', ', $q_result) . '.';
        }
        return $cuentas_principales;
    }

    /** -------------------------------------------
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws NotFoundHttpException
     * @throws \Exception
     */

    public function actionReporte()
    {
        $data = PlanCuenta::getCuentaLista();

        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $empresa = Empresa::findOne($empresa_id);
        $empresaNombre = 'Sin empresa seleccionada';
        $empresa_id = null;
        if ($empresa) {
            $empresaNombre = $empresa->nombre;
            $empresa_id = $empresa->id;
        }
        if (sizeof($data) == 0) {
            FlashMessageHelpsers::createWarningMessage('Cargue plan de cuentas.');
            return $this->redirect(['index']);
        }

        // Crear objetoExcel.
        /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        date_default_timezone_set('America/Asuncion');

        /** Create a new Spreadsheet Object **/
        $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/reporte-excel/templates/';
        $objPHPExcel = IOFactory::load($carpeta . 'reporteExcel_template.xlsx');


        //Rellenar Excel.
        $fila_titulo = 2;
        $fila = 3;
        $columna = 2;
        $columnaValor = 3;

        $max_fila = 0;
        $max_colum = 0;

        $currentDate = date('d-m-Y');
        $titulo = 'REPORTE HASTA EL   '.$currentDate;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo, $titulo);

        foreach ($data as $currentCuenta) {
            $currentCuentaNombre = $currentCuenta['nombre'];
            $currentCuentaId = $currentCuenta['id'];
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, ++$fila, $currentCuentaNombre);
            if($currentCuenta['asentable'] == 'no'){
                $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
                    ->getFont()->setBold(true);
                $suma_cuentas_hijo = $this->getSumaDeLasCuentasHijo($currentCuentaId, $empresa_id);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 3, $fila, $suma_cuentas_hijo);
            }
            $activos_fijos = $this->getActivosFijos($currentCuentaId, $empresa_id);
            $suma = 0;
            foreach ($activos_fijos as $currentActivoFijo) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, ++$fila, $currentActivoFijo['nombre']);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnaValor, $fila, $currentActivoFijo['valor_fiscal_neto']);
                $suma = $suma + $currentActivoFijo['valor_fiscal_neto'];
            }
            if($suma > 0){
                $currentActivoFijoNombre = $currentActivoFijo['nombre'];
                $titulo = 'SUMAS TOTALES '.$currentCuentaNombre;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, ++$fila, $titulo);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columnaValor + 1, $fila, $suma);
                $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
                    ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('b9c0cc');
                $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna + 1, $fila)->getCoordinate())
                    ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('b9c0cc');
                $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna + 2, $fila)->getCoordinate())
                    ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('b9c0cc');
            }


        }

        //Versión excel
        $carpeta = Yii::$app->basePath . '/web/uploads/';
        if (!is_dir($carpeta)) {
            if (!@mkdir($carpeta, 0777, true)) {
                throw new \Exception('No se puede crear carpeta.');
            }
        }
        Yii::$app->session->set('carpeta', $carpeta);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Exportar en navegador
        $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . 'Reporte_desde_'.$desde . '_hasta_el_' .$hasta .'_'.$empresaNombre.'.' . $fi['extension']);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx"));
        ob_clean();
        flush();
        readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");


        //borramos el fichero temporal
        unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Al index pero con el modelo
        $this->verificarSesion();


        return $this->redirect(['reporte-libro-venta']);

    }


    private function setNumberFormat(&$objPHPExcel, $columna, $fila)
    {
        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->getCoordinate())
            ->getNumberFormat()
            ->setFormatCode('#,##0');
    }

}