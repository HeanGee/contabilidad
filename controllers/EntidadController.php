<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\EntidadArchivo;
use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\Recibo;
use backend\modules\contabilidad\models\search\EntidadSearch;
use common\helpers\FileHelpers;
use common\helpers\FlashMessageHelpsers;
use RdKafka;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * EntidadController implements the CRUD actions for Entidad model.
 */
class EntidadController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Entidad models.
     * @return mixed
     */
    public function actionIndex($entidad = null)
    {
        $searchModel = new EntidadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $entidad);

        //        $this->executeJob();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'entidad' => $entidad,
        ]);
    }

    private function executeJob()
    {
        Yii::$app->queue->push(new \InsertEntidadesFromSet([
            'url' => 'http://example.com/image.jpg',
            'file' => '/tmp/image.jpg',
        ]));
    }

    /**
     * Displays a single Entidad model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Entidad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($entidad = null)
    {
        $model = new Entidad();
        $model->extranjero = 'no';

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                //                return $this->redirect(['view', 'id' => $model->id]);
                FlashMessageHelpsers::createSuccessMessage('Entidad ' . ucwords($model->razon_social) . ' creado correctamente.');
                //                return $this->redirect(['index', 'entidad'=>$entidad]);
                return $this->redirect(['index']);
            } else {
                FlashMessageHelpsers::createWarningMessage($model->getErrorSummaryAsString());
            }
        }

        return $this->render('/entidad/create', [
            'model' => $model,
            //            'entidad' => $entidad,
        ]);
    }

    /**
     * Updates an existing Entidad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $entidad = $model->tipo_entidad;

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();

            try {
                if ($model->save()) {
                    $transaction->commit();
                    FlashMessageHelpsers::createSuccessMessage($model->tipo_entidad . ' modificado correctamente.');
                    return $this->redirect(['index', 'entidad' => $entidad]);
                }
            } catch (\Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entidad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $model = $this->findModel($id);
            $nombre_entidad = $model->razon_social;
            $entidad = $model->tipo_entidad;

            // more tables using this 'entidad' table goes here...
            $ventas = $model->getFacturasVenta();
            $compras = $model->getFacturasCompra();
            $timbrados = $model->getTimbrados();
            $prestamos = Prestamo::find()->where(['entidad_id' => $id]);
            $recibos = Recibo::find()->where(['entidad_id' => $id]);


            // more checks for other tables using 'entidad' goes here...
            $msgs = [];
            if ($ventas->exists()) {
                $msgs[] = 'ERROR al intentar borrar entidad: Esta entidad tiene asociado FACTURAS DE VENTA.';
            }
            if ($compras->exists()) {
                $msgs[] = 'ERROR al intentar borrar entidad: Esta entidad tiene asociado FACTURAS DE COMPRA.';
            }
            if ($timbrados->exists()) {
                $msgs[] = 'ERROR al intentar borrar entidad: Esta entidad tiene asociado TIMBRADOS.';
            }
            if ($prestamos->exists()) {
                $msgs[] = 'ERROR al intentar borrar entidad: Esta entidad es utilizada en PRESTAMOS.';
            }
            if ($recibos->exists()) {
                $msgs[] = 'ERROR al intentar borrar entidad: Esta entidad es utilizada en RECIBOS de COMPRA y VENTA.';
            }
            foreach ($msgs as $msg) {
                FlashMessageHelpsers::createErrorMessage($msg);
            }
            if (sizeof($msgs) > 0) return $this->redirect(['index', 'entidad' => $entidad]);

            // other conditions for check goes here...

            // si la entidad no es utilizada en ninguna tabla, o si no viola otras posibles condiciones...
            if ($model->delete()) {
                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage($entidad . ' ' . $nombre_entidad . ' se ha eliminado correctamente.');
            } else {
                throw new \Exception("No se pudo eliminar: {$model->getErrorSummaryAsString()}");
            }
        } catch (\Exception $exception) {
            $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index', 'entidad' => $entidad]);
    }

    /**
     * Finds the Entidad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entidad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entidad::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false; // TODO: modificar mas tarde
    }

    public function actionGetDigitoVerificador($numero)
    {
        $basemax = 11;
        $v_numero_al = $numero;
        //cambiamos la ultima letra a ascii en caso que la cedula termine en letra
        $ultimoNro = substr($numero, -1);
        if (!($ultimoNro <= "9" && $ultimoNro >= "0"))
            $v_numero_al = substr($numero, 0, -1) . ord($ultimoNro);

        $k = 2;
        $v_total = 0;
        for ($i = strlen($v_numero_al) - 1; $i >= 0; $i--) {
            if ($k > $basemax) {
                $k = 2;
            }
            $v_numero_aux = intval(substr($v_numero_al, $i, 1));
            $v_total = $v_total + ($v_numero_aux * $k);
            $k++;
        }
        $v_resto = $v_total % $basemax;
        if ($v_resto > 1) {
            $v_digit = $basemax - $v_resto;
        } else {
            $v_digit = 0;
        }

        return $v_digit;
    }

    public function setOperaciones()
    {
        $operaciones = [];
        array_push($operaciones, 'contabilidad-entidad-view');
        array_push($operaciones, 'contabilidad-entidad-update');
        array_push($operaciones, 'contabilidad-entidad-delete');

        return $operaciones;
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionEntidadFromFile()
    {
        $conf = new RdKafka\Conf();
        $conf->set('log_level', (string)LOG_DEBUG);
        $conf->set('debug', 'all');
        $rk = new RdKafka\Producer($conf);
        $rk->addBrokers("localhost:9092");
        $topic = $rk->newTopic("t1");
        $k_msg = [
            'user' => Yii::$app->user->identity->id,
            'periodo-contable' => \Yii::$app->session->get('core_empresa_actual_pc', '<< missing empresa actual >>'),
            'empresa-actual' => Yii::$app->session->get('core_empresa_actual', '<< missing periodo contable >>'),
            'files' => []
        ];


        $model = new EntidadArchivo();
        $mensaje = "";

        if (Yii::$app->request->isPost) {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 300);

            $max_size = '10M';
            $max_time = 3600 * 5;
            /**
             * upload_max_filesize, post_max_size, max_input_time
             * Estas variables son de tipo PHP_INI_PERDIR, lo que significa que únicamente se debe establecer editando manualmente el php.ini
             *
             * @Fuente: Modos disponibles -> http://php.net/manual/en/configuration.changes.modes.php
             * @Fuente: Lista de variables setteables y sus modos -> http://php.net/manual/en/ini.list.php
             * @Fuente: Stackoverflow -> https://stackoverflow.com/questions/13442270/ini-setupload-max-filesize-200m-not-working-in-php
             */
            ini_set('max_execution_time', $max_time);
            $model->archivo_txt = UploadedFile::getInstances($model, 'archivo_txt');
            $extra_msg = "";
            if ($model->upload()) {
                foreach ($model->archivo_path as $file_path) {
                    $k_msg['files'][] = Yii::$app->basePath . '/web/' . str_replace('.zip', '.txt', $file_path);
                    $ext = substr($file_path, -4);
                    if ($ext == '.zip') {
                        if (FileHelpers::Unzip($file_path, false, false)) {
                            unlink($file_path);
                            if (!$extra_msg) $extra_msg = " and uncompressed.";
                        }
                    }
                }
                $topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode($k_msg));
                FlashMessageHelpsers::createSuccessMessage("Successfully uploaded$extra_msg");
            } else {
                FlashMessageHelpsers::createErrorMessage("Can not be uploaded");
            }
        } else {
            Yii::warning("Is Not a Post");
        }

        //        die();
        return $this->render('create_file', [
            'model' => $model, 'mensaje' => $mensaje,
            'log' => (isset($log) && !empty($log)) ? $log : [],
        ]);
    }

    public function actionGetColumnValueBy($column, $attribute = null, $value = null)
    {
        $_prev_memory_limit = ini_get('memory_limit');

        ini_set('memory_limit', '-1');

        $entidad = Entidad::find()->filterWhere([$attribute => $value]);

        if ($entidad->exists()) {
            $entidad = $entidad->one();

            if (array_key_exists($column, $entidad->attributes)) {
                return $entidad->$column;
            }

            return "-columna '{$column}' no existe en la tabla cont_entidad-";
        }

        ini_set('memory_limit', $_prev_memory_limit);

        return "-no existe, revisar filtro-";
    }
}
