<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use backend\modules\contabilidad\models\search\ActivoBiologicoEspecieClasificacionSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ActivoBiologicoEspecieClasificacionController implements the CRUD actions for ActivoBiologicoEspecieClasificacion model.
 */
class ActivoBiologicoEspecieClasificacionController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ActivoBiologicoEspecieClasificacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ActivoBiologicoEspecieClasificacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ActivoBiologicoEspecieClasificacion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ActivoBiologicoEspecieClasificacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ActivoBiologicoEspecieClasificacion();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            try {
                if (!$model->save()) {
                    throw new \Exception($model->getErrorSummaryAsString());
                }
                FlashMessageHelpsers::createSuccessMessage("Clasificacion {$model->nombre} creada correctamente.");

                if ($model->guardar_cerrar == 'si')
                    return $this->redirect(['index']);

                $especie_id = $model->activo_biologico_especie_id;
                $model = new ActivoBiologicoEspecieClasificacion();
                $model->loadDefaultValues();
                $model->activo_biologico_especie_id = $especie_id;
            } catch (\Exception $exception) {
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ActivoBiologicoEspecieClasificacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ActivoBiologicoEspecieClasificacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findModel($id);

            if (!$model->isDeleteable()) {
                throw new \Exception("Esta Clasificación es utilizada por un Activo Biológico");
            }

            $model->delete();
            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage("Clasificación {$model->nombre} eliminada correctamente.");
        } catch (\Exception $exception) {
            $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ActivoBiologicoEspecieClasificacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActivoBiologicoEspecieClasificacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ActivoBiologicoEspecieClasificacion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionGetClasificacionesAjax($q = null, $id = null)
    {
        $clasificicaciones = ActivoBiologicoEspecieClasificacion::find()
            ->where(['IS', 'activo_biologico_especie_id', null])
            ->orFilterWhere(['activo_biologico_especie_id' => $id])
            ->select(['id', 'nombre as text'])->asArray()->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result['results'] = array_values($clasificicaciones);
        Yii::warning($result);
        return $result;
    }
}
