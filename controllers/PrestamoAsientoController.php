<?php

namespace backend\modules\contabilidad\controllers;

use backend\modules\contabilidad\models\Asiento;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PrestamoDetalle;
use backend\modules\contabilidad\models\ReciboPrestamo;
use backend\modules\contabilidad\models\ReciboPrestamoDetalle;
use common\helpers\FlashMessageHelpsers;
use Yii;

class PrestamoAsientoController extends \backend\controllers\BaseController
{
    public function actionGenerarAsientoCuota($guardar = null)
    {
        $facturaDetallesProvider = [];
        $reciboDetallesProvider = [];
        if (\Yii::$app->request->isPost) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                // Obtener fecha-desde y fecha-hasta.
                $rango = \Yii::$app->request->post('rango_fecha', false);
                if (!$rango) {
                    throw new \Exception("Rango de Fecha está vacío.");
                }
                $slices = explode(' - ', $rango);
                $desde = implode('-', array_reverse(explode('-', $slices[0])));
                $hasta = implode('-', array_reverse(explode('-', $slices[1])));

                // Desde factura o desde recibo o ambos
                $desde_factura = Yii::$app->request->post('desde_factura', false);
                $desde_recibo = Yii::$app->request->post('desde_recibo', false);

                // Obtener detalles de factura con columnas marcadas para cuota de prestamo.
                $facturaDetallesProvider = ($desde_factura == '1') ? self::getFacturaDetalles($desde, $hasta) : [];
                $reciboDetallesProvider = ($desde_recibo == '1') ? self::getReciboDetalles($desde, $hasta) : [];

                $msg = [];
                if ($desde_factura == '1' && !sizeof($facturaDetallesProvider)) {
                    $msg[] = "No hay facturas no asentadas de cuotas de préstamos devengadas";
                }
                if ($desde_recibo == '1' && !sizeof($reciboDetallesProvider)) {
                    $msg[] = "No hay recibos no asentados de cuotas de préstamos devengadas";
                }
                if (sizeof($msg)) {
                    FlashMessageHelpsers::createWarningMessage(implode(', ', $msg) . '.');
                }

                // El usuario quiere guardar asientos.
                if ($guardar == 'si') {
                    self::registrarAsiento($facturaDetallesProvider, $desde, $hasta);
                    self::registrarAsientoRecibo($reciboDetallesProvider, $desde, $hasta);
                    if (sizeof($facturaDetallesProvider) || sizeof($reciboDetallesProvider)) {
                        $transaction->commit();
                        FlashMessageHelpsers::createSuccessMessage("Asiento generado correctamnte.");
                        return $this->redirect(['asiento/index']);
                    } else
                        $transaction->rollBack();
                }

            } catch (\Exception $exception) {
//                throw $exception;
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('/prestamo/asiento/generar-asiento-cuota', [
            'facturaDetallesProvider' => $facturaDetallesProvider,
            'reciboDetallesProvider' => $reciboDetallesProvider,
        ]);
    }

    private function getEmpresaActual()
    {
        return \Yii::$app->session->get('core_empresa_actual');
    }

    private function getPeriodoActual()
    {
        return \Yii::$app->session->get('core_empresa_actual_pc');
    }

    /**
     * @param string|null $desde
     * @param string|null $hasta
     * @return
     * @throws \yii\db\Exception
     */
    private function getFacturaDetalles($desde = null, $hasta = null)
    {
        $empresa_id = self::getEmpresaActual();
        $periodo_id = self::getPeriodoActual();

        $facturas = "
SELECT fc.id FROM cont_factura_compra fc
where fc.empresa_id = {$empresa_id} and fc.periodo_contable_id = {$periodo_id}
and fc.fecha_emision between '{$desde}' and '{$hasta}'
and fc.asiento_id is null
";
        $detalles = "
SELECT fcd.id as detalle_id,
 factura_compra_id,
  subtotal,
   cta_contable as partida,
    plan_cuenta_id,
     pc.cod_completo as cod_completo,
      pc.nombre as cuenta_nombre,
       prestamo_cuota_id
FROM cont_factura_compra_detalle fcd
left join cont_plan_cuenta pc on pc.id = fcd.plan_cuenta_id
where  fcd.factura_compra_id in ({$facturas})
and fcd.prestamo_cuota_id is not null
and fcd.asiento_id_por_cuota_prestamo is null
order by fcd.id, fcd.prestamo_cuota_id, fcd.cta_contable, fcd.plan_cuenta_id
";

        $query = \Yii::$app->db;
        $resultSet = $query->createCommand($detalles)->queryAll();

        return $resultSet;
    }

    /**
     * @param array $facturaDetallesProvider
     * @param null $desde
     * @param null $hasta
     * @throws \Exception
     */
    private function registrarAsiento($facturaDetallesProvider = [], $desde = null, $hasta = null)
    {
        /** @var Asiento $asiento */
        /** @var AsientoDetalle[] $asto_detalles */
        $asiento = null;
        $debe_total = 0;
        $haber_total = 0;
        $asto_detalles = [];
        $factura_detalles = [];
        $cuotas_procesadas = [];
        foreach ($facturaDetallesProvider as $index => $detalleFactura) {
            if (!in_array($detalleFactura['prestamo_cuota_id'], $cuotas_procesadas)) {
                // Insertar en el conjunto de cuotas procesadas.
                $cuotas_procesadas[] = $detalleFactura['prestamo_cuota_id'];

                // Si la cabecera no es nulo, significa que hay asiento por cuota anterior distinto al actual.
                if (isset($asiento)) { // cabecera del asiento correspondiente a la cuota anterior, diferente al actual.
                    $asiento->monto_debe = $debe_total;
                    $asiento->monto_haber = $haber_total;

                    // Actualizamos su monto del debe/haber y generamos id.
                    if (!$asiento->save(false)) {
                        throw new \Exception("Error guardando asiento: {$asiento->getErrorSummaryAsString()}");
                    }
                    $asiento->refresh();

                    // Asociamos detalles de asiento a su cabecera.
                    foreach ($asto_detalles as $asto_detalle) {
                        $asto_detalle->asiento_id = $asiento->id;
                        if (!$asto_detalle->save()) {
                            throw new \Exception("Error validando y guardando detalle de asiento: {$asto_detalle->getErrorSummaryAsString()}");
                        }
                        $asto_detalle->refresh();
                    }

                    // Asociamos los detalles de compra al asiento.
                    /** @var DetalleCompra $factura_detalle */
                    foreach (DetalleCompra::find()->where(['in', 'id', $factura_detalles])->all() as $factura_detalle) {
                        $factura_detalle->asiento_id_por_cuota_prestamo = $asiento->id;
                        if (!$factura_detalle->save()) {
                            throw new \Exception("Error actualizando detalle de compra: {$factura_detalle->getErrorSummaryAsString()}");
                        }
                    }
                }

                // Debe albergar nuevos detalles de asiento.
                $debe_total = 0;
                $haber_total = 0;
                $asto_detalles = [];
                $factura_detalles = [];
                $concepto = "[Desde Factura] " . PrestamoDetalle::findOne(['id' => $detalleFactura['prestamo_cuota_id']])->prestamo->concepto_devengamiento_cuota;

                $asiento = new Asiento();
                $asiento->empresa_id = self::getEmpresaActual();
                $asiento->periodo_contable_id = self::getPeriodoActual();
                $asiento->fecha = date('Y-m-t', strtotime($hasta));
                $asiento->concepto = "$concepto";
                $asiento->usuario_id = Yii::$app->user->id;
                $asiento->creado = date('Y-m-d H:i:s');
                $asiento->modulo_origen = 'contabilidad'; // TODO: ver si es necesario/correcto/mejorable

                // Campos requeridos mas no se puede determinar antes de tener sus detalles.
                $asiento->monto_debe = $asiento->monto_haber = 0;

                // Validamos.
                if (!$asiento->validate()) {
                    throw new \Exception("Error validando asiento: {$asiento->getErrorSummaryAsString()}");
                }
                $debug = $detalleFactura['prestamo_cuota_id'];
            }

            $asto_detalle = new AsientoDetalle();
            $asto_detalle->cuenta_id = $detalleFactura['plan_cuenta_id'];
            $asto_detalle->periodo_contable_id = self::getPeriodoActual();

            $debe_total += $asto_detalle->monto_debe = ($detalleFactura['partida'] == 'haber') ? 0 : (float)$detalleFactura['subtotal'];
            $haber_total += $asto_detalle->monto_haber = ($detalleFactura['partida'] == 'debe') ? 0 : (float)$detalleFactura['subtotal'];

            $asto_detalles[] = $asto_detalle;
            $factura_detalles[] = $detalleFactura['detalle_id'];
        }

        if (isset($asiento)) {
            $asiento->monto_debe = $debe_total;
            $asiento->monto_haber = $haber_total;

            // Actualizamos su monto del debe/haber y generamos id.
            if (!$asiento->save(false)) {
                throw new \Exception("Error guardando asiento: {$asiento->getErrorSummaryAsString()}");
            }
            $asiento->refresh();

            // Asociamos detalles de asiento a su cabecera.
            foreach ($asto_detalles as $asto_detalle) {
                $asto_detalle->asiento_id = $asiento->id;
                if (!$asto_detalle->save()) {
                    throw new \Exception("Error validando y guardando detalle de asiento: {$asto_detalle->getErrorSummaryAsString()}");
                }
                $asto_detalle->refresh();
            }

            // Asociamos los detalles de compra al asiento.
            /** @var DetalleCompra $factura_detalle */
            foreach (DetalleCompra::find()->where(['in', 'id', $factura_detalles])->all() as $factura_detalle) {
                $factura_detalle->asiento_id_por_cuota_prestamo = $asiento->id;
                if (!$factura_detalle->save()) {
                    throw new \Exception("Error actualizando detalle de compra: {$factura_detalle->getErrorSummaryAsString()}");
                }
            }
        }
    }

    private function getReciboDetalles($desde = null, $hasta = null)
    {
        $recibos = ReciboPrestamo::find()->where([
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ]);
        $recibos->andFilterWhere(['BETWEEN', 'fecha', $desde, $hasta]);

        if (!$recibos->exists()) return [];

        // Hay que retornar detalles como en getFacturaDetales(...)
        $recibos = $recibos->createCommand()->rawSql;
        $recibos = str_replace('SELECT *', 'SELECT id', $recibos);

        $reciboDetalles = "
SELECT * FROM cont_recibo_prestamo_detalle
where recibo_prestamo_id in ({$recibos})
and asiento_id is null
order by prestamo_detalle_id ASC";

        $query = \Yii::$app->db;
        $resultSet = $query->createCommand($reciboDetalles)->queryAll();

        $detalles = [];
        foreach ($resultSet as $resultItem) {
            $prestamoDetalle = PrestamoDetalle::findOne(['id' => $resultItem['prestamo_detalle_id']]);
            $cta_intereses_pagados = PlanCuenta::findOne(['id' => $prestamoDetalle->prestamo->cuenta_intereses_pagados]);
            $cta_caja = PlanCuenta::findOne(['id' => $prestamoDetalle->prestamo->cuenta_caja]);
            $cta_int_vencer = PlanCuenta::findOne(['id' => $prestamoDetalle->prestamo->cuenta_intereses_vencer]);
            $cta_intereses_a_pagar = PlanCuenta::findOne(['id' => $prestamoDetalle->prestamo->cuenta_intereses_a_pagar]);
            $cta_iva_10 = PlanCuenta::findOne(['id' => $prestamoDetalle->prestamo->cuenta_iva_10]);
            $cta_op = PlanCuenta::findOne(['id' => $prestamoDetalle->prestamo->cuenta_monto_operacion]);

            // Estructura similar a lo devuelto por getFacturaDetalles(...)
            // excepto por la clave 'factura_compra_id' que aqui es 'recibo_prestamo_detalle_id'

            // asiento de devengamiento
//            $detalles[] = [
//                'recibo_prestamo_detalle_id' => $resultItem['id'],
//                'subtotal' => $resultItem['interes'],
//                'partida' => 'debe',
//                'plan_cuenta_id' => $prestamoDetalle->prestamo->cuenta_intereses_pagados,
//                'cod_completo' => $cta_intereses_pagados->cod_completo,
//                'cuenta_nombre' => $cta_intereses_pagados->nombre,
//                'prestamo_cuota_id' => $resultItem['prestamo_detalle_id']
//            ];
//            $detalles[] = [
//                'recibo_prestamo_detalle_id' => $resultItem['id'],
//                'subtotal' => $resultItem['interes'],
//                'partida' => 'haber',
//                'plan_cuenta_id' => $prestamoDetalle->prestamo->cuenta_intereses_vencer,
//                'cod_completo' => $cta_int_vencer->cod_completo,
//                'cuenta_nombre' => $cta_int_vencer->nombre,
//                'prestamo_cuota_id' => $resultItem['prestamo_detalle_id']
//            ];

            // asiento de interes
            $interes = (float)$resultItem['interes'];
            $iva = $interes / 10;
            $detalles[] = [
                'recibo_prestamo_detalle_id' => $resultItem['id'],
                'subtotal' => $interes,
                'partida' => 'debe',
                'plan_cuenta_id' => $prestamoDetalle->prestamo->cuenta_intereses_a_pagar,
                'cod_completo' => $cta_intereses_a_pagar->cod_completo,
                'cuenta_nombre' => $cta_intereses_a_pagar->nombre,
                'prestamo_cuota_id' => $resultItem['prestamo_detalle_id']
            ];
            $detalles[] = [
                'recibo_prestamo_detalle_id' => $resultItem['id'],
                'subtotal' => $interes,
                'partida' => 'haber',
                'plan_cuenta_id' => $prestamoDetalle->prestamo->cuenta_caja,
                'cod_completo' => $cta_caja->cod_completo,
                'cuenta_nombre' => $cta_caja->nombre,
                'prestamo_cuota_id' => $resultItem['prestamo_detalle_id']
            ];

            // asiento de cuota
            $capital = $resultItem['capital'];
            $interes = (float)$resultItem['interes'];
            $iva_interes = round($interes / 10);
            $capital = $capital + $iva_interes;
            $detalles[] = [
                'recibo_prestamo_detalle_id' => $resultItem['id'],
                'subtotal' => $capital,
                'partida' => 'debe',
                'plan_cuenta_id' => $prestamoDetalle->prestamo->cuenta_monto_operacion,
                'cod_completo' => $cta_op->cod_completo,
                'cuenta_nombre' => $cta_op->nombre,
                'prestamo_cuota_id' => $resultItem['prestamo_detalle_id']
            ];
            $detalles[] = [
                'recibo_prestamo_detalle_id' => $resultItem['id'],
                'subtotal' => $capital,
                'partida' => 'haber',
                'plan_cuenta_id' => $prestamoDetalle->prestamo->cuenta_caja,
                'cod_completo' => $cta_caja->cod_completo,
                'cuenta_nombre' => $cta_caja->nombre,
                'prestamo_cuota_id' => $resultItem['prestamo_detalle_id']
            ];
        }

//        echo print_r($detalles); exit();
        return $detalles;
    }

    /**
     * @param array $reciboDetallesProvider
     * @param null $desde
     * @param null $hasta
     * @throws \Exception
     */
    private function registrarAsientoRecibo($reciboDetallesProvider = [], $desde = null, $hasta = null)
    {
        /** @var Asiento $asiento */
        /** @var AsientoDetalle[] $asto_detalles */
        $asiento = null;
        $debe_total = 0;
        $haber_total = 0;
        $asto_detalles = [];
        $recibo_detalles = [];
        $cuotas_procesadas = [];
        foreach ($reciboDetallesProvider as $index => $detalleRecibo) {
            if (!in_array($detalleRecibo['prestamo_cuota_id'], $cuotas_procesadas)) {
                // Insertar en el conjunto de cuotas procesadas.
                $cuotas_procesadas[] = $detalleRecibo['prestamo_cuota_id'];

                // Si la cabecera no es nulo, significa que hay asiento por cuota anterior distinto al actual.
                if (isset($asiento)) { // cabecera del asiento correspondiente a la cuota anterior, diferente al actual.
                    $asiento->monto_debe = $debe_total;
                    $asiento->monto_haber = $haber_total;

                    // Actualizamos su monto del debe/haber y generamos id.
                    if (!$asiento->save()) {
                        throw new \Exception("Error guardando asiento: {$asiento->getErrorSummaryAsString()}");
                    }
                    $asiento->refresh();

                    // Asociamos detalles de asiento a su cabecera.
                    foreach ($asto_detalles as $asto_detalle) {
                        $asto_detalle->asiento_id = $asiento->id;
                        if (!$asto_detalle->save()) {
                            throw new \Exception("Error validando y guardando detalle de asiento: {$asto_detalle->getErrorSummaryAsString()}");
                        }
                        $asto_detalle->refresh();
                    }

                    // Asociamos los detalles de recibo al asiento
                    /** @var ReciboPrestamoDetalle $recibo_detalle */
                    foreach (ReciboPrestamoDetalle::find()->where(['in', 'id', $recibo_detalles])->all() as $recibo_detalle) {
                        $recibo_detalle->asiento_id = $asiento->id;
                        if (!$recibo_detalle->save()) {
                            throw new \Exception("Error actualizando detalle de recibo: {$recibo_detalle->getErrorSummaryAsString()}");
                        }
                    }
                }

                // Debe albergar nuevos detalles de asiento.
                $debe_total = 0;
                $haber_total = 0;
                $asto_detalles = [];
                $recibo_detalles = [];
                $concepto = "[Desde Recibo] " . PrestamoDetalle::findOne(['id' => $detalleRecibo['prestamo_cuota_id']])->prestamo->concepto_devengamiento_cuota;

                $asiento = new Asiento();
                $asiento->empresa_id = self::getEmpresaActual();
                $asiento->periodo_contable_id = self::getPeriodoActual();
                $asiento->fecha = date('Y-m-t', strtotime($hasta));
                $asiento->concepto = "$concepto";
                $asiento->usuario_id = Yii::$app->user->id;
                $asiento->creado = date('Y-m-d H:i:s');
                $asiento->modulo_origen = 'contabilidad'; // TODO: ver si es necesario/correcto/mejorable

                // Campos requeridos mas no se puede determinar antes de tener sus detalles.
                $asiento->monto_debe = $asiento->monto_haber = 0;

                // Validamos.
                if (!$asiento->validate()) {
                    throw new \Exception("Error validando asiento: {$asiento->getErrorSummaryAsString()}");
                }
            }

            $asto_detalle = new AsientoDetalle();
            $asto_detalle->cuenta_id = $detalleRecibo['plan_cuenta_id'];
            $asto_detalle->periodo_contable_id = self::getPeriodoActual();

            $debe_total += $asto_detalle->monto_debe = ($detalleRecibo['partida'] == 'haber') ? 0 : (float)$detalleRecibo['subtotal'];
            $haber_total += $asto_detalle->monto_haber = ($detalleRecibo['partida'] == 'debe') ? 0 : (float)$detalleRecibo['subtotal'];

            $asto_detalles[] = $asto_detalle;
            $recibo_detalles[] = $detalleRecibo['recibo_prestamo_detalle_id'];
        }

        if (isset($asiento)) {
            $asiento->monto_debe = $debe_total;
            $asiento->monto_haber = $haber_total;

            // Actualizamos su monto del debe/haber y generamos id.
            if (!$asiento->save(false)) {
                throw new \Exception("Error guardando asiento: {$asiento->getErrorSummaryAsString()}");
            }
            $asiento->refresh();

            // Asociamos detalles de asiento a su cabecera.
            foreach ($asto_detalles as $asto_detalle) {
                $asto_detalle->asiento_id = $asiento->id;
                if (!$asto_detalle->save()) {
                    throw new \Exception("Error validando y guardando detalle de asiento: {$asto_detalle->getErrorSummaryAsString()}");
                }
                $asto_detalle->refresh();
            }

            // Asociamos los detalles de recibo al asiento
            /** @var ReciboPrestamoDetalle $recibo_detalle */
            foreach (ReciboPrestamoDetalle::find()->where(['in', 'id', $recibo_detalles])->all() as $recibo_detalle) {
                $recibo_detalle->asiento_id = $asiento->id;
                if (!$recibo_detalle->save()) {
                    throw new \Exception("Error actualizando detalle de recibo: {$recibo_detalle->getErrorSummaryAsString()}");
                }
            }
        }
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}
