<?php

namespace backend\modules\contabilidad\controllers;

use Yii;
use DateTime;
use yii\db\Query;
use yii\base\Model;
use yii\db\Command;
use yii\db\Exception;
use yii\web\Response;
use yii\db\Connection;
use yii\db\ActiveRecord;
use yii\filters\VerbFilter;
use backend\helpers\Helpers;
use yii\helpers\ArrayHelper;
use backend\models\Cotizacion;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use common\helpers\PermisosHelpers;
use backend\controllers\BaseController;
use common\helpers\FlashMessageHelpsers;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\Poliza;
use backend\modules\contabilidad\models\Recibo;
use backend\modules\contabilidad\models\Asiento;
use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\PrestamoDetalle;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PolizaDevengamiento;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\search\AsientoSearch;
use backend\modules\contabilidad\models\ReciboPrestamoDetalle;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;

/**
 * AsientoController implements the CRUD actions for Asiento model.
 */
class AsientoController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    private function clearSession()
    {
        Yii::$app->session->remove('cont_asiento_from_factura_compra_detalle-provider');
        Yii::$app->session->remove('cont_asiento_from_factura_costo_venta_detalle-provider');
        Yii::$app->session->remove('cont_asiento_from_factura_venta_detalle-provider');
        Yii::$app->session->remove('cont_asiento_from_factura_devengamientos-provider');
        Yii::$app->session->remove('cont_asiento_from_factura_prestamo-provider');
        Yii::$app->session->remove('cont_asiento_from_factura_cuota_recibo-provider');
        Yii::$app->session->remove('cont_asientodetalle_prestamo');
    }

    /**
     * Lists all Asiento models.
     * @param bool $showAllEmpresa
     * @return mixed
     */
    public function actionIndex($showAllEmpresa = false)
    {
        PermisosHelpers::getAcceso('contabilidad-asiento-show-all-empresa') ||
        $showAllEmpresa = false;

        $searchModel = new AsientoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $showAllEmpresa);

        $this->clearSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'showAllEmpresa' => $showAllEmpresa
        ]);
    }

    public function actionShowAllEmpresa()
    {
        return $this->redirect(['index']);
    }

    /**
     * Displays a single Asiento model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Asiento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception
     */
    public function actionCreate()
    {
        $model = new Asiento();

        $detalles = [];
        foreach ($model->asientoDetalles as $_k => $_v)
            $detalles[$_k + 1] = $_v;

        /* eliminar el blue print */
        if (!empty($_POST['AsientoDetalle']['__id__'])) {
            unset($_POST['AsientoDetalle']['__id__']);
            Yii::$app->request->setBodyParams($_POST);
        }
        $detalles_post = Yii::$app->request->post('AsientoDetalle', []);
        foreach ($detalles_post as $_k => $_v)
            if (empty($detalles[$_k]))
                $detalles[$_k] = new AsientoDetalle();

        if ($model->load(Yii::$app->request->post()) && AsientoDetalle::loadMultiple($detalles, Yii::$app->request->post())) {
            $model->empresa_id = Yii::$app->session->get('core_empresa_actual');
            $model->monto_debe = Yii::$app->request->post('total-monto_debe');
            $model->monto_haber = Yii::$app->request->post('total-monto_haber');
//            $fecha = DateTime::createFromFormat('d-m-Y', $model->fecha);
//            $model->fecha = $fecha->format('Y-m-d');
            $model->usuario_id = Yii::$app->user->identity->getId();
            $model->creado = date('Y-m-d H:i:s');
            $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
            $model->modulo_origen = 'contabilidad';
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    foreach ($detalles as &$_detalle) {
                        if (empty($_detalle->id))
                            $_detalle->asiento_id = $model->id;
                        $_detalle->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

                        if (!$_detalle->save())
                            throw new Exception('Ha ocurrido un error al guardar un detalle.');
                        $_detalle->refresh();
                    }
                    $model->refresh();

                    if (!$model->balance()) {
                        throw new \Exception("Error balanceando asiento: {$model->getErrorSummaryAsString()}");
                    }
                    if (!$model->save(false)) {
                        throw new \Exception("Error guardando asiento: La validacion fue exitosa pero no se pudo guardar.");
                    }

                    $transaction->commit();
                    FlashMessageHelpsers::createSuccessMessage('Asiento creado exitosamente.');
                    return $this->redirect(['index']);
                }
                FlashMessageHelpsers::createErrorMessage('No se pudo crear el asiento.');

            } catch (\Exception $e) {
                $transaction->rollBack();
                FlashMessageHelpsers::createErrorMessage($e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
            'detalles' => $detalles
        ]);
    }

    /**
     * Updates an existing Asiento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws Exception
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $detalles = [];
        foreach ($model->asientoDetalles as $_k => $_v)
            $detalles[$_k + 1] = $_v;

        if (!empty($_POST['AsientoDetalle']['__id__'])) { // eliminar el blue print
            unset($_POST['AsientoDetalle']['__id__']);
            Yii::$app->request->setBodyParams($_POST);
        }
        $detalles_post = Yii::$app->request->post('AsientoDetalle', []);
        foreach ($detalles_post as $_k => $_v)
            if (empty($detalles[$_k]))
                $detalles[$_k] = new AsientoDetalle();

        if ($model->load(Yii::$app->request->post()) && AsientoDetalle::loadMultiple($detalles, Yii::$app->request->post())) {
//            $asiento_fecha = DateTime::createFromFormat('d-m-Y', $model->fecha);
//            $model->fecha = $asiento_fecha->format('Y-m-d');
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    foreach ($detalles as $key => &$_detalle) {
                        if (empty($_detalle->id))
                            $_detalle->asiento_id = $model->id;

                        if (empty($detalles_post[$key])) { // se elimina lo que ya no vuelve del form
                            if (!$_detalle->delete()) // por alguna razón, no funciona en el mismo IF de la línea de arriba
                                throw new Exception('Ha ocurrido un error al eliminar un detalle.');

                        } elseif (!$_detalle->save())
                            throw new Exception('Ha ocurrido un error al guardar un detalle.');
                        $_detalle->refresh();
                    }
                    $model->refresh();

                    if (!$model->balance()) {
                        throw new \Exception("Error balanceando asiento: {$model->getErrorSummaryAsString()}");
                    }
                    if (!$model->save(false)) {
                        throw new \Exception("Error guardando asiento: La validacion fue exitosa pero no se pudo guardar.");
                    }

                    $transaction->commit();
                    FlashMessageHelpsers::createSuccessMessage('Asiento actualizado exitosamente.');
                    return $this->redirect(['index']);
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                FlashMessageHelpsers::createErrorMessage($e->getMessage());

            }
        }

        return $this->render('update', [
            'model' => $model,
            'detalles' => $detalles
        ]);
    }

    /**
     * Deletes an existing Asiento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        /** @var Asiento $model */
        $model = Asiento::find()->where([
            'id' => $id,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
        ])->one();

        if ($model == null) {
            throw new NotFoundHttpException('La página solicitada no existe.');
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            /** @var Compra[] $compras */
            /** @var Venta[] $ventas */
            /** @var DetalleCompra[] $detallesCompraAsientoPorCuotaPrestamo */

            $compras = Compra::find()->where(['asiento_id' => $id])->all();
            $ventas = Venta::find()->where(['asiento_id' => $id])->all();
            $asientos = Asiento::find()->where(['asiento_id' => $id])->all();
            $seguros = PolizaDevengamiento::find()->where(['asiento_id' => $id])->all();
            $cuotas = ReciboPrestamoDetalle::find()->where(['asiento_id' => $id])->all();
            $prestamos = Prestamo::find()->where(['asiento_id' => $id])->all();
            $detallesCompraAsientoPorCuotaPrestamo = DetalleCompra::find()->where(['asiento_id_por_cuota_prestamo' => $id])->all();
            $asientos_id = [];
            $recibos = Recibo::findAll(['asiento_id' => $id]);
            $retenciones = Retencion::findAll(['asiento_id' => $id]);

            foreach ($asientos as $asiento) {
                $asientos_id[] = $asiento->id;
            }
            $asientos_asiento_detalles = AsientoDetalle::find()->where(['IN', 'asiento_id', $asientos_id])->all();
            $asiento_detalles = AsientoDetalle::find()->where(['asiento_id' => $id])->all();

            foreach ($compras as $compra) {
                $compra->asiento_id = null;
                if (!$compra->save(false)) {
                    $transaction->rollBack();
                    FlashMessageHelpsers::createWarningMessage('No se pudo desasociar las compras del asiento.');
                    goto retorno;
                }
            }
            // Detalles de compra, asociadas a un asiento, por las cuotas de prestamo devengadas.
            foreach ($detallesCompraAsientoPorCuotaPrestamo as $detalle) {
                $detalle->asiento_id_por_cuota_prestamo = null;
                if (!$detalle->save()) {
                    throw new \Exception("Error desasociando detalles de compra (por cuota de prestamo): {$detalle->getErrorSummaryAsString()}");
                }
            }

            foreach ($ventas as $venta) {
                $venta->asiento_id = null;
                $venta->timbrado_detalle_id_prefijo = 0; // se queja por ser required.
                if (!$venta->save(false)) {
                    $msg = '';
                    foreach ($venta->getErrorSummary(true) as $item) {
                        $msg .= $item . ', ';
                    }
                    $transaction->rollBack();
                    FlashMessageHelpsers::createWarningMessage('No se pudo desasociar las ventas del asiento: ' . $msg);
                    goto retorno;
                }
            }

            foreach ($seguros as $seguro) {
                $seguro->asiento_id = null;
                $seguro->save(false);
            }

            foreach ($cuotas as $cuota) {
                $cuota->asiento_id = null;
                $cuota->save(false);
                $cuota->refresh();
            }

            foreach ($prestamos as $prestamo) {
                $prestamo->asiento_id = null;
                $prestamo->save(false);
                $prestamo->refresh();
            }

            // desasignar cuota de prestamos de asiento (de devengamiento)
            /** @var PrestamoDetalle $cuota */
            foreach (PrestamoDetalle::find()->where(['asiento_devengamiento_id' => $id])->all() as $cuota) {
                $cuota->asiento_devengamiento_id = null;
                if (!$cuota->save()) {
                    throw new \Exception("Error desasociando cuota #{$cuota->nro_cuota} del prestamo #{$cuota->prestamo_id} de este asiento (de devengamiento): {$cuota->getErrorSummaryAsString()}");
                }
            }

            // Borrar asiento_detalles de los asientos asociados al asiento a borrar, por ejemplo: asiento y sus detalles por costo de venta.
            foreach ($asientos_asiento_detalles as $item) {
                if (!$item->delete()) {
                    $transaction->rollBack();
                    FlashMessageHelpsers::createWarningMessage('No se pudo borrar detalles de asientos asociados a este asiento.');
                    goto retorno;
                }
            }
            foreach ($asientos as $asiento) {
                if (!$asiento->delete()) {
                    $transaction->rollBack();
                    FlashMessageHelpsers::createWarningMessage('Error borrando asiento asociado a este asiento.');
                    goto retorno;
                }
            }

            // borrar detalles del asiento a borrar.
            foreach ($asiento_detalles as $asiento_detalle) {
                if (!$asiento_detalle->delete()) {
                    $transaction->rollBack();
                    FlashMessageHelpsers::createWarningMessage('No se pudo desasociar los asiento_detalles del asiento.');
                    goto retorno;
                }
            }

            # Es asiento de revaluo de activo fijo
            $activosF = ActivoFijo::findAll(['asiento_revaluo_id' => $id]);
            if (isset($activosF)) {
                foreach ($activosF as $activoFijo) {
                    if ($activoFijo->asiento_depreciacion_id != '') {
                        throw new \Exception("No se puede borrar un asiento de revalúo de un activo fijo que se ha revaluado y depreciado. Borre primero el asiento de depreciación y luego vuelva a intentar.");
                    }

                    $activoFijo->asiento_revaluo_id = '';
                    $activoFijo->valor_fiscal_revaluado = 0;
                    $activoFijo->valor_contable_revaluado = 0;
                    if (!$activoFijo->save()) {
                        throw new \Exception("Error desvinculando activo fijo: Es un asiento de revalúo pero no se pudo desasociar del activo fijo correspondiente: {$activoFijo->getErrorSummaryAsString()}");
                    }
                }
            }

            # Es asiento de depreciacion
            $activosF = ActivoFijo::findAll(['asiento_depreciacion_id' => $id]);
            if (isset($activosF)) {
                foreach ($activosF as $activoFijo) {
                    $activoFijo->asiento_depreciacion_id = '';
                    $activoFijo->valor_fiscal_depreciado = 0;
                    $activoFijo->valor_contable_depreciado = 0;
                    if (!$activoFijo->save()) {
                        throw new \Exception("Error desvinculando activo fijo: Es un asiento de depreciación pero no se pudo desasociar del activo fijo correspondiente: {$activoFijo->getErrorSummaryAsString()}");
                    }
                }
            }

            // El asiento a borrar es uno de cierre de periodo.
            $periodoCierre = EmpresaPeriodoContable::findOne(['asiento_cierre_id' => $id]);
            if (isset($periodoCierre)) {
                $periodoReaperturaCorrelated = EmpresaPeriodoContable::find()->where(['periodo_anterior_id' => $periodoCierre->id])->one();
                if ($periodoReaperturaCorrelated->asiento_apertura_id != '') {
                    throw new \Exception('Hay un asiento de reapertura del siguiente periodo que es correlativo a este asiento de cierre. Aun falta definir los controles previos antes de borrar uno.');
                }

                $periodoCierre->asiento_cierre_id = '';
                if (!$periodoCierre->save()) {
                    throw new \Exception("Error al desasociar del periodo de cierre: {$periodoCierre->getErrorSummaryAsString()}");
                }
            } else {
                $periodoReapertura = EmpresaPeriodoContable::findOne(['asiento_apertura_id' => $id]);
                if (isset($periodoReapertura)) {
                    # El asiento a borrar es un asiento de reapertura.
                    if (($msg = $model->hasData($model->periodo_contable_id, $model->empresa_id))) {
                        throw new \Exception("Error borrando asiento de reapertura: $msg");
                    }

                    $activosFijosTraspasados = ActivoFijo::find()
                        ->where(['empresa_periodo_contable_id' => $model->periodo_contable_id, 'empresa_id' => $model->empresa_id])
                        ->andWhere(['IS NOT', 'activo_fijo_id', null]);  # Con esto se asume que es suficiente, para saber con exactitud que activo fijo traspasado.
//                        ->andWhere(['IS', 'asiento_revaluo_id', null])
//                        ->andWhere(['IS', 'asiento_depreciacion_id', null])
//                        ->andWhere(['IS', 'factura_compra_id', null])
//                        ->andWhere(['IS', 'factura_venta_id', null])
//                        ->andWhere(['IS', 'factura_venta_id', null]);
                    /** @var ActivoFijo $activoFijoTraspasado */
                    foreach ($activosFijosTraspasados->all() as $activoFijoTraspasado) {
                        if (!$activoFijoTraspasado->delete()) {
                            throw new \Exception("Error borrando activos fijos traspasados: {$activoFijoTraspasado->getErrorSummaryAsString()}");
                        }
                    }

                    # TODO: Borrar activos fijos traspasados (ok)
                    # TODO: Borrar activos biologicos traspasados
                    # TODO: Borrar id de asiento de reapertura del periodo contable (ok)

                    $periodoReapertura->asiento_apertura_id = '';  # eliminar id del asiento de reapertura del periodo contable
                    if (!$periodoReapertura->save()) {
                        throw new \Exception("Error al desasociar del periodo de reapertura: {$periodoReapertura->getErrorSummaryAsString()}");
                    }
                }
            }

            # Desasociar recibos asentados
            foreach ($recibos as $recibo) {
                $recibo->asiento_id = null;
                if (!$recibo->save())
                    throw new \Exception("No se pudo desasociar recibo ID:{$recibo->id}: {$recibo->getErrorSummaryAsString()}");
            }

            # Desasociar retenciones
            foreach ($retenciones as $retencion) {
                $retencion->asiento_id = null;
                $retencion->factura_id = 5; // campo virtual necesario
                if (!$retencion->save())
                    throw new \Exception("No se puede desasociar retencion ID: $retencion->id: {$retencion->getErrorSummaryAsString()}");
            }

            if (!$model->delete()) {
                FlashMessageHelpsers::createWarningMessage('No se pudo borrar el asiento.');
                $transaction->rollBack();
                goto retorno;
            };

            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage('Asiento eliminado exitosamente. Si este asiento había sido generado desde una factura, la misma queda liberada.');
        } catch (\Throwable $e) {
//            throw $e;
            FlashMessageHelpsers::createErrorMessage($e);
            $transaction->rollBack();
        }

        retorno:;
        return $this->redirect(['index']);
    }

    /**
     * Finds the Asiento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Asiento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Asiento::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param        $operacion
     * @param string $tipo_operacion
     *
     * @return string
     * @throws \Exception
     */
    public function actionCreateFromFactura($operacion, $tipo_operacion = 'generar')
    {
        require 'AsientoReciboManager.php';
        ini_set('memory_limit', '-1');

        $model = new TemplateModel();
	    $model->devengamientos = 0;
	    $model->devengamientos2 = 0;
	    $model->prestamo = 0;
	    $model->fecha_rango = date("01-m-Y", strtotime(date("d-m-Y") . "-1 month")) .
		    ' - ' . date("t-m-Y", strtotime(date("d-m-Y") . "-1 month"));
	    $model->descripcion = "COMPRA MES " . strtoupper(Helpers::monthToText(date('m', strtotime(date('d-m-Y') . '-1 month'))));
        /** DEBUGG PURPOSE */
//        $model->descripcion = "ASIENTO 1 NOV";
//        $model->concepto_devengamiento = "ASIENTO DEV SEG 1 NOV";
//        $model->concepto_devengamiento2 = "ASIENTO DEV PRESTAMO 1 NOV";
//        $model->prestamo_concepto = "ASIENTO PRESTAMO 1 NOV";
        /** DEBUGG PURPOSE */
        $result = [];
        $asientosIndependientes = [];
        $asiento_recibo = null;

        $t = Yii::$app->db->beginTransaction();

        try {
            // Limpiamos la session para que no quede rastros de asientos que ahora no van a estar posiblemente.
            $this->clearSession();

            if ($operacion == 'venta') {
                $coef_costo = ParametroSistema::getEmpresaActualCoefCosto();
                $asientos_generados = false;

                if (!isset($coef_costo)) {
                    throw new \yii\base\Exception("Falta especificar en el parámetro de la empresa el coeficiente de costo.");
                }

                Yii::$app->session->set('cont_asiento_from_factura_venta_detalle-provider', new ArrayDataProvider([
                    'allModels' => [],
                    'pagination' => false,
                ]));
                Yii::$app->session->set('cont_asiento_from_factura_costo_venta_detalle-provider', new ArrayDataProvider([
                    'allModels' => [],
                    'pagination' => false,
                ]));

                if ($model->load(Yii::$app->request->post())) {
                    /** @var AsientoDetalle[] $asientos_detalle */
                    /** @var AsientoDetalle[] $asientos_costo_detalle */

                    $fecha = explode(' - ', $model->fecha_rango);

                    if ($fecha[0] == '') { // explode siempre retorna un array, al menos de 1 solo elemento con value == ''
                        throw new \Exception('Debe especificar correctamente el rango de fecha.');
                    }

                    $desde = date_create_from_format('d-m-Y', $fecha[0]);
                    $hasta = date_create_from_format('d-m-Y', $fecha[1]);
                    $periodo_actual = EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')]);

                    if (!$desde) {
                        throw new \Exception('Formato de la fecha Desde incorrecto.');
                    } elseif (!$hasta) {
                        throw new \Exception('Formato de la feha Hasta incorrecto');
                    }/* elseif ($desde->format('Y') != $periodo_actual->anho) {
                    FlashMessageHelpsers::createWarningMessage('La fecha de inicio no corresponde al periodo actual.');
                    goto retorno;
                } elseif ($hasta->format('Y') != $periodo_actual->anho) {
                    FlashMessageHelpsers::createWarningMessage('La fecha de fin no corresponde al periodo actual.');
                    goto retorno;
                } */ elseif ($model->descripcion == '') {
                        throw new \Exception('Falta indicar el concepto del asiento.');
                    }

                    // Para venta, no hace falta porque aun la empresa no se dedica a venta de seguros.
//                if ($model->devengamientos && ParametroSistema::getValorByNombre('cuenta_seguros_pagados_id') == "") {
//                    FlashMessageHelpsers::createWarningMessage('Error Interno: Falta especificar en el parámetro del módulo, el id de la cuenta para SEGUROS PAGADOS.');
//                    goto retorno;
//                }

                    $fechaAsiento = date('Y-m-t', strtotime($hasta->format('Y-m-d')));;
                    $asiento = new Asiento();
                    $asiento->empresa_id = \Yii::$app->session->get('core_empresa_actual');
                    $asiento->periodo_contable_id = $periodo_actual->id;
                    $asiento->fecha = $fechaAsiento;
                    $asiento->concepto = $model->descripcion;
                    $asiento->usuario_id = Yii::$app->user->id;
                    $asiento->creado = date('Y-m-d H:i:s');
                    $asiento->modulo_origen = 'contabilidad'; // TODO: ver si es necesario/correcto/mejorable
                    $asiento->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');

                    $ve = Venta::tableName() . '.';
                    $de = DetalleVenta::tableName() . '.';
                    $pc = PlanCuenta::tableName() . '.';
                    $tipoDocSetNotaCredito = Venta::getTipodocSetNotaCredito();
                    $query = new Query();
                    $query->select([
                        'id_venta' => 'MIN(' . $ve . 'id)',
                        'tipo_documento_id' => "MIN({$ve}tipo_documento_id)",
                        'id_moneda' => 'MIN(' . $ve . 'moneda_id)',
                        'cotizacion' => 'MIN(' . $ve . 'valor_moneda)',
                        'fecha_de_emision' => 'MIN(' . $ve . 'fecha_emision)',
                        'estado' => 'MIN(' . $ve . 'estado)',
                        'subtotal' => 'SUM(' . $de . 'subtotal)',
                        'total_factura' => 'MIN(' . $ve . 'total)',
                        'saldo' => 'MIN(' . $de . 'cta_contable)',
                        'id_cta' => 'MIN(' . $de . 'plan_cuenta_id)',
                        'nombre_cta' => 'MIN(' . $pc . 'nombre)',
                        'cta_codigo' => 'MIN(' . $pc . 'cod_completo)',
                    ])->from(Venta::tableName());
                    $query->leftJoin(DetalleVenta::tableName(), $de . 'factura_venta_id = ' . $ve . 'id')
                        ->leftJoin(PlanCuenta::tableName(), $de . 'plan_cuenta_id = ' . $pc . 'id')
                        ->leftJoin('cont_tipo_documento td', 'td.id = cont_factura_venta.tipo_documento_id')
                        ->leftJoin('cont_tipo_documento_set tds', 'tds.id = td.tipo_documento_set_id');
                    $query->where([$ve . 'empresa_id' => \Yii::$app->session->get('core_empresa_actual'), $ve . 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])
                        ->andWhere(['BETWEEN', $ve . 'fecha_emision', $desde->format('Y-m-d'), $hasta->format('Y-m-d')])
                        ->andWhere(['NOT IN', $ve . 'estado', ['anulada', 'faltante']])
                        ->andWhere([$ve . 'asiento_id' => null]);
                    $query->groupBy([$ve . 'id', $pc . 'id']); // para agrupar detalles por factura y sumarizar c/detalle por cuentas
                    $command = $query->createCommand();
                    $data = $command->queryAll();

                    #Separar facturas con tipo de documento marcado para asentar independientemente.
                    $facturasNormales = [];
                    $facturasAsientoIndep = [];
                    foreach ($data as $item) {
                        $tipodoc = TipoDocumento::findOne(['id' => $item['tipo_documento_id']]);
                        if (!isset($tipodoc)) continue;
                        if ($tipodoc->asiento_independiente == 'no' && $tipodoc->tipo_documento_set_id != $tipoDocSetNotaCredito->id)
                            $facturasNormales[] = $item;
                        else
                            $facturasAsientoIndep[] = $item;
                    }

                    /* $data es usado para generar asientos: estan agrupadas por factura y por cuentas contable, sumarizadas por éstas. */
                    /* $facturas es usado para actualizar el campo asiento_id con el id del asiento creado. */
                    /* por ende, ambos objetos deben poseer las mismas condiciones de consulta. */
                    $facturas = [];
                    foreach ($facturasNormales as $element) {
                        if (!in_array($element['id_venta'], $facturas)) $facturas[] = $element['id_venta'];
                    }

                    $result = $this->generarAsientosVenta($facturasNormales); // retorna 2 arrays, 4 variables para debe y haber de venta y costo de venta.
                    $asientosIndependientes = $this->generarAsientosVentaIndependientes($facturasAsientoIndep, $fechaAsiento);

                    $asientos_detalle = $result[0];
                    $asientos_costo_detalle = $result[1];
                    $asiento->monto_debe = $result[2];
                    $asiento->monto_haber = $result[3];

                    /** @var Asiento|null $asiento_costo */
                    $asiento_costo = null;
                    /* Revision 22 noviembre 18: no quieren asiento por costo de venta. */
//                    if (sizeof($asientos_costo_detalle) > 0) {
//                        $asiento_costo = new Asiento();
//                        $asiento_costo->empresa_id = \Yii::$app->session->get('core_empresa_actual');
//                        $asiento_costo->periodo_contable_id = $periodo_actual->id;
//                        $asiento_costo->fecha = date('Y-m-d', time());
//                        $asiento_costo->concepto = 'Asiento de costo de venta.';
//                        $asiento_costo->usuario_id = Yii::$app->user->id;
//                        $asiento_costo->creado = date('Y-m-d H:i:s');
//                        $asiento_costo->modulo_origen = 'contabilidad'; // TODO: ver si es necesario/correcto/mejorable
//                        $asiento_costo->monto_debe = $result[4];
//                        $asiento_costo->monto_haber = $result[5];
//                        $asiento_costo->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
//                    }

                    if (isset($asientos_detalle) && !empty($asientos_detalle)) {
                        if (!$asiento->save()) {  // generar id
                            throw new \Exception('Error guardando asiento.');
                        };
                        $asiento->refresh();

                        foreach ($asientos_detalle as $item) {
                            $item->asiento_id = $asiento->id; // asociar id del asiento a los asiento_detalles.
                            $item->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            if (!$item->save(false)) {
                                throw new \Exception('Error validando asiento_detalles');
                            }
                        }
                        $asientos_generados = $asientos_generados || true;
                    }

//                    if ($asiento_costo != null) {
//                        $asiento_costo->asiento_id = $asiento->id; // asociar el asiento de costo al asiento normal
//                        if (!$asiento_costo->save()) { // generar id
//                            throw new \Exception('Error al guardar asiento por costo de venta: ' . $asiento_costo->getErrorSummaryAsString() . $asiento_costo->monto_haber);
//                        };
//                        $asiento_costo->refresh();
//                        foreach ($asientos_costo_detalle as $item) {
//                            $item->asiento_id = $asiento_costo->id; // asociar el id del asiento_costo a los asiento_costo_detalles
//                            $item->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
//                            if (!$item->save(false)) {
//                                throw new \Exception('Error validando asiento_costo_detalles');
//                            }
//                        }
//                    }

                    foreach ($facturas as $factura_id) {
                        $factura = Venta::findOne(['id' => $factura_id]);
                        $factura->asiento_id = $asiento->id;
                        if (!$factura->save(false))
                            throw new \Exception("Error asociando asiento con la factura: {$factura->getErrorSummaryAsString()}");
                    }

                    $asiento_recibo = generarAsientoRecibo($model, $operacion);
                    if (isset($asiento_recibo)) $asientos_generados = $asientos_generados || true;

                    if ($tipo_operacion == 'generar_guardar') {

                        #Guardar asientos independientes
                        $this->guardarAsientosIndependientes($asientosIndependientes, $operacion, $asientos_generados);

                        if ($asientos_generados) {
                            $t->commit();
                            FlashMessageHelpsers::createSuccessMessage('Los asientos se han generado y guardado exitosamente.');
                            return $this->redirect(['index']);
                        } else {
                            $t->rollBack();
                        }
                    } else {
                        $t->rollBack();
                    }
                    Yii::$app->session->set('cont_asiento_from_factura_venta_detalle-provider', new ArrayDataProvider([
                        'allModels' => $asientos_detalle,
                        'pagination' => false,
                    ]));
                    Yii::$app->session->set('cont_asiento_from_factura_costo_venta_detalle-provider', new ArrayDataProvider([
                        'allModels' => $asientos_costo_detalle,
                        'pagination' => false,
                    ]));
                }
            } elseif ($operacion == 'compra') {
                $asientos_generados = false; // por el momento solo para compra

                Yii::$app->session->set('cont_asiento_from_factura_compra_detalle-provider', new ArrayDataProvider([
                    'allModels' => [],
                    'pagination' => false,
                ]));
                Yii::$app->session->set('cont_asiento_from_factura_devengamientos-provider', new ArrayDataProvider([
                    'allModels' => [],
                    'pagination' => false,
                ]));

                if ($model->load(Yii::$app->request->post())) {
                    /** @var AsientoDetalle[] $asientos_detalle */

                    $fecha = explode(' - ', $model->fecha_rango);

                    if ($fecha[0] == '') { // explode siempre retorna un array, al menos de 1 solo elemento con value == ''
                        throw new \Exception('Debe especificar correctamente el rango de fecha.');
                    }

                    $desde = date_create_from_format('d-m-Y', $fecha[0]);
                    $hasta = date_create_from_format('d-m-Y', $fecha[1]);
                    $periodo_actual = EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')]);

                    if (!$desde) {
                        throw new \Exception('Formato de la fecha Desde incorrecto.');
                    } elseif (!$hasta) {
                        throw new \Exception('Formato de la feha Hasta incorrecto');
                    } elseif ($model->descripcion == '') {
                        throw new \Exception('Falta indicar el concepto del asiento.');
                    }

                    $empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
                    $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_seguros_pagados_id";
                    $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);

                    if ($model->devengamientos && !isset($parametro_sistema)) {
                        throw new \Exception('Error Interno: Falta especificar en el parámetro del módulo, el id de la cuenta para SEGUROS PAGADOS.');
                    }

                    $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                    $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                    $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                    $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                    if (!isset($parmsys_debe)) {
                        throw new \Exception("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Ganancia.");
                    }
                    if (!isset($parmsys_haber)) {
                        throw new \Exception("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Pérdida.");
                    }

                    $fechaAsiento = date('Y-m-t', strtotime($hasta->format('Y-m-d')));;
                    $asiento = new Asiento();
                    $asiento->empresa_id = \Yii::$app->session->get('core_empresa_actual');
                    $asiento->periodo_contable_id = $periodo_actual->id;
                    $asiento->fecha = $fechaAsiento;
                    $asiento->concepto = $model->descripcion;
                    $asiento->usuario_id = Yii::$app->user->id;
                    $asiento->creado = date('Y-m-d H:i:s');
                    $asiento->modulo_origen = 'contabilidad'; // TODO: ver si es necesario/correcto/mejorable

                    // IvasCuentasUsadas con plantillas para importacion.
                    // Suponga que algunas facturas registraron mas de un ivaCtaUsada;
                    // si N (N>0) ivasCtasUsadas de una factura fueron por plantillas de imporacion,
                    // esa factura debe excluirse de las que van a aparecer en el libro de Reporte de Compra.
                    // En $idsFactImport pueden encontrarse ids de facturas que no corresponden a este periodo o empresa;
                    // pero este hecho no afecta en el resultado de la consulta para traer las facturas a mostrar en el
                    // reporte ya que estos ids son ids a excluirse.
                    $ivasCtasImport = CompraIvaCuentaUsada::find()->alias('ivaCta')
                        ->leftJoin('cont_plantilla_compraventa as plantilla', 'ivaCta.plantilla_id = plantilla.id')
                        ->where(['!=', 'plantilla.para_importacion', 'no'])->all();
                    $idsFactImport = [];
                    /** @var CompraIvaCuentaUsada $ivaCta */
                    foreach ($ivasCtasImport as $ivaCta) {
                        if (!in_array($ivaCta->factura_compra_id, $idsFactImport))
                            $idsFactImport[] = $ivaCta->factura_compra_id;
                    }

                    $co = Compra::tableName() . '.';
                    $de = DetalleCompra::tableName() . '.';
                    $pc = PlanCuenta::tableName() . '.';
                    $tipoDocSetNotaCredito = Compra::getTipodocSetNotaCredito();
                    $query = new Query();
                    $query->select([
                        'id_compra' => 'MIN(' . $co . 'id)',
                        'nro_factura' => "MIN({$co}nro_factura)",
                        'tipo_documento_id' => "MIN({$co}tipo_documento_id)",
                        'id_moneda' => 'MIN(' . $co . 'moneda_id)',
                        'cotizacion' => 'MIN(' . $co . 'cotizacion)',
                        'fecha_de_emision' => 'MIN(' . $co . 'fecha_emision)',
                        'estado' => 'MIN(' . $co . 'estado)',
                        'subtotal' => 'SUM(' . $de . 'subtotal)',
                        'saldo' => 'MIN(' . $de . 'cta_contable)',
                        'saldo2' => 'MAX(' . $de . 'cta_contable)',
                        'id_cta' => 'MIN(' . $de . 'plan_cuenta_id)',
                        'nombre_cta' => 'MIN(' . $pc . 'nombre)',
                        'cta_codigo' => 'MIN(' . $pc . 'cod_completo)',
                    ])->from(Compra::tableName());
                    $query->leftJoin(DetalleCompra::tableName(), $de . 'factura_compra_id = ' . $co . 'id')
                        ->leftJoin(PlanCuenta::tableName(), $de . 'plan_cuenta_id = ' . $pc . 'id')
                        ->leftJoin('cont_tipo_documento td', 'td.id = cont_factura_compra.tipo_documento_id')
                        ->leftJoin('cont_tipo_documento_set tds', 'tds.id = td.tipo_documento_set_id');
                    $query->where([$co . 'empresa_id' => \Yii::$app->session->get('core_empresa_actual'), $co . 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])
                        ->andWhere(['BETWEEN', $co . 'fecha_emision', $desde->format('Y-m-d'), $hasta->format('Y-m-d')])
                        ->andWhere(['NOT IN', $co . 'estado', ['anulada', 'faltante']])
                        ->andWhere([$co . 'asiento_id' => null])
                        ->andWhere(['NOT IN', $co . 'id', $idsFactImport])
                        ->andWhere(['IS', $de . 'prestamo_cuota_id', null]);
                    $query->groupBy([$co . 'id', $pc . 'id']); // para agrupar detalles por factura y sumarizar c/detalle por cuentas
                    $command = $query->createCommand();
                    $data = $command->queryAll();

                    #Debug
//                    $myfile = fopen("/var/www/html/core/backend/web/uploads/asiento.csv", "a") or die("Unable to open file!");
//                    foreach ($data as $item) {
//                        $elements = [];
//                        foreach ($item as $key => $value) {
//                            $elements[] = $value;
//                        }
//                        fwrite($myfile, implode(';', $elements) . PHP_EOL);
//                    }
//                    fclose($myfile);
                    #Fin debug

                    #Separar facturas con tipo de documento marcado para asentar independientemente.
                    $facturasNormales = [];
                    $facturasAsientoIndep = [];
                    foreach ($data as $item) {
                        $tipodoc = TipoDocumento::findOne(['id' => $item['tipo_documento_id']]);
                        if (!isset($tipodoc)) continue;
                        if ($tipodoc->asiento_independiente == 'no' && $tipodoc->tipo_documento_set_id != $tipoDocSetNotaCredito->id)
                            $facturasNormales[] = $item;
                        else
                            $facturasAsientoIndep[] = $item;
                    }

                    /* $data es usado para generar asientos: estan agrupadas por factura y por cuentas contable, sumarizadas por éstas. */
                    /* $facturas es usado para actualizar el campo asiento_id con el id del asiento creado. */
                    /* por ende, ambos objetos deben poseer las mismas condiciones de consulta. */
                    $facturas = [];
                    foreach ($facturasNormales as $element) {
                        if (!in_array($element['id_compra'], $facturas)) $facturas[] = $element['id_compra'];
                    }

                    $result = $this->generarAsientosCompra($facturasNormales); // retorna 1 arrays, 2 variables para debe y haber.
                    $asientosIndependientes = $this->generarAsientosCompraIndependientes($facturasAsientoIndep, $fechaAsiento);

                    // TODO: desde aqui cambia
                    // TODO: no tiene asiento de costo
                    $asientos_detalle = $result[0];
                    $asiento->monto_debe = $result[1];
                    $asiento->monto_haber = $result[2];

	                if (sizeof($asientos_detalle) > 0 && $asiento->save(false)) {
                        foreach ($asientos_detalle as $item) {
	                        $item->asiento_id = $asiento->id; // asociar id del asiento a los asiento_detalles.
	                        $item->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
	                        if (!$item->save(false)) {
		                        throw new \Exception('Error validando asiento_detalles');
	                        }
                        }

                        foreach ($facturas as $factura_id) {
                            $factura = Compra::findOne(['id' => $factura_id]);
                            $factura->asiento_id = $asiento->id;
                            if (!$factura->save(false))
                                throw new \Exception("Error asociando asiento con la factura: {$factura->getErrorSummaryAsString()}");
                        }
                        $asientos_generados = true;
                    }

                    // Manejar devengamientos de seguros
                    $devengamientos = null;
                    if ($model->devengamientos == 1)
                        $devengamientos = $this->generarAsientosDevengamientos($desde, $hasta);
                    else {
                        Yii::$app->session->remove('cont_asiento_from_factura_devengamientos-provider');
                    }

                    if (isset($devengamientos)) {
                        $asiento_dev = new Asiento();
                        $asiento_dev->empresa_id = \Yii::$app->session->get('core_empresa_actual');
                        $asiento_dev->fecha = date('Y-m-t', strtotime($hasta->format('Y-m-d')));
                        $asiento_dev->concepto = $model->concepto_devengamiento;
                        $asiento_dev->usuario_id = Yii::$app->user->id;
                        $asiento_dev->creado = date('Y-m-d H:i:s');
                        $asiento_dev->modulo_origen = 'contabilidad'; // TODO: ver si es necesario/correcto/mejorable
                        $asiento_dev->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
                        $asiento_dev->monto_debe = $devengamientos[0][0]->monto_debe;
                        $asiento_dev->monto_haber = $devengamientos[0][1]->monto_haber;

                        if (sizeof($devengamientos[0]) > 0) {
                            $asiento_dev->save(false);
                            foreach ($devengamientos[0] as $asiento_d) {
                                $asiento_d->asiento_id = $asiento_dev->id;
                                $asiento_d->save(false);
                            }
                            foreach ($devengamientos[1] as $id_factura) {
                                $compra = Compra::findOne(['id' => $id_factura]);
                                $compra->asiento_id = $asiento_dev->id;
                                $compra->save(false);
                            }
                            foreach ($devengamientos[2] as $id_dev) {
                                $dev = PolizaDevengamiento::findOne(['id' => $id_dev]);
                                $dev->asiento_id = $asiento_dev->id;
                                $dev->save(false);
                            }
                            $asientos_generados = ($asientos_generados || true);
                        }

                        Yii::$app->session->set('cont_asiento_from_factura_devengamientos-provider', new ArrayDataProvider([
                            'allModels' => $devengamientos[0],
                            'pagination' => false,
                        ]));
                    }

                    Yii::$app->session->set('cont_asiento_from_factura_compra_detalle-provider', new ArrayDataProvider([
                        'allModels' => $asientos_detalle,
                        'pagination' => false,
                    ]));

                    // Manejar asiento por prestamos sin facturas
                    if ($model->prestamo == 1) {
                        $prestamos = Prestamo::getPrestamosNoFacturados($desde->format('Y-m-d'), $hasta->format('Y-m-d'));

                        $detalles_for_sim = [];
                        $debe_for_sim = 0.0;
                        $haber_for_sim = 0.0;

                        foreach ($prestamos as $prestamo) {
                            /** @var AsientoDetalle[] $detalles */
                            $detalles = [];
                            $monto_debe = 0.0;
                            $monto_haber = 0.0;

                            $_asiento_prestamo = new Asiento();
                            $_asiento_prestamo->empresa_id = \Yii::$app->session->get('core_empresa_actual');
                            $_asiento_prestamo->fecha = date('Y-m-t', strtotime($hasta->format('Y-m-d')));
                            $_asiento_prestamo->usuario_id = Yii::$app->user->id;
                            $_asiento_prestamo->creado = date('Y-m-d H:i:s');
                            $_asiento_prestamo->modulo_origen = 'contabilidad'; // TODO: ver si es necesario/correcto/mejorable
                            $_asiento_prestamo->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $_asiento_prestamo->concepto = $model->prestamo_concepto;

                            $gravado = 0;
                            $deducir = 0;
                            if ($prestamo->incluye_iva == 'si') {
                                $gravado = round((float)$prestamo->gastos_bancarios / 1.1)
                                    + round((float)$prestamo->intereses_vencer / 1.1);
                                $deducir = (int)$prestamo->gastos_bancarios + (int)$prestamo->intereses_vencer - $gravado;
                            } else {
                                $deducir = round((float)$prestamo->gastos_bancarios * 1.1) - (float)$prestamo->gastos_bancarios
                                    + round((float)$prestamo->intereses_vencer * 1.1) - (float)$prestamo->intereses_vencer;
                            }
                            $iva_interes = ($prestamo->incluye_iva == 'si') ? ((float)$prestamo->intereses_vencer / 11) : ((float)$prestamo->intereses_vencer / 10);
                            $iva_interes = round($iva_interes);

                            if ($prestamo->usar_iva_10 == 'si') {
                                foreach ($prestamo->getCuentasFields() as $atributo) {
                                    $concepto = str_replace('cuenta_', '', $atributo);

                                    if ($atributo == 'intereses_pagados') continue;

                                    $detalle = new AsientoDetalle();
                                    $detalle->cuenta_id = $prestamo->$atributo;
                                    $add = false;

                                    if (in_array($concepto, ['intereses_vencer', 'gastos_bancarios'])) {
                                        $detalle->monto_debe = ($concepto == 'intereses_vencer') ? (($prestamo->incluye_iva == 'si') ? ($prestamo->$concepto / 1.1) : ($prestamo->$concepto)) : ($prestamo->$concepto / 1.1);
                                        $add = true;
                                    } else {
                                        if ($concepto == 'caja' || $concepto == 'iva_10') {
                                            $detalle->monto_debe = $prestamo->$concepto;
                                            $add = true;
                                        } elseif ($concepto == 'monto_operacion') {
                                            $detalle->monto_haber = (($prestamo->iva_interes_cobrado == 'si') ? ($prestamo->$concepto) : ((int)$prestamo->$concepto + $iva_interes));
                                            $add = true;
                                        }
                                    }
                                    if ($add) {
                                        $detalles[] = $detalle;
                                        $monto_debe += (isset($detalle->monto_debe) ? $detalle->monto_debe : 0);
                                        $monto_haber += (isset($detalle->monto_haber) ? $detalle->monto_haber : 0);
                                    }
                                }

                            } else {
                                /** @var ParametroSistema $parametro */
                                foreach ($prestamo->getCuentasFields() as $atributo) {
                                    $concepto = str_replace('cuenta_', '', $atributo);

                                    if ($atributo == 'intereses_pagados') continue;

                                    $detalle = new AsientoDetalle();
                                    $detalle->cuenta_id = $prestamo->$atributo;
                                    $add = false;
                                    if (in_array($concepto, ['intereses_vencer', 'gastos_bancarios'])) {
                                        $detalle->monto_debe = ($concepto == 'intereses_vencer') ? (($prestamo->incluye_iva == 'si') ? round($prestamo->$concepto / 1.1) : ($prestamo->$concepto)) : round($prestamo->$concepto / 1.1);
                                        $add = true;
                                    } else {
                                        if ($concepto == 'caja') {
                                            $detalle->monto_debe = $prestamo->$concepto;
                                            $add = true;
                                        } elseif ($concepto == 'monto_operacion') {
                                            $detalle->monto_haber = (($prestamo->iva_interes_cobrado == 'si') ? ($prestamo->$concepto) : ((int)$prestamo->$concepto + $iva_interes));
                                            $add = true;
                                        } elseif ($concepto == 'gastos_no_deducibles') {
                                            $detalle->monto_debe = $deducir;
                                            $add = true;
                                        }
                                    }
                                    if ($add) {
                                        $detalles[] = $detalle;
                                        $monto_debe += (isset($detalle->monto_debe) ? $detalle->monto_debe : 0);
                                        $monto_haber += (isset($detalle->monto_haber) ? $detalle->monto_haber : 0);
                                    }
                                }
                            }

                            $_asiento_prestamo->monto_haber = $monto_haber;
                            $_asiento_prestamo->monto_debe = $monto_debe;

                            if (sizeof($detalles) > 0) {
                                $_asiento_prestamo->save(false);
                                $_asiento_prestamo->refresh();

                                foreach ($detalles as $detalle) {
                                    $detalle->asiento_id = $_asiento_prestamo->id;
                                    $detalle->save(false);
                                    $detalle->refresh();
                                }
                                $detalles_for_sim = array_merge($detalles_for_sim, $detalles);
                                $debe_for_sim += $monto_debe;
                                $haber_for_sim += $monto_haber;

                                $prestamo->asiento_id = $_asiento_prestamo->id;
                                $prestamo->save(false);
                                $prestamo->refresh();

                                $asientos_generados = ($asientos_generados || true);
                            }
                        }

                        // Agrupar y sumar detalles iguales.
                        $detalles_for_sim = AsientoDetalle::agruparDetallesIguales($detalles_for_sim);

                        // Agrupar junto asientos de partida debe / haber
                        $detalles_for_sim = AsientoDetalle::agruparDebeHaberAsientos($detalles_for_sim);

                        Yii::$app->session->set('cont_asiento_from_factura_prestamo-provider', [
                            'dataProvider' => new ArrayDataProvider([
                                'allModels' => $detalles_for_sim,
                                'pagination' => false,
                            ]),
                            'debe' => $debe_for_sim,
                            'haber' => $haber_for_sim
                        ]);
                    } else {
                        Yii::$app->session->remove('cont_asiento_from_factura_prestamo-provider');
                    }

	                # Recibos
                    $asiento_recibo = generarAsientoRecibo($model, $operacion);
	                $asientos_generados = $asientos_generados || isset($asiento_recibo);

                    // TODO: este if debe estar dentro de cada if (operacion == venta/compra)
                    if ($tipo_operacion == 'generar_guardar') {
                        if ($model->devengamientos && $model->concepto_devengamiento == "") {
                            throw new \Exception("Debe especificar concepto del asiento para devengamientos de seguros.");
                        } elseif ($model->prestamo && $model->prestamo_concepto == "") {
                            throw new \Exception("Debe especificar concepto del asiento para préstamo.");
                        } elseif ($model->devengamientos2 && $model->concepto_devengamiento2 == "") {
                            //throw new \Exception("Debe especificar concepto del asiento para devengamientos de préstamos por recibo.");
                        }

                        #Guardar asientos independientes.
                        $this->guardarAsientosIndependientes($asientosIndependientes, $operacion, $asientos_generados);

                        if ($asientos_generados) {
                            $t->commit();
                            FlashMessageHelpsers::createSuccessMessage('Los asientos se han generado y guardado exitosamente.');
	                        foreach ($asientos_detalle as $item) {
		                        echo json_encode($item->attributes);
		                        echo '<br/>';
		                        echo '<br/>';
	                        }
	                        exit();
                        } else {
                            $t->rollBack();
                            FlashMessageHelpsers::createInfoMessage("No se generaron asientos.");
                        }
                        return $this->redirect(['index']);
                    } else {
                        $t->rollBack();
                    }
                }
            }

        } catch (\Exception $exception) {
            //throw $exception;
            $t->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 13000);
        }

        retorno:;
        $content = null;
        if (sizeof($result) > 3) {
            $content = [
                'model' => $model,
                'totaldebe' => $result[2],
                'totalhaber' => $result[3],
                'totalCdebe' => $result[4],
                'totalChaber' => $result[5],
                'asientoIndep' => $asientosIndependientes,
            ];
        } elseif (sizeof($result) > 0) {
            $content = [
                'model' => $model,
                'totaldebe' => $result[1],
                'totalhaber' => $result[2],
                'totalCdebe' => '',
                'totalChaber' => '',
                'asientoIndep' => $asientosIndependientes,
            ];
        } else $content = [
            'model' => $model,
            'totaldebe' => '',
            'totalhaber' => '',
            'totalCdebe' => '',
            'totalChaber' => '',
            'asientoIndep' => $asientosIndependientes,
        ];
        $content['asientoRecibo'] = $asiento_recibo;

        return $this->render('/asiento-from-factura/create', $content);
    }

    /**
     * @param string $tipo_operacion
     *
     * @return string
     * @throws Exception
     */

    public function actionCreateFromRrhh($tipo_operacion = 'vista_previa')
    {
        Yii::$app->session->set('cont_asiento_from_rrhh-provider', new ArrayDataProvider([
            'allModels' => [],
            'pagination' => false,
        ]));

        $model = new AsientoDeSueldosForm;
        $result = [];

        if ($model->load(Yii::$app->request->post())) {
            $periodo_actual = EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
            $asiento = new Asiento();
            $asiento->empresa_id = \Yii::$app->session->get('core_empresa_actual');
            $asiento->periodo_contable_id = $periodo_actual->id;
            $asiento->fecha = date('Y-m-d', time());
            $asiento->concepto = $model->concepto_salida;
            $asiento->usuario_id = Yii::$app->user->id;
            $asiento->creado = date('Y-m-d H:i:s');
            $asiento->modulo_origen = 'rrhh';
            $asiento->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');

            if ($model->sucursal_id != null && $model->sucursal_id != '')
                $asiento->ref_esquema_bd = 'empresaid_' . $model->empresa_id . '_sucursalid_' . $model->sucursal_id . '_fecha_' . DateTime::createFromFormat('d-m-Y', '01-' . $model->mes_anho)->format('Ym');
            else
                $asiento->ref_esquema_bd = 'empresaid_' . $model->empresa_id . '_fecha_' . DateTime::createFromFormat('d-m-Y', '01-' . $model->mes_anho)->format('Ym');
            #obtener registros de RH
            try {
                $desde = date('Y-m-d', strtotime('01-' . $model->mes_anho));
                $hasta = date('Y-m-t', strtotime('01-' . $model->mes_anho));
                $sucursal_id = $model->sucursal_id;
                $empresa_id = $model->empresa_id;

                //Verificar que no se haya generado antes un asiento con los mismos parametros
                $asiento_ = Asiento::find()
                    ->where(['ref_esquema_bd' => $asiento->ref_esquema_bd])
                    ->one();
                if ($asiento_ != null) {
                    FlashMessageHelpsers::createWarningMessage('Ya existe un asiento generado para la misma fecha, empresa.');
                    goto retorno;
                }

                $connection = Yii::$app->db_rrhh;
                //Primero ver si existe planilla para mes, año, empresa que esté cerrada.
                $sql =
                    "
                    select count(cerrado) count
                    from rrhh_planilla_pagos_mensual
                      left join rrhh_empresa_sucursal s on rrhh_planilla_pagos_mensual.sucursal_id_fk = s.id
                    where (fecha >= '{$desde}' AND fecha <= '{$hasta}') and s.empresa_id_fk = $empresa_id AND 
                        cerrado = 1;   
                    ";

                /** @var Command $commandCount */
                $commandCount = $connection->createCommand($sql);
                if ($commandCount->queryAll()[0]['count'] == 0) {
                    FlashMessageHelpsers::createWarningMessage('No existen datos a recuperar.');
                    goto retorno;
                }

                //Tipos movimientos e ids
                $sql = 'select trim(nombre) nombre, valor
                        from parametro_sistema
                        where nombre like "tipo-mov-%";';
                /** @var Command $commandTipoMovsNombre */
                $commandTipoMovsNombre = $connection->createCommand($sql);
                $tipoMovsNombre = $commandTipoMovsNombre->queryAll();

                $arrayTipoMovNombreId = [];

                //Se construye lista de parametros de cuentas contables para cada tipo mov.
                foreach ($tipoMovsNombre as $tipoMovNombre) {
                    $nombresParam[] = "empresa_" . $empresa_id . "_cuenta-para_" . $tipoMovNombre['nombre'];
                    $arrayTipoMovNombreId[$tipoMovNombre['nombre']] = $tipoMovNombre['valor'];
                }
                $nombresParamString = implode('","', $nombresParam);

                $sql = 'select * from parametro_sistema where nombre in ("' . $nombresParamString . '");';
                /** @var Command $commandParamSistema */
                $commandParamSistema = $connection->createCommand($sql);
                $paramsSistema = $commandParamSistema->queryAll();

                $resultArray = [];
                //$paramsSistema contiene los tipo de movimientos configurados con cuentas contables.
                //Se optienen la suma de los movimientos por tipo mov según parametros configurados.
                foreach ($paramsSistema as $tipoMovCuenta) {
                    $idTipoMov = $arrayTipoMovNombreId[str_replace('empresa_' . $empresa_id . '_cuenta-para_', '', $tipoMovCuenta['nombre'])];
                    $cuentaTipoMov = $tipoMovCuenta['valor'];

                    $planCuenta = PlanCuenta::find()->where(['cod_completo' => $cuentaTipoMov])->one();
                    if ($planCuenta != null) {
                        if ($sucursal_id == '')
                            $sql =
                                "
                                    select
                                      sum(mov.valor) valor
                                    from rrhh_movimiento as mov
                                      left join rrhh_empleado empleado on mov.empleado_id_fk = empleado.id
                                    where empleado.rrhh_empresa_id = $empresa_id and fecha <= '{$hasta}' and fecha >= '{$desde}'
                                        and tipo_movimiento_id_fk = $idTipoMov
                                    group by tipo_movimiento_id_fk; 
                                ";
                        else
                            $sql =
                                "
                                    select
                                      sum(mov.valor) valor
                                    from rrhh_movimiento as mov
                                      left join rrhh_empleado empleado on mov.empleado_id_fk = empleado.id
                                    where empleado.rrhh_empresa_sucursal_id = $sucursal_id and fecha <= '{$hasta}' and fecha >= '{$desde}'
                                        and tipo_movimiento_id_fk = $idTipoMov
                                    group by tipo_movimiento_id_fk; 
                                ";

                        /** @var Command $command */
                        $command = $connection->createCommand($sql);
                        $sum = $command->queryAll();
                        if (!empty($sum)) {
                            $resultArray[$idTipoMov] = [$sum[0]['valor'], $planCuenta->id];
                        }
                    }
                }

                //Salario E IPS
                $sql = 'select valor from parametro_sistema where nombre = "empresa_' . $empresa_id . '_cuenta-para_ips";';
                $paramsSistemaIPS = $connection->createCommand($sql)->queryAll();

                $sql = 'select valor from parametro_sistema where nombre = "empresa_' . $empresa_id . '_cuenta-para_salario";';
                $paramsSistemaSalario = $connection->createCommand($sql)->queryAll();

                if (!empty($paramsSistemaIPS) && !empty($paramsSistemaSalario)) {
                    $planCuentaIps = PlanCuenta::find()->where(['cod_completo' => $paramsSistemaIPS[0]['valor']])->one();
                    $planCuentaSalario = PlanCuenta::find()->where(['cod_completo' => $paramsSistemaSalario[0]['valor']])->one();

                    if ($sucursal_id != '')
                        $sql =
                            "SELECT sum(ips) as ips_total, sum(bruto) as bruto_total
                                FROM rrhh_planilla_pagos_mensual
                                WHERE sucursal_id_fk = $sucursal_id AND 
                                    (fecha >= '{$desde}' AND fecha <= '{$hasta}') AND 
                                    cerrado = 1;   
                            ";
                    else
                        $sql =
                            "SELECT sum(ips) as ips_total, sum(bruto) as bruto_total
                                FROM rrhh_planilla_pagos_mensual
                            left join rrhh_empresa_sucursal s on rrhh_planilla_pagos_mensual.sucursal_id_fk = s.id
                            WHERE s.empresa_id_fk = $empresa_id AND (fecha >= '{$desde}' AND fecha <= '{$hasta}') AND 
                                    cerrado = 1;   
                            ";
                    /** @var Command $command */
                    $command = $connection->createCommand($sql);
                    $sum = $command->queryAll();

                    if (!empty($sum) && $planCuentaIps != null && $planCuentaSalario != null) {
                        $resultArray[] = [$sum[0]['ips_total'], $planCuentaIps->id];
                        $resultArray[] = [$sum[0]['bruto_total'], $planCuentaSalario->id];
                    }
                }

                $result = $this->generarAsientosSalario($model->cuenta_salida_id, $resultArray);
            } catch (\Exception $e) {
                FlashMessageHelpsers::createWarningMessage('No se pudo conectar a RRHH, verifique conf bd.' . $e->getMessage());
                goto retorno;
            }

            $asientos_detalle = $result[0];
            $asiento->monto_debe = $result[1];
            $asiento->monto_haber = $result[2];

            Yii::$app->session->set('cont_asiento_from_rrhh-provider', new ArrayDataProvider([
                'allModels' => $result[0],
                'pagination' => false,
            ]));

            $t = Yii::$app->db->beginTransaction();

            if (sizeof($asientos_detalle) < 2) {
                FlashMessageHelpsers::createWarningMessage('No se pudieron obtener datos del sistema RRHH para la fecha indicada.');
                $t->rollBack();
                goto retorno;
            }

            $asiento->save(false); // generar id.
            /** @var AsientoDetalle $item */
            foreach ($asientos_detalle as $item) {
                $item->asiento_id = $asiento->id; // asociar id del asiento a los asiento_detalles.
                $item->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
                if (!$item->save(false)) {
                    FlashMessageHelpsers::createWarningMessage('Error validando asiento_detalles');
                    $t->rollBack();
                    goto retorno;
                }
            }

            if ($tipo_operacion == 'generar_guardar') {
                $t->commit();
                FlashMessageHelpsers::createSuccessMessage('Los asientos se han generado y guardado exitosamente.');
                // resetear detalles
                $asientos_detalle = [];
                return $this->redirect(['index']);
            }
        }

        retorno:;
        $content = null;
        if (sizeof($result) > 0) {
            $content = [
                'model' => $model,
                'totaldebe' => $result[1],
                'totalhaber' => $result[2],
            ];
        } else {
            $content = [
                'model' => $model,
                'totaldebe' => '',
                'totalhaber' => '',
            ];
        }

        return $this->render('/asiento-from-rrhh/create', $content);
    }

    public function actionSucursal()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $empresa_id = $parents[0];

                $out = AsientoDeSueldosForm::getSucursalList($empresa_id, true);

                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]

                return (['output' => $out, 'selected' => '']);
            }
        }
        return (['output' => '', 'selected' => '']);
    }

    /**
     * @param $desde DateTime
     * @param $hasta DateTime
     *
     * @return array 1º: Array de asientos. 2º: Id's de facturas. 3º: Id's de cont_poliza_devengamientos.
     * @throws Exception
     */
    private function generarAsientosDevengamientos($desde, $hasta)
    {
        $asientos = [];

        $query = PolizaDevengamiento::find()->alias('devengamientos');
        $query->joinWith('poliza as poliza');
        $query->where(['BETWEEN', 'devengamientos.anho_mes', $desde->format('Y-m'), $hasta->format('Y-m')])
            ->andWhere([
                'devengamientos.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
                'poliza.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            ])
            ->andWhere(['IS', 'devengamientos.asiento_id', null]);

        // Al realizar consultas cuyas columnas resultantes son todas funciones, el resultado si o si tiene al menos una fila.
        // En caso de que no exista ningun dato que satisfaga las condiciones de la consulta, la misma retorna 1 fila con campos NULL.
        // Por eso se hace primero $query->exists() y luego la verdadera consulta deseada.
        if ($query->exists()) {
            $query = PolizaDevengamiento::find()->alias('devengamientos');
            $query->joinWith('poliza as poliza');
            $query->select([
                'MIN(devengamientos.id) as id',
                'MIN(devengamientos.poliza_id) as poliza_id',
                'MIN(devengamientos.anho_mes) as anho_mes',
                'ROUND(SUM(devengamientos.monto),0) as monto',
            ]);
            $query->where(['BETWEEN', 'devengamientos.anho_mes', $desde->format('Y-m'), $hasta->format('Y-m')])
                ->andWhere([
                    'devengamientos.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
                    'poliza.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                ])
                ->andWhere(['IS', 'devengamientos.asiento_id', null]);
            $query = $query->asArray()->all(); // devengamientos sumarizados

            $query2 = new Query();
            $query2->select([
                'cont_poliza.factura_compra_id'
            ])->distinct()->from(Poliza::tableName());
            $query2->leftJoin('cont_poliza_devengamiento', 'cont_poliza_devengamiento.poliza_id = cont_poliza.id');
            $query2->where(['BETWEEN', 'cont_poliza_devengamiento.anho_mes', $desde->format('Y-m'), $hasta->format('Y-m')])
                ->andWhere([
                    'cont_poliza_devengamiento.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
                    'cont_poliza_devengamiento.asiento_id' => null,
                    'cont_poliza.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                ]);
            $command = $query2->createCommand();
            $id_facturas = $command->queryAll(); // id de facturas de polizas correspondientes

            $query3 = PolizaDevengamiento::find()->alias('devengamientos');
            $query3->select([
                'id',
            ]);
            $query3->where(['BETWEEN', 'devengamientos.anho_mes', $desde->format('Y-m'), $hasta->format('Y-m')])
                ->andWhere(['devengamientos.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'), 'devengamientos.asiento_id' => null]);
            $id_devengamientos = $query3->asArray()->all(); // id de devengamientos

            $empresa_id = Yii::$app->session->get('core_empresa_actual');
            $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_seguros_pagados_id";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            $asiento = new AsientoDetalle();
            $asiento->cuenta_id = $parametro_sistema->valor; // TODO: parametro del modulo
            $asiento->monto_debe = $query[0]['monto'];
            $asiento->monto_haber = 0;
            $asiento->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $asientos[] = $asiento;

            $asiento = new AsientoDetalle();
            $asiento->cuenta_id = PlantillaCompraventa::getIdCuentaSeguroADevengar();
            $asiento->monto_debe = 0;
            $asiento->monto_haber = $query[0]['monto'];
            $asiento->periodo_contable_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $asientos[] = $asiento;
            return [$asientos, $id_facturas, $id_devengamientos];
        }

        return null;
    }

    /**
     * @param $facturas ActiveRecord[]
     *
     * @return array
     * @throws \Exception
     */
    private function generarAsientosVenta($facturas)
    {
        $asiento_detalles = [];
        $asiento_costo_detalles = [];
        $total_haber = 0.0;
        $total_debe = 0.0;
        $total_costo_haber = 0.0;
        $total_costo_debe = 0.0;
        $_costo_venta_procesado[] = ['factura_id' => '', 'plantilla_id' => ''];
        $ctaGananciaRoundingError = PlanCuenta::getCtaGananciaRoundingError();
        $ctaPerdidaRoundingError = PlanCuenta::getCtaPerdidaRoundingError();

        if (!isset($ctaGananciaRoundingError)) {
            throw new \Exception("Es necesario parametrizar en la empresa actual una cuenta para \"GANANCIA POR ERROR DE REDONDEO\".");
        } elseif (!isset($ctaPerdidaRoundingError)) {
            throw new \Exception("Es necesario parametrizar en la empresa actual una cuenta para \"GANANCIA POR ERROR DE REDONDEO\".");
        }

        $debe_por_factura = $haber_por_factura = 0;
        $factura_id = null;
        foreach ($facturas as $factura) {

            # Detalles de asiento
            $asiento_detalle = new AsientoDetalle();
            $asiento_detalle->cuenta_id = $factura['id_cta'];
            $asiento_detalle->monto_debe = $factura['saldo'] == 'debe' ? round($factura['subtotal'], 2) : 0.0;
            $asiento_detalle->monto_haber = $factura['saldo'] == 'haber' ? round($factura['subtotal'], 2) : 0.0;

            # Valorizacion
            /** @var Cotizacion $cotizacion */
            $cotizacion = $factura['id_moneda'] == 1 ? null : Cotizacion::find()
                ->where(['moneda_id' => $factura['id_moneda']])
                ->andFilterWhere(['fecha' => date('Y-m-d', strtotime($factura['fecha_de_emision'] . ' - 1 days'))])->one();

            $newDetalleDiffCambio = null;
            if ($cotizacion != null) {
                $diff = round($factura['cotizacion'], 2) - round($cotizacion->compra, 2);
                if ($diff != 0.0) {
                    $empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

                    $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                    $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                    $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                    $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                    $newDetalleDiffCambio = new AsientoDetalle();
                    $newDetalleDiffCambio->cuenta_id = PlanCuenta::findOne(['id' => $diff > 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor])->id; // TODO: parametro del modulo
                    $newDetalleDiffCambio->monto_debe = $factura['saldo'] == 'debe' ? round($diff, 2) * round($factura['subtotal'], 2) : 0.0;
                    $newDetalleDiffCambio->monto_haber = $factura['saldo'] == 'haber' ? round($diff, 2) * round($factura['subtotal'], 2) : 0.0;
                    $diff < 0.0 ? $newDetalleDiffCambio->monto_debe = (round($newDetalleDiffCambio->monto_debe, 2) * -1.0) : true;
                    $diff < 0.0 ? $newDetalleDiffCambio->monto_haber = (round($newDetalleDiffCambio->monto_haber, 2) * -1.0) : true;
                    $total_debe += round($newDetalleDiffCambio->monto_debe, 2);
                    $total_haber += round($newDetalleDiffCambio->monto_haber, 2);
                }

                $asiento_detalle->monto_debe *= $factura['id_cta'] == 16 ? round($factura['cotizacion'], 2) : round($cotizacion->compra, 2);
                $asiento_detalle->monto_haber *= round($cotizacion->compra, 2); //nunca va a haber caja en haber.
            }

            # Costo de venta.
            $iva_cta_usadas = VentaIvaCuentaUsada::findAll(['factura_venta_id' => $factura['id_venta']]);
            foreach ($iva_cta_usadas as $iva_cta_usada) {
                $plantilla = PlantillaCompraventa::findOne(['id' => $iva_cta_usada->plantilla_id]);

                // Para calcular costo de venta una sola vez por factura.
                if ($this->costoVtaFactYaGenerado($_costo_venta_procesado, $iva_cta_usada->plantilla_id, $iva_cta_usada->factura_venta_id)) {
                    continue;
                }

                // Se pregunta si esta setteado plantilla porque hay algunas ivaCuentaUsada que tienen entradas null en
                //  la columna de plantilla_id a pesar de que la venta asociada no es anulada ni faltante,
                //  debido a que son una de las ivaCtaUsadas a los que se les nullearon
                //  justamente esa columna en el momento en que se decidio implementar el MULTIPLANTILLA.
                if (isset($plantilla) && $plantilla->costo_mercad == 'si') {
                    /** @var PlantillaCompraventaDetalle[] $p_detalles_costo_merca */
                    $coef_costo = ParametroSistema::getEmpresaActualCoefCosto();
                    $p_detalles_costo_merca = $plantilla->getDetalles('tipo_asiento', 'costo_venta');
                    foreach ($p_detalles_costo_merca as $p_d) {
                        $asiento_costo_detalle = new AsientoDetalle();
                        $asiento_costo_detalle->cuenta_id = $p_d->p_c_gravada_id;
                        $asiento_costo_detalle->monto_debe = $p_d->tipo_saldo == 'debe' ? (float)$coef_costo * (float)$factura['total_factura'] : 0.0;
                        $asiento_costo_detalle->monto_haber = $p_d->tipo_saldo == 'haber' ? (float)$coef_costo * (float)$factura['total_factura'] : 0.0;
                        if ($cotizacion != null) {
                            $asiento_costo_detalle->monto_haber *= (float)$cotizacion->compra;
                            $asiento_costo_detalle->monto_debe *= (float)$cotizacion->compra;
                        }
                        $total_costo_debe = (float)$total_costo_debe + (float)$asiento_costo_detalle->monto_debe;
                        $total_costo_haber = (float)$total_costo_haber + (float)$asiento_costo_detalle->monto_haber;
                        $asiento_costo_detalles[] = $asiento_costo_detalle;
                    }
                }

                $_costo_venta_procesado[] = [
                    'factura_id' => $iva_cta_usada->factura_venta_id,
                    'plantilla_id' => $iva_cta_usada->plantilla_id
                ];
            }

            # Round to integer
            $asiento_detalle->monto_debe = round($asiento_detalle->monto_debe, 0);
            $asiento_detalle->monto_haber = round($asiento_detalle->monto_haber, 0);

            $total_debe += $asiento_detalle->monto_debe;
            $total_haber += $asiento_detalle->monto_haber;

            if (isset($factura_id) && $factura['id_venta'] != $factura_id) {
                $dif = $debe_por_factura - $haber_por_factura;
                $asiento_error_red = new AsientoDetalle();
                if ($dif > 0) {
                    $asiento_error_red->cuenta_id = $ctaGananciaRoundingError->id;
                    $asiento_error_red->monto_debe = 0;
                    $asiento_error_red->monto_haber = $dif;
                } else {
                    $dif = abs($dif);
                    $asiento_error_red->cuenta_id = $ctaPerdidaRoundingError->id;
                    $asiento_error_red->monto_debe = $dif;
                    $asiento_error_red->monto_haber = 0;
                }
                if ($dif != 0)
                    $asiento_detalles[] = $asiento_error_red;
                $debe_por_factura = $haber_por_factura = 0;
            }
            $debe_por_factura += $asiento_detalle->monto_debe;
            $haber_por_factura += $asiento_detalle->monto_haber;
            $factura_id = $factura['id_venta'];

            $asiento_detalles[] = $asiento_detalle;
            if ($newDetalleDiffCambio != null) $asiento_detalles[] = $newDetalleDiffCambio;
        }

        retorno:;
        // Agrupar y sumar detalles iguales.
        $asiento_detalles = AsientoDetalle::agruparDetallesIguales($asiento_detalles);
        $asiento_costo_detalles = AsientoDetalle::agruparDetallesIguales($asiento_costo_detalles);

        // Agrupar junto asientos de partida debe / haber
        $asiento_detalles = AsientoDetalle::agruparDebeHaberAsientos($asiento_detalles);
        $asiento_costo_detalles = AsientoDetalle::agruparDebeHaberAsientos($asiento_costo_detalles);

        return [
            $asiento_detalles, $asiento_costo_detalles,
            round($total_debe), round($total_haber),
            round($total_costo_debe), round($total_costo_haber)
        ];
    }

    /**
     * @param $_costo_venta_procesado array
     * @param $plantilla_id
     * @param $factura_id
     *
     * @return bool
     */
    private function costoVtaFactYaGenerado($_costo_venta_procesado, $plantilla_id, $factura_id)
    {
        foreach ($_costo_venta_procesado as $item) {
            if ($item['factura_id'] == $factura_id && $item['plantilla_id'] == $plantilla_id) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $facturas ActiveRecord[]
     *
     * @return array
     * @throws \Exception
     */
    private function generarAsientosCompra($facturas)
    {
        $asiento_detalles = [];
        $total_haber = 0.0;
        $total_debe = 0.0;
        $ctaGananciaRoundingError = PlanCuenta::getCtaGananciaRoundingError();
        $ctaPerdidaRoundingError = PlanCuenta::getCtaPerdidaRoundingError();

        if (!isset($ctaGananciaRoundingError)) {
            throw new \Exception("Es necesario parametrizar en la empresa actual una cuenta para \"GANANCIA POR ERROR DE REDONDEO\".");
        } elseif (!isset($ctaPerdidaRoundingError)) {
            throw new \Exception("Es necesario parametrizar en la empresa actual una cuenta para \"GANANCIA POR ERROR DE REDONDEO\".");
        }

        $debe_por_factura = $haber_por_factura = 0;
        $factura_id = null;
        foreach ($facturas as $factura) {
            /** @var Cotizacion $cotizacion */

            #Facturas de compra de moneda, usan la cuenta CAJA uno en el debe y otro en el haber. En el asiento, se anulan.
            if ($factura['saldo'] != $factura['saldo2']) continue;

            $asiento_detalle = new AsientoDetalle();
            $asiento_detalle->cuenta_id = $factura['id_cta'];
            $asiento_detalle->monto_debe = $factura['saldo'] == 'debe' ? round($factura['subtotal'], 2) : 0.0;
            $asiento_detalle->monto_haber = $factura['saldo'] == 'haber' ? round($factura['subtotal'], 2) : 0.0;

            $cotizacion = $factura['id_moneda'] == 1 ? null : Cotizacion::find()
                ->where(['moneda_id' => $factura['id_moneda']])
                ->andFilterWhere(['fecha' => date('Y-m-d', strtotime($factura['fecha_de_emision'] . ' - 1 days'))])->one();

            $newDetalleDiffCambio = null;
            if ($cotizacion != null) {
                $diff = round($factura['cotizacion'], 2) - round($cotizacion->venta, 2);
                if ($diff != 0.0) {
                    $empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

                    $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                    $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                    $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                    $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                    $newDetalleDiffCambio = new AsientoDetalle();
                    $newDetalleDiffCambio->cuenta_id = PlanCuenta::findOne(['id' => $diff < 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor])->id; // TODO: parametro del modulo
                    $newDetalleDiffCambio->monto_debe = $factura['saldo'] == 'debe' ? round($diff, 2) * round($factura['subtotal'], 2) : 0.0;
                    $newDetalleDiffCambio->monto_haber = $factura['saldo'] == 'haber' ? round($diff, 2) * round($factura['subtotal'], 2) : 0.0;
                    $diff < 0.0 ? $newDetalleDiffCambio->monto_debe = (round($newDetalleDiffCambio->monto_debe, 2) * -1.0) : true;
                    $diff < 0.0 ? $newDetalleDiffCambio->monto_haber = (round($newDetalleDiffCambio->monto_haber, 2) * -1.0) : true;
                    $total_debe += $newDetalleDiffCambio->monto_debe;
                    $total_haber += $newDetalleDiffCambio->monto_haber;
                }

                $asiento_detalle->monto_debe *= $factura['id_cta'] == 16 ? $factura['cotizacion'] : $cotizacion->venta;
                $asiento_detalle->monto_haber *= $cotizacion->venta;
            }

            # Round to integer
            $asiento_detalle->monto_debe = round($asiento_detalle->monto_debe);
            $asiento_detalle->monto_haber = round($asiento_detalle->monto_haber);

            if (isset($factura_id) && $factura['id_compra'] != $factura_id) {
                $dif = $debe_por_factura - $haber_por_factura;
                $asiento_error_red = new AsientoDetalle();
                if ($dif > 0) {
                    $asiento_error_red->cuenta_id = $ctaGananciaRoundingError->id;
                    $asiento_error_red->monto_debe = 0;
                    $asiento_error_red->monto_haber = $dif;
                } else {
                    $dif = abs($dif);
                    $asiento_error_red->cuenta_id = $ctaPerdidaRoundingError->id;
                    $asiento_error_red->monto_debe = $dif;
                    $asiento_error_red->monto_haber = 0;
                }
                if ($dif != 0)
                    $asiento_detalles[] = $asiento_error_red;
                $debe_por_factura = $haber_por_factura = 0;
            }
            $debe_por_factura += $asiento_detalle->monto_debe;
            $haber_por_factura += $asiento_detalle->monto_haber;
            $factura_id = $factura['id_compra'];

            $total_debe += $asiento_detalle->monto_debe;
            $total_haber += $asiento_detalle->monto_haber;
            $asiento_detalles[] = $asiento_detalle;
            if ($newDetalleDiffCambio != null) $asiento_detalles[] = $newDetalleDiffCambio;
        }

        retorno:;
        // Agrupar y sumar detalles iguales.
        $asiento_detalles = AsientoDetalle::agruparDetallesIguales($asiento_detalles);

        // Agrupar junto asientos de partida debe / haber
        $asiento_detalles = AsientoDetalle::agruparDebeHaberAsientos($asiento_detalles);

        return [$asiento_detalles, round($total_debe), round($total_haber)];
    }

    /**
     * @param $facturas ActiveRecord[]
     *
     * @param null $fechaAsiento
     * @return array
     * @throws \Exception
     */
    private function generarAsientosCompraIndependientes($facturas, $fechaAsiento = null)
    {
        if (!isset($fechaAsiento))
            throw new \Exception("Falta especificar fecha de asiento para asientos independientes.");

        #Crear conjunto de ID de tipo de documento. Cada elemento es diferente y unico
        $tipoDocIDs = [];
        foreach ($facturas as $factura)
            if (!in_array($factura['tipo_documento_id'], $tipoDocIDs))
                $tipoDocIDs[] = $factura['tipo_documento_id'];

        $ctaGananciaRoundingError = PlanCuenta::getCtaGananciaRoundingError();
        $ctaPerdidaRoundingError = PlanCuenta::getCtaPerdidaRoundingError();

        if (!isset($ctaGananciaRoundingError)) {
            throw new \Exception("Es necesario parametrizar en la empresa actual una cuenta para \"GANANCIA POR ERROR DE REDONDEO\".");
        } elseif (!isset($ctaPerdidaRoundingError)) {
            throw new \Exception("Es necesario parametrizar en la empresa actual una cuenta para \"GANANCIA POR ERROR DE REDONDEO\".");
        }

        #Crear tantos asientos como tipos de doc haya. Todos estos tipos de docs son para asientos independientes.
        $asientosIndependientes = [];
        foreach ($tipoDocIDs as $tipoDocID) {
            $tipoDoc = TipoDocumento::findOne(['id' => $tipoDocID]);
            if (!isset($tipoDoc)) continue;

            #Crear cabecera
            $asientoCabecera = new Asiento();
            $asientoCabecera->empresa_id = Yii::$app->session->get('core_empresa_actual');
            $asientoCabecera->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
            $asientoCabecera->fecha = $fechaAsiento;
            $asientoCabecera->monto_haber = $asientoCabecera->monto_debe = 0;
            $asientoCabecera->concepto = $tipoDoc->concepto_asiento_independiente;
            $asientoCabecera->usuario_id = Yii::$app->user->id;
            $asientoCabecera->creado = date('Y-m-d H:i:s');
            $asientoCabecera->modulo_origen = 'contabilidad';
            if (!$asientoCabecera->validate())
                throw new \Exception("Error generando asiento independiente: {$asientoCabecera->getErrorSummaryAsString()}");

            #Crear asiento detalles
            $asiento_detalles = [];
            $ID_facturasAsentadas = [];
            $total_haber = 0.0;
            $total_debe = 0.0;

            $debe_por_factura = $haber_por_factura = 0;
            $factura_id = null;
            foreach ($facturas as $key => $factura) {
                if ($factura['tipo_documento_id'] != $tipoDocID) continue;

                #Marcar facturas que van a ser asentadas con la cabecera actual.
                in_array($factura['id_compra'], $ID_facturasAsentadas) || $ID_facturasAsentadas[] = $factura['id_compra'];

                $asiento_detalle = new AsientoDetalle();
                $asiento_detalle->cuenta_id = $factura['id_cta'];
                $asiento_detalle->monto_debe = $factura['saldo'] == 'debe' ? $factura['subtotal'] : 0.0;
                $asiento_detalle->monto_haber = $factura['saldo'] == 'haber' ? $factura['subtotal'] : 0.0;
//                if ($tipoDoc->nombre == 'NOTA DE CRÉDITO CONTADO PROVEEDORES') {
//                    echo ("Procesar compra #{$factura['id_compra']}: {$factura['nro_factura']}, monto debe: {$asiento_detalle->monto_debe}, monto haber: {$asiento_detalle->monto_haber}, cuenta: {$asiento_detalle->cuenta->nombre}");
//                    echo '<br/>';
//                }
                #Valorizar si es necesario.
                /** @var Cotizacion $cotizacion */
                $cotizacion = $factura['id_moneda'] == 1 ? null : Cotizacion::find()
                    ->where(['moneda_id' => $factura['id_moneda']])
                    ->andFilterWhere(['fecha' => date('Y-m-d', strtotime($factura['fecha_de_emision'] . ' - 1 days'))])->one();

                if ($cotizacion != null) {
                    $diff = round($factura['cotizacion'], 2) - round($cotizacion->venta, 2);
                    if ($diff != 0.0) {
                        $empresa_id = Yii::$app->session->get('core_empresa_actual');
                        $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

                        $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                        $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                        $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                        $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                        $newDetalleDiffCambio = new AsientoDetalle();
                        $newDetalleDiffCambio->cuenta_id = PlanCuenta::findOne(['id' => $diff < 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor])->id; // TODO: parametro del modulo
                        $newDetalleDiffCambio->monto_debe = $factura['saldo'] == 'debe' ? round($diff, 2) * round($factura['subtotal'], 2) : 0.0;
                        $newDetalleDiffCambio->monto_haber = $factura['saldo'] == 'haber' ? round($diff, 2) * round($factura['subtotal'], 2) : 0.0;
                        $diff < 0.0 ? $newDetalleDiffCambio->monto_debe = (round($newDetalleDiffCambio->monto_debe, 2) * -1.0) : true;
                        $diff < 0.0 ? $newDetalleDiffCambio->monto_haber = (round($newDetalleDiffCambio->monto_haber, 2) * -1.0) : true;

                        #Acumular debe/haber por las cuentas de diferencia de cambio del asiento.
                        $total_debe += $newDetalleDiffCambio->monto_debe;
                        $total_haber += $newDetalleDiffCambio->monto_haber;

                        #Agregar a la lista de detalles de asiento.
                        $asiento_detalles[] = $newDetalleDiffCambio;
                    }

                    #Valorizar detalle de asiento.
                    $asiento_detalle->monto_debe *= $factura['id_cta'] == 16 ? $factura['cotizacion'] : $cotizacion->venta;
                    $asiento_detalle->monto_haber *= $cotizacion->venta;
                }

                #Acumular debe/haber
                $asiento_detalle->monto_debe = round($asiento_detalle->monto_debe);
                $asiento_detalle->monto_haber = round($asiento_detalle->monto_haber);

                if (isset($factura_id) && $factura['id_compra'] != $factura_id) {
                    $dif = $debe_por_factura - $haber_por_factura;
                    $asiento_error_red = new AsientoDetalle();
                    if ($dif > 0) {
                        $asiento_error_red->cuenta_id = $ctaGananciaRoundingError->id;
                        $asiento_error_red->monto_debe = 0;
                        $asiento_error_red->monto_haber = $dif;
                    } else {
                        $dif = abs($dif);
                        $asiento_error_red->cuenta_id = $ctaPerdidaRoundingError->id;
                        $asiento_error_red->monto_debe = $dif;
                        $asiento_error_red->monto_haber = 0;
                    }
                    if ($dif != 0)
                        $asiento_detalles[] = $asiento_error_red;
                    $debe_por_factura = $haber_por_factura = 0;
                }
                $debe_por_factura += $asiento_detalle->monto_debe;
                $haber_por_factura += $asiento_detalle->monto_haber;
                $factura_id = $factura['id_compra'];

                $total_debe += $asiento_detalle->monto_debe;
                $total_haber += $asiento_detalle->monto_haber;
                $asiento_detalles[] = $asiento_detalle;

                #Remover factura procesada.
                #De este modo, en cada ciclo de tipo de documento, se procesan solamente facturas que aun no fueron asentadas.
                #Mejora eficiencia.
                unset($facturas[$key]);
            }

            // Agrupar y sumar detalles iguales.
            $asiento_detalles = AsientoDetalle::agruparDetallesIguales($asiento_detalles);

            // Agrupar junto asientos de partida debe / haber
            $asiento_detalles = AsientoDetalle::agruparDebeHaberAsientos($asiento_detalles);

            #Resumir cuentas
            $asiento_detalles = self::resumirAsientosIndependientes($asiento_detalles);

            #Actualizar cabecera
            $asientoCabecera->monto_debe = round($total_debe);
            $asientoCabecera->monto_haber = round($total_haber);
            if (!$asientoCabecera->validate())
                throw new \Exception("Error validando asientos independientes: {$asientoCabecera->getErrorSummaryAsString()}");

            #Completar array principal: Agregar cabecera, detalles, las facturas asentadas y otras variables de utilidad
            $asientosIndependientes[] = [
                'cabecera' => $asientoCabecera,
                'detalles' => $asiento_detalles,
                'totalDebe' => round($total_debe),
                'totalHaber' => round($total_haber),
                'facturas' => $ID_facturasAsentadas,
                'tituloGrid' => "Asiento todas las factura {$tipoDoc->nombre}"
            ];
        }

        return $asientosIndependientes;
    }

    /**
     * @param $facturas ActiveRecord[]
     *
     * @param null $fechaAsiento
     * @return array
     * @throws \Exception
     */
    private function generarAsientosVentaIndependientes($facturas, $fechaAsiento = null)
    {
        if (!isset($fechaAsiento))
            throw new \Exception("Falta especificar fecha de asiento para asientos independientes.");

        #Crear conjunto de ID de tipo de documento. Cada elemento es diferente y unico
        $tipoDocIDs = [];
        foreach ($facturas as $factura)
            if (!in_array($factura['tipo_documento_id'], $tipoDocIDs))
                $tipoDocIDs[] = $factura['tipo_documento_id'];

        $ctaGananciaRoundingError = PlanCuenta::getCtaGananciaRoundingError();
        $ctaPerdidaRoundingError = PlanCuenta::getCtaPerdidaRoundingError();

        if (!isset($ctaGananciaRoundingError)) {
            throw new \Exception("Es necesario parametrizar en la empresa actual una cuenta para \"GANANCIA POR ERROR DE REDONDEO\".");
        } elseif (!isset($ctaPerdidaRoundingError)) {
            throw new \Exception("Es necesario parametrizar en la empresa actual una cuenta para \"GANANCIA POR ERROR DE REDONDEO\".");
        }

        #Crear tantos asientos como tipos de doc haya. Todos estos tipos de docs son para asientos independientes.
        $asientosIndependientes = [];
        foreach ($tipoDocIDs as $tipoDocID) {
            $tipoDoc = TipoDocumento::findOne(['id' => $tipoDocID]);
            if (!isset($tipoDoc)) continue;

            #Crear cabecera
            $asientoCabecera = new Asiento();
            $asientoCabecera->empresa_id = Yii::$app->session->get('core_empresa_actual');
            $asientoCabecera->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
            $asientoCabecera->fecha = $fechaAsiento;
            $asientoCabecera->monto_haber = $asientoCabecera->monto_debe = 0;
            $asientoCabecera->concepto = $tipoDoc->concepto_asiento_independiente;
            $asientoCabecera->usuario_id = Yii::$app->user->id;
            $asientoCabecera->creado = date('Y-m-d H:i:s');
            $asientoCabecera->modulo_origen = 'contabilidad';
            if (!$asientoCabecera->validate())
                throw new \Exception("Error generando asiento independiente: {$asientoCabecera->getErrorSummaryAsString()}");

            $asiento_detalles = [];
            $ID_facturasAsentadas = [];
            $total_haber = 0.0;
            $total_debe = 0.0;

            $debe_por_factura = $haber_por_factura = 0;
            $factura_id = null;
            foreach ($facturas as $key => $factura) {
                if ($factura['tipo_documento_id'] != $tipoDocID) continue;

                #Marcar facturas que van a ser asentadas con la cabecera actual.
                in_array($factura['id_venta'], $ID_facturasAsentadas) || $ID_facturasAsentadas[] = $factura['id_venta'];

                # Detalles de asiento
                $asiento_detalle = new AsientoDetalle();
                $asiento_detalle->cuenta_id = $factura['id_cta'];
                $asiento_detalle->monto_debe = $factura['saldo'] == 'debe' ? $factura['subtotal'] : 0.0;
                $asiento_detalle->monto_haber = $factura['saldo'] == 'haber' ? $factura['subtotal'] : 0.0;

                # Valorizacion
                /** @var Cotizacion $cotizacion */
                $cotizacion = $factura['id_moneda'] == 1 ? null : Cotizacion::find()
                    ->where(['moneda_id' => $factura['id_moneda']])
                    ->andFilterWhere(['fecha' => date('Y-m-d', strtotime($factura['fecha_de_emision'] . ' - 1 days'))])->one();

                if ($cotizacion != null) {
                    $diff = round($factura['cotizacion'], 2) - round($cotizacion->compra, 2);
                    if ($diff != 0.0) {
                        $empresa_id = Yii::$app->session->get('core_empresa_actual');
                        $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

                        $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                        $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                        $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                        $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                        $newDetalleDiffCambio = new AsientoDetalle();
                        $newDetalleDiffCambio->cuenta_id = PlanCuenta::findOne(['id' => $diff > 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor])->id; // TODO: parametro del modulo
                        $newDetalleDiffCambio->monto_debe = $factura['saldo'] == 'debe' ? round($diff, 2) * round($factura['subtotal'], 2) : 0.0;
                        $newDetalleDiffCambio->monto_haber = $factura['saldo'] == 'haber' ? round($diff, 2) * round($factura['subtotal'], 2) : 0.0;
                        $diff < 0.0 ? $newDetalleDiffCambio->monto_debe = (round($newDetalleDiffCambio->monto_debe, 2) * -1.0) : true;
                        $diff < 0.0 ? $newDetalleDiffCambio->monto_haber = (round($newDetalleDiffCambio->monto_haber, 2) * -1.0) : true;

                        #Acumular debe/haber por asiento de diff de cambio
                        $total_debe += round($newDetalleDiffCambio->monto_debe, 2);
                        $total_haber += round($newDetalleDiffCambio->monto_haber, 2);

                        #Agregar a la lista de detalles
                        $asiento_detalles[] = $newDetalleDiffCambio;
                    }

                    #Valorizar detalle de asiento
                    $asiento_detalle->monto_debe *= $factura['id_cta'] == 16 ? $factura['cotizacion'] : $cotizacion->compra;
                    $asiento_detalle->monto_haber *= $cotizacion->compra; //nunca va a haber caja en haber.
                }

                #Sin costo de venta, ya que ellos no quisieron.

                #Acumular debe/haber
                $total_debe += $asiento_detalle->monto_debe;
                $total_haber += $asiento_detalle->monto_haber;

                if (isset($factura_id) && $factura['id_venta'] != $factura_id) {
                    $dif = $debe_por_factura - $haber_por_factura;
                    $asiento_error_red = new AsientoDetalle();
                    if ($dif > 0) {
                        $asiento_error_red->cuenta_id = $ctaGananciaRoundingError->id;
                        $asiento_error_red->monto_debe = 0;
                        $asiento_error_red->monto_haber = $dif;
                    } else {
                        $dif = abs($dif);
                        $asiento_error_red->cuenta_id = $ctaPerdidaRoundingError->id;
                        $asiento_error_red->monto_debe = $dif;
                        $asiento_error_red->monto_haber = 0;
                    }
                    if ($dif != 0)
                        $asiento_detalles[] = $asiento_error_red;
                    $debe_por_factura = $haber_por_factura = 0;
                }
                $debe_por_factura += $asiento_detalle->monto_debe;
                $haber_por_factura += $asiento_detalle->monto_haber;
                $factura_id = $factura['id_venta'];

                $asiento_detalles[] = $asiento_detalle;

                #Remover factura procesada.
                #De este modo, en cada ciclo de tipo de documento, se procesan solamente facturas que aun no fueron asentadas.
                #Mejora eficiencia.
                unset($facturas[$key]);
            }

            // Agrupar y sumar detalles iguales.
            $asiento_detalles = AsientoDetalle::agruparDetallesIguales($asiento_detalles);

            // Agrupar junto asientos de partida debe / haber
            $asiento_detalles = AsientoDetalle::agruparDebeHaberAsientos($asiento_detalles);

            #Resumir cuentas
            $asiento_detalles = self::resumirAsientosIndependientes($asiento_detalles);

            #Actualizar cabecera
            $asientoCabecera->monto_debe = round($total_debe);
            $asientoCabecera->monto_haber = round($total_haber);
            if (!$asientoCabecera->validate())
                throw new \Exception("Error validando asientos independientes: {$asientoCabecera->getErrorSummaryAsString()}");

            $asientosIndependientes[] = [
                'cabecera' => $asientoCabecera,
                'detalles' => $asiento_detalles,
                'totalDebe' => round($total_debe),
                'totalHaber' => round($total_haber),
                'facturas' => $ID_facturasAsentadas,
                'tituloGrid' => "Asiento de todas las facturas {$tipoDoc->nombre}",
            ];
        }

        return $asientosIndependientes;
    }

    /**
     * @param AsientoDetalle[] $asientoDetalles
     * @return array
     */
    private function resumirAsientosIndependientes($asientoDetalles)
    {
//        return $asientoDetalles;
        /** @var AsientoDetalle[] $cuentasDebe */
        /** @var AsientoDetalle[] $cuentasHaber */
        $cuentasDebe = [];
        $cuentasHaber = [];
        foreach ($asientoDetalles as $detalle) {
            if ($detalle->monto_debe != 0)
                $cuentasDebe[] = $detalle;
            elseif ($detalle->monto_haber != 0)
                $cuentasHaber[] = $detalle;
        }

        foreach ($cuentasDebe as $keyDebe => $debe) {
            foreach ($cuentasHaber as $keyHaber => $haber) {
                if ($debe->cuenta_id == $haber->cuenta_id)
                    if ($debe->monto_debe > $haber->monto_haber) {
                        $cuentasDebe[$keyDebe]->monto_debe -= $haber->monto_haber;
                        unset($cuentasHaber[$keyHaber]);
                    } elseif ($haber->monto_haber > $debe->monto_debe) {
                        $cuentasHaber[$keyHaber]->monto_haber -= $debe->monto_debe;
                        unset($cuentasDebe[$keyDebe]);
                    } else {
                        unset($cuentasDebe[$keyDebe]);
                        unset($cuentasHaber[$keyHaber]);
                    }
            }
        }

        return array_merge($cuentasDebe, $cuentasHaber);
    }

    /**
     * @param $asientosIndependientes array
     * @param $operacion string|null
     * @throws \Exception
     */
    private function guardarAsientosIndependientes($asientosIndependientes, $operacion = null, &$asientos_generados = false)
    {
        if (!isset($operacion))
            throw new \Exception("Error interno: Falta especificar parametro \$operacion en la funcion " . __FUNCTION__);

        foreach ($asientosIndependientes as $asientoData) {
            /**
             * @var Asiento $cabecera
             * @var AsientoDetalle[] $detalles
             */
            $cabecera = $asientoData['cabecera'];
            $detalles = $asientoData['detalles'];

            #Guardar cabecera. Generar id.
            if (!$cabecera->save())
                throw new \Exception("Error guardando validando asiento independiente: {$cabecera->getErrorSummaryAsString()}");
            $cabecera->refresh();

            #Verificar concepto
            if ($cabecera->concepto == '') {
                $tipoDocNombre = ($operacion == 'compra') ?
                    Compra::findOne(['id' => $asientoData['facturas'][0]])->tipoDocumento->nombre :
                    Venta::findOne(['id' => $asientoData['facturas'][0]])->tipoDocumento->nombre;
                throw new \Exception("Falta concepto para asiento independiente por {$tipoDocNombre}. Tal vez el tipo de documento {$tipoDocNombre} no esté configurado para asiento independiente o no tenga definido el concepto.");
            }

            #Guardar detalles. Asociar a la cabecera.
            foreach ($detalles as $detalle) {
                $detalle->asiento_id = $cabecera->id;
                if (!$detalle->save())
                    throw new \Exception("Error guardando detalle de asiento independiente: {$detalle->getErrorSummaryAsString()}");
                $detalle->refresh();
                $asientos_generados = $asientos_generados || true;
            }
            $cabecera->refresh();
            $cabecera->setDebeHaberFromDetalles();
            if (!$cabecera->save())
                throw new \Exception("Error guardando asiento independiente: {$cabecera->getErrorSummaryAsString()}");
            $cabecera->refresh();

            #Actualizar facturas. Asignar id del asiento generado.
            $facturas_id = $asientoData['facturas'];
            $facturas = ($operacion == 'compra') ?
                Compra::find()->where(['IN', 'id', $facturas_id])->all() :
                Venta::find()->where(['IN', 'id', $facturas_id])->all();

            if (!isset($facturas) || empty($facturas))
                throw new \Exception("Error interno: NO hay facturas a asociar a asientos independientes (Function: " . __FUNCTION__ . ")");

            /** @var Compra|Venta $factura */
            foreach ($facturas as $factura) {
                $factura->asiento_id = $cabecera->id;
                if (!$factura->save(false))
                    throw new \Exception("Error asociando factura a asiento independiente: {$factura->getErrorSummaryAsString()}");
                $factura->refresh();
                $asientos_generados = $asientos_generados || true;
            }
        }
    }

    private function generarAsientosSalario($ctaPincipal, $cuentas)
    {
        $asiento_detalles = [];

        $totalHaber = 0;
        foreach ($cuentas as $cuenta) {
            $asiento_detalle = new AsientoDetalle();
            $asiento_detalle->cuenta_id = $cuenta[1];
            $asiento_detalle->monto_debe = $cuenta[0];
            $asiento_detalle->monto_haber = 0.0;
            $asiento_detalles[] = $asiento_detalle;
            $totalHaber += $cuenta[0];
        }

        // Agrupar y sumar detalles iguales.
        $asiento_detalles = AsientoDetalle::agruparDetallesIguales($asiento_detalles);

        // Agrupar junto asientos de partida debe / haber
        $asiento_detalles = AsientoDetalle::agruparDebeHaberAsientos($asiento_detalles);

        $asiento_detalle = new AsientoDetalle();
        $asiento_detalle->cuenta_id = $ctaPincipal;
        $asiento_detalle->monto_debe = 0.0;
        $asiento_detalle->monto_haber = $totalHaber;
        $asiento_detalles[] = $asiento_detalle;

        $total_debe = $totalHaber;
        $total_haber = $totalHaber;

        return [$asiento_detalles, round($total_debe), round($total_haber)];
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    // TODO: En prueba.
    public function actionBorrarAsientos($submit = false)
    {
        $model = new TemplateModel();

        if ($submit != false && $model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();

            try {
                $fecha = implode('-', array_reverse(explode('-', $model->fecha_rango)));
                $asientos = Asiento::findAll(['fecha' => $fecha]);

                if (!empty($asientos)) {
                    foreach ($asientos as $model) {

                        /** @var Compra[] $compras */
                        /** @var Venta[] $ventas */

                        $compras = Compra::find()->where(['asiento_id' => $model->id])->all();
                        $ventas = Venta::find()->where(['asiento_id' => $model->id])->all();
                        $asientos = Asiento::find()->where(['asiento_id' => $model->id])->all();
                        $seguros = PolizaDevengamiento::find()->where(['asiento_id' => $model->id])->all();
                        $prestamos = Prestamo::find()->where(['asiento_id' => $model->id])->all();
                        $cuotas = ReciboPrestamoDetalle::find()->where(['asiento_id' => $model->id])->all();
                        $detallesCompraAsientoPorCuotaPrestamo = DetalleCompra::find()->where(['asiento_id_por_cuota_prestamo' => $model->id])->all();
                        $asientos_id = [];
                        $recibos = Recibo::findAll(['asiento_id' => $model->id]);
                        $retenciones = Retencion::findAll(['asiento_id' => $model->id]);

                        foreach ($asientos as $asiento) {
                            $asientos_id[] = $asiento->id;
                        }
                        $asientos_asiento_detalles = AsientoDetalle::find()->where(['IN', 'asiento_id', $asientos_id])->all();
                        $asiento_detalles = AsientoDetalle::find()->where(['asiento_id' => $model->id])->all();

                        foreach ($compras as $compra) {
                            $compra->asiento_id = null;
                            if (!$compra->save(false)) {
                                throw new Exception("No se pudo desasociar las compras del asiento.");
                            }
                        }
                        // Detalles de compra, asociadas a un asiento, por las cuotas de prestamo devengadas.
                        foreach ($detallesCompraAsientoPorCuotaPrestamo as $detalle) {
                            $detalle->asiento_id_por_cuota_prestamo = null;
                            if (!$detalle->save()) {
                                throw new \Exception("Error desasociando detalles de compra (por cuota de prestamo): {$detalle->getErrorSummaryAsString()}");
                            }
                        }

                        foreach ($ventas as $venta) {
                            $venta->asiento_id = null;
                            $venta->timbrado_detalle_id_prefijo = 0; // se queja por ser required.
                            if (!$venta->save(false)) {
                                $msg = '';
                                foreach ($venta->getErrorSummary(true) as $item) {
                                    $msg .= $item . ', ';
                                }
                                throw new \Exception('No se pudo desasociar las ventas del asiento: ' . $msg);
                            }
                        }

                        foreach ($seguros as $seguro) {
                            $seguro->asiento_id = null;
                            $seguro->save(false);
                        }

                        foreach ($cuotas as $cuota) {
                            $cuota->asiento_id = null;
                            $cuota->save(false);
                        }

                        /** @var PrestamoDetalle $cuota */
                        foreach (PrestamoDetalle::find()->where(['asiento_devengamiento_id' => $model->id])->all() as $cuota) {
                            $cuota->asiento_devengamiento_id = null;
                            if (!$cuota->save()) {
                                throw new \Exception("Error desasociando cuota #{$cuota->nro_cuota} del prestamo #{$cuota->prestamo_id} de este asiento (de devengamiento): {$cuota->getErrorSummaryAsString()}");
                            }
                        }

                        foreach ($prestamos as $prestamo) {
                            $prestamo->asiento_id = null;
                            $prestamo->save(false);
                        }

                        // Borrar asiento_detalles de los asientos asociados al asiento a borrar, por ejemplo: asiento y sus detalles por costo de venta.
                        foreach ($asientos_asiento_detalles as $item) {
                            if (!$item->delete()) {
                                throw new \Exception('No se pudo borrar detalles de asientos asociados a este asiento.');
                            }
                        }
                        foreach ($asientos as $asiento) {
                            if (!$asiento->delete()) {
                                throw new Exception('Error borrando asiento asociado a este asiento.');
                            }
                        }

                        // borrar detalles del asiento a borrar.
                        foreach ($asiento_detalles as $asiento_detalle) {
                            if (!$asiento_detalle->delete()) {
                                throw new Exception('No se pudo desasociar los asiento_detalles del asiento.');
                            }
                        }

                        # Es asiento de revaluo de activo fijo
                        $activosF = ActivoFijo::findAll(['asiento_revaluo_id' => $model->id]);
                        if (isset($activosF)) {
                            foreach ($activosF as $activoFijo) {
                                if ($activoFijo->asiento_depreciacion_id != '') {
                                    throw new \Exception("No se puede borrar un asiento de revalúo de un activo fijo que se ha revaluado y depreciado. Borre primero el asiento de depreciación y luego vuelva a intentar.");
                                }

                                $activoFijo->asiento_revaluo_id = '';
                                $activoFijo->valor_fiscal_revaluado = 0;
                                $activoFijo->valor_contable_revaluado = 0;
                                if (!$activoFijo->save()) {
                                    throw new \Exception("Error desvinculando activo fijo: Es un asiento de revalúo pero no se pudo desasociar del activo fijo correspondiente: {$activoFijo->getErrorSummaryAsString()}");
                                }
                            }
                        }

                        # Es asiento de depreciacion
                        $activosF = ActivoFijo::findAll(['asiento_depreciacion_id' => $model->id]);
                        if (isset($activosF)) {
                            foreach ($activosF as $activoFijo) {
                                $activoFijo->asiento_depreciacion_id = '';
                                $activoFijo->valor_fiscal_depreciado = 0;
                                $activoFijo->valor_contable_depreciado = 0;
                                if (!$activoFijo->save()) {
                                    throw new \Exception("Error desvinculando activo fijo: Es un asiento de depreciación pero no se pudo desasociar del activo fijo correspondiente: {$activoFijo->getErrorSummaryAsString()}");
                                }
                            }
                        }

                        // El asiento a borrar es uno de cierre de periodo.
                        $periodoCierre = EmpresaPeriodoContable::findOne(['asiento_cierre_id' => $model->id]);
                        if (isset($periodoCierre)) {
                            $periodoReaperturaCorrelated = EmpresaPeriodoContable::find()->where(['periodo_anterior_id' => $periodoCierre->id])->one();
                            if ($periodoReaperturaCorrelated->asiento_apertura_id != '') {
                                throw new \Exception('Hay un asiento de reapertura del siguiente periodo que es correlativo a este asiento de cierre. Aun falta definir los controles previos antes de borrar uno.');
                            }

                            $periodoCierre->asiento_cierre_id = '';
                            if (!$periodoCierre->save()) {
                                throw new \Exception("Error al desasociar del periodo de cierre: {$periodoCierre->getErrorSummaryAsString()}");
                            }
                        } else {
                            $periodoReapertura = EmpresaPeriodoContable::findOne(['asiento_apertura_id' => $model->id]);
                            if (isset($periodoReapertura)) {
                                # El asiento a borrar es un asiento de reapertura.
                                if (($msg = $model->hasData($model->periodo_contable_id, $model->empresa_id))) {
                                    throw new \Exception("Error borrando asiento de reapertura: $msg");
                                }

                                $activosFijosTraspasados = ActivoFijo::find()
                                    ->where(['empresa_periodo_contable_id' => $model->periodo_contable_id, 'empresa_id' => $model->empresa_id])
                                    ->andWhere(['IS NOT', 'activo_fijo_id', null]);  # Con esto se asume que es suficiente, para saber con exactitud que activo fijo traspasado.
                                //                        ->andWhere(['IS', 'asiento_revaluo_id', null])
                                //                        ->andWhere(['IS', 'asiento_depreciacion_id', null])
                                //                        ->andWhere(['IS', 'factura_compra_id', null])
                                //                        ->andWhere(['IS', 'factura_venta_id', null])
                                //                        ->andWhere(['IS', 'factura_venta_id', null]);
                                /** @var ActivoFijo $activoFijoTraspasado */
                                foreach ($activosFijosTraspasados->all() as $activoFijoTraspasado) {
                                    if (!$activoFijoTraspasado->delete()) {
                                        throw new \Exception("Error borrando activos fijos traspasados: {$activoFijoTraspasado->getErrorSummaryAsString()}");
                                    }
                                }

                                # TODO: Borrar activos fijos traspasados (ok)
                                # TODO: Borrar activos biologicos traspasados
                                # TODO: Borrar id de asiento de reapertura del periodo contable (ok)

                                $periodoReapertura->asiento_apertura_id = '';  # eliminar id del asiento de reapertura del periodo contable
                                if (!$periodoReapertura->save()) {
                                    throw new \Exception("Error al desasociar del periodo de reapertura: {$periodoReapertura->getErrorSummaryAsString()}");
                                }
                            }
                        }

                        # Desasociar recibos asentados
                        foreach ($recibos as $recibo) {
                            $recibo->asiento_id = null;
                            if (!$recibo->save())
                                throw new \Exception("No se pudo desasociar recibo ID:{$recibo->id}: {$recibo->getErrorSummaryAsString()}");
                        }

                        # Desasociar retenciones
                        foreach ($retenciones as $retencion) {
                            $retencion->asiento_id = null;
                            $retencion->factura_id = 5; // campo virtual necesario
                            if (!$retencion->save())
                                throw new \Exception("No se puede desasociar retencion ID: $retencion->id: {$retencion->getErrorSummaryAsString()}");
                        }

                        if (!$model->delete()) {
                            throw new Exception('No se pudo borrar el asiento.');
                        };
                    }

                    $transaction->commit();
                    FlashMessageHelpsers::createSuccessMessage('Asiento eliminado exitosamente. Si este asiento había sido generado desde una factura, la misma queda liberada.');
                    return $this->redirect(['index']);

                } else {
                    FlashMessageHelpsers::createInfoMessage("No existen asientos para la fecha indicada.");
                }
            } catch (\Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 7000);
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('modal/_form_date_range_select', [
            'model' => $model
        ]);
    }

    public function actionGetSucursalLista($empresa_id = null, $q = null)
    {
        $resultado = ['results' => ['id' => '', 'text' => '']];

        try {
            /** @var Connection $connection */
            $connection = Yii::$app->db_rrhh;
            $sql = "SELECT id, nombre as text  FROM rrhh_empresa_sucursal WHERE empresa_id_fk = $empresa_id";
            $command = $connection->createCommand($sql);
            $_retornar = $command->queryAll();

            $resultado = ['results' => $_retornar];
        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
            FlashMessageHelpsers::createWarningMessage('No se pudo conectar a RRHH, verifique conf bd.');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $resultado;
    }
}

/**
 * @property string $fecha_rango
 * @property string $descripcion
 * @property string $devengamientos
 * @property string $devengamientos2
 * @property string $prestamo
 * @property string $prestamo_concepto
 * @property string $concepto_devengamiento
 * @property string $concepto_devengamiento2
 */
class TemplateModel extends Model
{
    public $fecha_rango;
    public $descripcion;
    public $devengamientos;
    public $devengamientos2; // prueba
    public $concepto_devengamiento;
    public $concepto_devengamiento2; // prueba
    public $prestamo;
    public $prestamo_concepto;

    public $campo_vacio;
    public $concepto_campo_vacio;

    public $campo_vacio2;
    public $concepto_campo_vacio2;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_rango', 'descripcion', 'devengamientos', 'devengamientos2', 'concepto_devengamiento',
                'concepto_devengamiento2', 'prestamo', 'prestamo_concepto', 'campo_vacio', 'concepto_campo_vacio', 'concepto_campo_vacio2'], 'safe'],
            [['fecha_rango', 'descripcion', 'devengamientos', 'devengamientos2', 'concepto_devengamiento',
                'concepto_devengamiento2', 'prestamo', 'prestamo_concepto', 'campo_vacio', 'concepto_campo_vacio', 'concepto_campo_vacio2'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fecha_rango' => 'Rango fecha',
            'descripcion' => 'Concepto',
            'devengamientos' => 'Dev. de Seguro',
            'devengamientos2' => 'Dev. de Préstamos',
            'prestamo' => "Préstamo",
            'prestamo_concepto' => "Concepto Asiento Préstamo",
            'concepto_devengamiento' => 'Concepto Dev. Seguro',
            'concepto_devengamiento2' => 'Concepto Dev. Cuota de Préstamo',
        ];
    }
}

/**
 * @property string $mes_anho
 * @property string $empresa_id
 * @property string $cuenta_salida_id
 * @property string $concepto_salida
 *
 *
 * @property PlanCuenta $cuentaSalida
 */
class AsientoDeSueldosForm extends Model
{
    public $mes_anho;
    public $empresa_id;
    public $cuenta_salida_id;
    public $concepto_salida;
    public $sucursal_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mes_anho', 'empresa_id', 'sucursal_id', 'cuenta_salida_id', 'concepto_salida'], 'safe'],
            [['mes_anho', 'empresa_id', 'sucursal_id', 'cuenta_salida_id', 'concepto_salida'], 'string'],
            [['mes_anho', 'empresa_id', 'cuenta_salida_id', 'concepto_salida'], 'required']
        ];
    }

    public static $map = [
        1 => 'tipo-mov-presencia',
        2 => 'tipo-mov-ausencia',
        3 => 'tipo-mov-bonificacion-familiar',
        4 => 'tipo-mov-suspension',
        5 => 'tipo-mov-permiso',
        6 => 'tipo-mov-gratificaciones',
        7 => 'tipo-mov-anticipo',
        8 => 'tipo-mov-vacaciones-causadas',
        9 => 'tipo-mov-vacaciones-proporcionales',
        10 => 'tipo-mov-embargo',
        11 => 'tipo-mov-omision-preaviso-patronal',
        12 => 'tipo-mov-omision-preaviso-empleado',
        13 => 'tipo-mov-indemnizacion',
        14 => 'tipo-mov-horas-extra',
        15 => 'tipo-mov-aguinaldo',
        16 => 'tipo-mov-desvinculacion',
        17 => 'tipo-mov-reposo-maternidad',
        19 => 'tipo-mov-renuncia',
        20 => 'tipo-mov-abandono',
        21 => 'tipo-mov-termino-contrato',
        22 => 'tipo-mov-despido-justificado',
        23 => 'tipo-mov-despido-injustificado',
        24 => 'tipo-mov-reposo',
        25 => 'tipo-mov-proceso-judicial',
        26 => 'tipo-mov-vacaciones',
    ];

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'mes_anho' => 'Mes/año',
            'empresa_id' => 'Empresa',
            'sucursal_id' => 'Sucursal',
            'cuenta_salida_id' => 'Cuenta de pago?',
            'concepto_salida' => 'Concepto de pago',
        ];
    }

    public function getCuentaSalida()
    {
        return PlanCuenta::findOne($this->cuenta_salida_id);
    }

    public static function getEmpresa()
    {
        $valores = [];
        try {
            /** @var Connection $connection */
            $connection = Yii::$app->db_rrhh;
            $sql = "SELECT id, nombre FROM rrhh_empresa_empresa";
            $command = $connection->createCommand($sql);
            $valores = $command->queryAll();

        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
            FlashMessageHelpsers::createWarningMessage('No se pudo conectar a RRHH, verifique conf bd.');
        }
        return ArrayHelper::map($valores, 'id', 'nombre');
    }

    public function getSucursal()
    {
        $_retornar = [];
        $empresa_id = $this->empresa_id;
        try {
            $connection = Yii::$app->db_rrhh;
            $sql = "SELECT id, nombre  FROM rrhh_empresa_sucursal WHERE empresa_id_fk = $empresa_id";
            $command = $connection->createCommand($sql);
            $_retornar = $command->queryOne();

        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
            FlashMessageHelpsers::createWarningMessage('No se pudo conectar a RRHH, verifique conf bd.');
        }
        return $_retornar;
    }

    public static function getSucursalList($empresa_id, $depend = false)
    {
        try {
            $connection = Yii::$app->db_rrhh;
            $sql = "SELECT id, nombre AS name FROM rrhh_empresa_sucursal WHERE empresa_id_fk = $empresa_id";
            $command = $connection->createCommand($sql);
            $valores_enum = $command->queryAll();
        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
            Yii::info("### ERROR QUERY SUCURSAL");

        }
        return $valores_enum;
    }
}
