<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Cotizacion;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\search\DetalleCompraSearch;
use common\helpers\FlashMessageHelpsers;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * DetalleCompraController implements the CRUD actions for DetalleCompra model.
 */
class DetalleCompraController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DetalleCompra models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DetalleCompraSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DetalleCompra model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DetalleCompra model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DetalleCompra();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DetalleCompra model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DetalleCompra model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the DetalleCompra model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DetalleCompra the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DetalleCompra::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /**
     * @param bool $submit
     * @return array|string
     */
    public function actionAdd($submit = false)
    {
        $model = new DetalleCompra();
        $session = Yii::$app->session;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            $dataProvider = \Yii::$app->session->get('cont_detallecompra-provider');
            $data = $dataProvider->allModels;

            array_push($data, $model);
            $data = DetalleCompra::agruparCuentas($data);

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);
            $session->set('cont_detallecompra-provider', $dataProvider);
        };

        retorno:;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('_form_modal', [
            'model' => $model,
        ]);
    }

    /**
     * @param $indice
     * @return bool
     */
    public function actionBorrar($indice)
    {
        $session = Yii::$app->session;
        $dataProvider = \Yii::$app->session->get('cont_detallecompra-provider');
        $data = $dataProvider->allModels;
        if ($data[$indice]->id != null) {
            if (DetalleCompra::find()->where(['id' => $data[$indice]->id])->exists())
                DetalleCompra::findOne(['id' => $data[$indice]->id])->delete();
        }
        unset($data[$indice]);
        $data = array_values($data);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);
        $session->set('cont_detallecompra-provider', $dataProvider);
        return true;
    }

    /**
     * @param $indice
     * @param bool $submit
     * @return array|string
     */
    public function actionModificar($indice, $submit = false)
    {
        $session = Yii::$app->session;
        $dataProvider = $session['cont_detallecompra-provider'];
        $data = $dataProvider->allModels;
        /** @var DetalleCompra $model */
        $model = $data[$indice];
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $data[$indice] = $model;
            if ($data[$indice] != null) {
                if ($data[$indice]->id != null)
                    $data[$indice]->save();
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);
            $session->set('cont_detallecompra-provider', $dataProvider);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('_form_modal', [
            'model' => $model,
        ]);
    }

    public function actionGetCotizacion()
    {
        $existe = array_key_exists('fecha_emision', $_POST) && array_key_exists('moneda_id', $_POST);
        $fecha = "";
        if ($existe) {
            $fecha_emision = $_POST['fecha_emision'];
            $fecha_emision = date_create_from_format('d-m-Y', $fecha_emision);
            if ($fecha_emision) {
                $fecha_emision = $fecha_emision->format('Y-m-d');
                $fecha = date('Y-m-d', strtotime('-1 day', strtotime($fecha_emision)));
                $moneda_id = $_POST['moneda_id'];
                $query = Cotizacion::find()->where(['fecha' => $fecha, 'moneda_id' => $moneda_id])->one();
                if ($query == null && $moneda_id != '' && $moneda_id != \common\models\ParametroSistema::getMonedaBaseId()) {
                    $fecha = implode('-', array_reverse(explode('-', $fecha)));
                    FlashMessageHelpsers::createWarningMessage("Falta registrar cotización de {$fecha}");
                }
                return $query != null ? $query->venta : '';
            }else{
                Yii::warning("111111");
            }

        } else {
            FlashMessageHelpsers::createWarningMessage("Falta especificar fecha y/o moneda." . $_POST['fecha_emision']);
        }

        return false;
    }
}