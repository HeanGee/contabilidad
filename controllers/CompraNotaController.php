<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 7/16/18
 * Time: 8:20 PM
 */

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Cotizacion;
use backend\models\Moneda;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\ActivoFijoStockManager;
use backend\modules\contabilidad\models\auxiliar\TimbradoDetalle2;
use backend\modules\contabilidad\models\auxiliar\TimbradoDetalleUpdate;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\search\CompraSearch;
use backend\modules\contabilidad\models\Timbrado;
use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\TipoDocumento;
use common\helpers\FlashMessageHelpsers;
use DateTime;
use Exception;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CompraController implements the CRUD actions for Compra model for Notas.
 */
class CompraNotaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view', 'bloquear', 'desbloquear'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Compra $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Compra $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['bloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Compra $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['desbloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Compra $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'si';
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Compra models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompraSearch();
        $searchModel->empresa_id = Yii::$app->session->get(SessionVariables::empresa_actual);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
        $dataProvider->pagination = ['pageSize' => 20];

        //borrar variables de session
        self::removeSessionVariables();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single Compra model (as Nota).
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    private static function isNotaCreable()
    {
        try {
//            throw new \yii\base\Exception("En proceso ..."); // descomentar al implementar
            if (!Compra::find()->where([
                'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
                'estado' => 'vigente',
            ])->exists()) throw new \yii\base\Exception("Aún no se ha registrado ninguna factura de compra para la empresa y periodo contable actuales.");
            Compra::getTipodocSetNotaCredito();
            Compra::getTipodocSetNotaDebito();
            Compra::getTipodocNotaCredito();
            Compra::getTipodocNotaDebito();
        } catch (\yii\base\Exception $exception) {
            throw $exception;
        }
        return true;
    }

    private static function removeSessionVariables()
    {
        Yii::$app->session->remove('cont_detallecompra-provider');
        Yii::$app->session->remove('cont_detallecompra_costo_sim-provider');
        Yii::$app->session->remove('cont_detallecompra_sim-provider');
        Yii::$app->getSession()->remove('cont_monto_totales_compra');
        Yii::$app->session->remove('cont_monto_totales');
        Yii::$app->session->remove('cont_activofijo_ids');
        Yii::$app->session->remove('cont_prefijo_fila_plantilla');
        Yii::$app->session->remove('cont_compra_id');
        Yii::$app->session->remove('cont_compra_nota_credito_id');
        Yii::$app->session->remove('cont_activo_bio_stock_manager');
        Yii::$app->session->remove('$campo_iva_activo');
        Yii::$app->session->remove('$nota_id');
        Yii::$app->session->remove('$compra_id');
        Yii::$app->session->remove('cont_act_bio_valor_para_plantilla');
        Yii::$app->session->remove('total-costo-adq');
    }

    /**
     * Creates a new Compra model as Nota.
     * @param null $_pjax
     * @return mixed
     * @throws \Throwable
     */
    public function actionCreate($_pjax = null)
    {
        try {
            self::isNotaCreable();
        } catch (\yii\base\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }

        $model = new Compra();
        $session = Yii::$app->session;

        if ($_pjax == null && !Yii::$app->request->isPost) {
            self::removeSessionVariables();

            $session->set('cont_detallecompra-provider', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));

            $session->set('cont_detallecompra_sim-provider', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
        }

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            $getTipoDoc = function ($tipoNota) {
                if ($tipoNota == 'nota_credito')
                    return Compra::getTipodocNotaCredito();
                else
                    return Compra::getTipodocNotaDebito();
            };
            try {
                $model->creado_por = Yii::$app->user->identity->username;
                $periodo_contable = Yii::$app->session->get('core_empresa_actual_pc');
                $model->empresa_id = Yii::$app->session->get(SessionVariables::empresa_actual);
                $model->periodo_contable_id = $periodo_contable;
                $factura_model = Compra::findOne($model->factura_compra_id);
                // datos estirados de la factura de compra
                if (isset($factura_model)) {
                    $model->entidad_id = $factura_model->entidad_id;
                    $model->obligacion_id = $factura_model->obligacion_id;
                    $model->para_iva = $factura_model->para_iva;
                    $model->cant_cuotas = $factura_model->cant_cuotas;
                    if ($model->cant_cuotas == '') $model->cant_cuotas = 1;
                } else {
                    $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                    $model->entidad_id = $entidad->id;
                    $model->para_iva = 'si';
                    $model->cant_cuotas = 1;
                }
                // propios de la nota
                $model->estado = 'vigente';
                $model->tipo_documento_id = $getTipoDoc($model->tipo)->id;
                $model->condicion = $model->tipoDocumento->condicion;
                $model->fecha_vencimiento = date('Y-m-d', strtotime($model->fecha_emision . ' +1 month'));

                //Actualizar número actual en timbrado detalle
                preg_match('/^((\d+)-(\d+))-(\d+)$/', $model->nro_factura, $match);
                $nro = (int)$match[4];
                $prefijo = $match[1];
                /** @var TimbradoDetalle $timbradoDetalle */
                $timbradoDetalle = TimbradoDetalle::find()
                    ->joinWith('timbrado as timbrado')
                    ->where([
                        'timbrado.id' => $model->timbrado_id,
                        'prefijo' => $prefijo,
                        'tipo_documento_set_id' => $model->tipoDocumento->tipo_documento_set_id,
                    ])
                    ->andWhere(['<=', 'nro_inicio', $nro])
                    ->andWhere(['>=', 'nro_fin', $nro])
                    ->one();
                if (empty($timbradoDetalle)) {
                    //No existe el numero de factura en el rango del timbrado
                    //Se crea un detalle para el timbrado
                    $newDetalleTimbrado = new TimbradoDetalle();
                    $newDetalleTimbrado->prefijo = $prefijo;
                    $newDetalleTimbrado->timbrado_id = $model->timbrado_id;
                    $newDetalleTimbrado->nro_inicio = $nro;
                    $newDetalleTimbrado->nro_fin = $nro;
                    $newDetalleTimbrado->nro_actual = $nro;
                    $newDetalleTimbrado->tipo_documento_set_id = $model->tipoDocumento->tipo_documento_set_id;
                    if (!$newDetalleTimbrado->save()) {
                        throw new \yii\base\Exception($newDetalleTimbrado->getErrorSummaryAsString());
                    }
                    $model->timbrado_detalle_id = $newDetalleTimbrado->id;
                } else {
//                    $timbradoDetalle->tipo_documento_set_id = $model->tipoDocumento->tipo_documento_set_id;
                    $timbradoDetalle->nro_actual = $nro;
                    if (!$timbradoDetalle->save()) {
                        throw new \yii\base\Exception($timbradoDetalle->getErrorSummaryAsString());
                    }
                    $model->timbrado_detalle_id = $timbradoDetalle->id;
                }

                $model->nombre_entidad = '-'; // me exige el modelo
                if (!$model->validate()) {
                    throw new \yii\base\Exception($model->getErrorSummaryAsString());
                }
                $model->total = 0; // se va a recalcular para evitar manipulaciones.

                $model->formatDateForSave();
                if (!$model->save()) {
                    throw new \yii\base\Exception("Error validando formulario: {$model->getErrorSummaryAsString()}");
                }

                // Verificar si la plantilla tiene detalles
                $totales_tabla = Yii::$app->request->post('CompraIvaCuentaUsada', []);
                foreach ($totales_tabla as $total) {
                    if ($total['plantilla_id'] != '') {
                        $query = PlantillaCompraventa::findOne(['id' => $total['plantilla_id']])->getPlantillaCompraventaDetalles();
                        if (!$query->exists()) {
                            $msg = 'La plantilla seleccionada con ID: ' . $total['plantilla_id'] . ' no tiene detalles. AGREGUE PRIMERO y luego vuelva a crear la factura.';
                            throw new \yii\base\Exception($msg);
                        }
                    }
                }

                // Manejar iva cuenta
                $ningun_total = true;
                $ivacuenta_guardar = [];
                $this->manejarIvasCuentasUsadas($model, $ivacuenta_guardar, $ningun_total, $factura_model);
                if ($ningun_total) {
                    $msg = 'Falta completar al menos un campo del Total Iva o Total Exenta.';
                    throw new \yii\base\Exception($msg);
                }

                //Validar nro de Nota
                $result = Compra::checkNroFactura($model->nro_factura, $model->id, $model->tipo, $model->entidad_id);
                $msg = '';
                switch ($result) {
                    case Compra::NRO_FACTURA_NO_RANGO:
                        $msg = 'Este nro de Nota no corresponde a ningún rango de ningún timbrado.';
                        break;
                    case Compra::NRO_FACTURA_NO_PREFIJO:
                        $msg = 'Este prefijo de Nota no existe. DEBE CREAR desde el menú de Timbrado primero.';
                        break;
                    case Compra::NRO_FACTURA_DUPLICADO:
                        $msg = 'Este nro de Nota está duplicado.';
                        break;
                }

                if ($result != Compra::NRO_FACTURA_CORRECTO) {
                    throw new \yii\base\Exception($msg);
                }

                /** @var DetalleCompra[] $detalleCompra */
                $detalleCompra = $session['cont_detallecompra-provider']->allModels;
                if (empty($detalleCompra)) {
                    $msg = 'Faltan los detalles de la factura';
                    throw new \yii\base\Exception($msg);
                }

                // Calcular gravada e impuesto en detalles. Asociar a la cabecera, sumar todos los saldos. Validar
                $saldo = ['debe' => 0.0, 'haber' => 0.0];
                foreach ($detalleCompra as $detalle) {
                    $saldo[$detalle->cta_contable] += (float)$detalle->subtotal;
                    $detalle->factura_compra_id = $model->id;
                    $detalle->periodo_contable_id = $periodo_contable;
                    if (!$detalle->validate()) {
                        $msg = 'Error en detalle de factura: ' . $detalle->planCuenta->nombre . ": " . $detalle->getErrorsEnString();
                        throw new \yii\base\Exception($msg);
                    }
                }
                // no hace falta validar ivactausada porque ya se valida en $this->manejarIvasCuentasUsadas()
                if ($saldo['debe'] !== $saldo['haber']) {
                    $msg = 'El DEBE y el HABER NO son iguales. Se recomienda verificar cómo está guardada la plantilla utilizada y también revisar los detalles y las simulaciones generados automáticamente.';
                    throw new \yii\base\Exception($msg);
                } elseif ($saldo['debe'] == 0)
                    throw new \yii\base\Exception("El monto de la nota no puede ser cero");

                $model->total = $saldo['debe']; // para evitar trampa

                $this->manejarDetalles($detalleCompra, $model, $factura_model);

                // Guardar detalles de venta
                foreach ($detalleCompra as $detalle) {
                    if (!$detalle->save(false)) {
                        throw new \Exception("Error guardando detalle de compra: {$detalle->getErrorSummaryAsString()}");
                    }
                }
                // Procesar y guardar ivas cuentas.
                $total_factura = 0;
                /** @var CompraIvaCuentaUsada $ivacta */
                foreach ($ivacuenta_guardar as $ivacta) {
                    if (!$ivacta->save(false)) {
                        throw new \Exception("Error interno guardando iva cuenta: {$ivacta->getErrorSummaryAsString()}");
                    }
                    $ivacta->refresh();
                    $total_factura += round($ivacta->monto, 2);
                }

                $model->total = $model->saldo = $total_factura;
                if (!$model->save()) { // || !$factura_model->save()) {
                    throw new \yii\base\Exception("Error guardando nota: {$model->getErrorSummaryAsString()}");
                }

                if (isset($factura_model)) {
                    $factura_model->timbrado_id = $factura_model->ruc = $factura_model->nombre_entidad = '-';
                    if (!$factura_model->save()) {
                        throw new \yii\base\Exception("Error actualizando factura asociada Nº {$factura_model->nro_factura}: {$factura_model->getErrorSummaryAsString()}");
                    }
                }
                $model->refresh();

                if (empty($model->detallesCompra))
                    throw new \Exception("Falta crear al menos un detalle de venta.");

                if (isset($model->facturaCompra)) {
                    $factura = $model->facturaCompra;
                    $factura->actualizarSaldo();
                    if (!$factura->save(false))
                        throw new Exception("No se pudo actualizar saldo de la factura {$factura->nro_factura}: {$factura->getErrorSummaryAsString()}");
                }

                // Manejar activos fijos.
                !isset($factura_model) || $result = $this->manejarActivosFijos($model, $factura_model, $model->factura_compra_id);

                // Manejar activos biologicos
                /** @var ActivoBiologicoStockManager[] $managersNota */
                !isset($factura_model) || $this->manejarActivosBiologicos($model, $factura_model, null);

                $trans->commit();
                FlashMessageHelpsers::createSuccessMessage('La Nota se ha generado correctamente.');
                return $this->redirect(['index']);

            } catch (\Exception $e) {
                //throw $e;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($e->getMessage());
            }
        }

        $model->id = null;
        $model->formatDateForViews();
        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Compra (as Nota) model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param mixed $_pjax
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionUpdate($id, $_pjax = null)
    {
        $model = $this->findModel($id);
        $detalles_bd = $model->detallesCompra;
        $entidad = Entidad::findOne(['id' => $model->entidad_id]);
        $model->ruc = $entidad != null ? $entidad->ruc : null;
        $model->nombre_entidad = $entidad != null ? $entidad->razon_social : null;

        $timbradoDetalleViejo = TimbradoDetalle::findOne(['id' => $model->timbrado_detalle_id]);  #se usa asi en vez de obtener por atributo para evitar que sean la misma referencia.
        $model->timbrado = $model->timbradoDetalle->timbrado->nro_timbrado;
        $model->timbrado_id = $model->timbradoDetalle->timbrado_id;
        $session = Yii::$app->session;
        $old_factura_id = $model->factura_compra_id;
        $ivasCuentasOriginales = CompraIvaCuentaUsada::findAll(['factura_compra_id' => $id]);

        if ($_pjax == null && !Yii::$app->request->isPost) {
            self::removeSessionVariables();

            // Carga detalles de nota.
            $detalles = new ArrayDataProvider([
                'allModels' => DetalleCompra::find()->where(['factura_compra_id' => $model->id])->orderBy('cta_contable')->all(),
                'pagination' => false,
            ]);
            $session->set('cont_detallecompra-provider', $detalles);

            // Carga datos de activo fijo (copiado desde actionManejarDesdeNotaCreditoCompra()
            $compra_id = $model->factura_compra_id;
            $compraNotaCredito_id = $model->id;
            $ids = [];
            foreach (ActivoFijoStockManager::findAll(['factura_compra_id' => $compra_id, 'compra_nota_credito_id' => $compraNotaCredito_id]) as $manager) {
                $ids[] = $manager->activo_fijo_id;
                $ids_activo_fijo_compra[] = $manager->activo_fijo_id;
            }
            $ids = [
                'compra_id' => $model->factura_compra_id,
                'ids' => $ids,
            ];
            if (!Yii::$app->session->has('cont_activofijo_ids')) {
                Yii::$app->session->set('cont_activofijo_ids', $ids);
            }

            #cargar plantillas
            foreach ($model->ivasCuentaUsadas as $ivasCuentaUsada) {
                $model->_iva_ctas_usadas[$ivasCuentaUsada->plantilla_id][isset($ivasCuentaUsada->ivaCta) ? $ivasCuentaUsada->ivaCta->iva->porcentaje : '0'] = $ivasCuentaUsada->monto; // para mostrar por si hay error
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            $getTipoDoc = function ($tipoNota) {
                if ($tipoNota == 'nota_credito')
                    return Compra::getTipodocNotaCredito();
                else
                    return Compra::getTipodocNotaDebito();
            };
            try {
                self::manejarCambioDeFactura($model);
                $factura_model = Compra::findOne($model->factura_compra_id);
                // datos estirados de la factura de compra
                if (isset($factura_model)) {
                    $model->entidad_id = $factura_model->entidad_id;
                    $model->obligacion_id = $factura_model->obligacion_id;
                    $model->para_iva = $factura_model->para_iva;
                    $model->cant_cuotas = $factura_model->cant_cuotas;
                    if ($model->cant_cuotas == '') $model->cant_cuotas = 1;
                } else {
                    $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                    $model->entidad_id = $entidad->id;
                    $model->para_iva = 'si';
                    $model->cant_cuotas = 1;
                }
                // propios de la nota
                $model->estado = 'vigente';
                $model->tipo_documento_id = $getTipoDoc($model->tipo)->id;
                $model->condicion = $model->tipoDocumento->condicion;
                $model->fecha_vencimiento = date('Y-m-d', strtotime($model->fecha_emision . ' +1 month'));

                // campos que son requeridos que no son columnas en db.
                (!isset($factura_model)) || $factura_model->ruc = $factura_model->nombre_entidad = $factura_model->timbrado_id = '-';
                $model->formatDateForSave();
                if (!$model->validate()) {
                    throw new \yii\base\Exception("Error validando formulario: {$model->getErrorSummaryAsString()}");
                }

                $model->total = 0;

                preg_match('/^((\d+)-(\d+))-(\d+)$/', $model->nro_factura, $match);
                $nro = (int)$match[4];
                $prefijo = $match[1];
                /** @var TimbradoDetalle $timbradoDetalle */
                $timbradoDetalle = TimbradoDetalle::find()
                    ->joinWith('timbrado as timbrado')
                    ->where([
                        'timbrado.id' => $model->timbrado_id,
                        'prefijo' => $prefijo,
                        'tipo_documento_set_id' => $model->tipoDocumento->tipo_documento_set_id
                    ])
                    ->andWhere(['<=', 'nro_inicio', $nro])
                    ->andWhere(['>=', 'nro_fin', $nro])
                    ->one();
                // se copia exacta
                if ($timbradoDetalle == null) {
                    //No existe el numero de factura en el rango del timbrado
                    //Se crea un detalle para el timbrado
                    $newDetalleTimbrado = new TimbradoDetalle2();  #Se usa esta clase para que valide que no haya otro tim con fechas y prefijos iguales
                    $newDetalleTimbrado->prefijo = $prefijo;
                    $newDetalleTimbrado->timbrado_id = $model->timbrado_id;
                    $newDetalleTimbrado->nro_inicio = $nro;
                    $newDetalleTimbrado->nro_fin = $nro;
                    $newDetalleTimbrado->nro_actual = $nro;
                    $newDetalleTimbrado->tipo_documento_set_id = $model->tipoDocumento->tipo_documento_set_id;
                    if (!$newDetalleTimbrado->save()) {
                        throw new \Exception("Error guardando nuevo detalle timbrado: {$newDetalleTimbrado->getErrorSummaryAsString()}");
                    }
                    $newDetalleTimbrado->refresh();
                    $model->timbrado_detalle_id = $newDetalleTimbrado->id;
                } else {
                    $timbradoDetalle->nro_actual = $nro; # TODO: Se debe actualizar? No es solo desde ventas?
                    if (!$timbradoDetalle->save()) {
                        throw new \Exception("Error guardando timbrado detalle: {$timbradoDetalle->getErrorSummaryAsString()}");
                    }
                    $timbradoDetalle->refresh();
                    $model->timbrado_detalle_id = $timbradoDetalle->id;
                }

                $ningun_total = true;
                $ivacuenta_guardar = [];

                // ahora que se permite cambiar de factura, es necesario borrar las ivas cuentas originales.
                foreach ($ivasCuentasOriginales as $item) $item->delete();
                $this->manejarIvasCuentasUsadas($model, $ivacuenta_guardar, $ningun_total, $factura_model);
                if ($ningun_total) {
                    $msg = 'Falta completar al menos un campo del Total Iva o Total Exenta.';
                    throw new \yii\base\Exception($msg);
                }

                // No permitir guardar factura sin detalle
                $detallesCompra = $session['cont_detallecompra-provider']->allModels;
                if (empty($detallesCompra)) {
                    $msg = 'Faltan los detalles de la factura';
                    throw new \yii\base\Exception($msg);
                }

                // Asociar a la cabecera, sumar todos los saldos. Validar
                $saldo = ['debe' => 0.0, 'haber' => 0.0];
                /** @var DetalleCompra $detalle */
                foreach ($detallesCompra as $detalle) {
                    $saldo[$detalle->cta_contable] += (float)$detalle->subtotal;
                    $detalle->factura_compra_id = $id;
                    if (!$detalle->validate()) {
                        $msg = 'Error en detalle de factura: ' . $detalle->planCuenta->nombre . ": " . $detalle->getErrorsEnString();
                        throw new \yii\base\Exception($msg);
                    }
                }

                $this->manejarDetalles($detallesCompra, $model, $factura_model);

                if ($saldo['debe'] !== $saldo['haber']) {
                    $msg = 'El DEBE y el HABER NO son iguales. Se recomienda verificar cómo está guardada la plantilla
                     utilizada y también revisar los detalles y las simulaciones generados automáticamente.';
                    throw new \yii\base\Exception($msg);
                } elseif ($saldo['debe'] == 0)
                    throw new \yii\base\Exception("El monto de la nota no puede ser cero");

                $model->total = $saldo['haber'];
                $model->saldo = 0;

                $result = Compra::checkNroFactura($model->nro_factura, $model->id, $model->tipo, $model->entidad_id);
                if ($result != Compra::NRO_FACTURA_CORRECTO && $result != Compra::NRO_FACTURA_DUPLICADO) {
                    $msg = '';
                    switch ($result) {
                        case Compra::NRO_FACTURA_NO_RANGO:
                            $msg = 'Este nro de nota no corresponde a ningún rango de ningún timbrado.';
                            break;
                        case Compra::NRO_FACTURA_NO_PREFIJO:
                            $msg = 'Este prefijo de nota no existe. DEBE CREAR desde el menú de Timbrado primero.';
                    }
                    throw new \yii\base\Exception($msg);
                }

                // Guardar los models y comprometer transaccion
                foreach ($detallesCompra as $detalle) {
                    /** @var DetalleCompra $detalle_bd */
                    $i = null;
                    foreach ($detalles_bd as $index => $detalle_bd) {
                        if ($detalle_bd->plan_cuenta_id == $detalle->plan_cuenta_id && $detalle_bd->cta_contable == $detalle->cta_contable) {
                            $detalle_bd->subtotal = $detalle->subtotal;
                            $detalle = $detalle_bd;
                            $i = $index;
                            break;
                        }
                    }

                    if (!$detalle->save(false)) {
                        throw new \yii\base\Exception("Error guardando detalles: {$detalle->getErrorSummaryAsString()}");
                    }
                    $detalle->refresh();

                    if (isset($i))
                        unset($detalles_bd[$i]);
                }

                //Borramos de la base de datos si corresponde
                /** @var DetalleCompra $detalle_bd */
                foreach (($detalles_bd = array_values($detalles_bd)) as $detalle_bd) {
                    $detalle_bd->delete();
                    $detalle_bd->refresh();
                }

                foreach ($ivacuenta_guardar as $ivacta) $ivacta->save(false);

                /*if (isset($factura_model))
                    if ($old_factura_id == $model->factura_compra_id && $model->total != $model->oldAttributes['total']) {
                        $oldFacturaModel = Compra::findOne(['id' => $old_factura_id]);
                        $oldFacturaModel->saldo += ($oldFacturaModel->condicion == 'credito') ? ($model->oldAttributes['total'] - $model->total) : 0;
                        $oldFacturaModel->ruc = $oldFacturaModel->nombre_entidad = $oldFacturaModel->timbrado_id = '-';
                        if (!$oldFacturaModel->save()) {
                            $msg = "Error actualizando saldo de factura asociada: {$oldFacturaModel->getErrorSummaryAsString()}";
                            throw new \yii\base\Exception($msg);
                        }
                    } else {
                        $factura_model->saldo -= ($factura_model->condicion == 'credito') ? $model->total : 0;
                        if (!$factura_model->save()) {
                            $msg = "Error actualizando saldo de nueva factura asociada: {$factura_model->getErrorSummaryAsString()}";
                            throw new \yii\base\Exception($msg);
                        }
                    }*/

                $model->formatDateForSave();
                if (!$model->save()) {
                    throw new \yii\base\Exception("Error guardando nota: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                if (empty($model->detallesCompra))
                    throw new \Exception("Falta crear al menos un detalle de venta.");

                // restaurar saldo de factura original
                $old_factura = Compra::findOne(['id' => $old_factura_id]);
                if (isset($old_factura))
                    $old_factura->actualizarSaldo();
                // actualizar saldo de factura actual
                if (isset($factura_model)) {
                    $factura_model->refresh();
                    $factura_model->actualizarSaldo();
                }

                // Manejar activos fijos.
                !isset($factura_model) || $this->manejarActivosFijos($model, $factura_model, $old_factura_id);

                // Manejar activos biologicos
                /** @var ActivoBiologicoStockManager[] $managersNota */
                /** @var ActivoBiologicoStockManager[] $oldManagersNota */
                !isset($factura_model) || $this->manejarActivosBiologicos($model, $factura_model, $old_factura_id);

                #Eliminar detalle y cabecera del timbrado anterior si ya no tiene mas notas asociadas
                if (isset($timbradoDetalleViejo)) {
                    if ($model->timbrado_detalle_id != $timbradoDetalleViejo->id) {
                        // Si se esta editando una nota sin timbrado, el $oldTimbradoDetalle es cargado con `null`. Pero muy poco probable porque desde este abm se cargan solamente notas y no una 'boleta de nota de credito' por ej.
                        if (!$timbradoDetalleViejo->getFacturasVenta()->exists() and !$timbradoDetalleViejo->getFacturasCompra()->exists()) {
                            $timbradoAsociado = $timbradoDetalleViejo->timbrado;
                            if (!$timbradoDetalleViejo->delete())
                                throw new \Exception("Error interno: No se puede borrar el timbrado detalle anterior (sin notas): {$timbradoDetalleViejo->getErrorSummaryAsString()}");

                            #Borrar timbrado si su unico detalle era el $oldTimbradoDetalle
                            $timbradoAsociado->refresh();
                            if (sizeof($timbradoAsociado->timbradoDetalles) == 0)
                                if (!$timbradoAsociado->delete())
                                    throw new \Exception("Error interno: No se puede borrar el timbrado (sin detalles): {$timbradoAsociado->getErrorSummaryAsString()}");
                        }
                    }
                }

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("La nota \"{$model->nro_factura}\" se ha editado correctamente.", 10000);
                return $this->redirect(['index']);

            } catch (\Exception $e) {
                //throw $e;
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($e->getMessage());
            }
        }

        $model->formatDateForViews();
        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Miercoes 13, abril 2019: Segun correo de Magali, se desea poder modificar la factura asociada.
     * @param Compra $nota
     * @throws Exception
     */
    private function manejarCambioDeFactura(&$nota)
    {
        // Si son las mismas facturas, no hacer nada.
        if ($nota->oldAttributes['factura_compra_id'] == $nota->factura_compra_id)
            return;

        $factura_original = Compra::findOne(['id' => $nota->oldAttributes['factura_compra_id']]);
        // Si no se pudo obtener la factura asociada, lanzar exception
        if (!isset($factura_original))
            return;
//            throw new \Exception("Error interno: No se puede obtener la factura originalmente asociada a esta
//             nota en edición");

        // Si la factura originalmente asociada es al contado, no hacer nada.
        if ($factura_original->condicion == 'contado') return;

        // La factura original es a credito.
        $factura_original->saldo += $nota->oldAttributes['total'];
        $factura_original->nombre_entidad = $factura_original->timbrado = $factura_original->timbrado_id = $factura_original->ruc = '-';
        if (!$factura_original->save())
            throw new \Exception("Error interno restableciendo saldo de compra anterior:
             {$factura_original->getErrorSummaryAsString()}");
    }

    /**
     * @param DetalleCompra[] $detalles
     * @param Compra $model
     * @param Compra $factura_model
     * @throws Exception
     */
    private function manejarDetalles($detalles, &$model, &$factura_model)
    {
        if (!isset($factura_model)) return;

        $totalNotas = 0.0;
        // acumular total de las notas existentes excepto el actual
        foreach ($factura_model->notas as $nota) {
            if ($nota->id != $model->id) {
                $totalNotas += (float)$nota->total;
            }
        }
        $total = $totalNotas + (float)$model->total;
        if ($total > $factura_model->total) {
            $maxTotalAllowed = (float)$factura_model->total - $totalNotas;
            $maxTotalAllowed = number_format($maxTotalAllowed, 2, ',', '.');
            throw new \Exception("El total de la nota es mayor al valor permitido: {$maxTotalAllowed}.");
        }
    }

    /**
     * @param Compra $model Modelo de nota.
     * @param Compra $fatura_model Modelo de factura asociada.
     * @param null $old_factura_id
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    private function manejarActivosBiologicos(&$model, &$fatura_model, $old_factura_id = null)
    {
        $sesion = Yii::$app->session;
        $sesion_key = 'cont_activo_bio_stock_manager';
        $actbios = $fatura_model->getActivosBiologicos();

        // Borra stock managers de la nota por factura anterior.
        /** @var ActivoBiologicoStockManager[] $old_managersNota */
        $old_managersNota = ActivoBiologicoStockManager
            ::findAll(['nota_compra_id' => $model->id, 'factura_compra_id' => $old_factura_id]);
        foreach ($old_managersNota as $managerNota) {
            $aBio = $managerNota->activoBiologico;
            $aBio->isEditable();
            if (!$managerNota->delete()) {
                throw new Exception("Error interno borrando stock manager: {$managerNota->getErrorSummaryAsString()}.");
            }
            //$managerNota->refresh();
            $aBio->refresh();
            $aBio->recalcularStockActual();
//                $aBio->recalcularPrecioUnitarioStockActuales();
            if (!$aBio->save()) {
                throw new Exception("Error actualizando stock de activo biológico {$aBio->nombre}: {$aBio->getErrorSummaryAsString()}");
            }
        }

        // Si no es nota de credito o si $actbios es vacio, retornar.
        // Si $actbios es vacio, la compra actual no es de activos biologicos.
        if (!$model->isNotaCredito() || empty($actbios)) return;

        // Si la factura es de compra de abios pero no se especifico nada en la sesion, error.
        if (!empty($actbios) && (!$sesion->has($sesion_key))) {
            throw new \yii\base\Exception("Debe especificar los Activos Biológicos que fueron devueltos. 
            Utilice el botón verde en la fila de plantillas correspondiente a Activo Biológico.");
        }

        $sessionData = $sesion->get($sesion_key);
        $dataProvider = $sessionData['dataProvider'];
        $compra_id = $sessionData['compra_id']; // compra asociada, desde modal.
        $allModels = $dataProvider->allModels;  // stock managers, desde modal.

        // Si cambio de factura pero no actualizo la informacion de activos biologicos (managers), error.
        if ($compra_id != $fatura_model->id) {
            throw new \Exception("Se ha cambiado de factura pero no ha actualizado los datos del activo fijo asociado.");
        }

        /** @var ActivoBiologicoStockManager[] $managersCompra */
        /** @var ActivoBiologicoStockManager[] $managersNota */
        $managersCompra = ActivoBiologicoStockManager::find()
            ->where(['factura_compra_id' => $fatura_model->id])->andWhere(['IS', 'nota_compra_id', null])->all();
        $managersNota = $allModels;

        // Si la cantidad indicada a devolver es mayor a lo comprado, error.
        self::checkCantidadDevuelta($managersNota, $managersCompra);

        // Guarda stock managers nuevos de la nota.
        // Actualiza stock actual de los activos biologicos correspondientes.
        foreach ($managersNota as $_managerNota) {
            $managerNota = new ActivoBiologicoStockManager();
            $managerNota->setAttributes($_managerNota->attributes);
            $managerNota->factura_compra_id = $model->factura_compra_id;
            $managerNota->nota_compra_id = $model->id;
            if (!$managerNota->save()) {
                throw new \yii\base\Exception("Error interno guardando stock manager: 
                        {$managerNota->getErrorSummaryAsString()}");
            }
            $managerNota->refresh();
            $actbio = $managerNota->activoBiologico;
            if ($actbio->getInventario()->exists())
                throw new \yii\base\Exception("Se intentaba actualizar el stock actual del activo biológico {$actbio->nombre} pero se encuentra registrado en el cierre de inventario. Por tanto no puede modificar.");
            $actbio->recalcularStockActual();
//            $actbio->recalcularPrecioUnitarioStockActuales();
            if (!$actbio->save()) {
                throw new \yii\base\Exception("Error actualizando activo biológico {$actbio->nombre}:
                         {$actbio->getErrorSummaryAsString()}");
            }
        }
    }

    /**
     * @param ActivoBiologicoStockManager[] $managersNota
     * @param ActivoBiologicoStockManager[] $managersCompra
     * @throws \yii\base\Exception
     */
    private function checkCantidadDevuelta($managersNota, $managersCompra)
    {
        foreach ($managersNota as $index => $manNota) {
            foreach ($managersCompra as $manCompra) {
                if ($manNota->especie_id == $manCompra->especie_id && $manNota->clasificacion_id == $manCompra->clasificacion_id
                    && $manNota->cantidad > $manCompra->cantidad) {
                    $msg = "La cantidad devuelta ({$manNota->cantidad})
                    no puede ser mayor a la cantidad comprada ({$manCompra->cantidad})";
                    $managersNota[$index]->addError('cantidad', $msg);
                    throw new \yii\base\Exception("Error en los datos del activo biológico {$manNota->activoBiologico->nombre}: {$msg}");
                }
            }
        }
    }

    /**
     * @param Compra $model Modelo para Nota
     * @param Compra $factura_model Modelo de factura
     * @param int $old_factura_id
     * @throws Exception
     */
    private function manejarActivosFijos(&$model, &$factura_model, $old_factura_id)
    {
        $model->refresh();
        $factura_model->refresh();
        $skey = 'cont_activofijo_ids';
        $dataProvider = Yii::$app->session->get($skey);
        $compra_id = $dataProvider['compra_id'];
        $ids = $dataProvider['ids'];
        $es_compra_de_activo_fijo = $factura_model->getActivoFijos()->exists();

        // activos fijos y managers anteriores.
        // Si es nueva nota, o la factura anterior no era de afijo, no deberia entrar en este foreach.
        foreach (ActivoFijoStockManager::findAll(['factura_compra_id' => $old_factura_id,
            'compra_nota_credito_id' => $model->id]) as $oldStockManager) {

            // restaurar activos fijos
            /** @var ActivoFijo $oldAFijo */
            $oldAFijo = $oldStockManager->activoFijo;
            $oldAFijo->setFacturaCompraId($old_factura_id);
            $oldAFijo->setEstado('activo');
            if (!$oldAFijo->save()) throw new \Exception("Error interno validando activo fijo anterior
                 (id: {$oldAFijo->id}): {$oldAFijo->getErrorSummaryAsString()}");;
            $oldAFijo->refresh();

            if (!$oldStockManager->delete()) {
                throw new \Exception("Error interno borrando stock manager anterior: {$oldStockManager->getErrorSummaryAsString()}");
            }
        }

        if (!$es_compra_de_activo_fijo) {
            $manager = ActivoFijoStockManager::find()->where(['factura_compra_id' => $factura_model->id, 'compra_nota_credito_id' => $model->id]);
            $es_compra_de_activo_fijo = $es_compra_de_activo_fijo || $manager->exists();
        }

        if ($es_compra_de_activo_fijo && Yii::$app->session->has($skey)) {
            // Si cambio de factura pero no se actualizo la sesion.
            if ($factura_model->id != $compra_id) {
                throw new \Exception("Se ha cambiado de factura asociada. Sin embargo los datos de activos fijos no se han actualizado.");
            }

            // Si no se especifico los activos fijos a devolver.
            if (!sizeof($ids)) {
                throw new \Exception("Debe especificar los Activos Fijos que fueron devueltos. 
                 Utilice el botón verde en la fila de plantillas correspondiente a Activo Fijo.");
            }

            // activos fijos y managers nuevos, correspondientes a los ids de activos fijos guardados en sesion.
            $costo_adq_total = -1;
            foreach ($ids as $id) {
                $newActivoFijo = ActivoFijo::findOne(['id' => $id]);
//                // no se permiten activos fijos ya asentados.?
//                if ($newActivoFijo->getAsiento()->exists())
//                    throw new \Exception("Error desvinculando Activo Fijo '{$newActivoFijo->nombre}'
//                    (ID: {$newActivoFijo->id}): Este Activo Fijo tiene asociado asientos.");
                $newActivoFijo->setFacturaCompraId(null); // desasociar de la factura de venta.;
                $newActivoFijo->setEstado('activo');
                if (!$newActivoFijo->save()) throw new \Exception("Error interno validando nuevo activo: {$newActivoFijo->getErrorSummaryAsString()}");;
                $newActivoFijo->refresh();

                if ($costo_adq_total == -1) $costo_adq_total = 0;
                $costo_adq_total += (float)$newActivoFijo->costo_adquisicion; // acumula el costo de adquisicion

                $newStockManager = new ActivoFijoStockManager();
                $newStockManager->setEmpresaPeriodoActuales(Yii::$app->session->get('core_empresa_actual'), Yii::$app->session->get('core_empresa_actual_pc'));
                $newStockManager->setFacturaCompraId($model->factura_compra_id);
                $newStockManager->setActivoFijoId($newActivoFijo->id);
                $newStockManager->setCompraNotaCreditoId($model->id);
                if (!$newStockManager->save()) throw new \Exception("Error interno validando nuevo stock manager: {$newStockManager->getErrorSummaryAsString()}");;
                $newStockManager->refresh();
            }

            // compara el total de costo de adquisicion contra el total de la factura
            if ($costo_adq_total != -1) {
                $costo_adq_total *= 1.1;
                $costo_adq_total = number_format($costo_adq_total, 0, '.', '');

//                echo abs((float)$model->total - (float)$costo_adq_total); exit();
                if (abs((float)$model->total - (float)$costo_adq_total) > 1) {
                    $factura_total = number_format($model->total, 2, ',', '.');
                    $costo_adq_total = number_format($costo_adq_total, 2, ',', '.');
                    throw new \Exception("Error en el monto de la factura: La suma de costo de adquisicion
                         {$costo_adq_total} no es igual al monto {$factura_total} de la factura");
                }
            }

            // Si cambio de factura, restaurar activos de la factura anterior
            Yii::warning("{$old_factura_id}, {$model->factura_compra_id}");
            if ($old_factura_id != $model->factura_compra_id) {
                $oldFacturaModel = Compra::findOne(['id' => $old_factura_id]);
                if (isset($oldFacturaModel) && $oldFacturaModel->getActivoFijos()->exists()) {
                    foreach (ActivoFijoStockManager::findAll(['factura_compra_id' => $old_factura_id,
                        'compra_nota_credito_id' => $model->id]) as $oldStockManager) {

                        // restaurar activos fijos
                        /** @var ActivoFijo $oldAFijo */
                        $oldAFijo = $oldStockManager->activoFijo;
                        $oldAFijo->setFacturaCompraId($old_factura_id);
                        $oldAFijo->setEstado('activo');
                        if (!$oldAFijo->save()) throw new \Exception("Error interno restableciendo activo fijo de la factura anterior
                         (id: {$oldAFijo->id}): {$oldAFijo->getErrorSummaryAsString()}");;
                        $oldAFijo->refresh();

                        if (!$oldStockManager->delete()) {
                            throw new \Exception("Error interno borrando stock manager de la factura anterior: {$oldStockManager->getErrorSummaryAsString()}");
                        }
                    }
                }
            }
        } elseif ($es_compra_de_activo_fijo && !Yii::$app->session->has($skey)) {
            throw new \Exception("Debe especificar los Activos Fijos que fueron devueltos. 
            Utilice el botón verde en la fila de plantillas correspondiente a Activo Fijo.");
        }
        $session = Yii::$app->session;
//        echo "es compra de activo fijo: {$es_compra_de_activo_fijo} y en la sesion hay: {$session->has($skey)}";
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model_nota = $this->findModel($id);
        $timbradoDetalleID = $model_nota->timbrado_detalle_id;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $ivaCuentas = CompraIvaCuentaUsada::find()->where([
                'factura_compra_id' => $id,
                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
                ->all();
            $detallesFactura = $model_nota->detallesCompra;

            /** @var CompraIvaCuentaUsada $ic */
            foreach ($ivaCuentas as $ic) {
                if (!$ic->delete()) {
                    throw new Exception("Error interno borrando iva cuentas: {$ic->getErrorSummaryAsString()}.");
                }
            }
            /** @var DetalleCompra $df */
            foreach ($detallesFactura as $df) {
                if (!$df->delete()) {
                    throw new Exception("Error borrando detalles de factura: {$df->getErrorSummaryAsString()}.");
                }
            }

            if (isset($model_nota->facturaCompra)) {
                $factura_model = Compra::findOne($model_nota->factura_compra_id);

                // Manejo de activos fijos y sus managers.
                /** @var ActivoFijoStockManager $stockManager */
                foreach (ActivoFijoStockManager::findAll(['factura_compra_id' => $factura_model->id]) as $stockManager) {
                    $aFijo = $stockManager->activoFijo;
                    $aFijo->setEstado('activo');
                    $aFijo->setFacturaCompraId($stockManager->factura_compra_id);
                    if (!$aFijo->save()) {
                        throw new Exception("Error validando la restauración de activo fijo desasociado: {$aFijo->getErrorSummaryAsString()}.");
                    }
                    if (!$stockManager->delete()) {
                        throw new Exception("Error interno borrando stock manager: {$stockManager->getErrorSummaryAsString()}.");
                    }
                    $stockManager->refresh();
                }
            }

            // Manejo de activos biologicos
            /** @var ActivoBiologicoStockManager $stockManager */
            foreach (ActivoBiologicoStockManager::findAll(['nota_compra_id' => $model_nota->id]) as $stockManager) {
                $aBio = $stockManager->activoBiologico;
                $aBio->isEditable();
                if (!$stockManager->delete()) {
                    throw new Exception("Error interno borrando stock manager: {$stockManager->getErrorSummaryAsString()}.");
                }
                $stockManager->refresh();
                $aBio->recalcularStockActual();
                if (!$aBio->save()) {
                    throw new Exception("Error actualizando stock de activo biológico {$aBio->nombre}: {$aBio->getErrorSummaryAsString()}");
                }
            }

            if (!$model_nota->delete()) {
                throw new Exception("Error borrando nota: {$model_nota->getErrorSummaryAsString()}.");
            }
            $model_nota->refresh();

            if (isset($factura_model)) {
                $factura_model->refresh();
                $factura_model->actualizarSaldo(true);
            }

            #Borrar talonario
            $timbradoDetalle = TimbradoDetalle::findOne(['id' => $timbradoDetalleID]);
            if (isset($timbradoDetalle)) {
                // Si se esta editando una nota sin timbrado, el $oldTimbradoDetalle es cargado con `null`. No creo que pase ya que es solamente notas de credito y no hay 'boleta de nota de credito' por ej.
                if (!$timbradoDetalle->getFacturasVenta()->exists() and !$timbradoDetalle->getFacturasCompra()->exists()) {
                    $timbradoAsociado = $timbradoDetalle->timbrado;
                    if (!$timbradoDetalle->delete())
                        throw new \Exception("Error interno: No se puede borrar el timbrado detalle anterior (sin notas): {$timbradoDetalle->getErrorSummaryAsString()}");

                    #Borrar timbrado si su unico detalle era el $timbradoDetalle.
                    $timbradoAsociado->refresh();
                    if (sizeof($timbradoAsociado->timbradoDetalles) == 0)
                        if (!$timbradoAsociado->delete())
                            throw new \Exception("Error interno: No se puede borrar el timbrado (sin detalles): {$timbradoAsociado->getErrorSummaryAsString()}");
                }
            }

            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage('La nota se ha borrado exitosamente.');
        } catch (Exception $e) {// throw $e;
            $transaction->rollBack();
            FlashMessageHelpsers::createErrorMessage($e->getMessage());
        }

        return $this->redirect(['index']);
    }

    public function actionGetFacturasByRuc($ruc, $q = null, $tipo = 'factura')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Compra::getFacturasListaByRucForNotas($ruc, $q, $tipo);
    }

    public function actionGetDatosFacturasById()
    {
        $factura_id = Yii::$app->request->get('factura_id');
        $action = Yii::$app->request->get('action');
        $nota_id = Yii::$app->request->get('nota_id');
        $notaTipo = Yii::$app->request->get('notaTipo');
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Compra::actionGetDatosFacturasByIdForNotas($factura_id, $notaTipo, $action, $nota_id);
    }

    /**
     * @param CompraIvaCuentaUsada $iva_cta_nota
     * @param Compra $factura_model
     * @return bool
     */
    private function checkMontosIvaConFactura($iva_cta_nota, $factura_model)
    {
//        foreach ($factura_model->ivasCuentaUsadas as $iva_cta_factura) {
//            echo "{$iva_cta_factura->id}, ivacuenta factura: {$iva_cta_factura->iva_cta_id}, plantilla factura: {$iva_cta_factura->plantilla_id}, ivacuenta nota: {$iva_cta_nota->iva_cta_id}, plantilla nota: {$iva_cta_nota->plantilla_id}<br/>";
//        }
//        echo '<br/>';

        if (isset($factura_model)) {
            $_factura_iva_monto = 0;
            $entroa = false;
            foreach ($factura_model->ivasCuentaUsadas as $iva_cta_factura) {
                if ($iva_cta_factura->iva_cta_id == $iva_cta_nota->iva_cta_id && $iva_cta_factura->plantilla_id == $iva_cta_nota->plantilla_id) {
                    $_factura_iva_monto = floatval($iva_cta_factura->monto);
                    $entroa = true;
                    break;
                }
            }
            $entrob = false;
            $monto_existente = 0.00;
            foreach ($factura_model->notas as $_nota) {
                foreach ($_nota->ivasCuentaUsadas as $_ivaCtaNota) {
                    if ($_ivaCtaNota->iva_cta_id == $iva_cta_nota->iva_cta_id && $_ivaCtaNota->plantilla_id == $iva_cta_nota->plantilla_id && $_ivaCtaNota->id != $iva_cta_nota->id) {
                        $monto_existente += floatval($_ivaCtaNota->monto);
                        $entrob = true;
                        break;
                    }
                }
            }

//        Yii::warning("Nota actual: {$iva_cta_nota->monto}");
//        Yii::warning("Factura valor: {$_factura_iva_monto}");
//        Yii::warning("Nota anterior valor: {$monto_existente}");
            if ($iva_cta_nota->monto > ($_factura_iva_monto - $monto_existente)) {
//            var_dump($iva_cta_nota->monto, $_factura_iva_monto, $monto_existente, $entroa, $entrob);
//            Yii::warning('entro');
                return false;
            }
        }

        return true;
    }

    /**
     * Finds the Compra model (as Nota) based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Compra the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Compra::findOne(['id' => $id, ['not' => ['factura_compra_id' => null]]])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('No existe el registro solicitado.');
    }

    /**
     * @param Compra $model
     * @param array $ivacuenta_guardar
     * @param bool $ningun_total
     * @param Compra $factura_model
     * @throws \yii\base\Exception
     * @throws Exception
     */
    private function manejarIvasCuentasUsadas(&$model, &$ivacuenta_guardar, &$ningun_total, $factura_model)
    {
        $_err_iva = false;
        $ivacuenta_usadas = Yii::$app->request->post('CompraIvaCuentaUsada', []);
        foreach ($ivacuenta_usadas as $ivacuenta_usada) {
            foreach (IvaCuenta::find()->all() as $iva_cta) {
                if (!empty($ivacuenta_usada['ivas']['iva_' . $iva_cta->iva->porcentaje])) {
//                    $iva_x_value = str_replace(['.', ','], ['', '.'], $ivacuenta_usada['ivas']['iva_' . $iva_cta->iva->porcentaje]);
                    $iva_x_value = $ivacuenta_usada['ivas']['iva_' . $iva_cta->iva->porcentaje];
                    $ningun_total = false;
                    $ivacuenta_nota = CompraIvaCuentaUsada::findOne([
                        'factura_compra_id' => $model->id,
                        'iva_cta_id' => !empty($iva_cta->cuenta_compra_id) ? $iva_cta->id : null,
                        'plan_cuenta_id' => $iva_cta->cuenta_compra_id,
                        'plantilla_id' => $ivacuenta_usada['plantilla_id']
                    ]);
                    if (empty($ivacuenta_nota)) {
                        $ivacuenta_nota = new CompraIvaCuentaUsada();
                        $ivacuenta_nota->factura_compra_id = $model->id;
                        $ivacuenta_nota->iva_cta_id = isset($iva_cta->cuenta_compra_id) ? $iva_cta->id : null;
                        $ivacuenta_nota->plantilla_id = $ivacuenta_usada['plantilla_id'];
                        $ivacuenta_nota->plan_cuenta_id = $iva_cta->cuenta_compra_id;
                        $ivacuenta_nota->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                    }
                    $ivacuenta_nota->monto = $model->moneda_id == 1 ? round($iva_x_value) : $iva_x_value;
                    $model->_iva_ctas_usadas[$ivacuenta_nota->plantilla_id][isset($ivacuenta_nota->iva_cta_id) ? (int)$iva_cta->iva->porcentaje : 0] = $ivacuenta_nota->monto; // para mostrar por si hay error

                    // solo registro 1 error
                    if ($_err_iva === false) {
                        #solamente ejecutar checkMontosIvaConFactura si existe factura asociada.
                        if (isset($factura_model) && !$this->checkMontosIvaConFactura($ivacuenta_nota, $factura_model)) {
                            throw new \yii\base\Exception('El Monto Total del ' . (empty($iva_cta->iva->porcentaje) ? 'Exenta' : ('IVA ' . $iva_cta->iva->porcentaje . '%')) . ' no puede ser mayor al de la factura');
                        } elseif (!$ivacuenta_nota->validate()) {
                            throw new \yii\base\Exception('ERROR INTERNO al guardar nota: ' . implode(' ', $ivacuenta_nota->getErrorSummary(true)));
                        } elseif ($ivacuenta_nota->plantilla_id == '')
                            throw new Exception("Error interno: iva cuenta no tiene plantilla_id");
                    }

                    $ivacuenta_guardar[] = $ivacuenta_nota;
                }
            }
        }
    }

    public function actionGetEntidadesForNotas($action, $q = null, $tipo = 'factura')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        // Miercoes 13, abril 2019: Segun correo de Magali, se desea poder modificar la factura asociada.
//        if ($action == 'create')
        return Entidad::actionGetEntidadesForNotas('compra', $q, $tipo);
//        else
//            return [];
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionSimular()
    {
        /** @var Cotizacion $cotizacionFactura */
        /** @var DetalleCompra[] $detalles */
        /** @var DetalleCompra[] $detalles_para_sim */

        $es_nota_credito = Yii::$app->request->get('es_nota') == 'nota_credito';
        $hoy = date_create_from_format('d-m-Y', $_GET['hoy']);
        $total_factura = (float)$_GET['cont_factura_total'];
        $moneda_id = $_GET['moneda_id'];
        $detalles = Yii::$app->getSession()->get('cont_detallecompra-provider')->allModels;
        $factura_id = $_GET['factura_id'];
        $cotizacionNota = (float)$_GET['cotizacionNota'];
        $cotizacionFactura = (float)$_GET['cotizacionFactura'];


        // Cuando cambia de anulada/faltante a vigente, el campo fecha_emision es vacia y llega en POST vacio.
        if ($moneda_id != null && $moneda_id != 1 && $hoy == false) {
            FlashMessageHelpsers::createInfoMessage('Debe especificar una fecha para poder obtener la cotización a utilizar.');
//            $detalles_para_sim = [];
//            goto retorno;
            return false;
        }

        $detalles = DetalleCompra::agruparCuentas($detalles);
        $detalles_para_sim = [];

        if ($total_factura == 0 || sizeof($detalles) == 0) {
            $detalles_para_sim = [];
            FlashMessageHelpsers::createWarningMessage("Total de Nota es cero.");
            goto retorno;
        }

        // Convertir a guaranies si la moneda es extrangera.
        $diff = 0.0;
        $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
        if ($moneda_id != \common\models\ParametroSistema::getMonedaBaseId()) {
            $tipoDocumento = TipoDocumento::findOne(['id' => Yii::$app->request->post('tipo_documento_id')]);
            $diff = $cotizacionFactura - $cotizacionNota;
            $newDetalleDiffCambio = null;
            if ($diff != 0.0) {
                $newDetalleDiffCambio = new DetalleCompra();
                $newDetalleDiffCambio->cta_contable = $diff < 0.0 ? 'haber' : 'debe';
                // 8.05 = resultado por diferencia de cambio como ingresos
                // 13.04 = resultado por diferencia de cambio como pérdida

                $empresa_id = \Yii::$app->session->get('core_empresa_actual');
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                $newDetalleDiffCambio->plan_cuenta_id = PlanCuenta::findOne(['id' => $diff < 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor])->id; // TODO: parametro del modulo
                $newDetalleDiffCambio->subtotal = $diff * $total_factura;
                $diff < 0.0 ? $newDetalleDiffCambio->subtotal = ((float)$newDetalleDiffCambio->subtotal * -1.0) : true;
            }
            foreach ($detalles as $detalle) {
                $newDetalle = new DetalleCompra();
                $newDetalle->subtotal = (float)$detalle->subtotal * ($detalle->cta_contable == 'debe' ? $cotizacionNota : $cotizacionFactura);
                $newDetalle->plan_cuenta_id = $detalle->plan_cuenta_id;
                $newDetalle->cta_contable = $detalle->cta_contable;
                $detalles_para_sim[] = $newDetalle;
                if ($newDetalle->planCuenta->cod_completo == '1.01.03.05.03.01')
                    Yii::warning($detalle->subtotal . ' ' . ($detalle->cta_contable == 'debe' ? $cotizacionNota : $cotizacionFactura) . ' ' . $newDetalle->subtotal);
            }

            // insertar el asiento por diferencia de cambio
            if (isset($newDetalleDiffCambio)) {
                $detalles_para_sim[] = $newDetalleDiffCambio;
            }
        }
//        elseif ($cotizacionNota != '') {
//            foreach ($detalles as $detalle) {
//                $newDetalle = new DetalleCompra();
//                $newDetalle->subtotal = (float)$detalle->subtotal * (float)$cotizacionNota;
//                $newDetalle->plan_cuenta_id = $detalle->plan_cuenta_id;
//                $newDetalle->cta_contable = $detalle->cta_contable;
//                $detalles_para_sim[] = $newDetalle;
//            }
//        }
        else {
            // se usa moneda nacional, solo copiar los detalles al array para simulacion
            foreach ($detalles as $detalle) {
                $detalles_para_sim[] = $detalle;
            }
        }

        $detalles_para_sim = $this->agruparDebeHaber($detalles_para_sim);

        retorno:;
        // poner en sesion
        Yii::$app->getSession()->set('cont_detallecompra_sim-provider', new ArrayDataProvider([
            'allModels' => $detalles_para_sim,
            'pagination' => false,
        ]));
        return true;
    }

    public function actionCalcularDetalle()
    {
        // Obtener variables del POST
        $total_factura = $_POST['cont_factura_total'];
        $porcentajes_iva = $_POST['ivas'];
        $plantillas = Yii::$app->request->post('plantillas', []);
        $tipo_docu_id = $_POST['tipo_docu_id'];
        $moneda_id = $_POST['moneda_id'];
        $factura_id = $_POST['factura_id'];

        // Verificar tipo de documento
        $tipoDoc = TipoDocumento::findOne(['id' => $tipo_docu_id]);
        if ($tipoDoc == null) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar Tipo de Documento');
            return false;
        }

        // Verificar que se ha seleccionado una plantilla.
        $plantillasModel = [];
        $totales_valor = [];
        foreach ($plantillas as $pl) {
            //Valores por iva
            $valores_iva = [];
            foreach ($porcentajes_iva as $iva) {
                array_push($valores_iva, $pl['iva_' . $iva]);
            }

            $plantilla = PlantillaCompraventa::findOne(['id' => $pl['plantilla_id']]);
            if ($plantilla != null) {
                array_push($plantillasModel, $plantilla);
                $totales_valor[$plantilla->id] = $valores_iva;
            }
        }

        if (!isset($plantillasModel)) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar PLANTILLA');
            return false;
        }

        // Verificar que la plantilla seleccionada tenga detalles.
        /** @var PlantillaCompraventa $plantilla */
        foreach ($plantillasModel as $plantilla) {
            if (!$plantilla->esParaPrestamo() && !$plantilla->esParaCuotaPrestamo()) {
                $plantilla_detalles = $plantilla->getPlantillaCompraventaDetalles();
                if (!$plantilla_detalles->exists()) {
                    FlashMessageHelpsers::createWarningMessage('La plantilla seleccionada no tiene ningún detalle. AGREGUE PRIMERO.');
                    return false;
                }
            }
        }

        /** @var DetalleCompra[] $detalles */
        $detalles = [];

        // Crear detalles por Plantilla e IVA's usadas.
        if ($factura_id == '') $detalles = $this->genDetallesByDefault($detalles, $tipoDoc, $total_factura, $totales_valor, $porcentajes_iva, $plantillasModel, $moneda_id);
        else $detalles = $this->genDetallesFromFacturaAsoc($factura_id);
        // Actualizar indices en la sesion
        $detalles = array_values($detalles);

        // Agrupar cuentas iguales
        $detalles = DetalleCompra::agruparCuentas($detalles);

        // Agrupar debe/haber
        $detalles = $this->agruparDebeHaber($detalles);

//        // detalles de la factura asociada.
//        // si los detalles por plantilla existe en los de la factura, se actualiza el monto.
//        // si es nuevo se agrega.
//        $factura = Compra::findOne($factura_id);
//        $detallesFactura = [];
//        // invertir partida de los detalles de factura
//        $map = ['debe' => 'haber', 'haber' => 'debe'];
//        foreach ($factura->detallesCompra as $index => $detalleCompra) {
//            $newDetalle = new DetalleCompra();
//            $newDetalle->load(['DetalleCompra' => $detalleCompra->attributes]);
//            $newDetalle->cta_contable = $map[$detalleCompra->cta_contable];
//            unset($newDetalle->id);
//            $detallesFactura[] = $newDetalle;
//        }
//        $detallesNuevos = [];
//        foreach ($detalles as $detalle) {
//            $hay = false;
//            foreach ($detallesFactura as $key => $detalleFactura) {
//                if ($detalle->cta_contable == $detalleFactura->cta_contable && $detalle->plan_cuenta_id == $detalleFactura->plan_cuenta_id) {
//                    $detallesFactura[$key]->subtotal = $detalle->subtotal;
//                    $hay = true;
//                }
//            }
////            if (!$hay)
////                $detallesNuevos[] = $detalle;
//        }
//        $detalles = array_merge($detallesFactura, $detallesNuevos);
//        $detalles = $this->agruparDebeHaber($detalles);

        // guarda en la sesión
        retorno:;
        Yii::$app->getSession()->set('cont_detallecompra-provider', new ArrayDataProvider([
            'allModels' => $detalles,
            'pagination' => false,
        ]));
        return true;
    }

    public function actionCalcularDetalleUpdate()
    {
        // Obtener variables del POST
        $total_factura = $_POST['cont_factura_total'];
        $porcentajes_iva = $_POST['ivas'];
        $plantillas = Yii::$app->request->post('plantillas', []);
        $tipo_docu_id = $_POST['tipo_docu_id'];
        $moneda_id = $_POST['moneda_id'];
        $factura_id = $_POST['factura_id'];
        $nota_id = $_POST['nota_id'];

        // Verificar tipo de documento
        $tipoDoc = TipoDocumento::findOne(['id' => $tipo_docu_id]);
        if ($tipoDoc == null) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar Tipo de Documento');
            return false;
        }

        // Verificar que se ha seleccionado una plantilla.
        $plantillasModel = [];
        $totales_valor = [];
        foreach ($plantillas as $pl) {
            //Valores por iva
            $valores_iva = [];
            foreach ($porcentajes_iva as $iva) {
                array_push($valores_iva, $pl['iva_' . $iva]);
            }

            $plantilla = PlantillaCompraventa::findOne(['id' => $pl['plantilla_id']]);
            if ($plantilla != null) {
                array_push($plantillasModel, $plantilla);
                $totales_valor[$plantilla->id] = $valores_iva;
            }
        }

        if (!isset($plantillasModel)) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar PLANTILLA');
            return false;
        }

        // Verificar que la plantilla seleccionada tenga detalles.
        /** @var PlantillaCompraventa $plantilla */
        foreach ($plantillasModel as $plantilla) {
            if (!$plantilla->esParaPrestamo() && !$plantilla->esParaCuotaPrestamo()) {
                $plantilla_detalles = $plantilla->getPlantillaCompraventaDetalles();
                if (!$plantilla_detalles->exists()) {
                    FlashMessageHelpsers::createWarningMessage('La plantilla seleccionada no tiene ningún detalle. AGREGUE PRIMERO.');
                    return false;
                }
            }
        }

        /** @var DetalleCompra[] $detalles */
        $detalles = [];

        // Crear detalles por Plantilla e IVA's usadas.
        if ($factura_id == '') $detalles = $this->genDetallesByDefault($detalles, $tipoDoc, $total_factura, $totales_valor, $porcentajes_iva, $plantillasModel, $moneda_id);
        else {
            $detalles = $this->genDetallesFromFacturaAsoc($factura_id);
        }

////         Obtener detalles de la factura indicada con partidas invertidas.
//        $this->genDetalles($detalles, $nota_id, $factura_id);

        // Actualizar indices en la sesion
        $detalles = array_values($detalles);

        // Agrupar cuentas iguales
        $detalles = DetalleCompra::agruparCuentas($detalles);

        // Agrupar debe/haber
        $detalles = $this->agruparDebeHaber($detalles);

//        // detalles de la factura asociada.
//        // si los detalles por plantilla existe en los de la factura, se actualiza el monto.
//        // si es nuevo se agrega.
//        $nota = Compra::findOne($nota_id);
//        $detallesNotaInicial = [];
//        // invertir partida de los detalles de factura
//        $map = ['debe' => 'haber', 'haber' => 'debe'];
//        foreach ($nota->detallesCompra as $index => $detalleCompra) {
//            $newDetalle = new DetalleCompra();
//            $newDetalle->load(['DetalleCompra' => $detalleCompra->attributes]);
//            unset($newDetalle->id);
//            $detallesNotaInicial[] = $newDetalle;
//        }
//        $detallesNuevos = [];
//        foreach ($detalles as $detalle) {
//            $hay = false;
//            foreach ($detallesNotaInicial as $key => $detalleNotaInicial) {
//                if ($detalle->cta_contable == $detalleNotaInicial->cta_contable && $detalle->plan_cuenta_id == $detalleNotaInicial->plan_cuenta_id) {
//                    $detallesNotaInicial[$key]->subtotal = $detalle->subtotal;
//                    $hay = true;
//                }
//            }
////            if (!$hay)
////                $detallesNuevos[] = $detalle;
//        }
//        $detalles = array_merge($detallesNotaInicial, $detallesNuevos);
//        $detalles = $this->agruparDebeHaber($detalles);

        // guarda en la sesión
        retorno:;
        Yii::$app->getSession()->set('cont_detallecompra-provider', new ArrayDataProvider([
            'allModels' => $detalles,
            'pagination' => false,
        ]));
        return true;
    }

    /**
     * @param $detalles DetalleCompra[]
     * @param $tipoDoc TipoDocumento
     * @param $total_factura
     * @param $array_totales_por_iva
     * @param $porcentajes_iva
     * @param $plantillasModel PlantillaCompraventa[]
     * @return mixed
     */
    private function genDetallesByDefault($detalles, $tipoDoc, $total_factura, $array_totales_por_iva, $porcentajes_iva, $plantillasModel, $moneda_id)
    {
        /** @var PlantillaCompraventaDetalle[] $plantilla_detalles */
        /** @var IvaCuenta $iva_cta */
        $es_nota_credito = Yii::$app->request->post('es_nota') == 'nota_credito';

        if ($total_factura == 0) { // si se controla esto, para prestamos no se podrá simular.
            $detalles = [];
            goto retorno;
        }

        $moneda = Moneda::findOne(['id' => $moneda_id]);

        // Crear detalle por Tipo de Documento
        $cuenta_haber = $tipoDoc != null ? $tipoDoc->cuenta : null;
        if (isset($cuenta_haber)) {
            /* Al elegir tipo_documento, SIEMPRE se trae 1 sola cuenta. */
            $detalle = new DetalleCompra();
            $detalle->cta_contable = 'debe';
            $detalle->plan_cuenta_id = $cuenta_haber->id;
            $detalle->subtotal = $this->numberFormatUseDotByMoneda($total_factura, $moneda);; // TODO: averiguar que valor debe tomar, dependiendo del tipo_documento.
            $detalle->agrupar = 'si';
            $detalle->cta_principal = 'no';

            array_push($detalles, $detalle);
        }

        foreach ($plantillasModel as $plantilla) {
            $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
            $gravada_total = 0.0;
            foreach ($plantilla->plantillaCompraventaDetalles as $p_detalle) {
                if ($p_detalle->tipo_asiento == 'compra') {
                    $i = 0;
                    foreach ($array_totales_por_iva[$plantilla->id] as $total_iva) {
                        if ($total_iva != '' && $p_detalle->ivaCta != null && $total_iva !== '' && (float)$total_iva > 0.0 && $p_detalle->ivaCta->iva->porcentaje == $porcentajes_iva[$i]) {
                            $detalle = new DetalleCompra();
                            $detalle->plan_cuenta_id = $p_detalle->pCGravada->id;
                            $detalle->cta_contable = $mapeo_inverso[$p_detalle->tipo_saldo];
                            $detalle->cta_principal = $p_detalle->cta_principal;
                            $detalle->subtotal = round($total_iva / (1.00 + ($p_detalle->ivaCta->iva->porcentaje / 100.0)), 2);
                            $detalle->subtotal = $this->numberFormatUseDotByMoneda($detalle->subtotal, $moneda);
                            $detalle->agrupar = 'si';
                            $gravada_total += (float)$detalle->subtotal;
                            // detalle por iva x%
                            $detalle2 = new DetalleCompra();
                            $detalle2->plan_cuenta_id = $p_detalle->ivaCta->cuentaCompra->id;
                            $detalle2->cta_contable = $mapeo_inverso[$p_detalle->tipo_saldo];
//                                $detalle2->cta_principal = $p_detalle->cta_principal;
                            $detalle2->cta_principal = 'no';
                            $detalle2->subtotal = $total_iva - $detalle->subtotal;
                            $detalle2->subtotal = $this->numberFormatUseDotByMoneda($detalle2->subtotal, $moneda);
                            $detalle2->agrupar = 'si';
                            array_push($detalles, $detalle);
                            array_push($detalles, $detalle2);
                            break;
                        } elseif ($p_detalle->ivaCta == null && $total_iva !== '' && (float)$total_iva > 0.0 && $porcentajes_iva[$i] == '0') {
                            $detalle = new DetalleCompra();
                            $detalle->plan_cuenta_id = $p_detalle->pCGravada->id;
                            $detalle->cta_contable = $mapeo_inverso[$p_detalle->tipo_saldo];
                            $detalle->cta_principal = $p_detalle->cta_principal;
                            $detalle->subtotal = $total_iva;
                            $detalle->subtotal = $this->numberFormatUseDotByMoneda($detalle->subtotal, $moneda);
                            $detalle->agrupar = 'si';
                            array_push($detalles, $detalle);
                            break;
                        }
                        $i++;
                    }
                }
            }
        }

        $detalles = array_values($detalles);

        retorno:;
        return $detalles;
    }

    private function genDetallesFromFacturaAsoc($factura_id)
    {
        $detalles = [];
        $map = ['debe' => 'haber', 'haber' => 'debe'];
        foreach (DetalleCompra::findAll(['factura_compra_id' => $factura_id]) as $detalles_factura_asoc) {
            $new_detalle = new DetalleCompra();
            foreach ($detalles_factura_asoc->attributes() as $attribute) {
                if (in_array($attribute, ['subtotal', 'plan_cuenta_id', 'cta_contable', 'cta_principal', 'prestamo_cuota_id', 'agrupar', 'asiento_id_por_cuota_prestamo'])) {
                    Yii::warning('entra');
                    if ($attribute == 'cta_contable')
                        $new_detalle->$attribute = $map[$detalles_factura_asoc->$attribute];
                    else
                        $new_detalle->$attribute = $detalles_factura_asoc->$attribute;
                }
            }
            $detalles[] = $new_detalle;
        }
        return $detalles;
    }

    private function agruparDebeHaber($array)
    {
        $debes = [];
        $haberes = [];

        foreach ($array as $item) {
            if ($item->cta_contable == 'debe') {
                $debes[] = $item;
            } elseif ($item->cta_contable == 'haber') {
                $haberes[] = $item;
            }
        }

        return array_values(array_merge($debes, $haberes));
    }

    private function numberFormatUseDotByMoneda($value, $moneda)
    {
        if (!($moneda instanceof Moneda)) {
            $moneda = Moneda::findOne(['id' => $moneda]);
        }

        if ($moneda->id == 1) {
            return number_format((float)$value, '0', '.', '');
        }

        return $value;
    }

    public function actionCargarTimbrado()
    {
        $entidad_ruc = $_POST['entidad_ruc'];
        $nro_factura = $_POST['nro_factura'];
        $fecha_emision = $_POST['fecha_emision'];
        $timbrado_vencido = $_POST['timbrado_vencido'];
        $tipodocumento_id = $_POST['tipodocumento_id'];

        if ($entidad_ruc == null or $nro_factura == null) return null;
        if (!Entidad::find()->where(['ruc' => $entidad_ruc])->exists()) return null;

        /** @var Entidad $entidad */
        $entidad = Entidad::find()->where(['ruc' => $entidad_ruc])->one();

        $tipodocumento = TipoDocumento::findOne($tipodocumento_id);
        if (!isset($tipodocumento)) return null;

        $nro = (int)substr($nro_factura, 8, 15);
        $prefijo = substr($nro_factura, 0, 7);

        if ($fecha_emision == '') return null;
        $date = DateTime::createFromFormat('d-m-Y', $fecha_emision);
        $fecha_emision = $date->format('Y-m-d');

        /** @var TimbradoDetalle $detalle */
        $query = TimbradoDetalle::find()
            ->joinWith('timbrado as timbrado')
            ->andWhere(['timbrado.entidad_id' => $entidad->id]);

        // Excluir detalle timbrado para retenciones
        $empresa = Yii::$app->session->get('core_empresa_actual');
        $periodo = Yii::$app->session->get('core_empresa_actual_pc');
        $nombreTipodocSetRet = "core_empresa-{$empresa}-periodo-{$periodo}-tipodoc_set_retencion";
        $parmsysTipoddcSet = ParametroSistema::find()->where(['nombre' => $nombreTipodocSetRet]);
        $tipodocSetRetId = null;
        if ($parmsysTipoddcSet->exists()) {
            $tipodocSetRetId = $parmsysTipoddcSet->one()->valor;
        }
        if (isset($tipodocSetRetId)) {
            $query->andWhere(['!=', 'cont_timbrado_detalle.tipo_documento_set_id', $tipodocSetRetId]);
        }
        // Fin excluir detalle timbrado para retenciones

	    if ($timbrado_vencido == '0' || $timbrado_vencido == '') {
            $query->andWhere(['or', ['and', ['<=', 'timbrado.fecha_inicio', $fecha_emision], ['is not', 'timbrado.fecha_inicio', null]], ['is', 'timbrado.fecha_inicio', null]]);
            $query->andWhere(['>=', 'timbrado.fecha_fin', $fecha_emision]);
        } else {
            $query->andWhere(['AND', ['<', 'timbrado.fecha_fin', $fecha_emision], ['IS NOT', 'timbrado.fecha_fin', null]]);
        }

        $detalle = $query
            ->andWhere(['cont_timbrado_detalle.prefijo' => $prefijo])
            ->andWhere(['<=', 'cont_timbrado_detalle.nro_inicio', $nro])
            ->andWhere(['>=', 'cont_timbrado_detalle.nro_fin', $nro])
            ->andFilterWhere(['=', 'cont_timbrado_detalle.tipo_documento_set_id', $tipodocumento->tipo_documento_set_id])
            ->asArray()->all();  # si se usa one(), cuando haya mas de 1 detalle con el mismo prefijo y rangos que corresponden, se va a seleccionar solito uno cualquiera.
        # Caso: 10~11 de junio, 2019: Empresa formosa, factura id 28579, nro 002-001-0070985, timbrado 13394250, emitido el 27-05-2019
        # Hay dos timbrados nro 13238208 y 13394250 cuyos detalles (cada uno 1 solo detalle hasta la fecha) acaparan/contemplan al nro de factura.

        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (!isset($detalle)) {
            $timbrados = Timbrado::getTimbradosVigentesAjax($entidad_ruc, $_POST['fecha_emision'], $nro_factura, $tipodocumento_id, $timbrado_vencido);
            if (sizeof($timbrados) == 1)
                return ['mostrar_boton', array_pop($timbrados)];
            else
                return ['mostrar_boton', null];
        }

        $data = [];
        foreach ($detalle as $item) {
            $element = ['id' => $item['timbrado_id'], 'nro_timbrado' => $item['timbrado']['nro_timbrado']];

            //Si fecha emision es posterior a fecha fin timbrado
            //Timbrado vencido
            if ($date->getTimestamp() > strtotime($item['timbrado']['fecha_fin']))
                $element['vencido'] = 'si';
            else
                $element['vencido'] = 'no';

            $data[] = $element;
        }

//        $data = ['id' => $detalle->timbrado_id, 'nro_timbrado' => $detalle->timbrado->nro_timbrado];
//
//        //Si fecha emision es posterior a fecha fin timbrado
//        //Timbrado vencido
//        if ($date->getTimestamp() > strtotime($detalle->timbrado->fecha_fin))
//            $data['vencido'] = 'si';
//        else
//            $data['vencido'] = 'no';

        return $data;
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdateTimbradoCompra($entidadRuc, $prefijo, $nro, $tipoDocumentoId, $timbradoId, $facturaCompraId, $submit = false)
    {
        $model = new \backend\modules\contabilidad\models\auxiliar\Timbrado2();
        $model_detalle = new TimbradoDetalleUpdate();
        $nro = (int)$nro;
        $timbrado = Timbrado::findOne(['id' => $timbradoId]);
        $model->nro_timbrado = $timbrado->nro_timbrado;
        $model->fecha_inicio = date('d-m-Y', strtotime($timbrado->fecha_inicio));
        $model->fecha_fin = date('d-m-Y', strtotime($timbrado->fecha_fin));

        $entidad = Entidad::findOne(['ruc' => $entidadRuc]);
        $facturaCompra = Compra::findOne(['id' => $facturaCompraId]);
        $tipoDocumento = TipoDocumento::findOne(['id' => $tipoDocumentoId]);

        /** @var TimbradoDetalle $timbradoDetalle */
        $timbradoDetalle = TimbradoDetalle::find()
            ->joinWith('timbrado as timbrado')
            ->where(['prefijo' => $prefijo])
            ->andWhere(['timbrado_id' => $timbradoId])
            ->andWhere(['timbrado.entidad_id' => $entidad->id])
            ->andWhere(['<=', 'nro_inicio', $nro])
            ->andWhere(['>=', 'nro_fin', $nro])
            ->one();

        #Puede que el timbrado seleccionado no tenga detalles que contemple al nro de factura.
        if (isset($timbradoDetalle)) {
            $model_detalle->nro_inicio = $timbradoDetalle->nro_inicio;
            $model_detalle->nro_fin = $timbradoDetalle->nro_fin;
            $model_detalle->prefijo = $timbradoDetalle->prefijo;
        } else {
            $model_detalle->prefijo = $prefijo;
            $model_detalle->nro_inicio = $model_detalle->nro_fin = $model_detalle->nro_actual = $nro;
            $model_detalle->tipo_documento_set_id = $facturaCompra->tipoDocumento->tipo_documento_set_id;
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model, $model_detalle);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post() && $submit == true) {
            $nro_timbrado = $_POST['Timbrado2']['nro_timbrado'];
            $timbradoEnDB = Timbrado::findOne(['nro_timbrado' => $nro_timbrado]);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($timbradoEnDB != null) {//El timbrado existe en la base de datos.
                    /** @var TimbradoDetalle $timbradoDetalleExistente */
                    $timbradoDetalleExistente = TimbradoDetalle::find()
                        ->joinWith('timbrado as timbrado')
                        ->where(['prefijo' => $prefijo])
                        ->andWhere(['=', 'timbrado.id', $timbradoEnDB->id])
                        ->andWhere(['timbrado.entidad_id' => $entidad->id])
                        ->andWhere(['<=', 'nro_inicio', $nro])
                        ->andWhere(['>=', 'nro_fin', $nro])
                        ->one();

                    if ($timbradoDetalleExistente != null) {//Ya existe un timbrado con detalle que contiene al numero de factura. Solo se debe referenciar.
                        FlashMessageHelpsers::createInfoMessage('No se ha hecho ninguna modificación.');
                        return 1;
//                        $facturaCompra->timbrado_detalle_id = $timbradoDetalleExistente->id;
//                        $facturaCompra->timbrado = '000';
//                        $facturaCompra->timbrado_id = '0';
//                        $facturaCompra->nombre_entidad = '--';
//                        $facturaCompra->ruc = '0';
//                        if (!$facturaCompra->save()) {
//                            throw new Exception('No se puede modificar timbrado.'); // No se puede guardar
//                        }
//
//                        //Si el detalle ya no es referenciado, se borra
//                        if (!$timbradoDetalleViejo->getFacturasCompra()->exists() && !$timbradoDetalleViejo->getFacturasVenta()->exists()) {
//                            $timbradoDetalleViejo->delete();
//                        }
                    } else { //Se debe agregar un detalle al timbrado ya existente.
                        # TODO: Preguntar a hernan
                        # Por que primero se carga datos del detalle viejo y luego los datos del post?
                        $timbradoDetalleNuevo = new TimbradoDetalle2();
                        $timbradoDetalleNuevo->timbrado_id = $timbradoEnDB->id;
                        $timbradoDetalleNuevo->prefijo = $prefijo;
                        $timbradoDetalleNuevo->nro_inicio = $nro;
                        $timbradoDetalleNuevo->nro_fin = $nro;
                        $timbradoDetalleNuevo->nro_actual = $nro;
                        $timbradoDetalleNuevo->tipo_documento_set_id = $tipoDocumento->tipo_documento_set_id;
                        if (!$timbradoDetalleNuevo->save()) { // No se puede guardar el rango. Se va a crear nro_inicio = nro_fin = nro
                            throw new \yii\db\Exception("No se puede modificar timbrado: {$timbradoDetalleNuevo->getErrorSummaryAsString()}"); // No se puede guardar
                        }
//
//                        $timbradoDetalleNuevo = new TimbradoDetalle();
//                        $timbradoDetalleNuevo->timbrado_id = $timbradoEnDB->id;
//                        $timbradoDetalleNuevo->prefijo = $timbradoDetalleViejo->prefijo;
//                        $timbradoDetalleNuevo->nro_inicio = $timbradoDetalleViejo->nro_inicio;
//                        $timbradoDetalleNuevo->nro_fin = $timbradoDetalleViejo->nro_fin;
//                        $timbradoDetalleNuevo->nro_actual = $timbradoDetalleViejo->nro_inicio;//nro_inicio es nro_actual
//                        $timbradoDetalleNuevo->tipo_documento_set_id = $timbradoDetalleViejo->tipo_documento_set_id;
//                        if (!$timbradoDetalleNuevo->validate()) { // No se puede guardar el rango. Se va a crear nro_inicio = nro_fin = nro
//                            $timbradoDetalleNuevo->nro_inicio = $nro;
//                            $timbradoDetalleNuevo->nro_fin = $nro;
//                            $timbradoDetalleNuevo->nro_actual = $nro;
//                            if (!$timbradoDetalleNuevo->save()) {
//                                throw new Exception('No se puede modificar timbrado.'); // No se puede guardar
//                            }
//                        } else {
//                            $timbradoDetalleNuevo->save(false);
//                        }
//
//                        $facturaCompra->timbrado_detalle_id = $timbradoDetalleNuevo->id;
//                        $facturaCompra->timbrado = '000';
//                        $facturaCompra->timbrado_id = '0';
//                        $facturaCompra->nombre_entidad = '--';
//                        $facturaCompra->ruc = '0';
//                        if (!$facturaCompra->save()) {
//                            throw new Exception('No se puede modificar timbrado.'); // No se puede guardar
//                        }
//
//                        //Si el detalle ya no es referenciado, se borra
//                        if (!$timbradoDetalleViejo->getFacturasCompra()->exists() && !$timbradoDetalleViejo->getFacturasVenta()->exists()) {
//                            $timbradoDetalleViejo->delete();
//                        }
                    }
                } else { //No existe el timbrado ingresado. Se va a crear junto a su detalle.
                    $timbradoNuevo = new Timbrado();
                    $timbradoNuevo->nro_timbrado = $nro_timbrado;
                    $timbradoNuevo->entidad_id = $timbrado->entidad_id;
                    $timbradoNuevo->nombre_entidad = $entidad->razon_social;
                    $timbradoNuevo->fecha_inicio = date('d-m-Y', strtotime($timbrado->fecha_inicio));
                    $timbradoNuevo->fecha_fin = date('d-m-Y', strtotime($timbrado->fecha_fin));
                    $timbradoNuevo->ruc = $entidadRuc;
                    if (!$timbradoNuevo->save()) {
                        throw new Exception("No se puede modificar timbrado: {$timbradoNuevo->getErrorSummaryAsString()}"); // No se puede guardar
                    }
                    $timbradoNuevo->refresh();

                    $timbradoDetalleNuevo = new TimbradoDetalle2();
                    $timbradoDetalleNuevo->timbrado_id = $timbradoNuevo->id;
                    $timbradoDetalleNuevo->prefijo = $prefijo;
                    $timbradoDetalleNuevo->nro_inicio = $nro;
                    $timbradoDetalleNuevo->nro_fin = $nro;
                    $timbradoDetalleNuevo->nro_actual = $nro;//nro_inicio es nro_actual
                    $timbradoDetalleNuevo->tipo_documento_set_id = $tipoDocumento->tipo_documento_set_id;

                    if (!$timbradoDetalleNuevo->save()) { // No se puede guardar el rango. Se va a crear nro_inicio = nro_fin = nro
                        throw new Exception("No se puede modificar timbrado: {$timbradoDetalleNuevo->getErrorSummaryAsString()}"); // No se puede guardar
                    }
//                    $timbradoDetalleNuevo->save(false);
//
//                    $facturaCompra->timbrado_detalle_id = $timbradoDetalleNuevo->id;
//                    $facturaCompra->timbrado = '000';
//                    $facturaCompra->timbrado_id = '0';
//                    $facturaCompra->nombre_entidad = '--';
//                    $facturaCompra->ruc = '0';
//                    if (!$facturaCompra->save()) {
//                        throw new Exception('No se puede modificar timbrado.'); // No se puede guardar
//                    }
//
//                    //Si el detalle ya no es referenciado, se borra
//                    if (!$timbradoDetalleViejo->getFacturasCompra()->exists() && !$timbradoDetalleViejo->getFacturasVenta()->exists()) {
//                        $timbradoDetalleViejo->delete();
//                    }
                }

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage('El timbrado se ha generado.');
                return 1;
            } catch (Exception $e) {
                FlashMessageHelpsers::createErrorMessage($e->getMessage());
                $transaction->rollBack();
                return false;
            }
        }

        return $this->renderAjax('../compra/_modalform_facturacompra_updatetimbrado', [
            'model' => $model,
            'model_detalle' => $model_detalle,
        ]);
    }

    public function actionBloquear($id)
    {
        $model = Compra::findOne(['id' => $id]);

        if ($model->bloqueado == 'no') {
            $model->bloqueado = 'si';

            //Solo para validacion
            $model->timbrado_id = '1';
            $model->nombre_entidad = 'a';
            $model->ruc = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Nota ha sido bloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede bloquear nota. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionDesbloquear($id)
    {
        $model = Compra::findOne(['id' => $id]);

        if ($model->bloqueado == 'si') {
            $model->bloqueado = 'no';

            //Solo para validacion
            $model->timbrado_id = '1';
            $model->nombre_entidad = 'a';
            $model->ruc = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Nota ha sido desbloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede desbloquear nota. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }
}