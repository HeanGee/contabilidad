<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 7/25/18
 * Time: 9:06 PM
 */

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Cotizacion;
use backend\models\Moneda;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\ActivoFijoStockManager;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\search\VentaSearch;
use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\FlashMessageHelpsers;
use common\models\ParametroSistema;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;

require __DIR__ . '/../utils/Utils.php';

/**
 * VentaNotaController implements the CRUD actions for Venta model (as Nota).
 */
class VentaNotaController extends BaseController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view', 'bloquear', 'desbloquear'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Venta $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Venta $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['bloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Venta $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['desbloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Venta $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'si';
                        }
                    ],
                ],
            ],
        ];
    }

    private static function removeSessionVariables()
    {
        Yii::$app->session->remove('cont_detalleventa-provider');
        Yii::$app->session->remove('cont_detalleventa_costo_sim-provider');
        Yii::$app->session->remove('cont_detalleventa_sim-provider');
        Yii::$app->session->remove('cont_monto_totales');
        Yii::$app->session->remove('cont_activofijo_ids');
        Yii::$app->session->remove('cont_prefijo_fila_plantilla');
        Yii::$app->session->remove('cont_venta_id');
        Yii::$app->session->remove('cont_venta_nota_credito_id');
        Yii::$app->session->remove('cont_activo_bio_stock_manager');
        Yii::$app->session->remove('cont_actfijo_pointer');
        Yii::$app->session->remove('$campo_iva_activo');
        Yii::$app->session->remove('$nota_id');
        Yii::$app->session->remove('$venta_id');
        Yii::$app->session->remove('cont_act_bio_valor_para_plantilla');
        Yii::$app->session->remove('total-costo-adq	');
    }

    /**
     * Lists all Venta models (as Nota).
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VentaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
        $dataProvider->pagination = ['pageSize' => 20];

        self::removeSessionVariables();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    private static function isNotaCreable()
    {
        try {
            if (!Venta::find()->where([
                'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
                'estado' => 'vigente',
            ])->andWhere(['IN', 'tipo', ['factura', 'nota_credito']])->exists()) throw new \yii\base\Exception("Aún no se ha registrado ninguna factura de venta para la empresa y periodo contable actuales.");
            Venta::getTipodocSetNotaCredito();
            Venta::getTipodocSetNotaDebito();
            Venta::getTipodocNotaCredito();
            Venta::getTipodocNotaDebito();
        } catch (\yii\base\Exception $exception) {
            throw $exception;
        }
        return true;
    }

    /**
     * Creates a new Venta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     * @throws \Throwable
     */
    public function actionCreate($_pjax = null)
    {
        try {
            self::isNotaCreable();
        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }

        $model = new Venta();
        $model->estado = 'vigente';
        $session = Yii::$app->session;

        if ($_pjax == null && !Yii::$app->request->isPost) {
            self::removeSessionVariables();

            $session->set('cont_detalleventa-provider', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
        }

        if (array_key_exists('VentaIvaCuentaUsada', $_POST)) {
            if (!empty($_POST['VentaIvaCuentaUsada']['new'])) {
                unset($_POST['VentaIvaCuentaUsada']['new']);
                foreach ($_POST['VentaIvaCuentaUsada'] as $new_keys => $new) {
                    if (empty($new['plantilla_id']))
                        unset($_POST['VentaIvaCuentaUsada'][$new_keys]);
                }
                Yii::$app->request->setBodyParams($_POST);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            /**
             * @param $tipoNota string
             * @return TipoDocumento|null
             */
            $getTipoDoc = function ($tipoNota) {
                if ($tipoNota == 'nota_credito')
                    return Venta::getTipodocNotaCredito();
                else
                    return Venta::getTipodocNotaDebito();
            };

            $trans = Yii::$app->db->beginTransaction();
            try {
                $model->creado_por = Yii::$app->user->identity->username;

                /** @var VentaIvaCuentaUsada[] $ivacuenta_guardar */
                /** @var DetalleVenta[] $detalleVenta */

                $periodo_contable = Yii::$app->session->get('core_empresa_actual_pc');
                $factura_model = Venta::findOne($model->factura_venta_id);
                // datos estirados de la factura de compra
                if (isset($factura_model)) {
                    $model->entidad_id = $factura_model->entidad_id;
                    $model->obligacion_id = $factura_model->obligacion_id;
                    $model->para_iva = $factura_model->para_iva;
                    $model->cant_cuotas = $factura_model->cant_cuotas;
                    $model->moneda_id = $factura_model->moneda_id;
                    if ($model->cant_cuotas == '') $model->cant_cuotas = 1;
                } else {
                    $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                    $model->entidad_id = $entidad->id;
                    $model->para_iva = 'si';
                    $model->cant_cuotas = 1;
                }
                $model->fecha_vencimiento = date('Y-m-d', strtotime($model->fecha_emision . ' +1 month'));
                $model->tipo_documento_id = $getTipoDoc($model->tipo)->id;
                $model->condicion = $model->tipoDocumento->condicion;
                $model->estado = 'vigente';
                $model->empresa_id = Yii::$app->session->get(SessionVariables::empresa_actual);
                $model->periodo_contable_id = $periodo_contable;
                $model->prefijo = TimbradoDetalle::findOne(['id' => $model->timbrado_detalle_id_prefijo])->prefijo;
                $timbradoD = TimbradoDetalle::find()->alias('detalle')
                    ->leftJoin('cont_timbrado timbrado', 'timbrado.id = detalle.timbrado_id')
                    ->where([
                        'detalle.id' => $model->timbrado_detalle_id_prefijo,
                        'prefijo' => $model->prefijo,
                        'tipo_documento_set_id' => $model->tipoDocumento->tipo_documento_set_id,
                        'entidad_id' => Entidad::findOne(['ruc' => $model->empresa->ruc])->id,
                    ])
                    ->andWhere(['<=', 'nro_inicio', (integer)$model->nro_factura])
                    ->andWhere(['>=', 'nro_fin', (integer)$model->nro_factura])
                    ->one();
                $model->timbrado_detalle_id = $timbradoD->id;
                $model->formatDateForSave();

                // Generar id
                if (!$model->save()) {
                    throw new \Exception("Error validando nota: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                // No permitir guradar factura sin detalle
                $detalleVenta = $session['cont_detalleventa-provider']->allModels;
                if (empty($detalleVenta)) {
                    $msg = 'Faltan los detalles de la factura';
                    throw new \Exception($msg);
                }

                // Registrar cada uno de los montos de los ivas, junto con las cuentas usadas.
                $ningun_total = true;
                $ivacuenta_guardar = [];
                $this->manejarIvasCuentasUsadas($model, $ivacuenta_guardar, $ningun_total, $factura_model);
                if ($ningun_total) {
                    $msg = 'Falta completar al menos un campo del Total Iva o Total Exenta.';
                    throw new \Exception($msg);
                }

                // Verificar balance.
                $saldo = ['debe' => 0.0, 'haber' => 0.0];
                foreach ($detalleVenta as $detalle) {
                    $saldo[$detalle->cta_contable] += (float)$detalle->subtotal;
                    $detalle->factura_venta_id = $model->id;
                    $detalle->periodo_contable_id = $periodo_contable;
                    if (!$detalle->validate()) {
                        $msg = 'Error en detalle de factura: ' . $detalle->planCuenta->nombre . ": " . $detalle->getErrorsEnString();
                        throw new \Exception($msg);
                    }
                }
                if ($saldo['debe'] !== $saldo['haber']) {
                    $msg = 'El DEBE y el HABER NO son iguales. Se recomienda verificar cómo está guardada la plantilla
                     utilizada y también revisar los detalles y las simulaciones generados automáticamente.';
                    throw new \Exception($msg);
                }
                $model->total = $saldo['debe']; // para evitar trampa

                // Verificar prefijo, nro_factura
                $result = Venta::checkNroFactura($model->nro_factura, $model->prefijo, $model->timbrado_detalle_id, 'create', $model->id);
                if ($result != Venta::NRO_FACTURA_CORRECTO && $result != Venta::NRO_FACTURA_DUPLICADO) {
                    switch ($result) {
                        case Venta::NRO_FACTURA_NO_RANGO:
                            $msg = 'Este nro de nota no corresponde a ningún rango de ningún timbrado.';
                            break;
                        case Venta::NRO_FACTURA_NO_PREFIJO:
                            $msg = 'Este prefijo de nota no existe. DEBE CREAR desde el menú de Timbrado primero.';
                    }
                    throw new \Exception($msg);
                }

                // Actualizar el detalle plantilla corresp. con el nro_actual adecuado.
                $update_next_nro = $model->updateNroActualSession();
                if (!$update_next_nro['resultado']) {
                    $msg = $update_next_nro['errores'];
                    throw new \Exception($msg);
                }

                // asignar saldo = total factura
                $model->saldo = $model->total; // no se por que miguel hizo esto, tal vez las notas de credito tengan que 'devengarse' a los clientes?

//                // actualizar saldo de factura asociada si es credito.
//                !isset($factura_model) || $factura_model->saldo -= ($factura_model->condicion == 'credito') ? $model->total : 0;
//                // campos que me exige el modelo de factura venta
//                !isset($factura_model) || $factura_model->timbrado_detalle_id_prefijo = '-';

                // Guardar detalles de venta
                foreach ($detalleVenta as $detalle) {
                    if (!$detalle->save()) {
                        throw new \Exception("Error guardando detalle de venta: {$detalle->getErrorSummaryAsString()}");
                    }
                    $detalle->refresh();
                }
                $model->refresh();

                // Procesar y guardar ivas cuentas.
                // Determinar si la plantilla maneja un concepto en particular y si hay datos en la sesión
                // para su correspondiente manejo.
                foreach ($ivacuenta_guardar as $ivacta) {
                    if (!$ivacta->save()) {
                        throw new \Exception("Error interno guardando iva cuenta: {$ivacta->getErrorSummaryAsString()}");
                    }
                    $ivacta->refresh();
                    // no hace falta determinar si maneja activo fijo porque se puede saber a travéz del modelo
                    // de factura, haciendo $factura_model->getActivosFijos()->exists().
                }
                $model->refresh();

                // Guardar nota
                if (!$model->save()) {
                    throw new Exception("Error guardando nota: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                if (empty($model->detalleVentas))
                    throw new \Exception("Falta crear al menos un detalle de venta.");

                if (isset($factura_model)) {
                    // Actualizar saldo.
                    $factura_model->refresh();
                    $factura_model->actualizarSaldo();
                    // Manejar activos fijos.
                    $this->manejarActivosFijos($model, $factura_model, $model->factura_venta_id);
                    // Manejar activos biologicos
                    $this->manejarActivosBiologicos($model, $factura_model, null);
                }

                $trans->commit(); // TODO: descomentar si todo funciona bien.
                FlashMessageHelpsers::createSuccessMessage("Nota Nº {$model->prefijo}-{$model->nro_factura} creada correctamente.");
                return $this->redirect(['index']);

            } catch (\Exception $e) {
                //throw  $e;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($e->getMessage());
            }
        }

        $model->id = null;
        $model->formatDateForViews();
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Venta model (as Nota).
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\db\Exception
     * @throws \Throwable
     */
    public function actionUpdate($id, $_pjax = null)
    {
        $model = $this->findModel($id);
        $old_factura = $model->facturaVenta;

        try {
            if ($model->isNotaCredito())
                $model->isNotaCreditoEditable();
            elseif ($model->isNotaDebito())
                $model->isNotaDebitoEditable();

        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }

        $model->timbrado_detalle_id_prefijo = $model->timbrado_detalle_id;
        $model->ruc = !empty($model->entidad) ? $model->entidad->ruc : null;
        $model->nombre_entidad = !empty($model->entidad) ? $model->entidad->razon_social : null;
        $model->nro_timbrado = $model->timbradoDetalle->timbrado->nro_timbrado;
        $session = Yii::$app->session;
        $detalles = DetalleVenta::find()->where(['factura_venta_id' => $model->id])->all();

        if ($_pjax == null && !Yii::$app->request->isPost) {
            self::removeSessionVariables();

            $session->set('cont_detalleventa-provider', new ArrayDataProvider([
                'allModels' => $detalles,
                'pagination' => false,
            ]));

            // Carga datos de activo fijo (copiado desde actionManejarDesdeNotaCreditoCompra()
            $venta_id = $model->factura_venta_id;
            $ventaNotaCredito_id = $model->id;
            $ids = [];
            foreach (ActivoFijoStockManager::findAll(['factura_venta_id' => $venta_id, 'venta_nota_credito_id' => $ventaNotaCredito_id]) as $manager) {
                $ids[] = $manager->activo_fijo_id;
                $ids_activo_fijo_compra[] = $manager->activo_fijo_id;
            }
            $ids = [
                'venta_id' => $model->factura_venta_id,
                'ids' => $ids,
            ];
            if (!Yii::$app->session->has('cont_activofijo_ids')) {
                Yii::$app->session->set('cont_activofijo_ids', $ids);
            }

            $model->_iva_ctas_usadas = [];
            foreach ($model->ivasCuentaUsadas as $ivasCuentaUsada) {
                $model->_iva_ctas_usadas[$ivasCuentaUsada->plantilla_id][isset($ivasCuentaUsada->ivaCta) ? (int)$ivasCuentaUsada->ivaCta->iva->porcentaje : 0] = $ivasCuentaUsada->monto;
            }
        }

        if (array_key_exists('VentaIvaCuentaUsada', $_POST)) {
            if (!empty($_POST['VentaIvaCuentaUsada']['new'])) {
                unset($_POST['VentaIvaCuentaUsada']['new']);
                foreach ($_POST['VentaIvaCuentaUsada'] as $new_keys => $new) {
                    if (empty($new['plantilla_id']))
                        unset($_POST['VentaIvaCuentaUsada'][$new_keys]);
                }
                Yii::$app->request->setBodyParams($_POST);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            $getTipoDoc = function ($tipoNota) {
                if ($tipoNota == 'nota_credito')
                    return Venta::getTipodocNotaCredito();
                else
                    return Venta::getTipodocNotaDebito();
            };
            try {
                /** @var VentaIvaCuentaUsada[] $ivasCtasAnteriores */
                /** @var DetalleVenta[] $detalleVenta */

                // self::manejarCambioDeFactura($model);
                $factura_model = Venta::findOne($model->factura_venta_id);
                // datos estirados de la factura de venta
                if (isset($factura_model)) {
                    $model->entidad_id = $factura_model->entidad_id;
                    $model->obligacion_id = $factura_model->obligacion_id;
                    $model->para_iva = $factura_model->para_iva;
                    $model->cant_cuotas = $factura_model->cant_cuotas;
                    if ($model->cant_cuotas == '') $model->cant_cuotas = 1;
                } else {
                    $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                    $model->entidad_id = $entidad->id;
                    $model->para_iva = 'si';
                    $model->cant_cuotas = 1;
                }
                // propios de la nota
                $model->estado = 'vigente';
                $model->tipo_documento_id = $getTipoDoc($model->tipo)->id;
                $model->condicion = $model->tipoDocumento->condicion;
                $model->fecha_vencimiento = date('Y-m-d', strtotime($model->fecha_emision . ' +1 month'));

                // borra detallesNota inicial
                foreach ($model->detalleVentas as $detalle) {
                    if (!$detalle->delete()) {
                        $msg = "Error borrando detalles anteriores: {$detalle->getErrorSummaryAsString()}";
                        throw new \Exception($msg);
                    }
                }

//                // IvaCtas anteriores. Deben borrarse solamente
//                $ivasCtasAnteriores = [];
//                foreach ($model->ivasCuentaUsadas as $ivaCuentaUsada) {
//                    $ivasCtasAnteriores[] = $ivaCuentaUsada;
//                }

                $model->prefijo = TimbradoDetalle::findOne(['id' => $model->timbrado_detalle_id_prefijo])->prefijo;
                $timbradoD = TimbradoDetalle::find()->alias('detalle')
                    ->leftJoin('cont_timbrado timbrado', 'timbrado.id = detalle.timbrado_id')
                    ->where([
                        'detalle.id' => $model->timbrado_detalle_id_prefijo,
                        'prefijo' => $model->prefijo,
                        'tipo_documento_set_id' => $model->tipoDocumento->tipo_documento_set_id,
                        'entidad_id' => Entidad::findOne(['ruc' => $model->empresa->ruc])->id,
                    ])
                    ->andWhere(['<=', 'nro_inicio', (integer)$model->nro_factura])
                    ->andWhere(['>=', 'nro_fin', (integer)$model->nro_factura])
                    ->one();
                $model->timbrado_detalle_id = $timbradoD->id;

                // Miguel hacia que solamente verifique y demas solo cuando cambie el prefijo pero
                // en realidad, asi como en venta, puede cambiar el nro de nota manualmente.
                $result = Venta::checkNroFactura($model->nro_factura, $model->prefijo, $model->timbrado_detalle_id, 'update', $model->id);
                if ($result != Venta::NRO_FACTURA_CORRECTO && $result != Venta::NRO_FACTURA_DUPLICADO) {
                    switch ($result) {
                        case Venta::NRO_FACTURA_NO_RANGO:
                            $msg = 'Este nro de nota no corresponde a ningún rango de ningún timbrado.';
                            break;
                        case Venta::NRO_FACTURA_NO_PREFIJO:
                            $msg = 'Este prefijo de nota no existe. DEBE CREAR desde el menú de Timbrado primero.';
                    }
                    FlashMessageHelpsers::createWarningMessage($msg);
                    throw new \Exception($msg);
                }

                // Actualizar el detalle plantilla corresp. con el nro_actual adecuado.
                $update_next_nro = $model->updateNroActualSession(false, 'update');
                if (!$update_next_nro['resultado']) {
                    $msg = $update_next_nro['errores'];
                    throw new \Exception($msg);
                }

                $model->formatDateForSave();
                if (!$model->validate()) {
                    $msg = 'Error al guardar la cabecera de factura.';
                    throw new \Exception("Error validando nota: {$model->getErrorSummaryAsString()}");
                }

                // Manejar ivaCtaas.
                $ningun_total = true;
                $ivacuenta_guardar = [];
                $this->manejarIvasCuentasUsadas($model, $ivacuenta_guardar, $ningun_total, $factura_model);
                if ($ningun_total) {
                    $msg = 'Falta completar al menos un campo del Total Iva o Total Exenta.';
                    throw new Exception($msg);
                }

                // No permitir guardar factura sin detalle
                $detalleVenta = $session['cont_detalleventa-provider']->allModels;
                if (empty($detalleVenta)) {
                    $msg = 'Faltan los detalles de la factura';
                    throw new Exception($msg);
                }

                // Verificar balance
                $saldo = ['debe' => 0.0, 'haber' => 0.0];
                foreach ($detalleVenta as $_detalle) {
                    $detalle = new DetalleVenta();
                    $detalle->setAttributes($_detalle->attributes);
                    unset($detalle->id);
                    $saldo[$detalle->cta_contable] += round($detalle->subtotal, 2);
                    $detalle->factura_venta_id = $model->id;
                    $detalle->periodo_contable_id = ($detalle->periodo_contable_id == null) ? Yii::$app->session->get('core_empresa_actual_pc') : $detalle->periodo_contable_id;
                    if (!$detalle->save()) {
                        throw new \Exception("No se pudo guardar el detalle \"{$detalle->planCuenta->nombre}\": {$detalle->getErrorSummaryAsString()}");
                    }
                }
                if ($saldo['debe'] !== $saldo['haber']) {
                    $msg = 'El DEBE y el HABER NO son iguales. Se recomienda verificar cómo está guardada la plantilla utilizada y también revisar los detalles y las simulaciones generados automáticamente.';
                    throw new \Exception($msg);
                }
                $model->total = $saldo['debe']; // para evitar trampa

                // asignar saldo = total factura
                $model->saldo = $model->total; // se copia del create, que hizo miguel.

                /** @var VentaIvaCuentaUsada $ivacta */
                foreach ($ivacuenta_guardar as $ivacta) {
                    if (!$ivacta->save(false)) {
                        throw new \Exception("No se pudo guardar iva cuenta: {$ivacta->getErrorSummaryAsString()}");
                    }
                    $ivacta->refresh();
                }

                if (!$model->save()) {
                    $msg = "Error guardando nota: {$model->getErrorSummaryAsString()}";
                    throw new \Exception($msg);
                }
                $model->refresh();

                if (empty($model->detalleVentas)) {
                    throw new \Exception("Falta crear al menos un detalle de venta.");
                }

                if (isset($old_factura)) {
                    $old_factura->refresh();
                    $old_factura->actualizarSaldo(true);
                }
                if (isset($factura_model)) {
                    $factura_model->refresh();
                    $factura_model->actualizarSaldo(true);
                }

                // Manejar activos fijos.
                $this->manejarActivosFijos($model, $factura_model, (isset($old_factura) ? $old_factura->id : null));

                // Manejar activos biologicos
                /** @var ActivoBiologicoStockManager[] $managersNota */
                /** @var ActivoBiologicoStockManager[] $oldManagersNota */
                $this->manejarActivosBiologicos($model, $factura_model, (isset($old_factura) ? $old_factura->id : null));

//                exit();

                $trans->commit();
                FlashMessageHelpsers::createSuccessMessage('La nota ' . $model->getNroFacturaCompleto() . ' se ha modificado correctamente.');
                return $this->redirect(['index']);

            } catch (\Exception $e) {
                //throw  $e;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($e->getMessage());
            }
        }

        $model->formatDateForViews();
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model_nota = $this->findModel($id);
        try {
            if ($model_nota->isNotaCredito())
                $model_nota->isNotaCreditoDeleteable();
            elseif ($model_nota->isNotaDebito())
                $model_nota->isNotaDebitoDeleteable();

        } catch (\yii\base\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $factura_model = Venta::findOne($model_nota->factura_venta_id);

            /** @var VentaIvaCuentaUsada[] $ivaCuentas */
            $ivaCuentas = VentaIvaCuentaUsada::find()->where([
                'factura_venta_id' => $id,
                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
                ->all();

            foreach ($ivaCuentas as $ic) {
                if (!$ic->delete()) {
                    throw new \yii\base\Exception("Error interno eliminando iva cuentas: {$ic->getErrorSummaryAsString()}");
                }
            }
            foreach ($model_nota->detalleVentas as $df) {
                if (!$df->delete()) {
                    throw new \yii\base\Exception("Error eliminando detalles de nota: {$df->getErrorSummaryAsString()}");
                }
            }

            // Manejo de activos biologicos
            /** @var ActivoBiologicoStockManager $stockManager */
            foreach (ActivoBiologicoStockManager::findAll(['nota_venta_id' => $model_nota->id, 'factura_venta_id' => $model_nota->factura_venta_id]) as $stockManager) {
                $aBio = $stockManager->activoBiologico;
//                $aBio->isEditable();
                if ($aBio->getInventario()->exists())
                    throw new \yii\base\Exception("Se intentaba actualizar el stock actual del activo
                         biológico {$aBio->nombre} pero se encuentra registrado en el cierre de inventario.
                         Por tanto no puede modificar.");
                if (!$stockManager->delete()) {
                    throw new \yii\base\Exception("Error interno borrando stock manager: {$stockManager->getErrorSummaryAsString()}.");
                }
                $stockManager->refresh();
                $aBio->recalcularStockActual();
                if (!$aBio->save()) {
                    throw new \yii\base\Exception("Error actualizando stock de activo biológico {$aBio->nombre}: {$aBio->getErrorSummaryAsString()}");
                }
            }

            if (!$model_nota->delete()) {
                throw new \yii\base\Exception("Error borrando nota: {$model_nota->getErrorSummaryAsString()}");
            }
            $model_nota->refresh();

            // actualizar saldo y stock de activos fijos
            if (isset($factura_model)) {
                $factura_model->refresh();
                $factura_model->actualizarSaldo(true);

                // Manejo de activos fijos y sus managers.
                /** @var ActivoFijoStockManager $stockManager */
                foreach (ActivoFijoStockManager::findAll(['factura_venta_id' => $factura_model->id]) as $stockManager) {
                    $aFijo = $stockManager->activoFijo;
                    $aFijo->setEstado('vendido');
                    $aFijo->setFacturaVentaId($stockManager->factura_venta_id);
                    if (!$aFijo->save()) {
                        throw new \yii\base\Exception("Error validando la restauración de activo fijo desasociado: {$aFijo->getErrorSummaryAsString()}");
                    }
                    if (!$stockManager->delete()) {
                        throw new \yii\base\Exception("Error interno borrando stock manager: {$stockManager->getErrorSummaryAsString()}");
                    }
                }
            }

            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage('La nota se ha borrado exitosamente.');
        } catch (\yii\base\Exception $e) {
            $transaction->rollBack();
            FlashMessageHelpsers::createErrorMessage($e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Displays a single Compra model (as Nota).
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Venta model (as Nota) based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Venta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Venta::findOne(['id' => $id, ['not' => ['factura_venta_id' => null]]])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('No existe el registro solicitado.');
    }

    /**
     * Miercoes 13, abril 2019: Segun correo de Magali, se desea poder modificar la factura asociada.
     * @param Venta $nota
     * @throws \Exception
     */
    private function manejarCambioDeFactura(&$nota)
    {
        return;
        // Si son las mismas facturas, no hacer nada.
        if ($nota->oldAttributes['factura_venta_id'] == $nota->factura_venta_id)
            return;

        $factura_original = Venta::findOne(['id' => $nota->oldAttributes['factura_venta_id']]);
        // Si no se pudo obtener la factura asociada, lanzar exception
        if (!isset($factura_original))
            return;  # ahora se permite guardar sin id de factura asociada
//            throw new \Exception("Error interno: No se puede obtener la factura originalmente asociada a esta
//             nota en edición");

        // Si la factura originalmente asociada es al contado, no hacer nada.
        if ($factura_original->condicion == 'contado') return;

        // La factura original es a credito.
        $factura_original->saldo += $nota->oldAttributes['total'];
        $factura_original->timbrado_detalle_id_prefijo = '-'; // campo virtual pero requerido
        if (!$factura_original->save()) {
            throw new \Exception("Error interno restableciendo saldo de venta anterior:
             {$factura_original->getErrorSummaryAsString()}");
        }
    }

    /**
     * @param Venta $model Modelo de nota.
     * @param Venta $fatura_model Modelo de factura asociada.
     * @param null $old_factura_id
     * @throws \Exception
     * @throws \Throwable
     */
    private function manejarActivosBiologicos(&$model, &$fatura_model, $old_factura_id = null)
    {
        $sesion = Yii::$app->session;
        $sesion_key = 'cont_activo_bio_stock_manager';
        $actbios = (isset($fatura_model) ? $fatura_model->getActivosBiologicos() : []);

        // Borra stock managers de la nota por factura anterior.
        /** @var ActivoBiologicoStockManager[] $old_managersNota */
        $old_managersNota = ActivoBiologicoStockManager
            ::findAll(['nota_venta_id' => $model->id, 'factura_venta_id' => $old_factura_id]);
        foreach ($old_managersNota as $managerNota) {
            $aBio = $managerNota->activoBiologico;
            if (!$managerNota->delete()) {
                throw new Exception("Error interno borrando stock manager: {$managerNota->getErrorSummaryAsString()}.");
            }
            //$managerNota->refresh();
            $aBio->refresh();
            $aBio->recalcularStockActual();
//                $aBio->recalcularPrecioUnitarioStockActuales();
            if (!$aBio->save()) {
                throw new Exception("Error actualizando stock de activo biológico {$aBio->nombre}: {$aBio->getErrorSummaryAsString()}");
            }
        }

        // Si no es nota de credito o si $actbios es vacio, retornar.
        // Si $actbios es vacio, la venta actual no es de activos biologicos.
        if (!$model->isNotaCredito() || empty($actbios)) return;

        if (!empty($actbios) && (!$sesion->has($sesion_key))) {
            throw new \yii\base\Exception("Debe especificar los Activos Biológicos que fueron retornados. 
            Utilice el botón verde en la fila de plantillas correspondiente a Activo Biológico.");
        }

        $sessionData = $sesion->get($sesion_key);
        $dataProvider = $sessionData['dataProvider'];
        $venta_id = $sessionData['venta_id']; // compra asociada, desde modal.
        $allModels = $dataProvider->allModels;  // stock managers, desde modal.

        // Si cambio de factura pero no actualizo la informacion de activos biologicos (managers), error.
        if (isset($fatura_model) && $venta_id != $fatura_model->id) {
            throw new \Exception("Se ha cambiado de factura pero no ha actualizado los datos del activo fijo asociado.");
        }

        /** @var ActivoBiologicoStockManager[] $managersVenta */
        /** @var ActivoBiologicoStockManager[] $managersNota */
        $managersVenta = ActivoBiologicoStockManager::find()->where(['factura_venta_id' => (isset($fatura_model) ? $fatura_model->id : null)])->andWhere(['IS', 'nota_venta_id', null])->all();
        $managersNota = $allModels;

        self::checkCantidadDevuelta($managersNota, $managersVenta);

        foreach ($managersNota as $_managerNota) {
            $managerNota = new ActivoBiologicoStockManager();
            $managerNota->setAttributes($_managerNota->attributes);
            $managerNota->factura_venta_id = $model->factura_venta_id;
            $managerNota->nota_venta_id = $model->id;
            if (!$managerNota->save()) {
                throw new \Exception("Error interno guardando stock manager: 
                        {$managerNota->getErrorSummaryAsString()}");
            }
            $managerNota->refresh();
            $actbio = $managerNota->activoBiologico;
//                    $actbio->isEditable();
            if ($actbio->getInventario()->exists())
                throw new \Exception("Se intentaba actualizar el stock actual del activo
                         biológico {$actbio->nombre} pero se encuentra registrado en el cierre de inventario.
                         Por tanto no puede modificar.");
            $actbio->recalcularStockActual();
            if (!$actbio->save()) {
                throw new \Exception("Error actualizando activo biológico {$actbio->nombre}:
                         {$actbio->getErrorSummaryAsString()}");
            }
            echo "activo bio {$actbio->nombre} stock actual: {$actbio->stock_actual} y su precio actual es: {$actbio->precio_unitario_actual}<br/>";
        }
    }

    /**
     * @param ActivoBiologicoStockManager[] $managersNota
     * @param ActivoBiologicoStockManager[] $managersVenta
     * @throws \yii\base\Exception
     */
    private function checkCantidadDevuelta($managersNota, $managersVenta)
    {
        /*
         * Hay casos donde de las 10 vacas terneras, 9 se vendieron a X u.m. y 1 se vendio a Y u.m.
         * Esto crea dos managers con las mismas especies y clasificacion.
         * Entonces, para conocer la cantidad total de vacas terneras vendidas, se tienen que sumar el atributo `cantidad`
         * de todos los managers (creados desde venta) que coinciden en especie y clasificacion.
         *
         * Lo mismo es valido para managers creados por una nota.
         * Puede decidir devolverse de las 10
         */

        $cant_devueltas = [];
        $cant_vendidas = [];
        foreach ($managersNota as $manNota) {
            if (!isset($cant_devueltas["{$manNota->especie_id}-{$manNota->clasificacion_id}"]['cantidad']))
                $cant_devueltas["{$manNota->especie_id}-{$manNota->clasificacion_id}"]['cantidad'] = 0;
            $cant_devueltas["{$manNota->especie_id}-{$manNota->clasificacion_id}"]['cantidad'] += $manNota->cantidad;
        }
        foreach ($managersVenta as $mngVenta) {
            if (!isset($cant_vendidas["{$mngVenta->especie_id}-{$mngVenta->clasificacion_id}"]['cantidad']))
                $cant_vendidas["{$mngVenta->especie_id}-{$mngVenta->clasificacion_id}"]['cantidad'] = 0;
            $cant_vendidas["{$mngVenta->especie_id}-{$mngVenta->clasificacion_id}"]['cantidad'] += $mngVenta->cantidad;
        }
        Yii::warning(Json::encode($cant_devueltas));
        Yii::warning(Json::encode($cant_vendidas));
        foreach ($cant_devueltas as $key_dev => $cant_devuelta) {
            foreach ($cant_vendidas as $key_ven => $cant_vendida) {
                if ($key_dev == $key_ven) {
                    if ($cant_devuelta['cantidad'] > $cant_vendida['cantidad']) {
                        $slices = explode('-', $key_dev);
                        $especie_id = $slices[0];
                        $clasifi_id = $slices[1];
                        $activoFijo = ActivoBiologico::findOne(['especie_id' => $especie_id, 'clasificacion_id' => $clasifi_id])->nombre;
                        $msg = "La cantidad retornada ({$cant_devuelta['cantidad']}) de {$activoFijo} 
                        no puede ser mayor a la cantidad vendida ({$cant_vendida['cantidad']})";
                        throw new \yii\base\Exception("Error en los datos del activo biológico: {$msg}");
                    }
                    unset($cant_vendidas[$key_ven]); // para acelecer cada vez mas en ciclos posteriores.
                }
            }
        }
    }

    /**
     *  Maneja Activos Fijos que van a ser retornados por el comprador.
     *
     * @param Venta $model
     * @param Venta $factura_model
     * @param null|int $old_factura_id
     * @throws \yii\base\Exception
     */
    private function manejarActivosFijos(&$model, &$factura_model, $old_factura_id = null)
    {
        $model->refresh();
        !isset($factura_model) || $factura_model->refresh();
        $skey = 'cont_activofijo_ids';
        $dataProvider = Yii::$app->session->get($skey);
        $venta_id = $dataProvider['venta_id'];
        $ids = $dataProvider['ids'];
        $es_venta_de_activo_fijo = isset($factura_model) && $factura_model->getActivoFijos()->exists(); // si todos los activos fueron devueltos da false

        // activos fijos y managers anteriores.
        // Si es nueva nota o la factura anterior no era venta de activo fijo, no deberia entrar en este foreach.
        foreach (ActivoFijoStockManager::findAll(['factura_venta_id' => $old_factura_id,
            'venta_nota_credito_id' => $model->id]) as $oldStockManager) {
            /** @var ActivoFijo $oldAFijo */
            $oldAFijo = $oldStockManager->activoFijo;
            $oldAFijo->setFacturaVentaId($old_factura_id);
            $oldAFijo->setEstado('vendido');
            if (!$oldAFijo->save()) throw new \yii\base\Exception("Error interno validando activo
                 fijo anterior (id: {$oldAFijo->id}): {$oldAFijo->getErrorSummaryAsString()}");;
            $oldAFijo->refresh();

            if (!$oldStockManager->delete()) {
                throw new \Exception("Error interno borrando stock manager anterior: {$oldStockManager->getErrorSummaryAsString()}");
            }
        }

        if (!$es_venta_de_activo_fijo) {
            $es_venta_de_activo_fijo = isset($factura_model) && ActivoFijoStockManager::find()->where(['factura_venta_id' => $factura_model->id, 'venta_nota_credito_id' => $model->id])->exists();
        }

        if (!$model->isNotaCredito()) {
            Yii::warning("entro en isNotNotaCredito");
            return;
        }

        if ($es_venta_de_activo_fijo && Yii::$app->session->has($skey)) {
            // Si cambio de factura pero no se actualizo la sesion.
            if ($factura_model->id != $venta_id) {
                throw new \Exception("Se ha cambiado de factura asociada. Sin embargo los datos de activos fijos no se han actualizado.");
            }

            // Si no se especifico los activos fijos a devolver.
            if (!sizeof($ids)) {
                throw new \Exception("Debe especificar los Activos Fijos que fueron recuperados. 
                 Utilice el botón verde en la fila de plantillas correspondiente a Activo Fijo.");
            }

            // activos fijos y managers nuevos, correspondientes a los ids de activos fijos guardados en sesion.
            $costo_adq_total = -1;
            foreach ($ids as $id) {
                $newActivoFijo = ActivoFijo::findOne(['id' => $id]);
//                // no se permiten activos fijos ya asentados.?
//                if ($newActivoFijo->getAsiento()->exists())
//                    throw new \yii\base\Exception("Error desvinculando Activo Fijo '{$newActivoFijo->nombre}'
//                    (ID: {$newActivoFijo->id}): Este Activo Fijo tiene asociado asientos.");
                $newActivoFijo->setFacturaVentaId(null); // desasociar de la factura de venta.;
                $newActivoFijo->setEstado('activo');
                if (!$newActivoFijo->save()) throw new \yii\base\Exception("Error interno validando nuevo activo: {$newActivoFijo->getErrorSummaryAsString()}");;
                $newActivoFijo->refresh();

                if ($costo_adq_total == -1) $costo_adq_total = 0;
                $costo_adq_total += (float)$newActivoFijo->costo_adquisicion; // acumula el costo de adquisicion

                $newStockManager = new ActivoFijoStockManager();
                $newStockManager->setEmpresaPeriodoActuales(Yii::$app->session->get('core_empresa_actual'), Yii::$app->session->get('core_empresa_actual_pc'));
                $newStockManager->setFacturaVentaId($factura_model->id);
                $newStockManager->setActivoFijoId($id);
                $newStockManager->setVentaNotaCreditoId($model->id);
                if (!$newStockManager->save()) throw new \yii\base\Exception("Error interno validando nuevo stock manager: {$newStockManager->getErrorSummaryAsString()}");;
                $newStockManager->refresh();
            }

            // compara el total de costo de adquisicion contra el total de la factura
            // PARA VENTA no se va a hacer este control porque la suma de costos no es lo mismo que el total de la nota.
            // Al vender mayormente se vende a precio mayor que el costo de adquisicion.
//            if ($costo_adq_total != -1) {
//                $costo_adq_total *= 1.1;
//                $costo_adq_total = number_format($costo_adq_total, 0, '.', '');
//
//                //echo abs((float)$model->total - (float)$costo_adq_total); exit();
//                if (abs((float)$model->total - (float)$costo_adq_total) > 1) {
//                    $factura_total = number_format($model->total, 2, ',', '.');
//                    $costo_adq_total = number_format($costo_adq_total, 2, ',', '.');
//                    throw new \Exception("Error en el monto de la factura: La suma de costo de adquisicion
//                         {$costo_adq_total} no es igual al monto {$factura_total} de la factura");
//                }
//            }

            // Si cambio de factura, restaurar activos de la factura anterior
            Yii::warning("{$old_factura_id}, {$model->factura_venta_id}");
            if ($old_factura_id != $model->factura_venta_id) {
                $oldFacturaModel = Venta::findOne(['id' => $old_factura_id]);
                if (isset($oldFacturaModel) && $oldFacturaModel->getActivoFijos()->exists()) {
                    foreach (ActivoFijoStockManager::findAll(['factura_venta_id' => $old_factura_id,
                        'venta_nota_credito_id' => $model->id]) as $oldStockManager) {

                        // restaurar activos fijos
                        /** @var ActivoFijo $oldAFijo */
                        $oldAFijo = $oldStockManager->activoFijo;
                        $oldAFijo->setFacturaVentaId($old_factura_id);
                        $oldAFijo->setEstado('activo');
                        if (!$oldAFijo->save()) throw new \Exception("Error interno restableciendo activo fijo de la factura anterior
                         (id: {$oldAFijo->id}): {$oldAFijo->getErrorSummaryAsString()}");;
                        $oldAFijo->refresh();

                        if (!$oldStockManager->delete()) {
                            throw new \Exception("Error interno borrando stock manager de la factura anterior: {$oldStockManager->getErrorSummaryAsString()}");
                        }
                    }
                }
            }
        } elseif ($es_venta_de_activo_fijo && !Yii::$app->session->has($skey)) {
            throw new \yii\base\Exception("Debe especificar los Activos Fijos que fueron retornados. 
            Utilice el botón verde en la fila de plantillas correspondiente a Activo Fijo.");
        }
    }

    /**
     * @param VentaIvaCuentaUsada $iva_cta_nota
     * @param Venta $factura_model
     * @param Venta $model
     * @return bool
     */
    private function checkMontosIvaConFactura($iva_cta_nota, $factura_model, &$model)
    {
        if (isset($factura_model)) {
            $_factura_iva_monto = 0;
            foreach ($factura_model->ivasCuentaUsadas as $iva_cta_factura) {
                Yii::warning("ivactafac_ivactaid: {$iva_cta_factura->iva_cta_id}, ivactanota_ivaid: {$iva_cta_nota->iva_cta_id}, ivactaac_plantilla-id: {$iva_cta_factura->plantilla_id}, ivactanota_plantilla-id: {$iva_cta_nota->plantilla_id}");
                if ($iva_cta_factura->iva_cta_id == $iva_cta_nota->iva_cta_id && $iva_cta_factura->plantilla_id == $iva_cta_nota->plantilla_id) {
                    $_factura_iva_monto = round($iva_cta_factura->monto, 2);
                    break;
                }
            }
            $_notas_existentes_iva_monto = 0.00;
            foreach ($factura_model->notas as $_nota) {
                if ($_nota->id == $model->id) continue; // evitar nota actual.
                foreach ($_nota->ivasCuentaUsadas as $_iva_cta_nota) {
                    if ($_iva_cta_nota->iva_cta_id == $iva_cta_nota->iva_cta_id && $_iva_cta_nota->plantilla_id == $iva_cta_nota->plantilla_id) {
                        Yii::warning("Nota id: {$_nota->id}");
                        $_notas_existentes_iva_monto += round($_iva_cta_nota->monto, 2);
                        break;
                    }
                }
            }

//        if ($iva_cta_nota->monto > ($_factura_iva_monto - $_notas_existentes_iva_monto)) {
//            var_dump($iva_cta_nota->monto, $_factura_iva_monto, $_notas_existentes_iva_monto, $_iva_cta->id, $iva_cta_nota->id);
//            exit;
//        }

            $a = round($iva_cta_nota->monto, 2);
            $b = round($_factura_iva_monto - $_notas_existentes_iva_monto, 2);
            Yii::warning("ivactanota: $a, ivactafactura: $_factura_iva_monto, ivactanotaexistente: $_notas_existentes_iva_monto");
            if ($a > $b) {
                return false;
            }
        }

        return true;
    }

    public function actionGetFacturasByRuc($ruc, $q = null, $tipo = 'factura')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Venta::getFacturasListaByRucForNotas($ruc, $q, $tipo);
    }

    public function actionGetDatosFacturasById()
    {
        $factura_id = Yii::$app->request->get('factura_id');
        $nota_id = Yii::$app->request->get('nota_id');
        $action = Yii::$app->request->get('action_id');
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Venta::getDatosFacturasByIdForNotas($factura_id, $nota_id, $action);
    }

    /**
     * @param Venta $model Modelo para Nota
     * @param array $ivacuenta_guardar
     * @param bool $ningun_total
     * @param Venta $factura_model Modelo de factura de Venta
     * @throws Exception
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws StaleObjectException
     */
    private function manejarIvasCuentasUsadas(&$model, &$ivacuenta_guardar, &$ningun_total, $factura_model)
    {
        foreach ($model->ivasCuentaUsadas as $ivasCuentaUsada) {
            if (!$ivasCuentaUsada->delete())
                throw new \Exception("Error interno borrando iva cuenta usada: {$ivasCuentaUsada->getErrorSummaryAsString()}");
        }

        $_err_iva = false;
        $ivacuenta_usadas = Yii::$app->request->post('VentaIvaCuentaUsada', []);
        foreach ($ivacuenta_usadas as $ivacuenta_usada) {
            foreach (IvaCuenta::find()->all() as $iva_cta) {
                if (!empty($ivacuenta_usada['ivas']['iva_' . $iva_cta->iva->porcentaje])) {
                    //$iva_x_value = str_replace(',', '.', str_replace('.', '', $ivacuenta_usada['ivas']['iva_' . $iva_cta->iva->porcentaje]));
                    // 16 de agosto: en el post va como numero sin formato.
                    $iva_x_value = $ivacuenta_usada['ivas']['iva_' . $iva_cta->iva->porcentaje];
                    $ningun_total = false;

                    $ivacuenta_venta = new VentaIvaCuentaUsada();
                    $ivacuenta_venta->factura_venta_id = $model->id;
                    $ivacuenta_venta->iva_cta_id = isset($iva_cta->cuenta_venta_id) ? $iva_cta->id : null;
                    $ivacuenta_venta->plantilla_id = $ivacuenta_usada['plantilla_id'];
                    $ivacuenta_venta->plan_cuenta_id = $iva_cta->cuenta_venta_id;
                    $ivacuenta_venta->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                    $ivacuenta_venta->monto = (($model->moneda_id == ParametroSistema::getMonedaBaseId()) ? round($iva_x_value) : $iva_x_value);
                    $model->_iva_ctas_usadas[$ivacuenta_venta->plantilla_id][isset($ivacuenta_venta->iva_cta_id) ? (int)$iva_cta->iva->porcentaje : 0] = $ivacuenta_venta->monto; // para mostrar por si hay error

                    if ($_err_iva === false) {
                        if (isset($factura_model) && !$this->checkMontosIvaConFactura($ivacuenta_venta, $factura_model, $model)) {
                            throw new \yii\base\Exception('El Monto Total del ' . (empty($iva_cta->iva->porcentaje) ? 'Exenta' : ('IVA ' . $iva_cta->iva->porcentaje . '%')) . ' no puede ser mayor al de la factura');
                        } elseif (!$ivacuenta_venta->validate()) {
                            throw new \yii\base\Exception('ERROR INTERNO al guardar nota: ' . implode(' ', $ivacuenta_venta->getErrorSummary(true)));
                        } elseif ($ivacuenta_venta->plantilla_id == '')
                            throw new Exception("Error interno: iva cuenta no tiene plantilla_id");
                    }

                    $ivacuenta_guardar[] = $ivacuenta_venta;
                }
            }
        }
//        return $_err_iva;
    }

    public function actionGetRazonSocialByRuc()
    {
        $ruc = Yii::$app->request->post('ruc');
        $entidad = Entidad::findOne(['ruc' => $_POST['ruc']]);
        if ($entidad != null)
            return $entidad->razon_social;

        return '';
    }

    public function actionGetEntidadesForNotas($action, $q = null, $tipo = 'factura')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
//        if ($action == 'create')
        return Entidad::actionGetEntidadesForNotas('venta', $q, $tipo);
//        else
//            return [];
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionBloquear($id)
    {
        $model = Venta::findOne(['id' => $id]);

        if ($model->bloqueado == 'no') {
            $model->bloqueado = 'si';

            //Solo para validacion
            $model->timbrado_detalle_id_prefijo = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Nota ha sido bloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede bloquear nota. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionDesbloquear($id)
    {
        $model = Venta::findOne(['id' => $id]);

        if ($model->bloqueado == 'si') {
            $model->bloqueado = 'no';

            //Solo para validacion
            $model->timbrado_detalle_id_prefijo = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Nota ha sido desbloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede desbloquear nota. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionCalcDetalle()
    {
        /** @var DetalleVenta[] $detalles_debe */
        /** @var DetalleVenta[] $detalles */

        $total_factura = $_POST['cont_factura_total'];
        $porcentajes_iva = $_POST['ivas'];
        $tipo_docu_id = $_POST['tipo_docu_id'];
        $estado = Yii::$app->request->post('estado_comprobante', '');
        $moneda_id = $_POST['moneda_id'];

        // Si la variable del post recibe un array y si ese array es vacio, directamente no viene la variable.
        if (!array_key_exists('plantillas', $_POST)) {
            $detalles = [];
            goto retorno;
        }

        if (in_array($estado, ['anulada', 'faltante'])) {
            $detalles = [];
            goto retorno;
        }

        // Verificar tipo de documento
        $tipoDoc = TipoDocumento::findOne(['id' => $tipo_docu_id]);
        if ($tipoDoc == null) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar Tipo de Documento');
            return false;
        }

        $plantillas = $_POST['plantillas'];
        $plantillasModel = [];
        $totales_valor = [];
        $ids_previos = [];
        foreach ($plantillas as $pl) {
            $plantilla = PlantillaCompraventa::findOne(['id' => $pl['plantilla_id']]);

            if (in_array($plantilla->id, $ids_previos)) {
                FlashMessageHelpsers::createWarningMessage('No puede elegir dos plantillas iguales.');
                return false;
            }

            if ($plantilla != null) {
                // Valores iva por plantilla
                $valores_iva = [];
                foreach ($porcentajes_iva as $iva) {
                    $valores_iva[] = $pl['iva_' . $iva];
                }

                $plantillasModel[] = $plantilla;
                $totales_valor[$plantilla->id] = $valores_iva;
            }

            $ids_previos[] = $plantilla->id;
        }

//        Yii::$app->session->set('totales_valor', $totales_valor);

        // Verificar que se ha seleccionado una plantilla
        if (!isset($plantillasModel)) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar PLANTILLA');
            return false;
        }

        // Verificar que se haya seleccionado una moneda
        $moneda = Moneda::findOne(['id' => $moneda_id]);
        if ($moneda == null) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar una MONEDA');
            return false;
        }

        // Crear detalles por Plantilla e IVA's usadas.
        $detalles = [];
        $detalles = genDetallesByDefault($detalles, $tipoDoc, $total_factura, $totales_valor, $porcentajes_iva, $plantillasModel, $moneda_id);

        // Actualizar indices en la sesion
        $detalles = array_values($detalles);

        // Agrupar cuentas iguales
        $detalles = DetalleVenta::agruparCuentas($detalles);

        // Agrupar debe/haber
        $detalles = agruparDebeHaber($detalles);

        // guarda en la sesión
        retorno:;
        Yii::$app->getSession()->set('cont_detalleventa-provider', new ArrayDataProvider([
            'allModels' => $detalles,
            'pagination' => false,
        ]));

        return true;
    }

    /**
     * @return bool
     */
    public function actionSimular()
    {
        /** @var Cotizacion $cotizacion */
        /** @var DetalleVenta[] $detalles */
        /** @var DetalleVenta[] $detalles_para_sim */

        $hoy = date_create_from_format('d-m-Y', $_POST['hoy']);
        $moneda_id = $_POST['moneda_id'];
        $es_nota_credito = Yii::$app->request->post('es_nota') == 'nota_credito';
        $estado = Yii::$app->request->post('estado_comprobante', '');
        $total_factura = $_POST['cont_factura_total'];
        $detalles = Yii::$app->getSession()->get('cont_detalleventa-provider')->allModels;

        if ($total_factura == 0 || in_array($estado, ['anulada', 'faltante']) || sizeof($detalles) == 0) {
            $detalles_para_sim = [];
            $detalles_para_sim_costo = [];
            goto retorno;
        }

        $monedaBaseId = \common\models\ParametroSistema::getMonedaBaseId();
        // Cuando cambia de anulada/faltante a vigente, el campo fecha_emision es vacia y llega en POST vacio.
        if ($moneda_id != null && $moneda_id != $monedaBaseId && $hoy == false) {
            FlashMessageHelpsers::createInfoMessage('Debe especificar una fecha para poder obtener la cotización a utilizar.');
            return false;
//            $detalles_para_sim = [];
//            $detalles_para_sim_costo = [];
//            goto retorno;
        }

        $cotizacion = null;
        if ($moneda_id != null && $moneda_id != $monedaBaseId)
            $cotizacion = Cotizacion::find()
                ->where(['moneda_id' => $_POST['moneda_id']])
                ->andFilterWhere(['!=', 'moneda_id', Moneda::findOne(['nombre' => 'Guaraní'])->id])
                ->andFilterWhere(['fecha' => date('Y-m-d', strtotime($hoy->format('Y-m-d') . ' - 1 days'))])->one();
        $valor_moneda = Yii::$app->request->post('valor_moneda', '');
        $detalles = DetalleVenta::agruparCuentas($detalles);
        $detalles_para_sim = [];
        $plantillas = $_POST['plantillas'];

        // Convertir a guaranies si la moneda es extrangera.
        if (isset($cotizacion) && $valor_moneda != '') {
            $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
            $diff = (float)$valor_moneda - (float)$cotizacion->compra;
            $newDetalleDiffCambio = null;
            if ($diff != 0.0) {
                $newDetalleDiffCambio = new DetalleVenta();
                $newDetalleDiffCambio->cta_contable = $diff > 0.0 ? 'haber' : 'debe'; // TODO: parametro del modulo
                if ($es_nota_credito)
                    $newDetalleDiffCambio->cta_contable = $mapeo_inverso[$newDetalleDiffCambio->cta_contable];
                // 8.05 = resultado por diferencia de cambio como ingresos
                // 13.04 = resultado por diferencia de cambio como pérdida

                $empresa_id = \Yii::$app->session->get('core_empresa_actual');
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                $parmsys_debe = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_debe]);

                $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                $newDetalleDiffCambio->plan_cuenta_id = PlanCuenta::findOne(['id' => $diff > 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor])->id; // TODO: parametro del modulo
                $newDetalleDiffCambio->subtotal = (float)$diff * (float)$total_factura;
                $diff < 0.0 ? $newDetalleDiffCambio->subtotal = ((float)$newDetalleDiffCambio->subtotal * -1.0) : true;
            }
            foreach ($detalles as $detalle) {
                $newDetalle = new DetalleVenta();
                $newDetalle->subtotal = (float)$detalle->subtotal * ($detalle->planCuenta->nombre == 'CAJA' ? (float)$valor_moneda : (float)$cotizacion->compra);
                $newDetalle->plan_cuenta_id = $detalle->plan_cuenta_id;
                $newDetalle->cta_contable = $detalle->cta_contable;
                $detalles_para_sim[] = $newDetalle;
            }

            // insertar el asiento por diferencia de costo
            if (isset($newDetalleDiffCambio)) {
                $detalles_para_sim[] = $newDetalleDiffCambio;
            }
        } elseif ($valor_moneda != '') {
            foreach ($detalles as $detalle) {
                $newDetalle = new DetalleVenta();
                $newDetalle->subtotal = (float)$detalle->subtotal * (float)$valor_moneda;
                $newDetalle->plan_cuenta_id = $detalle->plan_cuenta_id;
                $newDetalle->cta_contable = $detalle->cta_contable;
                $detalles_para_sim[] = $newDetalle;
            }
        } else {
            // se usa moneda nacional, solo copiar los detalles al array para simulacion
            foreach ($detalles as $detalle) {
                $detalles_para_sim[] = $detalle;
            }
        }

        $detalles_para_sim = agruparDebeHaber($detalles_para_sim);


        /* *********************** ASIENTO DE COSTO *********************** */

        $coef_costo = (float)\backend\modules\contabilidad\models\ParametroSistema::getEmpresaActualCoefCosto();
        if ($coef_costo == 0) {
            $detalles_para_sim_costo = [];
//            FlashMessageHelpsers::createInfoMessage("La simulación de asiento por Costo de Venta no se muestra porque el coef. de costo en la empresa actual es cero.");
            goto retorno;
        }

        $detalles_para_sim_costo = [];
        if (sizeof($detalles_para_sim) > 0) {
            $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
            foreach ($plantillas as $key => $pl) {
                $plantilla = PlantillaCompraventa::findOne(['id' => $pl['plantilla_id']]);
                $_total_por_plantilla = sumAllIvas($pl);

                if ($plantilla->costo_mercad == 'si') {
                    /** @var PlantillaCompraventaDetalle[] $p_detalles_costo_merca */
                    $p_detalles_costo_merca = $plantilla->getDetalles('tipo_asiento', 'costo_venta');
                    foreach ($p_detalles_costo_merca as $p_d) {
                        $d = new DetalleVenta();
                        $d->cta_contable = !$es_nota_credito ? $p_d->tipo_saldo : $mapeo_inverso[$p_d->tipo_saldo];
                        $d->plan_cuenta_id = $p_d->p_c_gravada_id;
                        $d->subtotal = $coef_costo * round($_total_por_plantilla, 2);
                        if ($cotizacion != null)
                            $d->subtotal *= round($cotizacion->compra, 2);
                        $detalles_para_sim_costo[] = $d;
                    }
                }
            }
        }

        Yii::warning($detalles_para_sim_costo);
        $detalles_para_sim_costo = agruparDebeHaber($detalles_para_sim_costo);

        retorno:;
        // poner en sesion
        Yii::$app->getSession()->set('cont_detalleventa_sim-provider', new ArrayDataProvider([
            'allModels' => $detalles_para_sim,
            'pagination' => false,
        ]));
        Yii::$app->getSession()->set('cont_detalleventa_costo_sim-provider', new ArrayDataProvider([
            'allModels' => $detalles_para_sim_costo,
            'pagination' => false,
        ]));
        return true;
    }
}