<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\PrestamoDetalle;
use backend\modules\contabilidad\models\PrestamoDetalleCompra;
use backend\modules\contabilidad\models\ReciboPrestamoDetalle;
use backend\modules\contabilidad\models\search\PrestamoSearch;
use common\helpers\FlashMessageHelpsers;
use DateTime;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * PrestamoController implements the CRUD actions for Prestamo model.
 */
class PrestamoController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view', 'bloquear', 'desbloquear'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Prestamo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Prestamo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['bloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Prestamo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['desbloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Prestamo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'si';
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Prestamo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PrestamoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session->remove("cont_prestamo_cuotas");
        Yii::$app->session->remove('cont_prestamo_asentado');
        Yii::$app->session->remove('cont_asientodetalle_prestamo');
        Yii::$app->session->remove('cont_asientodetalle_prestamo_debe');
        Yii::$app->session->remove('cont_asientodetalle_prestamo_haber');
        Yii::$app->session->remove('cont_compra_actual_entidad_ruc');
        Yii::$app->session->remove('log-file');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Prestamo model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Prestamo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Prestamo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prestamo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Prestamo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \Exception
     */
    public function actionCreate()
    {
        $model = new Prestamo();
        $model->loadDefaultValues();

        $skey = 'cont_prestamo_cuotas';
        if (!Yii::$app->request->isPjax) {
            Yii::$app->session->set($skey, new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));

            Yii::$app->session->remove('cont_asientodetalle_prestamo');
        }

        if (!Yii::$app->request->isPjax && $model->load(Yii::$app->request->post())) {
            $model->tipo_interes = 'sm'; // El 8 de julio 2019: Jose hablo con magali que las cuotas seran todos a mano y que ya no se necesita tipo de interes.

            $this->removeBluePrintFromPostForPrestamoDetalle();

            $filas_cuota = array_key_exists('PrestamoDetalle', $_POST) ? $_POST['PrestamoDetalle'] : [];

            /** @var PrestamoDetalle[] $prestamoDetalles */
            $prestamoDetalles = [];
            foreach ($filas_cuota as $key => $fila) {
                $new_detalle = new PrestamoDetalle();
                $new_detalle->loadDefaultValues();

                /** @var array $temp Utilizado para que tome la forma requerida por $model->load(...) */
                $temp = [];
                $temp['PrestamoDetalle'] = $_POST['PrestamoDetalle'][$key];
                $new_detalle->load($temp);
                $prestamoDetalles[] = $new_detalle;
            }

            $transaccion = Yii::$app->db->beginTransaction();

            try {

                if (!$model->validate()) {
                    throw new \Exception("Error guardando prestamo: {$model->getErrorSummaryAsString()}");
                }

                if (sizeof($prestamoDetalles) == 0) {
                    throw new \Exception("Falta generar las cuotas del prestamo.");
                }

                $model->save(false);

                $result = $this->validate($prestamoDetalles, $model->id);
                if (!$result['result']) {
                    throw new \Exception($result['error']);
                }

                foreach ($prestamoDetalles as $key => $prestamoDetalle) {
                    if (!$prestamoDetalle->save(false)) {
                        throw new \Exception("Error guardando cuotas: {$prestamoDetalle->getErrorSummaryAsString()}");
                    }
                    $prestamoDetalle->refresh();
                }

                $model->refresh();

                if (!$model->validarMontoCuotas()) {
                    throw new \Exception("Error comprobando monto de las cuotas: {$model->getErrorSummaryAsString()}.");
                }

                if (!Prestamo::cuentasForAsientoPrestamoExists()) {
                    throw new \Exception("Falta especificar en el parametro de Empresa, las cuentas contables a utilizar para el asiento de prestamos.");
                }

                $transaccion->commit();
                FlashMessageHelpsers::createSuccessMessage("Préstamo #{$model->nro_prestamo} generado correctamente.");
                return $this->redirect(['index']);

            } catch (\Exception $exception) {
//                throw $exception;
                $model->formatDateField('view');
                $this->formatDateFieldDetalles($prestamoDetalles, 'view');
                Yii::$app->session->set($skey, new ArrayDataProvider([
                    'allModels' => $prestamoDetalles,
                    'pagination' => false,
                ]));
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    private function removeBluePrintFromPostForPrestamoDetalle()
    {

        if (array_key_exists('PrestamoDetalle', $_POST)) {
            if (!empty($_POST['PrestamoDetalle']['__new__'])) {
                unset($_POST['PrestamoDetalle']['__new__']);
                Yii::$app->request->setBodyParams($_POST);
            }
        }
    }

    private function validate(&$array, $prestamo_id)
    {
        $result = [
            'result' => true,
            'error' => "",
        ];

        foreach ($array as $item) {

            if ($item instanceof PrestamoDetalle) {

                $item->prestamo_id = $prestamo_id;
                $item->igualarSaldoMonto();

                if (!$item->validate()) {
                    $msg = implode(', ', $item->getErrorSummary(true));
                    $result['result'] = false;
                    $result['error'] = "Error validando cuotas: {$msg}";
                }
            }
        }

        return $result;
    }

    private function formatDateFieldDetalles(&$detalles, $_formatFor)
    {
        foreach ($detalles as $detalle) {
            if ($detalle instanceof PrestamoDetalle)
                $detalle->formatDateField($_formatFor);
        }
    }

    /**
     * Updates an existing Prestamo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->loadDefaultValues();

        if (!$model->isEditable()) {
            $msg = implode(', ', Prestamo::$_EDITABLE_ERROR_MSGS) . '.';
            FlashMessageHelpsers::createWarningMessage($msg);
            Prestamo::$_EDITABLE_ERROR_MSGS = [];
            return $this->redirect(['index']);
        }

        $model->formatDateField('view');
        $model->refillCalculatedFields();

        $skey = 'cont_prestamo_cuotas';
        if (!Yii::$app->request->isPjax) {
            Yii::$app->session->set($skey, new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));

            Yii::$app->session->remove('cont_asientodetalle_prestamo');
        }

        if (!Yii::$app->request->isPjax && $model->load(Yii::$app->request->post())) {
            $model->tipo_interes = 'sm'; // El 8 de julio 2019: Jose hablo con magali que las cuotas seran todos a mano y que ya no se necesita tipo de interes.

            $this->removeBluePrintFromPostForPrestamoDetalle();

            $filas_cuota = array_key_exists('PrestamoDetalle', $_POST) ? $_POST['PrestamoDetalle'] : [];

            /** @var PrestamoDetalle[] $prestamoDetalles */
            $prestamoDetalles = [];
            foreach ($filas_cuota as $key => $fila) {
                $new_detalle = new PrestamoDetalle();
                $new_detalle->loadDefaultValues();

                /** @var array $temp Utilizado para que tome la forma requerida por $model->load(...) */
                $temp = [];
                $temp['PrestamoDetalle'] = $_POST['PrestamoDetalle'][$key];
                $new_detalle->load($temp);
                $prestamoDetalles[] = $new_detalle;
            }

            $transaccion = Yii::$app->db->beginTransaction();

            try {

                if (!$model->validate()) {
                    throw new \Exception("Error guardando prestamo: {$model->getErrorSummaryAsString()}");
                }

                if (sizeof($prestamoDetalles) == 0) {
                    throw new \Exception("Falta generar las cuotas del prestamo.");
                }

                $model->save(false);

                $result = $this->validate($prestamoDetalles, $model->id);
                if (!$result['result']) {
                    throw new \Exception($result['error']);
                }

                // TODO: Ver si se puede borrar asi nomas los detalles.
                foreach ($model->prestamoDetalles as $prestamoDetalle) {
                    if (!$prestamoDetalle->delete()) {
                        throw new \Exception("Error borrando cuotas anteriores: {$prestamoDetalle->getErrorSummaryAsString()}");
                    }
                }

                foreach ($prestamoDetalles as $key => $prestamoDetalle) {

                    if (!$prestamoDetalle->save(false)) {
                        throw new \Exception("Error guardando cuotas: {$prestamoDetalle->getErrorSummaryAsString()}");
                    }
                    $prestamoDetalle->refresh();
                }

                $model->refresh();

                if (!$model->validarMontoCuotas()) {
                    throw new \Exception("Error comprobando monto de las cuotas: {$model->getErrorSummaryAsString()}.");
                }

                if (!Prestamo::cuentasForAsientoPrestamoExists()) {
                    throw new \Exception("Falta especificar en el parametro de Empresa, las cuentas contables a utilizar para el asiento de prestamos.");
                }

                $transaccion->commit();
                FlashMessageHelpsers::createSuccessMessage("Préstamo #{$model->nro_prestamo} generado correctamente.");
                return $this->redirect(['index']);

            } catch (\Exception $exception) {
                $transaccion->rollBack();
                $model->formatDateField('view');
//                $model->detalles = $prestamoDetalles;
                $this->formatDateFieldDetalles($prestamoDetalles, 'view');
                Yii::$app->session->set($skey, new ArrayDataProvider([
                    'allModels' => $prestamoDetalles,
                    'pagination' => false,
                ]));
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionCargarCuotasForUpdate($id)
    {
        $cuotas = PrestamoDetalle::findAll(['prestamo_id' => $id]);
        $allModels = [];
        foreach ($cuotas as $cuota) {
            $cuota->formatDateField('view');
            /** @var Prestamo $prestamo */
            $prestamo = $cuota->prestamo;
            Yii::warning($prestamo->usar_iva_10);
            if ($prestamo->usar_iva_10 == 'si' || ($prestamo->tiene_factura == 'por_cuota'))
                $cuota->impuesto = round($cuota->interes / 10);
            $allModels[] = $cuota;
        }

        $skey = 'cont_prestamo_cuotas';
        Yii::$app->session->set($skey, new ArrayDataProvider(['allModels' => $allModels, 'pagination' => false]));
    }

    /**
     * Deletes an existing Prestamo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $transaccion = Yii::$app->db->beginTransaction();

        try {
            if (!$model->isEditable()) {
                $msg = implode(', ', Prestamo::$_EDITABLE_ERROR_MSGS);
                FlashMessageHelpsers::createWarningMessage($msg);
                Prestamo::$_EDITABLE_ERROR_MSGS = [];
                return $this->redirect(['index']);
            }

            foreach ($model->prestamoDetalles as $detalle) {
                if (!$detalle->delete()) {
                    throw new \Exception("Error borrando cuotas: {$detalle->getErrorSummaryAsString()}");
                }
            }

            if (!$model->delete()) {
                throw new \Exception("Error borrando prestamo: {$model->getErrorSummaryAsString()}");
            }

            $model->delete();
            $transaccion->commit();
            FlashMessageHelpsers::createSuccessMessage("Prestamo #{$model->id} se ha borrado exitosamente.");

        } catch (\Exception $exception) {
            $transaccion->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /**
     * @param $prestamo_detalles PrestamoDetalle[]
     * @param $detalle PrestamoDetalle
     * @return int|mixed|string
     */
    private function buscarDetalle(&$prestamo_detalles, &$detalle)
    {
        foreach ($prestamo_detalles as $key => $element) {
            if ($element->id == $detalle->id)
                return $key;
        }

        return -1;
    }

    /**
     * @param $_model Prestamo
     * @return Prestamo
     */
    private function copyAttributesFrom($_model)
    {
        $model = new Prestamo();
        $model->loadDefaultValues();
        $model->setAttributes($_model->attributes);
        $model->prestamo_id_selector = $_model->prestamo_id_selector;
        $model->ruc = $_model->ruc;
        $model->razon_social = $_model->razon_social;
        return $model;
    }

    /**
     *
     * [prestamo_id_selector] siempre se guarda en la session asi que no hace falta reasignar cuando se recupera de alli
     *
     * @param bool $submit
     * @param null $compra_id
     * @param null $prestamo_id
     * @param null $fila_plantilla Prefijo comun de los campos de una fila de plantilla, en el form de compra.
     * @param string $accion
     * @return array|string
     */
    public function actionManejarCuotaPrestamoDesdeCompra($submit = false, $compra_id = null, $ruc = null, $prestamo_id = null, $fila_plantilla = null, $accion = "")
    {
        /** @var Prestamo $model */
        $model = new Prestamo();
        $model->loadDefaultValues();

        $session_key = 'cont_prestamos_compra';
        $skey_para_form_detalle = 'cont_prestamo_cuotas';
        $session = Yii::$app->session;

        if (isset($fila_plantilla)) // se espera que solo se ejecute la 1ra vez que se abre el modal
            $session->set('cont_fila_plantilla', $fila_plantilla);

        if (isset($compra_id) && !$session->has('cont_compra_actual_id'))
            $session->set('cont_compra_actual_id', $compra_id);

        if (isset($ruc) && !$session->has('cont_compra_actual_entidad_ruc'))
            $session->set('cont_compra_actual_entidad_ruc', $ruc);

        if ($submit != true) {
            $primera_vez = Yii::$app->request->get('primera_vez');
            if ($primera_vez == 'true') {

                if ($session->has($session_key)) {

                    // Mostrar el primer prestamo guardado en sesion
                    $data = $session->get($session_key, []);
                    $model = $this->copyAttributesFrom($data[0]['prestamo']);
                    $model_detalles = $data[0]['detalles'];

                    $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);
                    $session->set($skey_para_form_detalle, $dataProvider);
                } else {

                    // Mostrar el primero de los prestamos libres
                    $compra_id = $session->get('cont_compra_actual_id');
                    $ruc = $session->get('cont_compra_actual_entidad_ruc');
                    $ids_prestamo_libre = $this->actionGetPrestamosLibresParaCompraAjax(null, null, $compra_id, $ruc, false);

                    $model = Prestamo::findOne($ids_prestamo_libre['results'][0]['id']);
                    $model->prestamo_id_selector = $model->id;
                    $model->formatDateField('view');
                    $model->refillCalculatedFields();

                    $model_detalles = $model->prestamoDetallesConSaldo;
                    $this->formatDateFieldDetalles($model_detalles, 'view');

                    $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);
                    $session->set($skey_para_form_detalle, $dataProvider);
                }

            } else {
                if ($accion == 'mostrar_datos') {
                    if ($prestamo_id != '') {
                        if ($session->has($session_key)) {

                            $data = $session->get($session_key, []);
                            $key = $this->searchPrestamo($data, $prestamo_id, null);

                            if (isset($key)) {
                                $model = $this->copyAttributesFrom($data[$key]['prestamo']);

                                $model_detalles = $data[$key]['detalles'];

                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                                $session->set($skey_para_form_detalle, $dataProvider);

                            } else {
                                $model = Prestamo::findOne(['id' => $prestamo_id]);
                                $model->prestamo_id_selector = $model->id;
                                $model->formatDateField('view');
                                $model->refillCalculatedFields();

                                $model_detalles = $model->prestamoDetallesConSaldo;
                                $this->formatDateFieldDetalles($model_detalles, 'view');

                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                                $session->set($skey_para_form_detalle, $dataProvider);
                            }

                        } else {

                            $model = Prestamo::findOne(['id' => $prestamo_id]);
                            $model->prestamo_id_selector = $model->id;
                            $model->formatDateField('view');
                            $model->refillCalculatedFields();

                            $model_detalles = $model->prestamoDetallesConSaldo;
                            $this->formatDateFieldDetalles($model_detalles, 'view');

                            $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                            $session->set($skey_para_form_detalle, $dataProvider);
                        }
                    }

                } else { // anterior, siguiente o borrar actual

                    if (!$session->has($session_key)) {

                        $model = Prestamo::findOne(['id' => $prestamo_id]);
                        $model->prestamo_id_selector = $model->id;
                        $model->formatDateField('view');
                        $model->refillCalculatedFields();

                        $model_detalles = $model->prestamoDetallesConSaldo;
                        $this->formatDateFieldDetalles($model_detalles, 'view');

                        $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                        $session->set($skey_para_form_detalle, $dataProvider);

                    } else {

                        $data = $session->get($session_key, []);

                        if ($accion == 'borrar-actual') {

                            $t = sizeof($data);

                            $this->removePrestamo($data, $prestamo_id);

                            $t = sizeof($data);

                            if (sizeof($data) == 0) {

                                $session->remove($session_key);

                                // Mostrar el primero de los prestamos libres
                                $compra_id = $session->get('cont_compra_actual_id');
                                $ruc = $session->get('cont_compra_actual_entidad_ruc');
                                $ids_prestamo_libre = $this->actionGetPrestamosLibresParaCompraAjax(null, null, $compra_id, $ruc, false);

                                $model = Prestamo::findOne(['id' => $ids_prestamo_libre['results'][0]['id']]);
                                $model->prestamo_id_selector = $model->id;
                                $model->formatDateField('view');
                                $model->refillCalculatedFields();

                                $model_detalles = $model->prestamoDetallesConSaldo;
                                $this->formatDateFieldDetalles($model_detalles, 'view');

                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);
                                $session->set($skey_para_form_detalle, $dataProvider);

                            } else {

                                $session->set($session_key, $data);

                                $model = $data[0]['prestamo']; // elegir el primero o el unico que hay en sesion

                                $model_detalles = $data[0]['detalles'];
                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);

                                $session->set($skey_para_form_detalle, $dataProvider);
                            }

                        } else {

                            $key = $this->searchPrestamo($data, $prestamo_id, $accion);

                            if (isset($key)) {
                                $model = $this->copyAttributesFrom($data[$key]['prestamo']);

                                $model_detalles = $data[$key]['detalles'];
                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);

                                $session->set($skey_para_form_detalle, $dataProvider);
                            } else {
                                $model = Prestamo::findOne(['id' => $prestamo_id]);
                                $model->formatDateField('view');
                                $model->prestamo_id_selector = $model->id;
                                $model->refillCalculatedFields();

                                $model_detalles = $model->prestamoDetallesConSaldo;
                                $this->formatDateFieldDetalles($model_detalles, 'view');

                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                                $session->set($skey_para_form_detalle, $dataProvider);
                            }
                        }
                    }
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit != true) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($model);
            $msg = implode(', ', $model->getErrorSummary(true));
            if ($model->hasErrors()) FlashMessageHelpsers::createWarningMessage($msg);
            return $result;
        }

        $valor_para_exenta = 0.0;
        $valor_para_iva_10 = 0.0;
        if ($submit == true) {
            $prestamo_id = $_POST['Prestamo']['prestamo_id_selector'];
            $model = Prestamo::findOne(['id' => $prestamo_id]);
            $model->prestamo_id_selector = $model->id;
            $model->formatDateField('view');
            $model->refillCalculatedFields();

            $this->removeBluePrintFromPostForPrestamoDetalle();

            $model_detalles = [];
            foreach ($_POST['PrestamoDetalle'] as $key => $post_pDetalle) {
                $newDetalle = new PrestamoDetalle();
                $newDetalle->loadDefaultValues();
                $data = [];
                $data["PrestamoDetalle"] = $post_pDetalle;
                $newDetalle->load($data);
                $newDetalle->id = $post_pDetalle['id'];
                $newDetalle->prestamo_id = $prestamo_id;
                $newDetalle->selected = $post_pDetalle['selected']; // en mi pc no se renderiza el checkbox
                $newDetalle->recibo_capital_nro = $post_pDetalle['recibo_capital_nro'];
                $model_detalles[] = $newDetalle;
            }

            $data = [];
            if ($session->has($session_key)) {
                $data = $session->get($session_key, []);
            }

            $this->addOrReplacePrestamo($data, $model, $model_detalles);
            $session->set($session_key, $data);

            $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
            $session->set($skey_para_form_detalle, $dataProvider);

            // calcular valor para exenta
            foreach ($data as $item) {
                foreach ($item['detalles'] as $detalle) {
                    if ($detalle['selected'] == '1') {
                        $valor_para_exenta += ($item['prestamo']->usar_iva_10 == 'no') ? (float)$detalle['monto_cuota_saldo'] : 0.0;
                        $valor_para_iva_10 += ($item['prestamo']->usar_iva_10 == 'si') ? (float)$detalle['monto_cuota_saldo'] : 0.0;
                    }
                }
            }
            $session->set('cont_compra_valor_para_plantilla', ['exenta' => $valor_para_exenta, 'iva_10' => $valor_para_iva_10]);

//            $session->remove('cont_fila_plantilla');
            $session->remove('cont_compra_actual_id');
            $session->remove('cont_compra_actual_entidad_ruc');
        }

        retorno:;
        return $this->renderAjax('modal/_modal_desde_compra', [
            'model' => $model,
            'fila_plantilla' => Yii::$app->session->get('cont_fila_plantilla'),
            'compra_id' => $session->get('cont_compra_actual_id'),
        ]);
    }

    public function actionManejarCuotaPrestamoDesdeRecibo($submit = false, $recibo_id = null, $ruc = null, $prestamo_id = null, $accion = "")
    {
        /** @var Prestamo $model */
        $model = new Prestamo();
        $model->loadDefaultValues();

        $session_key = 'cont_prestamos_recibo';
        $skey_para_form_detalle = 'cont_prestamo_cuotas';
        $session = Yii::$app->session;

        if (isset($recibo_id) && !$session->has('cont_recibo_actual_id'))
            $session->set('cont_recibo_actual_id', $recibo_id);

        if (isset($ruc) && !$session->has('cont_recibo_actual_entidad_ruc'))
            $session->set('cont_recibo_actual_entidad_ruc', $ruc);

        if ($submit != true) {
            $primera_vez = Yii::$app->request->get('primera_vez');
            if ($primera_vez == 'true') {

                if ($session->has($session_key)) {

                    // Mostrar el primer prestamo guardado en sesion
                    $data = $session->get($session_key, []);
                    $model = $this->copyAttributesFrom($data[0]['prestamo']);
                    $model_detalles = $data[0]['detalles'];

                    $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);
                    $session->set($skey_para_form_detalle, $dataProvider);
                } else {

                    // Mostrar el primero de los prestamos libres
                    $recibo_id = $session->get('cont_recibo_actual_id');
                    $ruc = $session->get('cont_recibo_actual_entidad_ruc');
                    $ids_prestamo_libre = $this->actionGetPrestamosLibresParaReciboAjax(null, null, $recibo_id, $ruc, false);

                    $model = Prestamo::findOne($ids_prestamo_libre['results'][0]['id']);
                    $model->prestamo_id_selector = $model->id;
                    $model->formatDateField('view');
                    $model->refillCalculatedFields();

                    $model_detalles = $model->prestamoDetallesConSaldo;
                    $this->formatDateFieldDetalles($model_detalles, 'view');

                    $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);
                    $session->set($skey_para_form_detalle, $dataProvider);
                }

            } else {
                if ($accion == 'mostrar_datos') {
                    if ($prestamo_id != '') {
                        if ($session->has($session_key)) {

                            $data = $session->get($session_key, []);
                            $key = $this->searchPrestamo($data, $prestamo_id, null);

                            if (isset($key)) {
                                $model = $this->copyAttributesFrom($data[$key]['prestamo']);

                                $model_detalles = $data[$key]['detalles'];

                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                                $session->set($skey_para_form_detalle, $dataProvider);

                            } else {
                                $model = Prestamo::findOne(['id' => $prestamo_id]);
                                $model->prestamo_id_selector = $model->id;
                                $model->formatDateField('view');
                                $model->refillCalculatedFields();

                                $model_detalles = $model->prestamoDetallesConSaldo;
                                $this->formatDateFieldDetalles($model_detalles, 'view');

                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                                $session->set($skey_para_form_detalle, $dataProvider);
                            }

                        } else {

                            $model = Prestamo::findOne(['id' => $prestamo_id]);
                            $model->prestamo_id_selector = $model->id;
                            $model->formatDateField('view');
                            $model->refillCalculatedFields();

                            $model_detalles = $model->prestamoDetallesConSaldo;
                            $this->formatDateFieldDetalles($model_detalles, 'view');

                            $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                            $session->set($skey_para_form_detalle, $dataProvider);
                        }
                    }

                } else { // anterior, siguiente o borrar actual

                    if (!$session->has($session_key)) {

                        $model = Prestamo::findOne(['id' => $prestamo_id]);
                        $model->prestamo_id_selector = $model->id;
                        $model->formatDateField('view');
                        $model->refillCalculatedFields();

                        $model_detalles = $model->prestamoDetallesConSaldo;
                        $this->formatDateFieldDetalles($model_detalles, 'view');

                        $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                        $session->set($skey_para_form_detalle, $dataProvider);

                    } else {

                        $data = $session->get($session_key, []);

                        if ($accion == 'borrar-actual') {

                            $this->removePrestamo($data, $prestamo_id);

                            if (sizeof($data) == 0) {

                                $session->remove($session_key);

                                // Mostrar el primero de los prestamos libres
                                $recibo_id = $session->get('cont_recibo_actual_id');
                                $ruc = $session->get('cont_recibo_actual_entidad_ruc');
                                $ids_prestamo_libre = $this->actionGetPrestamosLibresParaReciboAjax(null, null, $recibo_id, $ruc, false);

                                $model = Prestamo::findOne(['id' => $ids_prestamo_libre['results'][0]['id']]);
                                $model->prestamo_id_selector = $model->id;
                                $model->formatDateField('view');
                                $model->refillCalculatedFields();

                                $model_detalles = $model->prestamoDetallesConSaldo;
                                $this->formatDateFieldDetalles($model_detalles, 'view');

                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);
                                $session->set($skey_para_form_detalle, $dataProvider);

                            } else {

                                $session->set($session_key, $data);

                                $model = $data[0]['prestamo']; // elegir el primero o el unico que hay en sesion

                                $model_detalles = $data[0]['detalles'];
                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);

                                $session->set($skey_para_form_detalle, $dataProvider);
                            }

                        } else {

                            $key = $this->searchPrestamo($data, $prestamo_id, $accion);

                            if (isset($key)) {
                                $model = $this->copyAttributesFrom($data[$key]['prestamo']);

                                $model_detalles = $data[$key]['detalles'];
                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false]);

                                $session->set($skey_para_form_detalle, $dataProvider);
                            } else {
                                $model = Prestamo::findOne(['id' => $prestamo_id]);
                                $model->formatDateField('view');
                                $model->prestamo_id_selector = $model->id;
                                $model->refillCalculatedFields();

                                $model_detalles = $model->prestamoDetallesConSaldo;
                                $this->formatDateFieldDetalles($model_detalles, 'view');

                                $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
                                $session->set($skey_para_form_detalle, $dataProvider);
                            }
                        }
                    }
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit != true) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($model);
            if ($model->hasErrors()) FlashMessageHelpsers::createWarningMessage($model->getErrorSummaryAsString());
            return $result;
        }

        $total = 0.0;
        if ($submit == true) {
            $prestamo_id = $_POST['Prestamo']['prestamo_id_selector'];
            $model = Prestamo::findOne(['id' => $prestamo_id]);
            $model->prestamo_id_selector = $model->id;
            $model->formatDateField('view');
            $model->refillCalculatedFields();

            $this->removeBluePrintFromPostForPrestamoDetalle();

            $model_detalles = [];
            foreach ($_POST['PrestamoDetalle'] as $key => $post_pDetalle) {
                $newDetalle = new PrestamoDetalle();
                $newDetalle->loadDefaultValues();
                $data = [];
                $data["PrestamoDetalle"] = $post_pDetalle;
                $newDetalle->load($data);
                $newDetalle->id = $post_pDetalle['id'];
                $newDetalle->prestamo_id = $prestamo_id;
                $newDetalle->selected = $post_pDetalle['selected']; // en mi pc no se renderiza el checkbox
                $model_detalles[] = $newDetalle;
            }

            $data = [];
            if ($session->has($session_key)) {
                $data = $session->get($session_key, []);
            }

            $this->addOrReplacePrestamo($data, $model, $model_detalles);
            if (!sizeof($data))
                $session->remove($session_key);
            else
                $session->set($session_key, $data);

            foreach ($data as $datum) {
                foreach ($datum['detalles'] as $detalle) {
                    $total += $detalle->selected == '1' ? (float)$detalle->monto_cuota_saldo : 0.0;
                }
            }
            $session->set('cont_recibo_valor_total', $total);

            $dataProvider = new ArrayDataProvider(['allModels' => $model_detalles, 'pagination' => false,]);
            $session->set($skey_para_form_detalle, $dataProvider);

            $session->remove('cont_recibo_actual_id');
            $session->remove('cont_recibo_actual_entidad_ruc');
        }


        retorno:;
        return $this->renderAjax('modal/_modal_desde_recibo', [
            'model' => $model,
            'recibo_id' => $session->get('cont_recibo_actual_id'),
            'total' => $total,
        ]);
    }

    public function actionGetValorParaExenta()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->session->get('cont_compra_valor_para_plantilla');
    }

    public function actionRetrieveValorParaTotalRecibo()
    {
        return (string)Yii::$app->session->get('cont_recibo_valor_total');
    }

    private function removePrestamo(&$data, $id)
    {
        foreach ($data as $key => $item) {
            if ($item['prestamo']->id == $id) {
                unset($data[$key]);
                $data = array_values($data);
                return true;
            }
        }
        return false;
    }

    private function searchPrestamo(&$data, $id, $accion = null)
    {
        foreach ($data as $key => $item) {
            if ($item['prestamo']->id == $id) {
                if ($accion == null) {
                    return $key;
                } else {
                    if ($accion == 'anterior') {
                        $t = $key - 1;
                        if (array_key_exists(($key - 1), $data)) {
                            $key = $key - 1;
                            return $key;
                        } else {
                            return $key;
                        }
                    } elseif ($accion == 'siguiente') {
                        $t = $key + 1;
                        if (array_key_exists(($key + 1), $data)) {
                            return $key + 1;
                        } else {
                            return $key;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * @param $data array
     * @param $prestamo Prestamo
     * @param $detalles PrestamoDetalle[]
     */
    private function addOrReplacePrestamo(&$data, $prestamo, $detalles)
    {
        $no_selection = true;
        foreach ($detalles as $detalle) {
            if ($detalle->selected) {
                $no_selection = false;
                break;
            }
        }

        if ($no_selection) {
            foreach ($data as $key => $object) {
                if ($data[$key]['prestamo']->id == $prestamo->id) {
                    unset($data[$key]);
                    $data = array_values($data);
                    return;
                }
            }
        }

        foreach ($data as $key => $object) {
            $t = $object['prestamo']->id;
            if ($object['prestamo']->id == $prestamo->id) {
                $data[$key]['prestamo'] = $prestamo;
                $data[$key]['detalles'] = $detalles;
                return;
            }
        }

        $data[] = [
            'prestamo' => $prestamo,
            'detalles' => $detalles,
        ];
    }

    /** Retorna un array bidimensional, cuya primera columna contiene los ids de prestamos libres
     *
     * Un prestamo es libre si al menos posee una cuota con algun campo de saldo > 0.
     * Se filtran por aquellos prestamos pertenecientes a empresa y periodo actual.
     *
     * Es utilizado tanto por el widget Select2 del _modal como por la action "manejarPrestamoDesdeCompra".
     *
     * @param null $q
     * @param null $id
     * @param null $compra_id
     * @param null $ruc
     * @param bool $json_format
     * @return array
     */
    public function actionGetPrestamosLibresParaCompraAjax($q = null, $id = null, $compra_id = null, $ruc = null, $json_format = true)
    {
        if ($json_format) Yii::$app->response->format = Response::FORMAT_JSON;

        $prestamos = [];
        $entidad = Entidad::find()->where(['ruc' => $ruc]);
        $entidad_id = $entidad->exists() ? $entidad->one()->id : null;

        $prestamo_asociado = Prestamo::find()->alias('prestamo');
        $prestamo_asociado->leftJoin('cont_prestamo_detalle as detalle', 'prestamo.id = detalle.prestamo_id');
        $prestamo_asociado->select([
            'prestamo.id AS id',
            "CONCAT(('Préstamo #'), (prestamo.nro_prestamo)) AS text",
            'prestamo.nro_prestamo'
        ]);
        $prestamo_asociado->where(['detalle.empresa_id' => Yii::$app->session->get('core_empresa_actual')]);
//        $prestamo_asociado->andWhere(['usar_iva_10' => 'si']); // en compra, puede facturar por cuotas de aquellos prestamos que usan iva 10 como los que no.
        $prestamo_asociado->andWhere(['detalle.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
        $prestamo_asociado->andWhere(['prestamo.entidad_id' => $entidad_id]);
        // Filtrar solo cuotas con saldo.
        $prestamo_asociado->andWhere(
            [
                'OR',
                ['!=', 'detalle.monto_cuota_saldo', 0],
                [
                    'OR',
                    ['!=', 'detalle.interes_saldo', 0],
                    ['!=', 'detalle.capital_saldo', 0]
                ]
            ]
        );

        /** @var array $prestamo_ids Arreglo de ids de prestamos cuyas cuotas son devengadas generando ReciboPrestamos. */
        $prestamo_ids = [];
        /** @var ReciboPrestamoDetalle[] $prestamo_detalle_recibos */
        $prestamo_detalle_recibos = ReciboPrestamoDetalle::find()->all();

        foreach ($prestamo_detalle_recibos as $_pdr) {
            if (!in_array($_pdr->prestamoDetalle->prestamo_id, $prestamo_ids)) {
                $prestamo_ids[] = $_pdr->prestamoDetalle->prestamo_id;
            }
        }

        // excluir prestamos que tenga al menos una cuota devengada mediante recibo.
        // 19 FEBRERO 2019: JOSE SOLICITA REMOVER ESTA RESTRICCION PORQUE DICE AHORA QUE
        // HAY CASOS DONDE UN MES TIENE FACTURA Y OTROS NO, SIENDO LA MISMA CUOTA DE UN SOLO PRESTAMO.
//        $prestamo_asociado->andWhere(['NOT IN', 'prestamo.id', $prestamo_ids]);

        // Incluir cuotas asociadas a la compra
        if ($compra_id != "") {
            $prestamo_detalle_compras = PrestamoDetalleCompra::findAll(['factura_compra_id' => $compra_id]);
            $prestamo_detalles_id = [];

            foreach ($prestamo_detalle_compras as $item)
                $prestamo_detalles_id[] = $item->prestamo_detalle_id;

            $prestamo_asociado = $prestamo_asociado->orWhere(['AND', ['=', 'prestamo.entidad_id', $entidad_id], ['IN', 'detalle.id', $prestamo_detalles_id]]);
        }

        // se emiten facturas por cuotas solamente si el iva del interes se paga junto con las cuotas.
        // entonces se filtra solamente prestamos con iva_interes_cobrado == no.
        // 28 03 19 se elimina la columna iva_interes_cobrado, y tiene_factura tiene una opcion mas para contemplar la funcion de esta col eliminada.
        $prestamo_asociado->andWhere(['prestamo.tiene_factura' => 'por_cuota']); // El significado real del iva_interes_cobrado es iva_interes_facturado. Su significado cambio el 25 de marzo de 19.

        $prestamo_asociado = $prestamo_asociado->groupBy('prestamo.id');
        $results = $prestamo_asociado->asArray()->all();

        return ['results' => array_values($results)];
    }

    /** Retorna un array bidimensional, cuya primera columna contiene los ids de prestamos libres
     *
     * Un prestamo es libre si al menos posee una cuota con algun campo de saldo > 0.
     * Se filtran por aquellos prestamos pertenecientes a empresa y periodo actual.
     *
     * Es utilizado tanto por el widget Select2 del _form de recibo-prestamo
     *
     * @param null $q
     * @param null $id
     * @param null $recibo_id
     * @param null $ruc
     * @param bool $json_format
     * @return array
     */
    public function actionGetPrestamosLibresParaReciboAjax($q = null, $id = null, $recibo_id = null, $ruc = null, $json_format = true)
    {
        if ($json_format) Yii::$app->response->format = Response::FORMAT_JSON;

        $entidad = Entidad::find()->where(['ruc' => $ruc]);
        $entidad_id = $entidad->exists() ? $entidad->one()->id : null;

        $prestamos = [];

        $prestamo_asociado = Prestamo::find()->alias('prestamo');
        $prestamo_asociado->leftJoin('cont_prestamo_detalle as detalle', 'prestamo.id = detalle.prestamo_id');
        $prestamo_asociado->select([
            'prestamo.id AS id',
            "CONCAT(('Préstamo #'), (prestamo.nro_prestamo)) AS text",
            'prestamo.nro_prestamo',
        ]);
        $prestamo_asociado->where(['detalle.empresa_id' => Yii::$app->session->get('core_empresa_actual')]);
        /*en recibo, es incoherente registrar recibos por cuotas de aquellos prestamos que usan iva 10 porque recibos no pueden ser deducidos. */
//        $prestamo_asociado->andWhere(['usar_iva_10' => 'no']);
        $prestamo_asociado->andWhere(['detalle.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
        $prestamo_asociado->andWhere(['prestamo.entidad_id' => $entidad_id]);
        // Filtrar cuotas con saldo.
        $prestamo_asociado->andWhere(
            [
                'OR',
                ['!=', 'detalle.monto_cuota_saldo', 0],
                [
                    'OR',
                    ['!=', 'detalle.interes_saldo', 0],
                    ['!=', 'detalle.capital_saldo', 0]
                ]
            ]
        );

        /** @var array $prestamo_ids Arreglo de ids de prestamos cuyas cuotas son devengadas generando fact.compra. */
        $prestamo_ids = [];
        /** @var PrestamoDetalleCompra[] $prestamo_detalle_compra */
        $prestamo_detalle_compra = PrestamoDetalleCompra::find()->all();

        foreach ($prestamo_detalle_compra as $_pdc) {
            if (!in_array($_pdc->prestamoDetalle->prestamo_id, $prestamo_ids)) {
                $prestamo_ids[] = $_pdc->prestamoDetalle->prestamo_id;
            }
        }

        // excluir prestamos que tenga al menos una cuota devengada mediante compra.
        // 19 FEBRERO 2019: JOSE SOLICITA REMOVER ESTA RESTRICCION PORQUE DICE AHORA QUE
        // HAY CASOS DONDE UN MES TIENE FACTURA Y OTROS NO, SIENDO LA MISMA CUOTA DE UN SOLO PRESTAMO.
//        $prestamo_asociado->andWhere(['NOT IN', 'prestamo.id', $prestamo_ids]);

        // Incluir cuotas asociadas al recibo
        if ($recibo_id != "") {
            /** @var ReciboPrestamoDetalle[] $reciboPrestamoDetalles */
            $reciboPrestamoDetalles = ReciboPrestamoDetalle::findAll(['recibo_prestamo_id' => $recibo_id]);
            $prestamo_detalles_id = [];

            foreach ($reciboPrestamoDetalles as $item)
                $prestamo_detalles_id[] = $item->prestamo_detalle_id;

            $prestamo_asociado = $prestamo_asociado->orWhere(['AND', ['=', 'prestamo.entidad_id', $entidad_id], ['IN', 'detalle.id', $prestamo_detalles_id]]);
        }

        // se emiten recibos siempre por cuotas solamente si el iva del interes ya fue pagado al sacar el prestamo.
        // entonces se filtra solamente prestamos con iva_interes_cobrado == si.
        // VAMOS A QUITAR ESTO PORQUE EL 19 FEB 2019 SE DIJO CON JOSE QUE HAY VECES QUE SE DEVENGA UNA CUOTA CON FACTURA
        // Y OTRAS CON RECIBO, SIENDO TODOS DE UN MISMO PRESTAMO
//        $prestamo_asociado->andWhere(['prestamo.iva_interes_cobrado' => 'si']);

        $prestamo_asociado = $prestamo_asociado->groupBy('prestamo.id');
        $results = $prestamo_asociado->asArray()->all();

        return ['results' => array_values($results)];
    }

    public function actionGenerarCuotas()
    {
        $tipo_interes = $_GET['tipo_interes'];
        $vencimiento = $_GET['vencimiento'];
        $primer_vencimiento = $_GET['primer_venc'];

        $cant_meses = $this->calcularCantMeses($vencimiento, $primer_vencimiento);

        // Controlar que la cantidad de cuotas coincida con la cantidad de meses entre vencimiento y el primer vencimiento.
        // 5 cuotas desde febrero, termina en junio. La diferencia de meses son 4. Por eso se suma 1.

        /*if ((int)$cant_cuotas !== $cant_meses) { // descomentar si se llega a necesitar de nuevo.
            Yii::$app->response->format = Response::FORMAT_JSON;
            FlashMessageHelpsers::createWarningMessage("Cantidad de cuotas no coincide con el rango entre 1er. Venc y Vencimiento.");
            return ['result' => "", 'error' => true];
        }*/

//        Yii::

        Yii::$app->response->format = Response::FORMAT_JSON;
        switch ($tipo_interes) {
            case 'us':
                return $this->interesAmericano();
            case 'fr':
                return $this->interesFrances();
            case 'de':
                return $this->interesAleman();
            case 'sm':
                return $this->interesSimple();
        }

        Yii::$app->response->format = Response::FORMAT_RAW;
        return ['result' => "Error Interno: Se alcanzo la linea do codigo que no deberia alcanzar.", 'error' => true];
    }

    private function interesSimple()
    {
        $cant_cuotas = (int)$this->unformattNumber($_GET['cant_cuotas']);
        $primer_vencimiento = $_GET['primer_venc'];
        $interes = $this->unformattNumber($_GET['interes']);
        $capital = $this->unformattNumber($_GET['neto_entregar']);
        $vencimiento = $_GET['vencimiento'];
        $incluye_iva = $_GET['incluye_iva'];
        $iva_10 = (float)$this->unformattNumber($_GET['iva_10']);

        if ($cant_cuotas <= 0) {
            Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
            FlashMessageHelpsers::createWarningMessage("Cantidad de cuotas no puede ser menor a cero.");
            return ['result' => '', 'error' => true];
        }


        if ($incluye_iva == 'si') {
            $interes = round($interes / 1.10);
        }

        // distribuir fechas
        $_primer_venc_time = strtotime($primer_vencimiento);
        $_venc_time = strtotime($vencimiento);

        $_time_diff = $_venc_time - $_primer_venc_time;

        $_venc_fecha_N = $_primer_venc_time;
        $_incremento = $_time_diff / ($cant_cuotas - 1);
        $vencimientos[] = $_venc_fecha_N;

        for ($i = 1; $i < $cant_cuotas; $i++) {
            $_venc_fecha_N += $_incremento;
            $vencimientos[] = $_venc_fecha_N;
        }

        // calcular interes y capital mensual, son constantes.
        $interes_mensual = round((float)$interes / (int)$cant_cuotas);
        $capital_mensual = round((float)$capital / (int)$cant_cuotas);

        // variables acumulativas
        $interes_acu = 0;
        $capital_acu = 0;

        $data = [];
        $data['PrestamoDetalle'] = [
            'nro_cuota' => '',
            'vencimiento' => '',
            'monto_cuota' => '',
            'interes' => '',
            'capital' => '',
        ];

        /** @var PrestamoDetalle[] $prest_detalles */
        $prest_detalles = [];
        $nro_cuota = 1;

        foreach ($vencimientos as $vencimiento) {
            $new_detalle = new PrestamoDetalle();
            $new_detalle->loadDefaultValues();

            $data['PrestamoDetalle']['nro_cuota'] = $nro_cuota++;
            $data['PrestamoDetalle']['vencimiento'] = date('d-m-Y', $vencimiento);
            $data['PrestamoDetalle']['capital'] = 0;//$capital_mensual;
            $data['PrestamoDetalle']['interes'] = 0;//$interes_mensual;
            $data['PrestamoDetalle']['monto_cuota'] = 0;//$data['PrestamoDetalle']['interes'] + $data['PrestamoDetalle']['capital'];

            $new_detalle->load($data);
            $prest_detalles[] = $new_detalle;

            $interes_acu += (int)$new_detalle->interes;
            $capital_acu += (int)$new_detalle->capital;
        }

        // sumar/restar valor residual que falta para igualar a sus sumas correspondientes.
//        $prest_detalles[$cant_cuotas - 1]->capital += ((int)$capital - $capital_acu);
//        $prest_detalles[$cant_cuotas - 1]->interes += ((int)$interes - $interes_acu);
//        $prest_detalles[$cant_cuotas - 1]->monto_cuota = (int)$prest_detalles[$cant_cuotas - 1]->capital + (int)$prest_detalles[$cant_cuotas - 1]->interes;

        Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
            'allModels' => $prest_detalles,
            'pagination' => false,
        ]));

        return ['result' => '', 'error' => ''];
    }

    private
    function interesAmericano()
    {
        $cant_cuotas = (int)$this->unformattNumber($_GET['cant_cuotas']);
        $primer_vencimiento = $_GET['primer_venc'];
        $interes = $this->unformattNumber($_GET['interes']);
        $capital = $this->unformattNumber($_GET['neto_entregar']);
        $vencimiento = $_GET['vencimiento'];
        $incluye_iva = $_GET['incluye_iva'];

        if ($cant_cuotas <= 0) {
            Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
            FlashMessageHelpsers::createWarningMessage("Cantidad de cuotas no puede ser menor a cero.");
            return ['result' => '', 'error' => true];
        }

        if ($incluye_iva == 'si') {
            $interes = round($interes / 1.1);
        }

        // distribuir fechas
        $_primer_venc_time = strtotime($primer_vencimiento);
        $_venc_time = strtotime($vencimiento);

        $_time_diff = $_venc_time - $_primer_venc_time;

        $_venc_fecha_N = $_primer_venc_time;
        $_incremento = $_time_diff / ($cant_cuotas - 1);
        $vencimientos[] = $_venc_fecha_N;

        for ($i = 1; $i < $cant_cuotas; $i++) {
            $_venc_fecha_N += $_incremento;
            $vencimientos[] = $_venc_fecha_N;
        }

        // calcular el interes, es cte.
        $interes_mensual = round((float)$interes / (int)$cant_cuotas);

        // variables acumulativas
        $interes_acu = 0;
        $capital_acu = 0;

        $data = [];
        $data['PrestamoDetalle'] = [
            'nro_cuota' => '',
            'vencimiento' => '',
            'monto_cuota' => '',
            'interes' => '',
            'capital' => '',
        ];

        /** @var PrestamoDetalle[] $prest_detalles */
        $prest_detalles = [];
        $nro_cuota = 1;

        foreach ($vencimientos as $vencimiento) {
            $new_detalle = new PrestamoDetalle();
            $new_detalle->loadDefaultValues();

            $data['PrestamoDetalle']['nro_cuota'] = $nro_cuota;
            $data['PrestamoDetalle']['vencimiento'] = date('d-m-Y', $vencimiento);
//            $data['PrestamoDetalle']['capital'] = $nro_cuota == (int)$cant_cuotas + 1 ? $capital : 0; // en la ultima cuota, se paga to'do el capital
//            $data['PrestamoDetalle']['interes'] = $interes_mensual;
//            $data['PrestamoDetalle']['monto_cuota'] = $data['PrestamoDetalle']['interes'] + $data['PrestamoDetalle']['capital'];

            $new_detalle->load($data);
            $prest_detalles[] = $new_detalle;

            $interes_acu += (int)$new_detalle->interes;
            $capital_acu += (int)$new_detalle->capital;

            $nro_cuota++;
        }

        // sumar/restar valor residual que falta para igualar a sus sumas correspondientes.
//        $prest_detalles[$cant_cuotas - 1]->capital += ((int)$capital - $capital_acu);
//        $prest_detalles[$cant_cuotas - 1]->interes += ((int)$interes - $interes_acu);
//        $prest_detalles[$cant_cuotas - 1]->monto_cuota = (int)$prest_detalles[$cant_cuotas - 1]->capital + (int)$prest_detalles[$cant_cuotas - 1]->interes;

        Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
            'allModels' => $prest_detalles,
            'pagination' => false,
        ]));

        return ['result' => '', 'error' => ''];
    }

    private
    function interesFrances()
    {
        $primer_venc = $_GET['primer_venc'];
        $vencimiento = $_GET['vencimiento'];
        $incluye_iva = $_GET['incluye_iva'];
        $usar_iva_10 = $_GET['usar_iva_10'];
        $tiene_factura = $_GET['tiene_factura'];
        $tasa = (float)$this->unformattNumber($_GET['tasa']);
        $interes = (int)$this->unformattNumber($_GET['interes']);
        $capital = (int)$this->unformattNumber($_GET['capital']);
        $cant_cuotas = (int)$this->unformattNumber($_GET['cant_cuotas']);


        if ($cant_cuotas <= 0) {
            Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
            FlashMessageHelpsers::createWarningMessage("Cantidad de cuotas no puede ser menor a cero.");
            return ['result' => '', 'error' => true];
        }

        if ($incluye_iva == 'si') {
            $iva_interes = round($interes / 11);
            $interes -= $iva_interes;
        } else {
            $iva_interes = round($interes / 10);
        }

//        if (!($usar_iva_10 == 'si' || $usar_iva_10 == 'no' && $tiene_factura == 'por_cuota')) {
//            $iva_interes = 0; // el iva del interes queda dentro del capital.
//        }

//        if ($tiene_factura == 'por_prestamo') {
//            // el capital se aumenta en una cantidad igual al iva del interes desde el form, asi que no hacer nada.
//        } else {
//            // aumentar el interes en una cantidad igual a su iva.
//            $interes += $iva_interes;
//        }

        if ($tasa > 1) {
            $tasa /= 100;
        }
        $tasa /= 12; // convertir en tasa mensual

        // calcular la cuota, es constante.
//        $capital += $gastos_adm + (($iva_interes_cobrado == 'si') ? $iva_interes : 0);
        $factor_exp = pow((1 + $tasa), $cant_cuotas);
        $cuota = round($capital * $tasa * $factor_exp / ($factor_exp - 1));

        // distribuir fechas
        $_primer_venc_time = strtotime($primer_venc);
        $_venc_time = strtotime($vencimiento);

        $_time_diff = $_venc_time - $_primer_venc_time;

        $_venc_fecha_N = $_primer_venc_time;
        $_incremento = $_time_diff / ($cant_cuotas - 1);
        $vencimientos[] = $_venc_fecha_N;

        for ($i = 1; $i < $cant_cuotas; $i++) {
            $_venc_fecha_N += $_incremento;
            $vencimientos[] = $_venc_fecha_N;
        }

        // variables acumulativas
        $interes_acu = 0;
        $capital_acu = 0;
        $monto_c_acu = 0;
        $iva_acu = 0;

        /** @var PrestamoDetalle[] $prest_detalles */
        $prest_detalles = [];

        $data = [];
        $data['PrestamoDetalle'] = [
            'nro_cuota' => '',
            'vencimiento' => '',
            'monto_cuota' => '',
            'interes' => '',
            'capital' => '',
        ];

        $capital_original = $capital;
        $nro_cuota = 1;

        foreach ($vencimientos as $vencimiento) {
            $new_detalle = new PrestamoDetalle();
            $new_detalle->loadDefaultValues();

            $interes_por_periodo = ($capital * $tasa);
            $amortizacion_por_periodo = ($cuota - $interes_por_periodo);

            $data['PrestamoDetalle']['nro_cuota'] = $nro_cuota;
            $data['PrestamoDetalle']['vencimiento'] = date('d-m-Y', $vencimiento);
//            $data['PrestamoDetalle']['capital'] = round($amortizacion_por_periodo);
//            $data['PrestamoDetalle']['interes'] = round($interes_por_periodo);
//            $data['PrestamoDetalle']['impuesto'] = ($usar_iva_10 == 'si' || $usar_iva_10 == 'no' && $tiene_factura == 'por_cuota') ? round($interes_por_periodo / 10) : 0;
//            $data['PrestamoDetalle']['monto_cuota'] = $cuota;

            Yii::warning("CAPITAL:{$capital}, CUOTA:{$cuota}, INTERES:{$interes_por_periodo}, AMORTIZACION:{$amortizacion_por_periodo}");

            $new_detalle->load($data);
            $prest_detalles[] = $new_detalle;

            $interes_acu += (int)$new_detalle->interes;
            $capital_acu += (int)$new_detalle->capital;
            $monto_c_acu += (int)$new_detalle->monto_cuota;
            $iva_acu += (int)$new_detalle->impuesto;

            $nro_cuota++;
            $capital -= $amortizacion_por_periodo;
        }

        // sumar/restar valor residual que falta para igualar a sus sumas correspondientes.
        $cuota_total = $capital_original + $interes + $iva_interes;
        Yii::warning("oe---- interes: {$interes}, capital: {$capital_original}, cuota: {$cuota_total}");
        Yii::warning("ac---- interes: {$interes_acu}, capital: {$capital_acu}, cuota: {$monto_c_acu}");
        $difin = $interes - $interes_acu;
        $dicap = $capital_original - $capital_acu;
        $dimc = $cuota_total - $monto_c_acu;
        $diiva = $iva_interes - $iva_acu;
        Yii::warning("df---- interes: {$difin}, capital: {$dicap}, cuota: {$dimc}, impuesto: {$diiva}");

//        $prest_detalles[$cant_cuotas - 1]->capital += ((int)$capital_original - $capital_acu);
//        $prest_detalles[$cant_cuotas - 1]->interes += ((int)$interes - $interes_acu);
//        $prest_detalles[$cant_cuotas - 1]->impuesto += ($usar_iva_10 == 'si' || $usar_iva_10 == 'no' && $tiene_factura == 'por_cuota') ? ((int)$iva_interes - $iva_acu) : 0;
//        $prest_detalles[$cant_cuotas - 1]->monto_cuota += $cuota_total - $monto_c_acu;

        Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
            'allModels' => $prest_detalles,
            'pagination' => false,
        ]));

        return ['result' => '', 'error' => ''];
    }

    private
    function interesAleman()
    {
        $cant_cuotas = (int)$this->unformattNumber($_GET['cant_cuotas']);
        $primer_vencimiento = $_GET['primer_venc'];
        $interes = (int)$this->unformattNumber($_GET['interes']);
        $neto = (int)$this->unformattNumber($_GET['neto_entregar']);
        $capital = (int)$this->unformattNumber($_GET['capital']);
        $vencimiento = $_GET['vencimiento'];
        $incluye_iva = $_GET['incluye_iva'];
        $tasa = (float)$this->unformattNumber($_GET['tasa']);

        if ($cant_cuotas <= 0) {
            Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
            FlashMessageHelpsers::createWarningMessage("Cantidad de cuotas no puede ser menor a cero.");
            return ['result' => '', 'error' => true];
        }

        if ($incluye_iva == 'no') {
            $interes = round($interes * 110 / 100);
        }

        if ($tasa > 1) {
            $tasa /= 100;
        }
        $tasa /= 12; // convertir en tasa mensual

        // distribuir fechas
        $_primer_venc_time = strtotime($primer_vencimiento);
        $_venc_time = strtotime($vencimiento);

        $_time_diff = $_venc_time - $_primer_venc_time;

        $_venc_fecha_N = $_primer_venc_time;
        $_incremento = $_time_diff / ($cant_cuotas - 1);
        $vencimientos[] = $_venc_fecha_N;

        for ($i = 1; $i < $cant_cuotas; $i++) {
            $_venc_fecha_N += $_incremento;
            $vencimientos[] = $_venc_fecha_N;
        }

        // variables acumulativas
        $interes_acu = 0;
        $capital_acu = 0;
        $monto_c_acu = 0;

        $data = [];
        $data['PrestamoDetalle'] = [
            'nro_cuota' => '',
            'vencimiento' => '',
            'monto_cuota' => '',
            'interes' => '',
            'capital' => '',
        ];

        /** @var PrestamoDetalle[] $prest_detalles */
        $prest_detalles = [];
        $nro_cuota = 1;

        $capital_original = $capital;
        $amortizacion_cte = round($capital_original / $cant_cuotas);
        foreach ($vencimientos as $vencimiento) {
            $new_detalle = new PrestamoDetalle();
            $new_detalle->loadDefaultValues();

            $interes_por_periodo = round($capital * $tasa);

            $data['PrestamoDetalle']['nro_cuota'] = $nro_cuota;
            $data['PrestamoDetalle']['vencimiento'] = date('d-m-Y', $vencimiento);
            $data['PrestamoDetalle']['capital'] = $amortizacion_cte;
            $data['PrestamoDetalle']['interes'] = $interes_por_periodo;
            $data['PrestamoDetalle']['monto_cuota'] = $amortizacion_cte + $interes_por_periodo;

            $new_detalle->load($data);
            $prest_detalles[] = $new_detalle;

            $interes_acu += (int)$new_detalle->interes;
            $capital_acu += (int)$new_detalle->capital;
            $monto_c_acu += (int)$new_detalle->monto_cuota;

            $nro_cuota++;
            $capital -= $amortizacion_cte;
        }

        $prest_detalles[$cant_cuotas - 1]->capital += ((int)$capital_original - $capital_acu);
        $prest_detalles[$cant_cuotas - 1]->interes += ((int)$interes - $interes_acu);
        $prest_detalles[$cant_cuotas - 1]->monto_cuota = (int)$prest_detalles[$cant_cuotas - 1]->capital + (int)$prest_detalles[$cant_cuotas - 1]->interes;

        Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
            'allModels' => $prest_detalles,
            'pagination' => false,
        ]));

        return ['result' => '', 'error' => ''];
    }

    private
    function unformattNumber($string)
    {
        return str_replace(',', '.', str_replace('.', '', $string));
    }

    private
    function calcularCantMeses($fecha_mayor, $fecha_menor)
    {
        $date1 = DateTime::createFromFormat('d-m-Y', $fecha_mayor);
        $date2 = DateTime::createFromFormat('d-m-Y', $fecha_menor);

        $diff = $date1->diff($date2);

        return ((int)($diff->format('%y') * 12) + (int)$diff->format('%m')) + 1;
    }

    public function actionRemoverAsientoSimulado()
    {
        Yii::$app->session->set('cont_asientodetalle_prestamo', []);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => '', 'error' => ''];
    }

    public function actionSimularAsiento()
    {
        $monto_operacion = $this->unformattNumber($_GET['monto_operacion']);
        $interes = $this->unformattNumber($_GET['interes']);
        $gastos_bancarios = $this->unformattNumber($_GET['gastos_bancarios']);
        $neto_entregar = $this->unformattNumber($_GET['neto_entregar']);
        $iva_10 = $this->unformattNumber($_GET['iva_10']);
        $usar_iv_10 = $_GET['usar_iva_10'];
        $incluye_iva = $_GET['incluye_iva'];
        $cuentas = $_GET['cuentas'];
        $tiene_factura = $_GET['tiene_factura'];

        $data = [];
        $data['Prestamo'] = [
            'monto_operacion' => $monto_operacion,
            'intereses_vencer' => $interes,
            'gastos_bancarios' => round($gastos_bancarios / 1.1), // gastos bancarios siempre con iva.
            'caja' => $neto_entregar,
            'iva_10' => $iva_10,
            'incluye_iva' => $incluye_iva,
            'usar_iva_10' => $usar_iv_10,
            'tiene_factura' => $tiene_factura
        ];

        $prestamo = new Prestamo();
        $prestamo->load($data);

        $asiento_detalles = [];
        $debe = 0.0;
        $haber = 0.0;
        $iva_interes = ($prestamo->incluye_iva == 'si') ? ((float)$prestamo->intereses_vencer / 11) : ((float)$prestamo->intereses_vencer / 10);
        $iva_interes = round($iva_interes);
        $interes_sin_iva = ($prestamo->incluye_iva == 'si') ? ((float)$prestamo->intereses_vencer - $iva_interes) : $prestamo->intereses_vencer;
        $iva_gasto = round($prestamo->gastos_bancarios / 10);

        foreach ($cuentas as $cuenta) {

            $concepto = str_replace('cuenta_', '', $cuenta['atributo']);
            $asientod = new AsientoDetalle();
            $asientod->cuenta_id = $cuenta['valor'];

            if ($concepto == 'intereses_pagados') continue;

            if ($concepto == 'gastos_bancarios') {
                $asientod->monto_debe = $prestamo->$concepto;
                $asientod->monto_haber = 0.0;
            } elseif ($concepto == 'iva_10' && $prestamo->isCase1()) {
                $asientod->monto_debe = $prestamo->$concepto;
                $asientod->monto_haber = 0.0;
            } elseif ($concepto == 'gastos_no_deducibles' && $prestamo->isCase2()) {
                $asientod->monto_debe = $prestamo->iva_10;
                $asientod->monto_haber = 0.0;
            } elseif (in_array($concepto, ['intereses_vencer'])) {
                $asientod->monto_debe = ($prestamo->incluye_iva == 'si') ? round($prestamo->$concepto / 1.1) : $prestamo->$concepto;
                $asientod->monto_haber = 0.0;
            } elseif ($concepto == 'caja') {
                $asientod->monto_debe = $prestamo->$concepto;
                $asientod->monto_haber = 0.0;
            } elseif ($concepto == 'monto_operacion') {
                $asientod->monto_debe = 0.0;
                $asientod->monto_haber = $prestamo->$concepto - $interes_sin_iva;

                if ($usar_iv_10 == 'no') {
                    $asientod->monto_haber -= ($iva_interes + $iva_gasto);
                }
            } elseif ($concepto == 'intereses_a_pagar') {
                $asientod->monto_debe = 0.0;
                $asientod->monto_haber = $interes_sin_iva;
            }

            $debe += round($asientod->monto_debe);
            $haber += round($asientod->monto_haber);

            Yii::warning('-- concepto debe y haber: ' . $concepto . ' ' . $asientod->monto_debe . ' ' . $asientod->monto_haber);
            if (($asientod->monto_haber + $asientod->monto_debe) != 0) {
                $asiento_detalles[] = $asientod;
                Yii::warning('---concepto: ' . $concepto);
            }
        }

        if (sizeof($asiento_detalles) > 0) {

            $this->agruparPorPartida($asiento_detalles);

            $skey = "cont_asientodetalle_prestamo";
            Yii::$app->session->set($skey, $asiento_detalles);
            Yii::$app->session->set("cont_asientodetalle_prestamo_debe", $debe);
            Yii::$app->session->set("cont_asientodetalle_prestamo_haber", $haber);

        } else {

            $skey = "cont_asientodetalle_prestamo";
            Yii::$app->session->remove($skey);
            Yii::$app->session->remove("cont_asientodetalle_prestamo_debe");
            Yii::$app->session->remove("cont_asientodetalle_prestamo_haber");
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => '', 'error' => ''];
    }

    /**
     * @param $asiento_detalles AsientoDetalle[]
     */
    private
    function agruparPorPartida(&$asiento_detalles)
    {
        $debes = [];
        $haberes = [];

        foreach ($asiento_detalles as $asiento_detalle) {
            if ($asiento_detalle->monto_debe > 0) $debes[] = $asiento_detalle;
            else $haberes[] = $asiento_detalle;
        }

        $asiento_detalles = array_values(array_merge($debes, $haberes));

    }

    public function actionPrestamoSinFactExists($compra_id = null, $ruc = null, $deducible = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $entidad = Entidad::find()->where(['ruc' => $ruc]);
        $entidad_id = $entidad->exists() ? $entidad->one()->id : null;

        $prestamos = Prestamo::find()
            ->where(['IS', 'factura_compra_id', null])
            ->andWhere(['entidad_id' => $entidad_id])
            ->andFilterWhere(['usar_iva_10' => $deducible])
            ->orFilterWhere(['factura_compra_id' => $compra_id]);
        $prestamos = $prestamos->select(['id', "CONCAT(('Préstamo #'), nro_prestamo) as text"])->orderBy('id');

        return $prestamos->exists();
    }

    public function actionSelectPrestamoParaFacturar($submit = false, $compra_id = null, $ruc = null, $deducible = null, $fila_plantilla = null)
    {
        $array_left = [];
        $array_right = [];

        $session = Yii::$app->session;
        $compra_actual_skey = 'cont_compra_actual_id';
        $compra_plantilla_skey = 'cont_compra_fila_plantilla';
        $compra_actual_ruc_skey = 'cont_compra_actual_entidad_ruc';
        $deducibilidad_prest_skey = 'cont_prestamo_deducible';

        if (isset($compra_id)) {
            $session->set($compra_actual_skey, $compra_id);
        }

        if (isset($fila_plantilla)) {
            $session->set($compra_plantilla_skey, $fila_plantilla);
        }

        if (isset($ruc)) {
            $session->set($compra_actual_ruc_skey, $ruc);
        }

        if (isset($deducible)) {
            $session->set($deducibilidad_prest_skey, $deducible);
        }

        //$array_left[] = ['id' => $item->id, 'text' => $item->id . ' - ' . $item->nombre];

        $compra_id = $session->get($compra_actual_skey);
        $ruc = $session->get($compra_actual_ruc_skey);
        $deducible = $session->get($deducibilidad_prest_skey);
        $all_prestamos_ids = $this->actionGetPrestamosSinFacturaAjax(null, null, $compra_id, $ruc, $deducible, false)['results'];

        foreach ($all_prestamos_ids as $_item) {
            $flag = false;
            $_id = $_item['id'];
            if ($session->has('cont_prestamo_facturar')) {
                $p_ids_ses = $session->get('cont_prestamo_facturar');
                foreach ($p_ids_ses as $_id_ses) {
                    if ($_id_ses == $_id) {
                        $flag = true;
                        break;
                    }
                }
                if (!$flag)
                    $array_left[] = ['id' => $_id, 'text' => "Préstamo #{$_item['nro_prestamo']}"];
                else
                    $array_right[] = ['id' => $_id, 'text' => "Préstamo #{$_item['nro_prestamo']}"];
            } else {
                $array_left[] = ['id' => $_id, 'text' => "Préstamo #{$_item['nro_prestamo']}"];
            }
        }


        $json_pick_left = json_encode($array_left);
        $json_pick_right = json_encode($array_right);

        $total_deducible = 0.0;
        $total_gnd = 0.0;

        $fila_plantilla = $session->get($compra_plantilla_skey);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isPost && $submit != false) {
            $prestamos_ids = [];
            if (array_key_exists('selected', $_POST))
                $prestamos_ids = $_POST['selected'];
            if (sizeof($prestamos_ids) > 0) {
                Yii::$app->session->set('cont_prestamo_facturar', $prestamos_ids);
                foreach ($prestamos_ids as $prestamo_id) {
                    $prestamo = Prestamo::findOne($prestamo_id);
                    $total = (float)$prestamo->gastos_bancarios;

                    if ($prestamo->usar_iva_10 == 'si') {
                        $interes = $prestamo->intereses_vencer;

                        if ($prestamo->incluye_iva == 'si') {
                            $total += (float)$interes;
                        } else {
                            $iva = $interes / 10;
                            $total += round($iva + (float)$interes);
                        }
                    }

                    $total_deducible += $prestamo->usar_iva_10 == 'si' ? round($total) : 0.0;
                    $total_gnd += $prestamo->usar_iva_10 == 'no' ? round($total) : 0.0;
                }

                $session->remove('cont_compra_actual_id');
                $session->remove('cont_compra_fila_plantilla');
                $session->remove('cont_compra_actual_entidad_ruc');
                $session->remove('cont_prestamo_deducible');
            } else {
                Yii::$app->session->remove('cont_prestamo_facturar');

                $session->remove('cont_compra_actual_id');
                $session->remove('cont_compra_fila_plantilla');
                $session->remove('cont_compra_actual_entidad_ruc');
                $session->remove('cont_prestamo_deducible');
            }
        }

        $data = [
            'exenta' => $total_gnd ? $total_gnd : "",
            'gravada_10' => $total_deducible ? $total_deducible : ""
        ];

        Yii::$app->session->set('cont_values_for_plantilla_fields', $data);

        return $this->renderAjax('multiselector/_modal_multiselector', [
            'json_pick_left' => $json_pick_left,
            'json_pick_right' => $json_pick_right,
            'fila_plantilla' => $fila_plantilla,
        ]);
    }

    public function actionGetPrestamoValueForPlantilla()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->session->get('cont_values_for_plantilla_fields');
    }

    public function actionGetPrestamosSinFacturaAjax($q = null, $id = null, $compra_id = null, $ruc = null, $deducible = null, $as_json = true)
    {
        if ($as_json) Yii::$app->response->format = Response::FORMAT_JSON;

        $entidad = Entidad::findOne(['ruc' => $ruc]);

        $prestamos = Prestamo::find()
            ->where(['IS', 'factura_compra_id', null])
//            ->andFilterWhere(['usar_iva_10' => $deducible])
            ->andFilterWhere(['=', 'usar_iva_10', 'si'])// tiene_factura puede ser por_prestamo o no, lo que cambia es la cuenta para el impuesto.
            ->andWhere(['entidad_id' => isset($entidad) ? $entidad->id : null])
            ->orFilterWhere(['factura_compra_id' => $compra_id]);
        $prestamos = $prestamos->select(['id', "CONCAT(('Préstamo #'), nro_prestamo) as text", 'nro_prestamo'])->orderBy('id');

        return ['results' => array_values($prestamos->asArray()->all())];
    }

    public function actionBloquear($id)
    {
        $model = Prestamo::findOne(['id' => $id]);

        if ($model->bloqueado == 'no') {
            $model->bloqueado = 'si';

            //Solo para validacion
            $model->ruc = $model->entidad->ruc;

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Prestamo ha sido bloqueado.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede bloquear prestamo. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionDesbloquear($id)
    {
        $model = Prestamo::findOne(['id' => $id]);

        if ($model->bloqueado == 'si') {
            $model->bloqueado = 'no';

            //Solo para validacion
            $model->ruc = $model->entidad->ruc;

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Prestamo ha sido desbloqueado.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede desbloquear prestamo. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionVerLog()
    {
        if (!Yii::$app->request->isAjax && Yii::$app->request->isGet) {
            Yii::$app->session->remove('log-file');
        }
        return $this->render('ver-log', []);
    }

    public function actionVerLogSelectFile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $file = $_GET['log_file_selector'];
        Yii::$app->session->set('log-file', $file);
        return [$file, filesize("/var/log/jmg-contabilidad-log/$file")];
    }

    public function actionVerLogGetFiles()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $files = [];
        $dir = "/var/log/jmg-contabilidad-log/";
        if ($handle = opendir($dir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $files[$entry] = $entry;
                }
            }
            closedir($handle);
        }
        $results['results'][] = $files;
        return $results;
    }
}