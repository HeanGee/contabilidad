<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\administracion\controllers\Cuota;
use backend\modules\contabilidad\models\CuotaVenta;
use backend\modules\contabilidad\models\search\CuotaVentaSearch;
use kartik\form\ActiveForm;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * CuotaVentaController implements the CRUD actions for CuotaVenta model.
 */
class CuotaVentaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CuotaVenta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CuotaVentaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CuotaVenta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CuotaVenta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CuotaVenta();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CuotaVenta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CuotaVenta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CuotaVenta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CuotaVenta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CuotaVenta::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /*************** Utilizado externamente ***************/

    public function actionAddCuotas($submit = false)
    {
        $model = new CuotasVenta();
        $session = Yii::$app->session;


        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
//            exit();
            $cuotas = [];
            for($i=1; $i<=$model->nro_cuotas; $i++) {
                $cuota = new CuotaVenta();
                $cuota->nro_cuota = $i;
                $cuota->fecha_vencimiento = date('Y-m-d', strtotime('+'.($i-1).' month', strtotime($model->fecha)));
                $cuota->monto = $this->formatPostToFloat($model->monto);
                array_push($cuotas, $cuota);
            }

            $dataProvider = new ArrayDataProvider([
                'allModels' => $cuotas,
                'pagination' => false,
            ]);

            $session->set('cont_cuotas_venta-provider', $dataProvider);
        };

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('_modal_add_cuotas', [
            'model' => $model,
        ]);
    }

    /**
     * @param $indice
     * @param string $cuenta
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBorrarCuota($indice)
    {
//        $session = Yii::$app->session;
//
//        $dataProvider = \Yii::$app->session->get('cont_detalleventa-provider');
//        $data = $dataProvider->allModels;
//
//        if($data[$indice]->id != null)
//            DetalleVenta::findOne(['id'=>$data[$indice]->id])->delete();
//
//        unset($data[$indice]);
//        $data = array_values($data);
//
//        $dataProvider = new ArrayDataProvider([
//            'allModels' => $data,
//            'pagination' => [
//                'pageSize' => 5,
//            ],
//        ]);
//
//        $session->set('cont_detalleventa-provider', $dataProvider);
//
//        return true;
    }

    public function actionUpdateCuota($indice, $submit = false)
    {
        $models = Yii::$app->session->get('cont_cuotas_venta-provider')->allModels;
        $model = $models[$indice];
//        $model->monto = $this->formatFloatToGet($model->monto);

//        /** @var ComprobanteDetalle $model */
        if ($model->load(Yii::$app->request->post())) {
            $model->monto = $this->formatPostToFloat($model->monto);
            //Se agrega el objeto a la lista
            $models[$indice] = $model;
            $provider = new ArrayDataProvider([
                'allModels' => $models,
                'pagination' => false
            ]);
            Yii::$app->session->set('adm_comprobante_cuota_provider', $provider);
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        return $this->renderAjax('_modal_update_cuota', [
            'model' => $model,
        ]);
    }

    /**
     * @param $number
     * @return mixed
     */
    private function formatPostToFloat($number)
    {
        //Se convierte a una manera que maneje float
        if (strpos($number, '.') && strpos($number, ',')) {
            $number = str_replace(',', '.', str_replace('.', '', $number));
        } elseif (strpos($number, '.') && !strpos($number, ',')) {
            $number = str_replace('.', '', $number);
        } elseif (!strpos($number, '.') && strpos($number, ',')) {
            $number = str_replace(',', '.', $number);
        }
        return $number;
    }

    /**
     * Convierte un float obtenido de la base de datos en formato 1000000.5 a float 1000.000,5
     * @param $number
     * @return mixed
     */
    private function formatFloatToGet($number)
    {
        //Se convierte a una manera que maneje float
        if (strpos($number, '.') && !strpos($number, ',')) {
            $number = number_format($number, 2, ',', '.');
        }
        return $number;
    }
}

/**
 * @property string $nro_cuotas
 * @property string $fecha
 * @property string $monto
 */
class CuotasVenta extends Model
{
    public $nro_cuotas;
    public $fecha;
    public $monto;

    public function rules()
    {
        return [
            [['nro_cuotas', 'fecha', 'monto'], 'required'],
//            [['nro_cuotas', 'fecha', 'monto'], 'safe'],
        ];
    }
}
