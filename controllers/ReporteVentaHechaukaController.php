<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\Factura;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\FlashMessageHelpsers;
use common\models\ParametroSistema;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\Response;
use yii2tech\csvgrid\CsvGrid;

/**
 * controller para generar reporte de ventas en formato consumible por heckauka
 */
class ReporteVentaHechaukaController extends BaseController
{
    private $monto_total_rango = 0;
    private $timbrado = [];
    private $gra10 = [];
    private $gra5 = [];
    private $iva10 = [];
    private $iva5 = [];
    private $exent = [];


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    private function conversionAMonedaLocal($model, $campo)
    {
        $cotizacion = 1;
        Yii::info($model->moneda_id, "### id de moneda");
        if ($model->moneda_id != null && $model->moneda_id != ParametroSistema::getMonedaBaseId()) {
            $cotizacion = $model->cotizacion;
        }
        return round($campo * (float)$cotizacion);
    }

    /**
     * @param null $_pjax
     * @return $this|string
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function actionGetParams($_pjax = null)
    {
        $model = new ReporteVentaHechauka();

        if (!isset($_pjax) && Yii::$app->request->isGet) {
            $gridViewQuery = Venta::find()->where(['DATE_FORMAT(cont_factura_venta.fecha_emision, "%m/%Y")' => '']);
            $dataProvider = new ActiveDataProvider(['query' => $gridViewQuery, 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]]);
            $dataProvider->pagination = ['pageSize' => 20];
            Yii::$app->session->set('dataProvider', $dataProvider);
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $this->monto_total_rango;

            if ($model->periodo == '') {
                FlashMessageHelpsers::createWarningMessage('Indique el período.');
                goto retorno;
            }

            $periodo = implode('-', array_reverse(explode('-', "01-" . str_replace("/", "-", $model->periodo))));
            $desde = date('Y-m-d', strtotime($periodo));
            $hasta = date('Y-m-t', strtotime($periodo));
            if ($desde && $hasta) {

                $tipoDocSetNotaCredito = Venta::getTipodocSetNotaCredito();
                $query = new Query();
                $query->select(['venta.id as id', 'venta.condicion', 'venta.total',
                    'entidad.ruc AS ruc', "CONCAT(venta.prefijo, '-', venta.nro_factura) as nro_factura", 'entidad.digito_verificador as dv',
                    'entidad.razon_social', 'DATE_FORMAT(venta.fecha_emision, "%d/%m/%Y") as fecha',
                    'venta.cant_cuotas', 'venta.moneda_id', 'venta.valor_moneda as cotizacion',
                    'venta.fecha_emision', 'venta.timbrado_detalle_id', 'venta.entidad_id',
                    'tim.nro_timbrado as nro_timbrado', "('venta') as operacion"])
                    ->from('cont_factura_venta as venta');
                $query->leftJoin('cont_entidad as entidad', 'entidad.id = venta.entidad_id');
                $query->leftJoin('cont_timbrado_detalle as timdet', 'timdet.id = venta.timbrado_detalle_id');
                $query->leftJoin('cont_timbrado as tim', 'tim.id = timdet.timbrado_id');
                $query->leftJoin('cont_tipo_documento as tipodoc', 'tipodoc.id = venta.tipo_documento_id');
                $query->leftJoin('cont_tipo_documento_set as tipodocset', 'tipodocset.id = tipodoc.tipo_documento_set_id');
                $query->where(['venta.empresa_id' => Yii::$app->session->get('core_empresa_actual')]);
                $query->andWhere(['venta.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]);
                $query->andWhere(['venta.estado' => 'vigente']);
                $query->andWhere(['IS NOT', 'timbrado_detalle_id', null]);
                $query->andFilterWhere(['!=', 'tipodoc.tipo_documento_set_id', $tipoDocSetNotaCredito->id]);
                $query->andFilterWhere(['between', 'venta.fecha_emision', $desde, $hasta]);

                # 2019-05-29 -> Silvia solicita: Sumarizar facturas que son para cliente con ruc 44444401
                $rucGenericos = ParametroSistema::findOne(['nombre' => 'contabilidad-ruc-generico-libro-venta']);
                if (isset($rucGenericos)) {
                    $findemes = $hasta;
                    $findemes2 = date('t/m/Y', strtotime($periodo));
                    $rucGenericos = explode(",", $rucGenericos->valor);
                    array_walk($rucGenericos, function (&$element, $index) {
                        $element = explode('-', $element);
                    });

                    foreach ($rucGenericos as $rucGenerico) {
                        $entidadGenerica = Entidad::findOne(['ruc' => $rucGenerico[0]]);

                        if (isset($entidadGenerica)) {
                            # Del query principal, excluir ventas del cliente ocasional
                            $query->andFilterWhere(['!=', 'venta.entidad_id', $entidadGenerica->id]);
                            $monedaBase = ParametroSistema::getMonedaBaseId();

                            # Al agrupar las facturas, la fecha debe ser el ultimo dia del mes de reporte establecido en el formulario, segun silvia.
                            # Correo 29-05-2019 11:57 asunto: `RE: Reporte Hechauka Ventas`
                            $queryForEntidadOcasional = new Query();
                            $queryForEntidadOcasional = $queryForEntidadOcasional
                                ->select(['GROUP_CONCAT(venta.id) as id', 'MIN(venta.condicion) as condicion', "SUM(venta.total * IF(venta.moneda_id = $monedaBase, 1, venta.valor_moneda)) as total",
                                    'MIN(entidad.ruc) AS ruc', "('0') as nro_factura", "('{$rucGenerico[1]}') as dv",
                                    'MIN(entidad.razon_social) as razon_social', "('{$findemes2}') as fecha",
                                    'MIN(venta.cant_cuotas) as cant_cuotas', "('1') as moneda_id", "('1') as cotizacion",
                                    "('{$findemes}') as fecha_emision", 'MIN(venta.timbrado_detalle_id) as timbrado_detalle_id', 'MIN(venta.entidad_id) as entidad_id',
                                    "('0') as nro_timbrado", "('venta') as operacion"])
                                ->from('cont_factura_venta as venta')
                                ->leftJoin('cont_entidad as entidad', 'entidad.id = venta.entidad_id')
                                ->leftJoin('cont_timbrado_detalle as timdet', 'timdet.id = venta.timbrado_detalle_id')
                                ->leftJoin('cont_timbrado as tim', 'tim.id = timdet.timbrado_id')
                                ->leftJoin('cont_tipo_documento as tipodoc', 'tipodoc.id = venta.tipo_documento_id')
                                ->leftJoin('cont_tipo_documento_set as tipodocset', 'tipodocset.id = tipodoc.tipo_documento_set_id')
                                ->where(['venta.empresa_id' => Yii::$app->session->get('core_empresa_actual')])
                                ->andWhere(['venta.periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
                                ->andWhere(['venta.estado' => 'vigente'])
                                ->andWhere(['IS NOT', 'timbrado_detalle_id', null])
                                ->andFilterWhere(['!=', 'tipodoc.tipo_documento_set_id', $tipoDocSetNotaCredito->id])
                                ->andFilterWhere(['between', 'venta.fecha_emision', $desde, $hasta])
                                ->andFilterWhere(['=', 'venta.entidad_id', $entidadGenerica->id])
                                ->groupBy(['venta.entidad_id']);

                            $query->union($queryForEntidadOcasional->createCommand()->rawSql);
                        }
                    }
                }

                $empresa_id = Yii::$app->session->get('core_empresa_actual');
                $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
                $tipoDocSetNotaCredito = Compra::getTipodocSetNotaCredito();
                $sql = "
                SELECT compra.id as id, compra.condicion as condicion, compra.total as total, entidad.ruc as ruc,
                compra.nro_factura AS nro_factura, entidad.digito_verificador as dv,
                entidad.razon_social as razon_social, DATE_FORMAT(compra.fecha_emision, \"%d/%m/%Y\") as fecha,
                compra.cant_cuotas as cant_cuotas, compra.moneda_id as moneda_id, compra.cotizacion as cotizacion,
                compra.fecha_emision as fecha_emision, compra.timbrado_detalle_id as timbrado_detalle_id,
                compra.entidad_id as entidad_id, tim.nro_timbrado as nro_timbrado,
                ('compra') as operacion
                from cont_factura_compra as compra
                left join cont_timbrado_detalle as timdet on timdet.id = compra.timbrado_detalle_id
                left join cont_timbrado as tim on tim.id = timdet.timbrado_id
                left join cont_entidad as entidad on compra.entidad_id = entidad.id
                left join cont_tipo_documento ctd on compra.tipo_documento_id = ctd.id
                left join cont_tipo_documento_set ctds on ctd.tipo_documento_set_id = ctds.id
                where compra.empresa_id = {$empresa_id} and compra.periodo_contable_id = {$periodo_id}
                and compra.fecha_emision between '{$desde}' and '{$hasta}'
                and (compra.factura_compra_id is not null or ctd.tipo_documento_set_id = {$tipoDocSetNotaCredito->id})
                ";
                $query->union($sql);

            } else {
                FlashMessageHelpsers::createWarningMessage('Rango de fecha incorrecto.');
                goto retorno;
            }

            $count = count($query->all());
            if ($count == 0) {
                FlashMessageHelpsers::createWarningMessage('Sin datos en el periodo.');
                goto retorno;
            }

            $allModels = [];
            $command = $query->createCommand();
            foreach ($command->queryAll() as $fila) {
                $_model = new Factura();
                foreach ($fila as $attribute => $value) {
                    if (array_key_exists($attribute, $_model->attributes)) {
                        $_model->$attribute = $value;
                    }
                }
                $allModels[] = $_model;

                $gravs10 = 0.0;
                $gravs5 = 0.0;
                $imps10 = 0.0;
                $imps5 = 0.0;
                $excs = 0.0;
                $id = array_values(explode(',', $_model->id));
                if (sizeof($id) == 1)
                    $id = $id[0];

                if ($_model->operacion == 'compra') {
                    $query = CompraIvaCuentaUsada::find()->where(['IN', 'factura_compra_id', $id])->all();
                } else {
                    $query = VentaIvaCuentaUsada::find()->where(['IN', 'factura_venta_id', $id])->all();
                }
                /** @var VentaIvaCuentaUsada|CompraIvaCuentaUsada $q */
                foreach ($query as $q) {
                    if ($q->ivaCta != null) {
                        if ($q->ivaCta->iva->porcentaje == 10) {
                            $gravs10 += (float)$q->monto / (1.10);
                            $imps10 += (float)$q->monto - ((float)$q->monto / (1.10));
                        } elseif ($q->ivaCta->iva->porcentaje == 5) {
                            $gravs5 += (float)$q->monto / (1.05);
                            $imps5 += (float)$q->monto - ((float)$q->monto / (1.05));
                        }
                    } else {
                        $excs += (float)$q->monto;
                    }
                }
                $gravs10 = $this->conversionAMonedaLocal($_model, $gravs10);
                $this->gra10[$_model->id] = $gravs10;
                $imps10 = $this->conversionAMonedaLocal($_model, $imps10);
                $this->iva10[$_model->id] = $imps10;
                $gravs5 = $this->conversionAMonedaLocal($_model, $gravs5);
                $this->gra5[$_model->id] = $gravs5;
                $imps5 = $this->conversionAMonedaLocal($_model, $imps5);
                $this->iva5[$_model->id] = $imps5;
                $excs = $this->conversionAMonedaLocal($_model, $excs);
                $this->exent[$_model->id] = $excs;

                $this->monto_total_rango += (float)($gravs5 + $gravs10 + $imps5 + $imps10 + $excs);

//                Yii::warning("Sumando: " . ($gravs5 + $gravs10 + $imps5 + $imps10 + $excs));
            }

            $exporter = new CsvGrid([
                'csvFileConfig' => [
                    'cellDelimiter' => ";",
                    'rowDelimiter' => "\n",
                    'enclosure' => '',
                ],
                'showHeader' => false,
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => $allModels,
                    'pagination' => false,
                ]),
                'columns' => [
                    [
                        'label' => 'TipoRegistro',
                        'value' => function () {
                            return 2;
                        },
                    ],
                    "ruc",
                    "dv",
                    "razon_social",
                    [
                        'label' => 'tipo_documento',
                        'value' => function ($model) {
                            if ($model->operacion == 'compra') return 3;
                            return 1;
                        },
                    ],
                    'nro_factura',
                    "fecha",
                    [
                        'label' => 'Gravada 10%',
                        'value' => function ($model) {
                            return $this->gra10[$model->id];
                        },
                    ],
                    [
                        'label' => 'IVA 10%',
                        'value' => function ($model) {
                            return $this->iva10[$model->id];
                        },
                    ],
                    [
                        'label' => 'Gravada 5%',
                        'value' => function ($model) {
                            return $this->gra5[$model->id];
                        },
                    ],
                    [
                        'label' => 'IVA 5%',
                        'value' => function ($model) {
                            return $this->iva5[$model->id];
                        },
                    ],
                    [
                        'label' => 'EXENTA',
                        'value' => function ($model) {
                            return $this->exent[$model->id];
                        },
                    ],
                    [
                        'label' => 'total',
                        'value' => function ($model) {
                            $total = $this->conversionAMonedaLocal($model, $model->total);
                            return $total;
                        },
                    ],
                    [
                        'label' => 'Condición',
                        'value' => function ($model) {
                            return $condicion = $model->condicion == 'contado' ? 1 : 2;
                        },
                    ],
                    [
                        'label' => 'Cuotas',
                        'value' => function ($model) {
                            return $cuotas = $model->condicion == 'credito' ? $model->cant_cuotas : 0;
                        },
                    ],
                    'nro_timbrado',
                ]

            ]);

            $carpeta = Yii::$app->basePath . '/web/uploads/';
            if (!is_dir($carpeta)) {
                if (!@mkdir($carpeta, 0777, true)) {
                    throw new \Exception('No se puede crear carpeta.');
                }
            }

            $exporter->export()->saveAs($carpeta . 'tmp/tmp_ventas');
            $cantidad_registros = $count;
            Yii::info($cantidad_registros . "#" . $this->monto_total_rango, "### total general");
            $this->createCabecera($periodo, $cantidad_registros, $this->monto_total_rango)->saveAs($carpeta . 'tmp/tmp_cabecera_ventas');
            $fp1 = fopen($carpeta . "tmp/tmp_cabecera_ventas", 'a+');
            $file2 = file_get_contents($carpeta . "tmp/tmp_ventas");
            fwrite($fp1, "\n" . $file2);
            if (file_exists($carpeta . "tmp/tmp_cabecera_ventas")) {
                $file = Yii::$app->response->sendFile($carpeta . "tmp/tmp_cabecera_ventas", 'reporte_ventas_' . $periodo[1] . $periodo[0] . '.csv');
                FileHelper::removeDirectory($carpeta . 'tmp');
                return $file;
            }
        }

        retorno:;
        return $this->render('reporte-venta-hechauka', [
            'model' => $model,
        ]);
    }

    public function actionLoadVentas()
    {
        $request = Yii::$app->request;
        $session = Yii::$app->session;

        $periodo = $request->get('periodo');
        $query = Venta::find();
        $query->where([
            'DATE_FORMAT(fecha_emision, "%m/%Y")' => $periodo,
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'estado' => 'vigente'
        ]);
        $query->andWhere(['IS NOT', 'timbrado_detalle_id', null]);
//        $query->orderBy(['fecha_emision' => SORT_DESC]);
        $session->remove('dataProvider');
        $session->set('dataProvider', new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => false
        ]));
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /**
     * @param $periodo
     * @param $cantidad_registros
     * @param $monto_total
     * @return \yii2tech\csvgrid\ExportResult
     * @throws \yii\base\InvalidConfigException
     */
    function createCabecera($periodo, $cantidad_registros, $monto_total)
    {
        $empresa = Empresa::findOne(\Yii::$app->session->get('core_empresa_actual'));
        $slices = explode('-', $periodo);
        $exporter = new CsvGrid([
            'csvFileConfig' => [
                'rowDelimiter' => "\n",
                'enclosure' => '',
                'cellDelimiter' => ";",
            ],
            'showHeader' => false,
            'dataProvider' => new ArrayDataProvider([
                'allModels' => [
                    [
                        'tipo' => '1',
                        'periodo' => $slices[0] . $slices[1],
                        'tipoRegistro' => '1',
                        'obligacion' => '921',
                        'formulario' => '221',
                        'ruc' => $empresa->ruc,
                        'dv' => $empresa->digito_verificador,
                        'nombre' => $empresa->razon_social,
                        'ruc_representante_legal' => $empresa->cedula,
                        'dv_representante_legal' => $empresa->dvCedula,
                        'nombre_representante_legal' => "$empresa->primer_apellido $empresa->segundo_apellido, $empresa->primer_nombre $empresa->segundo_nombre",
                        'cantidad_de_registros' => $cantidad_registros,
                        'sumatoria_del_monto_reportado' => $monto_total,
                        'version' => '2'
                    ],
                ],
            ]),
            'columns' => [
                [
                    'attribute' => 'tipo',
                ],
                [
                    'attribute' => 'periodo'
                ],
                [
                    'attribute' => 'tipoRegistro'
                ],
                [
                    'attribute' => 'obligacion'
                ],
                [
                    'attribute' => 'formulario'
                ],
                [
                    'attribute' => 'ruc'
                ],
                [
                    'attribute' => 'dv'
                ],
                [
                    'attribute' => 'nombre'
                ],
                [
                    'attribute' => 'ruc_representante_legal'
                ],
                [
                    'attribute' => 'dv_representante_legal'
                ],
                [
                    'attribute' => 'nombre_representante_legal'
                ],
                [
                    'attribute' => 'cantidad_de_registros'
                ],
                [
                    'attribute' => 'sumatoria_del_monto_reportado'
                ],
                [
                    'attribute' => 'version'
                ]

            ],
        ]);
        return $exporter->export();
    }
}

/**
 * @property string $periodo
 */
class ReporteVentaHechauka extends Model
{
    public $periodo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periodo'], 'safe'],
            [['periodo'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'periodo' => 'Corresponde al período fiscal que se informa.',
        ];
    }
}