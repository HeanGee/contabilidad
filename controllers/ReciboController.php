<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Cotizacion;
use backend\models\Moneda;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\Recibo;
use backend\modules\contabilidad\models\ReciboDetalle;
use backend\modules\contabilidad\models\ReciboDetalleContracuentas;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\RetencionDetalleIvas;
use backend\modules\contabilidad\models\search\ReciboSearch;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\Venta;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use common\models\ParametroSistema;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ReciboController implements the CRUD actions for Recibo model.
 */
class ReciboController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view', 'bloquear', 'desbloquear'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Recibo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Recibo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['bloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Recibo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['desbloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Recibo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'si';
                        }
                    ],
                ],
            ],
        ];
    }

    private function clearSesion()
    {

        $skey1 = 'cont_recibo_detalle_sim_data';
        $skey2 = 'cont_recibo_detalle_contracuenta_sim_data';
        $skey3 = 'cont_recibo_detalle_diff_costo_sim_data';
        $skey4 = 'cont_retencion_id';
        $skey5 = 'cont_recibo_sim_balanced';


        Yii::$app->session->remove($skey1);
        Yii::$app->session->remove($skey2);
        Yii::$app->session->remove($skey3);
        Yii::$app->session->remove($skey4);
        Yii::$app->session->remove($skey5);
        Yii::$app->session->remove('cont_recibo_detalle_retenciones');
        Yii::$app->session->remove('cont_recibo_detalle_retenciones_renta');
        Yii::$app->session->remove('total_debe');
        Yii::$app->session->remove('total_haber');
    }

    /**
     * Lists all Recibo models.
     * @return mixed
     */
    public function actionIndex($operacion = '')
    {
        $searchModel = new ReciboSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $operacion);

        $this->clearSesion();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recibo model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $operacion)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Recibo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Recibo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     */
    protected function findModel($id)
    {
        if (($model = Recibo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Recibo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $operacion
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreate($operacion = 'compra', $_pjax = null)
    {
        // verificar variables necesarias de la session porque en getFacturas(...) de Compra y Venta se acceden a estas sin preguntar.
        if (!\Yii::$app->session->has('core_empresa_actual')) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar empresa actual.');
            return $this->redirect(['index', 'operacion' => $operacion]);
        } elseif (!\Yii::$app->session->has('core_empresa_actual_pc')) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar el periodo contable para la empresa actual.');
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
        $parmsys_debe = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_debe]);

        $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
        $parmsys_haber = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_haber]);

        if (!isset($parmsys_debe)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Ganancia.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }
        if (!isset($parmsys_haber)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Pérdida.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_error_redondeo_ganancia";
        $ctaErrRedondeoGanancia = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre]);
        if (!isset($ctaErrRedondeoGanancia)) {
            FlashMessageHelpsers::createWarningMessage("Falta parametrizar en la empresa actual la cuenta para Ganancia por error de redondeo.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_error_redondeo_perdida";
        $ctaErrRedondeoPerdida = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre]);
        if (!isset($ctaErrRedondeoPerdida)) {
            FlashMessageHelpsers::createWarningMessage("Falta parametrizar en la empresa actual la cuenta para Pérdida por error de redondeo.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        // Crear modelo
        $model = new Recibo();
        $model->factura_tipo = $operacion;
        $model->empresa_id = Yii::$app->session->get('core_empresa_actual');
        $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');

        // Vaciar valores de las variables de session
        $skey1 = 'cont_recibo_detalle_sim_data';
        $skey2 = 'cont_recibo_detalle_contracuenta_sim_data';
        $skey3 = 'cont_recibo_detalle_diff_costo_sim_data';
        if (!Yii::$app->request->isPost && $_pjax == null) {
            $this->clearSesion();

            Yii::$app->session->set($skey1, []);
            Yii::$app->session->set($skey2, []);
            Yii::$app->session->set($skey3, []);
        }

        // Eliminar los blueprint
        if (!empty($_POST['Detalles']['new'])) {
            unset($_POST['Detalles']['new']);
            Yii::$app->request->setBodyParams($_POST);
        }
        if (!empty($_POST['Detallescontracuenta']['new'])) {
            unset($_POST['Detallescontracuenta']['new']);
            Yii::$app->request->setBodyParams($_POST);
        }

        /** @var ReciboDetalle[] $detalles */
        /** @var ReciboDetalleContracuentas[] $detallescontracta */
        $detalles = [];
        $detallescontracta = [];
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();

            try {
                // Eliminar los blueprit de los detalles
                $reciboDetalles = $_POST['Detalles'];
                $reciboDetallesContracta = $_POST['Detallescontracuenta'];
                unset($reciboDetalles['new']);
                unset($reciboDetallesContracta['new']);

                $total_detalles = 0;
                $total_cuentas = 0;
                // Crear nuevos detalles. Usado para poder recuperar y ver los detalles agregados, cuando vuelva al formulario por algun error.
                foreach ($reciboDetalles as $detalle) {
                    $newDetalle = new ReciboDetalle();
                    $newDetalle->setAttributes($detalle);
                    $newDetalle->factura_tipo = $model->factura_tipo;
                    $newDetalle->empresa_id = $model->empresa_id;
                    $newDetalle->periodo_contable_id = $model->periodo_contable_id;
                    $detalles[] = $newDetalle;

                }
                foreach ($reciboDetallesContracta as $detalle) {
                    $newDetalle = new ReciboDetalleContracuentas();
                    $newDetalle->setAttributes($detalle);
                    $newDetalle->empresa_id = $model->empresa_id;
                    $newDetalle->periodo_contable_id = $model->periodo_contable_id;
                    $detallescontracta[] = $newDetalle;
                }

                // Verificar que tenga detalles
                if (sizeof($reciboDetalles) == 0) {
                    throw new \Exception('Ud. No ha especificado ningun detalle.');
                }

                // Validar cabecera
                if (!$model->save()) {
                    throw new \Exception("No se pudo validar el recibo: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                /** @var Compra[]|Venta[] $facturas */
                $facturas = [];
                foreach ($detalles as $index => $newDetalle) {
                    $detalles[$index]->recibo_id = $model->id;
                    $newDetalle = $detalles[$index];

                    // Verificar coherencia de monedas de facturas y la del recibo.
                    if ($operacion == 'venta' && ($model->moneda_id != 1
                            && $model->moneda_id != $newDetalle->venta->moneda_id && $newDetalle->venta->moneda_id != 1)) {
                        throw new \Exception('Sólo se puede agregar facturas en moneda ' . $model->moneda->nombre . '. o en Guaraníes.');
                    } elseif ($operacion == 'compra' && ($model->moneda_id != 1
                            && $model->moneda_id != $newDetalle->compra->moneda_id && $newDetalle->compra->moneda_id != 1)) {
                        throw new \Exception('Sólo se puede agregar facturas en moneda ' . $model->moneda->nombre . '. o en Guaraníes.');
                    }

                    // verificar por cada detalle, que se hay especificado el monto y la factura.
                    $id_factura = $operacion == 'compra' ? $newDetalle->factura_compra_id : $newDetalle->factura_venta_id;
                    if ($id_factura == '' || $newDetalle->monto == '' || $newDetalle->monto < 1.0) {
                        $trans->rollBack();
                        $campo = $id_factura == '' ? "Nro Factura" : "Monto del Detalles";
                        $newDetalle->addError('monto', ("{$campo} no puede estar vacío."));
                        throw new \Exception("{$campo} no puede estar vacío.");
                    }

                    // Guardar detalle
//                    if (!$detalles[$index]->validate()) {
                    //if (!$newDetalle->validate()) {
                    if (!$newDetalle->save()) {
                        throw new \Exception("No se pudieron validar los detalles: " . implode(', ', $detalles[$index]->getErrorSummary(true)));
                    }
                    $newDetalle->refresh();
                    $total_detalles += round($newDetalle->valorizado, 2);
                    if (isset($newDetalle->compra)) $facturas[] = $newDetalle->compra;
                    elseif (isset($newDetalle->venta)) $facturas[] = $newDetalle->venta;

                    /*// verificar que no se indique monto mayor al saldo de la factura.
                    $factura = $operacion == 'compra' ? Compra::findOne(['id' => $newDetalle->factura_compra_id]) : Venta::findOne(['id' => $newDetalle->factura_venta_id]);
                    if ($newDetalle->monto > $factura->saldo) {
                        $trans->rollBack();
                        $nro_factura = $operacion == 'compra' ? $factura->nro_factura : $factura->getNroFacturaCompleto();
                        $detalles[$index]->addError('monto', 'El monto para la Factura ' . $nro_factura . ' no puede ser mayor a su saldo (' . $factura->saldo . ').');
                        throw new \Exception('El monto para la Factura ' . $nro_factura . ' no puede ser mayor a su saldo (' . $factura->saldo . ').');
                    }

                    // Actualizar el saldo de la factura asociada y guardar.
                    $saldo_original = number_format($factura->saldo, 2, ',', '.');
                    $factura->saldo -= (float)$newDetalle->monto;
                    #se valida asi para no tener que rellenar campos virtuales
                    if (round($factura->saldo, 2) < 0)
                        throw new \Exception("El saldo de la factura no puede quedar en negativo. Saldo original: {$saldo_original}");
                    $factura->save(false);

                    // Guardar el detalle.
                    $newDetalle->save(false);  // si se vueve a validar, se reemplaza otra vez la coma por punto y blah...*/
                }

                foreach ($detallescontracta as $index => $newDetalle) {
                    $detallescontracta[$index]->recibo_id = $model->id;
                    $newDetalle = $detallescontracta[$index];

                    // verificar que se haya indicado cuenta contable y monto.
                    if ($newDetalle->plan_cuenta_id == '' || $newDetalle->monto == '' || $newDetalle->monto < 1.0) {
                        $campo = $newDetalle->plan_cuenta_id == '' ? "Cuenta" : "Monto de Contracuentas";
                        throw new \Exception("{$campo} no puede estar vacío.");
                    }

                    // Validar detallecontracuenta y guardar
                    if (!$newDetalle->validate()) {
                        $detallescontracta[$index]->validate(); // para que agreque error
                        throw new \Exception("No se pudieron validar los detalles.");
                    }
                    $total_cuentas += round($newDetalle->monto, 2);

                    $newDetalle->save(false);
                }

                if (round($total_detalles, 2) != round($total_cuentas, 2)) {
                    $total_detalles = number_format($total_detalles, 2, ',', '.');
                    $total_cuentas = number_format($total_cuentas, 2, ',', '.');
                    throw new \Exception("El total de las facturas no coincide con el total de cuentas: facturas=$total_detalles y cuentas=$total_cuentas");
                }
                if (round($total_detalles, 2) > round($model->total, 2)) { // se podria obviar, ya que es lo mismo que preguntar si el monto excedente es negativo y eso esta prohibido en rules.
                    throw new \Exception("El total del recibo no puede ser menor que la suma de los montos de facturas en cancelación.");
                }

                // Asociar a este recibo, las retenciones registradas.
                $skey = 'cont_retencion_id';
                $session = Yii::$app->session;
                $retencion_ids = $session->has($skey) ? $session->get($skey) : [];
                foreach ($retencion_ids as $retencion_id) {
                    $retencion = Retencion::findOne(['id' => $retencion_id]);
                    $retencion->recibo_id = $model->id;
                    $retencion->save(false);
                }

                // actualizar saldo de facturas; si hay error, lanza excepcion.
                foreach ($facturas as $factura) {
                    $factura->refresh();
                    $factura->actualizarSaldo();
                }

//                if (!Yii::$app->session->has('cont_recibo_sim_balanced') || !Yii::$app->session->get('cont_recibo_sim_balanced')) {
//                    $total_debe = Yii::$app->session->get('total_debe');
//                    $total_debe = number_format($total_debe, '3', ',', '.');
//                    $total_haber = Yii::$app->session->get('total_haber');
//                    $total_haber = number_format($total_haber, '3', ',', '.');
//                    throw new \Exception("La simulación de asiento no está balanceado: Debe: {$total_debe}; Haber: {$total_haber}");
//                }

                // Comprometer la transaccion, remover variables de session.
                $trans->commit();
                $session->remove($skey);
                FlashMessageHelpsers::createSuccessMessage('El recibo ' . $model->numero . ' se ha creado exitosamente.');
                return $this->redirect(['index', 'operacion' => $operacion]);
            } catch (\Exception $exception) {
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());

                Yii::$app->session->set($skey1, []);
                Yii::$app->session->set($skey2, []);
                Yii::$app->session->set($skey3, []);
            }
        }

        retorno:;
        return $this->render('create', [
            'model' => $model,
            'detalles' => $detalles,
            'detallescontracta' => $detallescontracta
        ]);
    }

    /**
     * Updates an existing Recibo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param $operacion
     * @param null $_pjax
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate($id, $operacion, $_pjax = null)
    {
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
        $parmsys_debe = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_debe]);

        $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
        $parmsys_haber = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_haber]);

        if (!isset($parmsys_debe)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Ganancia.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }
        if (!isset($parmsys_haber)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Pérdida.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_error_redondeo_ganancia";
        $ctaErrRedondeoGanancia = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre]);
        if (!isset($ctaErrRedondeoGanancia)) {
            FlashMessageHelpsers::createWarningMessage("Falta parametrizar en la empresa actual la cuenta para Ganancia por error de redondeo.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }
        $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_error_redondeo_perdida";
        $ctaErrRedondeoPerdida = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre]);
        if (!isset($ctaErrRedondeoPerdida)) {
            FlashMessageHelpsers::createWarningMessage("Falta parametrizar en la empresa actual la cuenta para Pérdida por error de redondeo.");
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        $model = $this->findModel($id);
        if (($msg = $model->checkEditability()) != '') {
            FlashMessageHelpsers::createWarningMessage($msg, 13000);
            return $this->redirect(['index', 'operacion' => $operacion]);
        }

        // Controlar que se modifique solo recibo de la empresa actual y periodo contable actual
        if ($model->empresa_id != Yii::$app->session->get('core_empresa_actual') || $model->periodo_contable_id != Yii::$app->session->get('core_empresa_actual_pc')) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        // Localizar modelo
        $entidad = Entidad::findOne(['id' => $model->entidad_id]);
        $model->ruc = $entidad->ruc;
        $model->razon_social = $entidad->razon_social;

        // Vaciar valores de las variables de session
        if (!Yii::$app->request->isPost && $_pjax == null) {
            $this->clearSesion();

            $skey1 = 'cont_recibo_detalle_sim_data';
            $skey2 = 'cont_recibo_detalle_contracuenta_sim_data';
            $skey3 = 'cont_recibo_detalle_diff_costo_sim_data';

            Yii::$app->session->set($skey1, []);
            Yii::$app->session->set($skey2, []);
            Yii::$app->session->set($skey3, []);
        }

        // Eliminar los blueprint
        if (!empty($_POST['Detalles']['new'])) {
            unset($_POST['Detalles']['new']);
            Yii::$app->request->setBodyParams($_POST);
        }
        if (!empty($_POST['Detallescontracuenta']['new'])) {
            unset($_POST['Detallescontracuenta']['new']);
            Yii::$app->request->setBodyParams($_POST);
        }

        // Cargar los detalles del recibo. Para visualizar en el formulario.
        $detalles = $model->reciboDetalles;
        $detallescontracta = $model->reciboDetalleContracuentas;

        if ($model->load(Yii::$app->request->post())) {

            $trans = Yii::$app->db->beginTransaction();

            try {
                $detalles = [];
                $detallescontracta = [];

                // Eliminar blueprint de los detalles traidos del post.
                $reciboDetalles = $_POST['Detalles'];
                $reciboDetallesContracta = $_POST['Detallescontracuenta'];
                unset($reciboDetalles['new']);
                unset($reciboDetallesContracta['new']);
                $detalles_a_guardar = [];
                $detalles_contracta_a_guardar = [];

                $total_detalles = 0;
                $total_cuentas = 0;
                // crear nuevos detalles.
                foreach ($reciboDetalles as $detalle) {
                    $newDetalle = new ReciboDetalle();
                    $newDetalle->setAttributes($detalle);
                    $newDetalle->recibo_id = $model->id;
                    $newDetalle->factura_tipo = $model->factura_tipo;
                    $newDetalle->empresa_id = $model->empresa_id;
                    $newDetalle->periodo_contable_id = $model->periodo_contable_id;
                    $detalles[] = $newDetalle;
                }
                foreach ($reciboDetallesContracta as $detalle) {
                    $newDetalle = new ReciboDetalleContracuentas();
                    $newDetalle->setAttributes($detalle);
                    $newDetalle->recibo_id = $model->id;
                    $newDetalle->empresa_id = $model->empresa_id;
                    $newDetalle->periodo_contable_id = $model->periodo_contable_id;
                    $detallescontracta[] = $newDetalle;
                }

                // Verificar que haya detalle.
                if (sizeof($reciboDetalles) == 0) {
                    throw new \Exception('Ud. No ha especificado ningun detalle.');
                }

                // Validar cabecera
                if (!$model->save()) {
                    throw new \Exception("No se pudo validar el recibo: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                // Validar los detalles.
                foreach ($detalles as $index => $newDetalle) {
                    // Verificar coherencia de monedas de facturas y la del recibo.
                    if ($operacion == 'venta' && ($model->moneda_id != 1 && $model->moneda_id != $newDetalle->venta->moneda_id && $newDetalle->venta->moneda_id != 1)) {
                        $detalles = $model->reciboDetalles;
                        $detallescontracta = $model->reciboDetalleContracuentas;
                        throw new \Exception('Sólo se puede agregar facturas en moneda ' . $model->moneda->nombre . '.');
                    } elseif ($operacion == 'compra' && ($model->moneda_id != 1 && $model->moneda_id != $newDetalle->compra->moneda_id && $newDetalle->compra->moneda_id != 1)) {
                        $detalles = $model->reciboDetalles;
                        $detallescontracta = $model->reciboDetalleContracuentas;
                        throw new \Exception('Sólo se puede agregar facturas en moneda ' . $model->moneda->nombre . '.');
                    }

                    // Verificar que el monto no este vacio.
                    $id_factura = $operacion == 'compra' ? $newDetalle->factura_compra_id : $newDetalle->factura_venta_id;
                    if ($id_factura == '' || $newDetalle->monto == '' || $newDetalle->monto < 1.0) {
                        $campo = $id_factura == '' ? "Nro Factura" : "Monto del Detalles";
                        $detalles = $model->reciboDetalles;
                        $detallescontracta = $model->reciboDetalleContracuentas;
                        throw new \Exception("{$campo} no puede estar vacío.");
                    }

                    // Validar detalle
                    if (!$detalles[$index]->validate()) {
                        $msg = '';
                        foreach ($newDetalle->getErrorSummary(true) as $item) {
                            $msg .= $item . ', ';
                        }
                        $detalles = $model->reciboDetalles;
                        $detallescontracta = $model->reciboDetalleContracuentas;
                        throw new \Exception("No se pudieron validar los detalles: " . substr($msg, 0, (strlen($msg) - 3)) . '.');
                    }

                    $detalles_a_guardar[] = $detalles[$index];
                    $total_detalles += round($detalles[$index]->valorizado, 2);
                }
                foreach ($detallescontracta as $key => $newDetalle) {
                    // Verificar que no este vacio el monto
                    if ($newDetalle->plan_cuenta_id == '' || $newDetalle->monto == '' || $newDetalle->monto < 1.0) {
                        $campo = $newDetalle->plan_cuenta_id == '' ? "Cuenta" : "Monto de Contracuentas";
                        $detalles = $model->reciboDetalles;
                        $detallescontracta = $model->reciboDetalleContracuentas;
                        throw new \Exception("{$campo} no puede estar vacío.");
                    }

                    // Validar detallecontracuenta.
                    if (!$detallescontracta[$key]->validate()) {
                        $msg = '';
                        foreach ($newDetalle->getErrorSummary(true) as $item) {
                            $msg .= $item . ', ';
                        }
                        $detalles = $model->reciboDetalles;
                        $detallescontracta = $model->reciboDetalleContracuentas;
                        throw new \Exception("No se pudieron validar los detalles: " . substr($msg, 0, (strlen($msg) - 3)) . '.');
                    }

                    $detalles_contracta_a_guardar[] = $detallescontracta[$key];
                    $total_cuentas += round($detallescontracta[$key]->monto, 2);
                }

                if (round($total_detalles, 2) != round($total_cuentas, 2)) {
                    $total_detalles = number_format($total_detalles, 2, ',', '.');
                    $total_cuentas = number_format($total_cuentas, 2, ',', '.');
                    throw new \Exception("El total de las facturas no coincide con el total de cuentas: facturas=$total_detalles y cuentas=$total_cuentas");
                }
                if (round($total_detalles, 2) > round($model->total, 2)) { // se podria obviar, ya que es lo mismo que preguntar si el monto excedente es negativo y eso esta prohibido en rules.
                    throw new \Exception("El total del recibo no puede ser menor que la suma de los montos de facturas en cancelación.");
                }

                // eliminar los detalles viejos, regresar saldo.
                /** @var Compra[]|Venta[] $old_facturas */
                $old_facturas = [];
                $oldDetalles = ReciboDetalle::findAll(['recibo_id' => $id]);
                foreach ($oldDetalles as $oldDetalle) {
                    if (isset($oldDetalle->compra)) $old_facturas[] = $oldDetalle->compra;
                    elseif (isset($oldDetalle->venta)) $old_facturas[] = $oldDetalle->venta;
                    if (!$oldDetalle->delete()) {
                        throw new \Exception("No se pudo borrar detalle anterior: {$oldDetalle->getErrorSummaryAsString()}");
                    }
                }
                // actualizar saldo de factura; si hay error, lanza excepcion.
                foreach ($old_facturas as $old_factura) {
                    $old_factura->refresh();
                    $old_factura->actualizarSaldo();
                }

                // guardar detalles nuevos
                /** @var Venta[]|Compra[] $facturas */
                $facturas = [];
                /** @var ReciboDetalle $detalle */
                foreach ($detalles_a_guardar as $index => $detalle) {
                    /*// Actualizar saldo de la factura asociada.
                    $factura = $operacion == 'compra' ? Compra::findOne(['id' => $detalle->factura_compra_id]) : Venta::findOne(['id' => $detalle->factura_venta_id]);
                    if ($detalle->monto > $factura->saldo) {
                        $factura_nro = $operacion == 'venta' ? $factura->getNroFacturaCompleto() : $factura->nro_factura;
                        $detalles = $model->reciboDetalles;
                        $detallescontracta = $model->reciboDetalleContracuentas;
                        throw new \Exception('Monto ' . $detalle->monto . ' no válido para la Factura ' . $factura_nro . ': El saldo queda en negativo.');
                    }

                    $saldo_original = number_format($factura->saldo, 2, ',', '.');
                    $factura->saldo -= (float)$detalle->monto;
                    #se valida asi para no tener que rellenar campos virtuales
                    if (round($factura->saldo, 2) < 0)
                        throw new \Exception("El saldo de la factura no puede quedar en negativo. Saldo original: {$saldo_original}");
                    $factura->save(false);

                    if (!$detalle->save(false)) { // si se valida nuevamente, se vuelve a reemplazar la coma por punto y blah
                        $msg = '';
                        foreach ($detalle->getErrorSummary(true) as $item) {
                            $msg .= $item . ', ';
                        }
                        $detalles = $model->reciboDetalles;
                        $detallescontracta = $model->reciboDetalleContracuentas;
                        throw new \Exception('Error guardando detalles.' . substr($msg, 0, (strlen($msg) - 3)));
                    }*/
                    if (!$detalle->save(false))
                        throw new \Exception("No se pudo guardar detalle: {$detalle->getErrorSummaryAsString()}");
                    $detalle->refresh();
                    if (isset($detalle->compra)) $facturas[] = $detalle->compra;
                    elseif (isset($detalle->venta)) $facturas[] = $detalle->venta;
                }

                // actualizar saldo de facturas; si hay error, lanza excepcion.
                foreach ($facturas as $factura) {
                    $factura->refresh();
                    $factura->actualizarSaldo();
                }

                // eliminar detallescontracta viejos
                $oldDetallesContraCta = ReciboDetalleContracuentas::findAll(['recibo_id' => $model->id]);
                foreach ($oldDetallesContraCta as $item) {
                    $item->delete();
                }

                // guardar detallescontracta nuevos.
                foreach ($detalles_contracta_a_guardar as $detalle) {
                    $detalle->save(false); // si se valida, se vuelve a reemplazar la coma por punto y blah...
                }

//                if (!Yii::$app->session->has('cont_recibo_sim_balanced') || !Yii::$app->session->get('cont_recibo_sim_balanced')) {
//                    $total_debe = Yii::$app->session->get('total_debe');
//                    $total_debe = number_format($total_debe, '3', ',', '.');
//                    $total_haber = Yii::$app->session->get('total_haber');
//                    $total_haber = number_format($total_haber, '3', ',', '.');
//                    throw new \Exception("La simulación de asiento no está balanceado: Debe: {$total_debe}; Haber: {$total_haber}");
//                }

                $trans->commit();
                FlashMessageHelpsers::createSuccessMessage('El recibo ' . $model->numero . ' se ha modificado exitosamente.');
                return $this->redirect(['index', 'operacion' => $operacion]);
            } catch (\Exception $exception) {
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        retorno:;
        return $this->render('update', [
            'model' => $model,
            'detalles' => $detalles,
            'detallescontracta' => $detallescontracta
        ]);
    }

    /**
     * Deletes an existing Recibo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id, $operacion)
    {
        $model = $this->findModel($id);
        try {
            if (($msg = $model->checkEditability()) != '') {
                FlashMessageHelpsers::createWarningMessage($msg, 13000);
                return $this->redirect(['index', 'operacion' => $operacion]);
            }

            if ($model->empresa_id != Yii::$app->session->get('core_empresa_actual') || $model->periodo_contable_id != Yii::$app->session->get('core_empresa_actual_pc')) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $detalles = $model->reciboDetalles;
            $contractas = $model->reciboDetalleContracuentas;
            $transaccion = Yii::$app->db->beginTransaction();

            /** @var Compra[]|Venta[] $facturas */
            $facturas = [];
            foreach ($detalles as $detalle) {
                if (isset($detalle->compra)) $facturas[] = $detalle->compra;
                elseif (isset($detalle->venta)) $facturas[] = $detalle->venta;
                if (!$detalle->delete()) {
                    $nro_factura = $detalle->factura_tipo == 'compra' ? $detalle->compra->nro_factura : $detalle->venta->prefijo . '-' . $detalle->venta->nro_factura;
                    throw new \Exception('Error itentando borrar el detalle del recibo para la Factura de ' . ucfirst($operacion) . ' ' . $nro_factura);
                }
            }

            // actualizar saldo de facturas; si hay error, excepcion
            foreach ($facturas as $factura) {
                $factura->refresh();
                $factura->actualizarSaldo(true);
            }

            // Borrar las contracuentas.
            foreach ($contractas as $contracta) {
                if (!$contracta->delete()) {
                    throw new \Exception('Error intentando borrar la Cuenta ' . $contracta->planCuenta->getNombreConCodigo() . ' - ' . $contracta->planCuenta->nombre);
                }
            }

            $retencion = Retencion::findOne(['recibo_id' => $id]);
            if (isset($retencion)) {
                $retencion->recibo_id = "";
                $retencion->save(false);
            }

            // Borrar el recibo
            if (!$model->delete()) {
                throw new \Exception('Error intentando borrar el Recibo ' . $model->numero);
            }
            $transaccion->commit();
            FlashMessageHelpsers::createSuccessMessage('Recibo ' . $model->numero . ' eliminado exitosamente.');
        } catch (\Exception $exception) {
            !isset($transaccion) || $transaccion->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 15000);
        }

        retorno:;
        return $this->redirect(['index', 'operacion' => $operacion]);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionGetRazonSocialByRuc()
    {
        $ruc = $_POST['ruc'];
        $entidad = Entidad::findOne(['ruc' => $_POST['ruc']]);
        if ($entidad != null)
            return $entidad->razon_social;

//        FlashMessageHelpsers::createWarningMessage('Este R.U.C. no corresponde a ninguna Entidad.');
        return '';
    }

    public function actionSimularAsiento($operacion)
    {
        // Si por razon desconocida, no viene la variable en el post, limpiar sesion y retornar falso.
        if (!array_key_exists('detalles_factura', $_POST)) {
            $skey1 = 'cont_recibo_detalle_sim_data';
            $skey2 = 'cont_recibo_detalle_contracuenta_sim_data';
            $skey3 = 'cont_recibo_detalle_diff_costo_sim_data';
            $skey4 = 'cont_recibo_detalle_retenciones';
            $skey5 = 'cont_recibo_detalle_retenciones_renta';
            Yii::$app->session->set($skey1, []);
            Yii::$app->session->set($skey2, []);
            Yii::$app->session->set($skey3, []);
            Yii::$app->session->set($skey4, []);
            Yii::$app->session->set($skey5, []);
            return false;
        }
        if (!array_key_exists('detalles_contracta', $_POST)) {
            $skey1 = 'cont_recibo_detalle_sim_data';
            $skey2 = 'cont_recibo_detalle_contracuenta_sim_data';
            $skey3 = 'cont_recibo_detalle_diff_costo_sim_data';
            $skey4 = 'cont_recibo_detalle_retenciones';
            $skey5 = 'cont_recibo_detalle_retenciones_renta';
            Yii::$app->session->set($skey1, []);
            Yii::$app->session->set($skey2, []);
            Yii::$app->session->set($skey3, []);
            Yii::$app->session->set($skey4, []);
            Yii::$app->session->set($skey5, []);
            return false;
        }

        // Obtener datos del post.
        $detalles_factura = $_POST['detalles_factura'];
        $detalles_contracta = $_POST['detalles_contracta'];

        // Generar datos en la sesion.
        // Estos datos son el monto, partida(debe/haber), id del input, codigo de cta contable y nombre de cta contable,
        // agrupadas por el id del input.
        foreach ($detalles_factura as $fila) {
            $exitoso = $this->execSimulation($operacion, $fila);
            if (!$exitoso) return $exitoso;
        }

        $counter = 1;
        foreach ($detalles_contracta as $fila) {
            $exitoso = $this->execSimulation($operacion, $fila);
            if (!$exitoso) return $exitoso;
            $counter++;
        }

        return true;
    }

    private function execSimulation($operacion, $fila)
    {
        $skey1 = 'cont_recibo_detalle_sim_data';
        $skey2 = 'cont_recibo_detalle_contracuenta_sim_data';
        $skey3 = 'cont_recibo_detalle_diff_costo_sim_data';
        $skey4 = 'cont_recibo_detalle_retenciones';
        $skey5 = 'cont_recibo_detalle_retenciones_renta';
        $session = Yii::$app->session;
//        $session->remove($skey4);
//        $session->remove($skey5);

        $monto = $fila['monto'];
        $cotizacion_factura = $fila['cotizacion_factura'];
        $cotizacion_a_usar = $fila['cotizacion_a_usar'];
        $cuenta_id = $fila['id_cuenta'];
        $factura_id = $fila['id_factura'];
        $id_input = $fila['id_input'];
        $recibo_moneda_id = $fila['recibo_moneda_id'];
        $recibo_valor_moneda = $fila['recibo_valor_moneda'];
        $recibo_fecha = $fila['recibo_fecha'];

        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');

        #buscar cuenta contable en el parametro de empresa actual
        $tipo_retencion = ($operacion == 'compra') ? "cuenta_retenciones_emitidas" : "cuenta_retenciones_recibidas";
        $parmName = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-{$tipo_retencion}";
        $parmCtaRetencIva = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $parmName]);

        #buscar cuenta contable en el parametro de empresa actual
        $tipo_retencion = ($operacion == 'compra') ? "cuenta_retenciones_renta_emitidas" : "cuenta_retenciones_renta_recibidas";
        $parmName = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-{$tipo_retencion}";
        $parmCtaRetencRenta = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $parmName]);

        if ($recibo_fecha == '') {
            FlashMessageHelpsers::createWarningMessage('Falta especificar la fecha.');
            goto retorno;
        }

        if ($monto == '' || $monto == 0 || $monto == '0') {
            if ($factura_id != '') {
                if ($operacion == 'compra') {
                    $factura = Compra::findOne(['id' => $factura_id]);
                    FlashMessageHelpsers::createWarningMessage('Monto esta vacio para Factura Nro ' . $factura->nro_factura);
                } else {
                    $factura = Venta::findOne(['id' => $factura_id]);
                    FlashMessageHelpsers::createWarningMessage('Monto esta vacio para Factura Nro ' . $factura->getNroFacturaCompleto());
                }
                goto retorno;
            } elseif ($cuenta_id != '') {
                $planCuenta = PlanCuenta::findOne(['id' => $cuenta_id]);
                FlashMessageHelpsers::createWarningMessage('Monto esta vacio para Cuenta ' . $planCuenta->nombre);
                goto retorno;
            }
        }

        if ($factura_id != '') {
            // Determinar factura (compra/venta)
            $factura = $operacion == 'compra' ? Compra::findOne(['id' => $factura_id]) : Venta::findOne(['id' => $factura_id]);
            // Obtener el tipo de documento
            $tipo_doc_factura = TipoDocumento::findOne(['id' => $factura->tipo_documento_id]);
            // Obtener la cuenta contable asociada al tipo de documento.
            $cuenta_contable_tipodoc_factura = PlanCuenta::findOne(['id' => $tipo_doc_factura->cuenta_id]);

            // Verificar que la factura, tenga 1 detalle por la cuenta de este tipo de documento.
            $facturaDetalle = $operacion == 'compra' ?
                DetalleCompra::findOne(['plan_cuenta_id' => $cuenta_contable_tipodoc_factura->id, 'cta_contable' => 'haber']) :
                DetalleVenta::findOne(['plan_cuenta_id' => $cuenta_contable_tipodoc_factura->id, 'cta_contable' => 'debe']);

            if ($facturaDetalle == null) {
                FlashMessageHelpsers::createWarningMessage('Extrañamente la factura no tiene la cuenta asociada al tipo de documento.');
                goto retorno;
            }

            // Crear modelo auxiliar para simulacion
            $model = new AsientoSimulatorData();
            {
                $retencion_total = 0;
                if (isset($parmCtaRetencIva) && isset($parmCtaRetencRenta)) {
                    #si la factura tiene retenciones, incluir el monto total de los mismos
                    $retencion_renta = 0;
                    $retencion_iva = 0;
                    foreach (Retencion::findAll(["factura_{$operacion}_id" => $factura_id]) as $retencion) {
                        $retencion_renta += round(round($retencion->base_renta_porc, 2) * (round($retencion->factor_renta_porc, 2) / 100), 2);
                        /** @var RetencionDetalleIvas $retencionDetalle */
                        foreach ($retencion->detalles as $retencionDetalle) {
                            $retencion_iva += round($retencionDetalle->base * $retencionDetalle->factor / 100, 2);
                        }
                    }
                    if ($retencion_iva > 0) {
                        $cuentaRetencion = PlanCuenta::findOne(['id' => $parmCtaRetencIva->valor]);
                        $model_ret = new AsientoSimulatorData();
                        $model_ret->monto = round($retencion_iva * $cotizacion_factura, 2);
                        $retencion_total += round($model_ret->monto, 2);
                        $model_ret->partida = ($operacion == 'compra') ? 'haber' : 'debe';
                        $model_ret->codigo_cuenta = $cuentaRetencion->cod_completo;
                        $model_ret->nombre_cuenta = $cuentaRetencion->nombre;
                        $model_ret->id_input = $id_input;
                        $data = $session->get($skey4);
                        $index = $this->findElement($model_ret, $data);
                        if ($index == -1)
                            $data[$model_ret->id_input] = [
                                'monto' => $model_ret->monto,
                                'partida' => $model_ret->partida,
                                'codigo_cuenta' => $model_ret->codigo_cuenta,
                                'nombre_cuenta' => $model_ret->nombre_cuenta,
                            ];
                        else {
                            $data[$index]['monto'] = $model_ret->monto;
                            $data[$index]['partida'] = $model_ret->partida;
                            $data[$index]['codigo_cuenta'] = $model_ret->codigo_cuenta;
                            $data[$index]['nombre_cuenta'] = $model_ret->nombre_cuenta;
                        }
                        $session->set($skey4, $data);
                    }
                    if ($retencion_renta > 0) {
                        $cuentaRetencion = PlanCuenta::findOne(['id' => $parmCtaRetencRenta->valor]);
                        $model_ret = new AsientoSimulatorData();
                        $model_ret->monto = round($retencion_renta * $cotizacion_factura, 2);
                        $retencion_total += round($model_ret->monto, 2);
                        $model_ret->partida = ($operacion == 'compra') ? 'haber' : 'debe';
                        $model_ret->codigo_cuenta = $cuentaRetencion->cod_completo;
                        $model_ret->nombre_cuenta = $cuentaRetencion->nombre;
                        $model_ret->id_input = $id_input;
                        $data = $session->get($skey5);
                        $index = $this->findElement($model_ret, $data);
                        if ($index == -1)
                            $data[$model_ret->id_input] = [
                                'monto' => $model_ret->monto,
                                'partida' => $model_ret->partida,
                                'codigo_cuenta' => $model_ret->codigo_cuenta,
                                'nombre_cuenta' => $model_ret->nombre_cuenta,
                            ];
                        else {
                            $data[$index]['monto'] = $model_ret->monto;
                            $data[$index]['partida'] = $model_ret->partida;
                            $data[$index]['codigo_cuenta'] = $model_ret->codigo_cuenta;
                            $data[$index]['nombre_cuenta'] = $model_ret->nombre_cuenta;
                        }
                        $session->set($skey5, $data);
                    }
                } else {
                    $retencion_total = 0;
                    $session->remove($skey4);
                    $session->remove($skey5);
                }
            }
            $model->monto = ($monto) * $cotizacion_factura + $retencion_total;
            $model->partida = $operacion == 'compra' ? 'debe' : 'haber';
            $model->codigo_cuenta = $cuenta_contable_tipodoc_factura->cod_completo;
            $model->nombre_cuenta = $cuenta_contable_tipodoc_factura->nombre;
            $model->id_input = $id_input;
            $data = $session->get($skey1);
            $index = $this->findElement($model, $data);
            if ($index == -1)
                $data[$model->id_input] = [
                    'monto' => $model->monto,
                    'partida' => $model->partida,
                    'codigo_cuenta' => $model->codigo_cuenta,
                    'nombre_cuenta' => $model->nombre_cuenta,
                ];
            else {
                $data[$index]['monto'] = $model->monto;
                $data[$index]['partida'] = $model->partida;
                $data[$index]['codigo_cuenta'] = $model->codigo_cuenta;
                $data[$index]['nombre_cuenta'] = $model->nombre_cuenta;
            }
            $session->set($skey1, $data);

            // calculo de diff de cambio
            $model = null;
            // Si la moneda de la factura es extrangera
            if ($factura->moneda_id != ParametroSistema::getMonedaBaseId()) {
                if ($cotizacion_a_usar == 0) {
                    if ($recibo_valor_moneda == "") {
                        $fecha = date('Y-m-d', strtotime('-1 day', strtotime($recibo_fecha)));
                        $cotizacion = Cotizacion::findOne(['moneda_id' => $factura->moneda_id, 'fecha' => $fecha]);
                        $cotizacion_a_usar = $operacion == 'compra' ? $cotizacion->venta : $cotizacion->compra;
                    } else
                        $cotizacion_a_usar = $recibo_valor_moneda;
                }
                $diff = round($cotizacion_a_usar, 2) - round($cotizacion_factura, 2);
                $model = new AsientoSimulatorData();
                $model->monto = round(round($monto, 2) * round(abs($diff), 2), 4);
                $model->id_input = $id_input;
                $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";  # el sufijo debe/haber estan invertidos con respecto a la ganancia/perdida. Si la diferencia de cambio resulta en ganancia, va en el haber y viceversa.
                $parmsys_debe = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_debe]);
                $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";  # el sufijo debe/haber estan invertidos con respecto a la ganancia/perdida. Si la diferencia de cambio resulta en ganancia, va en el haber y viceversa.
                $parmsys_haber = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $nombre_haber]);
                if ($operacion == 'venta') {
                    $model->partida = $diff > 0 ? 'haber' : 'debe';
                    $cuenta = PlanCuenta::findOne(['id' => $diff > 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor]);
                } else {
                    $model->partida = $diff > 0 ? 'debe' : 'haber';
                    $cuenta = PlanCuenta::findOne(['id' => $diff > 0.0 ? $parmsys_haber->valor : $parmsys_debe->valor]);
                }
                $model->codigo_cuenta = $cuenta->cod_completo;
                $model->nombre_cuenta = $cuenta->nombre;
            }
            #poner en sesion
            if ($model != null) {
                $data = $session->get($skey3);
                $index = $this->findElement($model, $data);
                if ($index == -1)
                    $data[$model->id_input] = [
                        'monto' => $model->monto,
                        'partida' => $model->partida,
                        'codigo_cuenta' => $model->codigo_cuenta,
                        'nombre_cuenta' => $model->nombre_cuenta,
                    ];
                else {
                    $data[$index]['monto'] = $model->monto;
                    $data[$index]['partida'] = $model->partida;
                    $data[$index]['codigo_cuenta'] = $model->codigo_cuenta;
                    $data[$index]['nombre_cuenta'] = $model->nombre_cuenta;
                }
                $session->set($skey3, $data);
            }
            return true;
        } elseif ($cuenta_id != '') {
            $cuenta = PlanCuenta::findOne(['id' => $cuenta_id]);
            $model = new AsientoSimulatorData();
            $model->monto = $monto * ($recibo_valor_moneda != "" ? $recibo_valor_moneda : 1);
            $model->partida = $operacion == 'compra' ? 'haber' : 'debe';
            $model->codigo_cuenta = $cuenta->cod_completo;
            $model->nombre_cuenta = $cuenta->nombre;
            $model->id_input = $id_input;
            $data = $session->get($skey2);
            $index = $this->findElement($model, $data);
            if ($index == -1)
                $data[$model->id_input] = [
                    'monto' => $model->monto,
                    'partida' => $model->partida,
                    'codigo_cuenta' => $model->codigo_cuenta,
                    'nombre_cuenta' => $model->nombre_cuenta,
                ];
            else {
                $data[$index]['monto'] = $model->monto;
                $data[$index]['partida'] = $model->partida;
                $data[$index]['codigo_cuenta'] = $model->codigo_cuenta;
                $data[$index]['nombre_cuenta'] = $model->nombre_cuenta;
            }
            $session->set($skey2, $data);
            return true;
        }

        retorno:;
        return false;
    }

    /**
     * @param $operacion
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSimularAsientoRemoverItem($operacion)
    {
        $skey1 = 'cont_recibo_detalle_sim_data';
        $skey2 = 'cont_recibo_detalle_contracuenta_sim_data';
        $skey3 = 'cont_recibo_detalle_diff_costo_sim_data';
        $skey4 = 'cont_recibo_detalle_retenciones';
        $skey5 = 'cont_recibo_detalle_retenciones_renta';
        $session = Yii::$app->session;

        $del = $_POST['del'];
        $id_input = Yii::$app->request->post('id_input');
        $action_id = $_POST['action_id'];
        $matches = [];

        if ($del == 'factura') {
            foreach ([$skey1, $skey3, $skey4, $skey5] as $skey) {
                $data = $session->get($skey);
                unset($data[$id_input]);
                $session->set($skey, $data);
            }
            /*$data = $session->get($skey1);
            unset($data[$id_input]);
            $session->set($skey1, $data);

            $data = $session->get($skey3);
            unset($data[$id_input]);
            $session->set($skey3, $data);*/

//            // si el detalle existe en la bd, borrar.
//            if ($action_id == "update" && preg_match('/Detalles_new([0-9]+)/', $id_input, $matches) && array_key_exists('recibo_id', $_POST)) {
//                $recibo_det_id = $matches[1];
//                $recibo_det = ReciboDetalle::findOne(['id' => $recibo_det_id, 'recibo_id' => $_POST['recibo_id']]);
//                if ($recibo_det != null) {
//                    $recibo_det->delete();
//                }
//            }
            return true;
        } elseif ($del == 'contracta') {
            $data = $session->get($skey2);
            unset($data[$id_input]);
            $session->set($skey2, $data);

//            // si el detalle existe en la bd, borrar.
//            if ($action_id == "update" && preg_match('/Detallescontracuenta_new([0-9]+)/', $id_input, $matches) && array_key_exists('recibo_id', $_POST)) {
//                $recibo_det_id = $matches[1];
//                $recibo_det = ReciboDetalleContracuentas::findOne(['id' => $recibo_det_id, 'recibo_id' => $_POST['recibo_id']]);
//                if ($recibo_det != null) {
//                    $recibo_det->delete();
//                }
//            }
//            return true;
        }

        retorno:;
        return false;
    }

    /**
     * @param $needle AsientoSimulatorData
     * @param $conjunto array
     * @return int|mixed|string
     */
    private function findElement($needle, $conjunto, $compareAlsoPartida = false)
    {
        if ($conjunto != null)
            foreach ($conjunto as $index => $item) {
                if ($needle->id_input == $index) {
                    if ($compareAlsoPartida && $needle->partida == $item['partida']) return $index;
                    elseif ($compareAlsoPartida && $needle->partida != $item['partida']) return -1;
                    return $index;
                }
            }

        return -1;
    }

    public function actionGetCotizacion($operacion = 'compra')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $existe = array_key_exists('fecha', $_GET) && array_key_exists('moneda_id', $_GET);
        $result = ['result' => "", 'error' => 'Error Interno: Falta  enviar en el GET Fecha de Emisión.'];
        Yii::$app->session->set('get', array_key_exists('fecha', $_GET));

        if ($existe) {
            $result = null;
            $fecha_emision = $_GET['fecha'];
            $fecha = date('Y-m-d', strtotime('-1 day', strtotime($fecha_emision)));
            $moneda_id = $_GET['moneda_id'];
            if ($moneda_id == 1) {
                $result = ['result' => "", 'error' => ""];
            } else {
                $query = Cotizacion::find()->where(['fecha' => $fecha, 'moneda_id' => $moneda_id])->one();
                if ($query != null) {
                    if ($operacion == 'compra')
                        $result = ['result' => $query->venta, 'error' => ""];
                    else
                        $result = ['result' => $query->compra, 'error' => ''];
                } else {
                    $result = ['result' => "", 'error' => 'Falta cargar cotizaciön de la SET.'];
                }
            }
        }
        return $result;
    }

    public function actionGetCotizacionFactura($operacion)
    {
        $factura = $operacion = 'venta' ? Venta::findOne(['id' => $_GET['factura_id']]) : Compra::findOne(['id' => $_GET['factura_id']]);
        $fecha_emision = $factura->fecha_emision;
        $fecha_hoy = date('Y-m-d');
        $moneda_id = $factura->moneda_id;
        $fecha = $fecha_emision | $fecha_hoy;
        $query = Cotizacion::find()->where(['fecha' => $fecha, 'moneda_id' => $moneda_id])->one();
        return $query != null ? ($operacion == 'compra' ? $query->venta : $query->compra) : '';
    }

    public function actionGetSaldoAndMonedanombre($operacion)
    {
        try {
            if ($operacion == 'compra') {
                $compra = Compra::findOne(['id' => $_GET['factura_id']]);
                $moneda = Moneda::findOne(['id' => $compra->moneda_id]);
                return $compra->saldo . '|' . $moneda->nombre;
            } else {
                $venta = Venta::findOne(['id' => $_GET['factura_id']]);
                $moneda = Moneda::findOne(['id' => $venta->moneda_id]);
                return $venta->saldo . '|' . $moneda->nombre;
            }
        } catch (\Exception $e) { // por si envia un id no existente de factura.
            throw $e;
        }
    }

    public function actionGetFacturasByEntidad($operacion)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!array_key_exists('entidad_id', $_GET)) return [];

        $action_id = "";
        if (array_key_exists('action_id', $_GET)) $action_id = $_GET['action_id'];
        $entidad_id = $_GET['entidad_id'];
        $facturas_id = !array_key_exists('facturas_id', $_GET) ? [] : $_GET['facturas_id'];
        $fecha_recibo = $_GET['fecha_recibo'];

        if ($operacion == 'compra')
            if ($action_id == 'create')
                return Compra::getFacturas(false, $facturas_id, "credito", false, false, true, $entidad_id, $fecha_recibo);
            else
                return Compra::getFacturas(false, $facturas_id, "credito", false, false, false, $entidad_id, $fecha_recibo);
        else
            if ($action_id == 'create')
                return Venta::getFacturas(false, $facturas_id, "credito", false, false, true, $entidad_id, $fecha_recibo);
            else
                return Venta::getFacturas(false, $facturas_id, "credito", false, false, false, $entidad_id, $fecha_recibo);
    }

    public function actionCheckRuc($operacion = "")
    {
        if (!array_key_exists('ruc', $_GET)) return false;
        $ruc = $_GET['ruc'];
        $entidad = Entidad::findOne(['ruc' => $ruc]);
        if ($entidad == null) return false;
        return $entidad->ruc;
    }

    public function actionReciboDetalleMontoValidator($operacion)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $msg = "";
        $monto = $_GET['monto'];
        $factura_id = $_GET['factura_id'];
        if ($factura_id != "") {
            $factura = $operacion == 'venta' ?
                Venta::findOne(['id' => $factura_id]) :
                Compra::findOne(['id' => $factura_id]);
            $saldo = $factura->saldo;
            if ($monto > $saldo) {
                if (strlen($msg) == 0)
                    $msg .= 'El monto no puede ser mayor a ' . number_format($saldo, 2, ',', '.') . '.';
                else
                    $msg .= ' y ' . 'El monto no puede ser mayor a ' . number_format($saldo, 2, ',', '.') . '.';
            }
        }

        return ['result' => '', 'error' => $msg];
    }

    public function actionGetEntidadesFactCredito($operacion, $fecha_hasta)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $resultado = [
            'results' => ['id' => '', 'text' => ''],
        ];

        $tname = $operacion == 'compra' ? Compra::tableName() : Venta::tableName();
        $query = $operacion == 'compra' ? Compra::find() : Venta::find();
        $query->joinWith('entidad as entidad');
        $query->select([
            $tname . '.entidad_id as entidad_id',
            "CONCAT((entidad.razon_social), (' - '), (entidad.ruc)) as text",
            'entidad.id as id'
        ]);
        $entidad_id_incluir = $_GET['entidad_id']; // esto hace que se incluya la factura (de la entidad) del recibo en modificacion.

        $entidades = $query
            ->where([
                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
                'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
                'condicion' => 'credito'])
            ->andWhere(['>', 'saldo', 0.0])
            ->andWhere(['<=', $tname . '.fecha_emision', $fecha_hasta])
            ->orFilterWhere(['entidad.id' => $entidad_id_incluir])
            ->asArray()->all();

        // Es nada mas para eliminar del array, para minimizar algo de tamanho.
        foreach ($entidades as $key => $value) {
            unset($entidades[$key]['entidad']);
        }

        $resultado['results'] = array_values($entidades);

        return $resultado;
    }

    public function actionGetMontoAndCotizacionCurrentDetalle()
    {
        $id_r_d = $_GET['id_recibo_detalle'];
        if (array_key_exists('recibo_id', $_GET)) {
            $id_r = $_GET['recibo_id'];
            $r_d = ReciboDetalle::findOne(['id' => $id_r_d, 'recibo_id' => $id_r]);
            if ($r_d == null) return false;

            if ($r_d->factura_compra_id == $_GET['factura_id'] || $r_d->factura_venta_id == $_GET['factura_id'])
                return $r_d->monto . '|' . $r_d->cotizacion;
            return false;
        }
        return false;
    }

    public function actionGetMontoCurrentDetalleContracta()
    {
        $id_r_d = $_GET['id_recibo_detalle_contracta'];
        if (array_key_exists('recibo_id', $_GET)) {
            $id_r = $_GET['recibo_id'];
            $r_d = ReciboDetalleContracuentas::findOne(['id' => $id_r_d, 'recibo_id' => $id_r]);
            if ($r_d == null) return false;
            return $r_d->monto;
        }
        return false;
    }

    public function actionGetMonedas($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $query = Moneda::find()->alias('moneda');
        $query->leftJoin('core_cotizacion AS cotizacion', 'moneda.id = cotizacion.moneda_id');
        $query_sentence = [
            'moneda.id as id',
            "CONCAT((moneda.id), (' - '), (moneda.nombre)) AS text",
            'moneda.tiene_decimales',
            'cotizacion.fecha as fecha',
            'cotizacion.compra as compra',
            'cotizacion.venta as venta',
        ];

        $query->select($query_sentence)->where([
            'OR',
            ['moneda.id' => 1],
            ['AND',
                ['!=', 'moneda.id', 1],
                ['=', 'cotizacion.fecha', date('Y-m-d', strtotime('-1 day', strtotime($id)))]
            ]
        ]);
        $array = $query->asArray()->all();
        if (sizeof($array) == 0)
            FlashMessageHelpsers::createWarningMessage('Falta cargar la cotización del día');
        $result['results'] = $array;
        return $result;
    }

    /** Validador de numero de recibo, usado por deferred del ReciboNroValidator.php
     *
     */
    public function actionReciboNumeroValidator()
    {
        $result = ['result' => '', 'error' => ''];

        $entidad_id = $_GET['entidad_id'];
        $recibo_nro = $_GET['recibo_nro'];
        $recibo_id = $_GET['recibo_id'];

        $recibo = Recibo::find()
            ->where(['numero' => $recibo_nro, 'entidad_id' => $entidad_id])
            ->andFilterWhere(['!=', 'id', $recibo_id]); // TODO: Ver si permitir repetir nro del mismo proveedor si periodo contable es distinto.

        if ($recibo->exists()) {
            $recibo = $recibo->one();
            $result['error'] = "El número del recibo ya fue usado para la entidad {$recibo->entidad->razon_social} de la empresa {$recibo->empresa->razon_social} en el periodo {$recibo->periodoContable->anho}.";
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionBloquear($id, $operacion)
    {
        $model = Recibo::findOne(['id' => $id]);

        if ($model->bloqueado == 'no') {
            $model->bloqueado = 'si';

            //Solo para validacion

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Recibo ha sido bloqueado.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede bloquear recibo. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index', 'operacion' => $operacion]);
    }

    public function actionDesbloquear($id, $operacion)
    {
        $model = Recibo::findOne(['id' => $id]);

        if ($model->bloqueado == 'si') {
            $model->bloqueado = 'no';

            //Solo para validacion

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Recibo ha sido desbloqueado.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede desbloquear recibo. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index', 'operacion' => $operacion]);
    }

    public function actionSetTotalRecibo($operacion = 'compra')
    {
        try {
            $transaction = Yii::$app->db->beginTransaction();
            {
                if (PermisosHelpers::esSuperUsuario() && Yii::$app->user->identity->username == 'admin') {
                    /** @var Recibo $recibo */
                    foreach (Recibo::find()->all() as $recibo) {
                        if ($recibo->total == '' || round($recibo->total) == 0) {
                            $recibo->total = $recibo->getSumaryDetalles();
                            $recibo->monto_extra = 0;
                            if (!$recibo->save())
                                throw new \Exception("No se puede guardar #{$recibo->id}: {$recibo->getErrorSummaryAsString()}");
                        }
                    }
                } else
                    throw new NotFoundHttpException('The requested page does not exist.');
            }
            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage('Ok');
        } catch (\Exception $exception) {
            (!isset($transaction)) || $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }
        return $this->redirect(['index', 'operacion' => $operacion]);
    }

    /** No se usa... */
    public function actionGetRetencionesFactura()
    {
        $retencion = 0;
        foreach (Yii::$app->session->get('cont_recibo_detalle_retenciones', []) as $field_id => $detalleRetencion) {
            $retencion += intval($detalleRetencion['monto']);
        }
        return $retencion;
    }
}

/**
 * @property string $partida
 * @property string $codigo_cuenta
 * @property string $nombre_cuenta
 * @property string $monto
 * @property string $id_input
 * @property string $multiplier
 * @property string $operador
 */
class AsientoSimulatorData extends Model
{
    public $partida;
    public $codigo_cuenta;
    public $nombre_cuenta;
    public $monto;
    public $id_input;
    public $multiplier;
    public $operador;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['partida', 'codigo_cuenta', 'nombre_cuenta', 'monto', 'id_input', 'multiplier', 'operador'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'partida' => 'Application Name',
            'codigo_cuenta' => 'Frontend Theme',
            'nombre_cuenta' => 'Backend Theme',
            'monto' => 'Cache Class',
            'id_input' => 'Show introduction tour for new users'
        ];
    }
}

/**
 * This is the model class for table "cont_recibo_detalle".
 *
 * @property int $factura_compra_id
 * @property int $factura_venta_id
 * @property string $monto
 *
 * @property string $moneda_nombre
 *
 */
class ReciboDetalleTemp extends ReciboDetalle
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['factura_compra_id', 'factura_venta_id'], 'integer'],
            [['monto'], 'number'],
            [['moneda_nombre'], 'safe'],
        ];
    }
}

/**
 * This is the model class for table "cont_recibo_detalle_contracuenta".
 *
 * @property int $plan_cuenta_id
 * @property string $monto
 *
 * @property string $moneda_nombre
 *
 */
class ReciboDetalleContracuentaTemp extends ReciboDetalleContracuentas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_cuenta_id',], 'integer'],
            [['monto'], 'number'],
        ];
    }
}