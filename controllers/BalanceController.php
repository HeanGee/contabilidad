<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\FlashMessageHelpsers;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\base\Model;
use yii\filters\VerbFilter;

/**
 * VentaController implements the CRUD actions for Venta model.
 */
class BalanceController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param string $anho
     * @param BalanceSumaSaldo|null $model
     * @return array|\yii\db\ActiveRecord[]
     * @throws \Exception
     */
    private function getCuentasFromAsiento($anho, $model = null)
    {
        if (isset($model) && $model->verifyDate()) {
            $rango = $model->rango;
            if ($rango != '') {
                $slices = explode(' - ', $rango);
                $desde = implode('-', array_reverse(explode('-', $slices[0])));
                $hasta = implode('-', array_reverse(explode('-', $slices[1])));
            } else {
                $desde = "{$anho}-01-01";
                $hasta = "{$anho}-12-31";
            }
        } else {
            $desde = "{$anho}-01-01";
            $hasta = "{$anho}-12-31";
        }

        $cuentasDebeToString = implode(', ', PlanCuenta::INIT_COD_CUENTA_DEBE);
        $cuentasHaberToString = implode(', ', PlanCuenta::INIT_COD_CUENTA_HABER);
        $query = AsientoDetalle::find()->alias('detalle')
            ->joinWith('asiento as asiento')
            ->where([
                'asiento.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                'asiento.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')
            ])
            ->leftJoin('cont_plan_cuenta cuenta', 'cuenta.id = detalle.cuenta_id')
            ->andWhere(['BETWEEN', 'asiento.fecha', $desde, $hasta])
            ->select([
                'cuenta_id' => 'MIN(cuenta_id)',
                'cuenta_codigo' => 'MIN(cod_completo)',
                'cuenta_nombre' => 'MIN(cuenta.nombre)',
                'debe' => 'SUM(detalle.monto_debe)',
                'haber' => 'SUM(detalle.monto_haber)',
                'saldo_debe' => 'IF((SUM(detalle.monto_debe)-SUM(ABS(detalle.monto_haber)) > 0), SUM(detalle.monto_debe)-SUM(ABS(detalle.monto_haber)), 0)',
                'saldo_haber' => 'IF((SUM(ABS(detalle.monto_haber))-SUM(detalle.monto_debe) > 0), SUM(ABS(detalle.monto_haber))-SUM(detalle.monto_debe), 0)',

                'asiento_id' => "MIN(asiento.asiento_id)", // exige que exista asiento_id asi que cualquier cosa se le carga.
            ])
            ->groupBy('cuenta_id')
            ->orderBy('cod_ordenable ASC');
        if (!$query->exists()) {
            throw new \Exception("No se ha encontrado ningún asiento.");
        }

        return $query->asArray()->all();
    }

    private function renderPdf($data, $anho)
    {
        $path = 'modules/contabilidad/views/reporte/balance/suma-y-saldo';
//                    $path = str_replace('/', '\\', $path);
        $cssInline = Yii::$app->view->renderFile("@app/{$path}/css.css");
        $contenido = Yii::$app->view->renderFile("@app/{$path}/pdf.php", ['data' => $data]);
        $encabezado = Yii::$app->view->renderFile("@app/{$path}/header.php", ['anho' => $anho]);
        $pie = Yii::$app->view->renderFile("@app/{$path}/pie.php", []);
        $currentDate = strftime('%d-%m-%Y a las %H:%M:%S', time());

        $pdf = new Pdf([
            'mode' => Pdf::DEST_BROWSER,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'format' => Pdf::FORMAT_FOLIO,
            'marginTop' => 37,
            'marginLeft' => 7,
            'marginBottom' => 15,
            'marginRight' => 7,
            'content' => $contenido,
            'cssInline' => $cssInline,
//                    'destination' => Pdf::DEST_BROWSER,
            'filename' => "Reporte Balance Sumas y Saldos {$currentDate}.pdf",
            'options' => [
                'title' => "",
                'subject' => "",
                'defaultheaderline' => 0,
                'defaultfooterline' => 1,
            ],
            'methods' => [
                'SetHeader' => [$encabezado],
                'SetFooter' => [$pie],
            ]
        ]);
        setlocale(LC_TIME, 'es_ES.UTF-8');
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    private function renderXls($data, $anho)
    {
        // Crear objetoExcel.
        /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        date_default_timezone_set('America/Asuncion');

        /** Create a new Spreadsheet Object **/
        $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/reporte/balance/suma-y-saldo/';
        $objPHPExcel = IOFactory::load($carpeta . 'ReporteBalanceSumaSaldo.xls');

        $fila = 6;
        $desde = "01-01-{$anho}";
        $hasta = "31-12-{$anho}";
        $empresa = Empresa::findOne(\Yii::$app->session->get('core_empresa_actual'));

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, $desde);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, $hasta);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 2, $empresa->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 3, "{$empresa->ruc}-{$empresa->digito_verificador}");

        foreach ($data as $item) {
            $column = 1;
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column++, $fila)->setValueExplicit($item['cuenta_codigo'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column++, $fila)->setValueExplicit($item['cuenta_nombre'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column++, $fila)->setValueExplicit($item['debe'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column++, $fila)->setValueExplicit($item['haber'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column++, $fila)->setValueExplicit($item['saldo_debe'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column++, $fila)->setValueExplicit($item['saldo_haber'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);

            $fila++;
        }

        //Versión excel
        $carpeta = Yii::$app->basePath . '/web/uploads/';
        if (!is_dir($carpeta)) {
            if (!@mkdir($carpeta, 0777, true)) {
                throw new \Exception('No se puede crear carpeta.');
            }
        }
        Yii::$app->session->set('carpeta', $carpeta);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xls');
        $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xls");

        // Exportar en navegador
        $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xls");
        $OS = strtolower(empty($_SERVER['HTTP_USER_AGENT']) ? '' : $_SERVER['HTTP_USER_AGENT']);
        if (!$OS)
            $OS_IS_MAC = true;
        else
            $OS_IS_MAC = (strpos($OS, 'mac') !== false)
                || (strpos($OS, 'macintosh') !== false)
                || (strpos($OS, 'iphone') !== false)
                || (strpos($OS, 'ipad') !== false);

        if ($OS_IS_MAC) {
//            if(!file_exists($target_relative)){
//                copy($filename,$target_relative);
//                sleep(1);
//            }
//            header('Location: ' . $target);
        } else {
            $currentDate = strftime('%d-%m-%Y a las %H:%M:%S', time());
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . 'Reporte Balance Sumas y Saldos ' . $currentDate . '.' . $fi['extension']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xls"));
            ob_clean();
            flush();
            readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xls");
        }
//borramos el fichero temporal
        unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xls");
    }

    public function actionSumasYSaldos()
    {
        $model = new BalanceSumaSaldo();

        if ($model->load(Yii::$app->request->post())) {
            try {
                $anho = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'))->anho;

                $data = self::getCuentasFromAsiento($anho, $model);
                self::fixPartidaForStaticCtas($data);

                if ($model->tipo_doc == 'pdf') {
                    return $this->renderPdf($data, $anho);
                } else {
                    return $this->renderXls($data, $anho);
                }

            } catch (\Exception $exception) {
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('/reporte/balance/suma-y-saldo/reporte', ['model' => $model]);
    }

    /**
     * @param array $data
     * @return mixed|array
     */
    private function fixPartidaForStaticCtas(&$data)
    {
        foreach ($data as $key => $item) {
            $saldoDebe = (int)$item['saldo_debe'];
            $saldoHaber = (int)$item['saldo_haber'];
            $primerCod = (int)explode('.', $item['cuenta_codigo'])[0];

            if (in_array($primerCod, PlanCuenta::INIT_COD_CUENTA_DEBE))
                if ($saldoDebe == 0)
                    list($saldoDebe, $saldoHaber) = array(-$saldoHaber, $saldoDebe);  #Swap without temp var: https://www.zigpress.com/2018/03/16/swap-two-variables-in-php/
            if (in_array($primerCod, PlanCuenta::INIT_COD_CUENTA_HABER))
                if ($saldoHaber == 0)
                    list($saldoDebe, $saldoHaber) = array($saldoHaber, -$saldoDebe);  #Swap without temp var: https://www.zigpress.com/2018/03/16/swap-two-variables-in-php/

            $data[$key]['saldo_debe'] = $saldoDebe;
            $data[$key]['saldo_haber'] = $saldoHaber;
        }
        return $data;
    }


    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}

/**
 * @property string $tipo_doc
 * @property string $rango
 */
class BalanceSumaSaldo extends Model
{
    public $tipo_doc;
    public $rango;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_doc', 'rango'], 'string'],
        ];
    }

    public function verifyDate()
    {
        return preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{4}\ -\ [0-9]{2}-[0-9]{2}-[0-9]{4}$/', $this->rango);
    }
}