<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * PlantillaCompraventaDetalleController implements the CRUD actions for PlantillaCompraventaDetalle model.
 */
class PlantillaCompraventaDetalleController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    private function getElementAndPosition($array, $key, $value)
    {
        $poss = -1;
        foreach ($array as $pos => $content) {
            if ($content instanceof PlantillaCompraventaDetalle) {
                if ($content->$key == $value) {
                    $poss = $pos;
                    break;
                }
            }
        }

        $result = [];
        if ($poss > -1)
            $result[] = $array[$poss];
        else
            $result[] = null;
        $result[] = $poss;

        return $result;
    }

    private function findPosition($array, $item)
    {
        foreach ($array as $pos => $content) {
            if ($content instanceof PlantillaCompraventaDetalle) {
                if ($content === $item) {
                    return $pos;
                }
            }
        }

        return -1;
    }

    public function actionAddDetalle($tipo_asiento, $submit = false)
    {
        $model = new PlantillaCompraventaDetalle();
        $model->tipo_asiento = $tipo_asiento;
        $model->cta_principal = 'no';

        $session = Yii::$app->session;

//        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return ActiveForm::validate($model);
//        }

        if ($model->load(Yii::$app->request->post()) && $submit != false) {
            $dataProvider = null;
            if ($tipo_asiento == 'venta' || $tipo_asiento == 'compra') $dataProvider = \Yii::$app->session->get('cont_detalleplantilla-provider');
            elseif ($tipo_asiento == 'costo_venta') $dataProvider = \Yii::$app->session->get('cont_detalleplantilla_costoventa-provider');
            $data = ($dataProvider != null) ? $dataProvider->allModels : [];

            //controlar que no haya datos de otro tipo de asiento
            $i = 0;
            $limit = sizeof($data);
            while ($i < $limit) {
                if ($data[$i]->tipo_asiento != $tipo_asiento) {
                    unset($data[$i]);
                }
                $i++;
            }

            array_push($data, $model);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);

            $session_key = ($tipo_asiento == 'venta' || $tipo_asiento == 'compra') ? 'cont_detalleplantilla-provider' : (($tipo_asiento == 'costo_venta') ? 'cont_detalleplantilla_costoventa-provider' : '');

            $session->set($session_key, $dataProvider);
        };

        Yii::$app->response->format = Response::FORMAT_JSON;
        $view = '-no view-';
        if ($tipo_asiento == 'venta' || $tipo_asiento == 'compra')
            $view = '_modal_create';
        elseif ($tipo_asiento == 'costo_venta')
            $view = '_modal_costoventa_create';

        return $this->renderAjax($view, ['model' => $model, 'tipo_asiento' => $tipo_asiento]);
    }

// todo: ver si se puede no usar session.
    public function actionModificarDetalle($index, $tipo_asiento, $submit = false)
    {
        $session = Yii::$app->session;
        $dataProvider = null;
        if ($tipo_asiento == 'venta' || $tipo_asiento == 'compra') $dataProvider = $session['cont_detalleplantilla-provider'];
        elseif ($tipo_asiento == 'costo_venta') $dataProvider = $session['cont_detalleplantilla_costoventa-provider'];
        $data = ($dataProvider != null) ? $dataProvider->allModels : [];
        $model = $data[$index];

//        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return ActiveForm::validate($model);
//        }

        if ($model->load(Yii::$app->request->post()) && $submit != false) {
            $data[$index] = $model;
//            // guardar en BD
//            if ($data[$index] != null) {
//                if ($data[$index]->id != null)
//                    $data[$index]->save();
//            }

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);

            $session_key = ($tipo_asiento == 'venta' || $tipo_asiento == 'compra') ? 'cont_detalleplantilla-provider' : (($tipo_asiento == 'costo_venta') ? 'cont_detalleplantilla_costoventa-provider' : '');

            $session->set($session_key, $dataProvider);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $view = '-no view-';
        if ($tipo_asiento == 'venta' || $tipo_asiento == 'compra')
            $view = '_modal_create';
        elseif ($tipo_asiento == 'costo_venta')
            $view = '_modal_costoventa_create';

        return $this->renderAjax($view, ['model' => $model, 'tipo_asiento' => $tipo_asiento]);
//        return $this->renderAjax($view, [
//            'model' => $model,
//        ]);
    }

// todo: al borrar, no es suficiente con id ni index, index ya no sirve porque se separa el arrayprovider de la session en dos providers.

    /**
     * @param $indice
     *
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBorrarDetalle($index, $tipo_asiento = null)
    {
        /** @var PlantillaCompraventaDetalle[] $data */

        $session = Yii::$app->session;
        $dataProvider = null;
        if ($tipo_asiento == 'venta' || $tipo_asiento == 'compra') $dataProvider = $session['cont_detalleplantilla-provider'];
        elseif ($tipo_asiento == 'costo_venta') $dataProvider = $session['cont_detalleplantilla_costoventa-provider'];
        $data = $dataProvider->allModels;

//        if ($data[$index]->id != null) {
//            $detalle = $data[$index];
//            if (!$detalle->plantilla->hasAnyAssociation(true)) {
//                if (PlantillaCompraventaDetalle::find()->where(['id' => $detalle->id])->exists()) {
//                    $detalle->delete();
//                    unset($data[$index]);
//                }
//            } else {
//                FlashMessageHelpsers::createWarningMessage('Esta plantilla tiene asociaciones. Por tanto sus detalles no pueden ser borradas.');
//            }
//
//        } else
//            unset($data[$index]);

        unset($data[$index]);

        $data = array_values($data);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        $session_key = ($tipo_asiento == 'venta' || $tipo_asiento == 'compra') ? 'cont_detalleplantilla-provider' : (($tipo_asiento == 'costo_venta') ? 'cont_detalleplantilla_costoventa-provider' : '');

        $session->set($session_key, $dataProvider);

        return true;
    }

    /**
     * Finds the PlantillaCompraventaDetalle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $id
     *
     * @return PlantillaCompraventaDetalle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = PlantillaCompraventaDetalle::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}
