<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\search\ParametroSistemaSearch;
use common\helpers\FlashMessageHelpsers;
use Exception;
use Throwable;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ParametroSistemaController implements the CRUD actions for ParametroSistema model.
 */
class ParametroSistemaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    private function concepts()
    {
        $concepts = [];
        $concepts ['todos'] = "Todos";
        $concepts ["fact_venta_paralelo$||regex"] = "Carga de Factura en paralelo";
        $concepts ["cta_resultado_diff_cambio||regex"] = "Cuentas de Resultado por Diferencia de Cambio";
        $concepts ["(cuenta_ganado|cuenta_costo_venta_gnd|cuenta_costo_venta_gd|cuenta_procreo|cuenta_mortandad_gnd|cuenta_mortandad_gd|cuenta_consumo_gnd|cuenta_consumo_gd|cuenta_valorizacion_hacienda)||regex"] = "Cuentas para Cierre de Inventario de Activos Biológicos";
        $concepts ["coeficiente_costo||regex"] = "Coeficiente de costo";
        $concepts ["^core_empresa-([0-9]+)-periodo-([0-9]+)-||regex"] = "Cuentas para Préstamo";
        $concepts ["seguro_a_devengar_id$||regex"] = "Seguro a Devengar";
        $concepts ["tipodoc_set_retencion$||regex"] = "Tipo de Documento SET para Retenciones";

        return $concepts;
    }

    /**
     * Lists all ParametroSistema models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParametroSistemaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'concepts' => self::concepts(),
        ]);
    }

    /**
     * Displays a single ParametroSistema model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ParametroSistema model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ParametroSistema();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ParametroSistema model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ParametroSistema model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ParametroSistema model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParametroSistema the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParametroSistema::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionDeleteParametro()
    {
        if (!(Yii::$app->user->identity->getId() == 1)) {
            return $this->redirect(['index']);
        }

        if (Yii::$app->request->isGet) {
            return $this->renderAjax('_modal-delete-parametro', []);
        } else {
            try {
                $postAttribs = $_POST['kvform'];

                if (!isset($postAttribs['parametro']) || $postAttribs['parametro'] == '')
                    FlashMessageHelpsers::createInfoMessage("No se ha especificado ningún parámetro");
                else {
                    $transaction = Yii::$app->db->beginTransaction();
                    $value = trim($postAttribs['parametro']);
                    $exists = false;
                    /** @var ParametroSistema $parm */
                    foreach (ParametroSistema::find()->where(['LIKE', 'nombre', "%{$value}", false])->all() as $parm) {
                        if (!$parm->delete())
                            throw new Exception("No se pudo borrar el parámetro {$parm->nombre}: {$parm->getErrorSummaryAsString()}");
                        $exists || $exists = true;

                    }

                    $transaction->commit();
                    if ($exists)
                        FlashMessageHelpsers::createSuccessMessage("Todos los parámetros que terminan en {$value} se han eliminado correctamente.");
                    else
                        FlashMessageHelpsers::createInfoMessage("NO se ha encontrado ningún parámetro que se parezca a \"{$value}\"");
                }
            } catch (Exception $exception) {
                FlashMessageHelpsers::createErrorMessage($exception->getMessage(), 15000);
                !isset($transaction) || $transaction->rollBack();
            } catch (Throwable $throwable) {
                FlashMessageHelpsers::createErrorMessage($throwable->getMessage(), 15000);
                !isset($transaction) || $transaction->rollBack();
            }
        }

        return $this->redirect(['index']);
    }
}
