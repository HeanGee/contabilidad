<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Cotizacion;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\search\DetalleVentaSearch;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * DetalleVentaController implements the CRUD actions for DetalleVenta model.
 */
class DetalleVentaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DetalleVenta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DetalleVentaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DetalleVenta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DetalleVenta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DetalleVenta();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DetalleVenta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DetalleVenta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DetalleVenta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DetalleVenta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DetalleVenta::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    /** ************************** Utilizado externamente ************************** **/

    public function actionAddCuenta($submit = false)
    {
        $model = new DetalleVenta();
        $model->cta_principal = 'no';
        $session = Yii::$app->session;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $dataProvider = \Yii::$app->session->get('cont_detalleventa-provider');
            $data = $dataProvider->allModels;
            array_push($data, $model);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);

            $session->set('cont_detalleventa-provider', $dataProvider);
        };

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('_form_modal', [
            'model' => $model,
        ]);
    }

    /**
     * @param $indice
     * @param string $cuenta
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBorrarCuenta($indice)
    {
        $session = Yii::$app->session;

        $dataProvider = \Yii::$app->session->get('cont_detalleventa-provider');
        $data = $dataProvider->allModels;

//        if($data[$indice]->id != null) {
//            if(DetalleVenta::find()->where(['id' => $data[$indice]->id])->exists())
//                DetalleVenta::findOne(['id' => $data[$indice]->id])->delete();
//        }

        unset($data[$indice]);
        $data = array_values($data);
        $data = DetalleVenta::agruparCuentas($data);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        $session->set('cont_detalleventa-provider', $dataProvider);

        return true;
    }

    public function actionModificarCuenta($indice, $submit = false)
    {
        $session = Yii::$app->session;
        $dataProvider = $session['cont_detalleventa-provider'];
        $data = $dataProvider->allModels;
        $model = $data[$indice];

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $data[$indice] = $model;
            if ($data[$indice] != null) {
                if ($data[$indice]->id != null)
                    $data[$indice]->save();
            }

            $dataProvider = new ArrayDataProvider([
                'allModels' => DetalleVenta::agruparCuentas($data),
                'pagination' => false,
            ]);

            Yii::$app->response->format = Response::FORMAT_JSON;
            $session->set('cont_detalleventa-provider', $dataProvider);
            return 8;
        }

        return $this->renderAjax('_form_modal', [
            'model' => $model,
        ]);
    }

    public function actionGetCotizacion()
    {
        $existe = array_key_exists('fecha_emision', $_POST) && array_key_exists('moneda_id', $_POST);
        if($existe) {
            $fecha_emision = $_POST['fecha_emision'];
            // date('Y-m-d', strtotime('-1 day', strtotime('2015-08-10')))
            $fecha_emision = date_create_from_format('d-m-Y', $fecha_emision);
            if($fecha_emision) {
                $fecha_emision = $fecha_emision->format('Y-m-d');
                $fecha = date('Y-m-d', strtotime('-1 day', strtotime($fecha_emision)));
                $moneda_id = $_POST['moneda_id'];
                $query = Cotizacion::find()->where(['fecha' => $fecha, 'moneda_id' => $moneda_id])->one();
//                echo $query != null ? $query->compra : ''; exit();
                return $query != null ? $query->compra : '';
            }
        }

        return false;
    }
}
