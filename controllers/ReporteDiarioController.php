<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\Asiento;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use common\helpers\FlashMessageHelpsers;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\base\Model;
use yii\filters\VerbFilter;

/**
 * VentaController implements the CRUD actions for Venta model.
 */
class ReporteDiarioController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionGetParams()
    {
        $model = new ReporteDiario();
        $model->paper_size = Pdf::FORMAT_A4;
        $model->orientacion = Pdf::ORIENT_LANDSCAPE;

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->paper_size == '' && $model->tipo_reporte) {
                FlashMessageHelpsers::createWarningMessage('Indique un tamaño de hoja.');
                goto retorno;
            }

            if ($model->orientacion == '' && $model->tipo_reporte) {
                FlashMessageHelpsers::createWarningMessage('Indique orientación de hoja.');
                goto retorno;
            }

            $data = Asiento::find();
            $data->andWhere(['empresa_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);

            $desde = '';
            $hasta = '';
            if ($model->fecha_rango != '') {
                $rango = explode(' - ', $model->fecha_rango);
                $desde = $rango[0];
                $hasta = $rango[1];

                $desde = date_create_from_format('d-m-Y', $desde);
                $hasta = date_create_from_format('d-m-Y', $hasta);

                if ($desde && $hasta) {
                    $desde = $desde->format('Y-m-d');
                    $hasta = $hasta->format('Y-m-d');

                    $data->andFilterWhere(['between', 'fecha', $desde, $hasta]);
                } else {
                    FlashMessageHelpsers::createWarningMessage('Rango de fecha incorrecto.');
                    goto retorno;
                }
            }

            $data = $data->all();
            $count = count($data);
            if ($count == 0) {
                FlashMessageHelpsers::createWarningMessage('Sin asientos en el período.');
                goto retorno;
            }
            if ($count > 150) {
                ini_set('memory_limit', (int)ceil($count * 0.9) . 'M');
            }

            if ($model->tipo_reporte) {
                $path = 'modules\contabilidad\views\reporte-diario\templates';
                $path = str_replace('\\', '/', $path);
                $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/reporte.css");
                $contenido = Yii::$app->view->renderFile("@app/" . $path . "/reporte.php", ['data' => $data]);
                //            $encabezado = '';
                $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/reporte_header.php", ['data' => $data, 'del' => $desde, 'al' => $hasta]);
                $pie = Yii::$app->view->renderFile("@app/" . $path . "/reporte_pie.php", []);
                $pdf = new Pdf([
                    'mode' => Pdf::DEST_BROWSER,
                    'orientation' => $model->orientacion,
                    'format' => $model->paper_size,
                    'marginTop' => 37,
                    'marginLeft' => 7,
                    'marginBottom' => 17,
                    'marginRight' => 7,
                    'content' => $contenido,
                    'cssInline' => $cssInline,
                    //                    'destination' => Pdf::DEST_BROWSER,
                    'filename' => "Reporte Libro diario.pdf",
                    'options' => [
                        'title' => "",
                        'subject' => "",
                        //                        'defaultheaderline' => false
                    ],
                    'methods' => [
                        'SetHeader' => [$encabezado],
                        //                    'SetFooter' => ['|Página {PAGENO} / {nb}|'],
                        'SetFooter' => [$pie],
                    ],
                ]);

                setlocale(LC_TIME, 'es_ES.UTF-8');
                Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
                Yii::$app->response->headers->add('Content-Type', 'application/pdf');
                return $pdf->render();
            } else {
                $this->generateExcelReport($data, $desde, $hasta);
            }

        }
        retorno:;
        return $this->render('reporte-libro-diario', [
            'model' => $model,
        ]);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    public function getNoRequierenEmpresa()
    {
        return [];
    }

    /** -------------------------------------------
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function generateExcelReport($data, $de, $al)
    {
        $items = 0;
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $empresa = Empresa::findOne($empresa_id);
        $empresaNombre = 'Sin empresa seleccionada';
        $empresa_id = null;

        $dataSize = sizeof($data);

        if ($empresa) {
            $empresaNombre = $empresa->nombre;
            $empresa_id = $empresa->id;
        }

        // Crear objetoExcel.
        /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        date_default_timezone_set('America/Asuncion');

        // estilo de totales
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        // linea punteada
        $lineaPunteada = [
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED,
                ],
            ],
        ];

        /** Create a new Spreadsheet Object **/
        $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/reporte-diario/templates/';
        $objPHPExcel = IOFactory::load($carpeta . 'libro_diario_template.xls');

        //Rellenar Excel.
        $fila_titulo = 6;
        $fila = 9;
        $columna = 2;
        $columnaValor = 3;

        $currentDate = date('d-m-Y');

        $dataSize = sizeof($data);
        $fecha_desde = $dataSize ? $data[0]->fecha : null;
        $fecha_hasta = $dataSize ? $data[0]->fecha : null;
        $list = array_slice($data, 1, ($dataSize - 1));
        foreach ($list as $item) {
            if ($item->fecha > $fecha_hasta) {
                $fecha_hasta = $item->fecha;
            } elseif ($item->fecha < $fecha_desde) {
                $fecha_desde = $item->fecha;
            }

        }

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna - 1, $fila_titulo, date_create_from_format('Y-m-d', $de ? $de : $fecha_desde)->format('d-m-Y'));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila_titulo, date_create_from_format('Y-m-d', $al ? $al : $fecha_hasta)->format('d-m-Y'));
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 1, $fila_titulo, Yii::$app->user->identity->username);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 2, $fila_titulo, $currentDate);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 3, $fila_titulo, Empresa::findOne(['id' => \Yii::$app->session->get('core_empresa_actual')])->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 4, $fila_titulo, EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')])->anho);

        try {
            foreach ($data as $currentAsiento) {
                if (($currentAsiento instanceof Asiento)) {
                    $currentAsientoId = $currentAsiento->id;
                    $currentAsientoFecha = $currentAsiento->fechaFormatted;
                    $currentAsientoConcepto = $currentAsiento->concepto;

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila, $currentAsientoId);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 1, $fila, $currentAsientoFecha);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 2, $fila, $currentAsientoConcepto);

                    $contador = 0;
                    $fila_desde = $fila + 1;
                    foreach ($currentAsiento->asientoDetalles as $detalle) {
                        $contador++;
                        $desc = $detalle->monto_debe > 0 ? $detalle->cuentaNombreDebe : $detalle->cuentaNombreHaber;
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 2, ++$fila, $desc);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columna + 3, $fila, $detalle->monto_debe, DataType::TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columna + 4, $fila, $detalle->monto_haber, DataType::TYPE_NUMERIC);
                        if ($contador == sizeof($currentAsiento->asientoDetalles)) {
                            $fila++;
                        }

                    }
                    $fila_hasta = $fila - 1;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna - 1, $fila, 'Suma:');
                    $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columna + 3, $fila, "=SUM(E{$fila_desde}:E{$fila_hasta})", DataType::TYPE_FORMULA);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow($columna + 4, $fila, "=SUM(F{$fila_desde}:F{$fila_hasta})", DataType::TYPE_FORMULA);
                    $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna - 1, $fila)->getCoordinate())
                        ->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna + 4, $fila)->getCoordinate())
                        ->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna + 3, $fila)->getCoordinate())
                        ->applyFromArray($styleArray);
                    //linea punteada
                    for ($i = $columna - 1; $i <= 6; $i++) {
                        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i, $fila)->getCoordinate())
                            ->applyFromArray($lineaPunteada);
                    }

                    $fila++;
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }

        //Versión excel
        $carpeta = Yii::$app->basePath . '/web/uploads/';
        if (!is_dir($carpeta)) {
            if (!@mkdir($carpeta, 0777, true)) {
                throw new \Exception('No se puede crear carpeta.');
            }
        }
        Yii::$app->session->set('carpeta', $carpeta);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Exportar en navegador
        $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . 'libro_diario_' . $currentDate . '_' . $empresaNombre . '.' . $fi['extension']);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx"));
        ob_clean();
        flush();
        readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        //borramos el fichero temporal
        unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Al index pero con el modelo
        $this->verificarSesion();

        return $this->redirect(['index']);

    }
}

/**
 * @property string $paper_size
 * @property string $fecha_rango
 * @property string $orientacion
 * @property boolean $tipo_reporte
 */
class ReporteDiario extends Model
{
    public $paper_size;
    public $fecha_rango;
    public $orientacion;
    public $tipo_reporte;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paper_size', 'fecha_rango', 'orientacion', 'tipo_reporte'], 'safe'],
            [['paper_size', 'fecha_rango', 'orientacion'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paper_size' => 'Tamaño de hoja',
            'fecha_rango' => 'Filtrar por Fecha Rango',
            'orientacion' => 'Orientación de hoja',
            'tipo_reporte' => 'Tipo de Reporte',
        ];
    }

    public static function getPaperSizes()
    {
        $array = [];
        $i = 1;
        // (a4, oficio, oficiopy)
        $array[Pdf::FORMAT_A4] = ($i++) . " - A4";
        $array[Pdf::FORMAT_FOLIO] = ($i++) . " - Oficio";
        return $array;
    }

    public static function getOrientaciones()
    {
        $array = [];
        $i = 1;
        // (horizontal, vertical)
        $array[Pdf::ORIENT_LANDSCAPE] = ($i++) . " - Horizontal";
        $array[Pdf::ORIENT_PORTRAIT] = ($i++) . " - Vertical";
        return $array;
    }

}
