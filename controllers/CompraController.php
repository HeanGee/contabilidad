<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Cotizacion;
use backend\models\Iva;
use backend\models\Moneda;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\ActivoFijoAtributo;
use backend\modules\contabilidad\models\ActivoFijoTipoAtributo;
use backend\modules\contabilidad\models\auxiliar\TimbradoDetalle2;
use backend\modules\contabilidad\models\auxiliar\TimbradoDetalleUpdate;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\Poliza;
use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\PrestamoDetalle;
use backend\modules\contabilidad\models\PrestamoDetalleCompra;
use backend\modules\contabilidad\models\ReciboDetalle;
use backend\modules\contabilidad\models\search\CompraSearch;
use backend\modules\contabilidad\models\Timbrado;
use backend\modules\contabilidad\models\TimbradoDetalle;
use backend\modules\contabilidad\models\TipoDocumento;
use common\helpers\FlashMessageHelpsers;
use common\helpers\ParametroSistemaHelpers;
use common\helpers\PermisosHelpers;
use DateTime;
use kartik\helpers\Html;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CompraController implements the CRUD actions for Compra model.
 */
class CompraController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view', 'bloquear', 'desbloquear'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Compra $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Compra $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['bloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Compra $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['desbloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var Compra $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'si';
                        }
                    ],
                ],
            ],
        ];
    }

    private function clearSession()
    {
        Yii::$app->getSession()->remove('debug');
        Yii::$app->getSession()->remove('cont_detallecompra_sim-provider');
        Yii::$app->getSession()->remove('cont_monto_totales_compra');
        Yii::$app->getSession()->remove('cont_detallecompra-provider');
//        Yii::$app->getSession()->remove('cont_compra_valores_usados');
        Yii::$app->getSession()->remove('cont_poliza_desde_compra');
        Yii::$app->getSession()->remove('cont_actfijo_desde_factura');
        Yii::$app->getSession()->remove('cont_actfijo_pointer');
        Yii::$app->getSession()->remove('cont_prestamo_desde_compra');
        Yii::$app->getSession()->remove('cont_prestamod_desde_compra');
        Yii::$app->getSession()->remove('cont_fila_plantilla');
        Yii::$app->getSession()->remove('cont_prestamos_compra');
        Yii::$app->getSession()->remove('cont_prestamo_cuotas');
        Yii::$app->getSession()->remove('cont_compra_actual_id');
        Yii::$app->getSession()->remove('cont_compra_actual_entidad_id');
        Yii::$app->getSession()->remove('cont_compra_valor_para_exenta');
        Yii::$app->getSession()->remove('cont_compra_actual_entidad_ruc');
        Yii::$app->getSession()->remove('cont_prestamo_facturar');
        Yii::$app->getSession()->remove('cont_compra_actual_id');
        Yii::$app->getSession()->remove('cont_compra_actual_entidad_ruc');
        Yii::$app->getSession()->remove('cont_prestamo_deducible');
        Yii::$app->getSession()->remove('cont_values_for_plantilla_fields');
        Yii::$app->getSession()->remove('cont_activo_bio_stock_manager');
        Yii::$app->getSession()->remove('cont_activo_bio_stock_delta');
        Yii::$app->getSession()->remove('cont_act_bio_valor_exenta');
        Yii::$app->getSession()->remove('cont_act_bio_compra_fila_id');
        Yii::$app->getSession()->remove('cont_act_bio_compra_deducible');
        Yii::$app->getSession()->remove('cont_act_bio_compra_campo_iva_activo');
        Yii::$app->getSession()->remove('cont_compra_actfijo_moneda_id');
        Yii::$app->getSession()->remove('cont_afijo_atributos');
        Yii::$app->getSession()->remove('cont_compra_actfijo_tipo_id');
        Yii::$app->getSession()->remove('cont_compra_valor_para_plantilla');
        Yii::$app->getSession()->remove('costo_adq_desde_factura');
        Yii::$app->getSession()->remove('$compra_id');
    }

    /**
     * Lists all Compra models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompraSearch();
        $searchModel->empresa_id = Yii::$app->session->get(SessionVariables::empresa_actual);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 50];

        //borrar variables de session
        self::clearSession();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Compra model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCargarDetalleHaber($suma = null)
    {
        $tipo_docu_id = $_POST['tipo_docu_id'];
        if ($suma != null) {
            $total_factura = $suma;
        } else {
            $total_factura = $_POST['cont_factura_total'];
        }
        $cuenta_haber = TipoDocumento::findOne(['id' => $tipo_docu_id])->cuenta;

        /** Al elegir tipo_documento, SIEMPRE se trae 1 sola cuenta. */
        $detalle = new DetalleCompra();
        $detalle->cta_contable = 'haber';
        $detalle->plan_cuenta_id = $cuenta_haber->id;
        $detalle->subtotal = $total_factura; // TODO: averiguar que valor debe tomar, dependiendo del tipo_documento.

        $allModels = array();
        array_push($allModels, $detalle);
        // Crear detalle y guardar en la sesion
        $detallesProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => false,
        ]);
        Yii::$app->getSession()->set('cont_detallecompra-provider', $detallesProvider);
        return true;
    }

    /**
     * Contruye la tabla para asiento
     *
     * Entra: mercaderia, iva_cred_fiscal
     * Sale: Caja
     *
     * @param float $total_factura
     * @param int $iva_porc
     * @return array
     */
    private function detalleCompraMercaderia($total_factura = 0.0, $iva_porc = 10)
    {
        $detalles = [];
        $cuentas_entrantes = trim(ParametroSistemaHelpers::getValorByNombre('ctas_in_compra_merca_diez'));
        $cuentas_salientes = trim(ParametroSistemaHelpers::getValorByNombre('ctas_out_compra_merca_diez'));

        // Caja. Sale.
        $sale = new DetalleCompra();
        $sale->plan_cuenta_id = PlanCuenta::findOne(['cod_completo' => explode('|', $cuentas_salientes)[0]])->id;
        $sale->subtotal = $total_factura;
        $sale->cta_contable = 'haber';

        // Compras. Entra. Debe ser igual al monto de la factura.
        $entra = new DetalleCompra();
        $entra->iva_id = Iva::findOne(['porcentaje' => $iva_porc])->id;
        $entra->plan_cuenta_id = PlanCuenta::findOne(['cod_completo' => explode('|', $cuentas_entrantes)[0]])->id;
        $entra->subtotal = $total_factura / 1.1; // caja = 110 % con iva cfiscal = 10 %
        $entra->cta_contable = 'debe';

        // Iva credito fiscal. Entra.
        $entra2 = new DetalleCompra();
        $entra2->plan_cuenta_id = PlanCuenta::findOne(['cod_completo' => explode('|', $cuentas_entrantes)[1]])->id;
        $entra2->subtotal = $total_factura / 1.1 * 0.1; // iva credito fiscal = 10%
        $entra2->cta_contable = 'debe';

        array_push($detalles, $entra);
        array_push($detalles, $entra2);
        array_push($detalles, $sale);

        return $detalles;
    }

    /**
     * Calcula las columnas del detalle en base al monto total de la factura.
     */
    public function actionCalcDetalle()
    {
        // Obtener variables del POST
        $total_factura = $_POST['cont_factura_total'];
        $porcentajes_iva = $_POST['ivas'];
        $plantillas = $_POST['plantillas'];
        $tipo_docu_id = $_POST['tipo_docu_id'];
        $moneda_id = $_POST['moneda_id'];

        // Verificar tipo de documento
        $tipoDoc = TipoDocumento::findOne(['id' => $tipo_docu_id]);
        if ($tipoDoc == null) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar Tipo de Documento');
            return false;
        }

        // Verificar que se ha seleccionado una plantilla.
        $plantillasModel = [];
        $totales_valor = [];
        foreach ($plantillas as $pl) {
            //Valores por iva
            $valores_iva = [];
            foreach ($porcentajes_iva as $iva) {
                array_push($valores_iva, $pl['iva_' . $iva]);
            }

            $plantilla = PlantillaCompraventa::findOne(['id' => $pl['plantilla_id']]);
            if ($plantilla != null) {
                array_push($plantillasModel, $plantilla);
                $totales_valor[$plantilla->id] = $valores_iva;
            }
        }

        if (!isset($plantillasModel)) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar PLANTILLA');
            return false;
        }

        // Verificar que la plantilla seleccionada tenga detalles.
        /** @var PlantillaCompraventa $plantilla */
        foreach ($plantillasModel as $plantilla) {
            if (!$plantilla->esParaPrestamo() && !$plantilla->esParaCuotaPrestamo()) {
                $plantilla_detalles = $plantilla->getPlantillaCompraventaDetalles();
                if (!$plantilla_detalles->exists()) {
                    FlashMessageHelpsers::createWarningMessage('La plantilla seleccionada no tiene ningún detalle. AGREGUE PRIMERO.');
                    return false;
                }
            } else {
                if ($plantilla->esParaCuotaPrestamo()) {
                    $skey1 = 'cont_prestamos_compra';

                    if (!Yii::$app->session->has($skey1)) {
                        FlashMessageHelpsers::createWarningMessage('La plantilla es para devengamiento de cuotas de préstamo pero Ud. no ha especificado ninguno.');
                        return false;
                    }

//                    if (!Prestamo::cuentaInteresesPagadosExist()) { // los prestamos guardan consigo mismo las cuentas.
//                        FlashMessageHelpsers::createWarningMessage("Falta crear en el parámetro de la empresa la cuenta para usar con Intereses Pagados.");
//                        return false;
//                    }
                } elseif ($plantilla->esParaPrestamo()) {
                    $skey1 = 'cont_prestamo_facturar';

                    if (!Yii::$app->session->has($skey1) || sizeof(Yii::$app->session->get($skey1)) == 0) {
                        FlashMessageHelpsers::createWarningMessage('La plantilla es para prestamos pero Ud. no ha especificado ninguno.');
                        return false;
                    }
                }
            }
        }

        $detalles = [];

        // Crear detalles por Plantilla e IVA's usadas.
        $detalles = $this->genDetallesByDefault($detalles, $tipoDoc, $total_factura, $totales_valor, $porcentajes_iva, $plantillasModel, $moneda_id);
        // Actualizar indices en la sesion
        $detalles = array_values($detalles);

        // Agrupar cuentas iguales
        $detalles = DetalleCompra::agruparCuentas($detalles);

        // Agrupar debe/haber
        $detalles = $this->agruparDebeHaber($detalles);

        // invertir si es nota de credito
        self::invertirPartidas($detalles, $tipoDoc);

        // guarda en la sesión
        retorno:;
        Yii::$app->getSession()->set('cont_detallecompra-provider', new ArrayDataProvider([
            'allModels' => $detalles,
            'pagination' => false,
        ]));

        return true;
    }

    /**
     * @param DetalleCompra[] $detalles
     * @param TipoDocumento $tipoDocumento
     */
    private function invertirPartidas(&$detalles, $tipoDocumento)
    {
        try {
            $TDS_NotaCredito = Compra::getTipodocSetNotaCredito();
            if ($tipoDocumento->tipo_documento_set_id == $TDS_NotaCredito->id) {
                $map = ['debe' => 'haber', 'haber' => 'debe'];
                foreach ($detalles as $key => $_) {
                    $detalles[$key]->cta_contable = $map[$_->cta_contable];
                }
                $detalles = $this->agruparDebeHaber($detalles);
            }
        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }
    }

    /**
     * @param DetalleCompra[] $array Detalles de compra
     * @return array Detalles de compra agrupados en debe / haber
     */
    private function agruparDebeHaber($array)
    {
        $debes = [];
        $haberes = [];

        foreach ($array as $item) {
            if ($item->cta_contable == 'debe') {
                $debes[] = $item;
            } elseif ($item->cta_contable == 'haber') {
                $haberes[] = $item;
            }
        }

        return array_values(array_merge($debes, $haberes));
    }

    public function actionUpdateDetalle()
    {
        /** @var DetalleCompra[] $old_detalles */
        /** @var DetalleCompra[] $new_detalles */

        $old_detalles = Yii::$app->getSession()->get('cont_detallecompra-provider')->allModels;
        $total_factura = $_POST['cont_factura_total'];
        $porcentajes_iva = $_POST['ivas'];
        $tipo_docu_id = $_POST['tipo_docu_id'];
        $plantillas = $_POST['plantillas'];

        $tipoDoc = TipoDocumento::findOne(['id' => $tipo_docu_id]);
        if ($tipoDoc == null) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar Tipo de Documento');
            return false;
        }

        // Verificar que se ha seleccionado una plantilla.
        $plantillasModel = [];
        $totales_valor = [];
        foreach ($plantillas as $pl) {
            //Valores por iva
            $valores_iva = [];
            foreach ($porcentajes_iva as $iva) {
                array_push($valores_iva, $pl['iva_' . $iva]);
            }

            $plantilla = PlantillaCompraventa::findOne(['id' => $pl['plantilla_id']]);
            if ($plantilla != null) {
                array_push($plantillasModel, $plantilla);
                $totales_valor[$plantilla->id] = $valores_iva;
            }
        }

        if (!isset($plantillasModel)) {
            FlashMessageHelpsers::createWarningMessage('Falta especificar PLANTILLA');
            return false;
        }

        // Verificar que la plantilla seleccionada tenga detalles.
        /** @var PlantillaCompraventa $plantilla */
        foreach ($plantillasModel as $plantilla) {
            if (!$plantilla->esParaPrestamo() && !$plantilla->esParaCuotaPrestamo()) {
                $plantilla_detalles = $plantilla->getPlantillaCompraventaDetalles();
                if (!$plantilla_detalles->exists()) {
                    FlashMessageHelpsers::createWarningMessage('La plantilla seleccionada no tiene ningún detalle. AGREGUE PRIMERO.');
                    return false;
                }
            } else {

                if ($plantilla->esParaCuotaPrestamo()) {
                    $skey1 = 'cont_prestamos_compra';

                    if (!Yii::$app->session->has($skey1)) {
                        FlashMessageHelpsers::createWarningMessage('La plantilla es para devengamiento de cuotas de préstamo pero Ud. no ha especificado ninguno.');
                        return false;
                    }

//                    if (!Prestamo::cuentaInteresesPagadosExist()) { // los prestamos guardan consigo mismo las cuentas.
//                        FlashMessageHelpsers::createWarningMessage("Falta crear en el parámetro de la empresa la cuenta para usar con Intereses Pagados.");
//                        return false;
//                    }
                } elseif ($plantilla->esParaPrestamo()) {
                    $skey1 = 'cont_prestamo_facturar';

                    if (!Yii::$app->session->has($skey1) || sizeof(Yii::$app->session->get($skey1)) == 0) {
                        FlashMessageHelpsers::createWarningMessage('La plantilla es para prestamos pero Ud. no ha especificado ninguno.');
                        return false;
                    }
                }
            }
        }

        $new_detalles = [];
        $new_detalles = $this->genDetallesByDefault($new_detalles, $tipoDoc, $total_factura, $totales_valor, $porcentajes_iva, $plantillasModel);
        $i = 0;
        // Agrupar cuentas iguales
        $new_detalles = DetalleCompra::agruparCuentas($new_detalles);

        foreach ($new_detalles as $new_detalle) {
            $old_detalle = $this->getIfExist($new_detalle, $old_detalles);
            if ($old_detalle != null) {
                $old_detalle->subtotal = $new_detalles[$i]->subtotal;
            } else {
                array_push($old_detalles, $new_detalle);
            }
            $i++;
        }

        retorno:;
        // Actualizar los indices del array
        $old_detalles = array_values($old_detalles);

        // Agrupar cuentas iguales
        $old_detalles = DetalleCompra::agruparCuentas($old_detalles);

        // invertir si es nota de credito
        self::invertirPartidas($detalles, $tipoDoc);

        // Agrupar debe / haber
        $old_detalles = $this->agruparDebeHaber($old_detalles);
        // guarda en la sesión
        Yii::$app->getSession()->set('cont_detallecompra-provider', new ArrayDataProvider([
            'allModels' => $old_detalles,
            'pagination' => false,
        ]));

        // TODO: Identificar las cuentas actuales.
        // TODO: Actualizar los montos
        // TODO: Si hay cuentas no correspondientes a tipoDOc y plantillas, dejar asi como vino de la sesion
        // TODO: Pisar sesion
        return true;
    }

    /**
     * @param $new_detalle DetalleCompra
     * @param $old_detalles DetalleCompra[]
     * @return null|DetalleCompra
     */
    private function getIfExist($new_detalle, $old_detalles)
    {
        foreach ($old_detalles as $old) {
            if ($new_detalle->cta_contable == $old->cta_contable && $new_detalle->planCuenta->cod_completo == $old->planCuenta->cod_completo)
                return $old;
        }

        return null;
    }

    private function numberFormatUseDotByMoneda($value, $moneda)
    {
        if (!($moneda instanceof Moneda)) {
            $moneda = Moneda::findOne(['id' => $moneda]);
        }

        if ($moneda->id == 1) {
            return (int)number_format($value, 0, '.', '');
        }

        return $value;
    }

    /**
     * @param $detalles DetalleCompra[]
     * @param $tipoDoc TipoDocumento
     * @param $total_factura
     * @param $array_totales_por_iva
     * @param $porcentajes_iva
     * @param $plantillasModel PlantillaCompraventa[]
     * @return mixed
     */
    private function genDetallesByDefault($detalles, $tipoDoc, $total_factura, $array_totales_por_iva, $porcentajes_iva, $plantillasModel, $moneda_id)
    {
        /** @var PlantillaCompraventaDetalle[] $plantilla_detalles */
        /** @var IvaCuenta $iva_cta */
        $es_nota_credito = Yii::$app->request->post('es_nota') == 'nota_credito';

//        if ($total_factura == 0) { // si se controla esto, para prestamos no se podrá simular.
//            $detalles = [];
//            Yii::trace('Entro aqui', 'ppp');
//            goto retorno;
//        }

        $moneda = Moneda::findOne(['id' => $moneda_id]);

        // Crear detalle por Tipo de Documento
        $cuenta_haber = $tipoDoc != null ? $tipoDoc->cuenta : null;
        if (isset($cuenta_haber)) {
            /* Al elegir tipo_documento, SIEMPRE se trae 1 sola cuenta. */
            $detalle = new DetalleCompra();
            $detalle->cta_contable = !$es_nota_credito ? 'haber' : 'debe';
            $detalle->plan_cuenta_id = $cuenta_haber->id;
            $detalle->subtotal = $this->numberFormatUseDotByMoneda($total_factura, $moneda);; // TODO: averiguar que valor debe tomar, dependiendo del tipo_documento.
            $detalle->agrupar = 'si';
            $detalle->cta_principal = 'no';

            array_push($detalles, $detalle);
        }

        {
            /** Variables usadas para el caso de mezcla de plantilla normal y de cuota de prestamo. */
            $prestamos_diferentes = [];
            $plantilla_cuota_prestamo = false;
            $plantilla_normal = false;
            $cuenta_caja_prestamo = null;
            $total_plantilla_normal = 0;
        }
        /** RECORDAR:
         *
         *  Plantilla de cuota de prestamo se puede mezclar con otras plantillas normales.
         *  Plantilla de factura de prestamo no se debe mezclar (verificar en las action create/update)
         */
        foreach ($plantillasModel as $plantilla) {

            if ($plantilla->esParaCuotaPrestamo()) { // TODO: establecer cta principal
                $plantilla_cuota_prestamo = true;
                $prestamoDataProvider = Yii::$app->session->get('cont_prestamos_compra');
                $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];

//                // Poner en cero la cuenta del debe proveniente del tipo de documento y coincidente con la cuenta de
//                // algun detalle.
//                foreach ($prestamoDataProvider as $prestamo) {
//                    foreach ($detalles as $key => $detalle) {
//                        if ($prestamo['prestamo']->cuenta_monto_operacion == $detalle->plan_cuenta_id && $detalle->cta_contable == 'debe')
//                            $detalles[$key]->subtotal = 0;
//                    }
//                }

                /** SOLAMENTE vacia el array si hay 1 solo detalle y este es igual a la cuenta del tipo de documento.
                 *
                 *  Si la plantilla de cuota de prestamo esta antes que las plantillas normales, hay 1 solo detalle con cuenta del tipo de documento.
                 *  Si es el caso contrario, hay mas de 1 detalle.
                 */
                if (sizeof($detalles) == 1 && $detalles[0]->plan_cuenta_id == $cuenta_haber->id)
                    $detalles = [];

                // Recorrer los prestamos de la session.
                foreach ($prestamoDataProvider as $data) {
                    foreach ($data['detalles'] as $prestamo_detalle) {
                        if ($prestamo_detalle['selected'] == '1') {
                            // Contabilizar prestamos diferentes.
                            if (!in_array($data['prestamo']->id, $prestamos_diferentes)) {
                                $prestamos_diferentes[] = $data['prestamo']->id;
                                $cuenta_caja_prestamo = $data['prestamo']->cuenta_caja; // por cada prestamo diferente, se reemplaza, pero al final se va a usar solo si `$prestamos_diferentes` tiene 1 solo elemento.
                            }

                            $_pCapital = (float)$prestamo_detalle->capital_saldo;
                            $_pInteres = (float)$prestamo_detalle->interes_saldo; // sin iva siempre.
                            $_pCuota = (float)$prestamo_detalle->monto_cuota_saldo;

                            // Crear detalles de compra
                            /** @var Prestamo $prestamo */
                            $prestamo = $data['prestamo'];
                            $usar_iva_10 = $prestamo->usar_iva_10;
                            $tiene_factura = $prestamo->tiene_factura;
                            foreach ($prestamo->getCuentasFields() as $cuenta) {
                                if (in_array($cuenta, ['cuenta_intereses_vencer', 'cuenta_intereses_pagados', 'cuenta_monto_operacion', 'cuenta_intereses_a_pagar'])) {
                                    switch ($cuenta) {
//                                        case 'cuenta_intereses_pagados':
//                                            $detalle = new DetalleCompra();
//                                            $detalle->plan_cuenta_id = $prestamo->$cuenta;
//                                            $iva = 0;
//                                            $interes = $_pInteres - $iva;
//                                            $detalle->subtotal = $interes;
//                                            $detalle->cta_contable = 'debe';
//                                            $detalle->agrupar = 'no';
//                                            $detalle->cta_principal = 'no';
//                                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;
//                                            $detalles[] = $detalle;
//                                            Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");
//                                            break;
//                                        case 'cuenta_intereses_vencer':
//                                            $detalle = new DetalleCompra();
//                                            $detalle->plan_cuenta_id = $prestamo->$cuenta;
//                                            $iva = 0;
//                                            $interes = $_pInteres - $iva;
//                                            $detalle->subtotal = $interes;
//                                            $detalle->cta_contable = 'haber';
//                                            $detalle->agrupar = 'no';
//                                            $detalle->cta_principal = 'no';
//                                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;
//                                            $detalles[] = $detalle;
//                                            Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");
//                                            break;
                                        case 'cuenta_intereses_a_pagar':
                                            $detalle = new DetalleCompra();
                                            $detalle->plan_cuenta_id = $prestamo->$cuenta;
                                            $iva = round($_pInteres / 10);
                                            $interes = $_pInteres;
                                            $detalle->subtotal = $interes;
                                            $detalle->cta_contable = 'debe';
                                            $detalle->agrupar = 'no';
                                            $detalle->cta_principal = 'no';
                                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;

                                            if ($prestamo->isCase4()) {
                                                $detalles[] = $detalle;
                                                Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");

                                                $detalle = new DetalleCompra();
                                                $detalle->plan_cuenta_id = $prestamo->cuenta_iva_10; // por cuenta iva 10%
                                                $detalle->subtotal = $iva;
                                                $detalle->cta_contable = 'debe';
                                                $detalle->agrupar = 'no';
                                                $detalle->cta_principal = 'no';
                                                $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                                $detalles[] = $detalle;
                                                Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");

                                                $detalle = new DetalleCompra();
                                                $detalle->plan_cuenta_id = $prestamo->cuenta_caja; // por caja o banco
                                                $detalle->subtotal = $_pInteres + $iva;
                                                $detalle->cta_contable = 'haber';
                                                $detalle->agrupar = 'no';
                                                $detalle->cta_principal = 'no';
                                                $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                                $detalles[] = $detalle;
                                                Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");

                                            } else {
                                                $detalles[] = $detalle;

                                                $detalle = new DetalleCompra();
                                                $detalle->plan_cuenta_id = $prestamo->cuenta_caja; // por caja o banco
                                                $detalle->subtotal = $_pInteres;
                                                $detalle->cta_contable = 'haber';
                                                $detalle->agrupar = 'no';
                                                $detalle->cta_principal = 'no';
                                                $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                                $detalles[] = $detalle;
                                                Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");
                                            }
                                            break;
                                        case 'cuenta_monto_operacion':
                                            $iva = round($_pInteres / 10);
                                            $capital = $_pCapital + $iva;
                                            Yii::warning('capital: ' . $_pCapital);
                                            Yii::warning('iva del int: ' . $iva);
                                            Yii::warning('capital toal: ' . $capital);
                                            $detalle = new DetalleCompra();
                                            $detalle->plan_cuenta_id = $prestamo->$cuenta; // por cuenta PRESTAMOS A BANCO XX ...
                                            $detalle->subtotal = $capital;
                                            $detalle->cta_contable = 'debe';
                                            $detalle->agrupar = 'no';
                                            $detalle->cta_principal = 'si';
                                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                            $detalles[] = $detalle;
                                            Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");

                                            $detalle = new DetalleCompra();
                                            $detalle->plan_cuenta_id = $prestamo->cuenta_caja; // por caja o banco, nuevamente
                                            $detalle->subtotal = $capital;
                                            $detalle->cta_contable = 'haber';
                                            $detalle->agrupar = 'no';
                                            $detalle->cta_principal = 'no';
                                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                            $detalles[] = $detalle;
                                            Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }

            } elseif ($plantilla->esParaPrestamo()) {
                // Remover cuenta creada por tipo de documento
                $detalles = [];
                $prestamo_ids = Yii::$app->session->get('cont_prestamo_facturar');
                $prestamos = Prestamo::findAll($prestamo_ids);

                foreach ($prestamos as $prestamo) {
                    foreach ($detalles as $key => $detalle) {
                        if ($prestamo->cuenta_monto_operacion == $detalle->plan_cuenta_id && $detalle->cta_contable == 'haber') {
                            $detalles[$key]->subtotal = 0;
                        }
                    }
                }

                foreach ($prestamos as $prestamo) {
                    $usar_iv_10 = $prestamo->usar_iva_10;
                    $cuentas = $prestamo->getCuentasFields();
                    $gastos_bancarios = round($prestamo->gastos_bancarios / 1.1);
                    $iva_interes = ($prestamo->incluye_iva == 'si') ? ((float)$prestamo->intereses_vencer / 11) : ((float)$prestamo->intereses_vencer / 10);
                    $iva_interes = round($iva_interes);
                    $interes_sin_iva = ($prestamo->incluye_iva == 'si') ? ((float)$prestamo->intereses_vencer - $iva_interes) : $prestamo->intereses_vencer;
                    $iva_gasto = round($gastos_bancarios / 10);
                    foreach ($cuentas as $cuenta) {

                        $concepto = str_replace('cuenta_', '', $cuenta);
                        $detalle_prestamo = new DetalleCompra();
                        $detalle_prestamo->plan_cuenta_id = $prestamo->$cuenta;

                        if ($concepto == 'intereses_pagados') continue;

                        if ($concepto == 'gastos_bancarios') {
                            $detalle_prestamo->subtotal = $gastos_bancarios;
                            $detalle_prestamo->cta_principal = 'no';
                            $detalle_prestamo->cta_contable = 'debe';
                            $detalle_prestamo->agrupar = 'no';
                            $detalle_prestamo->prestamo_cuota_id = null;
                        } elseif ($concepto == 'iva_10' && $prestamo->isCase1()) {
                            $detalle_prestamo->subtotal = $prestamo->$concepto;
                            $detalle_prestamo->cta_principal = 'no';
                            $detalle_prestamo->cta_contable = 'debe';
                            $detalle_prestamo->agrupar = 'no';
                            $detalle_prestamo->prestamo_cuota_id = null;
                        } elseif ($concepto == 'gastos_no_deducibles' && $prestamo->isCase2()) {
                            $detalle_prestamo->subtotal = $prestamo->iva_10;
                            $detalle_prestamo->cta_principal = 'no';
                            $detalle_prestamo->cta_contable = 'debe';
                            $detalle_prestamo->agrupar = 'no';
                            $detalle_prestamo->prestamo_cuota_id = null;
                        } elseif (in_array($concepto, ['intereses_vencer'])) {
                            $detalle_prestamo->subtotal = ($prestamo->incluye_iva == 'si') ? round($prestamo->$concepto / 1.1) : $prestamo->$concepto;
                            $detalle_prestamo->cta_principal = 'no';
                            $detalle_prestamo->cta_contable = 'debe';
                            $detalle_prestamo->agrupar = 'no';
                            $detalle_prestamo->prestamo_cuota_id = null;
                        } elseif ($concepto == 'caja') {
                            $detalle_prestamo->subtotal = $prestamo->$concepto;
                            $detalle_prestamo->cta_principal = 'no';
                            $detalle_prestamo->cta_contable = 'debe';
                            $detalle_prestamo->agrupar = 'no';
                            $detalle_prestamo->prestamo_cuota_id = null;
                        } elseif ($concepto == 'monto_operacion') {
                            $detalle_prestamo->subtotal = $prestamo->$concepto - $interes_sin_iva;
                            $detalle_prestamo->cta_principal = 'si';
                            $detalle_prestamo->cta_contable = 'haber';
                            $detalle_prestamo->agrupar = 'no';
                            $detalle_prestamo->prestamo_cuota_id = null;

                            if ($usar_iv_10 == 'no') {
                                $detalle_prestamo->subtotal -= ($iva_interes + $iva_gasto);
                            }
                        } elseif ($concepto == 'intereses_a_pagar') {
                            $detalle_prestamo->subtotal = $interes_sin_iva;
                            $detalle_prestamo->cta_principal = 'no';
                            $detalle_prestamo->cta_contable = 'haber';
                            $detalle_prestamo->agrupar = 'no';
                            $detalle_prestamo->prestamo_cuota_id = null;
                        }

                        $detalles[] = $detalle_prestamo;
                    }
                }

            } else { // Es plantilla normal
                $plantilla_normal = true;

                $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
                $gravada_total = 0.0;
                $descuento = $plantilla->isForDiscount();
                foreach ($plantilla->plantillaCompraventaDetalles as $p_detalle) {
                    if ($p_detalle->tipo_asiento == 'compra') {
                        $i = 0;
                        foreach ($array_totales_por_iva[$plantilla->id] as $total_iva) {
                            if ($total_iva != '' && $p_detalle->ivaCta != null && $total_iva !== '' && round($total_iva, 2) > 0.0 && $p_detalle->ivaCta->iva->porcentaje == $porcentajes_iva[$i]) {
                                $detalle = new DetalleCompra();
                                $detalle->plan_cuenta_id = $p_detalle->pCGravada->id;
                                $detalle->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
                                $detalle->cta_principal = $p_detalle->cta_principal;
                                $detalle->subtotal = round($total_iva / (1.00 + ($p_detalle->ivaCta->iva->porcentaje / 100.0)), 2);
                                $detalle->subtotal = $this->numberFormatUseDotByMoneda($detalle->subtotal, $moneda);
                                $detalle->subtotal *= ($descuento ? -1 : 1);
                                $detalle->agrupar = 'si';
                                $gravada_total += round($detalle->subtotal, 2);
                                // detalle por iva x%
                                $detalle2 = new DetalleCompra();
                                $detalle2->plan_cuenta_id = $p_detalle->ivaCta->cuentaCompra->id;
                                $detalle2->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
                                $detalle2->cta_principal = 'no';
                                $detalle2->subtotal = round($total_iva * ($descuento ? -1 : 1) - $detalle->subtotal, 2);
                                $detalle2->subtotal = $this->numberFormatUseDotByMoneda($detalle2->subtotal, $moneda);
                                $detalle2->agrupar = 'si';
                                array_push($detalles, $detalle);
                                array_push($detalles, $detalle2);
                                $total_plantilla_normal += $total_iva;
                                break;
                            } elseif ($p_detalle->ivaCta == null && $total_iva !== '' && round($total_iva, 2) > 0.0 && $porcentajes_iva[$i] == '0') {
                                $detalle = new DetalleCompra();
                                $detalle->plan_cuenta_id = $p_detalle->pCGravada->id;
                                $detalle->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
                                $detalle->cta_principal = $p_detalle->cta_principal;
                                $detalle->subtotal = round($total_iva, 2);
                                $detalle->subtotal = $this->numberFormatUseDotByMoneda($detalle->subtotal, $moneda);
                                $detalle->subtotal *= ($descuento ? -1 : 1);
                                $detalle->agrupar = 'si';
                                array_push($detalles, $detalle);
                                $total_plantilla_normal += $total_iva;
                                break;
                            }
                            $i++;
                        }
                    }
                }
            }
//            array_merge($detalles, array_values($detalles));
        }

        if ($plantilla_normal && $plantilla_cuota_prestamo) {
            $existCuentaTipoDoc = false;
            foreach ($detalles as $key => $detalle) {
                if ($detalle->plan_cuenta_id == $cuenta_haber->id && $detalle->prestamo_cuota_id == '') {
                    $existCuentaTipoDoc = true;
                    break;
                }
            }

            if (!$existCuentaTipoDoc) {
                $detalle = new DetalleCompra();
                $detalle->cta_contable = !$es_nota_credito ? 'haber' : 'debe';
                $detalle->plan_cuenta_id = $cuenta_haber->id;
                $detalle->subtotal = $this->numberFormatUseDotByMoneda($total_plantilla_normal, $moneda);; // TODO: averiguar que valor debe tomar, dependiendo del tipo_documento.
                $detalle->agrupar = 'si';
                $detalle->cta_principal = 'no';
                $detalles[] = $detalle;
            }
        }

        $detalles = array_values($detalles);

        retorno:;
        return $detalles;
    }

    /**
     * @param null $oldModel
     * @return Compra
     * @throws \Exception
     */
    private
    function newModel($oldModel = null)
    {
        if ($oldModel != null) unset($oldModel); // $oldModel = null;
        $model = new Compra();
        $model->moneda_id = ParametroSistemaHelpers::getValorByNombre('moneda_base') == null ? 1 : ParametroSistemaHelpers::getValorByNombre('moneda_base');
        //$model->fecha_emision = date('Y-m-d');
        $model->empresa_id = Yii::$app->getSession()->get(SessionVariables::empresa_actual);
        $model->obligacion_id = Compra::getObligacionesEmpresaActual($model->empresa_id)[0]['id'];  #No es necesario pero es para controlar que haya obligacion activa en la empresa actual.
        $model->estado = 'vigente';
        $model->para_iva = 'si';

        if (Yii::$app->session->has('cont_compra_valores_usados')) {
            $valoresUsados = Yii::$app->session->get('cont_compra_valores_usados');
            foreach ($valoresUsados as $key => $value) {
                if ($key != 'id' && $key != 'plantilla_id') $model->$key = $value;
            }

//            $nro = (int)substr($model->nro_factura, 8, 15);
//            $prefijo = substr($model->nro_factura, 0, 7);
//            $nro = (string)($nro + 1);
//            $nro = str_repeat('0', (7 - strlen($nro))) . $nro;
            $model->nro_factura = '';
            $model->total = '';
        }

        $model->empresa_id = Yii::$app->session->get('core_empresa_actual');
        $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
        return $model;
    }

    /**
     * @param Compra $model
     * @param array $plantillaTable
     * @return mixed
     * @throws \Exception
     */
    private function cargarIvaCtaUsada($model, $plantillaTable)
    {
        $cpas_iva_cta_used = [];
        foreach ($plantillaTable as $row) {
            /** @var IvaCuenta $iva_cta */
            foreach (IvaCuenta::find()->all() as $iva_cta) {
                // $totales es un array, cuyas entradas estan indexadas por un nombre del formato 'iva-N'
                if (isset($row['ivas']['iva_' . $iva_cta->iva->porcentaje]) && $row['ivas']['iva_' . $iva_cta->iva->porcentaje] != '') {
                    $cpa_iva_cta_used = new CompraIvaCuentaUsada();
                    $cpa_iva_cta_used->iva_cta_id = isset($iva_cta->cuenta_compra_id) ? $iva_cta->id : null;
                    $cpa_iva_cta_used->factura_compra_id = $model->id;

                    $monto = floatval(str_replace([".", ","], ["", "."], $row['ivas']['iva_' . $iva_cta->iva->porcentaje]));
                    $cpa_iva_cta_used->monto = $monto;
                    $cpa_iva_cta_used->plan_cuenta_id = $iva_cta->cuenta_compra_id; // cuenta_compra_id puede ser vacio
                    $cpa_iva_cta_used->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                    $cpa_iva_cta_used->plantilla_id = $row['plantilla_id'];

                    // Verificar motivo de gnd
                    $plantilla = PlantillaCompraventa::findOne(['id' => $cpa_iva_cta_used->plantilla_id]);
                    if (isset($plantilla) && $plantilla->deducible == 'no') {
                        if (!isset($row['gnd_motivo']))
                            throw new \Exception("Error en la fila de plantilla {$plantilla->nombre}: Motivo por el que se envia a GND no puede estar vacio.");
                        $cpa_iva_cta_used->gnd_motivo = $row['gnd_motivo'];
                    }

                    $cpas_iva_cta_used [] = $cpa_iva_cta_used;
                }
            }
        }

        $_iva_ctas_usadas = $model->_iva_ctas_usadas;
        /** @var CompraIvaCuentaUsada $monto_iva_usada */
        foreach ($cpas_iva_cta_used as $monto_iva_usada) {
            $_iva_ctas_usadas[$monto_iva_usada->plantilla_id]['gnd_motivo'] = $monto_iva_usada->gnd_motivo;
            //Para cargar tabla de plantillas ya existentes
            if (array_key_exists($monto_iva_usada->plantilla_id, $_iva_ctas_usadas)) {
                if ($monto_iva_usada->ivaCta != null) {
                    if (array_key_exists($monto_iva_usada->ivaCta->iva->porcentaje, $_iva_ctas_usadas[$monto_iva_usada->plantilla_id])) {
                        $_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] += (int)$monto_iva_usada->monto;
                    } else {
                        $_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
                    }
                } else {
                    if (array_key_exists(0, $_iva_ctas_usadas[$monto_iva_usada->plantilla_id])) {
                        $_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] += (int)$monto_iva_usada->monto;
                    } else {
                        $_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
                    }
                }
            } else {
                if ($monto_iva_usada->ivaCta != null) {
                    $_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
                } else {
                    $_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
                }
            }
        }

        return $_iva_ctas_usadas;
    }

    public function actionGetClienteAutofacturaByCedula()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $cedula = Yii::$app->request->get('cedula');
        if (!isset($cedula) || $cedula == '') return ['data' => '', 'error' => '- cédula está vacío -'];

        $compra = Compra::findOne(['cedula_autofactura' => $cedula]);
        if (!isset($compra)) return ['data' => '', 'error' => ''];

        return ['data' => $compra->nombre_completo_autofactura, 'error' => ''];
    }

    /**
     * Creates a new Compra model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception
     */
// todo: si la moneda es extrangera, realizar los cambios en los montos. En update y en create.
    /**
     * @param null $_pjax
     * @return string|Response
     * @throws Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreate($_pjax = null)
    {
        /** @var Compra $model */
        try {
            $model = $this->newModel();
        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            return $this->redirect(['index']);
        }
        $model->guardarycerrar = 'no';
        $model->timbrado_vencido = 0;
        $session = Yii::$app->session;

        $empresa_id = $session->get('core_empresa_actual');
        $periodo_id = $session->get('core_empresa_actual_pc');
        $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
        $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

        $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
        $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

        if (!isset($parmsys_debe)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Ganancia.");
            return $this->redirect(['index']);
        }
        if (!isset($parmsys_haber)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Pérdida.");
            return $this->redirect(['index']);
        }

        if ($_pjax == null && !Yii::$app->request->isPost) {
            $session->set('cont_detallecompra-provider', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));

            $session->set('cont_detallecompra_sim-provider', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));

            $session->remove('cont_prestamos_compra');
            $session->remove('cont_prestamos_compra');
            $session->remove('cont_prestamo_facturar');
            $session->remove('cont_compra_actual_id');
            $session->remove('cont_compra_actual_entidad_ruc');
            $session->remove('cont_prestamo_deducible');
            $session->remove('cont_activo_bio_stock_manager');
            $session->remove('cont_act_bio_valor_exenta');
            $session->remove('cont_act_bio_compra_fila_id');
            $session->remove('cont_afijo_atributos');
            $session->remove('cont_compra_actfijo_tipo_id');
        }

        if (array_key_exists('CompraIvaCuentaUsada', $_POST)) {
            if (!empty($_POST['CompraIvaCuentaUsada']['__id__'])) { // eliminar el blue print
                unset($_POST['CompraIvaCuentaUsada']['__id__']);
                Yii::$app->request->setBodyParams($_POST);
                foreach ($_POST['CompraIvaCuentaUsada'] as $new_keys => $new) {
                    if ($new['plantilla_id'] == "") {
                        unset($_POST['CompraIvaCuentaUsada'][$new_keys]); // eliminar items que tienen plantilla_id vacio.
                        Yii::$app->request->setBodyParams($_POST);
                    }
                }
            }
        }

        if ($model->load(Yii::$app->request->post())) {

            $trans = Yii::$app->db->beginTransaction();

            try {

                $model->creado_por = Yii::$app->user->identity->username;

                /** @var IvaCuenta $iva_cta */
                /** @var DetalleCompra[] $detalleCompra */

                if ($model->tipo_documento_id != '') {
                    $tipodoc = TipoDocumento::findOne(['id' => $model->tipo_documento_id]);
                    if (isset($tipodoc)) {
                        $model->condicion = $tipodoc->condicion;
                    }
                }
                $entidad = Entidad::findOne(['ruc' => $model->ruc]);
                $model->entidad_id = $entidad != null ? $entidad->id : null;
                $totales_tabla = $_POST['CompraIvaCuentaUsada'];

                $model->_iva_ctas_usadas = $this->cargarIvaCtaUsada($model, $totales_tabla);

                $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

                if ($model->condicion == 'credito')
                    $model->fecha_vencimiento = implode('-', array_reverse(explode('-', $model->fecha_vencimiento)));

                //Actualizar número actual en timbrado detalle
                $nro = (int)substr($model->nro_factura, 8, 15);
                $prefijo = substr($model->nro_factura, 0, 7);
                $tipo_documento_set_ninguno_id = ParametroSistema::getValorByNombre('tipo_documento_set_ninguno');
                if ($model->tipoDocumento->tipo_documento_set_id != $tipo_documento_set_ninguno_id) {
                    /** @var TimbradoDetalle $timbradoDetalle */
                    $timbradoDetalle = TimbradoDetalle::find()
                        ->joinWith('timbrado as timbrado')
                        ->where([
                            'timbrado.id' => $model->timbrado_id,
                            'prefijo' => $prefijo,
                            'tipo_documento_set_id' => $model->tipoDocumento->tipo_documento_set_id
                        ])
                        ->andWhere(['<=', 'nro_inicio', $nro])
                        ->andWhere(['>=', 'nro_fin', $nro])
                        ->one();

                    Yii::error($timbradoDetalle);
                    if ($timbradoDetalle == null) {
                        //No existe el numero de factura en el rango del timbrado
                        //Se crea un detalle para el timbrado
                        $newDetalleTimbrado = new TimbradoDetalle2();  #Se usa esta clase para que valide que no haya otro tim con fechas y prefijos iguales
                        $newDetalleTimbrado->prefijo = $prefijo;
                        $newDetalleTimbrado->timbrado_id = $model->timbrado_id;
                        $newDetalleTimbrado->nro_inicio = $nro;
                        $newDetalleTimbrado->nro_fin = $nro;
                        $newDetalleTimbrado->nro_actual = $nro;
                        $newDetalleTimbrado->tipo_documento_set_id = $model->tipoDocumento->tipo_documento_set_id;
                        if (!$newDetalleTimbrado->save()) {
                            throw new \Exception("Error guardando timbrado detalle: {$newDetalleTimbrado->getErrorSummaryAsString()}");
                        }
                        $newDetalleTimbrado->refresh();
                        $model->timbrado_detalle_id = $newDetalleTimbrado->id;
                    } else {
                        $timbradoDetalle->nro_actual = $nro; # TODO: Se debe actualizar? No es solo desde ventas?
                        if (!$timbradoDetalle->save()) {
                            throw new \Exception('Error al guardar timbrado detalle' . $timbradoDetalle->getErrorSummaryAsString());
                        }
                        $timbradoDetalle->refresh();
                        $model->timbrado_detalle_id = $timbradoDetalle->id;
                    }
                }

                if (!$model->validate()) {
                    throw new \Exception("Error validando formulario: {$model->getErrorSummaryAsString()}");
                }

                //Validar nro de factura
                if ($model->tipoDocumento->tipo_documento_set_id != $tipo_documento_set_ninguno_id) {
                    $result = Compra::checkNroFactura($model->nro_factura, null, 'factura', $model->entidad_id);
                    $msg = '';
                    switch ($result) {
                        case Compra::NRO_FACTURA_NO_RANGO:
                            $msg = 'Este nro de factura no corresponde a ningún rango de ningún timbrado.';
                            break;
                        case Compra::NRO_FACTURA_NO_PREFIJO:
                            $msg = 'Este prefijo de factura no existe. DEBE CREAR desde el menú de Timbrado primero.';
                            break;
                        case Compra::NRO_FACTURA_DUPLICADO:
                            $msg = 'Este nro de factura está duplicado.';
                            break;
                    }

                    if ($result != Compra::NRO_FACTURA_CORRECTO) {
                        throw new \Exception($msg);
                    }

                    //Verificar vigencia del timbrado
                    $is_timbradoVirtual = $model->timbradoDetalle->timbrado->isTimbradoVirtual();
                    if (!$is_timbradoVirtual && strtotime($model->timbradoDetalle->timbrado->fecha_fin) < strtotime($model->fecha_emision)) {
                        foreach ($totales_tabla as $total) {
                            if ($total['plantilla_id'] != '') {
                                if (PlantillaCompraventa::findOne(['id' => $total['plantilla_id']])->deducible == 'si') {
                                    throw new \Exception('Solo se pueden agregar plantillas no deducibles');
                                }
                            }
                        }
                    }
                }

                $model->total = 0; // se va a recalcular para evitar manipulaciones.
                $model->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                if (!$model->save()) {
                    throw new \Exception("Error guardando compra: {$model->getErrorSummaryAsString()}");
                } // para generar id.

                //Tipo documento para importación
                $tipoDocParaImportacion = \backend\modules\contabilidad\helpers\ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-periodo-{$model->periodo_contable_id}-tipo_doc_importacion_id");

                // Verificar si la plantilla tiene detalles
                foreach ($totales_tabla as $total) {
                    if ($total['plantilla_id'] != '') {
                        $plantilla = PlantillaCompraventa::findOne($total['plantilla_id']);
                        if (!$plantilla->esParaPrestamo() && !$plantilla->esParaCuotaPrestamo()) {
                            $query = PlantillaCompraventa::findOne(['id' => $total['plantilla_id']])->getPlantillaCompraventaDetalles();
                            if (!$query->exists())
                                throw new \Exception('La plantilla seleccionada con ID: ' . $total['plantilla_id'] . ' no tiene detalles. AGREGUE PRIMERO y luego vuelva a crear la factura.');
                        }

                        //Validar plantilla para importación
                        if ($tipoDocParaImportacion == $model->tipo_documento_id && $plantilla->para_importacion == 'no')
                            throw new \Exception('La plantilla seleccionada con ID: ' . $total['plantilla_id'] . ' no corresponde a una plantilla de importación.');
                    }
                }

                // No permitir guardar si elige moneda extrangera y no existe cargado de la set.
                $fecha = date('Y-m-d', strtotime('-1 day', strtotime($model->fecha_emision)));
                $moneda_id = $model->moneda_id;
                /** @var Cotizacion $query */
                $query = Cotizacion::find()
                    ->where(['fecha' => $fecha, 'moneda_id' => $moneda_id])
                    ->andWhere(['<>', 'moneda_id', ParametroSistemaHelpers::getValorByNombre('moneda_base') != null ? Moneda::findOne(['id' => ParametroSistemaHelpers::getValorByNombre('moneda_base')])->id : 1])
                    ->one();
                if ($model->moneda_id != 1 && ($query == null || $model->cotizacion == '')) {
                    if ($query == null) throw new \Exception('Falta cargar cotización de la SET.');
                    elseif ($model->cotizacion == '') throw new \Exception('Falta especificar valor de la moneda');
                }
//die();
                //Totales por plantilla
                ///////////////////////////////////////////////////////////////////////////////////
                //Guardar
                $ningun_total = true;
                $plantillas_ids = []; // para verificar que no haya duplicados
                $cpas_iva_cta_used = [];
                $compra_de_activofijo = false;
                $es_para_cuota_prestamo = false;
                $es_para_fact_de_prestamo = false;
                $es_para_act_bio = false;
                $plantilla_id = null;
                $plantilla_actfijo_id = null;
                $subtotal_plantilla_actfijo = 0.0;
                $plantilla_para_importacion = null;
                foreach ($totales_tabla as $total) {
                    foreach (IvaCuenta::find()->all() as $iva_cta) {
                        // $totales es un array, cuyas entradas estan indexadas por un nombre del formato 'iva-N'
                        if (isset($total['ivas']['iva_' . $iva_cta->iva->porcentaje]) && $total['ivas']['iva_' . $iva_cta->iva->porcentaje] != '') {
                            $cpa_iva_cta_used = new CompraIvaCuentaUsada();
                            $cpa_iva_cta_used->iva_cta_id = isset($iva_cta->cuenta_compra_id) ? $iva_cta->id : null;
                            $cpa_iva_cta_used->factura_compra_id = $model->id;

                            $monto = floatval(str_replace([".", ","], ["", "."], $total['ivas']['iva_' . $iva_cta->iva->porcentaje]));
                            $cpa_iva_cta_used->monto = $monto;
                            $cpa_iva_cta_used->plan_cuenta_id = $iva_cta->cuenta_compra_id; // cuenta_compra_id puede ser vacio
                            $cpa_iva_cta_used->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                            $cpa_iva_cta_used->plantilla_id = $total['plantilla_id'];

                            // Verificar motivo de gnd
                            $plantilla = PlantillaCompraventa::findOne(['id' => $cpa_iva_cta_used->plantilla_id]);
                            if (isset($plantilla) && $plantilla->deducible == 'no') {
                                if (!isset($total['gnd_motivo']))
                                    throw new \Exception("Error en la fila de plantilla {$plantilla->nombre}: Motivo por el que se envia a GND no puede estar vacio.");
                                $cpa_iva_cta_used->gnd_motivo = $total['gnd_motivo'];
                            }

                            $cpas_iva_cta_used [] = $cpa_iva_cta_used;

                            if (!$cpa_iva_cta_used->validate() || !$cpa_iva_cta_used->save()) {
                                $msg = implode(', ', $cpa_iva_cta_used->getErrorSummary(true));
                                throw new \Exception('ERROR INTERNO al guardar factura: ' . $msg);
                            };
                            $ningun_total = false;

                            $plantilla_id = $total['plantilla_id'];
                            $plantilla = PlantillaCompraventa::findOne(['id' => $plantilla_id]);
                            if ($plantilla->esParaActivoFijo()) {
                                $subtotal_plantilla_actfijo += round($monto / ((100 + $iva_cta->iva->porcentaje) / 100));
                            } elseif ($plantilla->esParaCuotaPrestamo()) {
                                $es_para_cuota_prestamo = true;
                            } elseif ($plantilla->esParaPrestamo()) {
                                $es_para_fact_de_prestamo = true;
                            } elseif ($plantilla->esParaActivoBiologico()) {
                                $es_para_act_bio = true;
                            }
                            $idsObls = [];
                            foreach ($plantilla->obligaciones as $obligacion) {
                                in_array($obligacion->id, $idsObls) || $idsObls[] = $obligacion->id;
                            }
                            if (!in_array($model->obligacion_id, $idsObls)) {
                                throw new \Exception("La plantilla {$plantilla->nombre} no pertenece a la obligación {$model->obligacion->nombre} seleccionada.");
                            }

                            //Controlar que no se mezclen plantillas normales con plantillas de importación
                            if ($plantilla->para_importacion != 'no') {
                                if ($plantilla_para_importacion != null && !$plantilla_para_importacion)
                                    throw new \Exception("No puede mezclar plantillas de importación con plantillas normales.");
                                $plantilla_para_importacion = true;
                            } else {
                                if ($plantilla_para_importacion != null && $plantilla_para_importacion)
                                    throw new \Exception("No puede mezclar plantillas de importación con plantillas normales.");
                                $plantilla_para_importacion = false;
                            }

                            // Controlar que no mezclen plantillas de prestamo con otros
                            if ($plantilla->esParaPrestamo() && sizeof($totales_tabla) > 1)
                                throw new \Exception("Error en el uso de plantillas: No puede mezclar plantilla de Préstamo con ninguna otra plantilla.");

                            // Controlar que el tipo de doc sea credito si la plantilla es de prestamo:
                            // Segun asistencia presencial del 24 de enero y segun el acta de acuerdo de entendimiento,
                            // la factura de prestamo debe ser credito.
                            if ($plantilla->esParaPrestamo() && $model->tipoDocumento->condicion != 'credito')
                                throw new \Exception("Error en el uso del tipo de documento: Si va a utilizar plantilla para prestamo, el tipo de documento debe ser credito.");
                            $empresa_id = Yii::$app->session->get('core_empresa_actual');
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-plantilla_seguro_a_devengar_id";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);

                            if (!isset($parametro_sistema)) {
                                throw new \Exception("Falta definir en el parámetro de la empresa, la Plantilla para Seguros a Devengar.");
                            }

                            if ($parametro_sistema->valor == $plantilla_id && sizeof($totales_tabla) > 1) {
                                throw new \Exception('Ud. no puede mezclar cuentas para SEGUROS A DEVENGAR con cuentas de otra plantilla.');
                            }
                        }
                    }

                    if (key_exists($total['plantilla_id'], $plantillas_ids)) {//Duplicado, se retorna con error se retorna con error
                        throw new \Exception('ERROR INTERNO al guardar factura: ' . 'Hay plantillas duplicadas, por favor corrija.');
                    } else {
                        $plantillas_ids [$total['plantilla_id']] = $total['plantilla_id'];
                    }

                    if (isset($plantilla_id) && PlantillaCompraventa::findOne(['id' => $plantilla_id])->esParaActivoFijo()) {
                        $compra_de_activofijo = true;
                        $plantilla_actfijo_id = $plantilla_id;
                    }
                }

//                $totales = [];
//                $total_iva = 0.0;
//                foreach ($cpas_iva_cta_used as $monto_iva_usada) {
//                    $monto = $monto_iva_usada->monto;
//                    if ($model->moneda_id == 1) {
//                        $monto = round($monto);
//                    }
//                    $monto = ValorHelpers::numberFormatMonedaSensitive($monto, 2, $model->moneda);
//                    if (isset($monto_iva_usada->ivaCta)) {
//                        if (array_key_exists('totales_iva-iva-' . $monto_iva_usada->ivaCta->iva->porcentaje . '-disp', $totales)) {
//                            $totales['totales_iva-iva-' . $monto_iva_usada->ivaCta->iva->porcentaje . '-disp'] += (float)$monto;
//                        } else {
//                            $totales['totales_iva-iva-' . $monto_iva_usada->ivaCta->iva->porcentaje . '-disp'] = (float)$monto;
//                        }
//                        if (array_key_exists('totales_iva-impuesto-' . $monto_iva_usada->ivaCta->iva->porcentaje . '-disp', $totales)) {
//                            $totales['totales_iva-impuesto-' . $monto_iva_usada->ivaCta->iva->porcentaje . '-disp'] += (float)round($monto_iva_usada->monto - ($monto_iva_usada->monto / (1.0 + $monto_iva_usada->ivaCta->iva->porcentaje / 100.0)));
//                        } else {
//                            $totales['totales_iva-impuesto-' . $monto_iva_usada->ivaCta->iva->porcentaje . '-disp'] = (float)round($monto_iva_usada->monto - ($monto_iva_usada->monto / (1.0 + $monto_iva_usada->ivaCta->iva->porcentaje / 100.0)));
//                        }
//
//                        $total_iva += round($monto_iva_usada->monto - ($monto_iva_usada->monto / (1.0 + $monto_iva_usada->ivaCta->iva->porcentaje / 100.0)));
//                    } else {
//                        $monto_totales[] = [
//                            'totales_iva-iva-0-disp' => $monto
//                        ];
//                    }
//
//                    //Para cargar tabla de plantillas ya existentes
//                    if (array_key_exists($monto_iva_usada->plantilla_id, $model->_iva_ctas_usadas)) {
//                        if ($monto_iva_usada->ivaCta != null) {
//                            if (array_key_exists($monto_iva_usada->ivaCta->iva->porcentaje, $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id])) {
//                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] += $monto_iva_usada->monto;
//                            } else {
//                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
//                            }
//                        } else {
//                            if (array_key_exists(0, $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id])) {
//                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] += $monto_iva_usada->monto;
//                            } else {
//                                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
//                            }
//                        }
//                    } else {
//                        if ($monto_iva_usada->ivaCta != null) {
//                            $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
//                        } else {
//                            $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][0] = $monto_iva_usada->monto;
//                        }
//                    }
//                }
//                ///////////////////////////////////////////////////////////////////////////////////

                if ($ningun_total) {
                    throw new \Exception('Falta completar al menos un campo del Total Iva o Total Exenta.');
                }

                // No permitir guadar factura sin detalle
                $detalleCompra = $session['cont_detallecompra-provider']->allModels;
                if (sizeof($detalleCompra) == 0) {
                    throw new \Exception('Faltan los detalles de la factura');
                }

                $compra_de_poliza = false;
                $importe_gravado_seguro = 0.0;
                $id_cta_seguro = PlantillaCompraventa::getIdCuentaSeguroADevengar();
                // Calcular gravada e impuesto en detalles. Asociar a la cabecera, sumar todos los saldos. Validar
                $saldo = ['debe' => 0.0, 'haber' => 0.0];
                $hayCtaPrincipal = false;
                foreach ($detalleCompra as $old_detalle) {
                    $saldo[$old_detalle->cta_contable] += round($old_detalle->subtotal, 2);
                    $old_detalle->factura_compra_id = $model->id;
                    $old_detalle->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                    if (!$old_detalle->validate()) {
                        throw new \Exception('Error en detalle de factura: ' . $old_detalle->planCuenta->nombre . ": " . $old_detalle->getErrorsEnString());
                    }

                    // Si existe una plantilla marcada para seguro a devengar, id_cta_seguro es el id de la cuenta, sino null
                    if ($old_detalle->plan_cuenta_id == $id_cta_seguro) {
                        $compra_de_poliza = true;
                        $importe_gravado_seguro = round($old_detalle->subtotal, 2);
                    }

                    if ($old_detalle->cta_principal == 'si')
                        $hayCtaPrincipal = true;
                }

                if (!$hayCtaPrincipal) {
                    throw new \Exception("Es necesario establecer al menos una cuenta como principal.");
                }

                // TODO: Verificar balance entre saldo acreedor y deudor (debe y haber). Asignar total factura.
                if (round($saldo['debe'], 2) !== round($saldo['haber'], 2)) {
                    throw new \Exception('El DEBE y el HABER NO son iguales. Verifique. Se recomienda verificar cómo está guardada la plantilla utilizada y también revisar los detalles y las simulaciones generados automáticamente.');
                }

                $model->total = $saldo['debe']; // para evitar trampa

                // Guardar los models y comprometer transaccion
                foreach ($detalleCompra as $old_detalle) {
                    if (!$old_detalle->save()) {
                        throw new \Exception("Error guardando detalles: {$old_detalle->getErrorSummaryAsString()}");
                    }
                }

                // asignar saldo = total factura
                $model->saldo = $model->condicion == 'contado' ? 0.0 : $model->total;

                if (!$model->save()) {
                    throw new \Exception("Error guardando compra: {$model->getErrorSummaryAsString()}");
                }
                $model->refresh();

                # Unificar talonarios
                if ($model->tipoDocumento->tipo_documento_set_id != $tipo_documento_set_ninguno_id)
                    try {
                        $timbrado = $model->timbradoDetalle->timbrado;
                        $timbrado->unificarTalonarios();
                    } catch (\Exception $exception) {
                        throw new \Exception("Error interno unificando talonarios: {$timbrado->getErrorSummaryAsString()}");
                    }
                /** ---------------- VERIFICAR POLIZA Y ASOCIAR ----------------- */

                if ($compra_de_poliza && Yii::$app->session->get('cont_poliza_desde_compra') != null) {
                    // Suponemos que no hay dos plantillas para poliza, que seguro se filtro mas arriba en el controller.
                    $dataProvider = Yii::$app->session->get('cont_poliza_desde_compra');
                    /** @var Poliza $poliza */
                    $poliza = $dataProvider->allModels[0];
                    $poliza->factura_compra_id = $model->id;
                    $poliza->importe_gravado = $importe_gravado_seguro * ($model->moneda_id == \common\models\ParametroSistema::getMonedaBaseId() ? 1 : $model->cotizacion);
                    $poliza->generarImporteDiario();
                    if (!$poliza->save(false)) {
                        $msg = implode(', ', $poliza->getErrorSummary(true));
                        throw new \Exception('Error guardando poliza: ' . $msg);
                    }
                    $poliza->refresh();
                    $result = $poliza->generarDevengamientos($model->fecha_emision);
                    if ($result['error']) {
                        throw new \Exception($result['error']);
                    }
                } elseif ($compra_de_poliza && Yii::$app->session->get('cont_poliza_desde_compra') == null) {
                    throw new \Exception('Esta factura registra un detalle por SEGUROS A DEVENGAR pero Ud. no ha especificado qué póliza está facturando. Debe especificar una póliza.');
                }
                /** ------------------------------------------------------------- */

                /** ---------------- VERIFICAR ACTIVO FIJO Y CREAR ----------------- */
                $aFijosCompra = Yii::$app->session->get('cont_actfijo_desde_factura', null);
                if ($compra_de_activofijo) {
                    // Verifica si hay activos fijos.
                    if (!isset($aFijosCompra) || sizeof($aFijosCompra) == 0) {
                        throw new Exception('Esta factura registra detalle por Activo Fijo comprado pero Ud. no ha especificado los datos del mismo.');
                    }

                    $total_costo_actfijo = 0;

                    /** @var ActivoFijo $oneModel */
                    /** @var ActivoFijoAtributo[] $atributos */
                    foreach ($aFijosCompra as $afijocompra) {
                        $oneModel = $afijocompra['model'];
                        $atributos = $afijocompra['atributos'];
                        $cantidad = $oneModel->cantidad;
                        while ($cantidad-- > 0) {
                            $model_actfijo = new ActivoFijo();
                            $model_actfijo->loadDefaultValues();
                            $model_actfijo->observacion = "creado el {$model->fecha_emision} desde modificar compra";
                            $model_actfijo->activo_fijo_tipo_id = $oneModel->activo_fijo_tipo_id;
                            $model_actfijo->nombre = $oneModel->nombre;
                            $model_actfijo->fecha_adquisicion = $oneModel->fecha_adquisicion;
                            // costo_adquisicion se valoriza
                            $model_actfijo->costo_adquisicion = $oneModel->costo_adquisicion;
                            $model_actfijo->vida_util_fiscal = $oneModel->vida_util_fiscal;
                            $model_actfijo->vida_util_contable = $oneModel->vida_util_contable;
                            $model_actfijo->vida_util_restante = $oneModel->vida_util_restante;
                            // valor_fiscal_neto se valoriza
                            $model_actfijo->valor_fiscal_neto = $oneModel->valor_fiscal_neto;
                            $model_actfijo->cuenta_id = PlantillaCompraventa::getFirstCuentaPrincipal($plantilla_actfijo_id)->p_c_gravada_id;;
                            $model_actfijo->factura_compra_id = $model->id;
                            $model_actfijo->moneda_id = $model->moneda_id;

                            if (!$model_actfijo->save()) {
                                throw new \Exception("Error guardando activo fijo: {$model_actfijo->getErrorSummaryAsString()}");
                            }
                            $model_actfijo->refresh();
//                            $activos_fijos [] = $model_actfijo;

                            foreach ($atributos as $_atributo) {
                                $atributo = new ActivoFijoAtributo();
                                $atributo->setAttributes($_atributo->attributes);
                                $atributo->activo_fijo_id = $model_actfijo->id;
                                if (!$atributo->save()) {
                                    throw new \Exception("Error guardando atributo para activo fijo
                                     {$model_actfijo->nombre}: {$atributo->getErrorSummaryAsString()}");
                                }
                                $atributo->refresh();
                            }
                            $model_actfijo->refresh();

                            $total_costo_actfijo += $model_actfijo->costo_adquisicion;
                        }
                    }

                    $total_costo_actfijo = round($total_costo_actfijo);
                    if ($total_costo_actfijo != $subtotal_plantilla_actfijo) {
                        $subtotal_plantilla_actfijo = $this->numberFormatUseDotByMoneda($subtotal_plantilla_actfijo, $model->moneda);
                        $total_costo_actfijo = $this->numberFormatUseDotByMoneda($total_costo_actfijo, $model->moneda);
                        throw new Exception("La Suma de los costos de los Activos Fijos
                         {$total_costo_actfijo} no puede ser distinto a {$subtotal_plantilla_actfijo}.");
                    }

                }
//                // Se deja como backup
//                if ($compra_de_activofijo && array_key_exists('cont_actfijo_desde_factura', $_SESSION)) {
//                    $dataProvider = Yii::$app->session->get('cont_actfijo_desde_factura');
//
//                    if (sizeof($dataProvider->allModels) == 0) {
//                        throw new \Exception('Esta factura registra detalle por Activo Fijo comprado pero Ud. no ha especificado los datos del mismo.');
//                    }
//
//                    $total_costo_actfijo = 0;
//                    $cotizacion = null;
//                    if ($model->moneda_id != \common\models\ParametroSistema::getMonedaBaseId())
//                        $cotizacion = Cotizacion::findOne(['fecha' => date('Y-m-d', strtotime('-1 day', strtotime($model->fecha_emision)))]);
//
//                    /** @var ActivoFijo $oneModel */
//                    foreach ($dataProvider->allModels as $oneModel) {
//                        $cantidad = $oneModel->cantidad;
//                        while ($cantidad-- > 0) {
//                            $model_actfijo = new ActivoFijo();
//                            $model_actfijo->loadDefaultValues();
//                            $model_actfijo->observacion = "creado el {$model->fecha_emision} desde compra";
//                            $model_actfijo->activo_fijo_tipo_id = $oneModel->activo_fijo_tipo_id;
//                            $model_actfijo->nombre = $oneModel->nombre;
//                            $model_actfijo->fecha_adquisicion = $oneModel->fecha_adquisicion;
//                            // costo_adquisicion se valoriza
//                            $model_actfijo->costo_adquisicion = $oneModel->costo_adquisicion;
//                            $model_actfijo->vida_util_fiscal = $oneModel->vida_util_fiscal;
//                            $model_actfijo->vida_util_restante = $oneModel->vida_util_restante;
//                            // valor_fiscal_neto se valoriza
//                            $model_actfijo->valor_fiscal_neto = $oneModel->valor_fiscal_neto;
//                            $model_actfijo->cuenta_id = PlantillaCompraventa::getFirstCuentaPrincipal($plantilla_actfijo_id)->p_c_gravada_id;
//                            $model_actfijo->factura_compra_id = $model->id;
//                            $model_actfijo->moneda_id = $model->moneda_id;
//                            $activos_fijos[] = $model_actfijo;
//                            $total_costo_actfijo += (float)$model_actfijo->costo_adquisicion;
//                        }
//                    }
//
//                    if ($total_costo_actfijo != $subtotal_plantilla_actfijo) {
//                        $trans->rollBack();
//                        $subtotal_plantilla_actfijo = $this->numberFormatUseDotByMoneda($subtotal_plantilla_actfijo, $model->moneda);
//                        $total_costo_actfijo = $this->numberFormatUseDotByMoneda($total_costo_actfijo, $model->moneda);
//                        throw new \Exception("La Suma de los costos de los Activos Fijos {$total_costo_actfijo} no puede ser distinto a {$subtotal_plantilla_actfijo}.");
//                    }
//
//                    foreach ($activos_fijos as $activo_fijo) {
//                        if (!$activo_fijo->save(false)) {
//                            throw new \Exception("Error guardando activo fijo: {$activo_fijo->getErrorSummaryAsString()}");
//                        }
//                    }
//                } elseif ($compra_de_activofijo && !array_key_exists('cont_actfijo_desde_factura', $_SESSION)) {
//                    throw new \Exception('Esta factura registra detalle por Activo Fijo comprado pero Ud. no ha especificado los datos del mismo.');
//                }
                /** ------------------------------------------------------------- */

                /** ---------------- MANEJAR CUOTAS PRESTAMOS ----------------- */
                if ($es_para_cuota_prestamo) {
                    $skey1 = 'cont_prestamos_compra';

                    if (!Yii::$app->session->has($skey1)) {
                        throw new \Exception("Falta asociar un préstamo. Utilice el botón correspondiente a préstamos de la columna de Acciones de la Plantilla de préstamos.");
                    }

                    $prestamosDataProvider = $session->get($skey1);
                    $_prestamoDetalleCompraGuardar = [];
                    $_prestamoDetallesGuardar = [];

                    $msg = [];
                    $error_in_cuotas = false;

                    foreach ($prestamosDataProvider as $data) {
                        if ($data['prestamo']->entidad_id != $model->entidad_id) {
                            throw new \Exception("El préstamo al que corresponden las cuotas especificadas no pertenece a la entidad de la factura.");
                        }

                        $_detalles = $data['detalles'];

                        /** @var PrestamoDetalle $_detalle */
                        foreach ($_detalles as $_detalle) {

                            if ($_detalle['selected'] == '1') {
                                $old_detalle = PrestamoDetalle::findOne($_detalle['id']);

                                if (!$_detalle->montosCoherentes($old_detalle)) {
                                    array_push($msg, implode(', ', $_detalle->getErrorSummary(true)));
                                    $error_in_cuotas = true;
                                }

                                if (!$error_in_cuotas) {
                                    $old_detalle->restarSaldo($_detalle);
                                    $_prestamoDetallesGuardar[] = $old_detalle;

                                    $_prestamodeDetalleCompra = new PrestamoDetalleCompra();
                                    $_prestamodeDetalleCompra->loadDefaultValues();
                                    $_prestamodeDetalleCompra->factura_compra_id = $model->id;
                                    $_prestamodeDetalleCompra->recibo_capital_nro = $_detalle['recibo_capital_nro'];
                                    $_prestamodeDetalleCompra->setAttributesBasedOn($_detalle->attributes);
                                    $_prestamoDetalleCompraGuardar[] = $_prestamodeDetalleCompra;
                                }
                            }
                        }
                    }

                    if ($error_in_cuotas) {
                        $msg = implode(', ', $msg);
                        throw new \Exception("Error al procesar datos del prestamo: {$msg}");
                    }

                    foreach ($_prestamoDetallesGuardar as $item) {
                        if (!$item->save(false)) {
                            $msg = implode(', ', $item->getErrorSummary(true));
                            throw new \Exception("Error actualizando montos de cuota: {$msg}");
                        }
                    }

                    foreach ($_prestamoDetalleCompraGuardar as $item) {
                        if (!$item->save()) {
                            $msg = implode(', ', $item->getErrorSummary(true));
                            throw new \Exception("Error Interno creando cont_prestamo_detalle_compra: {$msg}");
                        }
                    }

                    // No hace falta verificar que se haya seleccionado al menos una cuota, porque sino se generan
                    // detalles compra con subtotal 0, lo cual hace que no sea validada mucho antes de llegar a estas lineas
                }
                /** ---------------------------------------------------- */

                /** -------------------------- MANEJAR FACTURA DE PRESTAMOS -------------------------- */
                if ($es_para_fact_de_prestamo) {
                    $prestamos_id = $session->get('cont_prestamo_facturar', null);

                    if (!isset($prestamos_id)) {
                        throw new \Exception("Error: Ha agregado una plantilla para préstamos pero Ud. no ha especificado ninguno.");
                    }

                    foreach (Prestamo::findAll($prestamos_id) as $prestamo) {
                        if ($prestamo->entidad_id != $model->entidad_id) {
                            throw new \Exception("El préstamo especificado no corresponde a la entidad de la factura.");
                        }

                        $prestamo->factura_compra_id = $model->id;
                        $prestamo->ruc = $model->ruc;

                        if (!$prestamo->save()) {
                            $msg = implode(', ', $prestamo->getErrorSummary(true));
                            throw new \Exception("Error al asociar los prestamos a la compra: {$msg}");
                        }
                    }
                }

                /** ------------------------ MANEJAR STOCK DE ACTIVO BIOLOGICO ------------------------ */
                if ($es_para_act_bio) {
                    ActivoBiologico::isCreable();
                    $session_key = 'cont_activo_bio_stock_manager';

                    if (!Yii::$app->session->has($session_key)) {
                        throw new \Error("Falta especificar la cantidad de A. Biológicos a comprar.");
                    } else {
                        $sessionData = Yii::$app->session->get($session_key);
                        $dataProvider = $sessionData['dataProvider'];
                        $allModels = $dataProvider->allModels;

                        if (empty($allModels)) {
                            throw new \Error("Falta especificar la cantidad de A. Biológicos a comprar.");
                        }

                        /** @var ActivoBiologicoStockManager[] $stock_managers */
                        $stock_managers = $allModels;

                        foreach ($stock_managers as $stock_manager) {
                            $stock_manager->factura_compra_id = $model->id;

                            if (!$stock_manager->datosValidos()) {
                                throw new \Exception("Error interno validando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                            }

                            // guardar manager
                            if (!$stock_manager->save()) {
                                throw  new \Exception("Error interno guardando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                            }
                            $stock_manager->refresh();

                            $actbio = ActivoBiologico::actualizarStock_O_CrearNuevo($stock_manager);
                            // en teoria, si no se puede crear nuevos a.bios porque existe cierre de inventario,
                            // tampoco se puede modificar porque [[isEditable()]] pregunta tambien si no existe
                            // un cierre de inventario de la empresa y periodo del activo biologico.
//                            if (!$actbio->isNewRecord)
//                                $actbio->isEditable();
                            if (!$actbio->save()) {
                                throw new Exception("Error actualizando stock del Activo Biológico: {$actbio->getErrorSummaryAsString()}");
                            }
                            $actbio->refresh();
                        }
                    }
                }
                /** ----------------------------------------------------------------------------------- */

                /** ----------------------- AUTOFACTURA ----------------------- */
                if ($model->tipo_documento_id != '' && $model->tipoDocumento->autofactura == 'si') {
                    $model->nombre_completo_autofactura = ucwords(trim($model->nombre_completo_autofactura));
                    $model->save(false);

                    if (!Compra::parametrosAutofacturaExists()) {
                        throw new \Exception("Ésta es una autofactura pero falta en la empresa actual el parámetro Salario Mínimo.
                          Verifique primero dicho parámetro en la empresa actual.");
                    }
                    $obligacion = $model->obligacion;
                    if ($obligacion->cantidad_salario_max == '') {
                        throw new \Exception("Ésta es una autofactura pero la obligación `{$model->obligacion->nombre}` de esta factura no
                         tiene definida la cantidad máxima de salarios mínimos para Autofacturas.");
                    }

                    $empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

                    $autofacturasQ = Compra::find()->where([
                        'cedula_autofactura' => $model->cedula_autofactura,
                        'empresa_id' => $empresa_id,
                        'periodo_contable_id' => $periodo_id,
                    ]);
//                    $autofacturasQ->andWhere(['!=', 'id', $model->id]); // excluir autofactura actual
                    if ($autofacturasQ->exists()) {
                        /** @var Compra[] $autofacturas */
                        $ingresos = 0;
                        $autofacturas = $autofacturasQ->all();
                        foreach ($autofacturas as $autofactura) {
                            $ingresos += (int)$autofactura->total * ($autofactura->moneda_id == \common\models\ParametroSistema::getMonedaBaseId() ? 1 : round($autofactura->cotizacion));
                        }

                        $salMin = (int)Compra::getSalarioMinimoEmpresaActual();
                        $cantSalMin = (int)$obligacion->cantidad_salario_max;

                        if ($salMin * $cantSalMin <= $ingresos) {
                            $sm = number_format(round($salMin), 0, ',', '.');
                            $limite = number_format(round($salMin * $cantSalMin), 0, ',', '.');
                            throw new \Exception("Ya no se puede registrar más autofacturas debido a que 
                            el cliente `{$model->nombre_completo_autofactura}` supera los {$cantSalMin} salarios mínimos.
                            El Salario Minimo es Gs. {$sm} y {$cantSalMin} salarios son Gs. {$limite}");
                        }

                        foreach ($autofacturas as $autofactura) {
                            if ($autofactura->id != $model->id && $autofactura->nombre_completo_autofactura != $model->nombre_completo_autofactura) {
                                throw new \Exception("El nombre `{$model->nombre_completo_autofactura}` no es
                                 válido. La cédula `{$model->cedula_autofactura}` se encuentra registrado a nombre de
                                  `$autofactura->nombre_completo_autofactura`.");
                            }
                        }
                    }
                }
                /** ----------------------- AUTOFACTURA ----------------------- */

                #Verificar si se invirtio si el tipo de documento set es nota de credito
                if (!$model->isPartidaCorrect())
                    throw new \Exception("Error validando compra: El tipo de documento SET es NOTA DE CREDITO pero las partidas no se han invertido.");

                $trans->commit();

                //TODO
                $valoresUsados = [];
                $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));
                foreach ($model->attributes() as $attr) {
                    if (!in_array($attr, ['ruc']))
                        $valoresUsados[$attr] = $model->$attr;
                }
                $valoresUsados['plantilla_id'] = $cpas_iva_cta_used[0]['plantilla_id'];
                //$valoresUsados['ruc'] = $model->ruc;TODO no se carga de un cliente en particular
                //$valoresUsados['nombre_entidad'] = $model->nombre_entidad; TODO no se carga de un cliente en particular
                //$valoresUsados['timbrado'] = $model->timbrado;
                Yii::$app->session->set('cont_compra_valores_usados', $valoresUsados);

                //Yii::$app->getSession()->set('cont_detallecompra-provider', []);

	            FlashMessageHelpsers::createSuccessMessage("La factura #ID {$model->id} se ha generado correctamente.", 15000);
                if ($model->guardarycerrar == 'no') {
                    Yii::$app->session->remove('cont_poliza_desde_compra');
                    self::clearSession();
                    Yii::$app->session->set('cont_compra_valores_usados', $valoresUsados);
                    return $this->redirect(['create']);
                } else {
                    if (in_array($model->tipo_documento_id, TipoDocumento::getIDsAsociadoSetNotaCredito()))
                        return $this->redirect(['/contabilidad/compra-nota/index']);
                    return $this->redirect(['index']);
                }
            } catch (\Exception $exception) {
                //throw $exception;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 20000);
                $model->id = null;
                $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

                if ($model->condicion == 'credito')
                    $model->fecha_vencimiento = implode('-', array_reverse(explode('-', $model->fecha_vencimiento)));
            }
        }

        retorno:;
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $model Compra
     * @return bool
     * @throws NotAcceptableHttpException
     */
    private function isEditable(&$model)
    {
        // Si hay asiento generado de la factura, bloquear.
        if ($model->asiento != null) {
            FlashMessageHelpsers::createWarningMessage('No puede modificar facturas que tienen asientos asociados.');
            return false;
        }

        // Si hay asiento por devengamiento de poliza, bloquear.
        if ($model->poliza != null && $model->poliza->tieneAsiento()) {
            FlashMessageHelpsers::createWarningMessage('Existen asientos generados para la póliza registrada en esta factura. Por tanto no puede modificar esta factura.');
            return false;
        }

        // Si algun activo fijo tiene asociado factura de venta, bloquear
        $result = [];
        foreach ($model->activoFijos as $activoFijo) {
            if (isset($activoFijo->venta)) {
                $result[] = 'Existe una Factura de Venta Nº '
                    . $activoFijo->venta->getNroFacturaCompleto()
                    . ' asociada al Activo Fijo ' . $activoFijo->nombre
                    . ' asociado a esta Factura.';
            }
            if (isset($activoFijo->asiento)) {
                $result[] = 'Existe un Asiento generado de concepto: \"'
                    . $activoFijo->asiento->concepto
                    . '\" asociada al Activo Fijo ' . $activoFijo->nombre . '. Elimínelo primero.';
            }
        }
        if (sizeof($result) > 0) {
            $msg = '';
            foreach ($result as $item) {
                $msg .= $item . '<br/>';
            }
            FlashMessageHelpsers::createWarningMessage($msg);
            return false;
        }

        // Si tiene asociado recibo

        if (ReciboDetalle::findOne(['factura_compra_id' => $model->id]) != null) {
            FlashMessageHelpsers::createWarningMessage("Esta Factura tiene asociado uno o varios Recibos.");
            return false;
        }

        $actbios = $model->activosBiologicos;
        foreach ($actbios as $actbio) {
            // si registra compra de a.bio y tambien hay venta de ese a.bio, no permitir modificar.
            if ($actbio->getVentas()->exists()) {
                FlashMessageHelpsers::createWarningMessage("Hay Activos Biológicos asociados a esta factura que también poseen factura(s) de venta(s).");
                return false;
            }
            // si el activo biologico esta en el cierre de inventario, no puede modificar.
            if ($actbio->getInventario()->exists()) {
                FlashMessageHelpsers::createWarningMessage("Los activos biológicos comprados por esta factura fueron registrados en el cierre de inventario.");
                return false;
            }
        }

        // si tiene notas, no se puede modificar
        if ($model->getNotas()->exists()) {
            FlashMessageHelpsers::createWarningMessage("Hay una o más notas asociadas a esta factura.");
            return false;
        }

        // si tiene retenciones
        if ($model->getRetencion()->exists()) {
            FlashMessageHelpsers::createWarningMessage("Hay retencion asociada esta factura.");
            return false;
        }

        return true;
    }

    /**
     * Updates an existing Compra model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Exception
     * @throws \Throwable
     */
    public function actionUpdate($id, $_pjax = null)
    {
        $model = $this->findModel($id);

        if (!$this->isEditable($model)) {
            return $this->redirect(['index']);
        }
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
        $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

        $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
        $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

        if (!isset($parmsys_debe)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Ganancia.");
            return $this->redirect(['index']);
        }
        if (!isset($parmsys_haber)) {
            FlashMessageHelpsers::createWarningMessage("Falta definir en el parámetro de la empresa, la cuenta correspondiente a Resultado Por Diferencia de Cambio como Pérdida.");
            return $this->redirect(['index']);
        }

        $entidad = Entidad::findOne(['id' => $model->entidad_id]);
        $model->ruc = $entidad != null ? $entidad->ruc : null;
        $model->nombre_entidad = $entidad != null ? $entidad->razon_social : null;

        $oldTimbradoDetalle = $model->timbradoDetalle;
        $model->timbrado = $model->timbradoDetalle != null ? $model->timbradoDetalle->timbrado->nro_timbrado : null;
        $model->timbrado_id = $model->timbradoDetalle != null ? $model->timbradoDetalle->timbrado_id : null;
        $model->guardarycerrar = 'si';
        $session = Yii::$app->session;

        if ($model->timbradoDetalle != null)
            //Si tiene timbrado vencido
            if ($model->fecha_emision > $model->timbradoDetalle->timbrado->fecha_fin)
                $model->timbrado_vencido = 1;
            else
                $model->timbrado_vencido = 0;

        $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

        // Si tiene poliza
        $poliza = $model->poliza;
        if (isset($poliza) && !$session->has('cont_poliza_desde_compra')) {
            $poliza->poliza_id_selector = $poliza->id;
            $session->set('cont_poliza_desde_compra', new ArrayDataProvider([
                'allModels' => [$poliza,],
                'pagination' => false,
            ]));
        }

        if (!Yii::$app->request->isPost && $_pjax == null) {
            $session->remove('cont_actfijo_desde_factura');
            $session->remove('cont_actfijo_pointer');

            // Si tiene activo fijo
            $query = ActivoFijo::find()->where(['factura_compra_id' => $model->id]);
            $query->select([
                'MIN(id) as id',
                'MIN(costo_adquisicion) as costo_adquisicion',
                'MIN(fecha_adquisicion) as fecha_adquisicion',
                'MIN(nombre) as nombre',
                'MIN(activo_fijo_tipo_id) as activo_fijo_tipo_id',
                'MIN(vida_util_fiscal) as vida_util_fiscal',
                'MIN(vida_util_restante) as vida_util_restante',
                'MIN(valor_fiscal_neto) as valor_fiscal_neto',
                'MIN(cuenta_id) as cuenta_id',
                'COUNT(id) as cantidad'
            ]);
            $query->groupBy(['cont_activo_fijo.nombre']);
            if ($query->exists() && !Yii::$app->session->has('cont_actfijo_desde_factura')) {
                $allModels = [];
                $cotizacion = $model->moneda_id != \common\models\ParametroSistema::getMonedaBaseId() ?
                    Cotizacion::findOne(['fecha' => date('Y-m-d', strtotime('-1 day', strtotime($model->fecha_emision))), 'moneda_id' => $model->moneda_id]) : null;

                $aFijosCompra = [];
                /** @var ActivoFijo $item */
                /** @var ActivoFijoAtributo[] $atributos */
                /** @var ActivoFijoAtributo[] $atribs_cm */
                /** @var ActivoFijoTipoAtributo[] $_atributos */
                foreach ($query->all() as $item) {
                    // estos costos se guardan valorizados por lo que se des-valoriza segun moneda de la factura
                    $item->costo_adquisicion /= isset($cotizacion) ? $cotizacion->venta : 1;
                    $item->valor_fiscal_neto /= isset($cotizacion) ? $cotizacion->venta : 1;
                    $atributos = [];
                    $atribs_cm = [];
                    $_atributos = ActivoFijoTipoAtributo::find()->where(['activo_fijo_tipo_id' => $item->activo_fijo_tipo_id])->all();
                    foreach ($item->atributos as $atributo) { // para que no se haga update al guardar sin modificar el activo fijo de la sesion
                        $newAtributo = new ActivoFijoAtributo();
                        $newAtributo->empresa_id = $atributo->empresa_id;
                        $newAtributo->periodo_contable_id = $atributo->periodo_contable_id;
                        $newAtributo->activo_fijo_id = $atributo->activo_fijo_id;
                        $newAtributo->activo_fijo_tipo_id = $atributo->activo_fijo_tipo_id;
                        $newAtributo->atributo = $atributo->atributo;
                        $newAtributo->valor = $atributo->valor;
                        $atributos[] = $newAtributo;
                        $atribs_cm[] = $newAtributo;
                    }

                    foreach ($_atributos as $_atributo) {
                        $hay = false;
                        foreach ($atribs_cm as $atrib_cm) {
                            if ($atrib_cm->atributo == $_atributo->atributo) {
                                $hay = true;
                                break;
                            }
                        }

                        if (!$hay) {
                            $_newAtributo = new ActivoFijoAtributo();
                            $_newAtributo->loadEmpresaPeriodo();
                            $_newAtributo->atributo = $_atributo->atributo;
                            $_newAtributo->valor = '';
                            $atributos[] = $_newAtributo;
                        }
                    }

                    $aFijosCompra[] = [
                        'model' => $item,
                        'atributos' => $atributos,
                    ];
                }
                Yii::$app->session->set('cont_actfijo_desde_factura', $aFijosCompra);
            }
        }

        // si tiene cuotas prestamos
        if ($model->getPrestamos()->exists()) {
            $skey1 = 'cont_prestamos_compra';

            if ($_pjax == null && !Yii::$app->request->isPost) {
                $data = [];
                /** @var Prestamo[] $prestamos */
                $prestamos = $model->getPrestamos()->all();


                foreach ($prestamos as $key => $prestamo) {
                    $p_det_a_mostrar = [];

                    $prestamos[$key]->refillCalculatedFields();
                    $prestamos[$key]->formatDateField('view');
                    $prestamo_detalles = $prestamos[$key]->prestamoDetalles;

                    foreach ($prestamo_detalles as $key2 => $_) {
                        $_prestamod_compra = PrestamoDetalleCompra::findOne(['factura_compra_id' => $model->id, 'prestamo_detalle_id' => $prestamo_detalles[$key2]->id]);


                        if (isset($_prestamod_compra)) {
                            $prestamo_detalles[$key2]->formatDateField('view');
                            $prestamo_detalles[$key2]->capital_saldo = (float)$_prestamod_compra->capital;
                            $prestamo_detalles[$key2]->interes_saldo = (float)$_prestamod_compra->interes;
                            $prestamo_detalles[$key2]->monto_cuota_saldo = (float)$_prestamod_compra->monto_cuota;
                            $prestamo_detalles[$key2]->selected = '1';
                            $prestamo_detalles[$key2]->recibo_capital_nro = $_prestamod_compra->recibo_capital_nro;

                        }

                        // Incluir y mostrar cuotas con saldo > 0, mas cuotas asociadas a este recibo
                        // Copiado desde ReciboPrestamo actionUpdate()
                        $saldo_mayor_0 = ($prestamo_detalles[$key2]->capital_saldo > 0) &&
                            ($prestamo_detalles[$key2]->interes_saldo > 0) &&
                            ($prestamo_detalles[$key2]->monto_cuota_saldo > 0);
                        if ($saldo_mayor_0 || $prestamo_detalles[$key2]->selected == '1')
                            $p_det_a_mostrar[] = $prestamo_detalles[$key2];
                    }

                    $data[] = [
                        'prestamo' => $prestamo,
                        'detalles' => $p_det_a_mostrar,
                    ];
                }
                $session->set($skey1, $data);
            }
        }

        // Cargar en session datos relacionados con la cant de act bio comprados, si existe.
        $query = ActivoBiologicoStockManager::find()->where(['factura_compra_id' => $id]);

        if ($query->exists() && $_pjax == null && !Yii::$app->request->isPost) {
            $allModels = $query->all();
            Yii::$app->session->set('cont_activo_bio_stock_manager', ['compra_id' => $id, 'dataProvider' => new ArrayDataProvider([
                'allModels' => $allModels,
                'pagination' => false,
            ])]);
        }

        if (array_key_exists('CompraIvaCuentaUsada', $_POST)) {
            if (!empty($_POST['CompraIvaCuentaUsada']['__id__'])) { // eliminar el blue print
                unset($_POST['CompraIvaCuentaUsada']['__id__']);
                Yii::$app->request->setBodyParams($_POST);
                foreach ($_POST['CompraIvaCuentaUsada'] as $new_keys => $new) {
                    if ($new['plantilla_id'] == "") {
                        unset($_POST['CompraIvaCuentaUsada'][$new_keys]); // eliminar items que tienen plantilla_id vacio.
                        Yii::$app->request->setBodyParams($_POST);
                    }
                }
            }
        }

        if ($_pjax == null && !Yii::$app->request->isPost) {
            $detalles = new ArrayDataProvider([
                'allModels' => DetalleCompra::find()->where(['factura_compra_id' => $model->id])->orderBy('cta_contable')->all(),
                'pagination' => false,
            ]);
            $session->set('cont_detallecompra-provider', $detalles);

            /** @var CompraIvaCuentaUsada [] $cpa_iva_cta_used */
            $cpa_iva_cta_used = CompraIvaCuentaUsada::find()
                ->where(['factura_compra_id' => $model->id, 'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
                ->select('SUM(monto) as monto, iva_cta_id, plantilla_id, MIN(gnd_motivo) as gnd_motivo')
                ->groupBy('plantilla_id, iva_cta_id')
                ->all();
            $monto_totales = [];
            $total_iva = 0.0;
            $totales = [];
            foreach ($cpa_iva_cta_used as $monto_iva_usada) {
                $monto = $monto_iva_usada->monto;
                if ($model->moneda_id == 1) {
                    $monto = round($monto);
                }

//                $monto = ValorHelpers::numberFormatMonedaSensitive($monto, 2, $model->moneda);

                $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id]['gnd_motivo'] = $monto_iva_usada->gnd_motivo;
                if (isset($monto_iva_usada->ivaCta)) {
                    $totales['totales_iva-iva-' . $monto_iva_usada->ivaCta->iva->porcentaje . '-disp'] = (float)$monto;
                    $totales['totales_iva-impuesto-' . $monto_iva_usada->ivaCta->iva->porcentaje . '-disp'] = (float)round($monto_iva_usada->monto - ($monto_iva_usada->monto / (1.0 + $monto_iva_usada->ivaCta->iva->porcentaje / 100.0)));

                    $total_iva += round($monto_iva_usada->monto - ($monto_iva_usada->monto / (1.0 + $monto_iva_usada->ivaCta->iva->porcentaje / 100.0)));

                    $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id][$monto_iva_usada->ivaCta->iva->porcentaje] = $monto_iva_usada->monto;
                } else {
                    $monto_totales[] = [
                        'totales_iva-iva-0-disp' => $monto
                    ];

                    $model->_iva_ctas_usadas[$monto_iva_usada->plantilla_id]['0'] = $monto_iva_usada->monto;
                }
            }
            foreach ($totales as $index => $t) {
                $monto_totales[] = [$index, $t];
            }

            $monto_totales[] = ['compra-_total_ivas-disp' => $total_iva];
            $session->set('cont_monto_totales_compra', $monto_totales);

            // Si hay prestamos facturados
            $data = [];
            foreach (Prestamo::findAll(['factura_compra_id' => $id]) as $prestamo) {
                $data[] = $prestamo->id;
            }
            if (sizeof($data) > 0)
                $session->set('cont_prestamo_facturar', $data);

            // Borrar datos de sesion relacionado a activos fijos.
            $session->remove('cont_afijo_atributos');
            $session->remove('cont_compra_actfijo_tipo_id');

            // Warning si la plantilla utilizada ya no pertenece a la obligacion de la compra
            /** @var CompraIvaCuentaUsada[] $ivaCtaUsadas */
            $ivaCtaUsadas = CompraIvaCuentaUsada::find()->where(['factura_compra_id' => $id])->all();
            $plantillas = [];
            foreach ($ivaCtaUsadas as $ivaCtaUsada) {
                $pertenece = false;
                foreach ($ivaCtaUsada->plantilla->plantillaObligaciones as $plantillaObligacion) {
                    if ($plantillaObligacion->obligacion_id == $model->obligacion_id) {
                        $pertenece = true;
                        break;
                    }
                }
                if (!$pertenece) {
                    if (!in_array($ivaCtaUsada->plantilla_id, $plantillas))
                        $plantillas[] = $ivaCtaUsada->plantilla_id;
                }
            }
            if (sizeof($plantillas)) {
                foreach ($plantillas as $key => $plantilla_id) {
                    $plantillas[$key] = PlantillaCompraventa::findOne(['id' => $plantilla_id])->nombre;
                }
                $submsg = "";
                if (sizeof($plantillas) == 1) {
                    $submsg = "La plantilla `{$plantillas[0]}` usada anteriormente ya no pertenece";
                } else {
                    $plantillas = implode(', ', $plantillas);
                    $submsg = "Las plantillas `{$plantillas}` usadas anteriormente ya no pertenecen";
                }
                FlashMessageHelpsers::createWarningMessage("{$submsg} a la obligación `{$model->obligacion->nombre}` de esta factura.");
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $entidad = Entidad::findOne(['ruc' => $model->ruc]);
            $model->entidad_id = $entidad != null ? $entidad->id : null;
            /** @var IvaCuenta $iva_cta */
            $totales_tabla = $_POST['CompraIvaCuentaUsada'];

            $model->_iva_ctas_usadas = $this->cargarIvaCtaUsada($model, $totales_tabla);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->tipo_documento_id != '') {
                    $tipodoc = TipoDocumento::findOne(['id' => $model->tipo_documento_id]);
                    if (isset($tipodoc)) {
                        $model->condicion = $tipodoc->condicion;
                    }
                }
                if (!empty($model->notas)) {
                    $msg = 'No se puede editar una factura que ya tiene una nota asociada.';
                    FlashMessageHelpsers::createErrorMessage($msg);
                    throw new Exception($msg);
                }

                $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

                if ($model->condicion == 'credito')
                    $model->fecha_vencimiento = implode('-', array_reverse(explode('-', $model->fecha_vencimiento)));

                $model->total = 0; // se va a recalcular para evitar manipulaciones.

                //Verificar timbrado con numero de factura
                //Actualizar número actual en timbrado detalle
                $nro = (int)substr($model->nro_factura, 8, 15);
                $prefijo = substr($model->nro_factura, 0, 7);

	            if (!$model->tipoDocumento->isTipoSetNinguno()) {
                    /** @var TimbradoDetalle $timbradoDetalle */
                    $timbradoDetalle = TimbradoDetalle::find()
                        ->joinWith('timbrado as timbrado')
                        ->where([
                            'timbrado.id' => $model->timbrado_id,
                            'prefijo' => $prefijo,
                            'tipo_documento_set_id' => $model->tipoDocumento->tipo_documento_set_id,
                        ])
                        ->andWhere(['<=', 'nro_inicio', $nro])
                        ->andWhere(['>=', 'nro_fin', $nro])
                        ->one();
                    if ($timbradoDetalle == null) {
                        //No existe el numero de factura en el rango del timbrado
                        //Se crea un detalle para el timbrado
                        $newDetalleTimbrado = new TimbradoDetalle2();  #Se usa esta clase para que valide que no haya otro tim con fechas y prefijos iguales
                        $newDetalleTimbrado->prefijo = $prefijo;
                        $newDetalleTimbrado->timbrado_id = $model->timbrado_id;
                        $newDetalleTimbrado->nro_inicio = $nro;
                        $newDetalleTimbrado->nro_fin = $nro;
                        $newDetalleTimbrado->nro_actual = $nro;
                        $newDetalleTimbrado->tipo_documento_set_id = $model->tipoDocumento->tipo_documento_set_id;
                        if (!$newDetalleTimbrado->save()) {
                            throw new \Exception("Error guardando nuevo detalle timbrado: {$newDetalleTimbrado->getErrorSummaryAsString()}");
                        }
                        $newDetalleTimbrado->refresh();
                        $model->timbrado_detalle_id = $newDetalleTimbrado->id;
                    } else {
                        $timbradoDetalle->nro_actual = $nro; # TODO: Se debe actualizar? No es solo desde ventas?
                        if (!$timbradoDetalle->save()) {
                            throw new \Exception("Error guardando timbrado detalle: {$timbradoDetalle->getErrorSummaryAsString()}");
                        }
                        $timbradoDetalle->refresh();
                        $model->timbrado_detalle_id = $timbradoDetalle->id;
                    }
                }

                //Tipo documento para importación
                $tipoDocParaImportacion = \backend\modules\contabilidad\helpers\ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-periodo-{$model->periodo_contable_id}-tipo_doc_importacion_id");

                // Verificar si la plantilla tiene detalles
                foreach ($totales_tabla as $total) {
                    if ($total['plantilla_id'] != '') {
                        $plantilla = PlantillaCompraventa::findOne($total['plantilla_id']);
                        if (!$plantilla->esParaPrestamo() && !$plantilla->esParaCuotaPrestamo()) {
                            $query = PlantillaCompraventa::findOne(['id' => $total['plantilla_id']])->getPlantillaCompraventaDetalles();
                            if (!$query->exists())
                                throw new \Exception('La plantilla seleccionada con ID: ' . $total['plantilla_id'] . ' no tiene detalles. AGREGUE PRIMERO y luego vuelva a crear la factura.');
                        }

                        //Validar plantilla para importación
                        if ($tipoDocParaImportacion == $model->tipo_documento_id && $plantilla->para_importacion == 'no')
                            throw new \Exception('La plantilla seleccionada con ID: ' . $total['plantilla_id'] . ' no corresponde a una plantilla de importación.');
                    }
                }

                //Verificar vigencia del timbrado
	            if (!$model->tipoDocumento->isTipoSetNinguno()) {
                    if (strtotime($model->timbradoDetalle->timbrado->fecha_fin) < strtotime($model->fecha_emision)) {
                        foreach ($totales_tabla as $total) {
                            if ($total['plantilla_id'] != '') {
                                if (PlantillaCompraventa::findOne(['id' => $total['plantilla_id']])->deducible == 'si') {
                                    throw new \Exception('Solo se pueden agregar plantillas no deducibles');
                                }
                            }
                        }
                    }
                }

                // No permitir guardar si elige moneda extrangera y no existe cargado de la set.
                $fecha = date('Y-m-d', strtotime('-1 day', strtotime($model->fecha_emision)));
                $moneda_id = $model->moneda_id;
                /** @var Cotizacion $query */
                $query = Cotizacion::find()
                    ->where(['fecha' => $fecha, 'moneda_id' => $moneda_id])
                    ->andWhere(['!=', 'moneda_id', ParametroSistemaHelpers::getValorByNombre('moneda_base') != null ? Moneda::findOne(['id' => ParametroSistemaHelpers::getValorByNombre('moneda_base')])->id : 1])
                    ->one();
                if ($model->moneda_id != 1 && ($query == null || $model->cotizacion == '')) {
                    $transaction->rollBack();
                    if ($query == null) FlashMessageHelpsers::createWarningMessage('Falta cargar cotización de la SET.');
                    elseif ($model->cotizacion == '') throw new Exception('Falta especificar valor de la moneda');
                }

                //Iva cuentas en base de datos
                $cpas_db = CompraIvaCuentaUsada::findAll([
                    'factura_compra_id' => $model->id
                ]);

                //Borrar iva cuenta usadas antes de cargar los nuevos
                foreach ($cpas_db as $cpa_db) {
                    if (!$cpa_db->delete()) {
                        throw new Exception('ERROR INTERNO al guardar factura: No se puede borrar Iva Cuenta Usadas');
                    }
                }

                // Registrar cada uno de los montos de los ivas, junto con las cuentas usadas.
                $ningun_total = true;
                $plantillas_ids = []; // para verificar que no haya duplicados
                $cpas_iva_cta_used = [];
                $compra_de_activofijo = false;
                $es_para_cuota_prestamo = false;
                $es_para_fact_de_prestamo = false;
                $es_para_act_bio = false;
                $plantilla_id = null;
                $plantilla_actfijo_id = null;
                $subtotal_plantilla_actfijo = 0.0;
                $plantilla_para_importacion = null;
                foreach ($totales_tabla as $total) {
                    foreach (IvaCuenta::find()->all() as $iva_cta) {
                        if (isset($total['ivas']['iva_' . $iva_cta->iva->porcentaje]) && $total['ivas']['iva_' . $iva_cta->iva->porcentaje] != '') {
                            $cpa_iva_cta_used = new CompraIvaCuentaUsada();
                            $cpa_iva_cta_used->iva_cta_id = isset($iva_cta->cuenta_compra_id) ? $iva_cta->id : null;
                            $cpa_iva_cta_used->factura_compra_id = $model->id;
                            $cpa_iva_cta_used->plan_cuenta_id = $iva_cta->cuenta_compra_id; // cuenta_compra_id puede ser vacio
                            $cpa_iva_cta_used->periodo_contable_id = Yii::$app->session->get('core_empresa_actual_pc');
                            $cpa_iva_cta_used->plantilla_id = $total['plantilla_id'];

                            //Se convierte números de string a float, por como viene en el post
                            $monto = $total['ivas']['iva_' . $iva_cta->iva->porcentaje];
                            $monto = floatval(str_replace([".", ","], ["", "."], $total['ivas']['iva_' . $iva_cta->iva->porcentaje]));
                            $cpa_iva_cta_used->monto = $monto;

                            // Verificar motivo de gnd
                            $plantilla = PlantillaCompraventa::findOne(['id' => $cpa_iva_cta_used->plantilla_id]);
                            if (isset($plantilla) && $plantilla->deducible == 'no') {
                                if (!isset($total['gnd_motivo']))
                                    throw new \Exception("Error en la fila de plantilla {$plantilla->nombre}: Motivo por el que se envia a GND no puede estar vacio.");
                                $cpa_iva_cta_used->gnd_motivo = $total['gnd_motivo'];
                            }

                            $cpas_iva_cta_used [] = $cpa_iva_cta_used;

                            if (!$cpa_iva_cta_used->validate() || !$cpa_iva_cta_used->save()) {
                                throw new Exception('ERROR INTERNO al guardar factura: ' . $cpa_iva_cta_used->getErrorSummaryAsString());
                            };
                            $ningun_total = false;

                            $plantilla_id = $total['plantilla_id'];
                            $plantilla = PlantillaCompraventa::findOne(['id' => $plantilla_id]);
                            if ($plantilla->esParaActivoFijo()) {
                                $subtotal_plantilla_actfijo += round($monto / ((100 + $iva_cta->iva->porcentaje) / 100));
                            } elseif ($plantilla->esParaCuotaPrestamo()) {
                                $es_para_cuota_prestamo = true;
                            } elseif ($plantilla->esParaPrestamo()) {
                                $es_para_fact_de_prestamo = true;
                            } elseif ($plantilla->esParaActivoBiologico()) {
                                $es_para_act_bio = true;
                            }
                            $idsObls = [];
                            foreach ($plantilla->obligaciones as $obligacion) {
                                in_array($obligacion->id, $idsObls) || $idsObls[] = $obligacion->id;
                            }
                            if (!in_array($model->obligacion_id, $idsObls)) {
                                throw new \Exception("La plantilla {$plantilla->nombre} no pertenece a la obligación {$model->obligacion->nombre} seleccionada.");
                            }

                            //Controlar que no se mezclen plantillas normales con plantillas de importación
                            if ($plantilla->para_importacion != 'no') {
                                if ($plantilla_para_importacion != null && !$plantilla_para_importacion)
                                    throw new \Exception("No puede mezclar plantillas de importación con plantillas normales.");
                                $plantilla_para_importacion = true;
                            } else {
                                if ($plantilla_para_importacion != null && $plantilla_para_importacion)
                                    throw new \Exception("No puede mezclar plantillas de importación con plantillas normales.");
                                $plantilla_para_importacion = false;
                            }

                            // Controlar que no mezclen plantillas de prestamo con otros
                            if ($plantilla->esParaPrestamo() && sizeof($totales_tabla) > 1)
                                throw new \Exception("Error en el uso de plantillas: No puede mezclar plantilla de Préstamo con ninguna otra plantilla.");

                            // Controlar que el tipo de doc sea credito si la plantilla es de prestamo:
                            // Segun asistencia presencial del 24 de enero y segun el acta de acuerdo de entendimiento,
                            // la factura de prestamo debe ser credito.
                            if ($plantilla->esParaPrestamo() && $model->tipoDocumento->condicion != 'credito')
                                throw new \Exception("Error en el uso del tipo de documento: Si va a utilizar plantilla para prestamo, el tipo de documento debe ser credito.");
                            $empresa_id = Yii::$app->session->get('core_empresa_actual');
                            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                            $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-plantilla_seguro_a_devengar_id";
                            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);

                            if (!isset($parametro_sistema)) {
                                throw new \Exception("Falta definir en el parámetro de la empresa, la Plantilla para Seguros a Devengar.");
                            }

                            if ($parametro_sistema->valor == $plantilla_id && sizeof($totales_tabla) > 1) {
                                throw new \Exception('Ud. no puede mezclar cuentas para SEGUROS A DEVENGAR con cuentas de otra plantilla.');
                            }
                        }
                    }
                    if (key_exists($total['plantilla_id'], $plantillas_ids)) {//Duplicado, se retorna con error
                        throw new Exception('ERROR INTERNO al guardar factura: ' . 'Hay plantillas duplicadas, por favor corrija.');
                    } else {
                        $plantillas_ids [$total['plantilla_id']] = $total['plantilla_id'];
                    }

                    if (isset($plantilla_id) && PlantillaCompraventa::findOne(['id' => $plantilla_id])->esParaActivoFijo()) {
                        $compra_de_activofijo = true;
                        $plantilla_actfijo_id = $plantilla_id;
                    }
                }

                if ($ningun_total) {
                    $transaction->rollBack();
                    //$model = $this->newModel($model);
                    $model->id = null;
                    throw new Exception('Falta completar al menos un campo del Total Iva o Total Exenta.');
                }

                // No permitir guardar factura sin detalle
                $detallesCompra = $session['cont_detallecompra-provider']->allModels;
                if (sizeof($detallesCompra) == 0)
                    throw new Exception('Faltan los detalles de la factura');

                $importe_gravado_poliza = 0.0;
                $compra_de_poliza = false;
                $id_cta_seguro = PlantillaCompraventa::getIdCuentaSeguroADevengar();
                $saldo = ['debe' => 0.0, 'haber' => 0.0];
                $hayCtaPrincipal = false;
                /** @var DetalleCompra $detalle */
                foreach ($detallesCompra as $detalle) {
                    $saldo[$detalle->cta_contable] += round($detalle->subtotal, 2);
                    $detalle->factura_compra_id = $model->id;
                    if (!$detalle->validate())
                        throw new Exception('Error en detalle de factura: ' . $detalle->planCuenta->nombre . ": " . $detalle->getErrorsEnString());

                    // Seguro a devengar
                    if ($detalle->plan_cuenta_id == $id_cta_seguro) {
                        $compra_de_poliza = true;
                        $importe_gravado_poliza = round($detalle->subtotal, 2);
                    }

                    if ($detalle->cta_principal == 'si')
                        $hayCtaPrincipal = true;
                }

                if (!$hayCtaPrincipal) {
                    throw new \Exception("Se necesita establecer al menos una cuenta como principal.");
                }

                // TODO: Verificar balance entre saldo acreedor y deudor (debe y haber). Asignar total factura.
                if (round($saldo['debe'], 2) != round($saldo['haber'], 2)) {
                    FlashMessageHelpsers::createWarningMessage('Se recomienda verificar cómo está guardada la plantilla utilizada y también revisar los detalles y las simulaciones generados automáticamente.');
                    $model->id = null;
                    throw new Exception('El DEBE y el HABER NO son iguales. Verifique.');
                }

                $model->total = $saldo['haber']; // para evitar trampa
                $model->saldo = $model->condicion == 'contado' ? 0.0 : $model->total;

	            if (!$model->tipoDocumento->isTipoSetNinguno()) {
                    $result = Compra::checkNroFactura($model->nro_factura, $model->id, 'factura', $model->entidad_id);
                    if ($result != Compra::NRO_FACTURA_CORRECTO && $result != Compra::NRO_FACTURA_DUPLICADO) {
                        $msg = '';
                        switch ($result) {
                            case Compra::NRO_FACTURA_NO_RANGO:
                                $msg = 'Este nro de factura no corresponde a ningún rango de ningún timbrado.';
                                break;
                            case Compra::NRO_FACTURA_NO_PREFIJO:
                                $msg = 'Este prefijo de factura no existe. DEBE CREAR desde el menú de Timbrado primero.';
                        }
                        throw new Exception($msg);
                    }
                }

                // Guardar los models y comprometer transaccion
                $detalles_bd = $model->detallesCompra;
                foreach ($detallesCompra as $detalle) {
                    $index = array_search($detalle, $detallesCompra);
                    /** @var DetalleCompra $detalle_bd */
                    foreach ($detalles_bd as $detalle_bd) {
                        if ($detalle_bd->plan_cuenta_id == $detalle->plan_cuenta_id && $detalle_bd->cta_contable == $detalle->cta_contable) {
                            $detalle_bd->subtotal = $detalle->subtotal;

                            $detalle = $detalle_bd;
                            break;
                        }
                    }

                    if (!$detalle->save()) {
                        throw new \Exception("Error guardando detalle: {$detalle->getErrorSummaryAsString()}");
                    }
                    $detallesCompra[$index] = $detalle;
                }

                //Borramos de la base de datos si corresponde
                /** @var DetalleCompra $detalle_bd */
                foreach ($detalles_bd as $detalle_bd) {
                    $encontrado = false;
                    foreach ($detallesCompra as $detalle) {
                        if ($detalle_bd->id == $detalle->id) {//encontrado
                            $encontrado = true;
                            break;
                        }
                    }

                    if (!$encontrado) {
                        $detalle_bd->delete();//borramos de la base de datos
                    }
                }

	            if (!$model->save()) {
		            throw new \Exception("Error guardando compra: {$model->getErrorSummaryAsString()}");
	            }
                $model->refresh();

                # Unificar talonarios
	            if (!$model->tipoDocumento->isTipoSetNinguno())
                    try {
                        $timbrado = $model->timbradoDetalle->timbrado;
                        $timbrado->unificarTalonarios();
                    } catch (\Exception $exception) {
                        throw new \Exception("Error interno unificando talonarios: {$timbrado->getErrorSummaryAsString()}");
                    }

                /** ---------------- VERIFICAR POLIZA Y ASOCIAR ----------------- */
                // Suponemos que no hay dos plantillas para poliza, que seguro se filtro mas arriba en el controller.
                // Hay detalle por SEGURO A DEVENGAR y en la sesion esta el id de la poliza.
                if ($compra_de_poliza && Yii::$app->session->get('cont_poliza_desde_compra') != null) {

                    /** @var Poliza $poliza */
                    $dataProvider = Yii::$app->session->get('cont_poliza_desde_compra');
                    $poliza = $dataProvider->allModels[0];

                    // Eliminar devengamientos de poliza inicialmente asociada, si ninguno de sus devengamientos
                    // estuvieran asentados. Resetear importe diario y el id de compra en la cabecera.
                    $old_poliza = Poliza::findOne(['factura_compra_id' => $model->id]);
                    if ($poliza->id == null || (isset($old_poliza) && $poliza->id != $old_poliza->id)) { // Se creo nueva poliza desde modal o se cambio de poliza.
                        if (isset($old_poliza)) { // Si habia poliza asociada antes.
                            foreach ($old_poliza->devengamientos as $old_pol_dev) {
                                if (!isset($old_pol_dev->asiento)) {
                                    $old_pol_dev->delete();
                                } else {
                                    $msg = "Error borrando devengamientos de la póliza {$old_poliza->nro_poliza}: ";
                                    $venc = implode('-', array_merge(explode('-', $old_pol_dev->anho_mes)));
                                    $msg .= "El devengamiento {$venc} se encuentra asentado. ";
                                    $msg .= "Por lo tanto, no puede alterar los datos de esta póliza ni sus devengamientos.";
                                    throw new Exception($msg);
                                }
                            }
                            $old_poliza->importe_diario = 0.0;
                            $old_poliza->factura_compra_id = "";
                            if (!$old_poliza->save(false)) {
                                throw new \Exception("Error restableciendo poliza: {$old_poliza->getErrorSummaryAsString()}");
                            }
                            $old_poliza->refresh();
                        }
                    }

                    // Procesar poliza elegida.
                    $poliza->factura_compra_id = $model->id;
                    $poliza->importe_gravado = $importe_gravado_poliza * ($model->moneda_id == \common\models\ParametroSistema::getMonedaBaseId() ? 1 : $model->cotizacion);
                    $poliza->generarImporteDiario();
                    if ($poliza->importe_diario == 0.0) {
                        throw new Exception('Falta especificar monto del Seguro A Devengar.');
                    }
                    if (!$poliza->save(false)) {
                        throw new \Exception("Error guardando poliza: {$poliza->getErrorSummaryAsString()}");
                    }
                    $poliza->refresh();
                    Yii::warning("importe es: {$poliza->importe_diario}");
                    $result = $poliza->generarDevengamientos($model->fecha_emision);
                    if ($result['error']) {
                        $transaction->rollBack();
                        throw new Exception($result['error']);
                    }
                } // Si no hay detalle por SEGURO A DEVENGAR pero la factura inicialmente tenía.
                elseif (!$compra_de_poliza) {
                    $poliza = Poliza::findOne(['factura_compra_id' => $model->id]);
                    if ($poliza != null) {
                        $poliza->factura_compra_id = "";
                        $poliza->importe_diario = 0.0;
                        if (!$poliza->save(false)) {
                            throw new \Exception("Error guardando poliza: {$poliza->getErrorSummaryAsString()}");
                        }
                        $poliza->refresh();
                        foreach ($poliza->devengamientos as $devengs) {
                            if (!isset($devengs->asiento)) {
                                $devengs->delete();
                            } else {
                                $msg = "Error borrando devengamientos de la póliza {$poliza->nro_poliza}: ";
                                $venc = implode('-', array_merge(explode('-', $devengs->anho_mes)));
                                $msg .= "El devengamiento {$venc} se encuentra asentado. ";
                                $msg .= "Por lo tanto, no puede alterar los datos de esta póliza ni sus devengamientos.";
                                throw new Exception($msg);
                            }
                        }
                    }
                }
                // Si hay detalle por SEGURO A DEVENGA pero no existe poliza asociada...
                // ...se pudo haber borrado la poliza, o agrego nvo detalle por SEGURO A DEVNGAR pero se olvido de asociar a una poliza
                elseif ($compra_de_poliza && Yii::$app->session->get('cont_poliza_desde_compra') == null) {
                    throw new Exception('Falta asociar a una póliza');
                }
                /** ---------------- VERIFICAR POLIZA Y ASOCIAR ----------------- */

                /** ---------------- VERIFICAR ACTIVO FIJO Y ELIMINAR/CREAR ----------------- */
                $aFijosCompra = Yii::$app->session->get('cont_actfijo_desde_factura', null);
                if ($compra_de_activofijo) {
                    // Verifica si hay activos fijos.
                    if (!isset($aFijosCompra) || sizeof($aFijosCompra) == 0) {
                        throw new Exception('Esta factura registra detalle por Activo Fijo comprado pero Ud. no ha especificado los datos del mismo.');
                    }

                    // Borrar los activos fijos anteriores.
                    foreach ($model->activoFijos as $activoFijo) {
                        foreach ($activoFijo->atributos as $atributo) {
                            if (!$atributo->delete())
                                throw new \Exception("Error interno borrando atributos del activo fijo: {$atributo->getErrorSummaryAsString()}");
                            $atributo->refresh();
                        }
                        $activoFijo->delete();
                    }
                    $model->refresh();

                    $activos_fijos = [];
                    $total_costo_actfijo = 0;
                    $cotizacion = null;
                    if ($model->moneda_id != \common\models\ParametroSistema::getMonedaBaseId())
                        $cotizacion = Cotizacion::findOne(['fecha' => date('Y-m-d', strtotime('-1 day', strtotime($model->fecha_emision)))]);

                    /** @var ActivoFijo $oneModel */
                    /** @var ActivoFijoAtributo[] $atributos */
                    foreach ($aFijosCompra as $afijocompra) {
                        $oneModel = $afijocompra['model'];
                        $atributos = $afijocompra['atributos'];
                        $cantidad = $oneModel->cantidad;
                        while ($cantidad-- > 0) {
                            $model_actfijo = new ActivoFijo();
                            $model_actfijo->loadDefaultValues();
                            $model_actfijo->observacion = "creado el {$model->fecha_emision} desde modificar compra";
                            $model_actfijo->activo_fijo_tipo_id = $oneModel->activo_fijo_tipo_id;
                            $model_actfijo->nombre = $oneModel->nombre;
                            $model_actfijo->fecha_adquisicion = $oneModel->fecha_adquisicion;
                            // costo_adquisicion se valoriza
                            $model_actfijo->costo_adquisicion = $oneModel->costo_adquisicion;
                            $model_actfijo->vida_util_fiscal = $oneModel->vida_util_fiscal;
                            $model_actfijo->vida_util_contable = $oneModel->vida_util_contable;
                            $model_actfijo->vida_util_restante = $oneModel->vida_util_restante;
                            // valor_fiscal_neto se valoriza
                            $model_actfijo->valor_fiscal_neto = $oneModel->valor_fiscal_neto;
                            $model_actfijo->cuenta_id = PlantillaCompraventa::getFirstCuentaPrincipal($plantilla_actfijo_id)->p_c_gravada_id;;
                            $model_actfijo->factura_compra_id = $model->id;
                            $model_actfijo->moneda_id = $model->moneda_id;

                            if (!$model_actfijo->save()) {
                                throw new \Exception("Error guardando activo fijo: {$model_actfijo->getErrorSummaryAsString()}");
                            }
                            $model_actfijo->refresh();
//                            $activos_fijos [] = $model_actfijo;

                            foreach ($atributos as $_atributo) {
                                $atributo = new ActivoFijoAtributo();
                                $atributo->setAttributes($_atributo->attributes);
                                $atributo->activo_fijo_id = $model_actfijo->id;
                                if (!$atributo->save()) {
                                    throw new \Exception("Error guardando atributo para activo fijo
                                     {$model_actfijo->nombre}: {$atributo->getErrorSummaryAsString()}");
                                }
                                $atributo->refresh();
                            }
                            $model_actfijo->refresh();

                            $total_costo_actfijo += $model_actfijo->costo_adquisicion;
                        }
                    }

                    $total_costo_actfijo = round($total_costo_actfijo);
                    if ($total_costo_actfijo != $subtotal_plantilla_actfijo) {
                        $subtotal_plantilla_actfijo = $this->numberFormatUseDotByMoneda($subtotal_plantilla_actfijo, $model->moneda);
                        $total_costo_actfijo = $this->numberFormatUseDotByMoneda($total_costo_actfijo, $model->moneda);
                        throw new Exception("La Suma de los costos de los Activos Fijos
                         {$total_costo_actfijo} no puede ser distinto a {$subtotal_plantilla_actfijo}.");
                    }

//                    foreach ($activos_fijos as $activo_fijo) {
//                        if (!$activo_fijo->save(false)) {
//                            throw new \Exception("Error guardando activo fijo: {$activo_fijo->getErrorSummaryAsString()}");
//                        }
//                    }
                } elseif (!$compra_de_activofijo) {
                    foreach ($model->activoFijos as $activoFijo) {
                        foreach ($activoFijo->atributos as $atributo) {
                            if (!$atributo->delete())
                                throw new \Exception("Error interno borrando atributos del activo fijo: {$atributo->getErrorSummaryAsString()}");
                        }
                        $activoFijo->refresh();
                        $activoFijo->factura_compra_id = "";
                        if (!$activoFijo->save(false)) {
                            throw new Exception("Error desasociando activos fijos: {$activoFijo->getErrorSummaryAsString()}");
                        }
                    }
                }
                /** ------------------------------------------------------------------------- */

                /** ---------------- MANEJAR CUOTAS DE PRESTAMOS ----------------- */
                if ($es_para_cuota_prestamo) {
                    $skey1 = 'cont_prestamos_compra';

                    if (!Yii::$app->session->has($skey1)) {
                        throw new \Exception("Falta asociar un préstamo. Utilice el botón correspondiente a préstamos de la columna de Acciones de la Plantilla de préstamos.");
                    }

                    // Borrar prestamoDetalle_compra anteriores, restaurando los montos de las cuotas respectivamente.
                    $_prestamod_compra = PrestamoDetalleCompra::findAll(['factura_compra_id' => $id]);

                    foreach ($_prestamod_compra as $item) {

                        $prestamo_detalle = PrestamoDetalle::findOne($item->prestamo_detalle_id);
                        $prestamo_detalle->capital_saldo += (float)$item->capital;
                        $prestamo_detalle->interes_saldo += (float)$item->interes;
                        $prestamo_detalle->monto_cuota_saldo += (float)$item->monto_cuota;

                        if (!$prestamo_detalle->save()) {
                            $msg = implode(', ', $prestamo_detalle->getErrorSummary(true));
                            throw new \Exception("Error restaurando saldos de las cuotas: {$msg}");
                        }

                        $prestamo_detalle->refresh();

                        if (!$item->delete()) {
                            $msg = implode(', ', $item->getErrorSummary(true));
                            throw new \Exception("Error interno eliminando cont_prestamo_detalle_compra: {$msg}");
                        }

                        $item->refresh();
                    }

                    $prestamosDataProvider = $session->get($skey1);
                    $_prestamoDetalleCompraGuardar = [];
                    $_prestamoDetallesGuardar = [];

                    foreach ($prestamosDataProvider as $data) {
                        if ($data['prestamo']->entidad_id != $model->entidad_id) {
                            throw new \Exception("El préstamo al que corresponden las cuotas especificadas no pertenece a la entidad de la factura.");
                        }

                        $_detalles = $data['detalles'];

                        /** @var PrestamoDetalle $_detalle */
                        foreach ($_detalles as $_detalle) {

                            if ($_detalle['selected'] == '1') {
                                $old_detalle = PrestamoDetalle::findOne($_detalle['id']);

                                if (!$_detalle->montosCoherentes($old_detalle)) {
                                    $msg = implode(', ', $_detalle->getErrorSummary(true));
                                    throw new \Exception("Error al procesar datos del prestamo: {$msg}");
                                }

                                $old_detalle->restarSaldo($_detalle);
                                $_prestamoDetallesGuardar[] = $old_detalle;

                                $_prestamodeDetalleCompra = new PrestamoDetalleCompra();
                                $_prestamodeDetalleCompra->loadDefaultValues();
                                $_prestamodeDetalleCompra->factura_compra_id = $model->id;
                                $_prestamodeDetalleCompra->recibo_capital_nro = $_detalle['recibo_capital_nro'];
                                $_prestamodeDetalleCompra->setAttributesBasedOn($_detalle->attributes);
                                $_prestamoDetalleCompraGuardar[] = $_prestamodeDetalleCompra;
                            }
                        }
                    }

                    foreach ($_prestamoDetallesGuardar as $item) {
                        if (!$item->save(false)) {
                            $msg = implode(', ', $item->getErrorSummary(true));
                            throw new \Exception("Error actualizando montos de cuota: {$msg}");
                        }
                    }

                    foreach ($_prestamoDetalleCompraGuardar as $item) {
                        if (!$item->save()) {
                            $msg = implode(', ', $item->getErrorSummary(true));
                            throw new \Exception("Error Interno creando cont_prestamo_detalle_compra: {$msg}");
                        }
                        $item->refresh();
                    }

                    // No hace falta verificar que se haya seleccionado al menos una cuota, porque sino se generan
                    // detalles compra con subtotal 0, lo cual hace que no sea validada mucho antes de llegar a estas lineas

                } else {
                    $_model = $this->findModel($id);

                    // Restaurar las cuotas del prestamo asociado
                    if ($_model->getPrestamos()->exists()) {
                        $_prestamod_compra = PrestamoDetalleCompra::findAll(['factura_compra_id' => $id]);

                        foreach ($_prestamod_compra as $item) {

                            $prestamo_detalle = PrestamoDetalle::findOne($item->prestamo_detalle_id);
                            $prestamo_detalle->capital_saldo += (float)$item->capital;
                            $prestamo_detalle->interes_saldo += (float)$item->interes;
                            $prestamo_detalle->monto_cuota_saldo += (float)$item->monto_cuota;

                            if (!$prestamo_detalle->save()) {
                                $msg = implode(', ', $prestamo_detalle->getErrorSummary(true));
                                throw new \Exception("Error restaurando saldos de las cuotas: {$msg}");
                            }

                            $prestamo_detalle->refresh();

                            if (!$item->delete()) {
                                $msg = implode(', ', $item->getErrorSummary(true));
                                throw new \Exception("Error interno eliminando cont_prestamo_detalle_compra: {$msg}");
                            }

                            $item->refresh();
                        }
                    }
                }
                /** ---------------------------------------------------- */

                /** -------------------------- MANEJAR FACTURA DE PRESTAMOS -------------------------- */
                if ($es_para_fact_de_prestamo) {
                    $prestamos_id = $session->get('cont_prestamo_facturar', null);

                    if (!isset($prestamos_id)) {
                        throw new \Exception("Error: Ha agregado una plantilla para préstamos pero Ud. no ha especificado ninguno.");
                    }

                    foreach (Prestamo::findAll(['factura_compra_id' => $model->id]) as $prestamo) {
                        $prestamo->factura_compra_id = null;
                        if ($prestamo->ruc == '')
                            $prestamo->ruc = $model->entidad->ruc;

                        #Truco para aquellos prestamos que aun no tienen establecidos manualmente el campo nro_prestamo.
                        #El campo nro_prestamo se introdujo mas tarde, y ademas es required.
                        if ($prestamo->nro_prestamo == '')
                            $prestamo->nro_prestamo = (string)$prestamo->id;

                        if (!$prestamo->save()) {
                            throw new \Exception("Error desvinculando prestamos anteriores: {$prestamo->getErrorSummaryAsString()}");
                        }
                        $prestamo->refresh();
                    }

                    foreach (Prestamo::findAll($prestamos_id) as $prestamo) {
                        if ($prestamo->entidad_id != $model->entidad_id) {
                            throw new \Exception("El préstamo especificado no corresponde a la entidad de la factura.");
                        }

                        $prestamo->factura_compra_id = $model->id;
                        $prestamo->ruc = $model->ruc;
                        if (!$prestamo->save()) {
                            throw new \Exception("Error al asociar los prestamos a la compra: {$prestamo->getErrorSummaryAsString()}");
                        }
                    }
                } else {
                    foreach (Prestamo::findAll(['factura_compra_id' => $id]) as $prestamo) {
                        $prestamo->ruc = $model->ruc;
                        $prestamo->factura_compra_id = null;

                        #Truco para aquellos prestamos que aun no tienen establecidos manualmente el campo nro_prestamo.
                        #El campo nro_prestamo se introdujo mas tarde, y ademas es required.
                        if ($prestamo->nro_prestamo == '')
                            $prestamo->nro_prestamo = (string)$prestamo->id;

                        if (!$prestamo->save()) {
                            throw new \Exception("Error desvinculando prestamos anteriormente asociados: {$prestamo->getErrorSummaryAsString()}");
                        }
                    }
                }
                /** ---------------------------------------------------------------------------------- */

                /** ------------------------ MANEJAR STOCK DE ACTIVO BIOLOGICO ------------------------ */
                if ($es_para_act_bio) {
                    $session_key = 'cont_activo_bio_stock_manager';

                    if (!Yii::$app->session->has($session_key)) {
                        throw new \Error("Falta especificar la cantidad de A. Biológicos a comprar.");
                    } else {
                        $sessionData = Yii::$app->session->get($session_key);
                        $dataProvider = $sessionData['dataProvider'];
                        $allModels = $dataProvider->allModels;

                        // borrar todos los managers asociados a esta factura.
                        $stock_managers = ActivoBiologicoStockManager::find()->where(['factura_compra_id' => $id])->all();
                        /** @var ActivoBiologicoStockManager $stock_manager */
                        foreach ($stock_managers as $stock_manager) {
                            $abio = $stock_manager->activoBiologico;
                            $stock_manager->delete();
                            $abio->refresh();
                            $abio->recalcularPrecioUnitarioStockActuales();
                            if (!$abio->save())
                                throw new \Exception("Error restaurando activo biológico {$abio->nombre}: {$abio->getErrorSummaryAsString()}");
                        }

                        // guardar todos los managers.
                        /** @var ActivoBiologicoStockManager[] $stock_managers */
                        $stock_managers = $allModels;
                        foreach ($stock_managers as $stock_manager) {
                            $stock_manager->factura_compra_id = $model->id;

                            if (!$stock_manager->datosValidos()) {
                                throw new \Exception("Error interno validando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                            }

                            // guardar manager
                            if (!$stock_manager->save()) {
                                throw  new \Exception("Error interno guardando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                            }
                            $stock_manager->refresh();

                            $actbio = ActivoBiologico::actualizarStock_O_CrearNuevo($stock_manager);
                            if ($actbio->isNewRecord)
                                ActivoBiologico::isCreable();
                            else
                                $actbio->isEditable();
                            if (!$actbio->save()) {
                                throw new Exception("Error actualizando stock del Activo Biológico: {$actbio->getErrorSummaryAsString()}");
                            }
                            $actbio->refresh();
                        }

//                        // recalcular stock y precio_unitario de todos los a.bios. Puede lanzar excepcion.
//                        $actbios = ActivoBiologico::recalcPrecioUStockActualesAll('compra');
//                        foreach ($actbios as $actbio) {
//                            $actbio->save(false); // ya fue validado al generarse.
//                            $actbio->refresh();
//                        }
                    }
                } else {
                    $query = ActivoBiologicoStockManager::find()->where(['factura_compra_id' => $id])->andWhere(['IS', 'nota_compra_id', null]);
                    /** @var ActivoBiologicoStockManager $stock_manager */
                    foreach ($query->all() as $stock_manager) {
                        // enviando el id del manager como aquel a excluir permite no tener en cuenta al mismo
                        // al realizar la actualizacion y asi poder borrar despues al manager
                        $actbio = ActivoBiologico::actualizarStock_O_CrearNuevo($stock_manager, $stock_manager->id);
                        if ($actbio->isNewRecord)
                            ActivoBiologico::isCreable();
                        else
                            $actbio->isEditable();
                        if (!$actbio->save()) {
                            throw new Exception("Error restableciendo stock del Activo Biológico: {$actbio->getErrorSummaryAsString()}");
                        }
                        $actbio->refresh();

                        if (!$stock_manager->delete()) {
                            throw new \Exception("Error interno borrando StockManager Id={$stock_manager->id}: {$stock_manager->getErrorSummaryAsString()}");
                        }
                        $stock_manager->refresh();
                    }
                }
                /** ----------------------------------------------------------------------------------- */

                /** ----------------------- AUTOFACTURA ----------------------- */
                if ($model->tipo_documento_id != '' && $model->tipoDocumento->autofactura == 'si') {
                    $model->nombre_completo_autofactura = ucwords(trim($model->nombre_completo_autofactura));
                    $model->save(false);

                    if (!Compra::parametrosAutofacturaExists()) {
                        throw new \Exception("Ésta es una autofactura pero falta en la empresa actual el parámetro Salario Mínimo.
                          Verifique primero dicho parámetro en la empresa actual.");
                    }
                    $obligacion = $model->obligacion;
                    if ($obligacion->cantidad_salario_max == '') {
                        throw new \Exception("Ésta es una autofactura pero la obligación `{$model->obligacion->nombre}` de esta factura no
                         tiene definida la cantidad máxima de salarios mínimos para Autofacturas.");
                    }

                    $empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

                    $autofacturasQ = Compra::find()->where([
                        'cedula_autofactura' => $model->cedula_autofactura,
                        'empresa_id' => $empresa_id,
                        'periodo_contable_id' => $periodo_id,
                    ]);
//                    $autofacturasQ->andWhere(['!=', 'id', $model->id]); // excluir autofactura actual
                    if ($autofacturasQ->exists()) {
                        /** @var Compra[] $autofacturas */
                        $ingresos = 0;
                        $autofacturas = $autofacturasQ->all();
                        foreach ($autofacturas as $autofactura) {
                            $ingresos += (int)$autofactura->total * ($autofactura->moneda_id == \common\models\ParametroSistema::getMonedaBaseId() ? 1 : round($autofactura->cotizacion));
                        }

                        $salMin = (int)Compra::getSalarioMinimoEmpresaActual();
                        $cantSalMin = (int)$obligacion->cantidad_salario_max;

                        if ($salMin * $cantSalMin <= $ingresos) {
                            $sm = number_format(round($salMin), 0, ',', '.');
                            $limite = number_format(round($salMin * $cantSalMin), 0, ',', '.');
                            throw new \Exception("Ya no se puede registrar más autofacturas debido a que 
                            el cliente `{$model->nombre_completo_autofactura}` supera los {$cantSalMin} salarios mínimos.
                            El Salario Minimo es Gs. {$sm} y {$cantSalMin} salarios son Gs. {$limite}");
                        }

                        foreach ($autofacturas as $autofactura) {
                            if ($autofactura->id != $id && $autofactura->nombre_completo_autofactura != $model->nombre_completo_autofactura) {
                                throw new \Exception("El nombre `{$model->nombre_completo_autofactura}` no es
                                 válido. La cédula `{$model->cedula_autofactura}` se encuentra registrado a nombre de
                                  `$autofactura->nombre_completo_autofactura`.");
                            }
                        }
                    }
                }
                /** ----------------------- AUTOFACTURA ----------------------- */

                #Verificar si se invirtio si el tipo de documento set es nota de credito
                if (!$model->isPartidaCorrect())
                    throw new \Exception("Error validando compra: El tipo de documento SET es NOTA DE CREDITO pero las partidas no se han invertido.");

                #Eliminar detalle y cabecera del timbrado anterior si ya no tiene mas facturas asociadas
                $model->refresh();
                if (isset($oldTimbradoDetalle)) {
                    if ($model->timbrado_detalle_id != $oldTimbradoDetalle->id) {
                        if (TimbradoDetalle::find()->where(['id' => $oldTimbradoDetalle->id])->exists()) {
                            // Si se esta editando una factura sin timbrado, el $oldTimbradoDetalle es cargado con `null`.
                            if (!$oldTimbradoDetalle->getFacturasVenta()->exists() and !$oldTimbradoDetalle->getFacturasCompra()->exists()) {
                                $timbradoAsociado = $oldTimbradoDetalle->timbrado;
                                if (!$oldTimbradoDetalle->delete())
                                    throw new \Exception("Error interno: No se puede borrar el timbrado detalle anterior (sin facturas): {$oldTimbradoDetalle->getErrorSummaryAsString()}");

                                #Borrar timbrado si su unico detalle era el $oldTimbradoDetalle
                                $timbradoAsociado->refresh();
                                if (sizeof($timbradoAsociado->timbradoDetalles) == 0)
                                    if (!$timbradoAsociado->delete())
                                        throw new \Exception("Error interno: No se puede borrar el timbrado (sin detalles): {$timbradoAsociado->getErrorSummaryAsString()}");
                            }
                        }
                    }
                }

                $transaction->commit();

                FlashMessageHelpsers::createSuccessMessage("La factura {$model->nro_factura} se ha editado correctamente.", 8000);
                Yii::$app->getSession()->set('cont_detallecompra-provider', []);

                if (in_array($model->tipo_documento_id, TipoDocumento::getIDsAsociadoSetNotaCredito()))
                    return $this->redirect(['/contabilidad/compra-nota/index']);
                return $this->redirect(['index']);
            } catch (\Exception $e) {
	            throw $e;
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($e->getMessage(), 20000);

                $model->fecha_emision = implode('-', array_reverse(explode('-', $model->fecha_emision)));

                if ($model->condicion == 'credito')
                    $model->fecha_vencimiento = implode('-', array_reverse(explode('-', $model->fecha_vencimiento)));
            }
        }

        retorno:;
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response|string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDeleteWithObs($id)
    {
        $model_compra = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction();
        $timbradoDetalleID = $model_compra->timbrado_detalle_id;

        try {
            # Modal para observaciones (motivo de borrado).
            if (Yii::$app->request->isGet) {
                return $this->renderAjax('_modal-observaciones', []);
            } else {
                $postAttribs = $_POST['kvform'];

                if (!isset($postAttribs['observaciones']) || $postAttribs['observaciones'] == '')
                    throw new \Exception("Falta especificar el motivo por el que se quiere borrar.");
            }
            # Fin modal de observaciones.

            if (!empty($model_compra->getNotas()->asArray()->all())) {
                $msg = 'Esta factura tiene asociada nota/s.';
                throw new \Exception($msg);
            }

            // verificar si tiene retencion asociada
            $retencion = $model_compra->retencion;
            if (!empty($retencion)) {
                $msg = 'Esta factura tiene asociada la retención ' . $retencion->nro_retencion . '. Elimínelo primero.';
                throw new \Exception($msg);
            }

            // verificar si tiene poliza asociada
            $poliza = $model_compra->poliza;
            // no hace falta preguntar si la poliza tiene asiento ya que cuando intente borrar la poliza asociada, alli tambien se quejara si hay asiento.
            if (!empty($poliza)) {
                $msg = 'Esta factura tiene asociada la póliza ' . $poliza->nro_poliza . '. Elimínelo primero.';
                throw new \Exception($msg);
            }

            // verificar si tiene activoFijos asociados y tambien venta de esos activoFijos
            $ventas = [];
            foreach ($model_compra->activoFijos as $activoFijo) {
                if ($activoFijo->venta != null) $ventas[] = $activoFijo->venta;
            }
            if (sizeof($ventas) > 0)
                throw  new Exception('Hay uno o más activos fijos que generaron factura de venta. Elimínelos primero.');
            // eliminar activos fijos comprados. Se asume que ninguno de estos fueron vendidos ni una unidad.
            foreach ($model_compra->activoFijos as $activoFijo) {
                foreach ($activoFijo->atributos as $atributo) {
                    if (!$atributo->delete())
                        throw new \Exception("Error borrando atributos del activo fijo {$activoFijo->nombre}: {$atributo->getErrorSummaryAsString()}");
                }
                $activoFijo->delete();
            }

            if ($model_compra->asiento_id != null) {
                $transaction->rollBack();
                throw new Exception('Esta factura tiene asociado asientos. Elimínelos primero.');
            }

            // Si hay recibos asociados
            if (ReciboDetalle::findOne(['factura_compra_id' => $id]) != null) {
                throw new Exception("Esta Factura tiene asociado uno o varios Recibos.");
            }

            // eliminar iva_cuenta_usada y detalles de esta compra
            $ivaCuentas = CompraIvaCuentaUsada::find()->where([
                'factura_compra_id' => $id,
                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
                ->all();
            $detallesFactura = $model_compra->detallesCompra;
            foreach ($ivaCuentas as $ic) $ic->delete();
            foreach ($detallesFactura as $df) $df->delete();

            // restaurar montos de cuotas de prestamos
            $_prestamodetalle_compra = PrestamoDetalleCompra::findAll(['factura_compra_id' => $id]);
            foreach ($_prestamodetalle_compra as $item) {

                $prestamoDetalle = PrestamoDetalle::findOne($item->prestamo_detalle_id);
                $prestamoDetalle->capital_saldo += (float)$item->capital;
                $prestamoDetalle->interes_saldo += (float)$item->interes;
                $prestamoDetalle->monto_cuota_saldo += (float)$item->monto_cuota;

                if (!$prestamoDetalle->save(false)) {
                    $msg = implode(', ', $prestamoDetalle->getErrorSummary(true));
                    throw new Exception("Error recuperando saldo en prestamos detalle: {$msg}");
                }

                if (!$item->delete()) {
                    $msg = implode(', ', $item->getErrorSummary(true));
                    throw new \Exception("Error interno borrando cont_prestamo_detalle_compra: {$msg}");
                }
            }

            // desvincular prestamos facturados.
            $prestamos = Prestamo::findAll(['factura_compra_id' => $id]);
            foreach ($prestamos as $prestamo) {
                $prestamo->factura_compra_id = null;
                $prestamo->ruc = $model_compra->entidad->ruc;

                #Truco para aquellos prestamos que aun no tienen establecidos manualmente el campo nro_prestamo.
                #El campo nro_prestamo se introdujo mas tarde, y ademas es required.
                if ($prestamo->nro_prestamo == '')
                    $prestamo->nro_prestamo = (string)$prestamo->id;

                if (!$prestamo->save()) {
                    throw new \Exception("Error desvinculando prestamos anteriores: {$prestamo->getErrorSummaryAsString()}");
                }
                $prestamo->refresh();
            }

            // verificar primero si el a.bio facturado tiene venta, si no tiene, eliminar.
            foreach ($model_compra->activosBiologicos as $activo_Biologico) {
                if ($activo_Biologico->getVentas()->exists()) {
                    throw new Exception("Hay Activos Biológicos asociados a esta factura que también poseen factura(s) de venta(s).");
                }
            }

            // borrar todos los stockManagers generados por esta factura.
            /** @var ActivoBiologicoStockManager[] $stock_managers */
            $stock_managers = ActivoBiologicoStockManager::find()->where(['factura_compra_id' => $id])->all();
            foreach ($stock_managers as $stock_manager) {
                $actbio = ActivoBiologico::actualizarStock_O_CrearNuevo($stock_manager, $stock_manager->id);
                if ($actbio->isNewRecord)
                    ActivoBiologico::isCreable();
                else
                    $actbio->isEditable();
                if (!$actbio->save()) {
                    throw new Exception("Error restaurando stock del Activo Biológico: {$actbio->getErrorSummaryAsString()}");
                }
                $actbio->refresh();

                if (!$stock_manager->delete()) {
                    throw new  Exception("Error interno borrando StockManager: {$stock_manager->getErrorSummaryAsString()}");
                }
                $stock_manager->refresh();
            }

//            // actualizar en LOTE todos los a.bios (stock y precio_unitario actuales)
//            $actbios = ActivoBiologico::recalcPrecioUStockActualesAll('compra');
//            foreach ($actbios as $actbio) {
//                $actbio->save(false); // ya fue validado al generarse.
//                $actbio->refresh();
//            }

            # Escribir el motivo del borrado.
            $model_compra->observaciones = $postAttribs['observaciones'];  # $postAttribs debe estar definido si o si.
            $model_compra->ruc = $model_compra->timbrado = $model_compra->timbrado_id = $model_compra->nombre_entidad = '-';
            if (!$model_compra->save()) {
                throw new \Exception("Error escribiendo observaciones: {$model_compra->getErrorSummaryAsString()}");
            }
            $model_compra->refresh();
            # Fin escribir motivo.

            # Borrar la cabecera
            if (!$model_compra->delete()) {
                throw new \Exception("Error borrando factura: {$model_compra->getErrorSummaryAsString()}");
            }
            $model_compra->refresh();

            #Borrar talonario
            $timbradoDetalle = TimbradoDetalle::findOne(['id' => $timbradoDetalleID]);
            if (isset($timbradoDetalle)) {
                // Si se esta editando una factura sin timbrado, el $oldTimbradoDetalle es cargado con `null`.
                if (!$timbradoDetalle->getFacturasVenta()->exists() and !$timbradoDetalle->getFacturasCompra()->exists()) {
                    $timbradoAsociado = $timbradoDetalle->timbrado;
                    if (!$timbradoDetalle->delete())
                        throw new \Exception("Error interno: No se puede borrar el timbrado detalle anterior (sin facturas): {$timbradoDetalle->getErrorSummaryAsString()}");

                    #Borrar timbrado si su unico detalle era el $timbradoDetalle.
                    $timbradoAsociado->refresh();
                    if (sizeof($timbradoAsociado->timbradoDetalles) == 0)
                        if (!$timbradoAsociado->delete())
                            throw new \Exception("Error interno: No se puede borrar el timbrado (sin detalles): {$timbradoAsociado->getErrorSummaryAsString()}");
                }
            }

            #Comprometer.
            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage('La factura de compra se ha borrado exitosamente.');
        } catch (\Exception $e) {
            $transaction->rollBack();
            FlashMessageHelpsers::createErrorMessage('La Factura no se puede borrar: ' . $e->getMessage());
        }

        retorno:;
        return $this->redirect(['index']);
    }

    /**
     * Finds the Compra model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Compra the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = Compra::findOne(['id' => $id, 'tipo' => 'factura'])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCargarPlantilla()
    {
        $session = Yii::$app->session;
        if ($_POST['plantilla'] == 'mercaderia') {
            try {
                $detallesProvider = new ArrayDataProvider([
                    'allModels' => $this->detalleCompraMercaderia(),
                    'pagination' => false,
                ]);
            } catch (\Exception $e) {
                $detallesProvider = new ArrayDataProvider([
                    'allModels' => [],
                    'pagination' => false,
                ]);
            }
            $session->set('cont_detallecompra-provider', $detallesProvider);

            return true;
        }

        return false;
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionCargarTimbrado()
    {
        $entidad_ruc = $_POST['entidad_ruc'];
        $nro_factura = $_POST['nro_factura'];
        $fecha_emision = $_POST['fecha_emision'];
        $timbrado_vencido = $_POST['timbrado_vencido'];
        $tipodocumento_id = $_POST['tipodocumento_id'];

        //No se puede hacer consulta sin fecha, entidad o ruc
        if (!isset($entidad_ruc) or $entidad_ruc == '' or !isset($nro_factura) or $nro_factura == '' or
            !isset($fecha_emision) or $fecha_emision == '') return null;

        /** @var Entidad $entidad */
        $entidad = Entidad::findOne(['ruc' => $entidad_ruc]);

        //Se verifica que la entidad exista
        if (!isset($entidad)) return null;

        $tipodocumento = TipoDocumento::findOne(['id' => $tipodocumento_id]);
        if (!isset($tipodocumento)) return null;

        $nro = (int)substr($nro_factura, 8, 15);
        $prefijo = substr($nro_factura, 0, 7);

        $date = DateTime::createFromFormat('d-m-Y', $fecha_emision);
        $fecha_emision = $date->format('Y-m-d');

        /** @var TimbradoDetalle $detalle */
        $query = TimbradoDetalle::find()
            ->joinWith('timbrado as timbrado')
            ->andWhere(['timbrado.entidad_id' => $entidad->id]);

        // Excluir detalle timbrado para retenciones
        $empresa = Yii::$app->session->get('core_empresa_actual');
        $periodo = Yii::$app->session->get('core_empresa_actual_pc');
        $nombreTipodocSetRet = "core_empresa-{$empresa}-periodo-{$periodo}-tipodoc_set_retencion";
        $parmsysTipoddcSet = ParametroSistema::find()->where(['nombre' => $nombreTipodocSetRet]);
        $tipodocSetRetId = null;
        if ($parmsysTipoddcSet->exists()) {
            $tipodocSetRetId = $parmsysTipoddcSet->one()->valor;
        }
        if (isset($tipodocSetRetId)) {
            $query->andWhere(['!=', 'cont_timbrado_detalle.tipo_documento_set_id', $tipodocSetRetId]);
        }
        // Fin excluir detalle timbrado para retenciones

	    if ($timbrado_vencido == '0' || $timbrado_vencido == '') {
            $query->andWhere(['or', ['and', ['<=', 'timbrado.fecha_inicio', $fecha_emision], ['is not', 'timbrado.fecha_inicio', null]], ['is', 'timbrado.fecha_inicio', null]]);
            $query->andWhere(['>=', 'timbrado.fecha_fin', $fecha_emision]);
        } else {
            $query->andWhere(['AND', ['<', 'timbrado.fecha_fin', $fecha_emision], ['IS NOT', 'timbrado.fecha_fin', null]]);
        }

        $detalle = $query
            ->andWhere(['cont_timbrado_detalle.prefijo' => $prefijo])
            ->andWhere(['<=', 'cont_timbrado_detalle.nro_inicio', $nro])
            ->andWhere(['>=', 'cont_timbrado_detalle.nro_fin', $nro])
            ->andFilterWhere(['=', 'cont_timbrado_detalle.tipo_documento_set_id', $tipodocumento->tipo_documento_set_id])
            ->asArray()->all();  # si se usa one(), cuando haya mas de 1 detalle con el mismo prefijo y rangos que corresponden, se va a seleccionar solito uno cualquiera.
        # Caso: 10~11 de junio, 2019: Empresa formosa, factura id 28579, nro 002-001-0070985, timbrado 13394250, emitido el 27-05-2019
        # Hay dos timbrados nro 13238208 y 13394250 cuyos detalles (cada uno 1 solo detalle hasta la fecha) acaparan/contemplan al nro de factura.

        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (!isset($detalle)) {
            $timbrados = Timbrado::getTimbradosVigentesAjax($entidad_ruc, $_POST['fecha_emision'], $nro_factura, $tipodocumento_id, $timbrado_vencido);
            if (sizeof($timbrados) == 1)
                return ['mostrar_boton', array_pop($timbrados)];
            else
                return ['mostrar_boton', null];
        }

        $data = [];
        foreach ($detalle as $item) {
            $element = ['id' => $item['timbrado_id'], 'nro_timbrado' => $item['timbrado']['nro_timbrado']];

            //Si fecha emision es posterior a fecha fin timbrado
            //Timbrado vencido
            if ($date->getTimestamp() > strtotime($item['timbrado']['fecha_fin']))
                $element['vencido'] = 'si';
            else
                $element['vencido'] = 'no';

            $data[] = $element;
        }

//        //Si fecha emision es posterior a fecha fin timbrado
//        //Timbrado vencido
//        if ($date->getTimestamp() > strtotime($detalle['timbrado']['fecha_fin']))
//            $data['vencido'] = 'si';
//        else
//            $data['vencido'] = 'no';

        return $data;
    }

    public function actionSimular()
    {
        /** @var Cotizacion $cotizacion */
        /** @var DetalleCompra[] $detalles */
        /** @var DetalleCompra[] $detalles_para_sim */

        $es_nota_credito = Yii::$app->request->post('es_nota') == 'nota_credito';
        $hoy = date_create_from_format('d-m-Y', $_POST['hoy']);
        $total_factura = $_POST['cont_factura_total'];
        $moneda_id = $_POST['moneda_id'];
//        $estado = $_POST['estado_comprobante'];
        $detalles = Yii::$app->getSession()->get('cont_detallecompra-provider')->allModels;

//        if ($total_factura == 0 || sizeof($detalles) == 0) {
//            $detalles_para_sim = [];
//            goto retorno;
//        }

        // Cuando cambia de anulada/faltante a vigente, el campo fecha_emision es vacia y llega en POST vacio.
        if ($moneda_id != null && $moneda_id != 1 && $hoy == false) {
            FlashMessageHelpsers::createInfoMessage('Debe especificar una fecha para poder obtener la cotización a utilizar.');
//            $detalles_para_sim = [];
//            goto retorno;
            return false;
        }

        $cotizacion = null;
        if ($moneda_id != null && $moneda_id != 1)
            $cotizacion = Cotizacion::find()
                ->where(['moneda_id' => $_POST['moneda_id']])
                ->andFilterWhere([
                    '!=', 'moneda_id',
                    Moneda::findOne(['id' => ParametroSistemaHelpers::getValorByNombre('moneda_base') == null ? 1 : ParametroSistemaHelpers::getValorByNombre('moneda_base')
                    ])->id])
                ->andFilterWhere(['fecha' => date('Y-m-d', strtotime($hoy->format('Y-m-d') . ' - 1 days'))])
                ->one();
        $cotizacion_form = Yii::$app->request->post('cotizacion', '');
        $detalles = DetalleCompra::agruparCuentas($detalles);
        $detalles_para_sim = [];

//        if ($total_factura == 0) {
//            $detalles_para_sim = [];
//            goto retorno;
//        }

        // Convertir a guaranies si la moneda es extrangera.
        $diff = 0.0;
        $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
        if (isset($cotizacion) && $cotizacion_form != '') {
            $tipoDocumento = TipoDocumento::findOne(['id' => Yii::$app->request->post('tipo_documento_id')]);
            $diff = round($cotizacion_form, 2) - round($cotizacion->venta, 2);
            $newDetalleDiffCambio = null;
            if ($diff != 0.0) {
                $newDetalleDiffCambio = new DetalleCompra();
                $newDetalleDiffCambio->cta_contable = $diff < 0.0 ? 'haber' : 'debe';
                if ($es_nota_credito)
                    $newDetalleDiffCambio->cta_contable = $mapeo_inverso[$newDetalleDiffCambio->cta_contable];
                // 8.05 = resultado por diferencia de cambio como ingresos
                // 13.04 = resultado por diferencia de cambio como pérdida
                $empresa_id = \Yii::$app->session->get('core_empresa_actual');
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');

                $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                $newDetalleDiffCambio->plan_cuenta_id = PlanCuenta::findOne(['id' => $diff < 0.0 ? $parmsys_debe->valor : $parmsys_haber->valor])->id; // TODO: parametro del modulo
                $newDetalleDiffCambio->subtotal = round($diff, 2) * round($total_factura, 2);
                $diff < 0.0 ? $newDetalleDiffCambio->subtotal = (round($newDetalleDiffCambio->subtotal, 2) * -1.0) : true;
            }
            foreach ($detalles as $detalle) {
                $newDetalle = new DetalleCompra();
                $newDetalle->subtotal = round($detalle->subtotal, 2) * (($detalle->plan_cuenta_id == $tipoDocumento->cuenta_id && $detalle->cta_contable == (!$es_nota_credito ? 'haber' : 'debe')) ? round($cotizacion_form, 2) : round($cotizacion->venta, 2));
                $newDetalle->plan_cuenta_id = $detalle->plan_cuenta_id;
                $newDetalle->cta_contable = $detalle->cta_contable;
                $newDetalle->prestamo_cuota_id = $detalle->prestamo_cuota_id;
                $newDetalle->agrupar = $detalle->agrupar;
                $detalles_para_sim[] = $newDetalle;
            }

            // insertar el asiento por diferencia de costo
            if (isset($newDetalleDiffCambio)) {
                $detalles_para_sim[] = $newDetalleDiffCambio;
            }
        } elseif ($cotizacion_form != '') {
            foreach ($detalles as $detalle) {
                $newDetalle = new DetalleCompra();
                $newDetalle->subtotal = round($detalle->subtotal, 2) * round($cotizacion_form, 2);
                $newDetalle->plan_cuenta_id = $detalle->plan_cuenta_id;
                $newDetalle->cta_contable = $detalle->cta_contable;
                $newDetalle->prestamo_cuota_id = $detalle->prestamo_cuota_id;
                $newDetalle->agrupar = $detalle->agrupar;
                $detalles_para_sim[] = $newDetalle;
            }
        } else {
            // se usa moneda nacional, solo copiar los detalles al array para simulacion
            foreach ($detalles as $detalle) {
                $detalles_para_sim[] = $detalle;
            }
        }

        $detalles_para_sim = $this->agruparDebeHaber($detalles_para_sim);

        retorno:;
        // poner en sesion
        Yii::$app->getSession()->set('cont_detallecompra_sim-provider', new ArrayDataProvider([
            'allModels' => $detalles_para_sim,
            'pagination' => false,
        ]));
        return true;
    }

    public function actionGetRazonSocialByRuc()
    {
        $ruc = $_POST['ruc'];
        return ($ruc != '') ? (Entidad::find()->where(['ruc' => $_POST['ruc']])->exists() ? Entidad::findOne(['ruc' => $_POST['ruc']])->razon_social : null) : null;
    }

    public function actionCondicionFromTipoDoc()
    {
        $query = TipoDocumento::findOne(['id' => $_POST['tipo_doc_id']]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $query->condicion;
    }

    public function actionEnableCamposTotales()
    {
        $campos_a_habilitar = [];
        $plantilla_id = $_POST['plantilla_id'];
        $prefijo_campo = $_POST['prefijo_campo'];
        $plantilla_detalles = PlantillaCompraventaDetalle::findAll([
            'plantilla_id' => $plantilla_id, 'tipo_asiento' => 'compra']);

        foreach ($plantilla_detalles as $detalle) {
            /** @var IvaCuenta $iva_cta */
            $iva_cta = $detalle->ivaCta;
            switch ($iva_cta) {
                case (null):
                    $campos_a_habilitar[] = '#' . $prefijo_campo . '0-disp';
                    break;
                case (!null):
                    $campos_a_habilitar[] = '#' . $prefijo_campo . $iva_cta->iva->porcentaje . '-disp';
                    $campos_a_habilitar[] = '#totales_iva-impuesto-' . $iva_cta->iva->porcentaje . '-disp';
                    break;
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $campos_a_habilitar;
    }

    public
    static function getAcceso($action)
    {
        return PermisosHelpers::getAcceso('contabilidad-compra-' . $action);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdateTimbradoCompra($entidadRuc, $prefijo, $nro, $tipoDocumentoId, $timbradoId, $facturaCompraId, $submit = false)
    {
        if ($timbradoId == '') {
            FlashMessageHelpsers::createWarningMessage('No se ha seleccionado un timbrado.');
            return 1;
        }
        $model = new \backend\modules\contabilidad\models\auxiliar\Timbrado2();
        $model_detalle = new TimbradoDetalleUpdate();
        $nro = (int)$nro;
        $timbrado = Timbrado::findOne(['id' => $timbradoId]);
        $model->nro_timbrado = $timbrado->nro_timbrado;
        $model->fecha_inicio = date('d-m-Y', strtotime($timbrado->fecha_inicio));
        $model->fecha_fin = date('d-m-Y', strtotime($timbrado->fecha_fin));

        $entidad = Entidad::findOne(['ruc' => $entidadRuc]);
        $facturaCompra = Compra::findOne(['id' => $facturaCompraId]);
        $tipoDocumento = TipoDocumento::findOne(['id' => $tipoDocumentoId]);

        /** @var TimbradoDetalle $timbradoDetalle */
        $timbradoDetalle = TimbradoDetalle::find()
            ->joinWith('timbrado as timbrado')
            ->where(['prefijo' => $prefijo])
            ->andWhere(['timbrado_id' => $timbradoId])
            ->andWhere(['timbrado.entidad_id' => $entidad->id])
            ->andWhere(['<=', 'nro_inicio', $nro])
            ->andWhere(['>=', 'nro_fin', $nro])
            ->one();

        #Puede que el timbrado seleccionado no tenga detalles que contemple al nro de factura.
        if (isset($timbradoDetalle)) {
            $model_detalle->nro_inicio = $timbradoDetalle->nro_inicio;
            $model_detalle->nro_fin = $timbradoDetalle->nro_fin;
            $model_detalle->prefijo = $timbradoDetalle->prefijo;
        } else {
            $model_detalle->prefijo = $prefijo;
            $model_detalle->nro_inicio = $model_detalle->nro_fin = $model_detalle->nro_actual = $nro;
            $model_detalle->tipo_documento_set_id = $facturaCompra->tipoDocumento->tipo_documento_set_id;
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model_detalle->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model, $model_detalle);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post() && $submit == true) {
            $nro_timbrado = $_POST['Timbrado2']['nro_timbrado'];
            $timbradoEnDB = Timbrado::findOne(['nro_timbrado' => $nro_timbrado]);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($timbradoEnDB != null) {//El timbrado existe en la base de datos.
                    /** @var TimbradoDetalle $timbradoDetalleExistente */
                    $timbradoDetalleExistente = TimbradoDetalle::find()
                        ->joinWith('timbrado as timbrado')
                        ->where(['prefijo' => $prefijo])
                        ->andWhere(['=', 'timbrado.id', $timbradoEnDB->id])
                        ->andWhere(['timbrado.entidad_id' => $entidad->id])
                        ->andWhere(['<=', 'nro_inicio', $nro])
                        ->andWhere(['>=', 'nro_fin', $nro])
                        ->andWhere(['=', 'tipo_documento_set_id', $tipoDocumento->tipo_documento_set_id])
                        ->one();

                    if ($timbradoDetalleExistente != null) {//Ya existe un timbrado con detalle que contiene al numero de factura. Solo se debe referenciar.
                        FlashMessageHelpsers::createInfoMessage('No se ha hecho ninguna modificación.');
                        return 1;
//                        $facturaCompra->timbrado_detalle_id = $timbradoDetalleExistente->id;
//                        $facturaCompra->timbrado = '000';
//                        $facturaCompra->timbrado_id = '0';
//                        $facturaCompra->nombre_entidad = '--';
//                        $facturaCompra->ruc = '0';
//                        if (!$facturaCompra->save()) {
//                            throw new Exception('No se puede modificar timbrado.'); // No se puede guardar
//                        }
//
//                        //Si el detalle ya no es referenciado, se borra
//                        if (isset($timbradoDetalleViejo) && !$timbradoDetalleViejo->getFacturasCompra()->exists() && !$timbradoDetalleViejo->getFacturasVenta()->exists()) {
//                            $timbradoDetalleViejo->delete();
//                        } elseif (isset($timbradoDetalleViejo)) {  #Al cambiar de timbrado detalle, se tiene que borrar el anterior.
//                            throw new \Exception("Error: No se puede borrar el talonario anterior.");
//                        }
                    } else { //Se debe agregar un detalle al timbrado ya existente.
                        # TODO: Preguntar a hernan
                        # Por que primero se carga datos del detalle viejo y luego los datos del post?
                        $timbradoDetalleNuevo = new TimbradoDetalle2();
                        $timbradoDetalleNuevo->timbrado_id = $timbradoEnDB->id;
                        $timbradoDetalleNuevo->prefijo = $prefijo;
                        $timbradoDetalleNuevo->nro_inicio = $nro;
                        $timbradoDetalleNuevo->nro_fin = $nro;
                        $timbradoDetalleNuevo->nro_actual = $nro;
                        $timbradoDetalleNuevo->tipo_documento_set_id = $tipoDocumento->tipo_documento_set_id;
                        if (!$timbradoDetalleNuevo->save()) { // No se puede guardar el rango. Se va a crear nro_inicio = nro_fin = nro
                            throw new Exception("No se puede modificar timbrado: {$timbradoDetalleNuevo->getErrorSummaryAsString()}"); // No se puede guardar
                        }

//                        $facturaCompra->timbrado_detalle_id = $timbradoDetalleNuevo->id;
//                        $facturaCompra->timbrado = '000';
//                        $facturaCompra->timbrado_id = '0';
//                        $facturaCompra->nombre_entidad = '--';
//                        $facturaCompra->ruc = '0';
//                        if (!$facturaCompra->save()) {
//                            throw new Exception('No se puede modificar timbrado.'); // No se puede guardar
//                        }
//
//                        //Si el detalle ya no es referenciado, se borra
//                        if (isset($timbradoDetalleViejo) && !$timbradoDetalleViejo->getFacturasCompra()->exists() && !$timbradoDetalleViejo->getFacturasVenta()->exists()) {
//                            $timbradoDetalleViejo->delete();
//                        } elseif (isset($timbradoDetalleViejo)) {  #Al cambiar de timbrado detalle, se tiene que borrar el anterior.
//                            throw new \Exception("Error: No se puede borrar el talonario anterior.");
//                        }
                    }
                } else { //No existe el timbrado ingresado. Se va a crear junto a su detalle.
                    $timbradoNuevo = new Timbrado();
                    $timbradoNuevo->nro_timbrado = $nro_timbrado;
                    $timbradoNuevo->entidad_id = $timbrado->entidad_id;
                    $timbradoNuevo->nombre_entidad = $entidad->razon_social;
                    $timbradoNuevo->fecha_inicio = date('d-m-Y', strtotime($timbrado->fecha_inicio));
                    $timbradoNuevo->fecha_fin = date('d-m-Y', strtotime($timbrado->fecha_fin));
                    $timbradoNuevo->ruc = $entidadRuc;
                    if (!$timbradoNuevo->save()) {
                        throw new Exception("No se puede modificar timbrado: {$timbradoNuevo->getErrorSummaryAsString()}"); // No se puede guardar
                    }
//                    if (!$timbradoNuevo->save(false)) {
//                        throw new \Exception("Error guardando timbrado nuevo: {$timbradoNuevo->getErrorSummaryAsString()}");
//                    }
                    $timbradoNuevo->refresh();

                    $timbradoDetalleNuevo = new TimbradoDetalle2();
                    $timbradoDetalleNuevo->timbrado_id = $timbradoNuevo->id;
                    $timbradoDetalleNuevo->prefijo = $prefijo;
                    $timbradoDetalleNuevo->nro_inicio = $nro;
                    $timbradoDetalleNuevo->nro_fin = $nro;
                    $timbradoDetalleNuevo->nro_actual = $nro;//nro_inicio es nro_actual
                    $timbradoDetalleNuevo->tipo_documento_set_id = $tipoDocumento->tipo_documento_set_id;

                    if (!$timbradoDetalleNuevo->save()) { // No se puede guardar el rango. Se va a crear nro_inicio = nro_fin = nro
                        throw new Exception("No se puede modificar timbrado: {$timbradoDetalleNuevo->getErrorSummaryAsString()}"); // No se puede guardar
                    }

//                    $facturaCompra->timbrado_detalle_id = $timbradoDetalleNuevo->id;
//                    $facturaCompra->timbrado = '000';
//                    $facturaCompra->timbrado_id = '0';
//                    $facturaCompra->nombre_entidad = '--';
//                    $facturaCompra->ruc = '0';
//                    if (!$facturaCompra->save()) {
//                        throw new Exception('No se puede modificar timbrado.'); // No se puede guardar
//                    }
//
//                    //Si el detalle ya no es referenciado, se borra
//                    if (isset($timbradoDetalleViejo) && !$timbradoDetalleViejo->getFacturasCompra()->exists() && !$timbradoDetalleViejo->getFacturasVenta()->exists()) {
//                        $timbradoDetalleViejo->delete();
//                    } elseif (isset($timbradoDetalleViejo)) {  #Al cambiar de timbrado detalle, se tiene que borrar el anterior.
//                        throw new \Exception("Error: No se puede borrar el talonario anterior.");
//                    }
                }

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage('El timbrado se ha generado.');
                return 1;
            } catch (Exception $e) {
                FlashMessageHelpsers::createErrorMessage($e->getMessage());
                $transaction->rollBack();
                return false;
            }
        }

        return $this->renderAjax('_modalform_facturacompra_updatetimbrado', [
            'model' => $model,
            'model_detalle' => $model_detalle,
        ]);
    }

    public function actionNroFacturaValidator($nro_factura, $ruc, $timbrado_id, $modelId, $tipo = 'factura')
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $entidad = Entidad::findOne(['ruc' => $ruc]);

        $detalles = TimbradoDetalle::findAll(['timbrado_id' => $timbrado_id]);

        $yaExiste = false;
        foreach ($detalles as $detalle) {
            $compra = Compra::find()
                ->where([
                    'nro_factura' => $nro_factura,
                    'tipo' => $tipo
                ])
                ->andWhere(['!=', 'id', $modelId])
                ->andWhere(['entidad_id' => $entidad->id])
                ->andWhere(['timbrado_detalle_id' => $detalle->id]);

            if ($compra->exists()) {
                $yaExiste = true;
            }
        }

        $result = '';
        if ($yaExiste) {
            $result = Compra::NRO_FACTURA_DUPLICADO;
        }

        if ($result == Compra::NRO_FACTURA_DUPLICADO) {
            return ['result' => '', 'error' => "El nro de factura ya existe para la entidad (Factura id: {$compra->one()->id})."];  # En este punto el objeto $compra está instanciado si o si.
        } else {
            return ['result' => '', 'error' => ''];
        }
    }

    public function actionNavegate($id, $goto)
    {
        $op_rels = [
            'next' => '>',
            'prev' => '<'
        ];

        $compras = Compra::find()
            ->where([$op_rels[$goto], 'id', $id])
            ->andWhere(['empresa_id' => Yii::$app->session->get('core_empresa_actual')])
            ->andWhere(['periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->orderBy('id' . ($goto == 'next' ? ' ASC' : ' DESC'))->all();

        /** @var Compra $compra */
        foreach ($compras as $compra) {
            if ($this->isEditable($compra))
                return $this->redirect(['update', 'id' => $compra->id]);
        }

        FlashMessageHelpsers::createInfoMessage('No hay más facturas editables.');
        return $this->redirect(['update', 'id' => $id]);
    }

    /** RECORDAR:
     *
     *  Desde CREATE: Se desplaza hacia la compra cuyo nro_factura es cercano o igual al siguiente/anterior con respecto al nro_factura actual.
     *
     *  Desde UPDATE: Se desplaza hacia la compra en orden en que aparece en index.
     *
     */

    public function actionNavegateFromCreate($goto, $nro_fac = null)
    {
        $op_rels = [
            'next' => '>',
            'prev' => '<'
        ];

        $nro_completo = $nro_fac;

        if (strlen($nro_completo) < 15)
            return $this->redirect(['create']);

        $compra = Compra::find()
            ->where([$op_rels[$goto], 'nro_factura', $nro_completo])
            ->andWhere(['empresa_id' => Yii::$app->session->get('core_empresa_actual')])
            ->andWhere(['periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')])
            ->orderBy('nro_factura' . ($goto == 'prev' ? ' DESC' : ' ASC'))->one();

        $url = "";

        if (!isset($compra)) {
            return $this->redirect(['create']);
        } else
            return $this->redirect(['update', 'id' => $compra->id]);
    }

    public function actionBloquear($id)
    {
        $model = Compra::findOne(['id' => $id]);

        if ($model->bloqueado == 'no') {
            $model->bloqueado = 'si';

            //Solo para validacion
            $model->timbrado_id = '1';
            $model->nombre_entidad = 'a';
            $model->ruc = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Factura ha sido bloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede bloquear factura. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionDesbloquear($id)
    {
        $model = Compra::findOne(['id' => $id]);

        if ($model->bloqueado == 'si') {
            $model->bloqueado = 'no';

            //Solo para validacion
            $model->timbrado_id = '1';
            $model->nombre_entidad = 'a';
            $model->ruc = '1';

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Factura ha sido desbloqueada.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede desbloquear factura. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionFindUnbalanced()
    {
        $session = Yii::$app->session;
        $empresaId = $session->get('core_empresa_actual');
        $periodoId = $session->get('core_empresa_actual_pc');
//        echo Empresa::findOne($empresaId)->razon_social . '<br/>';
        $_debe = 0;
        $_haber = 0;
        $diferentes = [];
        foreach (Compra::findAll(['empresa_id' => $empresaId, 'periodo_contable_id' => $periodoId]) as $factura) {
            $debe = "SELECT SUM(subtotal) from cont_factura_compra_detalle where cta_contable = 'debe' and factura_compra_id = {$factura->id}";
            $haber = "SELECT SUM(subtotal) from cont_factura_compra_detalle where cta_contable = 'haber' and factura_compra_id = {$factura->id}";
            $facturaID = "SELECT id FROM cont_factura_compra where id = {$factura->id}";
            $select = [
                'debe' => "({$debe})",
                'haber' => "({$haber})",
                'facturaID' => "({$facturaID})"
            ];
            $query = new Query();
            $query->select($select);
            $resultSet = $query->createCommand()->queryAll()[0];

//            echo "<font style='color: " .($resultSet['debe'] != $resultSet['haber'] ? "red" : "black"). "'>" . "factura: {$resultSet['facturaID']}, debe: {$resultSet['debe']}, haber = {$resultSet['haber']}" . '</font><br/>';
//            Yii::warning("factura: {$resultSet['facturaID']}, debe: {$resultSet['debe']}, haber = {$resultSet['haber']}");
            $_debe += ($resultSet['debe'] != $resultSet['haber']) ? 0 : $resultSet['debe'];
            $_haber += ($resultSet['debe'] != $resultSet['haber']) ? 0 : $resultSet['haber'];

            if ($resultSet['debe'] != $resultSet['haber']) {
                $diferentes[] = [
                    'id' => "$factura->nro_factura ($factura->id)",
                    'fechaEmision' => $factura->fecha_emision,
                    'debe' => number_format($resultSet['debe'], 2, ',', '.'),
                    'haber' => number_format($resultSet['haber'], 2, ',', '.'),
                    'ver' => Html::a("<span class='glyphicon glyphicon-eye-open'>", Url::to(['compra/view', 'id' => $factura->id]), ['target' => '_blank'])
                ];
            }
        }
//        echo "total: $_debe $_haber";

        return $this->render('_unbalanced', [
            'diferentes' => $diferentes
        ]);

        exit();
    }

//    public function actionCreado()
//    {
//        $sql = "
//SELECT MIN(model_id) as id, MIN(created) as creado FROM `audit_trail` where model like 'backend%_\modules%_\contabilidad%_\models%_\Compra' and action = 'CREATE' GROUP BY model_id
//";
//        $command = \Yii::$app->getDb()->createCommand($sql);
//        $resultSet = $command->queryAll();
////        foreach ($resultSet as $item) {
////            echo print_r($item). '<br/>';
////        }
//        /** @var Compra[] $compras */
//        foreach ($resultSet as $item) {
//            $compra = Compra::findOne(['id' => $item['id']]);
//            if (isset($compra)) {
//                $compra->creado = $item['creado'];
//                $compra->save(false);
//            }
//        }
//        return $this->redirect(['index']);
//    }

    public function actionGetCounts()
    {
        return $this->render('get-counts', []);
    }

    public function actionCalculateCounts()
    {
        $resultSet = Yii::$app->db->createCommand("SELECT (SELECT COUNT(*) from cont_factura_compra) as compras, (SELECT COUNT(*) from cont_factura_venta) as ventas")->queryAll();
        Yii::$app->session->set('cuentas', "Cantidad de compras: {$resultSet[0]['compras']}<br/>Cantidad de ventas: {$resultSet[0]['ventas']}");
    }
}