<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\helpers\ModelHelpers;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\ActivoFijoTipo;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\CuentaPrestamoSelector;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\search\PlanCuentaSearch;
use backend\modules\contabilidad\models\TipoDocumento;
use common\helpers\FlashMessageHelpsers;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * PlanCuentaController implements the CRUD actions for PlanCuenta model.
 */
class PlanCuentaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlanCuenta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlanCuentaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $session = Yii::$app->session;

        $session->remove('handle');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PlanCuenta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PlanCuenta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PlanCuenta();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            $error = false;
            $msg = 'No se pudo crear la Cuenta.';
            if (!empty($model->padre_id)) {
                $model->cod_cuenta = str_pad($model->cod_cuenta, 2, '0', STR_PAD_LEFT);
                $padre = PlanCuenta::findOne($model->padre_id);
                $model->cod_completo = $padre->cod_completo . '.' . $model->cod_cuenta;
                $model->empresa_id = $model->es_global == 'si' && empty($padre->empresa_id) ? null : Yii::$app->session->get('core_empresa_actual');
                if ($padre->estado == 'inactivo')
                    $model->estado = 'inactivo';
            } else {
                $model->empresa_id = $model->es_global == 'si' ? null : Yii::$app->session->get('core_empresa_actual');
                $model->cod_completo = $model->cod_cuenta = (string)intval($model->cod_cuenta);
            }
            $model->setCodigoOrdenable();

            if ($model->checkExisteCuenta()) {
                $error = true;
                $msg = 'Ya existe una cuenta con el mismo código';
            }

            $model->asentable = count(explode('.', $model->cod_completo)) < 2 ? 'no' : $model->asentable;

            try {
                if (!$error && $model->save()) {
                    FlashMessageHelpsers::createSuccessMessage('Cuenta creada exitosamente.');
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } catch (\Exception $e) {
                $msg = 'Ya existe una cuenta con el mismo código';
            }

            FlashMessageHelpsers::createErrorMessage($msg);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PlanCuenta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            /* TODO: agregar check the CON/SIN 'operaciones asociadas' */
            $msg = 'No se pudo modificar la Cuenta.';
            $error = false;
            if (empty($model->oldAttributes['empresa_id'])) { // global
                if ($model->oldAttributes['estado'] == 'activo') {
                    $model->setCodigos();
                    $model->padre_id = $model->oldAttributes['padre_id'];
                    if ($model->es_global == 'no' && !$model->globalAIndividual()) // cambia a individual
                        $error = true;
                } else {
                    if ($model->padre->estado == 'inactivo')
                        $model->estado = 'inactivo';
                }
            } else { // individual
                if ($model->oldAttributes['estado'] == 'activo') {
                    $model->setCodigos();
                    if ($model->es_global == 'si' && !$model->individualAGlobal()) // cambia a global
                        $error = true;

                } else {
                    if ($model->padre->estado == 'inactivo')
                        $model->estado = 'inactivo';
                }
            }
            $model->asentable = count(explode('.', $model->cod_completo)) < 2 ? 'no' : $model->asentable;

            if (!$error && $model->save()) {
                FlashMessageHelpsers::createSuccessMessage('Cuenta modificada exitosamente.');
                return $this->redirect(['index']);

            } elseif (!empty($model->errors['cod_completo'])) {
                $msg = 'Ya existe una cuenta con el mismo código';
            }

            FlashMessageHelpsers::createErrorMessage($msg);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PlanCuenta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);
            $tipoDocumento = TipoDocumento::findOne(['cuenta_id' => $id]);
            if (isset($tipoDocumento)) {
                throw new \Exception("Esta cuenta fue utilizada por el Tipo de Documento `{$tipoDocumento->nombre}`");
            }
            $compraDetalle = DetalleCompra::findOne(['plan_cuenta_id' => $id]);
            if (isset($compraDetalle)) {
                throw new \Exception("Esta cuenta fue utilizada en la factura de compra nro `{$compraDetalle->facturaCompra->nro_factura}` de la empresa `{$compraDetalle->facturaCompra->empresa->razon_social}` (id: {$compraDetalle->facturaCompra->empresa_id})");
            }
            $ventaDetalle = DetalleVenta::findOne(['plan_cuenta_id' => $id]);
            if (isset($ventaDetalle)) {
                throw new \Exception("Esta cuenta fue utilizada en la factura de venta nro `{$ventaDetalle->venta->getNroFacturaCompleto()}` de la empresa `{$ventaDetalle->venta->empresa->razon_social}` (id: {$ventaDetalle->venta->empresa_id})");
            }
            $activoFijoTipo = ActivoFijoTipo::findOne(['cuenta_id' => $id]);
            if (isset($activoFijoTipo)) {
                throw new \Exception("Esta cuenta fue utilizada en el Tipo de Activo Fijo `{$activoFijoTipo->nombre}`");
            }
            $activoFijo = ActivoFijo::findOne(['cuenta_id' => $id]);
            if (isset($activoFijo)) {
                throw new \Exception("Esta cuenta fue utilizada en el Activo Fijo `{$activoFijo->nombre}` de la empresa `{$activoFijo->empresa->razon_social} (id: {$activoFijo->empresa_id})`");
            }
            $prestamo = Prestamo::getPrestamoWithCuenta($id);
            if (isset($prestamo)) {
                throw new \Exception("Esta cuenta fue utilizada por el préstamo #{$prestamo->id} de la empresa `{$prestamo->empresa->razon_social}` (id: {$prestamo->empresa_id})");
            }
            $asientoDetalle = AsientoDetalle::findOne(['cuenta_id' => $id]);
            if (isset($asientoDetalle)) {
                throw new \Exception("Esta cuenta fue utilizada desde el Asiento ID {$asientoDetalle->asiento_id} de la fecha {$asientoDetalle->asiento->fechaFormatted} de la empresa `{$asientoDetalle->asiento->empresa->razon_social}` (id: {$asientoDetalle->asiento->empresa_id})");
            }
            $ivaCuenta = IvaCuenta::findOne(['OR', ['cuenta_compra_id' => $id], ['cuenta_venta_id' => $id]]);
            if (isset($ivaCuenta)) {
                throw new \Exception("Esta cuenta fue utilizada en IvaCuenta para el porcentaje `{$ivaCuenta->iva->porcentaje}`");
            }
            $plantillaDetalle = PlantillaCompraventaDetalle::findOne(['OR', ['p_c_gravada_id' => $id], ['iva_cta_id' => $id]]);
            if (isset($plantillaDetalle)) {
                throw new \Exception("Esta cuenta fue utilizada en la Plantilla `{$plantillaDetalle->plantilla->nombre}`");
            }

            if (!$model->delete()) {
                throw new \Exception("Error al eliminar Cuenta `{$model->getNombreConCodigo()}`: {$model->getErrorSummaryAsString()}");
            }
            FlashMessageHelpsers::createSuccessMessage("Cuenta `{$model->getNombreConCodigo()}` eliminada exitosamente.");

        } catch (\Exception $e) {
            FlashMessageHelpsers::createErrorMessage($e->getMessage(), 20000);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the PlanCuenta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlanCuenta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PlanCuenta::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    public function actionGetSiguiente()
    {
        $es_global = Yii::$app->request->post('es_global');
        if (!empty($es_global)) {
            $respuesta = PlanCuenta::getCodigoSiguiente($es_global, Yii::$app->request->post('cuenta_id'));
            print json_encode($respuesta);
        }
        exit;
    }


    /** Agregar cuentas contables posibles para el debe del asiento de los préstamos de la empresa.
     *
     * @param bool $submit
     * @return array|string
     */
    public function actionAddCuentaPrestamo($submit = false)
    {
        $model = new CuentaPrestamoSelector();
        $session = Yii::$app->session;
        $skey = 'cont_cuentas_prestamo_empresa';

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $dataProvider = Yii::$app->session->get($skey);
            $data = $dataProvider->allModels;
            array_push($data, $model);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);

            $session->set($skey, $dataProvider);
        };

        return $this->renderAjax('empresa-plan-cuenta/_modal_add_cuenta', [
            'model' => $model,
        ]);
    }

    /** Eliminar cuentas contables posibles para el debe del asiento de los préstamos de la empresa.
     *
     * @param $indice
     * @param string $cuenta
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBorrarCuentaPrestamo($indice)
    {
        $session = Yii::$app->session;
        $skey = 'cont_cuentas_prestamo_empresa';

        $dataProvider = Yii::$app->session->get($skey);
        $data = $dataProvider->allModels;

        unset($data[$indice]);
        $data = array_values($data);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        $session->set($skey, $dataProvider);

        return true;
    }

    /** Modificar cuentas contables posibles para el debe del asiento de los préstamos de la empresa.
     * @param $indice
     * @param bool $submit
     * @return array|int|string
     */
    public function actionModificarCuentaPrestamo($indice, $submit = false)
    {
        $session = Yii::$app->session;
        $skey = 'cont_cuentas_prestamo_empresa';
        $dataProvider = $session[$skey];
        $data = $dataProvider->allModels;
        $model = $data[$indice];

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $data[$indice] = $model;

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);

            Yii::$app->response->format = Response::FORMAT_JSON;
            $session->set($skey, $dataProvider);
            return 8;
        }

        return $this->renderAjax('empresa-plan-cuenta/_modal_add_cuenta', [
            'model' => $model,
        ]);
    }

    public function actionGetHijos($_pjax = null)
    {
        if (!isset($_pjax))
            Yii::$app->session->set('dataProviderHijos', new ActiveDataProvider(['query' => PlanCuenta::find()->where(['id' => -1])]));
        return $this->render('views/_view_ver_hijos');
    }

    public function actionGenerateHijos()
    {
        $padre_id = $_GET['padre_id'];
        $cuenta = PlanCuenta::findOne($padre_id);
        $ids = [];
        if (isset($cuenta)) {
            $ids = $cuenta->getHijosIds();
            $hijosQuery = PlanCuenta::find()->where(['IN', 'id', $ids]);
            $dataProvider = new ActiveDataProvider();
            $dataProvider->query = $hijosQuery;
            $dataProvider->pagination = false;
            Yii::$app->session->set("dataProviderHijos", $dataProvider);
            $sql = $hijosQuery->createCommand()->rawSql;
            Yii::warning($sql);
        }
        if (!sizeof($ids))
            FlashMessageHelpsers::createWarningMessage("No hubo ninguna cuenta.");

        return true;
    }

    public function actionArbol()
    {
        $arboles = ModelHelpers::treeOfMainCuenta();
        return $this->render('views/arbol', ['arboles' => $arboles, 'balance' => $arboles[0]->balance]);
    }
}