<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\PlantillaCompraventaObligacion;
use backend\modules\contabilidad\models\PlantillaCompraventaRubro;
use backend\modules\contabilidad\models\search\PlantillaCompraventaSearch;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * PlantillaCompraventaController implements the CRUD actions for PlantillaCompraventa model.
 */
class PlantillaCompraventaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlantillaCompraventa models.
     * @return mixed
     */
    public function actionIndex($tipo)
    {
        $searchModel = new PlantillaCompraventaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $tipo);

        $skey = 'cont_plantilla_en_edicion';
        Yii::$app->session->remove($skey);
        Yii::$app->session->remove('cont_detalleplantilla_costoventa-provider');
        Yii::$app->session->remove('cont_detalleplantilla-provider');
        Yii::$app->session->remove('rubros_seleccionados');
        Yii::$app->session->remove('obligaciones_seleccionados');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tipo' => $tipo,
        ]);
    }

    /**
     * Displays a single PlantillaCompraventa model.
     * @param string $id
     * @param null $tipo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $tipo = null)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $tipo),
        ]);
    }

    /**
     * Creates a new PlantillaCompraventa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $_pjax
     * @param null $tipo
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionCreate($tipo, $_pjax = null)
    {
        $model = new PlantillaCompraventa();
        $model->loadDefaultValues();
        $model->tipo = $tipo;

        $session = Yii::$app->session;

        if ($_pjax == null && !Yii::$app->request->isPost) {
            $session->set('cont_detalleplantilla-provider', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
            $session->set('cont_detalleplantilla_costoventa-provider', new ArrayDataProvider([
                'allModels' => [],
                'pagination' => false,
            ]));
        }

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            try {
                $model->nombre = preg_replace("/[\s]+/", " ", $model->nombre);

                /** @var PlantillaCompraventaDetalle[] $detalles */
                /** @var PlantillaCompraventaDetalle[] $detalles_costoventa */

                if ($tipo == 'venta') {
                    $model->para_seguro_a_devengar = 'no'; // por el momento, la empresa no se dedica a la venta de seguros, solo a compras.
                }

                if (!$model->validate()) {
                    throw new \Exception('Error al validar la plantilla: ' . $model->getErrorSummary(true)[0]);
                }

                $model->save();
                $model->refresh();

//                if (!$model->esParaPrestamo()) {

                $detalles = $session['cont_detalleplantilla-provider']->allModels;
                $detalles_costoventa = $session['cont_detalleplantilla_costoventa-provider']->allModels;

                $hay_detalle = false;
                $cuenta_principal = false;
                foreach ($detalles as $d) {
                    $d->plantilla_id = $model->id;
                    $hay_detalle = true;
                    if ($d->cta_principal == 'si')
                        $cuenta_principal = true;
                    if (!$d->validate()) {
                        $msg = implode(', ', $d->getErrorSummary(true));
                        throw new \Exception("Error validando detalles: {$msg}");
                    }
                }

                if ($model->costo_mercad == 'si') {
                    foreach ($detalles_costoventa as $d) {
                        $d->plantilla_id = $model->id;
                        if (!$d->validate()) {
                            $msg = implode(', ', $d->getErrorSummary(true));
                            throw new \Exception("Error validando detalles por costo de venta: {$msg}");
                        }
                    }
                }

                if (!$cuenta_principal && $hay_detalle) {
                    throw new \Exception('Falta especificar al menos una cuenta como principal.');
                }

                if ($model->para_prestamo != 'si' && $model->para_cuota_prestamo != 'si' && !$hay_detalle) {
                    throw new \Exception("Falta crear al menos un detalle.");
                }

                // manejar rubros y obligaciones
                $rubros_id = [];
                $obligaciones_id = [];
                $skey_rubs = 'rubros_seleccionados';
                $skey_obls = 'obligaciones_seleccionados';
                if (Yii::$app->session->has($skey_rubs)) $rubros_id = Yii::$app->session->get($skey_rubs);
                if (Yii::$app->session->has($skey_obls)) $obligaciones_id = Yii::$app->session->get($skey_obls);

                $rubs = [];
                $obls = [];
                foreach ($rubros_id as $skey_rub) {
                    $new_rub = new PlantillaCompraventaRubro();
                    $new_rub->plantilla_compraventa_id = $model->id;
                    $new_rub->rubro_id = $skey_rub;
                    if (!$new_rub->validate()) {
                        throw new \Exception('Error validando Rubros: ' . implode(', ', $new_rub->getErrorSummary(true)));
                    }
                    $rubs[] = $new_rub;
                }
                foreach ($obligaciones_id as $oblig_id) {
                    $new_obg = new PlantillaCompraventaObligacion();
                    $new_obg->plantilla_compraventa_id = $model->id;
                    $new_obg->obligacion_id = $oblig_id;
                    if (!$new_obg->validate()) {
                        throw new \Exception('Error validando Rubros: ' . implode(', ', $new_obg->getErrorSummary(true)));
                    }
                    $obls[] = $new_obg;
                }

                // guardar detalles
                foreach ($detalles as $detalle) {
                    $detalle->save(false);
                }
                foreach ($detalles_costoventa as $detalle) {
                    $detalle->save(false);
                }
                foreach ($rubs as $rub) {
                    $rub->save(false);
                }
                foreach ($obls as $obl) {
                    $obl->save(false);
                }

                $trans->commit();
                FlashMessageHelpsers::createSuccessMessage('La plantilla ' . ucfirst($model->nombre) . ' se ha creado exitosamente.');
                Yii::$app->getSession()->remove('cont_detalleplantilla-provider');
                Yii::$app->getSession()->remove('cont_detalleplantilla_costoventa-provider');
                return $this->redirect(['index', 'tipo' => $tipo]);
            } catch (\Exception $exception) {
//                throw $exception;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        retorno:;
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PlantillaCompraventa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param null $tipo
     * @param null $_pjax
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionUpdate($id, $tipo, $_pjax = null)
    {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $detalles = PlantillaCompraventaDetalle::find()->where(['plantilla_id' => $model->id, 'tipo_asiento' => $tipo])->all();
        $detalles_costoventa = PlantillaCompraventaDetalle::find()->where(['plantilla_id' => $model->id, 'tipo_asiento' => 'costo_venta'])->all();
        $skey = 'cont_plantilla_en_edicion';
        $session->set($skey, $model->id);

        if ($_pjax == null && !Yii::$app->request->isPost) {
            $session->set('cont_detalleplantilla-provider', new ArrayDataProvider([
                'allModels' => $detalles,
                'pagination' => false,
            ]));
            $session->set('cont_detalleplantilla_costoventa-provider', new ArrayDataProvider([
                'allModels' => $detalles_costoventa,
                'pagination' => false,
            ]));

            $rubros = PlantillaCompraventaRubro::findAll(['plantilla_compraventa_id' => $model->id]);
            $obligs = PlantillaCompraventaObligacion::find()
                ->alias('plantob')
                ->leftJoin("cont_obligacion ob", "ob.id = plantob.obligacion_id")
                ->where(['plantilla_compraventa_id' => $model->id, 'ob.estado' => 'activo']);

            $rubros_id = [];
            $obligs_id = [];
            foreach ($model->rubros as $rubro) {
                $rubros_id[] = $rubro->id;
            }
            foreach ($model->obligaciones as $oblig) {
                $obligs_id[] = $oblig->id;
            }

            if (!empty($rubros_id))
                Yii::$app->session->set('rubros_seleccionados', $rubros_id);
            if (!empty($obligs_id))
                Yii::$app->session->set('obligaciones_seleccionados', $obligs_id);
        }

        if ($model->load(Yii::$app->request->post())) {
            /** @var PlantillaCompraventaDetalle[] $detalles */
            /** @var PlantillaCompraventaDetalle[] $detalles_costoventa */

            $trans = Yii::$app->db->beginTransaction();

            try {
                $model->nombre = preg_replace("/[\s]+/", " ", $model->nombre);

                // borrar todos los detalles de plantilla
                foreach ($model->plantillaCompraventaDetalles as $plantillaCompraventaDetalle) {
                    $plantillaCompraventaDetalle->delete();
                    $plantillaCompraventaDetalle->refresh();
                }

                if ($tipo == 'venta') {
                    $model->para_seguro_a_devengar = 'no'; // por el momento, la empresa no se dedica a la venta de seguros, solo a compras.
                }

                if (!$model->validate()) {
                    throw new \Exception("Error al validar la plantilla: {$model->getErrorSummaryAsString()}");
                }

                $model->save();
                $model->refresh();

//                $model->refresh();

//                if (!$model->esParaPrestamo()) {

                $detalles = $session['cont_detalleplantilla-provider']->allModels;
                $detalles_costoventa = $session['cont_detalleplantilla_costoventa-provider']->allModels;
                $cuenta_principal = false;
                $hay_detalle = false;

                // guardar detalles
                foreach ($detalles as $d) {
                    $new_d = new PlantillaCompraventaDetalle();
                    $_tmp['PlantillaCompraventaDetalle'] = $d->attributes;
                    $new_d->load($_tmp);
                    $new_d->plantilla_id = $model->id;
                    $new_d->id = null;

                    if ($new_d->cta_principal == 'si') {
                        $cuenta_principal = true;
                    }

                    if (!$new_d->validate()) {
                        throw new \Exception("Error validando detalles: {$new_d->getErrorSummaryAsString()}");
                    }
                    if (!$new_d->save(false))
                        throw new \Exception("Error guardando detalle: {$new_d->getErrorSummaryAsString()}");
                    $new_d->refresh();
                    $hay_detalle = true;
//                    $d->refresh();
                }
                $model->refresh();
                if ($model->costo_mercad == 'si') {
                    foreach ($detalles_costoventa as $d) {
                        $new_d = new PlantillaCompraventaDetalle();
                        $_tmp['PlantillaCompraventaDetalle'] = $d->attributes;
                        $new_d->load($_tmp);
                        $new_d->plantilla_id = $model->id;
                        $new_d->id = null;

                        if (!$new_d->validate()) {
                            throw new \Exception("Error validando detalles por costo de venta: {$d->getErrorSummaryAsString()}");
                        }
                        if (!$new_d->save(false))
                            throw new \Exception("Error guardando detalle: {$new_d->getErrorSummaryAsString()}");
                        $new_d->refresh();
//                        $d->refresh();
                    }
                }
                $model->refresh();

                if (!$cuenta_principal && $hay_detalle) {
                    throw new \Exception('Falta especificar al menos una cuenta como principal.');
                }

                // permitir guardar sin detalle si es plantilla para prestamos o cuotas de prestamos.
                if ($model->para_prestamo != 'si' && $model->para_cuota_prestamo != 'si' && !$hay_detalle) {
                    throw new \Exception("Falta crear al menos un detalle.");
                }
//                }

                // manejar rubros y obligaciones
                $rubros_viejos = [];
                foreach ($model->plantillaRubros as $plantillaRubro) {
                    if ($plantillaRubro->rubro->estado == 'inactivo') continue;  # no tratar las obligaciones y rubros asociados que estan inactivos.
                    $rubros_viejos[] = $plantillaRubro->rubro->id;
                    if (!$plantillaRubro->delete()) {
                        throw new \Exception('Error borrando rubros anteriores: ' . implode(', ', $plantillaRubro->getErrorSummary(true)));
                    }
                    $plantillaRubro->refresh();
                    $model->refresh();
                }
                $obligs_viejas = [];
                foreach ($model->plantillaObligaciones as $plantillaObligacion) {
                    if ($plantillaObligacion->obligacion->estado == 'inactivo') continue;  # no tratar las obligaciones y rubros asociados que estan inactivos.

                    $obligs_viejas[] = $plantillaObligacion->obligacion->id;
                    if (!$plantillaObligacion->delete()) {
                        throw new \Exception('Error borrando obligaciones anteriores: ' . implode(', ', $plantillaObligacion->getErrorSummary(true)));
                    }
                    $plantillaObligacion->refresh();
                    $model->refresh();
                }

                $rubros_id = [];
                $obligaciones_id = [];
                $skey_rubs = 'rubros_seleccionados';
                $skey_obls = 'obligaciones_seleccionados';
                if (Yii::$app->session->has($skey_rubs)) $rubros_id = Yii::$app->session->get($skey_rubs);
                if (Yii::$app->session->has($skey_obls)) $obligaciones_id = Yii::$app->session->get($skey_obls);

                // Verificar si los ids viejos estan en el conjunto de los nuevos ids
                // Si al menos un id viejo falta en el nuevo conjunto y a la vez, la plantilla ya fue utilizada,
                // debe lanzar excepcion.
                // Fuente:
                // https://stackoverflow.com/questions/9655687/php-check-if-array-contains-all-array-values-from-another-array
                $containsRubro = count(array_intersect($rubros_viejos, $rubros_id)) == count($rubros_viejos);
                $containsOblig = count(array_intersect($obligs_viejas, $obligaciones_id)) == count($obligs_viejas);
                $usedByVenta = VentaIvaCuentaUsada::find()->where(['plantilla_id' => $id])->exists();
                $usedByCompra = CompraIvaCuentaUsada::find()->where(['plantilla_id' => $id])->exists();
                Yii::warning(($usedByCompra || $usedByVenta) && !($containsOblig && $containsRubro));
                // JOSE PIDIO QUE NO SE HAGA ESTE CONTROL PORQUE HAY CASOS EN EL QUE ELLOS LE MOSTRARON QUE
                // SE NECESITA DESASOCIAR LA OBLIGACION.
//                if (($usedByCompra || $usedByVenta) && !($containsOblig && $containsRubro)) {
//                    Yii::$app->session->set($skey_rubs, $rubros_viejos);
//                    Yii::$app->session->set($skey_obls, $obligs_viejas);
//                    if (!$containsOblig && $usedByCompra) {
//                        throw new \Exception("Error modificando obligciones: La plantilla `{$model->nombre}`
//                         ya fue usada desde Compras, por lo tanto, no se debe remover obligaciones asociadas originalmente.");
//                    }
//                    if (!$containsOblig && $usedByVenta) {
//                        throw new \Exception("Error modificando obligciones: La plantilla `{$model->nombre}`
//                         ya fue usada desde Ventas, por lo tanto, no se debe remover obligaciones asociadas originalmente.");
//                    }
//                    if (!$containsRubro && $usedByCompra) {
//                        throw new \Exception("Error modificando obligciones: La plantilla `{$model->nombre}`
//                         ya fue usada desde Compras, por lo tanto, no se debe remover rubros asociados originalmente.");
//                    }
//                    if (!$containsRubro && $usedByVenta) {
//                        throw new \Exception("Error modificando obligciones: La plantilla `{$model->nombre}`
//                         ya fue usada desde Ventas, por lo tanto, no se debe remover rubros asociados originalmente.");
//                    }
//                }

                foreach ($rubros_id as $skey_rub) {
                    $new_rub = new PlantillaCompraventaRubro();
                    $new_rub->plantilla_compraventa_id = $model->id;
                    $new_rub->rubro_id = $skey_rub;
                    if (!$new_rub->save()) {
                        throw new \Exception('Error validando Rubros: ' . implode(', ', $new_rub->getErrorSummary(true)));
                    }
                }
                foreach ($obligaciones_id as $oblig_id) {
                    $new_obg = new PlantillaCompraventaObligacion();
                    $new_obg->plantilla_compraventa_id = $model->id;
                    $new_obg->obligacion_id = $oblig_id;
                    if (!$new_obg->save()) {
                        throw new \Exception('Error validando Obligaciones: ' . implode(', ', $new_obg->getErrorSummary(true)));
                    }
                }

                $trans->commit();
                FlashMessageHelpsers::createSuccessMessage('La plantilla ' . ucfirst($model->nombre) . ' se ha modificado exitosamente.');
                Yii::$app->getSession()->remove('cont_detalleplantilla-provider');
                Yii::$app->getSession()->remove('cont_detalleplantilla_costoventa-provider');
                $session->remove($skey);
                return $this->redirect(['index', 'tipo' => $tipo]);
            } catch (\Exception $exception) {
//                throw $exception;
                $trans->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        retorno:;
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PlantillaCompraventa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        /** @var PlantillaCompraventaDetalle $detalle_plantilla */
        $model = $this->findModel($id);
        $tipo_op = $model->tipo;

        if ($model->estado == 'inactivo') {
            FlashMessageHelpsers::createInfoMessage("La plantilla ya se encuentra en estado 'Inactivo'.");
            return $this->redirect(['index', 'tipo' => $tipo_op]);
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $hasRelation = false;
            if ($tipo_op == 'compra') {
                $query = CompraIvaCuentaUsada::find()->where(['plantilla_id' => $id]);
                if ($query->exists()) {
                    $facturas = [];
                    foreach ($query->all() as $iva_cta) {
                        $facturas[] = Compra::findOne($iva_cta->factura_compra_id)->nro_factura;
                    }
                    $nro_facturas_list_txt = implode(', ', $facturas);
                    if (sizeof($facturas) < 3)
                        throw new \Exception("Error borrando Plantilla {$model->nombre}:
                         fue utilizada por las siguientes facturas: {$nro_facturas_list_txt}.");
                    throw new \Exception("Error borrando Plantilla {$model->nombre}:
                     Esta plantilla ya fue utilizada desde Compra, por lo tanto no se puede eliminar.");
                }
                $hasRelation = true;
            } elseif ($tipo_op == 'venta') {
                $query = VentaIvaCuentaUsada::find()->where(['plantilla_id' => $id]);
                if ($query->exists()) {
                    $facturas = [];
                    /** @var VentaIvaCuentaUsada $iva_cta */
                    foreach ($query->all() as $iva_cta) {
                        $facturas[] = Venta::findOne($iva_cta->factura_venta_id)->getNroFacturaCompleto();
                    }
                    $nro_facturas_list_txt = implode(', ', $facturas);
                    if (sizeof($facturas) < 3)
                        throw new \Exception("Error borrando Plantilla {$model->nombre}: fue utilizada 
                        por las siguientes facturas: {$nro_facturas_list_txt}.");
                    throw new \Exception("Error borrando Plantilla {$model->nombre}:
                     Esta plantilla ya fue utilizada desde Ventas.");
                }
                $hasRelation = true;
            }
            if ($hasRelation) {
                $model->estado = 'inactivo';
                if (!$model->save()) {
                    throw new \Exception("Error inactivando plantilla {$model->nombre}: {$model->getErrorSummaryAsString()}");
                }
                foreach ($model->plantillaCompraventaDetalles as $detalle) {
                    if (!$detalle->delete()) {
                        throw new \Exception("Error borrando detalle de plantilla: {$detalle->getErrorSummaryAsString()}");
                    }
                    $detalle->refresh();
                }
                $model->refresh();
                FlashMessageHelpsers::createInfoMessage("En vez de eliminar,
                 se ha modificado el estado de la Plantilla {$model->nombre} a 'Inactivo'.
                  Las cuentas asociadas anteriormente fueron liberadas.
                   Por lo tanto, al reactivar, será necesario volver a crear los detalles.", 10000);
            } else {
                foreach ($model->plantillaCompraventaDetalles as $detalle) {
                    $detalle->delete();
                }
                foreach ($model->plantillaRubros as $rubro) {
                    $rubro->delete();
                }
                foreach ($model->plantillaObligaciones as $obligacion) {
                    $obligacion->delete();
                }
                $model->delete();
            }
            $transaction->commit();
            FlashMessageHelpsers::createInfoMessage("La plantilla {$model->nombre} se ha eliminado exitosamente.", 10000);

        } catch (\Exception $exception) {
            $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index', 'tipo' => $tipo_op]);
    }

    /**
     * Finds the PlantillaCompraventa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PlantillaCompraventa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $tipo = null)
    {
        $model = PlantillaCompraventa::find()->where(['id' => $id])->andFilterWhere(['tipo' => $tipo])->one();
        if ($model !== null) {
            return $model;
        } else {

        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionResetCostoventadetalleProvider()
    {
        /** @var PlantillaCompraventaDetalle[] $detalles_costoventa */

        // DESCOMENTAR SI SE QUIERE BORRAR DE LA GRILLA O DE LA BD AL OCULTAR LA GRILLA.
        // LA GRILLA ES EL SEGUNDO DETALLE
//        $session = Yii::$app->session;
//        $detalles_costoventa = $session['cont_detalleplantilla_costoventa-provider']->allModels;
//
//        foreach ($detalles_costoventa as $detalle) {
//            if($detalle->id != null) {
//                $detalle->delete();
//            }
//        }
//
//        $session->set('cont_detalleplantilla_costoventa-provider', new ArrayDataProvider([
//            'allModels' => [],
//            'pagination' => false,
//        ]));
        return true;
    }

    public function actionGuardarRubrosEnSession($all = "false", $checked = "false")
    {
        $message = '';
        $skey = 'rubros_seleccionados';
        if ($all === "false") // se selecciona 1 elemento
        {
            if (isset($_POST['id_registro_seleccionado'])) {
                $lista = [];
                if (!Yii::$app->getSession()->has($skey)) // si en la sesion no habia ninguno
                {
                    if ($checked === "true") {
                        array_push($lista, $_POST['id_registro_seleccionado']);
                        $message = "La OBLIGACION seleccionado ha sido agregado a la sesión.";
                    } else {
                        $message = "La OBLIGACION se ha deseleccionado pero no se ha puesto en la session.";
                    }
                } else {
                    $lista = Yii::$app->getSession()->get($skey);
                    if ($checked === "true") {
                        if (!in_array($_POST['id_registro_seleccionado'], $lista)) {
                            array_push($lista, $_POST['id_registro_seleccionado']);
                        }
                        $message = "El rubro seleccionado ha sido agregado a la sesión.";
                    } else {
                        $index = array_search($_POST['id_registro_seleccionado'], $lista);
                        unset($lista[$index]);
                        $message = "El rubro seleccionado ha sido eliminado de la sesión.";
                        if (empty($lista)) {
                            if (Yii::$app->getSession()->has($skey))
                                Yii::$app->getSession()->remove($skey);
                            return $message;
                        }
                    }
                }
                Yii::$app->getSession()->set($skey, $lista);
            }
        } else {
            if (isset($_POST['seleccionados'])) {
                $seleccionados = $_POST['seleccionados'];
                if (!Yii::$app->getSession()->has($skey)) {
                    $lista = [];
                    foreach ($seleccionados as $item) {
                        array_push($lista, $item);
                    }
                } else {
                    $lista = Yii::$app->getSession()->get($skey);
                    foreach ($seleccionados as $item) {
                        if (!in_array($item, $lista)) {
                            array_push($lista, $item);
                        }
                    }
                }
                Yii::$app->getSession()->set($skey, $lista);
                $message = "Los RUBROS seleccionados han sido agregados todos a la sesión.";
            } else {
                Yii::$app->getSession()->remove($skey);
                $message = "Los RUBROS han sido eliminados de la sesión.";
            }
        }

        return $message;
    }

    public function actionGuardarObligacionesEnSession($all = "false", $checked = "false")
    {
        $message = '';
        $skey = 'obligaciones_seleccionados';
        if ($all === "false") // se selecciona 1 elemento
        {
            if (isset($_POST['id_registro_seleccionado'])) {
                $lista = [];
                if (!Yii::$app->getSession()->has($skey)) // si en la sesion NO habia ninguno
                {
                    if ($checked === "true") {
                        array_push($lista, $_POST['id_registro_seleccionado']);
                        $message = "La OBLIGACION seleccionado ha sido agregado a la sesión.";
                    } else {
                        $message = "La OBLIGACION se ha deseleccionado pero no se ha puesto en la session.";
                    }
                } else {
                    $lista = Yii::$app->getSession()->get($skey);
                    if ($checked === "true") {
                        if (!in_array($_POST['id_registro_seleccionado'], $lista)) {
                            array_push($lista, $_POST['id_registro_seleccionado']);
                        }
                        $message = "La OBLIGACION seleccionado ha sido agregado a la sesión.";
                    } else {
                        $index = array_search($_POST['id_registro_seleccionado'], $lista);
                        unset($lista[$index]);
                        $message = "La OBLIGACION seleccionado ha sido eliminado de la sesión.";
                        if (empty($lista)) {
                            if (Yii::$app->getSession()->has($skey))
                                Yii::$app->getSession()->remove($skey);
                            return $message;
                        }
                    }
                }
                Yii::$app->getSession()->set($skey, $lista);
            }
        } else {
            if (isset($_POST['seleccionados'])) {
                $seleccionados = $_POST['seleccionados'];
                if (!Yii::$app->getSession()->has($skey)) {
                    $lista = [];
                    foreach ($seleccionados as $item) {
                        array_push($lista, $item);
                    }
                } else {
                    $lista = Yii::$app->getSession()->get($skey);
                    foreach ($seleccionados as $item) {
                        if (!in_array($item, $lista)) {
                            array_push($lista, $item);
                        }
                    }
                }
                Yii::$app->getSession()->set($skey, $lista);
                $message = "Las OBLIGACIONES seleccionados han sido agregados todos a la sesión.";
            } else {
                if (Yii::$app->getSession()->has($skey)) {
                    $lista = Yii::$app->getSession()->get($skey);
                    foreach ($lista as $item) {
                        $index = array_search($item, $lista);
                        unset($lista[$index]);
                    }
                }
                Yii::$app->getSession()->remove($skey);
                $message = "Las OBLIGACIONES han sido eliminados de la sesión.";
            }
        }

        return $message;
    }

    public function actionGetPlantilla($tipo, $noDeducible = 'no', $paraImportacion = 'no')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = [];
        $obligacion_id = Yii::$app->request->get('obligacion_id');
        $plantillas = PlantillaCompraventa::getPlantillasLista($tipo, false, $noDeducible == 'si' ? true : false, $paraImportacion == 'si' ? true : false, $obligacion_id);
//        /** @var PlantillaCompraventa $plantilla */
//        foreach ($plantillas as $plantilla) {
//            array_push($data, [
//                'id' => $plantilla['id'],
//                'text' => $plantilla['text'],
//            ]);
//        }

        return $plantillas;
    }

    public function actionInsertarCtaPrestamoBancoX()
    {
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $q = ParametroSistema::find()
            ->where(['nombre' => "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_prestamo-monto_operacion"]);

        if ($q->exists()) {
            $model = new PlantillaCompraventaDetalle();
            $model->p_c_gravada_id = $q->one()->valor;
            $model->tipo_saldo = 'haber';
            $model->tipo_asiento = 'compra';
            $model->cta_principal = 'no';

            $skey = 'cont_detalleplantilla-provider';
            $dataProvider = Yii::$app->session->get($skey, null);

            if (!isset($dataProvider)) {
                $dataProvider = new ArrayDataProvider(['allModels' => [$model,], 'pagination' => false,]);
            } else {
                foreach ($dataProvider->allModels as $_model) {
                    if ($_model->p_c_gravada_id == $model->p_c_gravada_id &&
                        $_model->tipo_saldo == $model->tipo_saldo && $_model->tipo_asiento == $model->tipo_asiento &&
                        $_model->iva_cta_id == $model->iva_cta_id) {
                        return true;
                    }
                }
                $dataProvider->allModels[] = $model;
            }

            Yii::$app->session->set($skey, $dataProvider);
            FlashMessageHelpsers::createInfoMessage("Se trajo del parámetro de la empresa, la cuenta contable correspondiente al Prestamo del Banco X");
        }
        return true;
    }

    public function actionRemoverCtaPrestamoBancoX()
    {
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $q = ParametroSistema::find()
            ->where(['nombre' => "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_prestamo-monto_operacion"]);

        if ($q->exists()) {
            $skey = 'cont_detalleplantilla-provider';
            $dataProvider = Yii::$app->session->get($skey, null);

            foreach ($dataProvider->allModels as $key => $_model) {
                if ($_model->p_c_gravada_id == $q->one()->valor) {
                    unset($dataProvider->allModels[$key]);
                    $dataProvider->allModels = array_values($dataProvider->allModels);
                    break;
                }
            }

            Yii::$app->session->set($skey, $dataProvider);
        }

        return true;
    }
}
