<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\PlanCuenta;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * VentaController implements the CRUD actions for Venta model.
 */
class LibromayorController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    private function codTostring($cod)
    {
        return $cod < 10 ? '00' . (string)$cod : $cod < 100 ? '0' . (string)$cod : (string)$cod;
    }

    /**
     * @param $a_detalles
     * @param $model ReporteLibromayor
     * @param $count
     * @return array
     */
    private function generarMayorizacion($a_detalles, $model, &$count)
    {
        $result = [];
        foreach ($a_detalles as $a_detalle) {
            $fecha = $a_detalle['fecha'];
            $cuenta_id = $a_detalle['cuenta_id'];

            $result[$cuenta_id][] = [
                'debe' => $a_detalle['monto_debe'],
                'haber' => $a_detalle['monto_haber'],
                'descripcion' => $a_detalle['concepto'],
                'detalle_id' => $a_detalle['id'],
                'asiento_id' => $a_detalle['asiento_id'],
                'fecha' => $fecha,
            ];

            // TODO: remover en futuro.
//            if (array_key_exists($cuenta_id, $result)) {
//                $result[$cuenta_id][$fecha] = [
//                    'debe' => $a_detalle['monto_debe'],
//                    'haber' => $a_detalle['monto_haber'],
//                    'descripcion' => $a_detalle['concepto'],
//                    'detalle_id' => $a_detalle['id'],
//                ];
//            } else {
//                $result[$cuenta_id] = [
//                    "{$fecha}" => [
//                        'debe' => $a_detalle['monto_debe'],
//                        'haber' => $a_detalle['monto_haber'],
//                        'descripcion' => $a_detalle['concepto'],
//                        'detalle_id' => $a_detalle['id'],
//                    ],
//                ];
//            }
            $count++;
        }

        return $result;
    }

    /**
     * @return mixed|string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws Exception
     */
    public function actionReporteLibro()
    {
        $model = new ReporteLibromayor();
        $model->paper_size = Pdf::FORMAT_A4;
        $model->orientation = Pdf::ORIENT_LANDSCAPE;

        /** @var PlanCuenta[] $cuentas */

        $skey = 'cont_reporte_cuentas_selected';

        $array_right = [];
        $array_left = PlanCuenta::find()->select(['id', 'nombre as text'])->asArray()->all();

        $cuentas_id = [];
        if (array_key_exists('selected', $_POST))
            $cuentas_id = $_POST['selected'];
        if (sizeof($cuentas_id) > 0)
            Yii::$app->session->set($skey, $cuentas_id);
        else
            Yii::$app->session->remove($skey);

        foreach ($cuentas_id as $cta_id) {
            $array_right[] = ['id' => $cta_id, 'text' => PlanCuenta::findOne(['id' => $cta_id])->nombre];
        }

        $json_pick_left = json_encode($array_left);
        $json_pick_right = json_encode($array_right);

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            $model->separarEnDesdeHasta();
            $asiento_detalles = AsientoDetalle::find();
            $asiento_detalles->alias('detalle');
            $asiento_detalles->joinWith('asiento as asiento');
            $asiento_detalles->joinWith('cuenta as cuenta');

            $asiento_detalles->select([
                'detalle.id as id',
                'detalle.asiento_id as asiento_id',
                'detalle.cuenta_id as cuenta_id',
                'detalle.monto_debe as monto_debe',
                'detalle.monto_haber as monto_haber',
                'asiento.fecha as fecha',
                'asiento.periodo_contable_id',
                'asiento.empresa_id',
                'asiento.concepto as concepto',
                'cuenta.cod_ordenable as cod_ordenable',
                'cuenta.nombre as nombre',
                'cuenta.padre_id as padre_id',
            ]);
            $asiento_detalles->andWhere(['asiento.empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'asiento.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
            $asiento_detalles->andFilterWhere(['between', 'fecha', $model->fecha_desde, $model->fecha_hasta]);
            $asiento_detalles->andFilterWhere(['IN', 'detalle.cuenta_id', $cuentas_id]); // esto hace que se generen asiento_detalles solo por la cuenta seleccionada.
            $asiento_detalles->orderBy('cod_ordenable, fecha');

            $count = 0;
            $cuentas_mayorizadas = $this->generarMayorizacion($asiento_detalles->asArray()->all(), $model, $count);
//            Yii::$app->session->set('mayorizada', $cuentas_mayorizadas);
//            Yii::$app->session->set('mayorizada_count', $count);
            ini_set('memory_limit', (int)ceil(400) . 'M');
//            if ($count > 150) {
//                ini_set('memory_limit', (int)ceil($count * 4) . 'M');
//            }
            if ($model->tipo_doc == 'xls') {
                // Crear objetoExcel.
                /** Error reporting */
                error_reporting(E_ALL);
                ini_set('display_errors', true);
                ini_set('display_startup_errors', true);
                define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
                date_default_timezone_set('America/Asuncion');

                // estilo de totales
                $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];

                // linea punteada
                $lineaPunteada = [
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED,
                        ],
                    ],
                ];
                /** Create a new Spreadsheet Object **/
                $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/libromayor/templates/';
                $objPHPExcel = IOFactory::load($carpeta . 'libro_mayor_template.xlsx');


                //Rellenar Excel.
                $fila_cabecera = 3;
                $columna_cabecera = 2;
                $fila = 6;
                $columna = 1;

                //formateo de fecha
                $de = '';
                $al = '';
                $periodo_cont = EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')])->anho;
                if ($model->fecha_rango == "") {
                    $primero_enero = date('Y-m-d', strtotime("01-01-" . $periodo_cont));
                    $ayer = date('Y-m-d', strtotime('-1 day'));
                    $desde = implode('-', array_reverse(explode('-', $primero_enero)));
                    $hasta = implode('-', array_reverse(explode('-', $ayer)));
                } else {
                    $desde = implode('-', array_reverse(explode('-', $model->fecha_desde)));
                    $hasta = implode('-', array_reverse(explode('-', $model->fecha_hasta)));
                }

                $currentDate = strftime('%d-%m-%Y a las %H:%M:%S', time());
                $empresa = Empresa::findOne(\Yii::$app->session->get('core_empresa_actual'));

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna_cabecera, $fila_cabecera, $desde);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna_cabecera + 2, $fila_cabecera, $hasta);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna_cabecera + 3, $fila_cabecera, Yii::$app->user->identity->username);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna_cabecera + 4, $fila_cabecera, $currentDate);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna_cabecera + 5, $fila_cabecera, $empresa->razon_social);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna_cabecera + 6, $fila_cabecera, $periodo_cont);
                try {
                    $debes = 0.0;
                    $haberes = 0.0;
                    $saldo = 0.0;
                    foreach ($cuentas_mayorizadas as $cta_id => $datos) {
                        $cta = PlanCuenta::findOne(['id' => $cta_id]);

                        $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($columna, $fila)->setValueExplicit($cta->cod_completo, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 3, $fila, $cta->nombre);

                        $fila++;
                        $_debe = 0.0;
                        $_haber = 0.0;
                        $_saldo = 0.0;
                        $cant_detalles = 0;
                        $saldo = 0.0;

                        foreach ($datos as $_ => $detalle) {
                            $isCtaAcreedor = (in_array((int)explode('.', $cta->cod_completo)[0], [2, 3])) ? true : false;
                            $saldo += (float)$detalle['debe'] * ($isCtaAcreedor ? -1 : 1);
                            $saldo -= (float)$detalle['haber'] * ($isCtaAcreedor ? -1 : 1);

                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 1, $fila, $detalle['detalle_id']);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 4, $fila, implode(array_reverse(explode('-', $detalle['fecha']))));
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 5, $fila, $detalle['descripcion']);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 6, $fila, $detalle['debe']);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 7, $fila, $detalle['haber']);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 8, $fila, $saldo);

                            $debes += (float)$detalle['debe'];
                            $haberes += (float)$detalle['haber'];
                            $_debe += (float)$detalle['debe'];
                            $_haber += (float)$detalle['haber'];
                            $_saldo = $_debe - $_haber;
                            $fila++;
                        }

                        //linea punteada
                        for ($i = $columna; $i <= 9; $i++) {
                            $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i, $fila)->getCoordinate())
                                ->applyFromArray($lineaPunteada);
                        }

                        $objPHPExcel->getActiveSheet()->mergeCells("A$fila:F$fila");
                        $objPHPExcel->getActiveSheet()->getCell("A$fila")->setValueExplicit("SUMA:", DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->getCell("G$fila")->setValueExplicit($_debe, DataType::TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->getCell("H$fila")->setValueExplicit($_haber, DataType::TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->getStyle("A$fila:I$fila")->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getStyle("A$fila:I$fila")->applyFromArray([
                            'borders' => [
                                'bottom' => [
                                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                ],
                            ],
                        ]);
                        $fila++;
                    }
//                    // estilo para la ultima fila de resumen
//                    for ($i = $columna + 5; $i <= 9; $i++) {
//                        $objPHPExcel->getActiveSheet()->getStyle($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i, $fila + 2)->getCoordinate())
//                            ->applyFromArray($styleArray);
//                    }
                    $objPHPExcel->getActiveSheet()->mergeCells("A$fila:F$fila");
                    $objPHPExcel->getActiveSheet()->getCell("A$fila")->setValueExplicit('TOTALES:', DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->getStyle("A$fila:I$fila")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A$fila:I$fila")->applyFromArray([
                        'borders' => [
                            'bottom' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],
                        ],
                    ]);
                    $objPHPExcel->getActiveSheet()->getCell("G$fila")->setValueExplicit($debes, DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->getCell("H$fila")->setValueExplicit($haberes, DataType::TYPE_NUMERIC);
                    $fila++;
                    $objPHPExcel->getActiveSheet()->getCell("I$fila")->setValueExplicit('Listado concluido', DataType::TYPE_STRING);
//                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna + 8, $fila + 2, $saldo);

                } catch (Exception $e) {
                    throw $e;
                }

                //Versión excel
                $carpeta = Yii::$app->basePath . '/web/uploads/';
                if (!is_dir($carpeta)) {
                    if (!@mkdir($carpeta, 0777, true)) {
                        throw new \Exception('No se puede crear carpeta.');
                    }
                }
                Yii::$app->session->set('carpeta', $carpeta);
                $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
                $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

                // Exportar en navegador
                $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
                $OS = strtolower(empty($_SERVER['HTTP_USER_AGENT']) ? '' : $_SERVER['HTTP_USER_AGENT']);
                if (!$OS)
                    $OS_IS_MAC = true;
                else
                    $OS_IS_MAC = (strpos($OS, 'mac') !== false)
                        || (strpos($OS, 'macintosh') !== false)
                        || (strpos($OS, 'iphone') !== false)
                        || (strpos($OS, 'ipad') !== false);

                if ($OS_IS_MAC) {
//            if(!file_exists($target_relative)){
//                copy($filename,$target_relative);
//                sleep(1);
//            }
//            header('Location: ' . $target);
                } else {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename=' . 'libro_mayor_' . $currentDate . '.' . $fi['extension']);
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx"));
                    ob_clean();
                    flush();
                    readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
                }
                //borramos el fichero temporal
                unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

            } else {
                $path = 'modules\contabilidad\views\libromayor\templates';
                $path = str_replace('\\', '/', $path);
                $cssInline = Yii::$app->view->renderFile("@app/" . $path . "/reporte.css");
                $contenido = Yii::$app->view->renderFile("@app/" . $path . "/reporte.php", ['data' => $cuentas_mayorizadas]);
                $encabezado = Yii::$app->view->renderFile("@app/" . $path . "/reporte_header.php", ['model' => $model]);
                $pie = Yii::$app->view->renderFile("@app/" . $path . "/reporte_pie.php", []);
                $pdf = new Pdf([
                    'mode' => Pdf::DEST_BROWSER,
                    'orientation' => $model->orientation,
                    'format' => $model->paper_size,
                    'marginTop' => 35,
                    'marginLeft' => 7,
                    'marginBottom' => 10,
                    'marginRight' => 7,
                    'content' => $contenido,
                    'cssInline' => $cssInline,
//                    'destination' => Pdf::DEST_BROWSER,
                    'filename' => "Reporte Libro mayor.pdf",
                    'options' => [
                        'title' => "",
                        'subject' => "",
                    ],
                    'methods' => [
                        'SetHeader' => [$encabezado],
                    ]
                ]);
                setlocale(LC_TIME, 'es_ES.UTF-8');
                Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
                Yii::$app->response->headers->add('Content-Type', 'application/pdf');
                return $pdf->render();
            }
        }

        retorno:;
        return $this->render('reporte-libromayor', [
            'model' => $model,
            'json_pick_left' => $json_pick_left,
            'json_pick_right' => $json_pick_right,
        ]);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}


/**
 * @property string $fecha_desde
 * @property string $fecha_hasta
 * @property string $paper_size
 * @property array $cuentas
 * @property string $orientation
 */
class ReporteLibromayor extends Model
{
    public $fecha_desde;
    public $fecha_hasta;
    public $fecha_rango;
    public $paper_size;
    public $cuentas = [];
    public $tipo_doc;
    public $orientation;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_desde', 'fecha_hasta', 'cuentas', 'paper_size', 'fecha_rango', 'tipo_doc', 'orientation'], 'safe'],
            [['fecha_desde', 'fecha_hasta', 'paper_size', 'fecha_rango', 'tipo_doc', 'orientation'], 'string'],
            [['paper_size', 'orientation'], 'required'],
        ];
    }


    public static function getPaperSizes()
    {
        $array = [];
        $i = 1;
        // (a4, oficio, oficiopy)
        $array[Pdf::FORMAT_A4] = ($i++) . " - A4";
        $array[Pdf::FORMAT_FOLIO] = ($i++) . " - Oficio";
        return $array;
    }

    public static function getCuentaLista()
    {
        $query = PlanCuenta::find()->select([
            'id',
            'nombre as text',
        ]);
        return ArrayHelper::map($query->asArray()->all(), 'id', 'text');
    }

    public function separarEnDesdeHasta()
    {
        if ($this->fecha_rango != '') {
            $rango = explode(' - ', $this->fecha_rango);
            $desde = $rango[0];
            $hasta = $rango[1];

            $desde = date_create_from_format('d-m-Y', $desde);
            $hasta = date_create_from_format('d-m-Y', $hasta);

            if ($desde && $hasta) {
                $this->fecha_desde = $desde->format('Y-m-d');
                $this->fecha_hasta = $hasta->format('Y-m-d');
            }
        } else {
            $primero_enero = date('Y-m-d', strtotime("01-01-" . EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')])->anho));
            $ayer = date('Y-m-d', strtotime('-1 day'));
            $this->fecha_desde = $primero_enero;
            $this->fecha_hasta = $ayer;
        }
    }
}