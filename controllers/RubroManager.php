<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 03/01/19
 * Time: 10:43 AM
 */

namespace backend\modules\contabilidad\controllers;


use backend\modules\contabilidad\models\PresentacionIva;
use backend\modules\contabilidad\models\PresentacionIvaRubro;
use DateInterval;
use yii\db\Query;

class RubroManager extends PresentacionIvaController
{
    /**
     * @param PresentacionIva $model
     * @param $desde
     * @param $hasta
     * @param $ivaCta5
     * @param $ivaCta10
     * @param $controller PresentacionIvaController
     * @return PresentacionIvaRubro[]
     * @throws \yii\db\Exception
     */
    public static function Rubro1Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $controller)
    {
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        // Facturas
        $rubro = 1;
        $where = "fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'factura' and venta.empresa_id = {$empresa_id} and venta.periodo_contable_id = {$periodo_id}";
        $sql = "
SELECT venta.id,
       iva.id,
       IF(civa.porcentaje IS NULL, 0, civa.porcentaje)                         AS porcentaje,
       iva.monto                                                               AS monto,
       venta.valor_moneda,
       (iva.monto * IF(venta.valor_moneda IS NOT NULL, venta.valor_moneda, 1)) AS valorizado
FROM `cont_factura_venta_iva_cuenta_usada` iva
       LEFT JOIN cont_factura_venta venta ON iva.factura_venta_id = venta.id
       LEFT JOIN cont_iva_cuenta ivacta ON iva.iva_cta_id = ivacta.id
       LEFT JOIN core_iva civa ON ivacta.iva_id = civa.id
WHERE {$where}
ORDER BY civa.porcentaje";
        $command = \Yii::$app->getDb()->createCommand($sql);
        $resultSet = $command->queryAll();
        $resultSetFactura = ['exenta' => 0, 'iva5' => 0, 'iva10' => 0];
        /** `$resultSetFactura es de la forma:
         *   ____________________________________________________
         *  |______exenta_____|______iva5______|_____iva10______|
         *  |______valor______|_____valor______|_____valor______|
         */
        foreach ($resultSet as $item) {
            switch ($item['porcentaje']) {
                case 0:
                    $resultSetFactura['exenta'] += round($item['valorizado']);
                    break;
                case 5:
                    $resultSetFactura['iva5'] += round($item['valorizado']);
                    break;
                case 10:
                    $resultSetFactura['iva10'] += round($item['valorizado']);
                    break;
            }
        }

        // Notas de credito
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $where = "fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'nota_credito' and venta.empresa_id = {$empresa_id} and venta.periodo_contable_id = {$periodo_id}";
        $sql = "
SELECT IF(civa.porcentaje IS NULL, 0, civa.porcentaje)                         AS porcentaje,
       iva.monto                                                               AS monto,
       venta.valor_moneda,
       (iva.monto * IF(venta.valor_moneda IS NOT NULL, venta.valor_moneda, 1)) AS valorizado
FROM `cont_factura_venta_iva_cuenta_usada` iva
       LEFT JOIN cont_factura_venta venta ON iva.factura_venta_id = venta.id
       LEFT JOIN cont_iva_cuenta ivacta ON iva.iva_cta_id = ivacta.id
       LEFT JOIN core_iva civa ON ivacta.iva_id = civa.id
WHERE {$where}
ORDER BY civa.porcentaje";
        $command = \Yii::$app->getDb()->createCommand($sql);
        $resultSet = $command->queryAll();
        $resultSetCredito = ['exenta' => 0, 'iva5' => 0, 'iva10' => 0];
        foreach ($resultSet as $item) {
            switch ($item['porcentaje']) {
                case 0:
                    $resultSetCredito['exenta'] += round($item['valorizado']);
                    break;
                case 5:
                    $resultSetCredito['iva5'] += round($item['valorizado']);
                    break;
                case 10:
                    $resultSetCredito['iva10'] += round($item['valorizado']);
                    break;
            }
        }

        // Exenta, iva10 e iva5 de las notas de debito
        $where = "fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'nota_debito' and venta.empresa_id = {$empresa_id} and venta.periodo_contable_id = {$periodo_id}";
        $sql = "
SELECT IF(civa.porcentaje IS NULL, 0, civa.porcentaje)                         AS porcentaje,
       iva.monto                                                               AS monto,
       venta.valor_moneda,
       (iva.monto * IF(venta.valor_moneda IS NOT NULL, venta.valor_moneda, 1)) AS valorizado
FROM `cont_factura_venta_iva_cuenta_usada` iva
       LEFT JOIN cont_factura_venta venta ON iva.factura_venta_id = venta.id
       LEFT JOIN cont_iva_cuenta ivacta ON iva.iva_cta_id = ivacta.id
       LEFT JOIN core_iva civa ON ivacta.iva_id = civa.id
WHERE {$where}
ORDER BY civa.porcentaje";
        $command = \Yii::$app->getDb()->createCommand($sql);
        $resultSet = $command->queryAll();
        $resultSetDebito = ['exenta' => 0, 'iva5' => 0, 'iva10' => 0];
        foreach ($resultSet as $item) {
            switch ($item['porcentaje']) {
                case 0:
                    $resultSetDebito['exenta'] += round($item['valorizado']);
                    break;
                case 5:
                    $resultSetDebito['iva5'] += round($item['valorizado']);
                    break;
                case 10:
                    $resultSetDebito['iva10'] += round($item['valorizado']);
                    break;
            }
        }

        $models_d = [];
        // Array auxiliar, utilizado para acumular los montos para el inciso i.
        $total_18_21_24 = [
            18 => ['col' => 'I', 'monto' => 0],
            21 => ['col' => 'II', 'monto' => 0],
            24 => ['col' => 'III', 'monto' => 0]
        ];

        // Montos Imponibles del rubro 1 (-I-), casillas 'a' ~ 'c' (venta de mercaderias gravadas y exentas)
        foreach ($resultSetFactura as $key => $value) {
//            if ($value == "") continue;
            $col_I = new PresentacionIvaRubro();
            $col_I->loadDefaults();
            $col_I->rubro = $rubro;
            $col_I->inciso = $key == 'exenta' ? 'c' : ($key == 'iva5' ? 'b' : 'a');
            $col_I->casilla = $key == 'exenta' ? 12 : ($key == 'iva5' ? 11 : 10);
            $col_I->monto = (int)round($value);
            $col_I->columna = 'I';
            $col_I->presentacion_iva_id = $model->id;
            $models_d[] = $col_I;
            \Yii::warning("inciso: {$col_I->inciso}, casilla: {$col_I->casilla}");

            // Acumular los totales para el inciso i
            $controller->acumularTotalesRubroI($total_18_21_24, $col_I);
        }

        // TODO: Incisos d, e: exportaciones, en esta linea.

        // Incisos f, g, h
        foreach ($resultSetCredito as $key => $value) {
//            if ($value == "") continue;
            $ivaRubro = new PresentacionIvaRubro();
            $ivaRubro->loadDefaults();
            $ivaRubro->presentacion_iva_id = $model->id;
            $ivaRubro->rubro = $rubro;
            $ivaRubro->monto = (int)$value;
            $ivaRubro->columna = 'I';
            $ivaRubro->inciso = ($key == 'exenta') ? 'h' : (($key == 'iva10') ? 'f' : 'g');
            $ivaRubro->casilla = ($key == 'exenta') ? 17 : (($key == 'iva10') ? 15 : 16);
            $models_d[] = $ivaRubro;

            // Acumular los totales para el inciso i
            $controller->acumularTotalesRubroI($total_18_21_24, $ivaRubro);
        }

        // Calcular valores para columna II y III
        foreach ($models_d as $col_I) {
            if (!$col_I->validate()) {
                throw new \yii\db\Exception("Error de validacion para rubro {$col_I->rubro} inciso {$col_I->inciso} columna {$col_I->columna}: {$col_I->getAllErrorsAsString()}");
            }

            // Casillas de los IVA DEBITO de los incisos anteriores (-II- o -III-)
            $iva_dev = new PresentacionIvaRubro();
            $iva_dev->loadDefaults();
            $iva_dev->rubro = $rubro;
            $iva_dev->inciso = $col_I->inciso;
            $iva_dev->presentacion_iva_id = $model->id;
            switch ($col_I->casilla) {
                case 10:
                    $iva_dev->casilla = 22;
                    $iva_dev->monto = $col_I->monto / 11;
                    $iva_dev->columna = 'III';
                    break;
                case 11:
                    $iva_dev->casilla = 19;
                    $iva_dev->monto = $col_I->monto / 21;
                    $iva_dev->columna = 'II';
                    break;
                case 15:
                    $iva_dev->casilla = 23;
                    $iva_dev->monto = $col_I->monto / 11;
                    $iva_dev->columna = 'III';
                    break;
                case 16:
                    $iva_dev->casilla = 20;
                    $iva_dev->monto = $col_I->monto / 21;
                    $iva_dev->columna = 'II';
            }

            // Si al monto imponible no le corresponde ningun iva debito, pasar al siguiente.
            if (!isset($iva_dev->monto) && !isset($iva_dev->casilla)) continue;

            // Si al monto le corresponde algun iva debito, redondear por si haya decimales.
            else $iva_dev->monto = (int)round($iva_dev->monto);

            // Acumular los totales para el inciso i
            $controller->acumularTotalesRubroI($total_18_21_24, $iva_dev);

            if (!$iva_dev->validate()) {
                throw new \yii\db\Exception("Error de validacion para rubro {$iva_dev->rubro} inciso {$iva_dev->inciso} columna {$iva_dev->columna} (campo calculado): {$iva_dev->getAllErrorsAsString()}");
            }
//            $iva_dev->refresh();
            $models_d[] = $iva_dev;
        }

        // Inciso i, fila de totales.
//        $models_d = [];
        foreach ($total_18_21_24 as $key => $box) {
            $model_d = new PresentacionIvaRubro();
            $model_d->loadDefaults();
            $model_d->rubro = $rubro;
            $model_d->inciso = 'i';
            $model_d->casilla = $key;
            $model_d->monto = (int)round($box['monto']);
            $model_d->columna = $box['col'];
            $model_d->presentacion_iva_id = $model->id;
            $models_d[] = $model_d;
            if (!$model_d->validate()) {
                throw new \yii\db\Exception("Error de validacion para rubro {$model_d->rubro} inciso {$model_d->inciso} columna {$model_d->columna} (campo calculado): {$model_d->getAllErrorsAsString()}");
            }
        }

        return $models_d;
    }

    /**
     * @param PresentacionIva $model
     * @param $desde
     * @param $hasta
     * @param $ivaCta5
     * @param $ivaCta10
     * @param $controller PresentacionIvaController
     * @return PresentacionIvaRubro[]
     * @throws \yii\db\Exception
     */
    public static function Rubro2Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $controller)
    {
        // Facturas
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $rubro = 2;
        $where = "fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'factura' and venta.empresa_id = {$empresa_id} and venta.periodo_contable_id = {$periodo_id}";
        $sql = "
SELECT venta.id,
       iva.id,
       IF(civa.porcentaje IS NULL, 0, civa.porcentaje)                         AS porcentaje,
       iva.monto                                                               AS monto,
       venta.valor_moneda,
       (iva.monto * IF(venta.valor_moneda IS NOT NULL, venta.valor_moneda, 1)) AS valorizado
FROM `cont_factura_venta_iva_cuenta_usada` iva
       LEFT JOIN cont_factura_venta venta ON iva.factura_venta_id = venta.id
       LEFT JOIN cont_iva_cuenta ivacta ON iva.iva_cta_id = ivacta.id
       LEFT JOIN core_iva civa ON ivacta.iva_id = civa.id
WHERE {$where}
ORDER BY civa.porcentaje";
        $command = \Yii::$app->getDb()->createCommand($sql);
        $resultSet = $command->queryAll();
        $resultSetFactura = ['exenta' => 0, 'iva5' => 0, 'iva10' => 0];
        /** `$resultSetFactura es de la forma:
         *   ____________________________________________________
         *  |______exenta_____|______iva5______|_____iva10______|
         *  |______valor______|_____valor______|_____valor______|
         */
        foreach ($resultSet as $item) {
            switch ($item['porcentaje']) {
                case 0:
                    $resultSetFactura['exenta'] += round($item['valorizado']);
                    break;
                case 5:
                    $resultSetFactura['iva5'] += round($item['valorizado']);
                    break;
                case 10:
                    $resultSetFactura['iva10'] += round($item['valorizado']);
                    break;
            }
        }
        \Yii::$app->session->set('$resultSetFactura2', $resultSetFactura);

        $models_d = [];

        // Calcular gravada y exenta
        $gravada = 0;
        $exenta = 0;
        foreach ($resultSetFactura as $key => $value) {
            $iva = preg_match('/^iva([0-9]+)$/', $key, $matches);
            $match = $iva ? $matches[1] : '-no-';
            \Yii::warning("key: $key, iva: $iva, match: $match");
            if ($iva) {
                $gravada += (float)$value / ($matches[1] == 5 ? 1.05 : 1.1);
            } else {
                $exenta += (float)$value;
            }

        }
        $exenta = round($exenta);
        $gravada = round($gravada);
        \Yii::$app->session->set("gravada y exenta", "exenta: {$exenta}, gravada: {$gravada}");

        // Casillas 25
        $casilla25 = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_2]);
        $casilla25->loadDefaults();
        $casilla25->rubro = $rubro;
        $casilla25->inciso = 'a';
        $casilla25->casilla = 25;
        $casilla25->monto = $gravada;
        $casilla25->presentacion_iva_id = $model->id;
        $models_d[] = $casilla25;

        // Casillas 26
        $casilla26 = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_2]);
        $casilla26->loadDefaults();
        $casilla26->rubro = $rubro;
        $casilla26->inciso = 'b';
        $casilla26->casilla = 26;
        $casilla26->monto = $exenta;
        $casilla26->presentacion_iva_id = $model->id;
        $models_d[] = $casilla26;

        // Casilla 27
        $montoIncisoC = $exenta + $gravada;
        $casilla27 = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_2]);
        $casilla27->loadDefaults();
        $casilla27->rubro = $rubro;
        $casilla27->inciso = 'c';
        $casilla27->casilla = 27;
        $casilla27->monto = $montoIncisoC;
        $casilla27->presentacion_iva_id = $model->id;
        $models_d[] = $casilla27;

        // Casilla 28, 29, 30: exportaciones
        $montoIncisoF = 0;
        // TODO: Exportaciones con Anexo...
        $casilla30 = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_2]);
        $casilla30->loadDefaults();
        $casilla30->rubro = $rubro;
        $casilla30->inciso = 'f';
        $casilla30->casilla = 30;
        $casilla30->monto = (int)round($montoIncisoF);
        $casilla30->presentacion_iva_id = $model->id;
        $models_d[] = $casilla30;

        // Casilla 31
        $montoIncisoG = $montoIncisoC + $montoIncisoF;
        $casilla31 = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_2]);
        $casilla31->loadDefaults();
        $casilla31->rubro = $rubro;
        $casilla31->inciso = 'g';
        $casilla31->casilla = 31;
        $casilla31->monto = (int)round($montoIncisoG);
        $casilla31->presentacion_iva_id = $model->id;
        $models_d[] = $casilla31;

        return $models_d;
    }

    /**
     * @param PresentacionIva $model
     * @param $desde
     * @param $hasta
     * @param $ivaCta5
     * @param $ivaCta10
     * @param $controller PresentacionIvaController
     * @return PresentacionIvaRubro[]
     * @throws \yii\db\Exception
     */
    public static function Rubro3Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $controller)
    {
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $rubro = 3;
        $where = "fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'factura' and compra.empresa_id = {$empresa_id} and compra.periodo_contable_id = {$periodo_id}";
        $sql = "
SELECT IF(civa.porcentaje IS NULL, 0, civa.porcentaje)                       AS porcentaje,
       iva.monto                                                             AS monto,
       compra.cotizacion,
       (iva.monto * IF(compra.cotizacion IS NOT NULL, compra.cotizacion, 1)) AS valorizado
FROM `cont_factura_compra_iva_cuenta_usada` iva
       LEFT JOIN cont_factura_compra compra ON iva.factura_compra_id = compra.id
       LEFT JOIN cont_iva_cuenta ivacta ON iva.iva_cta_id = ivacta.id
       LEFT JOIN core_iva civa ON ivacta.iva_id = civa.id
WHERE {$where}
ORDER BY civa.porcentaje";
        $command = \Yii::$app->getDb()->createCommand($sql);
        $resultSet = $command->queryAll();
        $resultSetFactura = ['exenta' => 0, 'iva5' => 0, 'iva10' => 0];
        /** `$resultSetFactura es de la forma:
         *   ____________________________________________________
         *  |______exenta_____|______iva5______|_____iva10______|
         *  |______valor______|_____valor______|_____valor______|
         */
        foreach ($resultSet as $item) {
            switch ($item['porcentaje']) {
                case 0:
                    $resultSetFactura['exenta'] += round($item['valorizado']);
                    break;
                case 5:
                    $resultSetFactura['iva5'] += round($item['valorizado']);
                    break;
                case 10:
                    $resultSetFactura['iva10'] += round($item['valorizado']);
                    break;
            }
        }

        $models_d = [];
        $col_III_ACDE = 0;

        // Para inciso 'a'.
        $exenta = 0;
        $gravada5 = 0;
        $gravada10 = 0;
        $imp5 = 0;
        $imp10 = 0;
        $totalImp = 0;
        $totalGr = 0;
        foreach ($resultSetFactura as $key => $value) {
            $iva = preg_match('/^iva([0-9]+)$/', $key, $matches);
            if ($iva == 5) {
                $gravada5 += (float)$value / 1.05;
                $imp5 += (float)$value - (float)$value / 1.05;
            } elseif ($iva == 10) {
                $gravada10 += (float)$value / 1.1;
                $imp10 += (float)$value - (float)$value / 1.1;
            } else {
                $exenta += (float)$value;
            }

        }
        $exenta = round($exenta);
        $gravada5 = round($gravada5);
        $gravada10 = round($gravada10);
        $imp5 = round($imp5);
        $imp10 = round($imp10);
        $totalImp = round($imp5 + $imp10);
        $totalGr = round($gravada5 + $gravada10);

        // Casillas 32
        $casilla32 = new PresentacionIvaRubro();
        $casilla32->loadDefaults();
        $casilla32->rubro = $rubro;
        $casilla32->inciso = 'a';
        $casilla32->casilla = 32;
        $casilla32->columna = 'I';
        $casilla32->monto = $gravada5;
        $casilla32->presentacion_iva_id = $model->id;
        $models_d[] = $casilla32;

        // Casillas 35
        $casilla35 = new PresentacionIvaRubro();
        $casilla35->loadDefaults();
        $casilla35->rubro = $rubro;
        $casilla35->inciso = 'a';
        $casilla35->casilla = 35;
        $casilla35->columna = 'II';
        $casilla35->monto = $gravada10;
        $casilla35->presentacion_iva_id = $model->id;
        $models_d[] = $casilla35;

        // Casillas 38
        $casilla38 = new PresentacionIvaRubro();
        $casilla38->loadDefaults();
        $casilla38->rubro = $rubro;
        $casilla38->inciso = 'a';
        $casilla38->casilla = 38;
        $casilla38->columna = 'III';
        $casilla38->monto = $totalImp;
        $casilla38->presentacion_iva_id = $model->id;
        $models_d[] = $casilla38;
        $col_III_ACDE += $totalImp;

        // Para inciso 'b'
        $exenta = 0;
        $gravada5 = 0;
        $gravada10 = 0;
        $imp5 = 0;
        $imp10 = 0;
        $totalImp = 0;
        $totalGr = 0;
        // TODO: Casilla 33, 36, 39, inciso b.
        $exenta = round($exenta);
        $gravada5 = round($gravada5);
        $gravada10 = round($gravada10);
        $imp5 = round($imp5);
        $imp10 = round($imp10);
        $totalImp = round($imp5 + $imp10);
        $totalGr = round($gravada5 + $gravada10);

        // Casilla 33
        $casilla33 = new PresentacionIvaRubro();
        $casilla33->loadDefaults();
        $casilla33->rubro = $rubro;
        $casilla33->inciso = 'b';
        $casilla33->casilla = 33;
        $casilla33->columna = 'I';
        $casilla33->monto = $gravada5;
        $casilla33->presentacion_iva_id = $model->id;
        $models_d[] = $casilla33;

        // Casilla 36
        $casilla36 = new PresentacionIvaRubro();
        $casilla36->loadDefaults();
        $casilla36->rubro = $rubro;
        $casilla36->inciso = 'b';
        $casilla36->casilla = 36;
        $casilla36->columna = 'II';
        $casilla36->monto = $gravada10;
        $casilla36->presentacion_iva_id = $model->id;
        $models_d[] = $casilla36;

        // Casilla 39
        $col_III_inciso_b = $totalImp;
        $casilla39 = new PresentacionIvaRubro();
        $casilla39->loadDefaults();
        $casilla39->rubro = $rubro;
        $casilla39->inciso = 'b';
        $casilla39->casilla = 39;
        $casilla39->columna = 'III';
        $casilla39->monto = $totalImp;
        $casilla39->presentacion_iva_id = $model->id;
        $models_d[] = $casilla39;

        // Para inciso 'c'
        // Casilla 40
        $rubro2IncisoA = PresentacionIvaRubro::findOne(['rubro' => 2, 'inciso' => 'a', 'presentacion_iva_id' => $model->id]);
        $rubro2IncisoC = PresentacionIvaRubro::findOne(['rubro' => 2, 'inciso' => 'c', 'presentacion_iva_id' => $model->id]);
        $casilla40 = new PresentacionIvaRubro();
        $casilla40->loadDefaults();
        $casilla40->rubro = $rubro;
        $casilla40->inciso = 'c';
        $casilla40->casilla = 40;
        $casilla40->columna = 'III';
        $casilla40->monto = round($col_III_inciso_b * (float)($rubro2IncisoA->monto / $rubro2IncisoC->monto));
        $casilla40->presentacion_iva_id = $model->id;
        $models_d[] = $casilla40;
        $col_III_ACDE += $casilla40->monto;

        // Para inciso d
        // TODO: inciso d: exportaciones

        // Para inciso e: notas de credito
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $where = "fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'nota_credito' and compra.empresa_id = {$empresa_id} and compra.periodo_contable_id = {$periodo_id}";
        $sql = "
SELECT IF(civa.porcentaje IS NULL, 0, civa.porcentaje)                       AS porcentaje,
       iva.monto                                                             AS monto,
       compra.cotizacion,
       (iva.monto * IF(compra.cotizacion IS NOT NULL, compra.cotizacion, 1)) AS valorizado
FROM `cont_factura_compra_iva_cuenta_usada` iva
       LEFT JOIN cont_factura_compra compra ON iva.factura_compra_id = compra.id
       LEFT JOIN cont_iva_cuenta ivacta ON iva.iva_cta_id = ivacta.id
       LEFT JOIN core_iva civa ON ivacta.iva_id = civa.id
WHERE {$where}
ORDER BY civa.porcentaje";
        $command = \Yii::$app->getDb()->createCommand($sql);
        $resultSet = $command->queryAll();
        $resultSetCredito = ['exenta' => 0, 'iva5' => 0, 'iva10' => 0];
        /** `$resultSetFactura es de la forma:
         *   ____________________________________________________
         *  |______exenta_____|______iva5______|_____iva10______|
         *  |______valor______|_____valor______|_____valor______|
         */
        foreach ($resultSet as $item) {
            switch ($item['porcentaje']) {
                case 0:
                    $resultSetCredito['exenta'] += round($item['valorizado']);
                    break;
                case 5:
                    $resultSetCredito['iva5'] += round($item['valorizado']);
                    break;
                case 10:
                    $resultSetCredito['iva10'] += round($item['valorizado']);
                    break;
            }
        }

        $exenta = 0;
        $gravada5 = 0;
        $gravada10 = 0;
        $imp5 = 0;
        $imp10 = 0;
        $totalImp = 0;
        $totalGr = 0;
        foreach ($resultSetCredito as $key => $value) {
            $iva = preg_match('/^iva([0-9]+)$/', $key, $matches);
            if ($iva == 5) {
                $gravada5 += (float)$value / 1.05;
                $imp5 += (float)$value - (float)$value / 1.05;
            } elseif ($iva == 10) {
                $gravada10 += (float)$value / 1.1;
                $imp10 += (float)$value - (float)$value / 1.1;
            } else {
                $exenta += (float)$value;
            }

        }
        $exenta = round($exenta);
        $gravada5 = round($gravada5);
        $gravada10 = round($gravada10);
        $imp5 = round($imp5);
        $imp10 = round($imp10);
        $totalImp = round($imp5 + $imp10);
        $totalGr = round($gravada5 + $gravada10);

        // Casilla 34
        $casilla34 = new PresentacionIvaRubro();
        $casilla34->loadDefaults();
        $casilla34->rubro = $rubro;
        $casilla34->inciso = 'e';
        $casilla34->casilla = 34;
        $casilla34->columna = 'I';
        $casilla34->monto = $imp5;
        $casilla34->presentacion_iva_id = $model->id;
        $models_d[] = $casilla34;

        // Casilla 37
        $casilla37 = new PresentacionIvaRubro();
        $casilla37->loadDefaults();
        $casilla37->rubro = $rubro;
        $casilla37->inciso = 'e';
        $casilla37->casilla = 37;
        $casilla37->columna = 'II';
        $casilla37->monto = $imp10;
        $casilla37->presentacion_iva_id = $model->id;
        $models_d[] = $casilla37;

        // Casilla 42
        $casilla42 = new PresentacionIvaRubro();
        $casilla42->loadDefaults();
        $casilla42->rubro = $rubro;
        $casilla42->inciso = 'e';
        $casilla42->casilla = 42;
        $casilla42->columna = 'III';
        $casilla42->monto = $totalImp;
        $casilla42->presentacion_iva_id = $model->id;
        $models_d[] = $casilla42;
        $col_III_ACDE += $totalImp;

        // Para inciso f
        // Casilla 43
        $casilla43 = new PresentacionIvaRubro();
        $casilla43->loadDefaults();
        $casilla43->rubro = $rubro;
        $casilla43->inciso = 'f';
        $casilla43->casilla = 43;
        $casilla43->columna = 'III';
        $casilla43->monto = $col_III_ACDE;
        $casilla43->presentacion_iva_id = $model->id;
        $models_d[] = $casilla43;

        return $models_d;
    }

    /**
     * @param PresentacionIva $model
     * @param $desde
     * @param $hasta
     * @param $ivaCta5
     * @param $ivaCta10
     * @param $controller PresentacionIvaController
     * @return PresentacionIvaRubro[]
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public static function Rubro4Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $controller)
    {
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $rubro = 4;
        $periodoAnterior = date_create_from_format('m-Y', $model->periodo_fiscal)->sub(new DateInterval('P1M'))->format('m-Y');
        $debitoFiscal = "
SELECT SUM(monto)
FROM cont_presentacion_iva_rubro
WHERE rubro = 1
  and inciso = 'i'
  and columna in ('II', 'III')
  and presentacion_iva_id = {$model->id}
  and empresa_id = {$empresa_id}
  and periodo_contable_id = {$periodo_id}";
        $creditoFiscal = "
SELECT monto
FROM cont_presentacion_iva_rubro
WHERE rubro = 3
  and inciso = 'f'
  and presentacion_iva_id = {$model->id}
  and empresa_id = {$empresa_id}
  and periodo_contable_id = {$periodo_id}";
        $saldoFavorContribAnterior = "
SELECT monto
FROM cont_presentacion_iva_rubro rubro
       INNER JOIN cont_presentacion_iva iva on rubro.presentacion_iva_id = iva.id
WHERE rubro.rubro = 4
  and rubro.inciso = 'd'
  and iva.periodo_fiscal = '$periodoAnterior'
#   and rubro.presentacion_iva_id = {$model->id} # si es declaracion del periodo anterior, no tiene la misma cabecera.
  and iva.tipo_declaracion = '{$model->tipo_declaracion}'
  and iva.tipo_formulario = {$model->tipo_formulario}
  and rubro.empresa_id = {$empresa_id}
  and rubro.periodo_contable_id = {$periodo_id}";
        $select = [
            'debito_fiscal' => "({$debitoFiscal})",
            'credito_fiscal' => "({$creditoFiscal})",
            'saldo_favor_ca' => "({$saldoFavorContribAnterior})",
        ];
        $query = new Query();
        $query->select($select);
        $resultSet = $query->createCommand()->queryAll()[0];
        /** `$resultSet` es un array de una sola fila, de la forma:
         *   __________________________________________________
         *  |_debito_fiscal_|_credito_fiscal_|_saldo_favor_ca_|
         *  |_____valor_____|_____valor______|______valor_____|
         */
        /** @var PresentacionIvaRubro[] $rubro4 */
        $rubro4 = [];

        // Incisos a, b, c
        $valorIncisoA = 0;
        $valorIncisoB = 0;
        $valorIncisosBC = 0;
        foreach ($resultSet as $columnaNombre => $valor) {
//            if ($valor == "") continue;
            if ($valor == "") $valor = 0;
            $rubro4_inciso_a_hasta_c = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_4]);
            $rubro4_inciso_a_hasta_c->loadDefaults();
            $rubro4_inciso_a_hasta_c->presentacion_iva_id = $model->id;
            $rubro4_inciso_a_hasta_c->rubro = $rubro;
            $rubro4_inciso_a_hasta_c->monto = $valor;
            switch ($columnaNombre) {
                case 'debito_fiscal':
                    $rubro4_inciso_a_hasta_c->inciso = 'a';
                    $rubro4_inciso_a_hasta_c->casilla = 44;
                    $valorIncisoA = (int)$rubro4_inciso_a_hasta_c->monto; // solo entrara 1 sola vez
                    break;
                case 'credito_fiscal':
                    $rubro4_inciso_a_hasta_c->inciso = 'b';
                    $rubro4_inciso_a_hasta_c->casilla = 45;
                    $valorIncisoB = $rubro4_inciso_a_hasta_c->monto;
                    $valorIncisosBC += (int)$rubro4_inciso_a_hasta_c->monto;
                    break;
                case 'saldo_favor_ca':
                    $rubro4_inciso_a_hasta_c->inciso = 'c';
                    $rubro4_inciso_a_hasta_c->casilla = 46;
                    $valorIncisosBC += (int)$rubro4_inciso_a_hasta_c->monto;
                    break;
            }

            if (!$rubro4_inciso_a_hasta_c->validate()) {
                $inciso = $rubro4_inciso_a_hasta_c->inciso;
                $casilla = $rubro4_inciso_a_hasta_c->casilla;
                $errors = $rubro4_inciso_a_hasta_c->getErrorSummaryAsString();
                throw new \yii\db\Exception("Error validando inciso {$inciso} casilla {$casilla} del rubro 4: {$errors}");
            }
            $rubro4[] = $rubro4_inciso_a_hasta_c;
        }

        // Inciso d
        $incisoD = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_4]);
        $incisoD->loadDefaults();
        $incisoD->presentacion_iva_id = $model->id;
        $incisoD->rubro = $rubro;
        $incisoD->casilla = 47;
        $incisoD->inciso = 'd';
        $incisoD->monto = ($valorIncisoA < $valorIncisosBC) ? abs($valorIncisoA - $valorIncisoB) : 0;
        $rubro4[] = $incisoD;

        // Inciso e
        $incisoE = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_4]);
        $incisoE->loadDefaults();
        $incisoE->presentacion_iva_id = $model->id;
        $incisoE->rubro = $rubro;
        $incisoE->casilla = 48;
        $incisoE->inciso = 'e';
        $incisoE->monto = ($valorIncisoA < $valorIncisosBC) ? 0 : $valorIncisoA - $valorIncisoB;
        $rubro4[] = $incisoE;

        // TODO: Inciso f: exportador; por el momento el valor es cero.
        $incisoF = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_4]);
        $incisoF->loadDefaults();
        $incisoF->presentacion_iva_id = $model->id;
        $incisoF->rubro = $rubro;
        $incisoF->casilla = 49;
        $incisoF->inciso = 'f';
        $incisoF->monto = 0;
        $rubro4[] = $incisoF;

        // Inciso g
        $incisoG = new PresentacionIvaRubro(['scenario' => PresentacionIvaRubro::SCENARIO_RUBRO_4]);
        $incisoG->loadDefaults();
        $incisoG->presentacion_iva_id = $model->id;
        $incisoG->rubro = $rubro;
        $incisoG->casilla = 50;
        $incisoG->inciso = 'g';
        $incisoG->monto = $incisoE->monto - $incisoF->monto;
        $rubro4[] = $incisoG;

        foreach ($rubro4 as $item) {
            if (!$item->validate())
                throw new \Exception("Error validando rubro 4 inciso {$item->inciso} casilla {$item->casilla}: {$item->getErrorSummaryAsString()}");
        }

        return $rubro4;
    }

    /**
     * @param PresentacionIva $model
     * @param $desde
     * @param $hasta
     * @param $ivaCta5
     * @param $ivaCta10
     * @param $controller PresentacionIvaController
     * @return PresentacionIvaRubro[]
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public static function Rubro5Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $controller)
    {
        $rubro = 5;
        $rubros5 = [];

        $periodoAnterior = date_create_from_format('m-Y', $model->periodo_fiscal)->sub(new DateInterval('P1M'))->format('m-Y');

        // Inciso a, col II, casilla 55
        $rubro5 = new PresentacionIvaRubro();
        $rubro5->loadDefaults();
        $rubro5->presentacion_iva_id = $model->id;
        $rubro5->rubro = $rubro;
        $rubro5->inciso = 'a';
        $rubro5->columna = 'II';
        $rubro5->casilla = 55;
        $rubro5->monto = PresentacionIvaRubro::findOne([
            'presentacion_iva_id' => $model->id,
            'rubro' => 4,
            'inciso' => 'g'
        ])->monto;
        $montoIncisoA = $rubro5->monto;
        $rubros5[] = $rubro5;

        // Inciso b, col I, casilla 51
        $rubro5 = new PresentacionIvaRubro();
        $rubro5->loadDefaults();
        $rubro5->presentacion_iva_id = $model->id;
        $rubro5->rubro = $rubro;
        $rubro5->inciso = 'b';
        $rubro5->columna = 'I';
        $rubro5->casilla = 51;
        $rubro5_colI_incF_periodoAnterior = PresentacionIvaRubro::find()->alias('rubro')
            ->leftJoin('cont_presentacion_iva iva', 'iva.presentacion_iva_id = iva.id')
            ->where([
                'iva.tipo_formulario' => $model->tipo_formulario,
                'iva.tipo_declaracion' => $model->tipo_declaracion,
                'iva.periodo_fiscal' => $periodoAnterior,
                'rubro.rubro' => $rubro,
                'rubro.columna' => 'I',
                'rubro.inciso' => 'f',
            ])->one();
        $monto = isset($rubro5_colI_incF_periodoAnterior) ? $rubro5_colI_incF_periodoAnterior->monto : 0;
        $rubro5->monto = $monto;
        $montoIncisoB = $rubro5->monto;
        $rubros5[] = $rubro5;

        // TODO: Inciso c, col I, casilla 52: RETENCIONES
        $empresa_id = \Yii::$app->session->get('core_empresa_actual');
        $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
        $rubro5 = new PresentacionIvaRubro();
        $rubro5->loadDefaults();
        $rubro5->presentacion_iva_id = $model->id;
        $rubro5->rubro = $rubro;
        $rubro5->inciso = 'c';
        $rubro5->columna = 'I';
        $rubro5->casilla = 52;
        {
            $sql = "
SELECT (detalle.base * detalle.factor / 100) AS retencion
from cont_retencion_detalle_ivas detalle
       LEFT JOIN cont_retencion retencion ON detalle.retencion_id = retencion.id
where retencion.empresa_id = {$empresa_id}
      and retencion.periodo_contable_id = {$periodo_id}
      and fecha_emision BETWEEN '{$desde}' and '{$hasta}'
            ";
            $command = \Yii::$app->getDb()->createCommand($sql);
            $resultSet = $command->queryAll();
            $montoRetenido = array_key_exists('retencion', $resultSet) == "" ? 0 : $resultSet['retencion'];

        }
        $rubro5->monto = round($montoRetenido); // retenciones de venta
        $montoIncisoC = $rubro5->monto;
        $rubros5[] = $rubro5;

        // TODO: Inciso d, col II, casilla 56: MULTA POR PRESENTAR DESPUES DEL VENCIMIENTO
        $rubro5 = new PresentacionIvaRubro();
        $rubro5->loadDefaults();
        $rubro5->presentacion_iva_id = $model->id;
        $rubro5->rubro = $rubro;
        $rubro5->inciso = 'd';
        $rubro5->columna = 'II';
        $rubro5->casilla = 56;
        $rubro5->monto = 0;
        $montoIncisoD = $rubro5->monto;
        $rubros5[] = $rubro5;

        // Inciso e, col I, casilla 53: b+c
        $rubro5 = new PresentacionIvaRubro();
        $rubro5->loadDefaults();
        $rubro5->presentacion_iva_id = $model->id;
        $rubro5->rubro = $rubro;
        $rubro5->inciso = 'e';
        $rubro5->columna = 'I';
        $rubro5->casilla = 53;
        $rubro5->monto = (int)$montoIncisoB + (int)$montoIncisoC;
        $montoIncisoE_I = $rubro5->monto;
        $rubros5[] = $rubro5;

        // Inciso e, col II, casilla 57: a+d
        $rubro5 = new PresentacionIvaRubro();
        $rubro5->loadDefaults();
        $rubro5->presentacion_iva_id = $model->id;
        $rubro5->rubro = $rubro;
        $rubro5->inciso = 'e';
        $rubro5->columna = 'II';
        $rubro5->casilla = 57;
        $rubro5->monto = (int)$montoIncisoA + (int)$montoIncisoD;
        $montoIncisoE_II = $rubro5->monto;
        $rubros5[] = $rubro5;

        // Inciso f, col I, casilla 54
        $rubro5 = new PresentacionIvaRubro();
        $rubro5->loadDefaults();
        $rubro5->presentacion_iva_id = $model->id;
        $rubro5->rubro = $rubro;
        $rubro5->inciso = 'f';
        $rubro5->columna = 'I';
        $rubro5->casilla = 54;
        $rubro5->monto = ($montoIncisoE_I > $montoIncisoE_II) ? (int)$montoIncisoE_I - (int)$montoIncisoE_II : 0;
        $rubros5[] = $rubro5;

        // Inciso g, col II, casilla 58
        $rubro5 = new PresentacionIvaRubro();
        $rubro5->loadDefaults();
        $rubro5->presentacion_iva_id = $model->id;
        $rubro5->rubro = $rubro;
        $rubro5->inciso = 'g';
        $rubro5->columna = 'II';
        $rubro5->casilla = 58;
        $rubro5->monto = ($montoIncisoE_I < $montoIncisoE_II) ? abs($montoIncisoE_I - $montoIncisoE_II) : 0;
        $rubros5[] = $rubro5;

        return $rubros5;
    }

    /**
     * @param PresentacionIva $model
     * @param $desde
     * @param $hasta
     * @param $ivaCta5
     * @param $ivaCta10
     * @param $controller PresentacionIvaController
     * @return PresentacionIvaRubro[]
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public static function Rubro6Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $controller)
    {

    }
}