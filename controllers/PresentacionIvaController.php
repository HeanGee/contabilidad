<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\BaseModel;
use backend\models\Iva;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PresentacionIva;
use backend\modules\contabilidad\models\PresentacionIvaRubro;
use backend\modules\contabilidad\models\search\PresentacionIvaSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * PresentacionIvaController implements the CRUD actions for PresentacionIva model.
 */
class PresentacionIvaController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view'],
                'rules' => [
                    [
                        'actions' => ['update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var PresentacionIva $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PresentacionIva models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PresentacionIvaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PresentacionIva model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws BadRequestHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

//        if ($model->tipo_declaracion == PresentacionIva::$_ORIGINAL) {
        $cabecera = [
            'razon_social' => $model->empresa->razon_social,
            'dv' => $model->empresa->digito_verificador,
            'ruc' => $model->empresa->ruc,
            'primer_apellido' => $model->empresa->primer_apellido,
            'segundo_apellido' => $model->empresa->segundo_apellido,
            'primer_nombre' => $model->empresa->primer_nombre,
            'segundo_nombre' => $model->empresa->segundo_nombre,
            'timestamp' => "Fecha: " . date('d-m-Y H:i:s'),
            'periodo_fiscal' => str_replace('-', '', $model->periodo_fiscal),
            'declaracion' => strtoupper(str_replace('_', ' ', $model->tipo_declaracion)),
            'C1' => $model->tipo_declaracion == PresentacionIva::$_ORIGINAL ? true : false,
            'C2' => $model->tipo_declaracion == PresentacionIva::$_RECTIFICATIVA ? true : false,
            'C3' => isset($model->presentacionIva) ? $model->presentacionIva->nro_orden : "",
            'C5' => $model->tipo_declaracion == PresentacionIva::$_CARACTER_CLAUS ? true : false,
            'nombre_formulario' => "Formulario: {$model->tipo_formulario} V3",
            'contribuyente' => "Contribuyente: {$model->empresa->ruc}",
            'control' => "Control: ",
            'presentado_por' => "Presentado por: JMG."
        ];
        $detalle = $model->getPresentacionIvaRubros()->asArray()->all();

        foreach ($detalle as $key => $fila) {
            $detalle[$key]['monto'] = number_format($detalle[$key]['monto'], 0, '', '.');
        }

        return $this->render('_form_iva_marangatu', [
            'model' => $model,
            'cabecera' => $cabecera,
            'detalle' => $detalle,
        ]);
//        }

//        return $this->redirect(['index']);
    }

    /**
     * Finds the PresentacionIva model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PresentacionIva the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PresentacionIva::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new PresentacionIva model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $submit
     * @return mixed
     * @throws \Throwable
     */
    public function actionCreate($submit = null)
    {
        $model = new MesSelectorModel();
        $model->tipo_declaracion = PresentacionIva::$_ORIGINAL;
        $model->tipo_formulario = MesSelectorModel::$_FORMULARIO_120;

        if ($model->load(Yii::$app->request->post())) {
            try {
                $result = null;
                switch ($model->tipo_formulario) {
                    case MesSelectorModel::$_FORMULARIO_120:
                        $result = $this->generar120($model);
                        break;
                    case MesSelectorModel::$_FORMULARIO_121:
                        $result = $this->generar121($model);
                        break;
                    case MesSelectorModel::$_FORMULARIO_125:
                        $result = $this->generar125($model);
                        break;
                    case MesSelectorModel::$_FORMULARIO_126:
                        $result = $this->generar126($model);
                        break;
                }

//                exit();

                if ($result['result'] == false)
                    FlashMessageHelpsers::createWarningMessage($result['messagge']);
                else {
                    FlashMessageHelpsers::createSuccessMessage($result['messagge']);
                }
                return $this->redirect(['index']);
            } catch (\Exception $exception) { // No deberia ocurrir excepcion que provoque entrar a este catch.
                throw $exception;
            }
        }

        return $this->renderAjax('_form_selector_mes', ['model' => $model]);
    }

    /**
     * @param $total_18_21_24 array
     * @param $col_I PresentacionIvaRubro
     */
    protected function acumularTotalesRubroI(&$total_18_21_24, $col_I)
    {
        // En realidad no hace falta....
        if (!array_key_exists(18, $total_18_21_24)) {
            $box = ['col' => 'I', 'monto' => 0];
            $total_18_21_24[18] = $box;
        }
        if (!array_key_exists(21, $total_18_21_24)) {
            $box = ['col' => 'II', 'monto' => 0];
            $total_18_21_24[21] = $box;
        }
        if (!array_key_exists(24, $total_18_21_24)) {
            $box = ['col' => 'III', 'monto' => 0];
            $total_18_21_24[24] = $box;
        }

        // Acumular los totales para el inciso i
        if ($col_I->casilla <= 17)
            $total_18_21_24[18]['monto'] += (int)$col_I->monto;
        elseif ($col_I->casilla <= 20)
            $total_18_21_24[21]['monto'] += (int)$col_I->monto;
        else
            $total_18_21_24[24]['monto'] += (int)$col_I->monto;
    }

    /**
     * @param $model MesSelectorModel
     * @param null $id_pres_rectificado
     * @return array
     * @throws \Throwable
     */
    private function generar120($model, $id_pres_rectificado = null)
    {
        $result = ['result' => false, 'messagge' => 'No se ha realizado ninguna operación.'];
        $transaction = Yii::$app->db->beginTransaction();

        try {
            // Construir rango de fecha desde el primer dia del mes hasta el fin de mes
            $periodo_fiscal = $model->mes_anho;
            $tipo_declaracion = $model->tipo_declaracion;
            $desde = date('Y-m-d', strtotime('01-' . $model->mes_anho));
            $hasta = date('Y-m-t', strtotime('01-' . $model->mes_anho));

            // Verificar si existe declaracion ya presentada para el periodo (mes_anho)
            $iva_declarada = PresentacionIva::find()->where([
                'periodo_fiscal' => $periodo_fiscal,
                'tipo_declaracion' => $tipo_declaracion,
                'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc'),
                'estado' => 'presentado',
                'tipo_formulario' => 120
            ])
                ->andFilterWhere(['!=', 'tipo_declaracion', PresentacionIva::$_RECTIFICATIVA])
                ->all();
            if (!empty($iva_declarada)) {
                throw new \yii\db\Exception('Existe una declaración ya presentada para el mes indicado. Por lo tanto no se puede generar más declaraciones para el mes.');
            }

            // Borrar la declaracion anterior con sus rubros.
            foreach (PresentacionIva::findAll(['periodo_fiscal' => $periodo_fiscal, 'tipo_declaracion' => $tipo_declaracion, 'estado' => 'borrador', 'empresa_id' => Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual_pc')]) as $iva_declarada) {
                if ($iva_declarada->estado == 'borrador') {
                    /** @var PresentacionIvaRubro $presentacionIvaRubro */
                    foreach ($iva_declarada->presentacionIvaRubros as $presentacionIvaRubro) {
                        if (!$presentacionIvaRubro->delete()) {
                            throw new \yii\db\Exception("Error borrando detalles: {$presentacionIvaRubro->getAllErrorsAsString()}");
                        }
                        $presentacionIvaRubro->refresh();
                    }

                    if (!$iva_declarada->delete()) {
                        throw new \yii\db\Exception("Error borrando iva: {$iva_declarada->getAllErrorsAsString()}");
                    }
                    $iva_declarada->refresh();
                }
            }

            $iva10 = Iva::findOne(['porcentaje' => '10']);
            $iva05 = Iva::findOne(['porcentaje' => '5']);
            $ivaCta5 = IvaCuenta::findOne(['iva_id' => $iva05->id]);
            $ivaCta10 = IvaCuenta::findOne(['iva_id' => $iva10->id]);

            // Cabecera de la declaracion (se recicla la variable $model)
            $model = new PresentacionIva();
            $model->loadDefaultValues();
            $model->periodo_fiscal = $periodo_fiscal;
            $model->tipo_formulario = MesSelectorModel::$_FORMULARIO_120;
            $model->tipo_declaracion = $tipo_declaracion;
            $model->presentacion_iva_id = $id_pres_rectificado;

            // Generar id
            if (!$model->save()) {
                throw new \yii\db\Exception("Error de Validación: {$model->getAllErrorsAsString()}");
            }
            {
////            /** ---------- RUBRO I ---------- */
////            // Exenta, iva10 e iva5 de todas las facturas de la empresa y periodo actuales.
////            $sub_sql = "(select id from cont_factura_venta where fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'factura' and empresa_id = {Yii::$app->session->get('core_empresa_actual')} and periodo_contable_id = {Yii::$app->session->get('core_empresa_actual_pc')})";
////            $sql = [
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id IS null and factura_venta_id in {$sub_sql}) as exenta",
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id = {$ivaCta5->id} and factura_venta_id in {$sub_sql}) as iva5",
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id = {$ivaCta10->id} and factura_venta_id in {$sub_sql}) as iva10"
////            ];
////            // Ejecutar query y obtener el resultSet.
////            $query = new Query();
////            $query->select($sql);
////            /** `$query` es un array de una sola fila, de la forma:
////             *   __________________________________________________
////             *  |_____exenta____|______iva5______|______iva10_____|
////             *  |_____valor_____|_____valor______|______valor_____|
////             */
////            $resultSetFactura = $query->createCommand()->queryAll()[0];
////
////            // TODO: Falta Montos imponibles del rubro 1 (-I-), incisos 'd' ~ 'h' (exportaciones, devoluciones, etc...). Usar mismo esquema para incisos 'a' ~ 'c'.
////            // Agregar a la misma variable $models_d.
////            // Inciso d y e son para exportaciones (la funcionalidad aun esta en proceso)
////            // Inciso f, g, h
////            // Exenta, iva10 e iva5 de las notas de credito
////            $sub_sql = "(select id from cont_factura_venta where fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'nota_credito' and empresa_id = {Yii::$app->session->get('core_empresa_actual')} and periodo_contable_id = {Yii::$app->session->get('core_empresa_actual_pc')})";
////            $sql = [
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id IS null and factura_venta_id in {$sub_sql}) as exenta",
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id = {$ivaCta5->id} and factura_venta_id in {$sub_sql}) as iva5",
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id = {$ivaCta10->id} and factura_venta_id in {$sub_sql}) as iva10"
////            ];
////            $query = new Query();
////            $query->select($sql);
////            $resultSetCredito = $query->createCommand()->queryAll()[0];
////
////            // Exenta, iva10 e iva5 de las notas de debito
////            $sub_sql = "(select id from cont_factura_venta where fecha_emision BETWEEN '{$desde}' and '{$hasta}' and para_iva = 'si' and tipo = 'nota_debito' and empresa_id = {Yii::$app->session->get('core_empresa_actual')} and periodo_contable_id = {Yii::$app->session->get('core_empresa_actual_pc')})";
////            $sql = [
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id IS null and factura_venta_id in {$sub_sql}) as exenta",
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id = {$ivaCta5->id} and factura_venta_id in {$sub_sql}) as iva5",
////                "(SELECT sum(monto) FROM `cont_factura_venta_iva_cuenta_usada` WHERE iva_cta_id = {$ivaCta10->id} and factura_venta_id in {$sub_sql}) as iva10"
////            ];
////            $query = new Query();
////            $query->select($sql);
////            $resultSetDebito = $query->createCommand()->queryAll()[0];
////
////            $models_d = [];
////            // Array auxiliar, utilizado para acumular los montos para el inciso i.
////            $total_18_21_24 = [
////                18 => ['col' => 'I', 'monto' => 0],
////                21 => ['col' => 'II', 'monto' => 0],
////                24 => ['col' => 'III', 'monto' => 0]
////            ];
////
////            // Montos Imponibles del rubro 1 (-I-), casillas 'a' ~ 'c' (venta de mercaderias gravadas y exentas)
////            foreach ($resultSetFactura as $key => $value) {
////                $col_I = new PresentacionIvaRubro();
////                $col_I->loadDefaults();
////                $col_I->rubro = 1;
////                $col_I->inciso = $key == 'exenta' ? 'c' : ($key == 'iva5' ? 'b' : 'a');
////                $col_I->casilla = $key == 'exenta' ? 12 : ($key == 'iva5' ? 11 : 10);
////                $col_I->monto = round($value);
////                $col_I->columna = 'I';
////                $col_I->presentacion_iva_id = $model->id;
////                $models_d[] = $col_I;
////
////                // Acumular los totales para el inciso i
////                $this->acumularTotalesRubroI($total_18_21_24, $col_I);
////            }
////
////            // Incisos f, g, h
////            foreach ($resultSetCredito as $key => $value) {
////                $ivaRubro = new PresentacionIvaRubro();
////                $ivaRubro->loadDefaults();
////                $ivaRubro->presentacion_iva_id = $model->id;
////                $ivaRubro->rubro = 1;
////                $ivaRubro->monto = $value;
////                $ivaRubro->columna = 'I';
////                $ivaRubro->inciso = ($key == 'exenta') ? 'h' : (($key == 'iva10') ? 'f' : 'g');
////                $ivaRubro->casilla = ($key == 'exenta') ? 17 : (($key == 'iva10') ? 15 : 16);
////                $models_d[] = $col_I;
////
////                // Acumular los totales para el inciso i
////                $this->acumularTotalesRubroI($total_18_21_24, $col_I);
////            }
////
////            // Guardar montos imponibles del rubro 1 y generar/guardar los ivas debitos (calculados)
////            foreach ($models_d as $col_I) {
////                if (!$col_I->save()) {
////                    throw new \yii\db\Exception("Error de validacion para rubro {$col_I->rubro} inciso {$col_I->inciso} columna {$col_I->columna}: {$col_I->getAllErrorsAsString()}");
////                }
////
////                // Casillas de los IVA DEBITO de los incisos anteriores (-II- o -III-)
////                $col_I->refresh();
////                $iva_dev = new PresentacionIvaRubro();
////                $iva_dev->loadDefaults();
////                $iva_dev->rubro = 1;
////                $iva_dev->inciso = $col_I->inciso;
////                $iva_dev->presentacion_iva_id = $model->id;
////                switch ($col_I->casilla) {
////                    case 10:
////                        $iva_dev->casilla = 22;
////                        $iva_dev->monto = $col_I->monto / 11;
////                        $iva_dev->columna = 'III';
////                        break;
////                    case 11:
////                        $iva_dev->casilla = 19;
////                        $iva_dev->monto = $col_I->monto / 21;
////                        $iva_dev->columna = 'II';
////                        break;
////                    case 15:
////                        $iva_dev->casilla = 23;
////                        $iva_dev->monto = $col_I->monto / 11;
////                        $iva_dev->columna = 'III';
////                        break;
////                    case 16:
////                        $iva_dev->casilla = 20;
////                        $iva_dev->monto = $col_I->monto / 21;
////                        $iva_dev->columna = 'II';
////                }
////
////                // Si al monto imponible no le corresponde ningun iva debito, pasar al siguiente.
////                if (!isset($iva_dev->monto) && !isset($iva_dev->casilla)) continue;
////
////                // Si al monto le corresponde algun iva debito, redondear por si haya decimales.
////                else $iva_dev->monto = round($iva_dev->monto);
////
////                // Acumular los totales para el inciso i
////                $this->acumularTotalesRubroI($total_18_21_24, $iva_dev);
//////
////                if (!$iva_dev->save()) {
////                    throw new \yii\db\Exception("Error de validacion para rubro {$iva_dev->rubro} inciso {$iva_dev->inciso} columna {$iva_dev->columna} (campo calculado): {$iva_dev->getAllErrorsAsString()}");
////                }
////                $iva_dev->refresh();
////            }
//
//            // Inciso i, fila de totales.
//            $models_d = [];
//            foreach ($total_18_21_24 as $key => $box) {
//                $model_d = new PresentacionIvaRubro();
//                $model_d->loadDefaults();
//                $model_d->rubro = 1;
//                $model_d->inciso = 'i';
//                $model_d->casilla = $key;
//                $model_d->monto = round($box['monto']);
//                $model_d->columna = $box['col'];
//                $model_d->presentacion_iva_id = $model->id;
//                $models_d[] = $model_d;
//            }
            }

            /** ---------- RUBRO I ---------- */
            $model->refresh();
            $models_d = RubroManager::Rubro1Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $this);
            foreach ($models_d as $item) {
                if (!$item->save()) {
                    throw new \yii\db\Exception("Error de validacion para rubro {$item->rubro} inciso {$item->inciso} casilla {$item->casilla}: {$item->getAllErrorsAsString()}");
                }
                $item->refresh();
            }

            /** ---------- RUBRO II ---------- */
            $model->refresh();
            $start = implode('-', array_reverse(explode('-', $model->periodo_fiscal))) . '-01';
            $desde = Date("Y-m-d", strtotime("{$start} -5 Months"));
            Yii::warning("hasta: {$hasta}, desde: {$desde}");
            $models_d = RubroManager::Rubro2Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $this);
            foreach ($models_d as $item) {
                if (!$item->save()) {
                    throw new \yii\db\Exception("Error de validacion para rubro {$item->rubro} inciso {$item->inciso} casilla {$item->casilla}: {$item->getAllErrorsAsString()}");
                }
                $item->refresh();
            }

            /** ---------- RUBRO III ---------- */
            $model->refresh();
            $models_d = RubroManager::Rubro3Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $this);
            foreach ($models_d as $item) {
                if (!$item->save()) {
                    throw new \yii\db\Exception("Error de validacion para rubro {$item->rubro} inciso {$item->inciso} casilla {$item->casilla}: {$item->getAllErrorsAsString()}");
                }
                $item->refresh();
            }

            /** ---------- RUBRO IV ---------- */
            $model->refresh();
            $models_d = RubroManager::Rubro4Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $this);
            foreach ($models_d as $item) {
                if (!$item->save()) {
                    throw new \yii\db\Exception("Error de validacion para rubro {$item->rubro} inciso {$item->inciso} casilla {$item->casilla}: {$item->getAllErrorsAsString()}");
                }
                $item->refresh();
            }

            /** ---------- RUBRO V ---------- */
            $model->refresh();
            $models_d = RubroManager::Rubro5Form120($model, $desde, $hasta, $ivaCta5, $ivaCta10, $this);
            foreach ($models_d as $item) {
                if (!$item->save()) {
                    throw new \yii\db\Exception("Error de validacion para rubro {$item->rubro} inciso {$item->inciso} casilla {$item->casilla}: {$item->getAllErrorsAsString()}");
                }
                $item->refresh();
            }


            /** ---------- FIN RUBROS ---------- */


            // A veces guarda los detalles (presentacionIvaRubros) con monto 0
            if (!$model->getPresentacionIvaRubros()->where(['<>', 'monto', 0])->exists())
                throw new \Exception("No existe ningún dato para generar la presentación de IVAs.");

            $transaction->commit();
            $tipo_declaracion = ucfirst(str_replace('_', ' ', $model->tipo_declaracion));
            $result['result'] = true;
            $result['messagge'] = "Declaración {$tipo_declaracion} para {$model->monthYearToString()} se ha generado correctamente con el Formulario {$model->tipo_formulario}.";
            return $result;
        } catch (\Exception $exception) {
//            throw $exception;
            $transaction->rollBack();
            $result['result'] = false;
            $result['messagge'] = $exception->getMessage();
            return $result;
        }
    }

    private function generar121($model, $id_pres_rectificado = null)
    {
        throw new NotFoundHttpException('Formulario 121 aun no se ha implementado.');
    }

    private function generar125($model, $id_pres_rectificado = null)
    {
        throw new NotFoundHttpException('Formulario 125 aun no se ha implementado.');
    }

    private function generar126($model, $id_pres_rectificado = null)
    {
        throw new NotFoundHttpException('Formulario 126 aun no se ha implementado.');
    }

    /**
     * Updates an existing PresentacionIva model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param null $_pjax
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id, $_pjax = null)
    {
//        throw new BadRequestHttpException('Aún no se ha implementado. Se espera utilizar el mismo formulario de la SET.');

        $model = $this->findModel($id);

        $cabecera = [
            'razon_social' => $model->empresa->razon_social,
            'dv' => $model->empresa->digito_verificador,
            'ruc' => $model->empresa->ruc,
            'primer_apellido' => $model->empresa->primer_apellido,
            'segundo_apellido' => $model->empresa->segundo_apellido,
            'primer_nombre' => $model->empresa->primer_nombre,
            'segundo_nombre' => $model->empresa->segundo_nombre,
            'timestamp' => "Fecha: " . date('d-m-Y H:i:s'),
            'periodo_fiscal' => str_replace('-', '', $model->periodo_fiscal),
            'declaracion' => strtoupper(str_replace('_', ' ', $model->tipo_declaracion)),
            'C1' => $model->tipo_declaracion == PresentacionIva::$_ORIGINAL ? true : false,
            'C2' => $model->tipo_declaracion == PresentacionIva::$_RECTIFICATIVA ? true : false,
            'C3' => isset($model->presentacionIva) ? $model->presentacionIva->nro_orden : "",
            'C5' => $model->tipo_declaracion == PresentacionIva::$_CARACTER_CLAUS ? true : false,
            'nombre_formulario' => "Formulario: {$model->tipo_formulario} V3",
            'contribuyente' => "Contribuyente: {$model->empresa->ruc}",
            'control' => "Control: ",
            'presentado_por' => "Presentado por: JMG."
        ];
        $detalle = $model->getPresentacionIvaRubros()->asArray()->all();

        if (!isset($_pjax) && Yii::$app->request->isPost) {
            $transaction = Yii::$app->db->beginTransaction();

            try {
                if (!$model->validate()) {
                    throw new \Exception("Error validando cabecera: {$model->getErrorSummaryAsString()}");
                }

                foreach ($model->presentacionIvaRubros as $presentacionIvaRubro) {
                    if (!$presentacionIvaRubro->delete())
                        throw new \Exception("Error borrando los rubros anteriores: {$presentacionIvaRubro->getErrorSummaryAsString()}");
                }
                $model->refresh();

                /** @var PresentacionIvaRubro[] $rubros */
                $rubros = [];

                foreach ($_POST['PresentacionIvaRubro'] as $pir) {
                    $rubro = new PresentacionIvaRubro();
                    $rubro->loadDefaults();
                    $rubro->presentacion_iva_id = $model->id;
                    $load = ['PresentacionIvaRubro' => $pir];
                    $rubro->load($load);

                    if (in_array($pir['rubro'], [2, 4])) { // rubro 2 y 4 no tienen Columna I, II o III
                        $rubro->scenario = PresentacionIvaRubro::getScenarioBy($pir['rubro']);
                        unset($rubro->columna);
                    }

                    if (!$rubro->validate()) {
                        throw new \Exception("Error validando rubro {$rubro->rubro} inciso {$rubro->inciso} casilla {$rubro->casilla}: {$rubro->getErrorSummaryAsString()}");
                    }
                    $rubros[] = $rubro;
                }

                foreach ($rubros as $rubro) {
                    if (!$rubro->save(false)) {
                        throw new \Exception("Error guardando rubro {$rubro->rubro} inciso {$rubro->inciso} casilla {$rubro->casilla}: {$rubro->getErrorSummaryAsString()}");
                    }
                }

                if (!$model->save(false)) {
                    throw new \Exception("Error guardando Presentacion IVA: {$model->getErrorSummaryAsString()}");
                }
                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("Declaración {$model->getAttributeLabel($model->tipo_declaracion)}
                 para {$model->monthYearToString()} se ha generado correctamente con el Formulario {$model->tipo_formulario}.");
                return $this->redirect(['index']);

            } catch (\Exception $exception) {
                $transaction->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('_form2', [
            'model' => $model,
            'cabecera' => $cabecera,
            'detalle' => $detalle,
        ]);
    }

    /**
     * Deletes an existing PresentacionIva model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $periodo_fiscal_text = $model->monthYearToString();

        $transaction = Yii::$app->db->beginTransaction();

        try {

            if (!$model->isDeleteable())
                // Se puede usar $model->isDeleteable() pero no se puede mostrar Flashes adecuados.
                if ($model->isPresented()) {
                    throw  new \Exception('Esta declaración ya fue presentada. Por lo tanto, no puede modificar ni eliminarlo.');
                } elseif ($model->existsNexts()) {
                    throw new \Exception("Existen declaraciones posteriores al {$model->monthYearToString()}. Por lo tanto no se puede eliminar esta declaración.");
                }

            // Borrar los detalles (rubros y sus incisos)
            foreach ($model->presentacionIvaRubros as $presentacionIvaRubro) {
                if (!$presentacionIvaRubro->delete()) {
                    throw new \Exception("Error elinando detalle: {$presentacionIvaRubro->getAllErrorsAsString()}");
                }
            }
            // Borrar presentacion
            if (!$model->delete()) {
                throw new \Exception("Error eliminando declaración de {$periodo_fiscal_text}: {$model->getAllErrorsAsString()}");
            }

            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage("La declaración de {$periodo_fiscal_text} se eliminó correctamente.");
        } catch (\Exception $exception) {
            $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionPresent($id)
    {
        $model = new MesSelectorModel();

        try {
            if ($model->load(Yii::$app->request->post())) {
                $codigo = $model->codigo_set;
                $model = $this->findModel($id);
                $model->estado = 'presentado';
                $model->codigo_set = $codigo;
                if (!$model->save())
                    throw new \yii\db\Exception("Error cambiando estado a PRESENTADO: {$model->getAllErrorsAsString()}");
                FlashMessageHelpsers::createSuccessMessage("La declaración de {$model->monthYearToString()} ha cambiado su estado a 'Presentado'.");
                return $this->redirect(['index']);
            }
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }

        return $this->renderAjax('_modal_codigo_set', ['model' => $model]);
    }

    public function actionGenerateNext($id)
    {
        $model = $this->findModel($id);
        $periodo_fiscal = $model->periodo_fiscal;
        $sigte_periodo_f = date('m-Y', strtotime('+1 month', strtotime('01-' . $periodo_fiscal)));

        $model_mes_selector = new MesSelectorModel();
        $model_mes_selector->mes_anho = $sigte_periodo_f; // Siguiente periodo fiscal (sigte mes)
        $model_mes_selector->tipo_declaracion = $model->tipo_declaracion;

        $result = null;
        try {
            switch ($model->tipo_formulario) {
                case MesSelectorModel::$_FORMULARIO_120:
                    $result = $this->generar120($model_mes_selector);
                    break;
                case MesSelectorModel::$_FORMULARIO_121:
                    $result = $this->generar121($model_mes_selector);
                    break;
                case MesSelectorModel::$_FORMULARIO_125:
                    $result = $this->generar125($model_mes_selector);
                    break;
                case MesSelectorModel::$_FORMULARIO_126:
                    $result = $this->generar126($model_mes_selector);
                    break;
            }

            if ($result['result'] == false)
                FlashMessageHelpsers::createWarningMessage($result['messagge']);
            else {
                FlashMessageHelpsers::createSuccessMessage($result['messagge']);
            }

            return $this->redirect(['index']);
        } catch (\Exception $exception) { // No deberia ocurrir excepcion que provoque entrar a este catch.
            throw $exception;
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionRectificate($id)
    {
        $model = $this->findModel($id);

        $model_mes_selector = new MesSelectorModel();
        $model_mes_selector->mes_anho = $model->periodo_fiscal; // mismo periodo fiscal que la presentacion a rectificar.
        $model_mes_selector->tipo_declaracion = PresentacionIva::$_RECTIFICATIVA;

        $result = null;
        try {
            switch ($model->tipo_formulario) {
                case MesSelectorModel::$_FORMULARIO_120:
                    $result = $this->generar120($model_mes_selector, $id);
                    break;
                case MesSelectorModel::$_FORMULARIO_121:
                    $result = $this->generar121($model_mes_selector, $id);
                    break;
                case MesSelectorModel::$_FORMULARIO_125:
                    $result = $this->generar125($model_mes_selector, $id);
                    break;
                case MesSelectorModel::$_FORMULARIO_126:
                    $result = $this->generar126($model_mes_selector, $id);
                    break;
            }

            if ($result['result'] == false)
                FlashMessageHelpsers::createWarningMessage($result['messagge']);
            else {
                FlashMessageHelpsers::createSuccessMessage($result['messagge']);
            }

            return $this->redirect(['index']);
        } catch (\Exception $exception) { // No deberia ocurrir excepcion que provoque entrar a este catch.
            throw $exception;
        }
    }

    public function actionMatchEmpresaPeriodo()
    {
        /** @var PresentacionIvaRubro $rubro */
        foreach (PresentacionIvaRubro::find()->all() as $rubro) {
            $rubro->empresa_id = $rubro->iva->empresa_id;
            $rubro->periodo_contable_id = $rubro->iva->periodo_contable_id;
            $rubro->save(false);
        }

        return $this->redirect(['index']);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}


/**
 * @property string $mes_anho
 * @property string $tipo_formulario
 * @property string $tipo_declaracion
 * @property string $codigo_set
 */
class MesSelectorModel extends BaseModel
{
    public static $_FORMULARIO_120 = 120;
    public static $_FORMULARIO_121 = 121;
    public static $_FORMULARIO_125 = 125;
    public static $_FORMULARIO_126 = 126;
    // more types, if exists, define here...

    public static $_FORMULARIOS = [];

    public $mes_anho;
    public $tipo_formulario;
    public $tipo_declaracion;
    public $codigo_set;

    function __construct()
    {
        parent::__construct();
        MesSelectorModel::$_FORMULARIOS[] = MesSelectorModel::$_FORMULARIO_120;
        MesSelectorModel::$_FORMULARIOS[] = MesSelectorModel::$_FORMULARIO_121;
        MesSelectorModel::$_FORMULARIOS[] = MesSelectorModel::$_FORMULARIO_125;
        MesSelectorModel::$_FORMULARIOS[] = MesSelectorModel::$_FORMULARIO_126;
        // more types, if exists, goes here as an item for this array...
    }

    public function rules()
    {
        return [
            [['mes_anho', 'tipo_formulario', 'tipo_declaracion', 'codigo_set'], 'string'],
            [['mes_anho', 'tipo_formulario', 'tipo_declaracion', 'codigo_set'], 'required'],
            [['mes_anho', 'tipo_formulario', 'tipo_declaracion', 'codigo_set'], 'safe'],
        ];
    }

    public static function getTiposFormulario()
    {
        $result = [];
        foreach (MesSelectorModel::$_FORMULARIOS as $key => $formulario) {
            $result[$formulario] = $key + 1 . ' - Formulario ' . $formulario;
        }
        return $result;
    }

    public static function getTipoDeclaracion()
    {
        $_retornar = [];
        try {
            $valores_enum = PresentacionIva::getTableSchema()->columns['tipo_declaracion']->enumValues;
            foreach ($valores_enum as $_v)
                $_retornar[$_v] = str_replace('_', ' ', ucfirst($_v));

        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
        }
        return $_retornar;
    }

    public static function getTipoDeclaracionSinRectif()
    {
        $_retornar = [];
        try {
            $valores_enum = PresentacionIva::getTableSchema()->columns['tipo_declaracion']->enumValues;
            foreach ($valores_enum as $_v)
                if ($_v != 'jurada_rectificativa')
                    $_retornar[$_v] = str_replace('_', ' ', ucfirst($_v));

        } catch (\Exception $e) {
            /* TODO: manejar la excepción */
        }
        return $_retornar;
    }
}