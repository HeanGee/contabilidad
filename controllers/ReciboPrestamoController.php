<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\SessionVariables;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\PrestamoDetalle;
use backend\modules\contabilidad\models\ReciboPrestamo;
use backend\modules\contabilidad\models\ReciboPrestamoDetalle;
use backend\modules\contabilidad\models\search\ReciboPrestamoSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ReciboPrestamoController implements the CRUD actions for ReciboPrestamo model.
 */
class ReciboPrestamoController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'view', 'bloquear', 'desbloquear'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var ReciboPrestamo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc'));
                        }
                    ],
                    [
                        'actions' => ['update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var ReciboPrestamo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['bloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var ReciboPrestamo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'no';
                        }
                    ],
                    [
                        'actions' => ['desbloquear'],
                        'allow' => true,
                        'roles' => ['@'],
                        /**
                         * Solo si pertenecen a la empresa actual
                         */
                        'matchCallback' => function ($rule, $action) {
                            /** @var ReciboPrestamo $model */
                            $model = $this->findModel(Yii::$app->getRequest()->get('id'));
                            return ($model->empresa_id == Yii::$app->session->get(SessionVariables::empresa_actual) &&
                                    $model->periodo_contable_id == Yii::$app->session->get('core_empresa_actual_pc')) && $model->bloqueado == 'si';
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ReciboPrestamo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReciboPrestamoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $session = Yii::$app->session;

        $session->remove('cont_prestamo_cuotas');
        $session->remove('cont_prestamos_recibo');
        $session->remove('cont_recibo_actual_entidad_ruc');
        $session->remove('cont_cuotas_prestamo');
        $session->remove('cont_recibo_valor_total');
        $session->remove('debug');
        $session->remove('cont_recibo_actual_id');
        $session->remove('cont_detallecompra_sim-provider');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReciboPrestamo model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the ReciboPrestamo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ReciboPrestamo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReciboPrestamo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new ReciboPrestamo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionCreate()
    {
        $model = new ReciboPrestamo();
        $model->loadDefaultValues();
        $skey2 = 'cont_prestamos_recibo';
        $session = Yii::$app->session;

        $this->removeBluePrint();

        if ($model->load(Yii::$app->request->post())) {
            $transaccion = Yii::$app->db->beginTransaction();

            try {
                if (!$model->validate()) {
                    $msg = implode(', ', $model->getErrorSummary(true));
                    throw new \Exception("Error validando recibo: {$msg}");
                }

                $model->save(false);
                $model->refresh();

                $prestamosDataProvider = $session->get($skey2, null);

                if (!isset($prestamosDataProvider))
                    throw new \Exception("Ud. no ha especificado ningún préstamo. Utilice el botón naranja para manejar los préstamos registrados.");

                $_reciboPrestamoDetalleGuardar = [];
                $_prestamoDetallesGuardar = [];
                $hay_cuota = false;

                foreach ($prestamosDataProvider as $data) {
                    if ($data['prestamo']->entidad_id != $model->entidad_id) {
                        throw new \Exception("Se ha cambiado de RUC pero no se a especificado las cuotas de esta entidad.");
                    }

                    $ses_detalles = $data['detalles'];

                    /** @var PrestamoDetalle $ses_detalle */
                    foreach ($ses_detalles as $ses_detalle) {

                        if ($ses_detalle['selected'] == '1') {
                            $hay_cuota = true;
                            $old_detalle = PrestamoDetalle::findOne($ses_detalle['id']);

                            if (!$ses_detalle->montosCoherentes($old_detalle)) {
                                $msg = implode(', ', $ses_detalle->getErrorSummary(true));
                                throw new \Exception("Error al procesar datos del prestamo: {$msg}");
                            }

                            $old_detalle->restarSaldo($ses_detalle);
                            $_prestamoDetallesGuardar[] = $old_detalle;

                            $_reciboPrestamoDetalle = new ReciboPrestamoDetalle();
                            $_reciboPrestamoDetalle->loadDefaultValues();
                            $_reciboPrestamoDetalle->recibo_prestamo_id = $model->id;
                            $_reciboPrestamoDetalle->setAttributesBasedOn($ses_detalle->attributes);
                            $_reciboPrestamoDetalleGuardar[] = $_reciboPrestamoDetalle;
                        }
                    }
                }

                if (!$hay_cuota) {
                    throw new \Exception("Debe elegir al menos una cuota de algún préstamo.");
                }

                foreach ($_prestamoDetallesGuardar as $item) {
                    if (!$item->save(false)) {
                        $msg = implode(', ', $item->getErrorSummary(true));
                        throw new \Exception("Error actualizando montos de cuota: {$msg}");
                    }
                }

                foreach ($_reciboPrestamoDetalleGuardar as $item) {
                    if (!$item->save()) {
                        $msg = implode(', ', $item->getErrorSummary(true));
                        throw new \Exception("Error Interno creando cont_prestamo_detalle_compra: {$msg}");
                    }
                }

                $model->refresh();
                $transaccion->commit();
                FlashMessageHelpsers::createSuccessMessage("El recibo #{$model->numero} se ha generado correctamente.");
                return $this->redirect(['index']);

            } catch (\Exception $exception) {
                $transaccion->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ReciboPrestamo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->formatDateField();
        $model->refillCalculatedFields();
        $skey1 = 'cont_prestamos_recibo';
        $session = Yii::$app->session;

        if (!$model->isEditable()) {
            FlashMessageHelpsers::createWarningMessage(implode(', ', ReciboPrestamo::$_EDITABLE_ERROR_MSG));
            return $this->redirect(['index']);
        }

        if (!Yii::$app->request->isPost && !Yii::$app->request->isAjax) {

            if ($model->getPrestamos()->exists()) {
                $data = [];
                /** @var Prestamo[] $prestamos */
                $prestamos = $model->getPrestamos()->all(); // solo prestamos con al menos una cuota cancelada con este recibo.

                foreach ($prestamos as $key => $prestamo) {
                    $prestamos[$key]->refillCalculatedFields();
                    $prestamos[$key]->formatDateField('view');
                    $prestamo_detalles = $prestamos[$key]->prestamoDetalles;
                    $p_det_a_mostrar = [];

                    foreach ($prestamo_detalles as $key2 => $_) {
                        $_prestamod_recibo = ReciboPrestamoDetalle::findOne(['recibo_prestamo_id' => $model->id, 'prestamo_detalle_id' => $prestamo_detalles[$key2]->id]);

                        $prestamo_detalles[$key2]->formatDateField('view');

                        if (isset($_prestamod_recibo)) {
                            $prestamo_detalles[$key2]->capital_saldo = (float)$_prestamod_recibo->capital;
                            $prestamo_detalles[$key2]->interes_saldo = (float)$_prestamod_recibo->interes;
                            $prestamo_detalles[$key2]->monto_cuota_saldo = (float)$_prestamod_recibo->monto_cuota;
                            $prestamo_detalles[$key2]->selected = '1';
                        }

                        // Incluir y mostrar cuotas con saldo > 0, mas cuotas asociadas a este recibo
                        $saldo_mayor_0 = ($prestamo_detalles[$key2]->capital_saldo > 0) &&
                            ($prestamo_detalles[$key2]->interes_saldo > 0) &&
                            ($prestamo_detalles[$key2]->monto_cuota_saldo > 0);
                        if ($saldo_mayor_0 || $prestamo_detalles[$key2]->selected == '1')
                            $p_det_a_mostrar[] = $prestamo_detalles[$key2];
                    }

                    $data[] = [
                        'prestamo' => $prestamo,
                        'detalles' => $p_det_a_mostrar,
                    ];
                }
                $session->set($skey1, $data);
            }
        }

        $this->removeBluePrint();

        if ($model->load(Yii::$app->request->post())) {
            $transaccion = Yii::$app->db->beginTransaction();

            try {
                if (!$model->validate()) {
                    $msg = implode(', ', $model->getErrorSummary(true));
                    throw new \Exception("Error validando recibo: {$msg}");
                }

                $model->save(false);
                $model->refresh();

                $prestamosDataProvider = $session->get($skey1, null);

                if (!isset($prestamosDataProvider))
                    throw new \Exception("Ud. no ha especificado ningún préstamo. Utilice el botón naranja para manejar los préstamos registrados.");

                // borrar detalles anteriores y restaurar saldos.
                foreach ($model->reciboPrestamoDetalle as $item) {
                    $p_det = PrestamoDetalle::findOne(['id' => $item->prestamo_detalle_id]);
                    $p_det->interes_saldo += (float)$item->interes;
                    $p_det->capital_saldo += (float)$item->capital;
                    $p_det->monto_cuota_saldo += (float)$item->monto_cuota;

                    $p_det->save(false);
                    $p_det->refresh();

                    if (!$item->delete()) {
                        $msg = implode(', ', $item->getErrorSummary(true));
                        throw new \Exception("Error borrando detalle anterior (id: {$item->id}): {$msg}");
                    }
                    $item->refresh();
                }

                $_reciboPrestamoDetalleGuardar = [];
                $_prestamoDetallesGuardar = [];
                $hay_cuota = false;
                $msg = [];
                $error_in_cuota = false;

                foreach ($prestamosDataProvider as $data) {
                    if ($data['prestamo']->entidad_id != $model->entidad_id) {
                        throw new \Exception("Se ha cambiado de RUC pero no se a especificado las cuotas de esta entidad.");
                    }

                    $ses_detalles = $data['detalles'];

                    /** @var PrestamoDetalle $ses_detalle */
                    foreach ($ses_detalles as $ses_detalle) {

                        if ($ses_detalle['selected'] == '1') {
                            $hay_cuota = true;
                            $old_detalle = PrestamoDetalle::findOne($ses_detalle['id']);

                            if (!$ses_detalle->montosCoherentes($old_detalle)) {
                                array_push($msg, implode(', ', $ses_detalle->getErrorSummary(true)));
                                $error_in_cuota = true;
                            }

                            if (!$error_in_cuota) {
                                $old_detalle->restarSaldo($ses_detalle);
                                $_prestamoDetallesGuardar[] = $old_detalle;

                                $_reciboPrestamoDetalle = new ReciboPrestamoDetalle();
                                $_reciboPrestamoDetalle->loadDefaultValues();
                                $_reciboPrestamoDetalle->recibo_prestamo_id = $model->id;
                                $_reciboPrestamoDetalle->setAttributesBasedOn($ses_detalle->attributes);
                                $_reciboPrestamoDetalleGuardar[] = $_reciboPrestamoDetalle;
                            }
                        }
                    }
                }

                if ($error_in_cuota) {
                    $msg = implode(', ', $msg);
                    throw new \Exception("Error al procesar datos del prestamo: {$msg}");
                }

                if (!$hay_cuota) {
                    throw new \Exception("Debe elegir al menos una cuota de algún préstamo.");
                }

                foreach ($_prestamoDetallesGuardar as $item) {
                    if (!$item->save(false)) {
                        $msg = implode(', ', $item->getErrorSummary(true));
                        throw new \Exception("Error actualizando montos de cuota: {$msg}");
                    }
                }

                foreach ($_reciboPrestamoDetalleGuardar as $item) {
                    if (!$item->save()) {
                        $msg = implode(', ', $item->getErrorSummary(true));
                        throw new \Exception("Error Interno creando cont_prestamo_detalle_compra: {$msg}");
                    }
                }

                $model->refresh();
                $transaccion->commit();
                FlashMessageHelpsers::createSuccessMessage("El recibo #{$model->numero} se ha generado correctamente.");
                return $this->redirect(['index']);

            } catch (\Exception $exception) {
                $transaccion->rollBack();
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionSimularAsiento()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $prestamoDataProvider = Yii::$app->session->get('cont_prestamos_recibo', []);
        $detalles = [];
        try {
            foreach ($prestamoDataProvider as $data) {
                foreach ($data['detalles'] as $prestamo_detalle) {
                    if ($prestamo_detalle['selected'] == '1') {

                        $_pCapital = (float)$prestamo_detalle->capital_saldo;
                        $_pInteres = (float)$prestamo_detalle->interes_saldo; // sin iva siempre.
                        $_pCuota = (float)$prestamo_detalle->monto_cuota_saldo;

                        // Crear detalles de compra
                        /** @var Prestamo $prestamo */
                        $prestamo = $data['prestamo'];
                        $usar_iva_10 = $prestamo->usar_iva_10;
                        $tiene_factura = $prestamo->tiene_factura;
                        foreach ($prestamo->getCuentasFields() as $cuenta) {
                            if (in_array($cuenta, ['cuenta_intereses_vencer', 'cuenta_intereses_pagados', 'cuenta_monto_operacion', 'cuenta_intereses_a_pagar'])) {
                                switch ($cuenta) {
//                                    case 'cuenta_intereses_pagados':
//                                        $detalle = new DetalleCompra(); // En realidad, aqui no importa si se usa DetalleCompra o DetalleVenta, porque es nada mas a modo de poder reutilizar el view que simula independientemente las cuotas de prestamos. El view es de compra.
//                                        $detalle->plan_cuenta_id = $prestamo->$cuenta;
//                                        $iva = 0;
//                                        $interes = $_pInteres - $iva;
//                                        $detalle->subtotal = $interes;
//                                        $detalle->cta_contable = 'debe';
//                                        $detalle->agrupar = 'no';
//                                        $detalle->cta_principal = 'no';
//                                        $detalle->prestamo_cuota_id = $prestamo_detalle->id;
//                                        $detalles[] = $detalle;
//                                        Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");
//                                        break;
//                                    case 'cuenta_intereses_vencer':
//                                        $detalle = new DetalleCompra();
//                                        $detalle->plan_cuenta_id = $prestamo->$cuenta;
//                                        $iva = 0;
//                                        $interes = $_pInteres - $iva;
//                                        $detalle->subtotal = $interes;
//                                        $detalle->cta_contable = 'haber';
//                                        $detalle->agrupar = 'no';
//                                        $detalle->cta_principal = 'no';
//                                        $detalle->prestamo_cuota_id = $prestamo_detalle->id;
//                                        $detalles[] = $detalle;
//                                        Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");
//                                        break;
                                    case 'cuenta_intereses_a_pagar':
                                        $detalle = new DetalleCompra();
                                        $detalle->plan_cuenta_id = $prestamo->$cuenta;
                                        $iva = round($_pInteres / 10);
                                        $interes = $_pInteres;
                                        $detalle->subtotal = $interes;
                                        $detalle->cta_contable = 'debe';
                                        $detalle->agrupar = 'no';
                                        $detalle->cta_principal = 'no';
                                        $detalle->prestamo_cuota_id = $prestamo_detalle->id;

                                        if ($prestamo->isCase4()) {
                                            $detalles[] = $detalle;
                                            Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");

                                            $detalle = new DetalleCompra();
                                            $detalle->plan_cuenta_id = $prestamo->cuenta_iva_10; // por cuenta iva 10%
                                            $detalle->subtotal = $iva;
                                            $detalle->cta_contable = 'debe';
                                            $detalle->agrupar = 'no';
                                            $detalle->cta_principal = 'no';
                                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                            $detalles[] = $detalle;
                                            Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");

                                            $detalle = new DetalleCompra();
                                            $detalle->plan_cuenta_id = $prestamo->cuenta_caja; // por caja o banco
                                            $detalle->subtotal = $_pInteres + $iva;
                                            $detalle->cta_contable = 'haber';
                                            $detalle->agrupar = 'no';
                                            $detalle->cta_principal = 'no';
                                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                            $detalles[] = $detalle;
                                            Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");

                                        } else {
                                            $detalles[] = $detalle;

                                            $detalle = new DetalleCompra();
                                            $detalle->plan_cuenta_id = $prestamo->cuenta_caja; // por caja o banco
                                            $detalle->subtotal = $_pInteres;
                                            $detalle->cta_contable = 'haber';
                                            $detalle->agrupar = 'no';
                                            $detalle->cta_principal = 'no';
                                            $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                            $detalles[] = $detalle;
                                            Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");
                                        }
                                        break;
                                    case 'cuenta_monto_operacion':
                                        $iva = round($_pInteres / 10);
                                        $capital = $_pCapital + $iva;
                                        Yii::warning('capital: ' . $_pCapital);
                                        Yii::warning('iva del int: ' . $iva);
                                        Yii::warning('capital toal: ' . $capital);
                                        $detalle = new DetalleCompra();
                                        $detalle->plan_cuenta_id = $prestamo->$cuenta; // por cuenta PRESTAMOS A BANCO XX ...
                                        $detalle->subtotal = $capital;
                                        $detalle->cta_contable = 'debe';
                                        $detalle->agrupar = 'no';
                                        $detalle->cta_principal = 'si';
                                        $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                        $detalles[] = $detalle;
                                        Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");

                                        $detalle = new DetalleCompra();
                                        $detalle->plan_cuenta_id = $prestamo->cuenta_caja; // por caja o banco, nuevamente
                                        $detalle->subtotal = $capital;
                                        $detalle->cta_contable = 'haber';
                                        $detalle->agrupar = 'no';
                                        $detalle->cta_principal = 'no';
                                        $detalle->prestamo_cuota_id = $prestamo_detalle->id;
                                        $detalles[] = $detalle;
                                        Yii::warning("anadiendo cuenta {$detalle->planCuenta->nombre} con valor $detalle->subtotal.");
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            $detalles = DetalleCompra::agruparCuentas($detalles);
            $detalles = self::agruparDebeHaber($detalles);

        } catch (\Exception $exception) {
            return ['errors' => $exception->getMessage()];
        }

        Yii::$app->session->set('cont_detallecompra_sim-provider', new ArrayDataProvider([
            'allModels' => $detalles, 'pagination' => false
        ]));

        return ['errors' => false];
    }

    /**
     * @param DetalleCompra[] $array Detalles de compra
     * @return array Detalles de compra agrupados en debe / haber
     */
    private function agruparDebeHaber($array)
    {
        $debes = [];
        $haberes = [];

        foreach ($array as $item) {
            if ($item->cta_contable == 'debe') {
                $debes[] = $item;
            } elseif ($item->cta_contable == 'haber') {
                $haberes[] = $item;
            }
        }

        return array_values(array_merge($debes, $haberes));
    }

    /**
     * Deletes an existing ReciboPrestamo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $transaccion = Yii::$app->db->beginTransaction();

        try {

            if (!$model->isEditable()) {
                $msg = implode(', ', ReciboPrestamo::$_EDITABLE_ERROR_MSG) . '.';
                throw new \Exception($msg);
            }

            foreach ($model->reciboPrestamoDetalle as $item) {
                $prestamoDetalle = PrestamoDetalle::findOne($item->prestamo_detalle_id);
                $prestamoDetalle->capital_saldo += (float)$item->capital;
                $prestamoDetalle->interes_saldo += (float)$item->interes;
                $prestamoDetalle->monto_cuota_saldo += (float)$item->monto_cuota;

                if (!$prestamoDetalle->save(false)) {
                    $msg = implode(', ', $prestamoDetalle->getErrorSummary(true));
                    throw new Exception("Error recuperando saldo en prestamos detalle: {$msg}");
                }

                if (!$item->delete()) {
                    $msg = implode(', ', $item->getErrorSummary(true));
                    throw new \Exception("Error borrando los detalles del recibo: {$msg}");
                }
            }

            if (!$model->delete()) {
                $msg = implode(', ', $model->getErrorSummary(true));
                throw new \Exception("Error borrando recibo: {$msg}");
            }

            $transaccion->commit();
            FlashMessageHelpsers::createSuccessMessage("El Recibo #{$model->numero} se ha borrado correctamente.");
            return $this->redirect(['index']);

        } catch (\Exception $exception) {
            $transaccion->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }

    private function removeBluePrint()
    {

        if (array_key_exists('PrestamoDetalle', $_POST)) {
            if (!empty($_POST['PrestamoDetalle']['__new__'])) {
                unset($_POST['PrestamoDetalle']['__new__']);
                Yii::$app->request->setBodyParams($_POST);
            }
        }
    }

    public function actionShowCuotas()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $session = Yii::$app->session;
        $skey1 = 'cont_prestamos_recibo';

        $prestamo_id = Yii::$app->request->get('prestamo_id');
        $session_data = $session->get($skey1, []);

        foreach ($session_data as $session_datum) {
            if ($session_datum['prestamo']->id == $prestamo_id) {
                $prestamo_detalles = $session_datum['detalles'];
                Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
                    'allModels' => $prestamo_detalles,
                    'pagination' => false,
                ]));
                return ['result' => '', 'error' => ''];
            }
        }

        /** @var ActiveQuery $prestamo_detalles */
        $prestamo_detalles = PrestamoDetalle::find()
            ->where(['prestamo_id' => $prestamo_id])
            ->andWhere([
                'OR',
                ['>', 'capital', 0],
                [
                    'OR',
                    ['>', 'interes', 0],
                    ['>', 'monto_cuota', 0]
                ]
            ]);

        /**
         * @var  $key
         * @var  $prestamo_detalles PrestamoDetalle[]
         */
        $prestamo_detalles = $prestamo_detalles->all();
        foreach ($prestamo_detalles as $key => $prestamo_detalle) {
            $prestamo_detalles[$key]->formatDateField('view');
        }

        Yii::$app->session->set('cont_prestamo_cuotas', new ArrayDataProvider([
            'allModels' => $prestamo_detalles,
            'pagination' => false,
        ]));

        return ['result' => '', 'error' => ''];
    }

    public function actionBloquear($id)
    {
        $model = ReciboPrestamo::findOne(['id' => $id]);

        if ($model->bloqueado == 'no') {
            $model->bloqueado = 'si';

            //Solo para validacion
            $model->fecha = date('d-m-Y', strtotime($model->fecha));

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Recibo ha sido bloqueado.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede bloquear recibo. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }

    public function actionDesbloquear($id)
    {
        $model = ReciboPrestamo::findOne(['id' => $id]);

        if ($model->bloqueado == 'si') {
            $model->bloqueado = 'no';

            //Solo para validacion
            $model->fecha = date('d-m-Y', strtotime($model->fecha));

            if ($model->save())
                FlashMessageHelpsers::createSuccessMessage('Recibo ha sido desbloqueado.');
            else
                FlashMessageHelpsers::createErrorMessage('No se puede desbloquear recibo. ' . $model->getErrorSummaryAsString());
        }

        return $this->redirect(['index']);
    }
}
