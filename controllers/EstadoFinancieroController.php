<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\auxiliar\ArchivoEstadoFinanciero;
use backend\modules\contabilidad\models\CoeficienteRevaluo;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\EstadoFinanciero;
use backend\modules\contabilidad\models\Nodo;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\FlashMessageHelpsers;
use Exception;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DataType as ExcelDataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Font;
use Yii;
use yii\web\UploadedFile;

class EstadoFinancieroController extends BaseController
{
    private $empresa_id = null;
    private $periodo_id = null;

    private function setEmpresaPeriodo()
    {
        if (!isset($this->empresa_id)) {
            $this->empresa_id = Yii::$app->session->get('core_empresa_actual');
            $this->periodo_id = Yii::$app->session->get('core_empresa_actual_pc');
        }
    }

    private function getEmpresaActual()
    {
        self::setEmpresaPeriodo();
        return $this->empresa_id;
    }

    private function getPeriodoActual()
    {
        self::setEmpresaPeriodo();
        return $this->periodo_id;
    }

    /**
     * @param EstadoFinanciero $model
     */
    private function setEmpresaPeriodoActual(&$model)
    {
        $model->empresa_id = self::getEmpresaActual();
        $model->periodo_contable_id = self::getPeriodoActual();
    }

    public function actionGenerar()
    {
        # Check if exist at least 1 cuenta for flujo de efectivo.
        if (!PlanCuenta::find()->where(['!=', 'flujo_efectivo', 'no'])->exists()) {
            FlashMessageHelpsers::createWarningMessage("NO se ha configurado ninguna cuenta para `Flujo de Efectivos`.");
            return $this->redirect(['/']);
        }

        $model = new EstadoFinanciero();
        $model->loadTest();
        self::setEmpresaPeriodoActual($model);

        if (!Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            try {
                // verificar rango de fecha del post
                if (!$model->verifyDate())
                    throw new Exception("El formato de fecha es incorrecto.");

                // generar balance.
                $model->setBalance();
                if (!$model->isBalanceSetted())
                    throw new Exception("No existe todavía ningún asiento generado.");

                // generar arbol de cuentas con montos.
                $arboles = $model->getArboles();

                // generar arbol del periodo anterior
                $periodo = EmpresaPeriodoContable::findOne(['id' => self::getPeriodoActual()]);
                $pastYear = isset($periodo->periodoAnterior) ? $periodo->periodoAnterior->anho : '';
                $modelPeriodoAnterior = new EstadoFinanciero();
                $modelPeriodoAnterior->contador = $model->contador;
                $modelPeriodoAnterior->ruc_contador = $model->ruc_contador;
                $modelPeriodoAnterior->auditor = $model->auditor;
                $modelPeriodoAnterior->ruc_auditor = $model->ruc_auditor;
                $modelPeriodoAnterior->rango = "$pastYear-01-01 - $pastYear-12-31";
                $modelPeriodoAnterior->setBalance();
                $arbolesPeriodoAnterior = $modelPeriodoAnterior->getArboles();

                $fileName = $_FILES['EstadoFinanciero']['name']['archivo'];
                if ($fileName != '') {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $archivo = new ArchivoEstadoFinanciero();
                        $archivo->file = UploadedFile::getInstance($model, 'archivo');
                        if (($absFilePath = $archivo->upload())) {  # upload() is throwable.
                            $model->excel_file_name = $absFilePath;
                            $_model = EstadoFinanciero::findOne(['excel_file_name' => $absFilePath]);
                            if (isset($_model)) {
                                $attributes = $model->attributes;
                                unset($attributes['id']);
                                $_model->setAttributes($attributes);
                                $model = $_model;
                            }

                            if (!$model->save()) {
                                $msg[] = "No se pudo validar el formulario: {$model->getErrorSummaryAsString()}";
                                if (!$archivo->deleteUploaded())
                                    $msg[] = "No se pudo eliminar el archivo subido: {$model->excel_file_name}";
                                throw new Exception(implode(', ', $msg));
                            }
                            $transaction->commit();
                            FlashMessageHelpsers::createSuccessMessage("Reporte generado y guardado correctamente.");
                        }
                    } catch (Exception $exception) {
                        $transaction->rollBack();
                        throw $exception;
                    }
                } else {
                    self::generarExcel($model, $arboles, $arbolesPeriodoAnterior);
                }
            } catch (Exception $exception) {
//                throw $exception;
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('generar', ['model' => $model]);
    }

    /**
     * @param EstadoFinanciero $model
     * @param Nodo[] $arboles
     * @param $arbolesPeriodoAnterior
     * @throws Exception
     */
    private function generarExcel($model, $arboles, $arbolesPeriodoAnterior)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        date_default_timezone_set('America/Asuncion');

        // Abrir template
        $carpeta = Yii::$app->basePath . '/modules/contabilidad/views/estado-financiero/';
        $objPHPExcel = IOFactory::load($carpeta . 'template_resoluc_49.xls');

        // Rellenar cabecera de las hojas
        self::rellenarCabeceraHojas($objPHPExcel, $model);

        // Rellenar Hoja 1
        self::rellenarBalanceGeneral($objPHPExcel, $arboles, $arbolesPeriodoAnterior);

        // Rellenar Hoja 2
        self::rellenarEstadoDeResultados($objPHPExcel, $arboles, $arbolesPeriodoAnterior);

        // Rellenar Hoja 3
        self::rellenarFlujoEfectivo($objPHPExcel, $model, $arboles, $arbolesPeriodoAnterior);

        // Rellenar Hoja 4

        // Rellenar Hoja 5
        self::rellenarNotasContables($objPHPExcel, $model);

        // Rellenar Hoja 6
        self::rellenarCuadroRevaluo($objPHPExcel);

        // Descargar
        self::exportar($objPHPExcel, "estado_financiero_empresa_{$this->getEmpresaActual()}_periodo_{$this->getPeriodoActual()}");
    }

    /**
     * @param Spreadsheet $objPHPExcel
     * @param EstadoFinanciero $model
     * @throws Exception
     */
    private function rellenarCabeceraHojas(&$objPHPExcel, $model)
    {
        $empresa = Empresa::findOne(['id' => self::getEmpresaActual()]);
        $nombreLegal = $empresa->primer_apellido . ($empresa->segundo_apellido == "" ? "" : $empresa->segundo_apellido) . ', ' . $empresa->primer_nombre . ($empresa->segundo_nombre == '' ? "" : $empresa->segundo_nombre);

        // Hoja 1: Balance General
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue("A7", $empresa->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValue("F7", $empresa->ruc);
        $objPHPExcel->getActiveSheet()->setCellValue("G7", $model->getDesde(true));
        $objPHPExcel->getActiveSheet()->setCellValue("H7", $model->getHasta(true));
        $objPHPExcel->getActiveSheet()->setCellValue("A11", $nombreLegal);
        $objPHPExcel->getActiveSheet()->setCellValue("C11", $model->contador);
        $objPHPExcel->getActiveSheet()->setCellValue("E11", $model->ruc_contador);
        $objPHPExcel->getActiveSheet()->setCellValue("F11", $model->auditor);
        $objPHPExcel->getActiveSheet()->setCellValue("H11", $model->ruc_auditor);

        // Hoja 2: Estado de Resultados
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setCellValue("A7", $empresa->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValue("F7", $empresa->ruc);
        $objPHPExcel->getActiveSheet()->setCellValue("H7", $model->getDesde(true));
        $objPHPExcel->getActiveSheet()->setCellValue("I7", $model->getHasta(true));
        $objPHPExcel->getActiveSheet()->setCellValue("A11", $nombreLegal);
        $objPHPExcel->getActiveSheet()->setCellValue("C11", $model->contador);
        $objPHPExcel->getActiveSheet()->setCellValue("E11", $model->ruc_contador);
        $objPHPExcel->getActiveSheet()->setCellValue("F11", $model->auditor);
        $objPHPExcel->getActiveSheet()->setCellValue("H11", $model->ruc_auditor);

        // Hoja 3: Flujo de Efectivo
        $objPHPExcel->setActiveSheetIndex(2);
        $objPHPExcel->getActiveSheet()->setCellValue("A6", $empresa->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValue("D6", $empresa->ruc);
        $objPHPExcel->getActiveSheet()->setCellValue("F6", $model->getDesde(true));
        $objPHPExcel->getActiveSheet()->setCellValue("H6", $model->getHasta(true));
        $objPHPExcel->getActiveSheet()->setCellValue("A10", $nombreLegal);
        $objPHPExcel->getActiveSheet()->setCellValue("C10", $model->contador);
        $objPHPExcel->getActiveSheet()->setCellValue("E10", $model->ruc_contador);
        $objPHPExcel->getActiveSheet()->setCellValue("F10", $model->auditor);
        $objPHPExcel->getActiveSheet()->setCellValue("H10", $model->ruc_auditor);

        // Hoja auxiliar: Flujo de Efectivo - Hoja Auxiliar
        $objPHPExcel->setActiveSheetIndex(6);
        $objPHPExcel->getActiveSheet()->setCellValue("B5", $empresa->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValue("D5", "{$empresa->ruc}-{$empresa->digito_verificador}");
        $objPHPExcel->getActiveSheet()->setCellValue("E5", $model->getDesde(true));
        $objPHPExcel->getActiveSheet()->setCellValue("F5", $model->getHasta(true));

        // Hoja 4: Estado de patrimonio
        $objPHPExcel->setActiveSheetIndex(3);
        $objPHPExcel->getActiveSheet()->setCellValue("A5", $empresa->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValue("E5", $empresa->ruc);
        $objPHPExcel->getActiveSheet()->setCellValue("H5", $model->getDesde(true));
        $objPHPExcel->getActiveSheet()->setCellValue("I5", $model->getHasta(true));
        $objPHPExcel->getActiveSheet()->setCellValue("A9", $nombreLegal);
        $objPHPExcel->getActiveSheet()->setCellValue("D9", $model->contador);
        $objPHPExcel->getActiveSheet()->setCellValue("F9", $model->ruc_contador);
        $objPHPExcel->getActiveSheet()->setCellValue("G9", ""); // TODO: Formulario Nro.
        $objPHPExcel->getActiveSheet()->setCellValue("H9", ""); // TODO: Nro de Orden.

        // Hoja 5: Notas Contables
        $objPHPExcel->setActiveSheetIndex(4);
        $objPHPExcel->getActiveSheet()->setCellValue("A6", $empresa->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValue("D6", $empresa->ruc);
        $objPHPExcel->getActiveSheet()->setCellValue("F6", $model->getDesde(true));
        $objPHPExcel->getActiveSheet()->setCellValue("G6", $model->getHasta(true));
        $objPHPExcel->getActiveSheet()->setCellValue("A10", $nombreLegal);
        $objPHPExcel->getActiveSheet()->setCellValue("C10", $model->contador);
        $objPHPExcel->getActiveSheet()->setCellValue("E10", $model->ruc_contador);
        $objPHPExcel->getActiveSheet()->setCellValue("F10", $model->auditor);
        $objPHPExcel->getActiveSheet()->setCellValue("G10", $model->ruc_auditor);

        // Hoja 6: Cuadro de revaluo & depreciacion
        $objPHPExcel->setActiveSheetIndex(5);
        $objPHPExcel->getActiveSheet()->setCellValue("A7", $empresa->razon_social);
        $objPHPExcel->getActiveSheet()->setCellValue("F7", $empresa->ruc);
        $objPHPExcel->getActiveSheet()->setCellValue("H7", ""); // TODO: Formulario utilizado
        $objPHPExcel->getActiveSheet()->setCellValue("J7", ""); // TODO: Nro orden
        $objPHPExcel->getActiveSheet()->setCellValue("M7", $model->getDesde(true));
        $objPHPExcel->getActiveSheet()->setCellValue("O7", $model->getHasta(true));
        $objPHPExcel->getActiveSheet()->setCellValue("A11", $nombreLegal);
        $objPHPExcel->getActiveSheet()->setCellValue("G11", $model->contador);
        $objPHPExcel->getActiveSheet()->setCellValue("K11", $model->ruc_contador);
        $objPHPExcel->getActiveSheet()->setCellValue("M11", ""); // TODO: Formulario nro
        $objPHPExcel->getActiveSheet()->setCellValue("O11", ""); // TODO: Nro orden
    }

    /**
     * @param Spreadsheet $objPHPExcel
     * @param Nodo[] $arboles
     * @param Nodo[] $arbolesPeriodoAnterior
     * @param int $fila
     * @param int $deep
     * @throws Exception
     */
    private function rellenarBalanceGeneral(&$objPHPExcel, $arboles, $arbolesPeriodoAnterior, &$fila = 14, $deep = 1, $prevCtaLeng = 0)
    {
        $original_deep = $deep;
        if ($deep == 1) {
            $objPHPExcel->setActiveSheetIndex(0); // seleccionar la hoja correspondiente.
        }

        foreach ($arboles as $index => $arbol) {
            if ($arbol->cuenta->cod_completo == '4') break;

            $curCtaCodLeng = sizeof(explode('.', $arbol->cuenta->cod_completo));
            $noAsentable = ($curCtaCodLeng <= 3);
            $addEmptyRow = $noAsentable && ($curCtaCodLeng <= $prevCtaLeng);

            if ($fila > 16 && $addEmptyRow) {
                $fila++;
            }

            // rellenar fila de datos
            $objPHPExcel->getActiveSheet()->getCell("A$fila")->setValueExplicit($arbol->cuenta->cod_completo, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCell("B$fila")->setValueExplicit($arbol->cuenta->nombre, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCell("G$fila")->setValueExplicit($arbol->monto, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCell("H$fila")->setValueExplicit($arbolesPeriodoAnterior[$index]->monto, ExcelDataType::TYPE_NUMERIC);

            $objPHPExcel->getActiveSheet()->getStyle("A$fila:B$fila")->getFont()->setBold($noAsentable);

            $fila++;
            $deep++;
            $prevCtaLeng = sizeof(explode('.', $arbol->cuenta->cod_completo));
            self::rellenarBalanceGeneral($objPHPExcel, $arbol->hijos, $arbolesPeriodoAnterior[$index]->hijos, $fila, $deep, $prevCtaLeng);
        }

        // aplicar formatos
        if ($original_deep == 1) {
            $fila--;
            $objPHPExcel->getActiveSheet()->getStyle("A$fila:H$fila")->getBorders()->getBottom()->setBorderStyle('medium');

            $objPHPExcel->getActiveSheet()->getStyle("A14:A$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle("B14:B$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle("G14:G$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle("H14:H$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle("G14:G$fila")->getNumberFormat()->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle("H14:H$fila")->getNumberFormat()->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle("A14:A$fila")->getBorders()->getLeft()->setBorderStyle('medium');
            $objPHPExcel->getActiveSheet()->getStyle("F14:F$fila")->getBorders()->getRight()->setBorderStyle('medium');
            $objPHPExcel->getActiveSheet()->getStyle("G14:G$fila")->getBorders()->getRight()->setBorderStyle('medium');
            $objPHPExcel->getActiveSheet()->getStyle("H14:H$fila")->getBorders()->getRight()->setBorderStyle('medium');
        }
    }

    /**
     * @param Spreadsheet $objPHPExcel
     * @param Nodo[] $arboles
     * @param Nodo[] $arbolesPeriodoAnterior
     * @param int $fila
     * @param int $deep
     * @throws Exception
     */
    private function rellenarEstadoDeResultados(&$objPHPExcel, $arboles, $arbolesPeriodoAnterior, &$fila = 14, $deep = 1, $prevCtaLeng = 0)
    {
        $original_deep = $deep;
        if ($deep == 1) {
            $objPHPExcel->setActiveSheetIndex(1);
        }

        foreach ($arboles as $index => $arbol) {
            if (in_array($arbol->cuenta->cod_completo, ['1', '2', '3'])) continue;

            $curCtaCodLeng = sizeof(explode('.', $arbol->cuenta->cod_completo));
            $noAsentable = ($curCtaCodLeng <= 3);
            $addEmptyRow = $noAsentable && ($curCtaCodLeng <= $prevCtaLeng);

            if ($fila > 16 && $addEmptyRow) {
                $fila++;
            }

            $objPHPExcel->getActiveSheet()->getCell("A$fila")->setValueExplicit($arbol->cuenta->cod_completo, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCell("B$fila")->setValueExplicit($arbol->cuenta->nombre, ExcelDataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCell("H$fila")->setValueExplicit($arbol->monto, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCell("I$fila")->setValueExplicit($arbolesPeriodoAnterior[$index]->monto, ExcelDataType::TYPE_NUMERIC);

            $objPHPExcel->getActiveSheet()->getStyle("A$fila:B$fila")->getFont()->setBold($noAsentable);

            $fila++;
            $deep++;
            $prevCtaLeng = sizeof(explode('.', $arbol->cuenta->cod_completo));
            self::rellenarEstadoDeResultados($objPHPExcel, $arbol->hijos, $arbolesPeriodoAnterior[$index]->hijos, $fila, $deep, $prevCtaLeng);
        }

        if ($original_deep == 1) {
            $fila--;
            $objPHPExcel->getActiveSheet()->getStyle("A$fila:I$fila")->getBorders()->getBottom()->setBorderStyle('medium');

            $objPHPExcel->getActiveSheet()->getStyle("A14:A$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle("B14:B$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyle("H14:H$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle("I14:I$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle("H14:H$fila")->getNumberFormat()->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle("I14:I$fila")->getNumberFormat()->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle("A14:A$fila")->getBorders()->getLeft()->setBorderStyle('medium');
            $objPHPExcel->getActiveSheet()->getStyle("G14:G$fila")->getBorders()->getRight()->setBorderStyle('medium');
            $objPHPExcel->getActiveSheet()->getStyle("H14:H$fila")->getBorders()->getRight()->setBorderStyle('medium');
            $objPHPExcel->getActiveSheet()->getStyle("I14:I$fila")->getBorders()->getRight()->setBorderStyle('medium');
        }
    }

    /**
     * @param Spreadsheet $objPHPExcel
     * @param Nodo[] $arboles
     * @param EstadoFinanciero $model
     * @param Nodo[] $arbolesPeriodoAnterior
     * @throws Exception
     */
    private function rellenarFlujoEfectivo(&$objPHPExcel, &$model, $arboles, $arbolesPeriodoAnterior)
    {
        # Reset Monto to zero and...
        # ... Carry up `monto` of nodes only for `flujo de efectivos`.
        foreach ($arboles as $key => $nodo) {
            $nodo->resetMonto();
            $arbolesPeriodoAnterior[$key]->resetMonto();

            $nodo->carryUpMontoFlujoEfectivo();
            $arbolesPeriodoAnterior[$key]->carryUpMontoFlujoEfectivo();
        }

        $fila = 9;
        $filaInicial = $fila;
        $objPHPExcel->setActiveSheetIndex(6); // hoja auxiliar
        $sheet = $objPHPExcel->getActiveSheet();

        # Activos
        self::rellenarFlujoEfectivoAuxiliar($objPHPExcel, $model, $fila, $arboles[0], $arbolesPeriodoAnterior[0]);
        $sheet->getCell("B$fila")->setValue("Total Activo");
        $sheet->getCell("C$fila")->setValueExplicit($arboles[0]->monto, DataType::TYPE_NUMERIC);
        $sheet->getCell("F$fila")->setValueExplicit($arbolesPeriodoAnterior[0]->monto, DataType::TYPE_NUMERIC);

        $sheet->getStyle("B$fila:F$fila")->getFont()->setBold(true);
        $sheet->getStyle("B$fila:F$fila")->getFont()->setColor(new Color(Color::COLOR_RED));
        $sheet->getStyle("B$fila:T$fila")->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFffbf00');

        # Pasivos
        $fila++;
        self::rellenarFlujoEfectivoAuxiliar($objPHPExcel, $model, $fila, $arboles[1], $arbolesPeriodoAnterior[1]);
        $sheet->getCell("B$fila")->setValue("Total Pasivo");
        $sheet->getCell("C$fila")->setValueExplicit($arboles[1]->monto, DataType::TYPE_NUMERIC);
        $sheet->getCell("F$fila")->setValueExplicit($arbolesPeriodoAnterior[1]->monto, DataType::TYPE_NUMERIC);

        $sheet->getStyle("B$fila:F$fila")->getFont()->setBold(true);
        $sheet->getStyle("B$fila:T$fila")->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFffbf00');

        # Patrimonio Neto
        $fila++;
        self::rellenarFlujoEfectivoAuxiliar($objPHPExcel, $model, $fila, $arboles[2], $arbolesPeriodoAnterior[2]);
        $sheet->getCell("B$fila")->setValue("Total Patrimonio Neto");
        $sheet->getCell("C$fila")->setValueExplicit($arboles[2]->monto, DataType::TYPE_NUMERIC);
        $sheet->getCell("F$fila")->setValueExplicit($arbolesPeriodoAnterior[2]->monto, DataType::TYPE_NUMERIC);

        $sheet->getStyle("B$fila:F$fila")->getFont()->setBold(true);
        $sheet->getStyle("B$fila:T$fila")->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFffbf00');

        # Pasivo + Patrimonio Neto
        $fila++;
        $sheet->getCell("B$fila")->setValue("Total Pasivo + Neto");
        $sheet->getCell("C$fila")->setValueExplicit($arboles[1]->monto + $arboles[2]->monto, DataType::TYPE_NUMERIC);
        $sheet->getCell("F$fila")->setValueExplicit($arbolesPeriodoAnterior[1]->monto + $arbolesPeriodoAnterior[2]->monto, DataType::TYPE_NUMERIC);

        $sheet->getStyle("B$fila:F$fila")->getFont()->setBold(true);
        $sheet->getStyle("B$fila:T$fila")->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFffbf00');

        # Ganancias y Perdidas
        // TODO: Ganancias y perdidas

        # Utilidad Liquida
        $fila++;
        // TODO: falta C, D, E y F.
        $sheet->getCell("B$fila")->setValueExplicit("Utilidad Líquida", DataType::TYPE_STRING);
        $sheet->getCell("G$fila")->setValueExplicit("=SUM(G$filaInicial:G" . ($fila - 1) . ")", DataType::TYPE_FORMULA);
        $sheet->getStyle("C$fila:F$fila")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_DOUBLE);
        $sheet->getStyle("B$fila:G$fila")->getFont()->setBold(true);

        # Sumatoria de conceptos
        $cols = ['H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'];
        foreach ($cols as $col) {
            #Arriba
            $sheet->getCell("{$col}5")->setValueExplicit("=SUM($col$filaInicial:$col" . ($fila - 1) . ")", DataType::TYPE_FORMULA);
            #Abajo
            $sheet->getCell("$col$fila")->setValueExplicit("=SUM($col$filaInicial:$col" . ($fila - 1) . ")", DataType::TYPE_FORMULA);
        }

        # Formatos generales
        $sheet->getStyle("A$filaInicial:U$fila")->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle("C$filaInicial:G$fila")->getNumberFormat()->setFormatCode('#,##0');
        $sheet->getStyle("H$filaInicial:T$fila")->getNumberFormat()->setFormatCode('#,##0');

        # Complete the sheet for flujo efectivo using the auxiliar sheet.
        $sheet = $objPHPExcel->getSheet(2);  # Hoja de flujo de efectivos.
        $sheetFrom = $objPHPExcel->getSheet(6);
        $filas = array_values(array_merge(range(13, 18), range(21, 23), range(26, 29)));
        foreach ($filas as $key => $_fila) {
            $sheet->getCell("H$_fila")->setValueExplicit($sheetFrom->getCell("{$cols[$key]}$fila")->getCalculatedValue(), DataType::TYPE_NUMERIC);
            $sheet->getStyle("H$_fila")->getNumberFormat()->setFormatCode('#,##0');
        }

        # Reset Monto to zero and...
        # ... Carry up `monto` normally.
        foreach ($arboles as $key => $nodo) {
            $nodo->resetMonto();
            $arbolesPeriodoAnterior[$key]->resetMonto();

            $nodo->carryUpMonto();
            $arbolesPeriodoAnterior[$key]->carryUpMonto();
        }
    }

    /**
     * @param Spreadsheet $objPHPExcel
     * @param EstadoFinanciero $model
     * @param int $fila
     * @param Nodo $nodo
     * @param Nodo $nodoPeriodoAnterior
     * @throws Exception
     */
    private function rellenarFlujoEfectivoAuxiliar(&$objPHPExcel, &$model, &$fila, $nodo, $nodoPeriodoAnterior, &$sumarDesde = false)
    {
        if (isset($nodo) && $nodo->imprimir) {
            $sheet = $objPHPExcel->getActiveSheet();

            #Columna B: Nombre de cuentas
            $sheet->getCell("B$fila")
                ->setValueExplicit($nodo->cuenta->nombre, DataType::TYPE_STRING);

            #Columna C, F, G: Montos.
            #Si la cuenta es root, no poner ningun monto.
            if ($nodo->hasPadre()) {
                $sheet->getCell("C$fila")
                    ->setValueExplicit($nodo->monto, DataType::TYPE_NUMERIC);
                $sheet->getCell("F$fila")
                    ->setValueExplicit($nodoPeriodoAnterior->monto, DataType::TYPE_NUMERIC);
                $sheet->getCell("G$fila")
                    ->setValueExplicit("=C$fila-F$fila", DataType::TYPE_FORMULA);
            }

            #Columnas H~T (amarillas): Mapear valores a las columnas amarillas.
            #Si el nodo no tiene hijos, son cuentas asentables.
            if (empty($nodo->hijos)) {
                if (!$sumarDesde) $sumarDesde = $fila;
                $sheet->getCell("{$model->getConceptMap()[$nodo->cuenta->flujo_efectivo]}$fila")->setValueExplicit("=G$fila*(-1)", DataType::TYPE_FORMULA);
            }

            #Columnas B~G: Pintar en gris.
            if ($nodo->cuenta->asentable == 'no') {
                $sheet->getStyle("B$fila:G$fila")->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFBABABA');
                $sheet->getStyle("B$fila:G$fila")->getFont()->setBold(true);
            }

            $fila++;

            $sumarDesdePrev = $sumarDesde;
            foreach ($nodo->hijos as $index => $hijo) {
                self::rellenarFlujoEfectivoAuxiliar($objPHPExcel, $model, $fila, $hijo, $nodoPeriodoAnterior->hijos[$index], $sumarDesde);
            }

            #Fila de subtotal: Total activo corriente/no-corriente, Total pasivo corriente/no-corriente, etc...
            if ($sumarDesdePrev == false && $sumarDesde != false && sizeof(explode('.', $nodo->cuenta->cod_completo)) == 2) {
                $sheet->getCell("B$fila")->setValueExplicit("Total {$nodo->cuenta->nombre}", DataType::TYPE_STRING);
                $sheet->getStyle("B$fila:G$fila")->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFBABABA');
                $sheet->getStyle("B$fila:G$fila")->getFont()->setBold(true);

                $fila++;
                $sumarDesde = false;
            }

        }
    }

    /**
     * @param Spreadsheet $objPHPExcel
     * @param EstadoFinanciero $model
     * @throws Exception
     */
    private function rellenarNotasContables(&$objPHPExcel, &$model)
    {
        $fields = ['datos_entidad', 'base_preparacion', 'politicas_contables', 'composicion_cuentas_patrimoniales',
            'composicion_cuentas_resultado', 'detalle_rentas', 'contingencias', 'identificacion_partes_vinculadas',
            'hechos_posteriores_relevantes', 'otras_notas'];
        $objPHPExcel->setActiveSheetIndex(4);

        $fila = 12;
        foreach ($fields as $field) {
            $text = $model->$field;
            $pattern = '/\(([neg|sub|dobs|inc|-]+)\)(.*?)\(\/\)/';
            preg_match_all($pattern, $text, $match);
            $texts_sin_format = preg_split($pattern, $text, null);
            $texts_con_format = $match[2];
            $texts_formats = $match[1];

            $i = 0;

            $objRichText = new RichText();
            while (true) {
                $flag = 0;

                if (array_key_exists($i, $texts_sin_format)) {
                    $objRichText->createText($texts_sin_format[$i]);
                    $flag++;
                }

                if (array_key_exists($i, $texts_con_format)) {
                    $objRichTextStyle = $objRichText->createTextRun($texts_con_format[$i]);
                    foreach ((explode('-', $texts_formats[$i])) as $format) {
                        if ($format == 'neg')
                            $objRichTextStyle->getFont()->setBold(true);
                        elseif ($format == 'sub')
                            $objRichTextStyle->getFont()->setUnderline(Font::UNDERLINE_SINGLE);
                        elseif ($format == 'dobs')
                            $objRichTextStyle->getFont()->setUnderline(Font::UNDERLINE_DOUBLE);
                        elseif ($format == 'inc')
                            $objRichTextStyle->getFont()->setItalic(true);
                    }
                    $objRichTextStyle->getFont()->setSize(8);
                    $flag++;
                }

                if ($flag == 0)
                    break;

                $i++;
            }

            $objPHPExcel->getActiveSheet()->getCell("B$fila")->setValue($objRichText);
            $fila++;
        }


    }

    /**
     * @param Spreadsheet $objPHPExcel
     * @throws Exception
     */
    private function rellenarCuadroRevaluo(&$objPHPExcel)
    {
        $empresa_id = self::getEmpresaActual();
        $periodo_id = self::getPeriodoActual();
        $activosFijos = ActivoFijo::find()->where(['empresa_id' => $empresa_id, 'empresa_periodo_contable_id' => $periodo_id]);

        if (!$activosFijos->exists()) return;

        $fila = 14;
        $anhoActual = date("Y");
        $mesActual = date("m");
        $objPHPExcel->setActiveSheetIndex(5);
        $criterioMesSigte = Empresa::CRITERIO_REVALUO_MES_SIGUIENTE;
        $criterioPerSigte = Empresa::CRITERIO_REVALUO_PERIODO_SIGUIENTE;
        $parametroSistema = ParametroSistema::find()->where(['nombre' => "core_empresa-{$empresa_id}-periodo-{$periodo_id}-criterio_revaluo"]);

        if (!$parametroSistema->exists())
            throw new Exception("Falta definir en el parametro de empresa el criterio de revaluo (Al mes siguiente/Al ejercicio siguiente).");

        $criterioReval = $parametroSistema->one()->valor;
        $coefReval = CoeficienteRevaluo::findOne(['periodo' => "{$mesActual}-{$anhoActual}"]);

        /** @var ActivoFijo $activoFijo */
        foreach ($activosFijos->all() as $activoFijo) {
            $tabla_revaluo = $activoFijo->getTablaDepreciacion();
            $anhoAdq = date('Y', strtotime($activoFijo->fecha_adquisicion));
            $anhoX = abs($anhoAdq - $anhoActual) + 1;
            $anhosResantes = max(0, (int)$anhoAdq + (int)$activoFijo->vida_util_fiscal - (int)$anhoActual);
            $coefRevalValue = 1;  # si anho de adq == anho actual y criterio es periodo siguiente, coef = 1.

            // Calculos auxiliares
            if ($anhoAdq == $anhoActual)
                if ($criterioReval == $criterioMesSigte) {
                    if (!isset($coefReval))
                        throw new Exception("Error en " . __FUNCTION__ . '(): No existe coeficiente de revalúo registrado para este mes del periodo actual.');
                    $mesAdq = date('m', strtotime($activoFijo->fecha_adquisicion));
                    $mesDep = date('m', strtotime('+1 month', strtotime("01-{$mesAdq}-{$anhoActual}")));
                    $coefRevalValue = $coefReval->getCoeficiente($mesDep, CoeficienteRevaluo::EXISTENCIA);
                } else {  # Se compro este mismo anho actual pero se configura para depreciar al periodo siguiente.
                    # Coeficiente = 1 para que no se revalue.
                    # Depreciaciones acumuladas a cero porque no se deprecia nada.
                    $tabla_revaluo[$anhoX]['deprec_acum_fiscal'] = 0;
                    $tabla_revaluo[$anhoX]['deprec_acum_contab'] = 0;
                }
            else {
                if (!isset($coefReval))
                    throw new Exception("Error en " . __FUNCTION__ . '(): No existe coeficiente de revalúo registrado para este mes del periodo actual.');
                $coefRevalValue = $coefReval->getCoeficiente('01', CoeficienteRevaluo::EXISTENCIA);
            }

            // Carga de datos.

            // Nombre y fecha de adquisicion
            #Merge A y B
            $objPHPExcel->getActiveSheet()->mergeCells("A$fila:B$fila");
            $objPHPExcel->getActiveSheet()->setCellValue("A{$fila}", $activoFijo->nombre);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit("C{$fila}", $activoFijo->fecha_adquisicion, DataType::TYPE_STRING2);

            // Valores numericos. Formulas.
            $revaluarMesSigte = "AND(\"$anhoActual\"=\"$anhoAdq\",\"$criterioReval\"=\"$criterioMesSigte\")";
            $revaluarPerSigte = "AND(\"$anhoActual\"<>\"$anhoAdq\",\"$criterioReval\"=\"$criterioPerSigte\")";
            $condicional = "OR($revaluarMesSigte,$revaluarPerSigte)";
            $objPHPExcel->getActiveSheet()->getCell("D$fila")->setValueExplicit($activoFijo->vida_util_fiscal, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCell("E$fila")->setValueExplicit($anhosResantes, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCell("F$fila")->setValueExplicit($activoFijo->costo_adquisicion, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCell("G$fila")->setValueExplicit($activoFijo->valor_fiscal_anterior, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCell("H$fila")->setValueExplicit($activoFijo->valor_contable_anterior, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCell("I$fila")->setValueExplicit($coefRevalValue, ExcelDataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCell("J$fila")->setValueExplicit("=G$fila*I$fila", ExcelDataType::TYPE_FORMULA);
            $objPHPExcel->getActiveSheet()->getCell("K$fila")->setValueExplicit("=H$fila*I$fila", ExcelDataType::TYPE_FORMULA);
            $objPHPExcel->getActiveSheet()->getCell("L$fila")->setValueExplicit("=IF($condicional,J$fila/(E$fila+1),0)", ExcelDataType::TYPE_FORMULA);
            $objPHPExcel->getActiveSheet()->getCell("M$fila")->setValueExplicit("=IF($condicional,K$fila/(E$fila+1)-L$fila,0)", ExcelDataType::TYPE_FORMULA);
            $objPHPExcel->getActiveSheet()->getCell("N$fila")->setValueExplicit((array_key_exists($anhoX, $tabla_revaluo)) ? $tabla_revaluo[$anhoX]['deprec_acum_fiscal'] : 1, ExcelDataType::TYPE_NUMERIC);  # Si no existe `$anhoX` en `$tabla_revaluo` significa que se han pasado mas anhos que los anhos de su vida util.
            $objPHPExcel->getActiveSheet()->getCell("O$fila")->setValueExplicit((array_key_exists($anhoX, $tabla_revaluo)) ? $tabla_revaluo[$anhoX]['deprec_acum_contab'] : 1, ExcelDataType::TYPE_NUMERIC);  # Si no existe `$anhoX` en `$tabla_revaluo` significa que se han pasado mas anhos que los anhos de su vida util.
            $objPHPExcel->getActiveSheet()->getCell("P$fila")->setValueExplicit("=J$fila-N$fila", ExcelDataType::TYPE_FORMULA);
            $objPHPExcel->getActiveSheet()->getCell("Q$fila")->setValueExplicit("=K$fila-L$fila-M$fila", ExcelDataType::TYPE_FORMULA);

            $fila++;
        }

        // Aplicar formatos.

        // Alineacion
        $objPHPExcel->getActiveSheet()->getStyle("A14:A" . ($fila - 1))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT); // filas de datos
        $objPHPExcel->getActiveSheet()->getStyle("C14:C" . ($fila - 1))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // filas de datos, columna de fecha
        $objPHPExcel->getActiveSheet()->getStyle("A$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER); // fila de totales

        // Formato de numeros
        $objPHPExcel->getActiveSheet()->getStyle("F14:Q$fila")->getNumberFormat()->setFormatCode('#,##0.00'); // todas las filas, incluyendo total
        $objPHPExcel->getActiveSheet()->getStyle("I14:I" . ($fila - 1))->getNumberFormat()->setFormatCode('#,##0.0000000'); // fila de datos

        // Tamanho de fuente
        $objPHPExcel->getActiveSheet()->getStyle("A14:Q$fila")->getFont()->setSize(12); // todas las filas

        // Peso de fuente
        $objPHPExcel->getActiveSheet()->getStyle("A$fila:Q$fila")->getFont()->setBold(true); // fila de totales
        $objPHPExcel->getActiveSheet()->getStyle("A14:A$fila")->getFont()->setBold(true); // columna de nombres
        $objPHPExcel->getActiveSheet()->getStyle("B14:Q" . ($fila - 1))->getFont()->setBold(false); // fila de datos

        // Bordes
        $objPHPExcel->getActiveSheet()->getStyle("A14:Q$fila")->getBorders()->getAllBorders()->setBorderStyle('thin');
        $objPHPExcel->getActiveSheet()->getStyle("A14:A$fila")->getBorders()->getLeft()->setBorderStyle('medium');
        $objPHPExcel->getActiveSheet()->getStyle("Q14:Q$fila")->getBorders()->getRight()->setBorderStyle('medium');
        $objPHPExcel->getActiveSheet()->getStyle("A$fila:Q$fila")->getBorders()->getBottom()->setBorderStyle('medium');

        // Texto `TOTALES`
        $objPHPExcel->getActiveSheet()->mergeCells("A$fila:B$fila");
        $objPHPExcel->getActiveSheet()->getCell("A$fila")->setValueExplicit("TOTALES", ExcelDataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getCell("A$fila")->getStyle()->getBorders()->getRight()->setBorderStyle(Border::BORDER_NONE);  // no funciona
        $objPHPExcel->getActiveSheet()->getStyle("A$fila:C$fila")->getBorders()->getRight()->setBorderStyle(Border::BORDER_NONE);  // tampoco funciona

        // Calcular total por columnas y
        foreach (['F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q'] as $column) {
            $objPHPExcel->getActiveSheet()->getCell("$column$fila")->setValueExplicit("=SUM({$column}14:$column" . ($fila - 1) . ")", ExcelDataType::TYPE_FORMULA);
            $objPHPExcel->getActiveSheet()->getStyle("$column$fila")->getNumberFormat()->setFormatCode('#,##0.00');
        }

        // alinear todos los nros a la derecha
        $objPHPExcel->getActiveSheet()->getStyle("D14:Q$fila")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);

    }

    /**
     * @param Spreadsheet $objPHPExcel
     * @param string $fileName
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws Exception
     */
    private function exportar(&$objPHPExcel, $fileName = '')
    {
        if ($fileName == '') {
            throw new Exception("Falta especificar un nombre al excel.");
        }

        $objPHPExcel->setActiveSheetIndex(0);
        $carpeta = Yii::$app->basePath . '/web/uploads/';
        if (!is_dir($carpeta)) {
            if (!@mkdir($carpeta, 0777, true)) {
                throw new Exception('No se puede crear carpeta.');
            }
        }
        Yii::$app->session->set('carpeta', $carpeta);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $objWriter->save($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");

        // Exportar en navegador
        $fi = pathinfo($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
        $OS = strtolower(empty($_SERVER['HTTP_USER_AGENT']) ? '' : $_SERVER['HTTP_USER_AGENT']);
        if (!$OS)
            $OS_IS_MAC = true;
        else
            $OS_IS_MAC = (strpos($OS, 'mac') !== false)
                || (strpos($OS, 'macintosh') !== false)
                || (strpos($OS, 'iphone') !== false)
                || (strpos($OS, 'ipad') !== false);

        if ($OS_IS_MAC) {
//            if(!file_exists($target_relative)){
//                copy($filename,$target_relative);
//                sleep(1);
//            }
//            header('Location: ' . $target);
        } else {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . "$fileName.{$fi['extension']}");
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx"));
            ob_clean();
            flush();
            readfile($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
        }

        //borramos el fichero temporal
        unlink($carpeta . "temporal_user_id" . Yii::$app->getUser()->identity->getId() . ".xlsx");
    }

    /**
     * @return bool
     */
    private function guardarExcel()
    {
        $destinationPath = Yii::$app->basePath . '/modules/contabilidad/files/';
        $file = $_FILES['EstadoFinanciero']['name']['archivo'];
        $path = pathinfo($file);
        $filename = $path['filename'];
        $ext = $path['extension'];
        $temp_name = $_FILES['EstadoFinanciero']['tmp_name']['archivo'];
        $path_filename_ext = $destinationPath . $filename . "." . $ext;

        return move_uploaded_file($temp_name, $path_filename_ext);
    }

    /**
     * @param EstadoFinanciero $model
     * @return bool
     */
    private function deleteExcel(&$model)
    {
        $destinationFolder = Yii::$app->basePath . '/modules/contabilidad/files/';
        $fileName = "$destinationFolder{$model->archivo->baseName}.{$model->archivo->extension}";
        return unlink($fileName);
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}
