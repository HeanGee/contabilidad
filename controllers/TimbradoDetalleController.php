<?php

namespace backend\modules\contabilidad\controllers;

use backend\modules\contabilidad\models\TimbradoDetalle;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\Response;

class TimbradoDetalleController extends \backend\controllers\BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAddDetalle($submit = false)
    {
        $model = new TimbradoDetalle();
        $session = Yii::$app->session;
        $skey = 'cont_detalletimbrado-provider';

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $dataProvider = $session->get($skey);
            $data = $dataProvider->allModels;
            $model->nro_actual = $model->nro_inicio;
            array_push($data, $model);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);

            $session->set($skey, $dataProvider);
        };

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $this->renderAjax('_form_modal', [
            'modelo' => $model,
        ]);
    }

    public function actionEditDetalle($index, $submit = false)
    {
        /** @var TimbradoDetalle $model */

        $session = Yii::$app->session;
        $skey = 'cont_detalletimbrado-provider';
        $dataProvider = $session->get($skey);
        $data = $dataProvider->allModels;
        $model = $data[$index];

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $submit == false) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->nro_actual == '') $model->nro_actual = $model->nro_inicio;
            $data[$index] = $model;
            if ($data[$index] != null) {
                if ($data[$index]->id != null)
                    $data[$index]->save();
            }

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => false,
            ]);

            Yii::$app->response->format = Response::FORMAT_JSON;
            $session->set($skey, $dataProvider);
        }

        return $this->renderAjax('_form_modal', [
            'modelo' => $model,
        ]);
    }

    /**
     * @param $indice
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteDetalle($index)
    {
        $session = Yii::$app->session;
        $skey = 'cont_detalletimbrado-provider';

        $dataProvider = $session->get($skey);
        $data = $dataProvider->allModels;

        if($data[$index]->id != null) {
            if(TimbradoDetalle::find()->where(['id' => $data[$index]->id])->exists())
                TimbradoDetalle::findOne(['id' => $data[$index]->id])->delete();
        }

        unset($data[$index]);
        $data = array_values($data);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => false,
        ]);

        $session->set($skey, $dataProvider);

        return true;
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return false;
    }
}
