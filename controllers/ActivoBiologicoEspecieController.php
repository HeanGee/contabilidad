<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use backend\modules\contabilidad\models\search\ActivoBiologicoEspecieSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ActivoBiologicoEspecieController implements the CRUD actions for ActivoBiologicoEspecie model.
 */
class ActivoBiologicoEspecieController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ActivoBiologicoEspecie models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ActivoBiologicoEspecieSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ActivoBiologicoEspecie model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ActivoBiologicoEspecie model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ActivoBiologicoEspecie();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            try {
                if (!$model->save()) {
                    throw new \Exception($model->getErrorSummaryAsString());
                }
                FlashMessageHelpsers::createSuccessMessage("Especie {$model->nombre} creado correctamente.");
                if ($model->guardar_cerrar == 'si')
                    return $this->redirect(['index']);

                $model = new ActivoBiologicoEspecie();
                $model->loadDefaultValues();
            } catch (\Exception $exception) {
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ActivoBiologicoEspecie model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ActivoBiologicoEspecie model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findModel($id);

            if (!$model->isDeleteable()) {
                throw new \Exception("Esta Especie fue utilizada por un Activo Biológico o por una clasificación.");
            }

            $model->delete();
            $transaction->commit();
            FlashMessageHelpsers::createSuccessMessage("Especie {$model->nombre} eliminada correctamente.");
        } catch (\Exception $exception) {
            $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ActivoBiologicoEspecie model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActivoBiologicoEspecie the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ActivoBiologicoEspecie::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}
