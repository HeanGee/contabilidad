<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\models\Empresa;
use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\Asiento;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\Recibo;
use backend\modules\contabilidad\models\TraspasoPeriodo;
use common\helpers\FlashMessageHelpsers;
use Exception;
use Yii;

class TraspasoPeriodoController extends BaseController
{
    public function actionTraspasar()
    {

        if (self::isPeriodoCerrado()) {
            FlashMessageHelpsers::createWarningMessage("Este periodo ya está cerrado.");
            return $this->redirect(['/']);
        }

        $periodoActual = EmpresaPeriodoContable::findOne(Yii::$app->session->get('core_empresa_actual_pc'));
        $empresaActual = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')]);
        $model = new TraspasoPeriodo($periodoActual->anho);
        $model->periodo_actual_id = Yii::$app->session->get('core_empresa_actual_pc');
        $model->setBalance(Yii::$app->session->get('core_empresa_actual'),
            Yii::$app->session->get('core_empresa_actual_pc'));
        if ($model->isBalanceSetted())
            $model->generarAsiento();
        else {
            throw new Exception("No existe todavía ningún asiento de la empresa actual.");
        }

        try {
            self::isPeriodoCerrable();
        } catch (Exception $exception) {
            FlashMessageHelpsers::createWarningMessage("No se puede ejecutar el traspaso del periodo: {$exception->getMessage()}");
            return $this->redirect(['/']);
        }

        try {
            if ($model->load(Yii::$app->request->post())) {
                if (!$model->validate())
                    throw new Exception("Error en el formulario: {$model->getErrorSummaryAsString()}");

                $transaction = Yii::$app->db->beginTransaction();

                if ($model->asiento_cierre == '1') {
                    $periodo = $periodoActual;

                    # Verificar si los activos fijos fueron revaluados y depreciados.
                    $activosF = ActivoFijo::findAll(['empresa_periodo_contable_id' => $periodoActual->id, 'empresa_id' => Yii::$app->session->get('core_empresa_actual')]);
                    foreach ($activosF as $activo) {
                        if ($activo->asiento_revaluo_id == '')
                            throw new Exception("El activo fijo `{$activo->nombre}` no se ha revaluado todavia. Se necesita que exista un asiento de revalúo, antes de ejecutar el traspaso de periodo.");
                        elseif ($activo->asiento_depreciacion_id == '')
                            throw new Exception("El activo fijo `{$activo->nombre}` no se ha depreciado todavia. Se necesita que exista un asiento de depreciación, antes de ejecutar el traspaso de periodo.");
                    }

                    $asientoCierreCabecera = new Asiento();
                    $asientoCierreCabecera->empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $asientoCierreCabecera->periodo_contable_id = $periodo->id;
                    $asientoCierreCabecera->modulo_origen = 'contabilidad';
                    $asientoCierreCabecera->creado = date('Y-m-d H:i:s');
                    $asientoCierreCabecera->fecha = date('Y-m-t');
                    $asientoCierreCabecera->usuario_id = Yii::$app->user->id;
                    $asientoCierreCabecera->concepto = "Asiento de cierre para " .
                        "`{$asientoCierreCabecera->empresa->razon_social} - {$asientoCierreCabecera->periodoContable->anho}`.";
                    $asientoCierreCabecera->monto_debe = $model->monto_debe;
                    $asientoCierreCabecera->monto_haber = $model->monto_haber;

                    if (!$asientoCierreCabecera->save()) {
                        throw new Exception("Error validando cabecera de asiento de cierre: 
                        {$asientoCierreCabecera->getErrorSummaryAsString()}");
                    }

                    $asientoCierreCabecera->refresh();
                    foreach ($model->asientos as $asiento) {
                        unset($asiento->id);
                        $_asiento = new AsientoDetalle();
                        $_asiento->setAttributes($asiento->attributes);
                        $_asiento->asiento_id = $asientoCierreCabecera->id;
                        $tmp = $_asiento->monto_debe;
                        $_asiento->monto_debe = $_asiento->monto_haber;
                        $_asiento->monto_haber = $tmp;
                        if (!$_asiento->save()) {
                            throw new Exception("Error validando detalles del asiento de apertura: 
                            {$_asiento->getErrorSummaryAsString()}");
                        }
                    }

                    $periodo->asiento_cierre_id = $asientoCierreCabecera->id;
                    if (!$periodo->save()) {
                        throw new Exception("Error interno actualzando objeto periodo contable: 
                    {$periodo->getErrorSummaryAsString()}");
                    }
                }

                if ($model->asiento_apertura == '1') {
                    if ($model->asiento_cierre == '0')
                        throw new Exception("No se puede crear asiento de reapertura sin haber creado antes un asiento de cierre del periodo actual.");

                    $siguientePeriodo = $model->getSiguientePeriodo();

                    if ($siguientePeriodo->asiento_apertura_id != '')
                        throw new Exception("Ya existe un asiento de reapertura para el siguiente periodo {$siguientePeriodo->anho} de esta empresa actual.");

                    # Verificar si los activos fijos fueron revaluados y depreciados.
                    # Traspasar al siguiente periodo contable.
                    if (!isset($activosF)) {  # Si no se ejecuto el cierre, puede que esta variable no este seteada.
                        $activosF = ActivoFijo::findAll(['empresa_periodo_contable_id' => $periodoActual->id, 'empresa_id' => Yii::$app->session->get('core_empresa_actual')]);
                    }
                    foreach ($activosF as $activo) {
                        if ($activo->asiento_revaluo_id == '')
                            throw new Exception("El activo fijo `{$activo->nombre}` no se ha revaluado todavia. Se necesita que exista un asiento de revalúo, antes de ejecutar el traspaso de periodo.");
                        elseif ($activo->asiento_depreciacion_id == '')
                            throw new Exception("El activo fijo `{$activo->nombre}` no se ha depreciado todavia. Se necesita que exista un asiento de depreciación, antes de ejecutar el traspaso de periodo.");

                        $attributes = $activo->attributes;  # es copia del array `->attributes`
                        unset($attributes['id']);
                        unset($attributes['asiento_id']);
                        unset($attributes['activo_fijo_id']);
                        unset($attributes['factura_venta_id']);
                        unset($attributes['factura_compra_id']);
                        unset($attributes['asiento_revaluo_id']);
                        unset($attributes['asiento_depreciacion_id']);
                        unset($attributes['valor_fiscal_revaluado']);
                        unset($attributes['valor_contable_revaluado']);
                        unset($attributes['valor_fiscal_depreciado']);
                        unset($attributes['valor_contable_depreciado']);
                        $activoTraspasado = new ActivoFijo();
                        $activoTraspasado->setAttributes($attributes);
                        $activoTraspasado->activo_fijo_id = $activo->id;
                        $activoTraspasado->valor_fiscal_neto = $activo->valor_fiscal_depreciado;
                        $activoTraspasado->valor_contable_neto = $activo->valor_contable_depreciado;
                        $activoTraspasado->vida_util_restante = (int)$activoTraspasado->vida_util_restante - 1;
                        $activoTraspasado->empresa_periodo_contable_id = $siguientePeriodo->id;
                        $activoTraspasado->observacion = "Traspasado del periodo anterior `{$periodoActual->anho}` de la empresa `{$empresaActual->razon_social}`.";

                        if (!$activoTraspasado->save())
                            throw new Exception("Error traspasando activo fijo `{$activo->nombre}`: {$activoTraspasado->getErrorSummaryAsString()}");
                        $activoTraspasado->refresh();
                    }

                    $asientoAperturaCabecera = new Asiento();
                    $asientoAperturaCabecera->empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $asientoAperturaCabecera->periodo_contable_id = $siguientePeriodo->id;
                    $asientoAperturaCabecera->modulo_origen = 'contabilidad';
                    $asientoAperturaCabecera->creado = date('Y-m-d H:i:s');
                    $asientoAperturaCabecera->fecha = date('Y-m-t');
                    $asientoAperturaCabecera->usuario_id = Yii::$app->user->id;
                    $asientoAperturaCabecera->concepto = "Asiento de reapertura para " .
                        "`{$asientoAperturaCabecera->empresa->razon_social} - {$asientoAperturaCabecera->periodoContable->anho}`.";
                    $asientoAperturaCabecera->monto_debe = $model->monto_debe;
                    $asientoAperturaCabecera->monto_haber = $model->monto_haber;

                    if (!$asientoAperturaCabecera->save()) {
                        throw new Exception("Error validando cabecera de asiento de apertura: 
                        {$asientoAperturaCabecera->getErrorSummaryAsString()}");
                    }
                    $asientoAperturaCabecera->refresh();

                    foreach ($model->asientos as $asiento) {
                        $asiento->asiento_id = $asientoAperturaCabecera->id;
                        if (!$asiento->save()) {
                            throw new Exception("Error validando detalles del asiento de cierre: 
                            {$asiento->getErrorSummaryAsString()}");
                        }
                    }

                    $siguientePeriodo->asiento_apertura_id = $asientoAperturaCabecera->id;
                    if (!$siguientePeriodo->save()) {
                        throw new Exception("Error actualizando periodo siguiente con el id de asiento de
                         apertura: {$siguientePeriodo->getErrorSummaryAsString()}");
                    }

                    // TODO: Traspaso de activos fijos y biologicos al siguiente periodo.
                }

                $transaction->commit();
                FlashMessageHelpsers::createSuccessMessage("Traspaso ejecutado correctamente.");
            }
        } catch (Exception $exception) {
            !isset($transaction) || $transaction->rollBack();
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }

        retorno:;
        return $this->render('traspasar', ['model' => $model]);
    }

    private function isPeriodoCerrado()
    {
        $periodo = EmpresaPeriodoContable::findOne(Yii::$app->session->get('core_empresa_actual_pc'));
        return ($periodo->asiento_cierre_id != ''/* && $periodo->asiento_apertura_id == ''*/);
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function isPeriodoCerrable()
    {
        $empresa_id = Yii::$app->session->get('core_empresa_actual');
        $periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

        if (!empty(Recibo::getReciboConExtra($empresa_id, $periodo_id)))
            throw new Exception("Hay recibos con extras.");


        return true;
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
    }
}