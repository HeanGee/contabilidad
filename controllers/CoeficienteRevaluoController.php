<?php

namespace backend\modules\contabilidad\controllers;

use backend\controllers\BaseController;
use backend\modules\contabilidad\models\CoeficienteRevaluo;
use backend\modules\contabilidad\models\search\CoeficienteRevaluoSearch;
use common\helpers\FlashMessageHelpsers;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * CoeficienteRevaluoController implements the CRUD actions for CoeficienteRevaluo model.
 * @method static getAcceso($string)
 */
class CoeficienteRevaluoController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ['verbs' => ['class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['POST'],
            ],
        ],
        ];
    }

    /**
     * Lists all CoeficienteRevaluo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoeficienteRevaluoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CoeficienteRevaluo model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', ['model' => $this->findModel($id),]);
    }

    /**
     * Creates a new CoeficienteRevaluo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $_pjax
     * @return mixed
     * @throws \Exception
     */
    public function actionCreate($_pjax = null)
    {
        $model = new CoeficienteRevaluo();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            try {

                if (!$model->validate()) {
                    throw new Exception('No se pudo validar el formulario');
                }

                $model->enero_existencia = $this->formatPostToFloat($model->enero_existencia);
                $model->enero_enajenacion = $this->formatPostToFloat($model->enero_enajenacion);
                $model->febrero_existencia = $this->formatPostToFloat($model->febrero_existencia);
                $model->febrero_enajenacion = $this->formatPostToFloat($model->febrero_enajenacion);
                $model->marzo_existencia = $this->formatPostToFloat($model->marzo_existencia);
                $model->marzo_enajenacion = $this->formatPostToFloat($model->marzo_enajenacion);
                $model->abril_existencia = $this->formatPostToFloat($model->abril_existencia);
                $model->abril_enajenacion = $this->formatPostToFloat($model->abril_enajenacion);
                $model->mayo_existencia = $this->formatPostToFloat($model->mayo_existencia);
                $model->mayo_enajenacion = $this->formatPostToFloat($model->mayo_enajenacion);
                $model->junio_existencia = $this->formatPostToFloat($model->junio_existencia);
                $model->junio_enajenacion = $this->formatPostToFloat($model->junio_enajenacion);
                $model->julio_existencia = $this->formatPostToFloat($model->julio_existencia);
                $model->julio_enajenacion = $this->formatPostToFloat($model->julio_enajenacion);
                $model->agosto_existencia = $this->formatPostToFloat($model->agosto_existencia);
                $model->agosto_enajenacion = $this->formatPostToFloat($model->agosto_enajenacion);
                $model->septiembre_existencia = $this->formatPostToFloat($model->septiembre_existencia);
                $model->septiembre_enajenacion = $this->formatPostToFloat($model->septiembre_enajenacion);
                $model->octubre_existencia = $this->formatPostToFloat($model->octubre_existencia);
                $model->octubre_enajenacion = $this->formatPostToFloat($model->octubre_enajenacion);
                $model->noviembre_existencia = $this->formatPostToFloat($model->noviembre_existencia);
                $model->noviembre_enajenacion = $this->formatPostToFloat($model->noviembre_enajenacion);
                $model->diciembre_existencia = $this->formatPostToFloat($model->diciembre_existencia);
                $model->diciembre_enajenacion = $this->formatPostToFloat($model->diciembre_enajenacion);

                if (!preg_match('/^\d\d-\d\d\d\d$/', trim($model->periodo))) {
                    throw new Exception('Formato de periodo incorrecto');
                }

                if (!$model->validate())
                    throw new \Exception('Error validando datos: ' . implode(', ', $model->getErrorSummary(true)));

                $model->save(false);
                FlashMessageHelpsers::createSuccessMessage('El registro se ha guardado exitosamente.');
                return $this->redirect(['index']);
            } catch (Exception $exception) {
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }
        return $this->render('create', ['model' => $model,]);
    }

    private function formatPostToFloat($number)
    {
        //Se convierte a una manera que maneje float
        if (strpos($number, '.') && strpos($number, ',')) {
            $number = str_replace(',', '.', str_replace('.', '', $number));
        } elseif (strpos($number, '.') && !strpos($number, ',')) {
            $number = str_replace('.', '', $number);
        } elseif (!strpos($number, '.') && strpos($number, ',')) {
            $number = str_replace(',', '.', $number);
        }
        return $number;
    }

    /**
     * Updates an existing CoeficienteRevaluo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            try {
                if (!$model->validate()) {
                    throw new Exception('No se pudo validar el formulario');
                }
                $model->enero_existencia = $this->formatPostToFloat($model->enero_existencia);
                $model->enero_enajenacion = $this->formatPostToFloat($model->enero_enajenacion);
                $model->febrero_existencia = $this->formatPostToFloat($model->febrero_existencia);
                $model->febrero_enajenacion = $this->formatPostToFloat($model->febrero_enajenacion);
                $model->marzo_existencia = $this->formatPostToFloat($model->marzo_existencia);
                $model->marzo_enajenacion = $this->formatPostToFloat($model->marzo_enajenacion);
                $model->abril_existencia = $this->formatPostToFloat($model->abril_existencia);
                $model->abril_enajenacion = $this->formatPostToFloat($model->abril_enajenacion);
                $model->mayo_existencia = $this->formatPostToFloat($model->mayo_existencia);
                $model->mayo_enajenacion = $this->formatPostToFloat($model->mayo_enajenacion);
                $model->junio_existencia = $this->formatPostToFloat($model->junio_existencia);
                $model->junio_enajenacion = $this->formatPostToFloat($model->junio_enajenacion);
                $model->julio_existencia = $this->formatPostToFloat($model->julio_existencia);
                $model->julio_enajenacion = $this->formatPostToFloat($model->julio_enajenacion);
                $model->agosto_existencia = $this->formatPostToFloat($model->agosto_existencia);
                $model->agosto_enajenacion = $this->formatPostToFloat($model->agosto_enajenacion);
                $model->septiembre_existencia = $this->formatPostToFloat($model->septiembre_existencia);
                $model->septiembre_enajenacion = $this->formatPostToFloat($model->septiembre_enajenacion);
                $model->octubre_existencia = $this->formatPostToFloat($model->octubre_existencia);
                $model->octubre_enajenacion = $this->formatPostToFloat($model->octubre_enajenacion);
                $model->noviembre_existencia = $this->formatPostToFloat($model->noviembre_existencia);
                $model->noviembre_enajenacion = $this->formatPostToFloat($model->noviembre_enajenacion);
                $model->diciembre_existencia = $this->formatPostToFloat($model->diciembre_existencia);
                $model->diciembre_enajenacion = $this->formatPostToFloat($model->diciembre_enajenacion);
                if (!preg_match('/^\d\d-\d\d\d\d$/', trim($model->periodo))) {
                    throw new Exception('Formato de periodo incorrecto');
                }
                $model->save(false);
                FlashMessageHelpsers::createSuccessMessage('El registro se ha modificado exitosamente.');
                return $this->redirect(['index']);
            } catch (Exception $exception) {
                FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            }
        }
        return $this->render('update', ['model' => $model,]);
    }

    /**
     * Deletes an existing CoeficienteRevaluo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            FlashMessageHelpsers::createSuccessMessage('Registro eliminado con éxito.');

        } catch (\Throwable $e) {
            FlashMessageHelpsers::createErrorMessage('No se pudo eliminar el registro');

        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoeficienteRevaluo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoeficienteRevaluo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoeficienteRevaluo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Metodo que debe ser implementado retornando la lista de operaciones que no necesitan empresa.
     *
     * En caso de que el controller no requiera de ningún control por empresa se debe retornar false.
     *
     * @return mixed
     */
    function getNoRequierenEmpresa()
    {
        return [];
        // TODO: Implement getNoRequierenEmpresa() method.
    }
}
