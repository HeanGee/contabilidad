<?php

namespace backend\modules\contabilidad;

use common\helpers\PermisosHelpers;
use common\interfaces\ModuleInterface;

/**
 * contabilidad module definition class
 */
class MainModule extends \yii\base\Module implements ModuleInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\contabilidad\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }


    /**
     * Retorna los ítems de menú del módulo.
     * @return mixed
     */
    public static function getMenuItems()
    {
        $items = array();
        $subitems_entidad = [];

//        if (PermisosHelpers::getAcceso('contabilidad-entidad-index')) {
//            array_push($subitems_entidad, ['label' => 'Clientes', 'url' => ['/contabilidad/entidad/index', "entidad" => 'Cliente'], 'icon' => 'fa fa-group', 'active' => \Yii::$app->controller->id == 'entidad' && \Yii::$app->getRequest()->getQueryParam('entidad') == 'Cliente']);
//        }
//        if (PermisosHelpers::getAcceso('contabilidad-entidad-index')) {
//            array_push($subitems_entidad, ['label' => 'Proveedores', 'url' => ['/contabilidad/entidad/index', "entidad" => 'Proveedor'], 'icon' => 'fa fa-group', 'active' => \Yii::$app->controller->id == 'entidad' && \Yii::$app->getRequest()->getQueryParam('entidad') == 'Proveedor']);
//        }
//        if (PermisosHelpers::getAcceso('contabilidad-entidad-index')) {
//            array_push($subitems_entidad, ['label' => 'Subir archivo', 'url' => ['/contabilidad/entidad/entidad-from-file'], 'icon' => 'fa fa-file', 'active' => \Yii::$app->controller->action->id == 'entidad-from-file']);
//        }
        if (PermisosHelpers::getAcceso('contabilidad-entidad-index')) {
            array_push($items, ['label' => 'Entidades', 'url' => ['/contabilidad/entidad/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'entidad']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-tipo-documento-index')) {
            array_push($items, ['label' => 'Tipos de Documentos', 'url' => ['/contabilidad/tipo-documento/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'tipo-documento']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-tipo-documento-set-index')) {
            array_push($items, ['label' => 'Tipos de Documentos SET', 'url' => ['/contabilidad/tipo-documento-set/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'tipo-documento-set']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-plan-cuenta-index')) {
            array_push($items, ['label' => 'Plan de Cuentas', 'url' => ['/contabilidad/plan-cuenta/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'plan-cuenta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-timbrado-index')) {
            array_push($items, ['label' => 'Timbrados', 'url' => ['/contabilidad/timbrado/index'], 'icon' => 'fa fa-barcode', 'active' => \Yii::$app->controller->id == 'timbrado']);
        }

        if (PermisosHelpers::getAcceso('contabilidad-iva-cuenta-index')) {
            array_push($items, ['label' => 'Iva Cuenta', 'url' => ['/contabilidad/iva-cuenta/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'iva-cuenta']);
        }

        $subitems_activo_fijo = [];
        if (PermisosHelpers::getAcceso('contabilidad-activo-fijo-index')) {
            array_push($subitems_activo_fijo, ['label' => 'Activo Fijo', 'url' => ['/contabilidad/activo-fijo/index'], 'icon' => 'fa fa-cubes', 'active' => \Yii::$app->controller->id == 'activo-fijo']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-index')) {
            array_push($subitems_activo_fijo, ['label' => 'Activo Fijo Tipos', 'url' => ['/contabilidad/activo-fijo-tipo/index'], 'icon' => 'fa fa-list-alt', 'active' => \Yii::$app->controller->id == 'activo-fijo-tipo']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-activo-fijo-from-file')) {
            array_push($subitems_activo_fijo, ['label' => 'Cargar desde Archivo', 'url' => ['/contabilidad/activo-fijo/from-file'], 'icon' => 'fa fa-file-text', 'active' => \Yii::$app->controller->id == 'activo-fijo' && \Yii::$app->controller->action->id == 'from-file']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-activo-fijo-revaluacion')) {
            array_push($subitems_activo_fijo, ['label' => 'Revalúo', 'url' => ['/contabilidad/activo-fijo/revaluacion', 'operacion' => 'revaluo'], 'icon' => 'fa fa-file-text', 'active' => \Yii::$app->controller->id == 'activo-fijo' && \Yii::$app->controller->action->id == 'revaluacion' && \Yii::$app->request->getQueryParam('operacion') == 'revaluo']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-activo-fijo-revaluacion')) {
            array_push($subitems_activo_fijo, ['label' => 'Depreciación', 'url' => ['/contabilidad/activo-fijo/revaluacion', 'operacion' => 'depreciacion'], 'icon' => 'fa fa-file-text', 'active' => \Yii::$app->controller->id == 'activo-fijo' && \Yii::$app->controller->action->id == 'revaluacion' && \Yii::$app->request->getQueryParam('operacion') == 'depreciacion']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-activo-fijo-index')) {
            array_push($items, ['label' => 'Activo Fijo', 'url' => '#', 'icon' => 'fa fa-cubes', 'items' => $subitems_activo_fijo,]);
        }

        $subitem_act_bio = [];
        if (PermisosHelpers::getAcceso('contabilidad-activo-biologico-index')) {
            array_push($subitem_act_bio, ['label' => 'Activo Biológico', 'url' => ['/contabilidad/activo-biologico/index'], 'icon' => 'fa fa-paw ', 'active' => \Yii::$app->controller->id == 'activo-biologico']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-index')) {
            array_push($subitem_act_bio, ['label' => 'Especies', 'url' => ['/contabilidad/activo-biologico-especie/index'], 'icon' => 'fa fa-object-group', 'active' => \Yii::$app->controller->id == 'activo-biologico-especie']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-clasificacion-index')) {
            array_push($subitem_act_bio, ['label' => 'Clasificaciones', 'url' => ['/contabilidad/activo-biologico-especie-clasificacion/index'], 'icon' => 'fa fa-list-alt', 'active' => \Yii::$app->controller->id == 'activo-biologico-especie-clasificacion']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-activo-biologico-cierre-inventario-index')) {
            array_push($subitem_act_bio, ['label' => 'Cierre de Inventario', 'url' => ['/contabilidad/activo-biologico-cierre-inventario/index'], 'icon' => 'fa fa-check', 'active' => \Yii::$app->controller->id == 'activo-biologico-cierre-inventario']);
        }

        $subitems_ventas = [];
        $subitems_retenciones_venta = [];
        $subitem_reporte_venta = [];
        if (PermisosHelpers::getAcceso('contabilidad-venta-index')) {
            array_push($subitems_ventas, ['label' => 'Facturas', 'url' => ['/contabilidad/venta/index'], 'icon' => 'fa fa-tags', 'active' => \Yii::$app->controller->id == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-plantilla-compraventa-index')) {
            array_push($subitems_ventas, ['label' => 'Plantillas', 'url' => ['/contabilidad/plantilla-compraventa/index', 'tipo' => 'venta'], 'icon' => 'fa fa-object-ungroup', 'active' => \Yii::$app->controller->id == 'plantilla-compraventa' && \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro')) {
            array_push($subitem_reporte_venta, ['label' => 'Reporte Por Factura', 'url' => ['/contabilidad/reporte/reporte-libro', 'operacion' => 'venta'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'reporte-libro' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro-por-cuentas')) {
            array_push($subitem_reporte_venta, ['label' => 'Reporte Por Cuenta', 'url' => ['/contabilidad/reporte/reporte-libro-por-cuentas', 'operacion' => 'venta'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'reporte-libro-por-cuentas' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro-por-entidad')) {
            array_push($subitem_reporte_venta, ['label' => 'Reporte Por Entidad', 'url' => ['/contabilidad/reporte/reporte-libro-por-entidad', 'operacion' => 'venta'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'reporte-libro-por-entidad' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro-por-mes')) {
            array_push($subitem_reporte_venta, ['label' => 'Reporte Por Mes', 'url' => ['/contabilidad/reporte/reporte-libro-por-mes', 'operacion' => 'venta'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'reporte-libro-por-mes' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-auditoria-venta')) {
            array_push($subitem_reporte_venta, ['label' => 'Auditoría de Ventas', 'url' => ['/contabilidad/reporte/auditoria-venta'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'auditoria-venta']);
        }
        if (sizeof($subitem_reporte_venta)) {
            array_push($subitems_ventas, ['label' => 'Reportes', 'url' => ['#'], 'icon' => 'fa fa-fa fa-line-chart', 'items' => $subitem_reporte_venta,]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-venta-hechauka-get-params')) {
            array_push($subitems_ventas, ['label' => 'Reporte Hechauka', 'url' => ['/contabilidad/reporte-venta-hechauka/get-params', 'operacion' => 'venta'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte-venta-hechauka' && \Yii::$app->controller->action->id == 'get-params' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-asiento-create-from-factura')) {
            array_push($subitems_ventas, ['label' => 'Generar asiento', 'url' => ['/contabilidad/asiento/create-from-factura', 'operacion' => 'venta'], 'icon' => 'fa fa-pencil', 'active' => (\Yii::$app->controller->action->id == "create-from-factura" && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta')]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-recibo-index')) {
            array_push($subitems_ventas, ['label' => 'Recibos', 'url' => ['/contabilidad/recibo/index', 'operacion' => 'venta'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'recibo' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
        }
//        if (PermisosHelpers::getAcceso('retencion-index')) {
//            array_push($subitems_ventas, ['label' => 'Retenciones', 'url' => ['/contabilidad/retencion/index', 'operacion' => 'venta'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'retencion' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
//        }
        if (PermisosHelpers::getAcceso('contabilidad-retencion-index')) {
            array_push($subitems_retenciones_venta, ['label' => 'Retenciones', 'url' => ['/contabilidad/retencion/index', 'operacion' => 'venta'], 'icon' => 'fa fa-anchor', 'active' => \Yii::$app->controller->id == 'retencion' && (\Yii::$app->controller->action->id == 'index' || \Yii::$app->controller->action->id == 'update' || \Yii::$app->controller->action->id == 'view' || \Yii::$app->controller->action->id == 'create') && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-retencion-create-file')) {
            array_push($subitems_retenciones_venta, ['label' => 'Desde Archivo', 'url' => ['/contabilidad/retencion/create-file', 'operacion' => 'venta'], 'icon' => 'fa fa-file-text', 'active' => \Yii::$app->controller->id == 'retencion' && \Yii::$app->controller->action->id == 'create-file' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'venta']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-venta-nota-index')) {
            array_push($subitems_ventas, ['label' => 'Notas', 'url' => ['/contabilidad/venta-nota/index'], 'icon' => 'fa fa-sticky-note-o', 'active' => \Yii::$app->controller->id == 'venta-nota']);
        }


//        if (PermisosHelpers::getAcceso('contabilidad-cuota-venta-index')) {
//            array_push($subitems_ventas, ['label' => 'Cuotas', 'url' => ['/contabilidad/cuota-venta/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'cuota-venta']);
//        }

        $subitems_compras = [];
        $subitems_retenciones_compra = [];
        $subitem_poliza = [];
        $subitem_prestamo_compra = [];
        $subitem_reporte_compra = [];
        if (PermisosHelpers::getAcceso('contabilidad-compra-index')) {
            array_push($subitems_compras, ['label' => 'Facturas', 'url' => ['/contabilidad/compra/index'], 'icon' => 'fa fa-shopping-cart', 'active' => \Yii::$app->controller->id == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-importacion-index')) {
            array_push($subitems_compras, ['label' => 'Importaciones', 'url' => ['/contabilidad/importacion/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'importacion']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-plantilla-compraventa-index')) {
            array_push($subitems_compras, ['label' => 'Plantillas', 'url' => ['/contabilidad/plantilla-compraventa/index', 'tipo' => 'compra'], 'icon' => 'fa fa-object-ungroup', 'active' => \Yii::$app->controller->id == 'plantilla-compraventa' && \Yii::$app->getRequest()->getQueryParam('tipo') == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro')) {
            array_push($subitem_reporte_compra, ['label' => 'Reporte por Factura', 'url' => ['/contabilidad/reporte/reporte-libro', 'operacion' => 'compra'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'reporte-libro' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro-por-cuentas')) {
            array_push($subitem_reporte_compra, ['label' => 'Reporte Por Cuenta', 'url' => ['/contabilidad/reporte/reporte-libro-por-cuentas', 'operacion' => 'compra'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'reporte-libro-por-cuentas' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro-por-entidad')) {
            array_push($subitem_reporte_compra, ['label' => 'Reporte Por Entidad', 'url' => ['/contabilidad/reporte/reporte-libro-por-entidad', 'operacion' => 'compra'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'reporte-libro-por-entidad' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro-por-mes')) {
            array_push($subitem_reporte_compra, ['label' => 'Reporte Por Mes', 'url' => ['/contabilidad/reporte/reporte-libro-por-mes', 'operacion' => 'compra'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'reporte-libro-por-mes' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra']);
        }
        if (sizeof($subitem_reporte_compra))
            array_push($subitems_compras, ['label' => 'Reportes', 'url' => ['#'], 'icon' => 'fa fa-fa fa-line-chart', 'items' => $subitem_reporte_compra,]);
        if (PermisosHelpers::getAcceso('contabilidad-reporte-compra-hechauka-get-params')) {
            array_push($subitems_compras, ['label' => 'Reporte Hechauka', 'url' => ['/contabilidad/reporte-compra-hechauka/get-params', 'operacion' => 'compra'], 'icon' => 'fa fa-bar-chart', 'active' => \Yii::$app->controller->id == 'reporte-compra-hechauka' && \Yii::$app->controller->action->id == 'get-params' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-asiento-create-from-factura')) {
            array_push($subitems_compras, ['label' => 'Generar asiento', 'url' => ['/contabilidad/asiento/create-from-factura', 'operacion' => 'compra'], 'icon' => 'fa fa-pencil', 'active' => (\Yii::$app->controller->action->id == "create-from-factura" && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra')]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-recibo-index')) {
            array_push($subitems_compras, ['label' => 'Recibos', 'url' => ['/contabilidad/recibo/index', 'operacion' => 'compra'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'recibo' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-poliza-index')) {
            array_push($subitem_poliza, ['label' => 'Pólizas', 'url' => ['/contabilidad/poliza/index'], 'icon' => 'fa fa-shield', 'active' => \Yii::$app->controller->id == 'poliza']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-poliza-seccion-index')) {
            array_push($subitem_poliza, ['label' => 'Secciones', 'url' => ['/contabilidad/poliza-seccion/index'], 'icon' => 'fa fa-list-alt', 'active' => \Yii::$app->controller->id == 'poliza-seccion']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-prestamo-asiento-generar-asiento-cuota')) {
            array_push($subitem_prestamo_compra, ['label' => 'Asientos', 'url' => ['/contabilidad/prestamo-asiento/generar-asiento-cuota'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'prestamo-asiento']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-prestamo-index')) {
            array_push($subitem_prestamo_compra, ['label' => 'Préstamos', 'url' => ['/contabilidad/prestamo'], 'icon' => 'fa fa-dollar', 'active' => \Yii::$app->controller->id == 'prestamo']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-index')) {
            array_push($subitem_prestamo_compra, ['label' => 'Recibos', 'url' => ['/contabilidad/recibo-prestamo'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'recibo-prestamo']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-retencion-index')) {
            array_push($subitems_retenciones_compra, ['label' => 'Retenciones', 'url' => ['/contabilidad/retencion/index', 'operacion' => 'compra'], 'icon' => 'fa fa-anchor', 'active' => \Yii::$app->controller->id == 'retencion' && (\Yii::$app->controller->action->id == 'index' || \Yii::$app->controller->action->id == 'update' || \Yii::$app->controller->action->id == 'view' || \Yii::$app->controller->action->id == 'create') && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-retencion-create-file')) {
            array_push($subitems_retenciones_compra, ['label' => 'Desde Archivo', 'url' => ['/contabilidad/retencion/create-file', 'operacion' => 'compra'], 'icon' => 'fa fa-file-text', 'active' => \Yii::$app->controller->id == 'retencion' && \Yii::$app->controller->action->id == 'create-file' && \Yii::$app->getRequest()->getQueryParam('operacion') == 'compra']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-compra-nota-index')) {
            array_push($subitems_compras, ['label' => 'Notas', 'url' => ['/contabilidad/compra-nota/index'], 'icon' => 'fa fa-sticky-note-o', 'active' => \Yii::$app->controller->id == 'compra-nota']);
        }

        if (PermisosHelpers::getAcceso('contabilidad-retencion-index')) {
            array_push($subitems_ventas, ['label' => 'Retenciones', 'url' => ['#'], 'icon' => 'fa fa-anchor', 'items' => $subitems_retenciones_venta,]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-poliza-index')) {
            array_push($subitems_compras, ['label' => 'Préstamos', 'url' => ['#'], 'icon' => 'fa fa-dollar', 'items' => $subitem_prestamo_compra,]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-retencion-index')) {
            array_push($subitems_compras, ['label' => 'Retenciones', 'url' => ['#'], 'icon' => 'fa fa-anchor', 'items' => $subitems_retenciones_compra,]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-poliza-index')) {
            array_push($subitems_compras, ['label' => 'Pólizas', 'url' => ['#'], 'icon' => 'fa fa-shield', 'items' => $subitem_poliza,]);
        }

        if (PermisosHelpers::getAcceso('contabilidad-activo-biologico-index')) {
            array_push($items, ['label' => 'Activo Biológico', 'url' => ['#'], 'icon' => 'fa fa-paw', 'items' => $subitem_act_bio,]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-venta-index')) {
            array_push($items, ['label' => 'Ventas', 'url' => ['#'], 'icon' => 'fa fa-tags', 'items' => $subitems_ventas,]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-compra-index')) {
            array_push($items, ['label' => 'Compras', 'url' => ['#'], 'icon' => 'fa fa-shopping-cart', 'items' => $subitems_compras,]);
        }
        if (PermisosHelpers::getAcceso('contabilidad-asiento-index')) {
            array_push($items, ['label' => 'Asientos', 'url' => ['/contabilidad/asiento/index'], 'icon' => 'fa fa-pencil-square-o', 'active' => \Yii::$app->controller->id == 'asiento' && \Yii::$app->controller->action->id != 'create-from-factura' && \Yii::$app->controller->action->id != 'create-from-rrhh']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-asiento-create-from-rrhh')) {
            array_push($items, ['label' => 'Asiento de sueldos', 'url' => ['/contabilidad/asiento/create-from-rrhh'], 'icon' => 'fa fa-pencil-square-o', 'active' => \Yii::$app->controller->id == 'asiento' && \Yii::$app->controller->action->id == 'create-from-rrhh']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-presentacion-iva-index')) {
            array_push($items, ['label' => 'Presentación Iva', 'url' => ['/contabilidad/presentacion-iva/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'presentacion-iva']);
        }

        $subitems_reportes = [];
        //Reportes
        if (PermisosHelpers::getAcceso('contabilidad-reporte-balance')) {
            array_push($subitems_reportes, ['label' => 'Balance Impositivo', 'url' => ['/contabilidad/reporte/balance'], 'icon' => 'fa fa-line-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'balance']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-balance-sumas-y-saldos')) {
            array_push($subitems_reportes, ['label' => 'Balance Sumas y Saldos', 'url' => ['/contabilidad/balance/sumas-y-saldos'], 'icon' => 'fa fa-line-chart', 'active' => \Yii::$app->controller->action->id == 'sumas-y-saldos']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-estado-cuenta-proveedor')) {
            array_push($subitems_reportes, ['label' => 'Estado Cuenta proveedores', 'url' => ['/contabilidad/reporte/estado-cuenta-proveedor'], 'icon' => 'fa fa-line-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'estado-cuenta-proveedor']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-estado-cuenta-cliente')) {
            array_push($subitems_reportes, ['label' => 'Estado Cuenta clientes', 'url' => ['/contabilidad/reporte/estado-cuenta-cliente'], 'icon' => 'fa fa-line-chart', 'active' => \Yii::$app->controller->id == 'reporte' && \Yii::$app->controller->action->id == 'estado-cuenta-cliente']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-estado-financiero')) {
            array_push($subitems_reportes, ['label' => 'Estados Financieros', 'url' => ['/contabilidad/estado-financiero/generar'], 'icon' => 'fa fa-line-chart', 'active' => \Yii::$app->controller->id == 'estado-financiero']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-inventario-reporte')) {
            array_push($subitems_reportes, ['label' => 'Inventario', 'url' => ['/contabilidad/inventario/reporte'], 'icon' => 'fa fa-line-chart', 'active' => \Yii::$app->controller->id == 'inventario' && \Yii::$app->controller->action->id == 'reporte']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-reporte-diario-get-params')) {
            array_push($subitems_reportes, ['label' => 'Libro Diario', 'url' => ['/contabilidad/reporte-diario/get-params'], 'icon' => 'fa fa-line-chart', 'active' => \Yii::$app->controller->id == 'reporte-diario' && \Yii::$app->controller->action->id == 'get-params']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-libromayor-reporte-libro')) {
            array_push($subitems_reportes, ['label' => 'Libro Mayor', 'url' => ['/contabilidad/libromayor/reporte-libro'], 'icon' => 'fa fa-line-chart', 'active' => \Yii::$app->controller->id == 'libromayor']);
        }
        array_push($items, ['label' => 'Reportes', 'url' => ['#'], 'icon' => 'fa fa-line-chart', 'items' => $subitems_reportes,]);

//        if (PermisosHelpers::getAcceso('contabilidad-recibo-index')) {
//            array_push($items, ['label' => 'Recibos', 'url' => ['/contabilidad/recibo/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'recibo']);
//        }

        $subitems_parametros = [];
        $subitems_obligaciones = [];
        if (PermisosHelpers::getAcceso('contabilidad-obligacion-index'))
            array_push($subitems_obligaciones, ['label' => 'Obligaciones', 'url' => ['/contabilidad/obligacion/index'], 'icon' => 'fa fa-gavel', 'active' => \Yii::$app->controller->id == 'obligacion']);
        if (PermisosHelpers::getAcceso('contabilidad-sub-obligacion-index'))
            array_push($subitems_obligaciones, ['label' => 'Sub Obligaciones', 'url' => ['/contabilidad/sub-obligacion/index'], 'icon' => 'fa fa-list-ul', 'active' => \Yii::$app->controller->id == 'sub-obligacion']);
        if (!empty($subitems_obligaciones))
            array_push($subitems_parametros, ['label' => 'Obligaciones', 'url' => ['#'], 'icon' => 'fa fa-gavel', 'items' => $subitems_obligaciones]);

        if (PermisosHelpers::getAcceso('contabilidad-rubro-index')) {
            array_push($subitems_parametros, ['label' => 'Rubros', 'url' => ['/contabilidad/rubro/index'], 'icon' => 'fa fa-sitemap', 'active' => \Yii::$app->controller->id == 'rubro']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-parametro-sistema-index')) {
            array_push($subitems_parametros, ['label' => 'Parámetros', 'url' => ['/contabilidad/parametro-sistema/index'], 'icon' => 'fa fa-cogs', 'active' => \Yii::$app->controller->id == 'parametro-sistema' && \Yii::$app->controller->module->id == 'contabilidad']);
        }

        if (PermisosHelpers::getAcceso('contabilidad-obligacion-index')) {
            array_push($items, ['label' => 'Parámetros', 'url' => ['#'], 'icon' => 'fa fa-cogs', 'items' => $subitems_parametros,]);
        }

        if (PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-index')) {
            array_push($items, ['label' => 'Coeficiente Revaluo', 'url' => ['/contabilidad/coeficiente-revaluo/index'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'coeficiente-revaluo']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-aranduka-parse-to-json')) {
            array_push($items, ['label' => 'Aranduka', 'url' => ['/contabilidad/aranduka/parse-to-json'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'aranduka']);
        }
        if (PermisosHelpers::getAcceso('contabilidad-traspaso-periodo-traspasar')) {
            array_push($items, ['label' => 'Traspaso de Periodo', 'url' => ['/contabilidad/traspaso-periodo/traspasar'], 'icon' => 'fa fa-building', 'active' => \Yii::$app->controller->id == 'traspaso-periodo']);
        }

        return $items;
    }

    /**
     * Retorna la lista de variables de sesión del modulo, dependientes de la empresa actual seleccionada.
     * @return mixed
     */
    public static function getSessionVariablesDepEmpresa()
    {
        $items = array();

        return $items;
    }

    public static function getSessionVariables()
    {
        return [
            'cont_cuotas_venta-provider',
            'cont_factura_venta_nro_actual',
            'cont_detalleventa_sim-provider',
            'cont_factura_total',
            'cont_detalleventa-provider',
            'cont_detalleventa_costo_sim-provider',
            'cont_factura_venta_nro_actual',
            'cont_monto_totales',
            'cont_detallecompra_sim-provider',
            'cont_monto_totales_compra',
            'cont_detallecompra-provider',
            'cont_compra_valores_usados'
        ];
    }
}
