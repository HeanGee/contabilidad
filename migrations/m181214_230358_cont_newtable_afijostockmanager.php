<?php

use jamband\schemadump\Migration;

class m181214_230358_cont_newtable_afijostockmanager extends Migration
{

    public function safeUp()
    {
        $t = '{{%cont_activo_fijo_stock_manager}}';
        $fk = 'fk_afijo_stockman_';
        $afijo = '{{%cont_activo_fijo}}';
        $venta = '{{%cont_factura_venta}}';
        $compra = '{{%cont_factura_compra}}';
        $empresa = '{{%core_empresa}}';
        $periodo = '{{%cont_empresa_periodo_contable}}';

        try {
            if ($this->db->getTableSchema($t, true) != null) {
                echo "    >>> La tabla {$t} existe por lo tanto se borrará primero:\n";
                $this->dropTable($t);
                echo "    >>> La tabla {$t} se ha borrado exitosamente.\n\n";
            }

            $this->createTable($t, [
                'id' => $this->primaryKey()->unsigned(),
                'activo_fijo_id' => $this->integer(10)->unsigned()->notNull(),
                'factura_compra_id' => $this->integer(11)->null(),
                'compra_nota_credito_id' => $this->integer(11)->null(),
                'factura_venta_id' => $this->integer(11)->null(),
                'venta_nota_credito_id' => $this->integer(11)->null(),
                'empresa_id' => $this->integer(10)->unsigned()->notNull(),
                'periodo_contable_id' => $this->integer(11)->notNull(),
            ], $this->tableOptions);

            $this->addForeignKey("{$fk}empresa_id", "{$t}", "empresa_id", "{$empresa}", "id");
            $this->addForeignKey("{$fk}periodo_id", "{$t}", "periodo_contable_id", "{$periodo}", "id");
            $this->addForeignKey("{$fk}afijo_id", "{$t}", "activo_fijo_id", "{$afijo}", "id");
            $this->addForeignKey("{$fk}venta_id", "{$t}", "factura_venta_id", "{$venta}", "id");
            $this->addForeignKey("{$fk}compra_id", "{$t}", "factura_compra_id", "{$compra}", "id");
        } catch (Exception $exception) {
            echo self::className() . " falla en la migración: {$exception->getMessage()}.\n";
            return false;
        }
        return true;
    }

    public function safeDown()
    {
        $t = '{{%cont_activo_fijo_stock_manager}}';
        try {
            if ($this->db->getTableSchema($t, true) != null) {
                $this->dropTable($t);
            }
            echo self::className() . ' se ha revertido.\n';
        } catch (Exception $exception) {
            echo self::className() . " no puede ser revertido por la siguiente razón: {$exception->getMessage()}.\n";
            return false;
        }
        return true;
    }
}
