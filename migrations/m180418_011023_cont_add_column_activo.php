<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180418_011023_cont_add_column_activo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_empresa_periodo_contable}}', 'activo', 'enum(\'Si\', \'No\')');
    }

    public function safeDown()
    {
        echo "m180418_011023_cont_add_column_activo no puede revertirse.\n";
        return false;
    }
}
