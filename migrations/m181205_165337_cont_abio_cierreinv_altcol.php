<?php

use jamband\schemadump\Migration;

class m181205_165337_cont_abio_cierreinv_altcol extends Migration
{
    public function safeUp()
    {
        $tableName = '{{%cont_activo_biologico_cierre_inventario}}';
        $this->alterColumn($tableName, 'procreo_cantidad', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'compra_cantidad', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'venta_cantidad', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'venta_cantidad_gd', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'venta_cantidad_gnd', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'mortandad_cantidad', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'mortandad_cantidad_gd', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'mortandad_cantidad_gnd', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'consumo_cantidad', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'consumo_cantidad_gd', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'consumo_cantidad_gnd', $this->integer(17)->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'stock_final', $this->integer(17)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
