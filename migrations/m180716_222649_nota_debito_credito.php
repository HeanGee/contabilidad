<?php

use jamband\schemadump\Migration;

class m180716_222649_nota_debito_credito extends Migration
{
    public function safeUp()
    {
        // cont_factura_venta
        $this->addColumn('{{%cont_factura_venta}}', 'tipo', "ENUM ('factura', 'nota_credito', 'nota_debito') NOT NULL DEFAULT 'factura'");
        $this->addColumn('{{%cont_factura_venta}}', 'factura_venta_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_factura_venta_factura_venta_id', '{{%cont_factura_venta}}', 'factura_venta_id', '{{%cont_factura_venta}}', 'id');

        // cont_factura_compra
        $this->addColumn('{{%cont_factura_compra}}', 'tipo', "ENUM ('factura', 'nota_credito', 'nota_debito') NOT NULL DEFAULT 'factura'");
        $this->addColumn('{{%cont_factura_compra}}', 'factura_compra_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_factura_compra_factura_compra_id', '{{%cont_factura_compra}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');

        // Parámetros base
        $this->insert('{{%cont_parametro_sistema}}', ['nombre' => 'nota_credito_set_id', 'valor' => '3']);
        $this->insert('{{%cont_parametro_sistema}}', ['nombre' => 'nota_debito_set_id', 'valor' => '2']);
    }

    public function safeDown()
    {
        echo "m180716_222649_nota_debito_credito no puede ser revertido.\n";
        return false;
    }
}
