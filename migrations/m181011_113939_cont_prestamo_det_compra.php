<?php

use jamband\schemadump\Migration;

class m181011_113939_cont_prestamo_det_compra extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_prestamo_detalle_compra}}', [
            'id' => $this->primaryKey()->unsigned(),
            'factura_compra_id' => $this->integer(11)->null(),
            'prestamo_detalle_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_prestamo_compra_compraid', '{{%cont_prestamo_detalle_compra}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
        $this->addForeignKey('fk_prestamo_compra_prestamoid', '{{%cont_prestamo_detalle_compra}}', 'prestamo_detalle_id', '{{%cont_prestamo_detalle}}', 'id');
        $this->addForeignKey('fk_prestamo_compra_periodoid', '{{%cont_prestamo_detalle_compra}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_prestamo_compra_empresaid', '{{%cont_prestamo_detalle_compra}}', 'empresa_id', '{{%core_empresa}}', 'id');

        $this->dropForeignKey('fk_prestamo_compra_id', '{{%cont_prestamo}}');
        $this->dropColumn('{{%cont_prestamo}}', 'factura_compra_id');

        $this->dropForeignKey('fk_prestamoDetalle_compra', '{{%cont_prestamo_detalle}}');
        $this->dropColumn('{{%cont_prestamo_detalle}}', 'factura_compra_id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\b';
        return false;
    }
}
