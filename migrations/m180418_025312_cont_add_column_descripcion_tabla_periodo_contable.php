<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180418_025312_cont_add_column_descripcion_tabla_periodo_contable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_empresa_periodo_contable}}', 'descripcion', $this->string(255));
    }

    public function safeDown()
    {
        echo "m180418_025312_cont_add_column_descripcion_tabla_periodo_contable no puede ser revertido.\n";
        return false;
    }
}
