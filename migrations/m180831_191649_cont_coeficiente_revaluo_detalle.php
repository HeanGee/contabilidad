<?php

use jamband\schemadump\Migration;

class m180831_191649_cont_coeficiente_revaluo_detalle extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_coeficiente_revaluo_detalle}}', ['id' => $this->primaryKey()->unsigned(), 'id_cabecera' => $this->integer(11)->unsigned()->notNull(), 'tipo' => $this->string(45)->notNull(), 'coeficiente' => $this->integer(11)->unsigned()->notNull(),], $this->tableOptions);

        // fk: cont_coeficiente_revaluo
        $this->addForeignKey('fk_cont_coeficiente_revaluo_id', '{{%cont_coeficiente_revaluo_detalle}}', 'id_cabecera', '{{%cont_coeficiente_revaluo}}', 'id');

        // fk: cont_coeficiente_revaluo_detalle
        //$this->addForeignKey('fk_cont_coeficiente_revaluo_id', '{{%cont_coeficiente_revaluo_detalle}}', 'timbrado_id', '{{%cont_timbrado}}', 'id');

    }

    public function safeDown()
    {
        echo "m180520_001425_cont_coeficiente_revaluo_detalle no puede ser revertido.\n";
        return false;
    }
}
