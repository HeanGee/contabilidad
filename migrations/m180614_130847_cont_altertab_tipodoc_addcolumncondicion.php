<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180614_130847_cont_altertab_tipodoc_addcolumncondicion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_tipo_documento}}', 'condicion', "ENUM('contado','credito') DEFAULT NULL DEFAULT 'contado'");
    }

    public function safeDown()
    {
        echo "m180614_130847_cont_altertab_tipodoc_addcolumncondicion no puede ser revertido.\n";
        return false;
    }
}
