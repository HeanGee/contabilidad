<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180522_132100_cont_plantilla_compraventa extends Migration
{
    public function safeUp()
    {
        // cont_plantilla_compraventa
        $this->createTable('{{%cont_plantilla_compraventa}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull(),
            'tipo' => "ENUM ('compra', 'venta') NOT NULL",
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL",
        ], $this->tableOptions);

        // cont_plantilla_compraventa_detalle
        $this->createTable('{{%cont_plantilla_compraventa_detalle}}', [
            'id' => $this->primaryKey()->unsigned(),
            'plantilla_id' => $this->integer(10)->unsigned()->notNull(),
            'iva_cta_id' => $this->integer(11)->null(),
            'p_c_gravada_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        // fk: cont_plantilla_compraventa_detalle
        $this->addForeignKey('fk_cont_plantilla_compraventa_detalle_iva_cta_id', '{{%cont_plantilla_compraventa_detalle}}', 'iva_cta_id', '{{%cont_iva_cuenta}}', 'id');
        $this->addForeignKey('fk_cont_plantilla_compraventa_detalle_p_c_gravada_id', '{{%cont_plantilla_compraventa_detalle}}', 'p_c_gravada_id', '{{%cont_plan_cuenta}}', 'id');
        $this->addForeignKey('fk_cont_plantilla_compraventa_detalle_plantilla_id', '{{%cont_plantilla_compraventa_detalle}}', 'plantilla_id', '{{%cont_plantilla_compraventa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180522_132100_cont_plantilla_compraventa no puede ser revertido.\n";
        return false;
    }
}
