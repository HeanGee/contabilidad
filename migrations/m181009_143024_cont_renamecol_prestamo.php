<?php

use jamband\schemadump\Migration;

class m181009_143024_cont_renamecol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%cont_prestamo}}', 'capital', 'monto_operacion');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\b';
        return false;
    }
}
