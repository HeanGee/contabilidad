<?php

use jamband\schemadump\Migration;

class m180723_181047_cont_tabla_retencion_factor extends Migration
{
    public function safeUp()
    {
        // cont_retencion_detalle_base
        $this->createTable('{{%cont_retencion_detalle_ivas}}', [
            'id' => $this->primaryKey()->unsigned(),
            'retencion_id' => $this->integer(10)->unsigned()->notNull(),
            'base' => $this->decimal(14, 2)->null()->defaultValue('0.00'),
            'factor' => $this->decimal(14, 2)->null()->defaultValue('0.00'),
            'iva_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);
        $this->addForeignKey('fk_cont_retencion_detalle_ivas_retencion_id', '{{%cont_retencion_detalle_ivas}}', 'retencion_id', '{{%cont_retencion}}', 'id');
        $this->addForeignKey('fk_cont_retencion_detalle_ivas_iva_id', '{{%cont_retencion_detalle_ivas}}', 'iva_id', '{{%core_iva}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
