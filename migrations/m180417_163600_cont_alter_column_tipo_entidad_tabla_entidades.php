<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180417_163600_cont_alter_column_tipo_entidad_tabla_entidades extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_entidad}}', 'tipo_entidad', 'enum(\'Cliente\', \'Proveedor\')');
    }

    public function safeDown()
    {
        echo "m180417_163600_cont_alter_column_tipo_entidad_tabla_entidades no puede ser revertido.\n";
        return false;
    }
}
