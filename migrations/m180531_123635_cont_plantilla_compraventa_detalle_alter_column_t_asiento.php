<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180531_123635_cont_plantilla_compraventa_detalle_alter_column_t_asiento extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE cont_plantilla_compraventa_detalle MODIFY tipo_asiento ENUM('venta', 'costo_venta', 'compra') NOT NULL DEFAULT 'venta';");
    }

    public function safeDown()
    {
        echo "m180531_123635_cont_plantilla_compraventa_detalle_alter_column_t_asiento no puede ser revertido.\n";
        return false;
    }
}