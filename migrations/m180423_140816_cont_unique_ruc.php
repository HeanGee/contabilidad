<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180423_140816_cont_unique_ruc extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_entidad}}', 'ruc', $this->integer(11)->unique());
    }

    public function safeDown()
    {
        echo "m180423_140816_cont_unique_ruc no puede ser revertido\n";
        return false;
    }
}
