<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180531_123625_cont_detalle_factura_compra_venta_rename extends Migration
{
    public function safeUp()
    {
        $this->renameTable('cont_detalle_factura_compra', 'cont_factura_compra_detalle');
        $this->renameTable('cont_detalle_factura_venta', 'cont_factura_venta_detalle');
    }

    public function safeDown()
    {
        echo "m180531_123625_cont_detalle_factura_compra_venta_rename no puede ser revertido.\n";
        return false;
    }
}