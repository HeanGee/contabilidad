<?php

use jamband\schemadump\Migration;

class m190430_133501_cont_addcol_plancuenta extends Migration
{
    public function safeUp()
    {
        $actividadesOperativasConcepts = [
            "'no'",
            "'ventas_netas'",
            "'pago_a_proveedores_locales'",
            "'pago_a_proveedores__del_exterior'",
            "'efectivo_pagado_a_empleados'",
            "'efectivo_generado_usado_otras_actividades'",
            "'pago_de_impuestos'"
        ];
        $actividadesInversionConcepts = [
            "'aumento_disminucion_de_inversiones_temporarias'",
            "'aumento_disminucion_de_inversion_largo_plazo'",
            "'aumento_disminucion_de_activo_fijo'",
        ];
        $actividadesFinanciamientoConcepts = [
            "'dividendos_pagados'",
            "'aporte_capital'",
            "'aumento_disminucion_de_prestamos'",
            "'aumento_disminucion_de_intereses'",
        ];

        $table = "cont_plan_cuenta";
        $enumVal = array_merge($actividadesOperativasConcepts, $actividadesInversionConcepts, $actividadesFinanciamientoConcepts);
        $enumVal = implode(", ", $enumVal);
        $this->addColumn($table, "flujo_efectivo", "ENUM($enumVal) NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . " no puede ser revertido.\n";
        return false;
    }
}
