<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180517_160330_cont_asociar_factura_con_empresa extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_venta}}', 'empresa_id', $this->integer(10)->unsigned());
        $this->addForeignKey('fk_cont_factura_venta_empresa_id', '{{%cont_factura_venta}}', 'empresa_id', '{{%core_empresa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180517_160330_cont_asociar_factura_con_empresa no puede ser revertido\n";
        return false;
    }
}
