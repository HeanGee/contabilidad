<?php

use jamband\schemadump\Migration;

class m190520_133614_cont_addcol_actfijo_tipo extends Migration
{
    public function safeUp()
    {
        $table = 'cont_activo_fijo_tipo';
        $this->addColumn($table, 'cantidad_requerida', "ENUM('si','no') NOT NULL DEFAULT 'si'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
