<?php

use jamband\schemadump\Migration;

class m190401_182613_cont_alttab_plantilla extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_plantilla_compraventa', 'para_descuento', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
