<?php

use jamband\schemadump\Migration;

class m190503_152726_cont_bigint_asientodetalle extends Migration
{
    public function safeUp()
    {
        # sin unsigned porque al crear manualmente ellos crean algunas cuentas con valores negativos.

        $this->alterColumn('cont_asiento_detalle', 'monto_debe', $this->bigInteger(18)->notNull());
        $this->alterColumn('cont_asiento_detalle', 'monto_haber', $this->bigInteger(18)->notNull());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
