<?php

use jamband\schemadump\Migration;

class m180924_160048_cont_alttab_timbrado extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_timbrado}}', 'fecha_inicio', $this->date()->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
