<?php

use jamband\schemadump\Migration;

class m180706_125321_cont_abm_rubro extends Migration
{
    public function safeUp()
    {
        // cont_rubro
        $this->createTable('{{%cont_rubro}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull(),
            'descripcion' => $this->string(256)->null(),
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL DEFAULT 'activo'",
        ], $this->tableOptions);

        // cont_empresa_rubro
        $this->createTable('{{%cont_empresa_rubro}}', [
            'id' => $this->primaryKey()->unsigned(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'rubro_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        // fk: cont_emprea_rubro
        $this->addForeignKey('fk_cont_empresa_rubro_rubro_id', '{{%cont_empresa_rubro}}', 'rubro_id', '{{%cont_rubro}}', 'id');
        $this->addForeignKey('fk_cont_empresa_rubro_empresa_id', '{{%cont_empresa_rubro}}', 'empresa_id', '{{%core_empresa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180706_125321_cont_abm_rubro no puede ser revertido.\n";
        return false;
    }
}
