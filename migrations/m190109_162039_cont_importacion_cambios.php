<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190109_162039_cont_importacion_cambios extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('cont_importacion', 'numero_despacho', $this->string(45)->notNull());
        $this->addColumn('cont_importacion', 'exportador', $this->string('100')->notNull()->defaultValue('EXP'));
        $this->addColumn('cont_importacion', 'condicion', 'ENUM("fob", "cif") NOT NULL DEFAULT "fob"');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
