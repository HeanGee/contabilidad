<?php

use jamband\schemadump\Migration;

class m181114_161119_cont_activo_biologico_factura_compra_venta extends Migration
{
    public function safeUp()
    {
        $table_name = '{{%cont_activo_biologico_stock_manager}}';
        $venta = "{{%cont_factura_venta}}";
        $compra = "{{%cont_factura_compra}}";
        $actbio = '{{%cont_activo_biologico}}';
        $especie = '{{%cont_activo_biologico_especie}}';
        $clasifi = '{{%cont_activo_biologico_especie_clasificacion}}';
        $periodo_empresa = '{{%cont_empresa_periodo_contable}}';
        $empresa = '{{%core_empresa}}';

        $this->createTable($table_name, [
            'id' => $this->primaryKey()->unsigned(),
            'factura_venta_id' => $this->integer(11)->null(),
            'factura_compra_id' => $this->integer(11)->null(),
            'especie_id' => $this->integer(10)->unsigned()->notNull(),
            'clasificacion_id' => $this->integer(10)->unsigned()->notNull(),
            'cantidad' => $this->integer(11)->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_actbio_stockmanager_empresa', $table_name, 'empresa_id', $empresa, 'id');
        $this->addForeignKey('fk_actbio_stockmanager_periodo', $table_name, 'periodo_contable_id', $periodo_empresa, 'id');

        $this->addForeignKey('fk_actbio_stockmanager_compra', $table_name, 'factura_compra_id', $compra, 'id');
        $this->addForeignKey('fk_actbio_stockmanager_venta', $table_name, 'factura_venta_id', $venta, 'id');

        $this->addForeignKey('fk_actbio_stockmanager_especie', $table_name, 'especie_id', $especie, 'id');
        $this->addForeignKey('fk_actbio_stockmanager_clasifi', $table_name, 'clasificacion_id', $clasifi, 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
