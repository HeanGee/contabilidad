<?php

use jamband\schemadump\Migration;

class m180407_044236_cont_parametro_sistema extends Migration
{
    public function safeUp()
    {

        // parametro_sistema
        $this->createTable('{{%cont_parametro_sistema}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull()->unique(),
            'valor' => $this->string(45)->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        echo "m180407_044236_cont_parametro_sistema no puede ser revertido.\n";
        return false;
    }
}
