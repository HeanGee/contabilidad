<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180532_140212_cont_relacion_factura_compra_plantilla extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%cont_factura_compra}}', 'plantilla');
        $this->addColumn('{{%cont_factura_compra}}', 'plantilla_id', $this->integer(10)->unsigned()->notNull());
        $this->addForeignKey('fk_cont_factura_compra_plantilla_id', '{{%cont_factura_compra}}', 'plantilla_id', '{{%cont_plantilla_compraventa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180532_140212_cont_relacion_factura_compra_plantilla no puede ser revertido.\n";
        return false;
    }
}
