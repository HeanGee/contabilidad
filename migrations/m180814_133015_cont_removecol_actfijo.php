<?php

use jamband\schemadump\Migration;

class m180814_133015_cont_removecol_actfijo extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%cont_activo_fijo}}', 'coeficiente_revaluo');
        $this->dropColumn('{{%cont_activo_fijo}}', 'valor_fiscal_revaluo');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
