<?php

use jamband\schemadump\Migration;

class m180709_131153_cont_addColumn_saldo_factura extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_compra}}', 'saldo', $this->decimal(14, 2)->null());
        $this->addColumn('{{%cont_factura_venta}}', 'saldo', $this->decimal(14, 2)->null());
    }

    public function safeDown()
    {
        echo "m180709_131153_cont_addColumn_saldo_factura no puede ser revertido.\n";
        return false;
    }
}
