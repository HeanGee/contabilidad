<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180508_005743_timbrado extends Migration
{
    public function safeUp()
    {
        // cont_timbrado
        $this->createTable('{{%cont_timbrado}}', [
            'id' => $this->primaryKey()->unsigned(),
            'numero_inicio' => $this->integer(11)->unsigned()->notNull(),
            'numero_fin' => $this->integer(11)->unsigned()->notNull(),
            'fecha_vencimiento' => $this->date()->notNull(),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        echo "m180508_005743_timbrado no puede ser revertido.\n";
        return false;
    }
}
