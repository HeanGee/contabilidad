<?php

use jamband\schemadump\Migration;

class m180803_130754_cont_abm_poliza extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_poliza}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nro_poliza' => $this->string(13)->notNull(),
            'fecha_desde' => $this->date()->notNull(),
            'fecha_hasta' => $this->date()->notNull(),
            'importe_diario' => $this->integer(14)->notNull(),
            'forma_devengamiento' => "ENUM('desde_fecha_de_inicio_de_vigencia','desde_fecha_de_emision_de_factura') NOT NULL",
            'factura_compra_id' => $this->integer(11)->null(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_factura_compra_id', '{{%cont_poliza}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
        $this->addForeignKey('fk_empresa_id', '{{%cont_poliza}}', 'empresa_id', 'core_empresa', 'id');
        $this->addForeignKey('fk_periodo_contable_id', '{{%cont_poliza}}', 'periodo_contable_id', 'cont_empresa_periodo_contable', 'id');

    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
