<?php

use jamband\schemadump\Migration;

class m180719_153018_cont_asociar_retencion_recibos extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_retencion}}', 'recibo_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_retencion_recibo_recibo_id', '{{%cont_retencion}}', 'recibo_id', '{{%cont_recibo}}', 'id');
    }

    public function safeDown()
    {
        echo "m180719_153018_cont_asociar_retencion_recibos no puede ser revertido.\n";
        return false;
    }
}
