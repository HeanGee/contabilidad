<?php

use jamband\schemadump\Migration;

class m180824_131823_cont_presentacion_iva extends Migration
{
    public function safeUp()
    {
        // cont_presentacion_iva
        $this->createTable('{{%cont_presentacion_iva}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nro_orden' => $this->integer(12)->null()->comment('Es generado y otorgado por la SET al momento de presentar efectivamente.'),
            'codigo_set' => $this->string(45)->null()->comment('Es generado y otorgado por la SET al momento de presentar efectivamente.'),
            'tipo_declaracion' => "ENUM ('jurada_original', 'jurada_rectificativa', 'jurada_en_caracter_de_clausura_o_ces.') NOT NULL DEFAULT 'jurada_original'",
            'periodo_contable_id' => $this->integer(11)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'estado' => "ENUM ('borrador', 'presentado') NOT NULL DEFAULT 'borrador'",
            'presentacion_iva_id' => $this->integer(10)->unsigned()->null()->comment('Referencia a una declaración existente al cual se quiere enmendar, en caso de que la misma ya haya sido presentada.'),
            'periodo_fiscal' => $this->string(7)->notNull(),
            'tipo_formulario' => $this->integer(4)->notNull()->comment('Tipo de formulario: 120, 121, 125, 126. Dependen de la legislación vigente en el país.'),
        ], $this->tableOptions);

        // cont_presentacion_iva_rubro
        $this->createTable('{{%cont_presentacion_iva_rubro}}', [
            'id' => $this->primaryKey()->unsigned(),
            'rubro' => $this->integer(3)->notNull(),
            'inciso' => $this->string(2)->notNull(),
            'columna' => "ENUM('I','II','III') NOT NULL DEFAULT 'I'",
            'casilla' => $this->integer(4)->notNull(),
            'monto' => $this->integer(16)->notNull(),
            'presentacion_iva_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        // fk: cont_presentacion_iva
        $this->addForeignKey('fk_cont_presentacion_iva_periodo_contable_id', '{{%cont_presentacion_iva}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_cont_presentacion_iva_presentacion_iva_id', '{{%cont_presentacion_iva}}', 'presentacion_iva_id', '{{%cont_presentacion_iva}}', 'id');
        $this->addForeignKey('fk_cont_presentacion_iva_empresa_id', '{{%cont_presentacion_iva}}', 'empresa_id', '{{%core_empresa}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
