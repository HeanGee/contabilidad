<?php

use jamband\schemadump\Migration;

class m190329_200351_cont_addcol_prestamodetalle extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_prestamo_detalle', 'asiento_devengamiento_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_prestamodet_asientodev_id', 'cont_prestamo_detalle', 'asiento_devengamiento_id', 'cont_asiento', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
