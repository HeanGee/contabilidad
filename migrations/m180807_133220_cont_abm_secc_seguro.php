<?php

use jamband\schemadump\Migration;

class m180807_133220_cont_abm_secc_seguro extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_poliza_seccion}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->unique()->notNull(),
            'estado' => "ENUM('activo', 'inactivo') NOT NULL DEFAULT 'activo'",
        ]);
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}