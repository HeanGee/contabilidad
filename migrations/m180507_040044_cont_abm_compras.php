<?php

use jamband\schemadump\Migration;

class m180507_040044_cont_abm_compras extends Migration
{
    public function safeUp()
    {
        // cont_factura_compra
        $this->createTable('{{%cont_factura_compra}}', [
            'id' => $this->primaryKey(),
            'tipo_documento_id' => $this->integer(10)->unsigned()->notNull(),
            'fecha_emision' => $this->date()->notNull(),
            'condicion' => 'ENUM("contado","credito")',
            'fecha_vencimiento' => $this->date()->null(),
            'entidad_id' => $this->integer(11)->notNull(),
            'obligaciones' => $this->integer(11)->null(),
            'timbrado_id' => $this->integer(11)->null(),//TODO se guarda el id o el numero?
            'nota_remision' => $this->string(100)->null(), //TODO que es? Que tipo de dato? En caso se usa?
            'total' => $this->decimal(14, 2)->notNull(),
            'nro_factura' => $this->string(15)->notNull(),
            'moneda_id' => $this->integer(11)->notNull(),
            'cotizacion' => $this->decimal(7, 2)->null(),
            'cant_cuotas' => $this->integer(3)->null(),
            'empresa_id' => $this->integer(11)->unsigned()->notNull(),
            'plantilla' => $this->string(45)->null()
        ], $this->tableOptions);

        // cont_detalle_factura_compra
        $this->createTable('{{%cont_detalle_factura_compra}}', [
            'id' => $this->primaryKey(),
            'factura_compra_id' => $this->integer(11)->notNull(),
            'iva_id' => $this->integer(10)->unsigned()->notNull(),
            'subtotal' => $this->decimal(14, 2)->notNull(),
            'plan_cuenta_id' => $this->integer(10)->unsigned()->notNull(),
            'gravada' => $this->decimal(14, 2)->notNull(),
            'impuesto' => $this->decimal(14, 2)->notNull(),
            'cta_contable' => "ENUM('debe','haber') NOT NULL",
        ], $this->tableOptions);

        // fk: cont_detalle_factura_compra
        $this->addForeignKey('fk_cont_detalle_factura_compra_factura_compra_id', '{{%cont_detalle_factura_compra}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
        $this->addForeignKey('fk_cont_detalle_factura_compra_iva_id', '{{%cont_detalle_factura_compra}}', 'iva_id', '{{%core_iva}}', 'id');
        $this->addForeignKey('fk_cont_detalle_factura_compra_plan_cuenta_id', '{{%cont_detalle_factura_compra}}', 'plan_cuenta_id', '{{%cont_plan_cuenta}}', 'id');

        // fk: cont_factura_compra
        $this->addForeignKey('fk_cont_factura_compra_moneda_id', '{{%cont_factura_compra}}', 'moneda_id', '{{%core_moneda}}', 'id');
        $this->addForeignKey('fk_cont_factura_compra_entidad_id', '{{%cont_factura_compra}}', 'entidad_id', '{{%cont_entidad}}', 'id');
        $this->addForeignKey('fk_cont_factura_compra_tipo_documento_id', '{{%cont_factura_compra}}', 'tipo_documento_id', '{{%cont_tipo_documento}}', 'id');
        $this->addForeignKey('fk_cont_factura_compra_empresa_id', '{{%cont_factura_compra}}', 'empresa_id', '{{%core_empresa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180507_040044_cont_abm_compras no puede ser revertido.\n";
        return false;
    }
}
