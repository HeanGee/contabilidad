<?php

use jamband\schemadump\Migration;

class m180814_173503_cont_addcol_actfijo2 extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_fijo}}', 'factura_compra_id', $this->integer(11)->null());
        $this->addForeignKey('fk_activo_fijo_compra_id', '{{%cont_activo_fijo}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
