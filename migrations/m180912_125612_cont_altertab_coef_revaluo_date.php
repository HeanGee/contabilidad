<?php

use jamband\schemadump\Migration;

class m180912_125612_cont_altertab_coef_revaluo_date extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_coeficiente_revaluo}}', 'periodo', $this->string(7)->notNull());
    }

    public function safeDown()
    {
        echo " m180912_125612_cont_altertab_coef_revaluo_date no puede ser revertido.\n";
        return false;
    }
}
