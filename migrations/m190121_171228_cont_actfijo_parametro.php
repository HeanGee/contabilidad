<?php

use jamband\schemadump\Migration;

class m190121_171228_cont_actfijo_parametro extends Migration
{
    public function safeUp()
    {
        // ATRIBUTOS PLANTILLA EN EL TIPO DE ACTIVO FIJO
        $tableName = 'cont_activo_fijo_tipo_atributo';
        $table = "{{%{$tableName}}}";
        $fk = "fk_afijo_tipo_attr_";

        $this->createTable($table, [
            'id' => $this->primaryKey()->unsigned(),
            'activo_fijo_tipo_id' => $this->integer(10)->unsigned()->notNull(),
            'atributo' => $this->string(100)->notNull(),
            'obligatorio' => "ENUM('si','no') NOT NULL DEFAULT 'no'",
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey("{$fk}activofijotipo_id", $table, 'activo_fijo_tipo_id', '{{%cont_activo_fijo_tipo}}', 'id');
        $this->addForeignKey("{$fk}empresa_id", $table, 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey("{$fk}periodo_contable_id", $table, 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');


        // ATRIBUTOS EN EL ACTIVO FIJO
        $tableName = 'cont_activo_fijo_atributo';
        $table = "{{%{$tableName}}}";
        $fk = "fk_afijo_attr_";

        $this->createTable($table, [
            'id' => $this->primaryKey()->unsigned(),
            'activo_fijo_id' => $this->integer(10)->unsigned()->notNull(),
            'atributo' => $this->string(100)->notNull(),
            'valor' => $this->string(256)->null(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey("{$fk}activofijo_id", $table, 'activo_fijo_id', '{{%cont_activo_fijo}}', 'id');
        $this->addForeignKey("{$fk}empresa_id", $table, 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey("{$fk}periodo_contable_id", $table, 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
