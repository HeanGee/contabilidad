<?php

use jamband\schemadump\Migration;

class m181017_155526_cont_recibo_prestamos extends Migration
{
    public function safeUp()
    {
        // cont_recibo_prestamo
        $this->createTable('{{%cont_recibo_prestamo}}', [
            'id' => $this->primaryKey()->unsigned(),
            'fecha' => $this->date()->notNull(),
            'entidad_id' => $this->integer(11)->notNull(),
            'ruc' => $this->string(45)->null(),
            'razon_social' => $this->string(256)->null(),
            'numero' => $this->string(45)->notNull(),
            'total' => $this->decimal(14, 2)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        // cont_recibo_prestamo_detalle
        $this->createTable('{{%cont_recibo_prestamo_detalle}}', [
            'id' => $this->primaryKey()->unsigned(),
            'capital' => $this->decimal(14, 2)->notNull(),
            'interes' => $this->decimal(14, 2)->notNull(),
            'monto_cuota' => $this->decimal(14, 2)->notNull(),
            'recibo_prestamo_id' => $this->integer(10)->unsigned()->notNull(),
            'prestamo_detalle_id' => $this->integer(10)->unsigned()->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        // fk: cont_recibo_prestamo
        $this->addForeignKey('fk_rec_pre_periodo_contable_id', '{{%cont_recibo_prestamo}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_rec_pre_empresa_id', '{{%cont_recibo_prestamo}}', 'empresa_id', '{{%core_empresa}}', 'id');

        // fk: cont_recibo_prestamo_detalle
        $this->addForeignKey('fk_rec_pre_det_empresa_periodo_contable_id', '{{%cont_recibo_prestamo_detalle}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_rec_pre_det_recibo_prestamo_id', '{{%cont_recibo_prestamo_detalle}}', 'recibo_prestamo_id', '{{%cont_recibo_prestamo}}', 'id');
        $this->addForeignKey('fk_rec_pre_det_empresa_id', '{{%cont_recibo_prestamo_detalle}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_rec_pre_det_prest_det_id', '{{%cont_recibo_prestamo_detalle}}', 'prestamo_detalle_id', '{{%cont_prestamo_detalle}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
