<?php

use jamband\schemadump\Migration;

class m190211_133115_cont_addcol_obligaciones extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_obligacion', 'cantidad_salario_max', $this->integer(10)->unsigned()->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
