<?php

use jamband\schemadump\Migration;

class m190513_141410_cont_addcols_activofijo extends Migration
{
    public function safeUp()
    {
        $table = 'cont_activo_fijo';
        $this->addColumn($table, 'valor_fiscal_revaluado', $this->bigInteger(15)->notNull()->defaultValue(0));
        $this->addColumn($table, 'valor_contable_revaluado', $this->bigInteger(15)->notNull()->defaultValue(0));
        $this->addColumn($table, 'asiento_revaluo_id', $this->integer(10)->unsigned()->null());
        $this->addColumn($table, 'asiento_depreciacion_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_asiento_revaluo', $table, 'asiento_revaluo_id', 'cont_asiento', 'id');
        $this->addForeignKey('fk_asiento_depreci', $table, 'asiento_depreciacion_id', 'cont_asiento', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
