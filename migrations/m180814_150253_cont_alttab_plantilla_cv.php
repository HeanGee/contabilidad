<?php

use jamband\schemadump\Migration;

class m180814_150253_cont_alttab_plantilla_cv extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_plantilla_compraventa}}', 'activo_fijo', "ENUM('si','no') NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
