<?php

use jamband\schemadump\Migration;

class m181017_114219_cont_prestamo_relatewith_entidad extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo}}', 'entidad_id', $this->integer(11)->notNull());
        $this->addForeignKey('fk_prestamo_entidad_entidad_id', '{{%cont_prestamo}}', 'entidad_id', '{{%cont_entidad}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
