<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180505_021829_tipo_documento_cuenta_columna extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_tipo_documento}}', 'cuenta_id', $this->integer(10)->unsigned()->null());

        // fk: cont_tipo_documento
        $this->addForeignKey('fk_cont_tipo_documento_cuenta_id', '{{%cont_tipo_documento}}', 'cuenta_id', '{{%cont_plan_cuenta}}', 'id');
    }

    public function safeDown()
    {
        echo 'm180505_021829_tipo_documento_cuenta_columna no puede ser revertido.\n';
        return false;
    }
}
