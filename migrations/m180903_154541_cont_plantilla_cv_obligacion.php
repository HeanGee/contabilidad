<?php

use jamband\schemadump\Migration;

class m180903_154541_cont_plantilla_cv_obligacion extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_plantilla_compraventa_obligacion}}', [
            'id' => $this->primaryKey()->unsigned(),
            'plantilla_compraventa_id' => $this->integer(10)->unsigned()->notNull(),
            'obligacion_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_plantilla_obligacion_plantilla_id', '{{%cont_plantilla_compraventa_obligacion}}', 'plantilla_compraventa_id', '{{%cont_plantilla_compraventa}}', 'id');
        $this->addForeignKey('fk_plantilla_obligacion_obligacion_id', '{{%cont_plantilla_compraventa_obligacion}}', 'obligacion_id', '{{%cont_obligacion}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
