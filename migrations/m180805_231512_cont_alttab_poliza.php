<?php

use jamband\schemadump\Migration;

class m180805_231512_cont_alttab_poliza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_poliza}}', 'seccion', "ENUM('automoviles','incendios','obras') NOT NULL");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
