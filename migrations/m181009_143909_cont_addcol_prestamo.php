<?php

use jamband\schemadump\Migration;

class m181009_143909_cont_addcol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo}}', 'primer_vencimiento', $this->date()->notNull());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
