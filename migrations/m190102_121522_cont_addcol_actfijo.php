<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190102_121522_cont_addcol_actfijo extends Migration
{
    public function safeUp()
    {
        $tableName = 'cont_activo_fijo';
        $table = "{{%{$tableName}}}";
        $this->addColumn($table, 'observacion', $this->string(256)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido,\n';
        return false;
    }
}
