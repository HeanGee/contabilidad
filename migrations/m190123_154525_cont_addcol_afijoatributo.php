<?php

use jamband\schemadump\Migration;

class m190123_154525_cont_addcol_afijoatributo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_activo_fijo_atributo', 'activo_fijo_tipo_id', $this->integer(10)->unsigned()->notNull()->after('valor'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
