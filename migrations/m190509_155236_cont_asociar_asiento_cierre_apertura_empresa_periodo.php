<?php

use jamband\schemadump\Migration;

class m190509_155236_cont_asociar_asiento_cierre_apertura_empresa_periodo extends Migration
{
    public function safeUp()
    {
        $table = 'cont_empresa_periodo_contable';
        $this->addColumn($table, 'asiento_cierre_id', $this->integer(10)->unsigned()->null());
        $this->addColumn($table, 'asiento_apertura_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_asiento_cierre_id', $table, 'asiento_cierre_id', 'cont_asiento', 'id');
        $this->addForeignKey('fk_asiento_apertura_id', $table, 'asiento_apertura_id', 'cont_asiento', 'id');
    }

    public function safeDown()
    {
        try {
            $tname = 'cont_empresa_periodo_contable';
            $table = Yii::$app->db->schema->getTableSchema('cont_empresa_periodo_contable');
            if (!isset($table->columns['asiento_cierre_id']))
                $this->dropColumn($tname, 'asiento_cierre_id');
            if (!isset($table->columns['asiento_apertura_id']))
                $this->dropColumn($tname, 'asiento_apertura_id');
            echo self::className() . ' no puede ser revertido.\n';
        } catch (Exception $exception) {
            echo "Error al hacer rollback: {$exception->getMessage()}";
        }
        return false;
    }
}
