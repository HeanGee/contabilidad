<?php

use jamband\schemadump\Migration;

class m181009_145050_cont_dropcol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%cont_prestamo}}', 'nro_credito');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
