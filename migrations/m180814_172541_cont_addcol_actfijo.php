<?php

use jamband\schemadump\Migration;

class m180814_172541_cont_addcol_actfijo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_fijo}}', 'factura_venta_id', $this->integer(11)->null());
        $this->addForeignKey('fk_activo_fijo_venta_id', '{{%cont_activo_fijo}}', 'factura_venta_id', '{{%cont_factura_venta}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
