<?php

use jamband\schemadump\Migration;

class m181026_124646_cont_activo_ganado extends Migration
{
    public function safeUp()
    {
        $tableName = $this->db->tablePrefix . 'cont_activo_ganado';
        if ($this->db->getTableSchema($tableName, true) == null) {
            $this->createTable('{{%cont_activo_ganado}}', [
                'id' => $this->primaryKey()->unsigned(),
                'empresa_id' => $this->integer(10)->unsigned()->notNull(),
                'periodo_contable_id' => $this->integer(11)->notNull(),
                'tipo' => "ENUM('ternero', 'desmamante', 'vaquilla', 'vaca', 'novillo', 'toro', 'buey') NOT NULL",
                'stock_final' => $this->integer(17)->notNull(),
                'precio_unitario' => $this->decimal(14, 2)->notNull(),
            ], $this->tableOptions);

            $this->addForeignKey('fk_activo_ganado_empresa', '{{%cont_activo_ganado}}', 'empresa_id', '{{%core_empresa}}', 'id');
            $this->addForeignKey('fk_activo_ganado_periodo', '{{%cont_activo_ganado}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
            return true;
        }
        return false;
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
