<?php

use backend\modules\contabilidad\models\TipoDocumentoSet;
use yii\db\Schema;
use jamband\schemadump\Migration;

class m180918_182403_cont_parametro_sistema_tipo_doc_ninguno extends Migration
{
    public function safeUp()
    {
        try {
            if (!TipoDocumentoSet::find()->where(['like', 'nombre', 'ninguno'])->exists()) {
                $this->execute('
                    INSERT INTO cont_tipo_documento_set(nombre, detalle, estado) VALUES("ninguno", "ninguno", "activo");
                ');
            }

            if (!\backend\modules\contabilidad\models\ParametroSistema::find()->where(['nombre' => 'tipo_documento_set_ninguno'])->exists()) {
                $this->execute('
                    INSERT INTO cont_parametro_sistema
                      (nombre, valor) SELECT
                      "tipo_documento_set_ninguno",
                      id
                    FROM cont_tipo_documento_set
                    WHERE nombre LIKE "ninguno";
                ');
            }
        } catch (Exception $e) {

        }
    }

    public function safeDown()
    {
        echo "m180918_182403_aont_parametro_sistema_tipo_doc_ninguno no puede ser revertido.\n";
        return false;
    }
}
