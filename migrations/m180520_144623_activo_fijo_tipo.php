<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180520_144623_activo_fijo_tipo extends Migration
{
    public function safeUp()
    {
        // cont_activo_fijo_tipo
        $this->createTable('{{%cont_activo_fijo_tipo}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(255)->notNull(),
            'vida_util' => (new yii\db\ColumnSchemaBuilder('tinyint', 2, $this->db))->notNull(),
            'cuenta_id' => $this->integer(10)->unsigned()->notNull(),
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL",
        ], $this->tableOptions);

        // fk: cont_activo_fijo_tipo
        $this->addForeignKey('fk_cont_activo_fijo_tipo_cuenta_id', '{{%cont_activo_fijo_tipo}}', 'cuenta_id', '{{%cont_plan_cuenta}}', 'id');
    }

    public function safeDown()
    {
        echo "m180520_144623_activo_fijo_tipo no puede ser revertido.\n";
        return false;
    }
}
