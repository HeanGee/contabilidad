<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180919_045449_cont_factura_compra_alter_column_timbrado_detalle_id extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_factura_compra}}', 'timbrado_detalle_id', $this->integer(10)->unsigned()->null());
    }

    public function safeDown()
    {
        echo "m180919_045449_cont_factura_compra_alter_column_timbrado_detalle_id no puede ser revertido.\n";
        return false;
    }
}
