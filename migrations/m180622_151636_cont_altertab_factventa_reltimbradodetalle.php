<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180622_151636_cont_altertab_factventa_reltimbradodetalle extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_venta}}', 'timbrado_detalle_id', $this->integer(11)->unsigned()->null());
        $this->addForeignKey('fk_cont_factura_venta__timbrado_detalle_id', '{{%cont_factura_venta}}', 'timbrado_detalle_id', '{{%cont_timbrado_detalle}}', 'id');
    }

    public function safeDown()
    {
        echo "m180622_151636_cont_altertab_factventa_reltimbradodetalle no puede ser revertido.\n";
        return false;
    }
}
