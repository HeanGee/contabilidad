<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181206_175901_cont_importaciones extends Migration
{
    public function safeUp()
    {
        // cont_importacion
        $this->createTable('{{%cont_importacion}}', [
            'id' => $this->primaryKey(),
            'fecha' => $this->date()->notNull(),
            'estado' => "ENUM ('vigente', 'anulado') NOT NULL DEFAULT 'vigente'",
            'criterio_retencion' => "ENUM ('pago_neto', 'absorcion') NOT NULL",
            'medio_transporte' => "ENUM ('terrestre', 'aereo', 'maritimo') NOT NULL",
            'numero_despacho' => $this->integer(11)->notNull(),
            'cal_retencion_fact_local_flete_seguro' => "ENUM ('si', 'no') NOT NULL",
            'numero' => $this->integer(11)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        // cont_importacion_detalle_factura_local_exterior
        $this->createTable('{{%cont_importacion_detalle_factura_local_exterior}}', [
            'id' => $this->primaryKey(),
            'importacion_id' => $this->integer(11)->notNull(),
            'factura_compra_id' => $this->integer(11)->notNull(),
            'cotizacion' => $this->decimal(14, 2)->null(),
            'es_local' => "ENUM ('si', 'no') NOT NULL",
        ], $this->tableOptions);

        // fk: cont_importacion
        $this->addForeignKey('fk_cont_importacion_periodo_contable_id', '{{%cont_importacion}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_cont_importacion_empresa_id', '{{%cont_importacion}}', 'empresa_id', '{{%core_empresa}}', 'id');

        // fk: cont_importacion_detalle_factura_local_exterior
        $this->addForeignKey('fk_importacion_detalle_factura_local_exterior_factura_compra_id', '{{%cont_importacion_detalle_factura_local_exterior}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
        $this->addForeignKey('fk_importacion_detalle_factura_local_exterior_importacion_id', '{{%cont_importacion_detalle_factura_local_exterior}}', 'importacion_id', '{{%cont_importacion}}', 'id');

    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
