<?php

use jamband\schemadump\Migration;

class m180803_155448_cont_compra_polizaId extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_compra}}', 'poliza_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_compra_poliza_id', '{{%cont_factura_compra}}', 'poliza_id', '{{%cont_poliza}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
