<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190213_130227_cont_sub_obliacion extends Migration
{
    public function safeUp()
    {
        // cont_sub_obligacion
        $this->createTable('{{%cont_sub_obligacion}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'descripcion' => $this->string(256)->null(),
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL DEFAULT 'activo'",
            'obligacion_id' => $this->integer(11)->notNull()
        ], $this->tableOptions);

        $this->addForeignKey('fk_cont_sub_obligacion_obligacion_id', '{{%cont_sub_obligacion}}', 'obligacion_id', '{{%cont_obligacion}}', 'id');
    }

    public function safeDown()
    {
        echo "m190213_130227_cont_sub_obliacion no puede ser revertido.\n";
        return false;
    }
}
