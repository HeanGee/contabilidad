<?php

use jamband\schemadump\Migration;

class m190125_121911_cont_addcol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_prestamo', 'iva_interes_cobrado', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
