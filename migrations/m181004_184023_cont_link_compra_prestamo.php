<?php

use jamband\schemadump\Migration;

class m181004_184023_cont_link_compra_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo}}', 'factura_compra_id', $this->integer(11)->null());
        $this->addForeignKey('fk_prestamo_compra_id', '{{%cont_prestamo}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
