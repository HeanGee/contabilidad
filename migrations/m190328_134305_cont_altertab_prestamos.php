<?php

use jamband\schemadump\Migration;

class m190328_134305_cont_altertab_prestamos extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('cont_prestamo', 'tiene_factura', "ENUM('no','por_cuota','por_prestamo') NOT NULL DEFAULT 'por_cuota'");
        $this->dropColumn('cont_prestamo', 'iva_interes_cobrado');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
