<?php

use jamband\schemadump\Migration;

class m181001_174642_cont_addcol_plantilla extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_plantilla_compraventa}}', 'para_prestamo', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
