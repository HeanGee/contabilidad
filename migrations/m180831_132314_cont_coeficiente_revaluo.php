<?php

use jamband\schemadump\Migration;

class m180831_132314_cont_coeficiente_revaluo extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_coeficiente_revaluo}}', ['id' => $this->primaryKey()->unsigned(), 'periodo' => $this->date()->notNull(), 'resolucion' => $this->integer(11)->unsigned()->notNull(),

        ], $this->tableOptions);
    }

    public function safeDown()
    {
        echo "m180508_005743_coeficiente_revaluo no puede ser revertido.\n";
        return false;
    }
}
