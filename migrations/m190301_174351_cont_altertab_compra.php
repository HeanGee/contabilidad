<?php

use jamband\schemadump\Migration;

class m190301_174351_cont_altertab_compra extends Migration
{
    public function safeUp()
    {
        /** Uso de la columna asiento_id_por_cuota_prestamo.
         *
         * @Fecha: 01 de Marzo de 2019, Viernes.
         * @Explicacion: Anteriormente, las cuotas devengadas fueron asentadas al generar asiento de compra.
         * Sin embargo, el 28 de Febrero del 2019, JMG le pidio a Jose separar el asentamiento de cuotas devengadas
         * del GENERAR ASIENTO de Compra y ademas, se necesita que hayan tantos asientos como cantidad de cuotas
         * devengadas.
         *
         * Como hay tantos detalles necesarios como cuotas devengadas, marcar estos detalles de compra con el id del
         * asiento generado, permitira diferenciar de aquellas "cuotas devengadas aun no asentadas".
         *
         */

        $this->addColumn('cont_factura_compra_detalle', 'asiento_id_por_cuota_prestamo', $this->integer(11)->unsigned()->null());
        $this->addForeignKey('fk_asiento_por_cuota_prestamo', 'cont_factura_compra_detalle', 'asiento_id_por_cuota_prestamo', 'cont_asiento', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
