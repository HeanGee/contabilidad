<?php

use jamband\schemadump\Migration;

class m180920_134558_cont_prestamo extends Migration
{
    public function safeUp()
    {
        // cont_prestamo, segun ejemplar del cliente.
        $this->createTable('{{%cont_prestamo}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nro_credito' => $this->string(45)->notNull(),
            'fecha_operacion' => $this->date()->notNull(),
            'capital' => $this->decimal(14, 2)->notNull(),
            'interes' => $this->decimal(14, 2),
            'vencimiento' => $this->date()->notNull()->comment('El ultimo mes en el que se paga la ultima cuota.'),
            'tasa' => $this->decimal(6, 2)->null(),
            'cant_cuotas' => $this->integer(11)->null(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_prestamo_empresa', '{{%cont_prestamo}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_prestamo_periodocontable', '{{%cont_prestamo}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');


        // cont_prestamo_detalle
        $this->createTable('{{%cont_prestamo_detalle}}', [
            'id' => $this->primaryKey()->unsigned(),
            'prestamo_id' => $this->integer(11)->unsigned()->notNull(),
            'nro_cuota' => $this->integer(11)->notNull()->comment('Como maximo tiene que valer igual al cant_cuotas del cont_prestamo.'),
            'vencimiento' => $this->date()->notNull(),
            'monto_cuota' => $this->decimal(14, 2)->notNull(),
            'interes' => $this->decimal(14, 2)->notNull(),
            'capital' => $this->decimal(14, 2)->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'factura_compra_id' => $this->integer(11)->null(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_prestamoDetalle_prestamo', '{{%cont_prestamo_detalle}}', 'prestamo_id', '{{%cont_prestamo}}', 'id');
        $this->addForeignKey('fk_prestamoDetalle_empresa', '{{%cont_prestamo_detalle}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_prestamoDetalle_periodocontable', '{{%cont_prestamo_detalle}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_prestamoDetalle_compra', '{{%cont_prestamo_detalle}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido\n';
        return false;
    }
}
