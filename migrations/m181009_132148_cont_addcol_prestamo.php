<?php

use jamband\schemadump\Migration;

class m181009_132148_cont_addcol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo}}', 'tipo_interes', "ENUM('us','fr','de') NOT NULL DEFAULT 'fr'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
