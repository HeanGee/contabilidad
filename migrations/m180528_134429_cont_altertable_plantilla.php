<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180528_134429_cont_altertable_plantilla extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_plantilla_compraventa}}', 'tiene_asiento_costo', 'ENUM (\'si\', \'no\') NOT NULL DEFAULT \'no\'');
        $this->addColumn('{{%cont_plantilla_compraventa_detalle}}', 'para_asiento_costo', 'ENUM (\'si\', \'no\') NOT NULL DEFAULT \'no\'');
    }

    public function safeDown()
    {
        echo "m180528_134429_cont_altertable_plantilla no puede ser revertido.\n";
        return false;
    }
}
