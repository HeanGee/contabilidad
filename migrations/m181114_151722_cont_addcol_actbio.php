<?php

use jamband\schemadump\Migration;

class m181114_151722_cont_addcol_actbio extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_biologico}}', 'stock_actual', $this->integer(11)->notNull());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
