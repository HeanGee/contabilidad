<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181130_135905_cont_asiento_alter_column_modulo_origen extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('cont_asiento', 'modulo_origen', 'ENUM("contabilidad","rrhh") NOT NULL');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
