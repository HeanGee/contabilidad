<?php

use jamband\schemadump\Migration;

class m190605_122857_cont_addcol_ivactausada_compra extends Migration
{
    public function safeUp()
    {
        $table = 'cont_factura_compra_iva_cuenta_usada';
        $this->addColumn($table, 'gnd_motivo', $this->string(256)->null()->comment("Dejar escrito el motivo por el que se uso una plantilla gnd"));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
