<?php

use jamband\schemadump\Migration;

class m180705_165349_cont_abm_obligaciones_empresa extends Migration
{
    public function safeUp()
    {
        // cont_obligacion
        $this->createTable('{{%cont_obligacion}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(45)->notNull(),
            'descripcion' => $this->string(256)->null(),
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL DEFAULT 'activo'",
        ], $this->tableOptions);

        // cont_empresa_obligacion
        $this->createTable('{{%cont_empresa_obligacion}}', [
            'id' => $this->primaryKey()->unsigned(),
            'obligacion_id' => $this->integer(11)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        // fk: cont_empresa_obligacion
        $this->addForeignKey('fk_cont_empresa_obligacion_obligacion_id', '{{%cont_empresa_obligacion}}', 'obligacion_id', '{{%cont_obligacion}}', 'id');
        $this->addForeignKey('fk_cont_empresa_obligacion_empresa_id', '{{%cont_empresa_obligacion}}', 'empresa_id', '{{%core_empresa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180705_165349_cont_abm_obligaciones_empresa no puede ser revertido.\n";
        return false;
    }
}
