<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180627_020610_tipo_documento_set extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_tipo_documento_set}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull(),
            'detalle' => $this->string(255)->null(),
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL DEFAULT 'activo'",
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        echo 'm180627_020610_tipo_documento_set no puede ser revertido.\n';
        return false;
    }
}
