<?php

use jamband\schemadump\Migration;

class m181029_153818_cont_addcol_activog extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_ganado}}', 'stock_inicial', $this->integer(17)->notNull()->after('tipo'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido\n';
        return false;
    }
}
