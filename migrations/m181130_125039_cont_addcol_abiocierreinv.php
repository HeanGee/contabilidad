<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181130_125039_cont_addcol_abiocierreinv extends Migration
{
    public function safeUp()
    {
        $table_name = '{{%cont_activo_biologico_cierre_inventario}}';
        $this->addColumn($table_name, 'ajuste_inventario', $this->integer(17)->notNull()->defaultValue(0)->after('precio_foro'));
        $this->addColumn($table_name, 'recoluta', $this->integer(17)->notNull()->defaultValue(0)->after('ajuste_inventario'));
        $this->addColumn($table_name, 'abigeato', $this->integer(17)->notNull()->defaultValue(0)->after('recoluta'));
        $this->addColumn($table_name, 'faena', $this->integer(17)->notNull()->defaultValue(0)->after('abigeato'));
        $this->addColumn($table_name, 'faena_gnd', $this->integer(17)->notNull()->defaultValue(0)->after('faena'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
