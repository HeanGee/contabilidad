<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181228_193555_cont_altertab_venta extends Migration
{
    public function safeUp()
    {
        $tableName = 'cont_factura_venta';
        $table = "{{%{$tableName}}}";
        $this->alterColumn($table, 'para_iva', "ENUM('si', 'no') NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
