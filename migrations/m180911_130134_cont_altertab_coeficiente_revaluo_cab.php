<?php

use jamband\schemadump\Migration;

class m180911_130134_cont_altertab_coeficiente_revaluo_cab extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'enajenacion', $this->decimal(8, 7));

    }

    public function safeDown()
    {
        echo self::className() . 'No puede ser revertido.\n';
        return false;
    }
}
