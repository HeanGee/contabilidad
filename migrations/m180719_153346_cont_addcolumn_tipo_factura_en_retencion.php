<?php

use jamband\schemadump\Migration;

class m180719_153346_cont_addcolumn_tipo_factura_en_retencion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{cont_retencion}}', 'tipo_factura', "ENUM('compra','venta')");
    }

    public function safeDown()
    {
        echo self::className() . " no puede ser revertido.\n";
        return false;
    }
}
