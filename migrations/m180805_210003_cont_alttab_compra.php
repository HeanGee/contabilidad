<?php

use jamband\schemadump\Migration;

class m180805_210003_cont_alttab_compra extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_compra_poliza_id', '{{%cont_factura_compra}}');
        $this->dropIndex('fk_compra_poliza_id', '{{%cont_factura_compra}}');
        $this->dropColumn('{{%cont_factura_compra}}', 'poliza_id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
