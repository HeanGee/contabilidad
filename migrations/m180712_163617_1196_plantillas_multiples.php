<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180712_163617_1196_plantillas_multiples extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_cont_factura_compra_plantilla_id', '{{%cont_factura_compra}}');
        $this->dropIndex('fk_cont_factura_compra_plantilla_id', '{{%cont_factura_compra}}');
        $this->dropColumn('{{%cont_factura_compra}}', 'plantilla_id');

        $this->addColumn('cont_factura_compra_iva_cuenta_usada', 'plantilla_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_cont_factura_compra_iva_cuenta_usada_plantilla_id', '{{%cont_factura_compra_iva_cuenta_usada}}', 'plantilla_id', '{{%cont_plantilla_compraventa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180712_163617_1196_plantillas_multiples no puede ser revertido.\n";
        return false;
    }
}
