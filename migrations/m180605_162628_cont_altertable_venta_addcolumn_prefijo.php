<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180605_162628_cont_altertable_venta_addcolumn_prefijo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_venta}}', 'prefijo', $this->string(7)->notNull());
    }

    public function safeDown()
    {
        echo "m180605_162628_cont_altertable_venta_addcolumn_prefijo no puede ser revertido.\n";
        return false;
    }
}
