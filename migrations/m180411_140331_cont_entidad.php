<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180411_140331_cont_entidad extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_entidad}}', [
            'id' => $this->primaryKey(),
            'razon_social' => $this->string(255)->notNull(),
            'ruc' => $this->integer(11)->notNull(),
            'tipo_entidad' => $this->string(45)->notNull(),
            'digito_verificador' => $this->boolean()->notNull(),
            'direccion' => $this->string(255)->null(),
            'telefonos' => $this->string(255)->null(),
            'es_contribuyente' => $this->integer(2),
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        echo "m180411_140331_crear_tabla_entidad no puede ser revertido.\n";
        return false;
    }
}
