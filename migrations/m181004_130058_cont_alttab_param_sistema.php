<?php

use jamband\schemadump\Migration;

class m181004_130058_cont_alttab_param_sistema extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_parametro_sistema}}', 'nombre', $this->string(128)->notNull());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
