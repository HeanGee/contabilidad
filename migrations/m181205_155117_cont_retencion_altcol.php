<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181205_155117_cont_retencion_altcol extends Migration
{
    public function safeUp()
    {
        $tableName = '{{%cont_retencion}}';
        $this->alterColumn($tableName, 'base_renta_porc', $this->decimal(14, 2)->notNull()->defaultValue(0.0));
        $this->alterColumn($tableName, 'factor_renta_porc', $this->decimal(14, 2)->notNull()->defaultValue(0.0));
    }

    public function safeDown()
    {
        self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
