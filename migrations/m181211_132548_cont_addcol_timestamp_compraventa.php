<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181211_132548_cont_addcol_timestamp_compraventa extends Migration
{
    public function safeUp()
    {
        $tableNameCompra = '{{%cont_factura_compra}}';
        $tableNameVenta = '{{%cont_factura_venta}}';
        $this->addColumn($tableNameCompra, 'creado', $this->dateTime()->null());
        $this->addColumn($tableNameCompra, 'modificado', $this->dateTime()->null());
        $this->addColumn($tableNameVenta, 'creado', $this->dateTime()->null());
        $this->addColumn($tableNameVenta, 'modificado', $this->dateTime()->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
