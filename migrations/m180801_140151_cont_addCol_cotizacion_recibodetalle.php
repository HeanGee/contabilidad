<?php

use jamband\schemadump\Migration;

class m180801_140151_cont_addCol_cotizacion_recibodetalle extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_recibo_detalle}}', 'cotizacion', $this->decimal(14, 2)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
