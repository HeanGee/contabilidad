<?php

use jamband\schemadump\Migration;

class m180730_185746_cont_addcolumn_pc_recibo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_recibo}}', 'empresa_id', $this->integer(10)->unsigned()->null());
        $this->addColumn('{{%cont_recibo}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addColumn('{{%cont_recibo_detalle}}', 'empresa_id', $this->integer(10)->unsigned()->null());
        $this->addColumn('{{%cont_recibo_detalle}}', 'periodo_contable_id', $this->integer(11)->null());

        $this->addForeignKey('fk_recibo_empresa_empresa_id', '{{%cont_recibo}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_recibo_periodocontable_periodo_contable_id', '{{%cont_recibo}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_recibodetalle_empresa_empresa_id', '{{%cont_recibo_detalle}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_recibodetalle_periodocontable_periodo_contable_id', '{{%cont_recibo_detalle}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
