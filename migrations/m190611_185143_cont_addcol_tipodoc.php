<?php

use jamband\schemadump\Migration;

class m190611_185143_cont_addcol_tipodoc extends Migration
{
    public function safeUp()
    {
        $tab = 'cont_tipo_documento';
        $this->addColumn($tab, 'asiento_independiente', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
