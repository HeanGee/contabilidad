<?php

use jamband\schemadump\Migration;

class m181016_112731_cont_addenum_prestamo extends Migration
{
    public function safeUp()
    {
        // $this->addColumn('{{%cont_prestamo}}', 'tipo_interes', "ENUM('us','fr','de') NOT NULL DEFAULT 'fr'");
        $this->alterColumn('{{%cont_prestamo}}', 'tipo_interes', "ENUM('us','fr','de', 'sm') NOT NULL DEFAULT 'fr'");
    }

    public function safeDown()
    {
    }
}
