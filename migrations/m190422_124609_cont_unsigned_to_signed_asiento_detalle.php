<?php

use jamband\schemadump\Migration;

class m190422_124609_cont_unsigned_to_signed_asiento_detalle extends Migration
{
    public function safeUp()
    {

        $this->alterColumn('cont_asiento_detalle', 'monto_debe', $this->integer(11)->notNull()->defaultValue(0));
        $this->alterColumn('cont_asiento_detalle', 'monto_haber', $this->integer(11)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
