<?php

use jamband\schemadump\Migration;

class m190311_162753_cont_addcol_importacion extends Migration
{
    public function safeUp()
    {
        $table = 'cont_importacion';
        $this->addColumn($table, 'flete_costo', $this->decimal(14, 2)->null());
        $this->addColumn($table, 'seguro_costo', $this->decimal(14, 2)->null());
        $this->addColumn($table, 'despacho_moneda_id', $this->integer(11)->null());
        $this->addColumn($table, 'despacho_cotizacion', $this->decimal(14, 2)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
