<?php

use jamband\schemadump\Migration;

class m181029_173745_cont_addcol_activog extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_ganado}}', 'activo_ganado_padre_id', $this->integer(10)->unsigned()->null());

        $this->addForeignKey('fk_activo_ganado_self', '{{%cont_activo_ganado}}', 'activo_ganado_padre_id', '{{%cont_activo_ganado}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
