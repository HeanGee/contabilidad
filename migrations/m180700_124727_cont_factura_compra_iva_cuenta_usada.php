<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180700_124727_cont_factura_compra_iva_cuenta_usada extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_factura_compra_iva_cuenta_usada}}', [
            'id' => $this->primaryKey()->unsigned(),
            'monto' => $this->decimal(14, 2)->notNull(),
            'iva_cta_id' => $this->integer(11)->null(),
            'plan_cuenta_id' => $this->integer(10)->unsigned()->null(),
            'factura_compra_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        // fk: cont_factura_compra_iva_cuenta_usada
        $this->addForeignKey('fk_cont_factura_compra_iva_cuenta_usada_factura_compra_id', '{{%cont_factura_compra_iva_cuenta_usada}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
        $this->addForeignKey('fk_cont_factura_compra_iva_cuenta_usada_iva_cta_id', '{{%cont_factura_compra_iva_cuenta_usada}}', 'iva_cta_id', '{{%cont_iva_cuenta}}', 'id');
        $this->addForeignKey('fk_cont_factura_compra_iva_cuenta_usada_plan_cuenta_id', '{{%cont_factura_compra_iva_cuenta_usada}}', 'plan_cuenta_id', '{{%cont_plan_cuenta}}', 'id');
    }

    public function safeDown()
    {
        echo "m180700_124727_cont_factura_compra_iva_cuenta_usada no puede ser revertido.\n";
        return false;
    }
}
