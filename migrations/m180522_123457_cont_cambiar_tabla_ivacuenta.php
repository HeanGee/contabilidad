<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180522_123457_cont_cambiar_tabla_ivacuenta extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%cont_iva_cuenta}}', 'operacion');
        $this->dropForeignKey('fk_cont_iva_cuenta_contracuenta1id', '{{%cont_iva_cuenta}}');
        $this->dropForeignKey('fk_cont_iva_cuenta_contracuenta2_id', '{{%cont_iva_cuenta}}');
        $this->dropIndex('fk_cont_iva_cuenta_contracuenta1id', '{{%cont_iva_cuenta}}');
        $this->dropIndex('fk_cont_iva_cuenta_contracuenta2_id', '{{%cont_iva_cuenta}}');

        $this->renameColumn('{{%cont_iva_cuenta}}', 'contracuenta1_id', 'cuenta_compra_id');
        $this->renameColumn('{{%cont_iva_cuenta}}', 'contracuenta2_id', 'cuenta_venta_id');

        $this->addForeignKey('fk_cont_iva_cuenta_cuenta_compra_id', '{{%cont_iva_cuenta}}', 'cuenta_compra_id', '{{%cont_plan_cuenta}}', 'id');
        $this->addForeignKey('fk_cont_iva_cuenta_cuenta_venta_id', '{{%cont_iva_cuenta}}', 'cuenta_venta_id', '{{%cont_plan_cuenta}}', 'id');

        $this->alterColumn('{{%cont_iva_cuenta}}', 'cuenta_compra_id', $this->integer(10)->unsigned()->null());
    }

    public function safeDown()
    {
        echo "m180522_123457_cont_cambiar_tabla_ivacuenta no puede ser revertido.\n";
        return false;
    }
}
