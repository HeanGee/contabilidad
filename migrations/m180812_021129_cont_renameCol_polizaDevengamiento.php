<?php

use jamband\schemadump\Migration;

class m180812_021129_cont_renameCol_polizaDevengamiento extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%cont_poliza_devengamiento}}', 'aho_mes', 'anho_mes');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
