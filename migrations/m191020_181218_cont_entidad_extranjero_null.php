<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m191020_181218_cont_entidad_extranjero_null extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('cont_entidad', 'extranjero', $this->string(4)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
