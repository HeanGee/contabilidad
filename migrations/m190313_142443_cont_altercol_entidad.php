<?php

use jamband\schemadump\Migration;

class m190313_142443_cont_altercol_entidad extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('cont_entidad', 'ruc', $this->string(128)->unique()->notNull());
        $this->addColumn('cont_entidad', 'extrangero', "ENUM('si','no')NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
