<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180515_165442_cont_borrar_columnas_ventadetalle extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%cont_detalle_factura_venta}}', 'gravada');
        $this->dropColumn('{{%cont_detalle_factura_venta}}', 'impuesto');
        $this->dropForeignKey('fk_cont_detalle_factura_venta_iva_id', '{{%cont_detalle_factura_venta}}');
        $this->dropIndex('fk_cont_detalle_factura_venta_iva_id', '{{%cont_detalle_factura_venta}}');
        $this->dropColumn('{{%cont_detalle_factura_venta}}', 'iva_id');
    }

    public function safeDown()
    {
        echo "m180515_165442_cont_borrar_columnas_ventadetalle no puede ser revertido.\n";
        return false;
    }
}
