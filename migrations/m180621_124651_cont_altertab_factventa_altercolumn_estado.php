<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180621_124651_cont_altertab_factventa_altercolumn_estado extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_factura_venta}}', 'estado', "ENUM ('vigente', 'anulada', 'faltante') DEFAULT NULL DEFAULT 'vigente'");
    }

    public function safeDown()
    {
        echo "m180621_124651_cont_altertab_factventa_altercolumn_estado no puede ser revertido.\n";
        return false;
    }
}
