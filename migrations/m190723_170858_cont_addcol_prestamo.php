<?php

use jamband\schemadump\Migration;

class m190723_170858_cont_addcol_prestamo extends Migration
{
    public function safeUp()
    {
        $table = 'cont_prestamo';
        $this->addColumn($table, 'es_del_periodo_anterior', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
