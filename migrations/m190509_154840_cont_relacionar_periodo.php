<?php

use jamband\schemadump\Migration;

class m190509_154840_cont_relacionar_periodo extends Migration
{
    public function safeUp()
    {
        $table = 'cont_empresa_periodo_contable';
        $this->addColumn($table, 'periodo_anterior_id', $this->integer(11)->null());
        $this->addForeignKey('fk_periodo_anterior_id', $table, 'periodo_anterior_id', $table, 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
