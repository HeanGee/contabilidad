<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180427_202725_plan_cuenta_columna_ordenable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_plan_cuenta', 'cod_ordenable', $this->string(45)->notNull());
        // refactoring de datos
        $models = \backend\modules\contabilidad\models\PlanCuenta::find()->all();
        /** @var \backend\modules\contabilidad\models\PlanCuenta $_model */
        foreach($models as $_model){
            $_model->setCodigoOrdenable();
            $_model->save();
        }
    }

    public function safeDown()
    {
        echo 'm180427_202725_plan_cuenta_columna_ordenable no puede ser revertido.\n';
        return false;
    }
}
