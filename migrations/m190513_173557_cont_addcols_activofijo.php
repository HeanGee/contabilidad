<?php

use jamband\schemadump\Migration;

class m190513_173557_cont_addcols_activofijo extends Migration
{
    public function safeUp()
    {
        $tab = 'cont_activo_fijo';
        $this->addColumn($tab, 'cuenta_depreciacion_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_cta_deprec', $tab, 'cuenta_depreciacion_id', 'cont_plan_cuenta', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
