<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190102_221759_cont_altercol_ivarubro extends Migration
{
    public function safeUp()
    {
        $tableName = 'cont_presentacion_iva_rubro';
        $table = "{{%{$tableName}}}";
        $this->alterColumn($table, 'monto', $this->decimal(17, 0)->notNull());
        $this->alterColumn($table, 'columna', "ENUM('I','II','III') NULL");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
