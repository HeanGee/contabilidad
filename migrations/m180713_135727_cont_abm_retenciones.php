<?php

use jamband\schemadump\Migration;

class m180713_135727_cont_abm_retenciones extends Migration
{
    public function safeUp()
    {
        // cont_retencion
        $this->createTable('{{%cont_retencion}}', [
            'id' => $this->primaryKey()->unsigned(),
            'fecha_emision' => $this->date()->notNull(),
            'nro_retencion' => $this->string(15)->notNull(),
            'base_renta_porc' => $this->decimal(5, 2)->null()->defaultValue('0.00'),
            'base_renta_cabezas' => $this->decimal(14, 2)->null()->defaultValue('0.00'),
            'base_renta_toneladas' => $this->decimal(14, 2)->null()->defaultValue('0.00'),
            'factor_renta_porc' => $this->decimal(5, 2)->null()->defaultValue('0.00'),
            'factor_renta_cabezas' => $this->decimal(14, 2)->null()->defaultValue('0.00'),
            'factor_renta_toneladas' => $this->decimal(14, 2)->null()->defaultValue('0.00'),
            'factura_compra_id' => $this->integer(11)->null(),
            'factura_venta_id' => $this->integer(11)->null(),
        ], $this->tableOptions);

        // cont_retencion_detalle_base
        $this->createTable('{{%cont_retencion_detalle_base}}', [
            'id' => $this->primaryKey()->unsigned(),
            'retencion_id' => $this->integer(10)->unsigned()->notNull(),
            'base' => $this->decimal(14, 2)->null()->defaultValue('0.00'),
            'iva_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        // cont_retencion_detalle_factor
        $this->createTable('{{%cont_retencion_detalle_factor}}', [
            'id' => $this->primaryKey()->unsigned(),
            'retencion_id' => $this->integer(10)->unsigned()->notNull(),
            'factor' => $this->decimal(5, 2)->null()->defaultValue('30.00'),
            'iva_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        // fk: cont_retencion
        $this->addForeignKey('fk_cont_retencion_factura_compra_id', '{{%cont_retencion}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
        $this->addForeignKey('fk_cont_retencion_factura_venta_id', '{{%cont_retencion}}', 'factura_venta_id', '{{%cont_factura_venta}}', 'id');

        // fk: cont_retencion_detalle_base
        $this->addForeignKey('fk_cont_retencion_detalle_base_retencion_id', '{{%cont_retencion_detalle_base}}', 'retencion_id', '{{%cont_retencion}}', 'id');
        $this->addForeignKey('fk_cont_retencion_detalle_base_iva_id', '{{%cont_retencion_detalle_base}}', 'iva_id', '{{%core_iva}}', 'id');

        // fk: cont_retencion_detalle_factor
        $this->addForeignKey('fk_cont_retencion_detalle_factor_retencion_id', '{{%cont_retencion_detalle_factor}}', 'retencion_id', '{{%cont_retencion}}', 'id');
        $this->addForeignKey('fk_cont_retencion_detalle_factor_iva_id', '{{%cont_retencion_detalle_factor}}', 'iva_id', '{{%core_iva}}', 'id');

    }

    public function safeDown()
    {
    }
}
