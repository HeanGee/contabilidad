<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181203_174217_cont_addcol_plantilla extends Migration
{
    public function safeUp()
    {
        $tableName = '{{%cont_plantilla_compraventa}}';
        $this->addColumn($tableName, 'para_importacion', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
