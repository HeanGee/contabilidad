<?php

use jamband\schemadump\Migration;

class m180706_152727_cont_relate_plantillas_rubros extends Migration
{
    public function safeUp()
    {

        // cont_plantilla_compraventa_rubro
        $this->createTable('{{%cont_plantilla_compraventa_rubro}}', [
            'id' => $this->primaryKey()->unsigned(),
            'plantilla_compraventa_id' => $this->integer(10)->unsigned()->notNull(),
            'rubro_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        // fk: cont_plantilla_compraventa_rubro
        $this->addForeignKey('fk_cont_plantilla_compraventa_rubro_plantilla_compraventa_id', '{{%cont_plantilla_compraventa_rubro}}', 'plantilla_compraventa_id', '{{%cont_plantilla_compraventa}}', 'id');
        $this->addForeignKey('fk_cont_plantilla_compraventa_rubro_rubro_id', '{{%cont_plantilla_compraventa_rubro}}', 'rubro_id', '{{%cont_rubro}}', 'id');
    }

    public function safeDown()
    {
        echo "m180706_152727_cont_relate_plantillas_rubros no puede ser revertido.\n";
        return false;
    }
}
