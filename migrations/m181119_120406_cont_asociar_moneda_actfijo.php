<?php

use jamband\schemadump\Migration;

class m181119_120406_cont_asociar_moneda_actfijo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_fijo}}', 'moneda_id', $this->integer(11)->null());
        $this->addForeignKey('fk_actfijo_moneda', '{{%cont_activo_fijo}}', 'moneda_id', '{{%core_moneda}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
