<?php

use jamband\schemadump\Migration;

class m181114_160212_cont_addcol_plantilla extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_plantilla_compraventa}}', 'activo_biologico', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
