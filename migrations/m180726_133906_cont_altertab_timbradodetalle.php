<?php

use jamband\schemadump\Migration;

class m180726_133906_cont_altertab_timbradodetalle extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_timbrado}}', 'fecha_fin', $this->date()->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
