<?php

use jamband\schemadump\Migration;

class m190604_182720_cont_addcol_obligaciones extends Migration
{
    public function safeUp()
    {
        $table = 'cont_obligacion';
        $this->addColumn($table, 'para_factura', "ENUM('si', 'no') NOT NULL DEFAULT 'si'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
