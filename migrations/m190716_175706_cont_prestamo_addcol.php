<?php

use jamband\schemadump\Migration;

class m190716_175706_cont_prestamo_addcol extends Migration
{
    public function safeUp()
    {
        $table = 'cont_prestamo';
        $this->addColumn($table, 'nro_prestamo', $this->string(20)->null()->after('id'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
