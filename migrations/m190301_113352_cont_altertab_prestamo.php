<?php

use jamband\schemadump\Migration;

class m190301_113352_cont_altertab_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_prestamo', 'concepto_devengamiento_cuota', $this->string(100)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
