<?php

use jamband\schemadump\Migration;

class m181015_133604_cont_compraDet_addcol extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_compra_detalle}}', 'agrupar', "ENUM('si', 'no') NOT NULL DEFAULT 'si'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
