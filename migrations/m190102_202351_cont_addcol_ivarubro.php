<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190102_202351_cont_addcol_ivarubro extends Migration
{
    public function safeUp()
    {
        $tableName = 'cont_presentacion_iva_rubro';
        $tableEmpresa = 'core_empresa';
        $tablePeriodo = 'cont_empresa_periodo_contable';
        $table = "{{%{$tableName}}}";
        $fk = "fk_ivarubro_";

        $this->addColumn($table, 'empresa_id', $this->integer(10)->unsigned()->null());
        $this->addColumn($table, 'periodo_contable_id', $this->integer(11)->null());

        $this->addForeignKey("{$fk}empresa_id", $table, 'empresa_id', "{{%{$tableEmpresa}}}", 'id');
        $this->addForeignKey("{$fk}periodo_id", $table, 'periodo_contable_id', "{{%{$tablePeriodo}}}", 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
