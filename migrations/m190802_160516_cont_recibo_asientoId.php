<?php

use jamband\schemadump\Migration;

class m190802_160516_cont_recibo_asientoId extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_recibo', 'asiento_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_recibo_asiento', 'cont_recibo', 'asiento_id', 'cont_asiento', 'id');
    }

    public function safeDown()
    {
        echo self::class . ' no puede ser revertido.\n';
        return false;
    }
}
