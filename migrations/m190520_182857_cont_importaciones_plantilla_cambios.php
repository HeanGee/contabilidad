<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190520_182857_cont_importaciones_plantilla_cambios extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('cont_plantilla_compraventa', 'para_importacion',
            'ENUM("no","local_otros_gastos","local_honorarios_despachante","local_tasas_portuarias","local_gastos_aduaneros",
            "local_resguardo_verificacion_previa","local_servicio_removido_mercancia","local_registro_ventanilla_unica_importacion",
            "local_cancelacion_expediente","local_fotocopia_despacho_documento","local_canon_sistema_sofia","local_flete","local_seguro",
            "exterior_flete", "exterior_seguro", "exterior_mercaderias", "exterior_descuento", "exterior_otros_gastos") NOT NULL DEFAULT "no"');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
