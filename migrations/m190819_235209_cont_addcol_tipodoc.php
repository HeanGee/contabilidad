<?php

use jamband\schemadump\Migration;

class m190819_235209_cont_addcol_tipodoc extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_tipo_documento', 'para', "ENUM('nota_credito', 'nota_debito', '') NULL DEFAULT NULL");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
