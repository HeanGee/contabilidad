<?php

use yii\db\Schema;

class m180701_150101_abm_activo_fijo extends \yii\db\Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        // cont_activo_fijo
        $this->createTable('{{%cont_activo_fijo}}', [
            'id' => $this->primaryKey()->unsigned(),
            'activo_fijo_tipo_id' => $this->integer(10)->unsigned()->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'empresa_periodo_contable_id' => $this->integer(11)->notNull(),
            'nombre' => $this->string(100)->notNull(),
            'costo_adquisicion' => $this->decimal(14, 2)->notNull(),
            'fecha_adquisicion' => $this->date()->notNull(),
            'coeficiente_revaluo' => $this->decimal(8, 6)->null()->comment('- coeficiente de revaluo (segun tipo de activo fijo)'),
            'vida_util_fiscal' => (new yii\db\ColumnSchemaBuilder('tinyint', 2, $this->db))->notNull()->comment('- vida util fiscal (segun tipo de activo fijo)'),
            'vida_util_restante' => (new yii\db\ColumnSchemaBuilder('tinyint', 2, $this->db))->notNull(),
            'valor_fiscal_neto' => $this->decimal(14, 2)->null()->comment('- valor fiscal neto del ejercicio anterior'),
            'valor_fiscal_revaluo' => $this->decimal(14, 2)->null(),
            'cuenta_id' => $this->integer(10)->unsigned()->notNull(),
        ], $tableOptions);

        // fk: cont_activo_fijo
        $this->addForeignKey('fk_cont_activo_fijo_activo_fijo_tipo_id', '{{%cont_activo_fijo}}', 'activo_fijo_tipo_id', '{{%cont_activo_fijo_tipo}}', 'id');
        $this->addForeignKey('fk_cont_activo_fijo_empresa_periodo_contable_id', '{{%cont_activo_fijo}}', 'empresa_periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_cont_activo_fijo_cuenta_id', '{{%cont_activo_fijo}}', 'cuenta_id', '{{%cont_plan_cuenta}}', 'id');
        $this->addForeignKey('fk_cont_activo_fijo_empresa_id', '{{%cont_activo_fijo}}', 'empresa_id', '{{%core_empresa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180701_150101_abm_activo_fijo no puede ser revertido.\n";
        return false;
    }
}
