<?php

use jamband\schemadump\Migration;

class m190207_161659_cont_autofactura_compra extends Migration
{
    public function safeUp()
    {
        $table = 'cont_factura_compra';
        $this->addColumn($table, 'cedula_autofactura', $this->string(15)->null());
        $this->addColumn($table, 'nombre_completo_autofactura', $this->string(128)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
