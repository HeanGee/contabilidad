<?php

use jamband\schemadump\Migration;

class m180726_123322_cont_altertab_recibo extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_recibo}}', 'numero', $this->string(15)->notNull());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
