<?php

use jamband\schemadump\Migration;

class m190724_160601_cont_recibo_addcol extends Migration
{
    public function safeUp()
    {
        $table = 'cont_recibo';
        $this->addColumn($table, 'total', $this->decimal(14, 2));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}