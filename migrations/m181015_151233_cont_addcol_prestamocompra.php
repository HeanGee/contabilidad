<?php

use jamband\schemadump\Migration;

class m181015_151233_cont_addcol_prestamocompra extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo_detalle_compra}}', 'capital', $this->decimal(14, 2)->notNull());
        $this->addColumn('{{%cont_prestamo_detalle_compra}}', 'interes', $this->decimal(14, 2)->notNull());
        $this->addColumn('{{%cont_prestamo_detalle_compra}}', 'monto_cuota', $this->decimal(14, 2)->notNull());

    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
