<?php

use jamband\schemadump\Migration;

class m180730_191637_cont_addcolumn_pc_recibo_cntracta extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_recibo_detalle_contracuentas}}', 'empresa_id', $this->integer(10)->unsigned()->null());
        $this->addColumn('{{%cont_recibo_detalle_contracuentas}}', 'periodo_contable_id', $this->integer(11)->null());

        $this->addForeignKey('fk_recibodetallecontracuentas_empresa_empresa_id', '{{%cont_recibo_detalle_contracuentas}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_recibodetallecontracuentas_pc_pc_id', '{{%cont_recibo_detalle_contracuentas}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
