<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180515_122011_cont_add_columns_ivacuenta extends Migration
{
    public function safeUp()
    {
        // Ejecutar primero el dropForeignKey() y luego dropIndex().
        $this->dropForeignKey('fk_cont_iva_cuenta_plan_cuenta_id', '{{%cont_iva_cuenta}}');
        $this->dropIndex('fk_cont_iva_cuenta_plan_cuenta_id', '{{%cont_iva_cuenta}}');
        $this->renameColumn('{{%cont_iva_cuenta}}', 'plan_cuenta_id', 'contracuenta1_id');
        // Como puede haber ya filas insertadas, al establecer notNull() inserta 0, eso hace que no se pueda agregar constraints.
        $this->addColumn('{{%cont_iva_cuenta}}', 'contracuenta2_id', $this->integer(10)->unsigned()->null());
        $this->addColumn('{{%cont_iva_cuenta}}', 'operacion', "ENUM('compra','venta')");
//        $this->addColumn('{{%cont_iva_cuenta}}', 'cuenta_out1_compra_id', $this->integer(10)->unsigned()->null());
//        $this->addColumn('{{%cont_iva_cuenta}}', 'cuenta_out2_compra_id', $this->integer(10)->unsigned()->null());

        $this->addForeignKey('fk_cont_iva_cuenta_contracuenta1id', '{{%cont_iva_cuenta}}', 'contracuenta1_id', '{{%cont_plan_cuenta}}', 'id');
        $this->addForeignKey('fk_cont_iva_cuenta_contracuenta2_id', '{{%cont_iva_cuenta}}', 'contracuenta2_id', '{{%cont_plan_cuenta}}', 'id');
//        $this->addForeignKey('fk_cont_iva_cuenta_cuenta_out1_compra_id', '{{%cont_iva_cuenta}}', 'cuenta_out1_compra_id', '{{%cont_plan_cuenta}}', 'id');
//        $this->addForeignKey('fk_cont_iva_cuenta_cuenta_out2_compra_id', '{{%cont_iva_cuenta}}', 'cuenta_out2_compra_id', '{{%cont_plan_cuenta}}', 'id');

        //$this->update('{{%cont_iva_cuenta}}', ['contracuenta2_id'=>\backend\modules\contabilidad\models\PlanCuenta::findOne(['nombre'=>'CAJA'])->id]);
//        $this->update('{{%cont_iva_cuenta}}', ['cuenta_out1_compra_id'=>\backend\modules\contabilidad\models\PlanCuenta::findOne(['nombre'=>'CAJA'])->id]);
//        $this->update('{{%cont_iva_cuenta}}', ['cuenta_out2_compra_id'=>\backend\modules\contabilidad\models\PlanCuenta::findOne(['nombre'=>'CAJA'])->id]);

//        $this->alterColumn('{{%cont_iva_cuenta}}', 'cuenta_out2_venta_id', $this->integer(10)->unsigned()->notNull());
//        $this->alterColumn('{{%cont_iva_cuenta}}', 'cuenta_out1_compra_id', $this->integer(10)->unsigned()->notNull());
//        $this->alterColumn('{{%cont_iva_cuenta}}', 'cuenta_out2_compra_id', $this->integer(10)->unsigned()->notNull());
    }

    public function safeDown()
    {
        echo "m180515_122011_cont_add_columns_ivacuenta no puede ser revertido.\n";
        return false;
    }
}
