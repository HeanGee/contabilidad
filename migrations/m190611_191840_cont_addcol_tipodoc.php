<?php

use jamband\schemadump\Migration;

class m190611_191840_cont_addcol_tipodoc extends Migration
{
    public function safeUp()
    {
        $tab = 'cont_tipo_documento';
        $this->addColumn($tab, 'concepto_asiento_independiente', $this->string(128)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
