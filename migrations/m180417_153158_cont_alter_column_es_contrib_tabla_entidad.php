<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180417_153158_cont_alter_column_es_contrib_tabla_entidad extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_entidad}}', 'es_contribuyente', 'enum(\'Si\', \'No\')');
    }

    public function safeDown()
    {
        echo "m180417_153158_cont_alter_column_es_contrib_tabla_entidad no puede ser revertido.\n";
        return false;
    }
}
