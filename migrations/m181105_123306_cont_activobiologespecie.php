<?php

use jamband\schemadump\Migration;

class m181105_123306_cont_activobiologespecie extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_activo_biologico_especie}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull(),
            'descripcion' => $this->string(256)->null(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_actbioesp_empresa', '{{%cont_activo_biologico_especie}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_actbioesp_periodocont', '{{%cont_activo_biologico_especie}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->createTable('{{%cont_activo_biologico_especie_clasificacion}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull(),
            'descripcion' => $this->string(256)->null(),
            'activo_biologico_especie_id' => $this->integer(10)->unsigned(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_actbioespcla_empresa', '{{%cont_activo_biologico_especie_clasificacion}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_actbioespcla_periodocont', '{{%cont_activo_biologico_especie_clasificacion}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_actbioespcla_actbioespid', '{{%cont_activo_biologico_especie_clasificacion}}', 'activo_biologico_especie_id', '{{%cont_activo_biologico_especie}}', 'id');

        $this->renameTable('{{%cont_activo_ganado}}', '{{%cont_activo_biologico}}');
        $this->addColumn('{{%cont_activo_biologico}}', 'sexo', "ENUM('hembra', 'macho', 'generico') NOT NULL DEFAULT 'generico'");
        $this->renameColumn('{{%cont_activo_biologico}}', 'tipo', 'clasificacion_id');
        $this->alterColumn('{{%cont_activo_biologico}}', 'clasificacion_id', $this->integer(10)->unsigned()->null());
        $this->addColumn('{{%cont_activo_biologico}}', 'especie_id', $this->integer(10)->unsigned()->notNull()->after('periodo_contable_id'));

        $this->addForeignKey('fk_actbio_especie_clasi', '{{%cont_activo_biologico}}', 'clasificacion_id', '{{%cont_activo_biologico_especie_clasificacion}}', 'id');
        $this->addForeignKey('fk_actbio_especie', '{{%cont_activo_biologico}}', 'especie_id', '{{%cont_activo_biologico_especie}}', 'id');

        $this->renameColumn('{{%cont_activo_biologico}}', 'activo_ganado_padre_id', 'activo_biologico_padre_id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
