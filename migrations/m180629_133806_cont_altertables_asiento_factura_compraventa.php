<?php

use jamband\schemadump\Migration;

class m180629_133806_cont_altertables_asiento_factura_compraventa extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_venta}}', 'asiento_id', $this->integer(10)->unsigned()->null());
        $this->addColumn('{{%cont_factura_compra}}', 'asiento_id', $this->integer(10)->unsigned()->null());
        $this->addColumn('{{%cont_asiento}}', 'asiento_id', $this->integer(10)->unsigned()->null());


        $this->addForeignKey('fk_cont_factura_venta_asiento_id', '{{%cont_factura_venta}}', 'asiento_id', '{{%cont_asiento}}', 'id');
        $this->addForeignKey('fk_cont_factura_compra_asiento_id', '{{%cont_factura_compra}}', 'asiento_id', '{{%cont_asiento}}', 'id');
        $this->addForeignKey('fk_cont_asiento_asiento_id', '{{%cont_asiento}}', 'asiento_id', '{{%cont_asiento}}', 'id');
    }

    public function safeDown()
    {
        echo "m180629_133806_cont_altertables_asiento_factura_compraventa no puede ser revertido.\n";
        return false;
    }
}
