<?php

use jamband\schemadump\Migration;

class m181011_115517_cont_prestamodet_saldos extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo_detalle}}', 'capital_saldo', $this->decimal(14, 2)->notNull()->after('capital'));
        $this->addColumn('{{%cont_prestamo_detalle}}', 'interes_saldo', $this->decimal(14, 2)->notNull()->after('interes'));
        $this->addColumn('{{%cont_prestamo_detalle}}', 'monto_cuota_saldo', $this->decimal(14, 2)->notNull()->after('monto_cuota'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
