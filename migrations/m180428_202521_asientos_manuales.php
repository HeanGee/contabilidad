<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180428_202521_asientos_manuales extends Migration
{
    public function safeUp()
    {
        // cont_asiento
        $this->createTable('{{%cont_asiento}}', [
            'id' => $this->primaryKey()->unsigned(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
            'fecha' => $this->date()->notNull(),
            'monto_debe' => $this->integer(11)->unsigned()->notNull(),
            'monto_haber' => $this->integer(11)->unsigned()->notNull(),
            'concepto' => $this->text()->null(),
            'usuario_id' => $this->integer(10)->unsigned()->notNull(),
            'creado' => $this->datetime()->notNull(),
            'modulo_origen' => "ENUM ('contabilidad') NOT NULL",
        ], $this->tableOptions);

        // cont_asiento_detalle
        $this->createTable('{{%cont_asiento_detalle}}', [
            'id' => $this->primaryKey()->unsigned(),
            'asiento_id' => $this->integer(11)->unsigned()->notNull(),
            'cuenta_id' => $this->integer(10)->unsigned()->notNull(),
            'monto_debe' => $this->integer(11)->unsigned()->null(),
            'monto_haber' => $this->integer(11)->unsigned()->null(),
        ], $this->tableOptions);

        // fk: cont_asiento
        $this->addForeignKey('fk_cont_asiento_periodo_contable_id', '{{%cont_asiento}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
        $this->addForeignKey('fk_cont_asiento_empresa_id', '{{%cont_asiento}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_cont_asiento_usuario_id', '{{%cont_asiento}}', 'usuario_id', '{{%core_usuario}}', 'id');

        // fk: cont_asiento_detalle
        $this->addForeignKey('fk_cont_asiento_detalle_asiento_id', '{{%cont_asiento_detalle}}', 'asiento_id', '{{%cont_asiento}}', 'id');
        $this->addForeignKey('fk_cont_asiento_detalle_cuenta_id', '{{%cont_asiento_detalle}}', 'cuenta_id', '{{%cont_plan_cuenta}}', 'id');

    }

    public function safeDown()
    {
        echo 'm180428_202521_asientos_manuales no puede ser revertido.\n';
        return false;
    }
}
