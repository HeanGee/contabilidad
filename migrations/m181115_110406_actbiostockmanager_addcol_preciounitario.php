<?php

use jamband\schemadump\Migration;

class m181115_110406_actbiostockmanager_addcol_preciounitario extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_biologico_stock_manager}}', 'precio_unitario',
            $this->integer(11)->unsigned()->notNull()->after('cantidad'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
