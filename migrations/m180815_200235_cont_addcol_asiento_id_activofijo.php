<?php

use jamband\schemadump\Migration;

class m180815_200235_cont_addcol_asiento_id_activofijo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_fijo}}', 'asiento_id', $this->integer(11)->unsigned()->null());
        $this->addForeignKey('fk_asiento_id_activo_fijo', '{{%cont_activo_fijo}}', 'asiento_id', '{{%cont_asiento}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . " no puede ser revertido\n";
        return false;
    }
}
