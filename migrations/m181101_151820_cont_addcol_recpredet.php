<?php

use jamband\schemadump\Migration;

class m181101_151820_cont_addcol_recpredet extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_recibo_prestamo_detalle}}', 'asiento_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_recibopredet_asiento', '{{%cont_recibo_prestamo_detalle}}', 'asiento_id', '{{%cont_asiento}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
