<?php

use jamband\schemadump\Migration;

class m181023_112721_cont_addcol_plantillacv extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_plantilla_compraventa}}', 'para_cuota_prestamo', "ENUM('si', 'no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
