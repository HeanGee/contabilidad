<?php

use jamband\schemadump\Migration;

class m180807_150911_cont_poliza_devengamiento extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_poliza_devengamiento}}', [
            'id' => $this->primaryKey()->unsigned(),
            'poliza_id' => $this->integer(10)->unsigned()->notNull(),
            'asiento_id' => $this->integer(10)->unsigned()->null(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
            'dias' => $this->integer(3)->notNull(),
            'monto' => $this->decimal(14, 4),
            'aho_mes' => $this->string(7)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_poliza_dev_poliza_id', '{{%cont_poliza_devengamiento}}', 'poliza_id', '{{%cont_poliza}}', 'id');
        $this->addForeignKey('fk_poliza_dev_asiento_id', '{{%cont_poliza_devengamiento}}', 'asiento_id', '{{%cont_asiento}}', 'id');
        $this->addForeignKey('fk_poliza_pc_id', '{{%cont_poliza_devengamiento}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
