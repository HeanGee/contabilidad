<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180502_123513_cont_abm_ventas extends Migration
{
    public function safeUp()
    {
        // cont_factura_venta
        $this->createTable('{{%cont_factura_venta}}', [
            'id' => $this->primaryKey(),
            'tipo_documento_id' => $this->integer(10)->unsigned()->notNull(),
            'fecha_emision' => $this->date()->notNull(),
            'condicion' => $this->string(7)->notNull(),
            'fecha_vencimiento' => $this->date()->null(),
            'entidad_id' => $this->integer(11)->notNull(),
            'obligaciones' => $this->integer(11)->null(),
            'total' => $this->decimal(14,2)->notNull(),
            'nro_factura' => $this->string(15)->notNull(),
            'moneda_id' => $this->integer(11)->notNull(),
            'cant_cuotas' => $this->integer(11)->null(),
            'valor_moneda' => $this->decimal(7,2)->null(),
        ], $this->tableOptions);

        // cont_detalle_factura_venta
        $this->createTable('{{%cont_detalle_factura_venta}}', [
            'id' => $this->primaryKey(),
            'factura_venta_id' => $this->integer(11)->notNull(),
            'iva_id' => $this->integer(10)->unsigned()->notNull(),
            'subtotal' => $this->decimal(14,2)->notNull(),
            'plan_cuenta_id' => $this->integer(10)->unsigned()->notNull(),
            'gravada' => $this->decimal(14,2)->notNull(),
            'impuesto' => $this->decimal(14,2)->notNull(),
            'cta_contable' => "ENUM('debe','haber') NOT NULL",
        ], $this->tableOptions);

        // cont_cuota_factura_venta
        $this->createTable('{{%cont_cuota_factura_venta}}', [
            'id' => $this->primaryKey(),
            'factura_venta_id' => $this->integer(11)->notNull(),
            'fecha_vencimiento' => $this->date()->notNull(),
            'monto' => $this->decimal(14,2)->notNull(),
            'nro_cuota' => $this->integer(11)->notNull(),
            'pagado' => "ENUM ('si', 'no') NOT NULL",
        ], $this->tableOptions);

        // fk: cont_cuota_factura_venta
        $this->addForeignKey('fk_cont_cuota_factura_venta_factura_venta_id', '{{%cont_cuota_factura_venta}}', 'factura_venta_id', '{{%cont_factura_venta}}', 'id');

        // fk: cont_detalle_factura_venta
        $this->addForeignKey('fk_cont_detalle_factura_venta_factura_venta_id', '{{%cont_detalle_factura_venta}}', 'factura_venta_id', '{{%cont_factura_venta}}', 'id');
        $this->addForeignKey('fk_cont_detalle_factura_venta_iva_id', '{{%cont_detalle_factura_venta}}', 'iva_id', '{{%core_iva}}', 'id');
        $this->addForeignKey('fk_cont_detalle_factura_venta_plan_cuenta_id', '{{%cont_detalle_factura_venta}}', 'plan_cuenta_id', '{{%cont_plan_cuenta}}', 'id');

        // fk: cont_factura_venta
        $this->addForeignKey('fk_cont_factura_venta_moneda_id', '{{%cont_factura_venta}}', 'moneda_id', '{{%core_moneda}}', 'id');
        $this->addForeignKey('fk_cont_factura_venta_entidad_id', '{{%cont_factura_venta}}', 'entidad_id', '{{%cont_entidad}}', 'id');
        $this->addForeignKey('fk_cont_factura_venta_tipo_documento_id', '{{%cont_factura_venta}}', 'tipo_documento_id', '{{%cont_tipo_documento}}', 'id');

    }

    public function safeDown()
    {
        echo "m180502_123513_cont_abm_ventas no puede ser revertido.\n";
        return false;
    }
}
