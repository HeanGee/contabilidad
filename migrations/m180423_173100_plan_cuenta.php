<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180423_173100_plan_cuenta extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_plan_cuenta}}', [
            'id' => $this->primaryKey()->unsigned(),
            'cod_cuenta' => $this->string(45)->notNull(),
            'nombre' => $this->string(255)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->null(),
            'asentable' => "ENUM ('si', 'no') NOT NULL",
            'padre_id' => $this->integer(11)->unsigned()->null(),
            'cod_completo' => $this->string(45)->notNull(),
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL",
        ], $this->tableOptions);
        
        $this->addForeignKey('fk_cont_plan_cuenta_padre_id', '{{%cont_plan_cuenta}}', 'padre_id', '{{%cont_plan_cuenta}}', 'id');
        $this->addForeignKey('fk_cont_plan_cuenta_empresa_id', '{{%cont_plan_cuenta}}', 'empresa_id', '{{%core_empresa}}', 'id');
        
        $this->execute('CREATE UNIQUE INDEX cod_completo_empresa_UNIQUE ON cont_plan_cuenta (cod_completo, empresa_id);');
    }

    public function safeDown()
    {
        echo 'm180423_173100_plan_cuenta no puede ser revertido.\n';
        return false;
    }
}
