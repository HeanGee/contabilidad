<?php

use jamband\schemadump\Migration;

class m181023_170811_cont_addcol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo}}', 'cuenta_id', $this->integer(10)->unsigned()->null());
        $this->addForeignKey('fk_prestamo_plan_cta', '{{%cont_prestamo}}', 'cuenta_id', '{{%cont_plan_cuenta}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
