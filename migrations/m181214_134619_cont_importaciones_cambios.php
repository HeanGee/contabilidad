<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181214_134619_cont_importaciones_cambios extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_importacion_detalle_factura_local_exterior', 'plantilla_id', $this->integer(10)->unsigned()->notNull());
        $this->addForeignKey('fk_cont_importacion_detalle_factura_loc_ext_plantilla_id', '{{%cont_importacion_detalle_factura_local_exterior}}', 'plantilla_id', '{{%cont_plantilla_compraventa}}', 'id');
        $this->addColumn('cont_importacion_detalle_factura_local_exterior', 'absorber_impuesto', $this->tinyInteger(1)->notNull()->defaultValue(0));

        $this->addColumn('cont_importacion', 'iva', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'indi', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'derecho_aduanero', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'servicio_valoracion', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'iracis_generaral', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'tasa_interv_aduanera', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'repos_gtos_adm_annp', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'cdap_comis_can_annp', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'sofia_comis_can_annp', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'cannon_informatico_sistema_sofia', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'multas_varias', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'impuesto_selectivo_consumo', $this->decimal(14, 2)->null());
        $this->addColumn('cont_importacion', 'arancel_consular', $this->decimal(14, 2)->null());

        $this->alterColumn('cont_plantilla_compraventa', 'para_importacion',
            'ENUM("no","local_otros_gastos","local_honorarios_despachante","local_tasas_portuarias","local_gastos_aduaneros",
            "local_resguardo_verificacion_previa","local_servicio_removido_mercancia","local_registro_ventanilla_unica_importacion",
            "local_cancelacion_expediente","local_fotocopia_despacho_documento","local_canon_sistema_sofia","local_flete","local_seguro",
            "exterior_flete", "exterior_seguro", "exterior_mercaderias") NOT NULL DEFAULT "no"');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
