<?php

use jamband\schemadump\Migration;

class m181009_125005_cont_dropcol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%cont_prestamo}}', 'gastos_no_deducibles');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
