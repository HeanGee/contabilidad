<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180508_121239_cont_parametro_modulo extends Migration
{
    public function safeUp()
    {
        $this->insert('cont_parametro_sistema', ['nombre'=>'ctas_in_venta_merca_diez', 'valor'=>'1.01.01.02']);
        $this->insert('cont_parametro_sistema', ['nombre'=>'ctas_out_venta_merca_diez', 'valor'=>'4.01.01|1.01.04.01.01']);
    }

    public function safeDown()
    {
        echo "m180508_121239_cont_parametro_modulo no puede ser revertido.\n";
        return false;
    }
}
