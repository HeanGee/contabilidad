<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180515_165442_cont_borrar_columnas_compradetalle extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%cont_detalle_factura_compra}}', 'gravada');
        $this->dropColumn('{{%cont_detalle_factura_compra}}', 'impuesto');
        $this->dropForeignKey('fk_cont_detalle_factura_compra_iva_id', '{{%cont_detalle_factura_compra}}');
        $this->dropIndex('fk_cont_detalle_factura_compra_iva_id', '{{%cont_detalle_factura_compra}}');
        $this->dropColumn('{{%cont_detalle_factura_compra}}', 'iva_id');
    }

    public function safeDown()
    {
        echo "m180515_165442_cont_borrar_columnas_compradetalle no puede ser revertido.\n";
        return false;
    }
}
