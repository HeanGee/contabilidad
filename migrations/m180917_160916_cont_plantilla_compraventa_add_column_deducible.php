<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180917_160916_cont_plantilla_compraventa_add_column_deducible extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_plantilla_compraventa', 'deducible', 'ENUM("si","no") NOT NULL DEFAULT "si"');
    }

    public function safeDown()
    {
        echo " m180917_160916_cont_plantilla_compraventa_add_column_deducible no puede ser revertido.\n";
        return false;
    }
}
