<?php

use jamband\schemadump\Migration;

class m180904_173004_cont_altertab_coeficiente_revaluo extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_coeficiente_revaluo_detalle}}', 'coeficiente', $this->decimal(8, 7));
    }

    public function safeDown()
    {
        echo self::className() . ' No puede ser revertido.\n';
        return false;
    }
}
