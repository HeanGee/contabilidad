<?php

use jamband\schemadump\Migration;

class m190513_165316_cont_addcols_activofijo extends Migration
{
    public function safeUp()
    {
        $tab = 'cont_activo_fijo';
        $this->addColumn($tab, 'valor_fiscal_depreciado', $this->bigInteger(15)->notNull()->defaultValue(0));
        $this->addColumn($tab, 'valor_contable_depreciado', $this->bigInteger(15)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
