<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180520_001425_timbrado_final extends Migration
{
    public function safeUp()
    {
        $this->dropTable('{{%cont_timbrado}}');

        // cont_timbrado
        $this->createTable('{{%cont_timbrado}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nro_timbrado' => $this->integer(11)->unsigned()->notNull()->unique(),
            'entidad_id' => $this->integer(11)->notNull(),
            'fecha_inicio' => $this->date()->notNull(),
            'fecha_fin' => $this->date()->notNull(),
        ], $this->tableOptions);

        // cont_timbrado_detalle
        $this->createTable('{{%cont_timbrado_detalle}}', [
            'id' => $this->primaryKey()->unsigned(),
            'timbrado_id' => $this->integer(11)->unsigned()->notNull(),
            'tipo_documento_id' => $this->integer(10)->unsigned()->notNull(),
            'prefijo' => $this->string(45)->notNull(),
            'nro_inicio' => $this->integer(11)->unsigned()->notNull(),
            'nro_fin' => $this->integer(11)->unsigned()->notNull(),
            'nro_actual' => $this->integer(11)->unsigned()->notNull(),
        ], $this->tableOptions);

        // fk: cont_timbrado
        $this->addForeignKey('fk_cont_timbrado_entidad_id', '{{%cont_timbrado}}', 'entidad_id', '{{%cont_entidad}}', 'id');

        // fk: cont_timbrado_detalle
        $this->addForeignKey('fk_cont_timbrado_detalle_timbrado_id', '{{%cont_timbrado_detalle}}', 'timbrado_id', '{{%cont_timbrado}}', 'id');
        $this->addForeignKey('fk_cont_timbrado_detalle_tipo_documento_id', '{{%cont_timbrado_detalle}}', 'tipo_documento_id', '{{%cont_tipo_documento}}', 'id');

    }

    public function safeDown()
    {
        echo "m180520_001425_timbrado_final no puede ser revertido.\n";
        return false;
    }
}
