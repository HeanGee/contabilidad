<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181203_150918_cont_addcol_actbio_invent_cierre extends Migration
{
    public function safeUp()
    {
        $tableName = '{{%cont_activo_biologico_cierre_inventario}}';
        $this->addColumn($tableName, 'reclasificacion_entrada', $this->integer(14)->notNull()->defaultValue(0)->after('mortandad_porcentaje_gd'));
        $this->addColumn($tableName, 'reclasificacion_salida', $this->integer(14)->notNull()->defaultValue(0)->after('reclasificacion_entrada'));
        $this->addColumn($tableName, 'stock_final', $this->integer(14)->notNull()->defaultValue(0)->after('reclasificacion_salida'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
