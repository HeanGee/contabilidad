<?php

use jamband\schemadump\Migration;

class m190625_204611_addcol_tipodoc extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_tipo_documento', 'autofactura', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::class . ' no puede ser revertido.\n';
        return false;
    }
}
