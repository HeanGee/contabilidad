<?php

use jamband\schemadump\Migration;

class m181214_163500_cont_addcol_estado_actfijo extends Migration
{
    public function safeUp()
    {
        $tableName = 'cont_activo_fijo';
        $tableNameDecorated = "{{%{$tableName}}}";
        $this->addColumn($tableNameDecorated, 'estado', "ENUM('activo','vendido','devuelto') NOT NULL DEFAULT 'activo'");
        // activo: existe en el sistema y puede tener factura de compra asociada.
        // vendido: fue vendido y tiene asociado factura de venta.
        // devuelto: existe en el sistema, tiene factura de compra pero no de venta y se genera nota de credito.
        $sql = "UPDATE {$tableName} SET estado = 'vendido' WHERE factura_venta_id IS NOT NULL";
        $this->execute($sql);
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
