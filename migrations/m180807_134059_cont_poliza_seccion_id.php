<?php

use jamband\schemadump\Migration;

class m180807_134059_cont_poliza_seccion_id extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%cont_poliza}}', 'seccion', 'seccion_id');
        $this->alterColumn('{{%cont_poliza}}', 'seccion_id', $this->integer(10)->unsigned()->notNull());
        $this->addForeignKey('fk_poliza_seccion_id', '{{%cont_poliza}}', 'seccion_id', 'cont_poliza_seccion', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
