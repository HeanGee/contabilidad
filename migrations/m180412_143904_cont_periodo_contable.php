<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180412_143904_cont_periodo_contable extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_empresa_periodo_contable}}', [
            'id' => $this->primaryKey(),
            'empresa_id' => $this->integer(11)->unsigned()->notNull(),
            'anho' => $this->string(45)->notNull(),
        ]);

        // fk: cont_empresa_periodo_contable
        $this->addForeignKey(
            'fk_empresa_periodo_contable_empresa_id',
            '{{%cont_empresa_periodo_contable}}',
            'empresa_id',
            '{{%core_empresa}}',
            'id'
        );
    }

    public function safeDown()
    {
        echo 'm180412_143904_cont_periodo_contable no puede ser revertido.\n';
        return false;
    }
}
