<?php

use jamband\schemadump\Migration;

class m190502_143702_cont_table_estados_financieros extends Migration
{
    public function safeUp()
    {
        $table = 'cont_estado_financiero';
        $fk = 'fk_estado_financiero_';
        $this->createTable($table, [
            'id' => $this->primaryKey(10)->unsigned(),
            'rango' => $this->string(23)->notNull(),
            'contador' => $this->string(128)->notNull(),
            'ruc_contador' => $this->integer(12)->notNull(),
            'auditor' => $this->string(128)->notNull(),
            'ruc_auditor' => $this->integer(12)->notNull(),
            'datos_entidad' => $this->string(2048)->notNull(),
            'base_preparacion' => $this->string(2048)->notNull(),
            'politicas_contables' => $this->string(2048)->notNull(),
            'composicion_cuentas_patrimoniales' => $this->string(2048)->notNull(),
            'composicion_cuentas_resultado' => $this->string(2048)->notNull(),
            'detalle_rentas' => $this->string(2048)->notNull(),
            'contingencias' => $this->string(2048)->notNull(),
            'identificacion_partes_vinculadas' => $this->string(2048)->notNull(),
            'hechos_posteriores_relevantes' => $this->string(2048)->notNull(),
            'otras_notas' => $this->string(2048)->notNull(),
            'excel_file_name' => $this->string(128)->null(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey("{$fk}empresa_id", $table, 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey("{$fk}periodo_contable_id", $table, 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
