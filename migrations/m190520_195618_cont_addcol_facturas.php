<?php

use jamband\schemadump\Migration;

class m190520_195618_cont_addcol_facturas extends Migration
{
    public function safeUp()
    {
        $table = 'cont_factura_compra';
        $this->addColumn($table, 'creado_por', $this->string(20)->null());

        $table = 'cont_factura_venta';
        $this->addColumn($table, 'creado_por', $this->string(20)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
