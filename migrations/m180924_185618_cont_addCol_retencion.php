<?php

use jamband\schemadump\Migration;

class m180924_185618_cont_addCol_retencion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_retencion}}', 'timbrado_detalle_id', $this->integer(10)->unsigned()->null()->after('factura_venta_id'));
        $this->addForeignKey('fk_retencion_timbrado_d', '{{%cont_retencion}}', 'timbrado_detalle_id', '{{%cont_timbrado_detalle}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
