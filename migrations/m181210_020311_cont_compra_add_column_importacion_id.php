<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181210_020311_cont_compra_add_column_importacion_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_compra}}', 'importacion_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_factura_compra_importacion_id', '{{%cont_factura_compra}}', 'importacion_id', '{{%cont_importacion}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
