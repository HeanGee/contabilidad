<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190404_152323_cont_importacion_detalle_add_column_incluir_costo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_importacion_detalle_factura_local_exterior', 'incluir_costo', $this->tinyInteger(1)->notNull()->defaultValue(0));

    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
