<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180622_162812_compra_add_timbrado_detalle_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_compra}}', 'timbrado_detalle_id', $this->integer(10)->unsigned()->notNull());
        $this->addForeignKey('fk_cont_factura_compra_timbrado_detalle_id', '{{%cont_factura_compra}}', 'timbrado_detalle_id', '{{%cont_timbrado_detalle}}', 'id');
        $this->dropColumn('{{%cont_factura_compra}}', 'timbrado_id');
    }

    public function safeDown()
    {
        echo "m180622_162812_compra_add_timbrado_detalle_id no puede ser revertido.\n";
        return false;
    }
}
