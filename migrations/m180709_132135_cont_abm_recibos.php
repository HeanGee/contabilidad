<?php

use jamband\schemadump\Migration;

class m180709_132135_cont_abm_recibos extends Migration
{
    public function safeUp()
    {

        // cont_recibo
        $this->createTable('{{%cont_recibo}}', [
            'id' => $this->primaryKey()->unsigned(),
            'numero' => $this->string(7)->notNull(),
            'fecha' => $this->date()->notNull(),
            'entidad_id' => $this->integer(11)->notNull(),
            'moneda_id' => $this->integer(11)->notNull(),
            'valor_moneda' => $this->decimal(7, 2)->null(),
        ], $this->tableOptions);

        // cont_recibo_detalle
        $this->createTable('{{%cont_recibo_detalle}}', [
            'id' => $this->primaryKey()->unsigned(),
            'recibo_id' => $this->integer(10)->unsigned()->notNull(),
            'factura_compra_id' => $this->integer(11)->null(),
            'factura_venta_id' => $this->integer(11)->null(),
            'monto' => $this->decimal(14, 2)->notNull(),
        ], $this->tableOptions);

        // cont_recibo_detalle_contracuentas
        $this->createTable('{{%cont_recibo_detalle_contracuentas}}', [
            'id' => $this->primaryKey()->unsigned(),
            'recibo_id' => $this->integer(10)->unsigned()->notNull(),
            'plan_cuenta_id' => $this->integer(10)->unsigned()->notNull(),
            'monto' => $this->decimal(14, 2)->notNull(),
        ], $this->tableOptions);

        // fk: cont_recibo
        $this->addForeignKey('fk_cont_recibo_moneda_id', '{{%cont_recibo}}', 'moneda_id', '{{%core_moneda}}', 'id');

        // fk: cont_recibo_detalle
        $this->addForeignKey('fk_cont_recibo_detalle_factura_compra_id', '{{%cont_recibo_detalle}}', 'factura_compra_id', '{{%cont_factura_compra}}', 'id');
        $this->addForeignKey('fk_cont_recibo_detalle_factura_venta_id', '{{%cont_recibo_detalle}}', 'factura_venta_id', '{{%cont_factura_venta}}', 'id');
        $this->addForeignKey('fk_cont_recibo_detalle_recibo_id', '{{%cont_recibo_detalle}}', 'recibo_id', '{{%cont_recibo}}', 'id');

        // fk: cont_recibo_detalle_contracuentas
        $this->addForeignKey('fk_cont_recibo_detalle_contracuentas_plan_cuenta_id', '{{%cont_recibo_detalle_contracuentas}}', 'plan_cuenta_id', '{{%cont_plan_cuenta}}', 'id');
        $this->addForeignKey('fk_cont_recibo_detalle_contracuentas_recibo_id', '{{%cont_recibo_detalle_contracuentas}}', 'recibo_id', '{{%cont_recibo}}', 'id');

    }

    public function safeDown()
    {
    }
}
