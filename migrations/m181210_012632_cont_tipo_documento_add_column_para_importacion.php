<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181210_012632_cont_tipo_documento_add_column_para_importacion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_tipo_documento', 'para_importacion', 'ENUM("si","no") NOT NULL DEFAULT "no"');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
