<?php

use jamband\schemadump\Migration;

class m180928_140345_cont_addcol_poliza extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_poliza}}', 'importe_gravado', $this->decimal(14, 2)->null()->after('fecha_hasta'));
        $this->addCommentOnColumn('{{%cont_poliza}}', 'importe_gravado', "Necesario para poder regenerar el importe diario, sin tener que recurrir al detalle de la compra asociada.");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
