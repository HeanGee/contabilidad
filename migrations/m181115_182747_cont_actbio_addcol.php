<?php

use jamband\schemadump\Migration;

class m181115_182747_cont_actbio_addcol extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_activo_biologico}}', 'precio_unitario_actual', $this->integer(11)->unsigned()->notNull()->after('precio_unitario'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
