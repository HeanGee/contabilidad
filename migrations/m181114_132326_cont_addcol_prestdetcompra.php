<?php

use jamband\schemadump\Migration;

class m181114_132326_cont_addcol_prestdetcompra extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo_detalle_compra}}', 'recibo_capital_nro', $this->string(45)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
