<?php

use jamband\schemadump\Migration;

class m181002_165842_cont_alttab_prestamo extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%cont_prestamo}}', 'iva_10', 'usar_iva_10');
        $this->addColumn('{{%cont_prestamo}}', 'iva_10', $this->decimal(14, 2)->null()->after('gastos_no_deducibles'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
