<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180525_003059_cont_altertable_timbrado extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_timbrado_detalle}}', 'nro_actual', $this->integer(11)->unsigned()->null());
    }

    public function safeDown()
    {
        echo "m180525_003059_cont_altertable_timbrado no puede ser revertido.\n";
        return false;
    }
}
