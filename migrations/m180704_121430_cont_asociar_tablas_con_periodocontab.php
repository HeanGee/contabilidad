<?php

use jamband\schemadump\Migration;

class m180704_121430_cont_asociar_tablas_con_periodocontab extends Migration
{
    public function safeUp()
    {

//        $this->addColumn('{{%cont_activo_fijo}}', 'periodo_contable_id', $this->integer(11)->null());
//        $this->addForeignKey('fk_cont_activo_fijo_periodo_contable_id', '{{%cont_activo_fijo}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->addColumn('{{%cont_asiento_detalle}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_asiento_detalle_periodo_contable_id', '{{%cont_asiento_detalle}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->addColumn('{{%cont_cuota_factura_venta}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_cuota_factura_venta_periodo_contable_id', '{{%cont_cuota_factura_venta}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->addColumn('{{%cont_factura_compra}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_factura_compra_periodo_contable_id', '{{%cont_factura_compra}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->addColumn('{{%cont_factura_compra_detalle}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_factura_compra_detalle_periodo_contable_id', '{{%cont_factura_compra_detalle}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->addColumn('{{%cont_factura_compra_iva_cuenta_usada}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_fac_compra_iva_cta_usada_periodo_contable_id', '{{%cont_factura_compra_iva_cuenta_usada}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->addColumn('{{%cont_factura_venta}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_factura_venta_periodo_contable_id', '{{%cont_factura_venta}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->addColumn('{{%cont_factura_venta_detalle}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_factura_venta_detalle_periodo_contable_id', '{{%cont_factura_venta_detalle}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

        $this->addColumn('{{%cont_factura_venta_iva_cuenta_usada}}', 'periodo_contable_id', $this->integer(11)->null());
        $this->addForeignKey('fk_cont_fac_venta_iva_cta_usada_periodo_contable_id', '{{%cont_factura_venta_iva_cuenta_usada}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
    }

    public function safeDown()
    {
        echo 'm180704_121430_cont_asociar_tablas_con_periodocontab no puede ser revertido.\n';
        return false;
    }
}
