    <?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180613_234532_cont_altertab_ventas_estados extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_venta}}', 'estado', "ENUM ('vigente', 'anulada', 'no_declarada') DEFAULT NULL DEFAULT 'vigente'");
    }

    public function safeDown()
    {
        echo "m180613_234532_cont_altertab_ventas_estados no puede ser revertido.\n";
        return false;
    }
}
