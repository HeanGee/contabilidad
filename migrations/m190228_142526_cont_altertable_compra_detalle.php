<?php

use jamband\schemadump\Migration;

class m190228_142526_cont_altertable_compra_detalle extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_factura_compra_detalle', 'prestamo_cuota_id', $this->integer(10)->unsigned()->null());
        // Esta columna va a permitir diferenciar entre detalles por cuota de prestamo y detalles por plantillas normales.
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
