<?php

use jamband\schemadump\Migration;

class m190422_123920_cont_change_to_bigint_debehaber_asiento extends Migration
{
    public function safeUp()
    {
        # sin unsigned porque al crear manualmente ellos crean algunas cuentas con valores negativos.

        $this->alterColumn('cont_asiento', 'monto_debe', $this->bigInteger(18)->notNull());
        $this->alterColumn('cont_asiento', 'monto_haber', $this->bigInteger(18)->notNull());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
