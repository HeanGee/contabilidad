<?php

use jamband\schemadump\Migration;

class m180806_001840_cont_alttab_poliza extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_poliza}}', 'importe_diario', $this->decimal(14, 2)->notNull());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
