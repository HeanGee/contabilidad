<?php

use jamband\schemadump\Migration;

class m181001_160000_cont_addcol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo}}', 'caja', $this->decimal(14, 2)->unsigned()->notNull()->after('interes'));
        $this->addColumn('{{%cont_prestamo}}', 'gastos_bancarios', $this->decimal(14, 2)->unsigned()->notNull()->after('caja'));
        $this->addColumn('{{%cont_prestamo}}', 'intereses_vencer', $this->decimal(14, 2)->unsigned()->null()->after('gastos_bancarios'));
        $this->addColumn('{{%cont_prestamo}}', 'gastos_no_deducibles', $this->decimal(14, 2)->unsigned()->notNull()->after('intereses_vencer'));
        $this->addColumn('{{%cont_prestamo}}', 'iva_10', "ENUM('si', 'no') NOT NULL DEFAULT 'no'");
        $this->dropColumn('{{%cont_prestamo}}', 'interes');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
