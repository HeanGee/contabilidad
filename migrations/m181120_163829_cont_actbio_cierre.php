<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181120_163829_cont_actbio_cierre extends Migration
{
    public function safeUp()
    {
        $table_name = "{{%cont_activo_biologico_cierre_inventario}}";
        $fk_prefix = "fk_actbio_cierreinv_";

        $this->createTable($table_name, [
            'id' => $this->primaryKey()->unsigned(),
            'fecha' => $this->string(11)->notNull(),
            'especie_id' => $this->integer(10)->unsigned()->notNull(),
            'clasificacion_id' => $this->integer(10)->unsigned()->notNull(),
            'precio_foro' => $this->integer(11)->unsigned()->notNull()->comment('Precio establecido arbitrariamente y aceptado consensuadamente entre los productores de ganado.'),
            'procreo_cantidad' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad de nacimientos.'),
            'compra_cantidad' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad comprada'),
            'venta_cantidad' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad vendida.'),
            'venta_cantidad_gd' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad vendida GD'),
            'venta_cantidad_gnd' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad vendida GND'),
            'consumo_cantidad' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad consumida (normalmente 1%).'),
            'consumo_cantidad_gd' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad consumida GD.'),
            'consumo_cantidad_gnd' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad consumida GND.'),
            'mortandad_cantidad' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad de muertes (normalmente 3%).'),
            'mortandad_cantidad_gd' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad de muertes GD.'),
            'mortandad_cantidad_gnd' => $this->integer(11)->unsigned()->notNull()->comment('Cantidad de muertes GND.'),
            'consumo_porcentaje_gd' => $this->decimal(3, 2)->notNull()->defaultValue(1.00)->comment('Porcentaje de Consumo considerado para GD. En numeros enteros. Normalmente 1%.'),
            'mortandad_porcentaje_gd' => $this->decimal(3, 2)->notNull()->defaultValue(3.00)->comment('Porcentaje de Muerte considerado para GD En numeros enteros. Normalmente 3%.'),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
            'periodo_contable_id' => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey("{$fk_prefix}especie_id", $table_name, 'especie_id', "{{%cont_activo_biologico_especie}}", "id");
        $this->addForeignKey("{$fk_prefix}clasificacion_id", $table_name, 'clasificacion_id', "{{%cont_activo_biologico_especie_clasificacion}}", "id");
        $this->addForeignKey("{$fk_prefix}empresa_id", $table_name, 'empresa_id', "{{%core_empresa}}", "id");
        $this->addForeignKey("{$fk_prefix}periodo_contable_id", $table_name, 'periodo_contable_id', "{{%cont_empresa_periodo_contable}}", "id");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
