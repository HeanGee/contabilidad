<?php

use jamband\schemadump\Migration;

class m180919_131634_cont_altertab_coef_revaluo extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%cont_coeficiente_revaluo}}', 'existencia', 'enero_existencia');
        $this->renameColumn('{{%cont_coeficiente_revaluo}}', 'enajenacion', 'enero_enajenacion');
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'febrero_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'febrero_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'marzo_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'marzo_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'abril_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'abril_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'mayo_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'mayo_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'junio_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'junio_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'julio_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'julio_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'agosto_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'agosto_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'septiembre_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'septiembre_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'octubre_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'octubre_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'noviembre_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'noviembre_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'diciembre_existencia', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'diciembre_enajenacion', $this->decimal(8, 7));
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'empresa_id', $this->integer(10)->unsigned()->Null());
        $this->addColumn('{{%cont_coeficiente_revaluo}}', 'periodo_contable_id', $this->integer(11)->Null());

        $this->addForeignKey('fk_cont_coeficiente_revaluo_empresa_id', '{{%cont_coeficiente_revaluo}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_coeficiente_revaluo_periodo_contable_id', '{{%cont_coeficiente_revaluo}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');

    }

    public function safeDown()
    {
        echo 'm180919_131634_cont_altertab_coef_revaluo no puede ser revertido.\n';
        return false;
    }
}
