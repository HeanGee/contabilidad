<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180627_021633_add_tipo_documento_set_timbrado_detalle_tipo_doc extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_cont_timbrado_detalle_tipo_documento_id', '{{%cont_timbrado_detalle}}');
        $this->dropIndex('fk_cont_timbrado_detalle_tipo_documento_id', '{{%cont_timbrado_detalle}}');
        $this->dropColumn('{{%cont_timbrado_detalle}}', 'tipo_documento_id');

        //Por los datos que ya hay, no se puede poner not null.
        $this->addColumn('{{%cont_timbrado_detalle}}', 'tipo_documento_set_id', $this->integer(10)->unsigned()->notNull());
        $this->addForeignKey('fk_cont_timbrado_detalle_tipo_documento_set_id', '{{%cont_timbrado_detalle}}', 'tipo_documento_set_id', '{{%cont_tipo_documento_set}}', 'id');

        //Por los datos que ya hay, no se puede poner not null.
        $this->addColumn('{{%cont_tipo_documento}}', 'tipo_documento_set_id', $this->integer(10)->unsigned()->notNull());
        $this->addForeignKey('fk_cont_tipo_documento_tipo_documento_set_id', '{{%cont_tipo_documento}}', 'tipo_documento_set_id', '{{%cont_tipo_documento_set}}', 'id');
    }

    public function safeDown()
    {
        echo "m180627_021633_add_tipo_documento_set_timbrado_detalle_tipo_doc no puede ser revertido.\n";
        return false;
    }
}
