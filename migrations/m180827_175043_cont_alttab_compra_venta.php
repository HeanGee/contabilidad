<?php

use jamband\schemadump\Migration;

class m180827_175043_cont_alttab_compra_venta extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_venta}}', 'cotizacion_propia', "ENUM('si', 'no') NULL DEFAULT 'no'");
        $this->addColumn('{{%cont_factura_compra}}', 'cotizacion_propia', "ENUM('si', 'no') NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
