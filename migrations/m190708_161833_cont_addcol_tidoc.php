<?php

use jamband\schemadump\Migration;

class m190708_161833_cont_addcol_tidoc extends Migration
{
    public function safeUp()
    {
        $t = 'cont_tipo_documento';
        $this->addColumn($t, 'solo_exento', "ENUM('si','no') NOT NULL DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
