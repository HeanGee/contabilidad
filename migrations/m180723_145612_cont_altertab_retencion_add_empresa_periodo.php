<?php

use jamband\schemadump\Migration;

class m180723_145612_cont_altertab_retencion_add_empresa_periodo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_retencion}}', 'empresa_id', $this->integer(10)->unsigned()->notNull());
        $this->addColumn('{{%cont_retencion}}', 'periodo_contable_id', $this->integer(11)->notNull());

        $this->addForeignKey('fk_retencion_empresa_empresa_id', '{{%cont_retencion}}', 'empresa_id', '{{%core_empresa}}', 'id');
        $this->addForeignKey('fk_retencion_periodocontable_periodo_contable_id', '{{%cont_retencion}}', 'periodo_contable_id', '{{%cont_empresa_periodo_contable}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
