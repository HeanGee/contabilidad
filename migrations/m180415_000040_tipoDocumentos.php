<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180415_000040_tipoDocumentos extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cont_tipo_documento}}', [
            'id' => $this->primaryKey()->unsigned(),
            'nombre' => $this->string(45)->notNull(),
            'detalle' => $this->string(255)->null(),
            'asociacion' => "ENUM ('cliente', 'proveedor', 'interno') NOT NULL",
            'estado' => "ENUM ('activo', 'inactivo') NOT NULL",
        ], $this->tableOptions);
    }

    public function safeDown()
    {
        echo 'm180415_000040_tipoDocumentos no puede ser revertido.\n';
        return false;
    }
}
