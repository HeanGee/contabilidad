<?php

use jamband\schemadump\Migration;

class m181101_170431_cont_addcols_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{%cont_prestamo}}", 'cuenta_caja', $this->integer(10)->unsigned()->notNull());
        $this->addColumn("{{%cont_prestamo}}", 'cuenta_intereses_vencer', $this->integer(10)->unsigned()->notNull());
        $this->addColumn("{{%cont_prestamo}}", 'cuenta_gastos_bancarios', $this->integer(10)->unsigned()->notNull());
        $this->addColumn("{{%cont_prestamo}}", 'cuenta_iva_10', $this->integer(10)->unsigned()->notNull());
        $this->addColumn("{{%cont_prestamo}}", 'cuenta_gastos_no_deducibles', $this->integer(10)->unsigned()->notNull());
        $this->addColumn("{{%cont_prestamo}}", 'cuenta_monto_operacion', $this->integer(10)->unsigned()->notNull());
        $this->addColumn("{{%cont_prestamo}}", 'cuenta_intereses_pagados', $this->integer(10)->unsigned()->notNull());

        // 'cuenta_intereses_vencer', 'cuenta_gastos_bancarios', 'cuenta_iva_10', 'cuenta_gastos_no_deducibles', 'cuenta_monto_operacion', 'cuenta_intereses_pagados'
        $this->dropForeignKey('fk_prestamo_plan_cta', '{{%cont_prestamo}}');
        $this->dropColumn('{{%cont_prestamo}}', 'cuenta_id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
