<?php

use jamband\schemadump\Migration;

class m190408_131219_cont_renamecol_extrangero_entidad extends Migration
{
    public function safeUp()
    {
        // Fetch the table schema
        $table = Yii::$app->db->schema->getTableSchema('cont_entidad');
        if (!isset($table->columns['extranjero'])) {
            // Column doesn't exist
            $this->renameColumn('cont_entidad', 'extrangero', 'extranjero');
        }
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
