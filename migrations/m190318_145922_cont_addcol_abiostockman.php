<?php

use jamband\schemadump\Migration;

class m190318_145922_cont_addcol_abiostockman extends Migration
{
    public function safeUp()
    {
        $table = 'cont_activo_biologico_stock_manager';
        $this->addColumn($table, 'nota_compra_id', $this->integer(11)->null()->after('factura_compra_id'));
        $this->addForeignKey('fk_abiostockman_notacompraid', $table, 'nota_compra_id', 'cont_factura_compra', 'id');
        $this->addColumn($table, 'nota_venta_id', $this->integer(11)->null()->after('factura_venta_id'));
        $this->addForeignKey('fk_abiostockman_notaventaid', $table, 'nota_venta_id', 'cont_factura_venta', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
