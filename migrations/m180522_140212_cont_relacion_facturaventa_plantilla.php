<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180522_140212_cont_relacion_facturaventa_plantilla extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_venta}}', 'plantilla_id', $this->integer(10)->unsigned()->notNull());
        $this->addForeignKey('fk_cont_factura_venta_plantilla_id', '{{%cont_factura_venta}}', 'plantilla_id', '{{%cont_plantilla_compraventa}}', 'id');
    }

    public function safeDown()
    {
        echo "m180522_140212_cont_relacion_facturaventa_plantilla no puede ser revertido.\n";
        return false;
    }
}
