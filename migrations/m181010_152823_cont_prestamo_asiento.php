<?php

use jamband\schemadump\Migration;

class m181010_152823_cont_prestamo_asiento extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_prestamo}}', 'asiento_id', $this->integer(11)->unsigned()->null());

        $this->addForeignKey('fk_prestamo_asiento_asiento_id', '{{%cont_prestamo}}', 'asiento_id', '{{%cont_asiento}}', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
