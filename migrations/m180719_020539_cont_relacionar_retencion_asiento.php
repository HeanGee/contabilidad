<?php

use jamband\schemadump\Migration;

class m180719_020539_cont_relacionar_retencion_asiento extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_retencion}}', 'asiento_id', $this->integer(11)->unsigned()->null());
        $this->addForeignKey('fk_retencion_asiento_asiento_id', '{{%cont_retencion}}', 'asiento_id', '{{%cont_asiento}}', 'id');
    }

    public function safeDown()
    {
        echo "m180719_020539_cont_relacionar_retencion_asiento no puede ser revertido\n";
        return false;
    }
}
