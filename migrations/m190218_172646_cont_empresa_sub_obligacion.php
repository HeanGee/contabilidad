<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190218_172646_cont_empresa_sub_obligacion extends Migration
{
    public function safeUp()
    {
        // cont_empresa_sub_obligacion
        $this->createTable('{{%cont_empresa_sub_obligacion}}', [
            'id' => $this->primaryKey()->unsigned(),
            'sub_obligacion_id' => $this->integer(11)->notNull(),
            'empresa_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        // fk: cont_empresa_obligacion
        $this->addForeignKey('fk_cont_empresa_sub_obligacion_obligacion_id', '{{%cont_empresa_sub_obligacion}}', 'sub_obligacion_id', '{{%cont_sub_obligacion}}', 'id');
        $this->addForeignKey('fk_cont_empresa_sub_obligacion_empresa_id', '{{%cont_empresa_sub_obligacion}}', 'empresa_id', '{{%core_empresa}}', 'id');
    }

    public function safeDown()
    {
        echo "m190218_172646_cont_empresa_sub_obligacion no puede ser revertido.\n";
        return false;
    }
}
