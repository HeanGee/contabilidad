<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180615_150955_cont_altertab_fventa_removerequired extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_factura_venta}}', 'tipo_documento_id', $this->integer(10)->unsigned()->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'fecha_emision', $this->date()->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'condicion', $this->string(7)->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'fecha_vencimiento', $this->date()->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'entidad_id', $this->integer(11)->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'obligaciones', $this->integer(11)->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'total', $this->decimal(14,2)->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'moneda_id', $this->integer(11)->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'cant_cuotas', $this->integer(11)->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'valor_moneda', $this->decimal(7,2)->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'empresa_id', $this->integer(10)->unsigned()->null());
        $this->alterColumn('{{%cont_factura_venta}}', 'plantilla_id', $this->integer(10)->unsigned()->null());
    }

    public function safeDown()
    {
        echo "m180615_150955_cont_altertab_fventa_removerequired no puede ser revertido.\n";
        return false;
    }
}
