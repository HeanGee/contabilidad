<?php

use jamband\schemadump\Migration;

class m180723_153928_cont_altertab_retencion_add_total extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_retencion}}', 'total', $this->decimal(14, 2));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
