<?php

use jamband\schemadump\Migration;

class m190325_125105_addcol_prestamo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_prestamo', 'tiene_factura', "ENUM('si', 'no') NOT NULL DEFAULT 'si'");
        $this->addColumn('cont_prestamo', 'cuenta_intereses_a_pagar', $this->integer(10)->unsigned()->null()->after('cuenta_intereses_pagados'));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
