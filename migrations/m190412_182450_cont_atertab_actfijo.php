<?php

use jamband\schemadump\Migration;

class m190412_182450_cont_atertab_actfijo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_activo_fijo', 'activo_fijo_id', $this->integer(10)->unsigned()->null()->after('id'));
        $this->addColumn('cont_activo_fijo', 'vida_util_contable', $this->tinyInteger(2)->null()->after('vida_util_fiscal'));
        $this->addColumn('cont_activo_fijo', 'valor_contable_neto', $this->decimal(14, 2)->comment("Valor revaluado contable neto.")->after('valor_fiscal_neto'));
        $this->addForeignKey('fk_activo_fijo_periodo_anterior', 'cont_activo_fijo', 'activo_fijo_id', 'cont_activo_fijo', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
