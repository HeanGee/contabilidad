<?php

use jamband\schemadump\Migration;

class m190604_184641_cont_addcol_venta extends Migration
{
    public function safeUp()
    {
        $tabla = 'cont_factura_venta';
        $this->addColumn($tabla, 'observaciones', $this->string(256)->null()->comment("Audita el motivo por el que se borra o se cambia su estado a no-vigente, etc"));
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
