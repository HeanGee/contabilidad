<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m181130_135926_cont_asiento_add_column_ref_esquema_bd extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cont_asiento', 'ref_esquema_bd', $this->string(90)->null());
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
