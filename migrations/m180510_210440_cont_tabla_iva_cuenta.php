<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180510_210440_cont_tabla_iva_cuenta extends Migration
{
    public function safeUp()
    {
        // cont_detalle_factura_venta
        $this->createTable('{{%cont_iva_cuenta}}', [
            'id' => $this->primaryKey(),
            'iva_id' => $this->integer(10)->unsigned()->notNull(),
            'plan_cuenta_id' => $this->integer(10)->unsigned()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_cont_iva_cuenta_plan_iva_id', '{{%cont_iva_cuenta}}', 'iva_id', '{{%core_iva}}', 'id');
        $this->addForeignKey('fk_cont_iva_cuenta_plan_cuenta_id', '{{%cont_iva_cuenta}}', 'plan_cuenta_id', '{{%cont_plan_cuenta}}', 'id');

    }

    public function safeDown()
    {
        echo "m180510_210440_cont_tabla_iva_cuenta no puede ser revertido.\n";
        return false;
    }
}
