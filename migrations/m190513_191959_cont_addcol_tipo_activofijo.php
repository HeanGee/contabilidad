<?php

use jamband\schemadump\Migration;

class m190513_191959_cont_addcol_tipo_activofijo extends Migration
{
    public function safeUp()
    {
        $table = 'cont_activo_fijo_tipo';
        $this->addColumn($table, 'cuenta_depreciacion_id', $this->integer(10)->unsigned()->null()->after('cuenta_id'));
        $this->addForeignKey('fk_cta_deprec_tipo_actfijo', $table, 'cuenta_depreciacion_id', 'cont_plan_cuenta', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
