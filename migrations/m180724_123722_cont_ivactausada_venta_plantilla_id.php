<?php

use jamband\schemadump\Migration;

class m180724_123722_cont_ivactausada_venta_plantilla_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_venta_iva_cuenta_usada}}', 'plantilla_id', $this->integer(10)->unsigned()->null());

        $this->addForeignKey('fk_venta_ivacta_usada_plantilla_id', '{{%cont_factura_venta_iva_cuenta_usada}}', 'plantilla_id', 'cont_plantilla_compraventa', 'id');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
