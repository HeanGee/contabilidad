<?php

use jamband\schemadump\Migration;

class m181105_164705_renew_esgravada_prestamo extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%cont_prestamo}}', 'es_gravada', 'incluye_iva');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
