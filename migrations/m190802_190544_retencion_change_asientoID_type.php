<?php

use jamband\schemadump\Migration;

class m190802_190544_retencion_change_asientoID_type extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('cont_retencion', 'asiento_id', $this->integer(10)->unsigned()->null());
    }

    public function safeDown()
    {
        echo self::class . ' no puede ser revertido.\n';
        return false;
    }
}
