<?php

use jamband\schemadump\Migration;

class m180720_173038_cont_addcolumn_enum_recibo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{cont_recibo}}', 'factura_tipo', "ENUM('compra','venta')");
        $this->addColumn('{{cont_recibo_detalle}}', 'factura_tipo', "ENUM('compra','venta')");
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
