<?php

use jamband\schemadump\Migration;

class m180704_172347_cont_addColumn_isCtaPrincipal_facturas_plantilla_detalle extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_plantilla_compraventa_detalle}}', 'cta_principal', "ENUM('si','no') DEFAULT 'no'");
        $this->addColumn('{{%cont_factura_venta_detalle}}', 'cta_principal', "ENUM('si','no') DEFAULT 'no'");
        $this->addColumn('{{%cont_factura_compra_detalle}}', 'cta_principal', "ENUM('si','no') DEFAULT 'no'");
    }

    public function safeDown()
    {
        echo "m180704_172347_cont_addColumn_isCtaPrincipal_facturas_plantilla_detalle no puede ser revertido.\n";
        return false;
    }
}
