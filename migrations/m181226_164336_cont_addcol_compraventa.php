<?php

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\EmpresaObligacion;
use backend\modules\contabilidad\models\Obligacion;
use yii\db\Schema;
use jamband\schemadump\Migration;

class m181226_164336_cont_addcol_compraventa extends Migration
{
    public function safeUp()
    {
        $tableNameCompra = '{{%cont_factura_compra}}';
        $tableNameVenta = '{{%cont_factura_venta}}';
        $tableNameOblig = '{{%cont_obligacion}}';

        $this->addColumn($tableNameCompra, 'obligacion_id', $this->integer(11)->null());
        $this->addColumn($tableNameCompra, 'para_iva', "ENUM('si','no') NOT NULL DEFAULT 'si'");

        $this->addColumn($tableNameVenta, 'obligacion_id', $this->integer(11)->null());
        $this->addColumn($tableNameVenta, 'para_iva', "ENUM('si','no') NOT NULL DEFAULT 'si'");

//        $compras = Compra::find()->all();
//        /** @var Compra $compra */
//        foreach ($compras as $compra) {
//            $empresaObligacion = EmpresaObligacion::findOne(['empresa_id' => $compra->empresa_id]);
//            if (isset($empresaObligacion)) {
//                $compra->obligacion_id = $empresaObligacion->obligacion_id;
//            }
//        }
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
