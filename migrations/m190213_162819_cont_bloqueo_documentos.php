<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m190213_162819_cont_bloqueo_documentos extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_compra}}', "bloqueado", 'ENUM("si","no") NOT NULL DEFAULT "NO"');
        $this->addColumn('{{%cont_factura_venta}}', "bloqueado", 'ENUM("si","no") NOT NULL DEFAULT "NO"');
        $this->addColumn('{{%cont_retencion}}', "bloqueado", 'ENUM("si","no") NOT NULL DEFAULT "NO"');
        $this->addColumn('{{%cont_recibo}}', "bloqueado", 'ENUM("si","no") NOT NULL DEFAULT "NO"');
        $this->addColumn('{{%cont_prestamo}}', "bloqueado", 'ENUM("si","no") NOT NULL DEFAULT "NO"');
        $this->addColumn('{{%cont_recibo_prestamo}}', "bloqueado", 'ENUM("si","no") NOT NULL DEFAULT "NO"');
    }

    public function safeDown()
    {
        echo self::className() . ' no puede ser revertido.\n';
        return false;
    }
}
