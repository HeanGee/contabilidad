<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180510_000804_cont_alter_column_valor_parametro_sistema extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_parametro_sistema}}', 'valor', $this->string(2000)->notNull());
    }

    public function safeDown()
    {
        echo "m180510_000804_cont_alter_column_valor_parametro_sistema no puede ser revertido.\n";
        return false;
    }
}
