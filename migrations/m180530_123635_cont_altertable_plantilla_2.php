<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180530_123635_cont_altertable_plantilla_2 extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cont_plantilla_compraventa_detalle}}', 'para_asiento_costo', "ENUM('venta','costo_venta') NOT NULL DEFAULT 'venta'");
        $this->renameColumn('{{%cont_plantilla_compraventa_detalle}}', 'para_asiento_costo', 'tipo_asiento');
        $this->renameColumn('{{%cont_plantilla_compraventa}}', 'tiene_asiento_costo', 'costo_mercad');
        $this->addColumn('{{%cont_plantilla_compraventa_detalle}}', 'tipo_saldo', "ENUM('debe','haber') NOT NULL");
    }

    public function safeDown()
    {
        echo "m180530_123635_cont_altertable_plantilla_2 no puede ser revertido.\n";
        return false;
    }
}
