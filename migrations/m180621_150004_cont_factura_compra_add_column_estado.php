<?php

use yii\db\Schema;
use jamband\schemadump\Migration;

class m180621_150004_cont_factura_compra_add_column_estado extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cont_factura_compra}}', 'estado', "ENUM ('vigente') NOT NULL DEFAULT 'vigente'");
    }

    public function safeDown()
    {
        echo "m180621_150004_cont_factura_compra_add_column_estado no puede ser revertido.\n";
        return false;
    }
}
