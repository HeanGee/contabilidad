<?php

namespace backend\modules\contabilidad\helpers;

use backend\modules\contabilidad\models\ParametroSistema;

/**
 * Class ParametroSistemaHelpers
 * @package common
 *
 */
class ParametroSistemaHelpers
{
    public static function getValorByNombre($nombre)
    {
        $model = ParametroSistema::find()->where(['nombre' => $nombre])->one();
        return $model ? $model->valor : '';
    }

    public static function getNombreByValor($valor)
    {
        $model = ParametroSistema::find()->where(['valor' => $valor])->one();
        return $model ? $model->nombre : '';
    }
}
