<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 09/01/19
 * Time: 11:42 AM
 */

namespace backend\modules\contabilidad\helpers;

use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\PlanCuenta;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;

class ModelHelpers extends Model
{
    public static function treeOfMainCuenta()
    {
        $my_file = Yii::$app->basePath . '/web/uploads/' . 'arbolfile.txt';
        $handle = fopen($my_file, 'w');
        Yii::$app->session->set('handle', $handle); // para que lo utilice desde metodo mas abajo

        $cods_cta = [];
        for ($i = 1; $i < 50; $i++) $cods_cta[] = "{$i}";
        $cuentas = PlanCuenta::find()->where(['IN', 'cod_completo', $cods_cta])->orderBy('cod_ordenable ASC')->all();
//        $cuentas = PlanCuenta::find()->where(['cod_completo' => '11'])->all();
        $arboles = [];
        foreach ($cuentas as $cuenta) {
            $nodo = new Nodo($cuenta);
            $nodo->root = true;
            $arboles[] = $nodo;
        }

        fclose($handle);

        return $arboles;
    }
}

/**
 * Class Nodo
 *
 * @property PlanCuenta $cuenta
 * @property Nodo $padre
 * @property int $monto
 * @property Nodo[] $hijos
 * @property array $balance
 * @property boolean $root
 *
 * @package backend\modules\contabilidad\helpers
 */
class Nodo extends Model
{
    public $cuenta;
    public $padre;
    public $monto;
    public $hijos = [];
    public $balance;
    public $root = false;

    public function __construct($cuenta_root = null)
    {
        parent::__construct();

        $this->monto = 0;
        $this->padre = null;
        $this->hijos = [];
        $this->root = false;

        // cargar balance
        $anho = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'))->anho;
        $desde = "{$anho}-01-01";
        $hasta = "{$anho}-12-31";

        /** @var ActiveQuery $query */
        $query = AsientoDetalle::find()->alias('detalle')
            ->joinWith('asiento as asiento')
            ->where([
                'asiento.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                'asiento.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')
            ])
            ->leftJoin('cont_plan_cuenta cuenta', 'cuenta.id = detalle.cuenta_id')
            ->andWhere(['BETWEEN', 'asiento.fecha', $desde, $hasta])
            ->select([
                'cuenta_id' => 'MIN(cuenta_id)',
                'cuenta_codigo' => 'MIN(cod_completo)',
                'cuenta_nombre' => 'MIN(cuenta.nombre)',
                'debe' => 'SUM(detalle.monto_debe)',
                'haber' => 'SUM(detalle.monto_haber)',
                'saldo' => 'IF((SUM(detalle.monto_haber)-SUM(detalle.monto_debe) > 0), (SUM(detalle.monto_haber)-SUM(detalle.monto_debe))*-1, SUM(detalle.monto_debe)-SUM(detalle.monto_haber))',

                'asiento_id' => "MIN(asiento.asiento_id)", // exige que exista asiento_id asi que cualquier cosa se le carga.
            ])
            ->groupBy('cuenta_id')
            ->orderBy('cod_ordenable ASC');

        if ($query->exists()) {
            $this->balance = $query->asArray()->all();
        }

        // instanciar root e hijos
        if (isset($cuenta_root) && $cuenta_root instanceof PlanCuenta) {
            $this->padre = null; // o hacer unset($this->padre);
            $this->cuenta = $cuenta_root;

            $this->loadHijos();
            $this->loadMontoFromBalance();
            $this->carryUpMonto();
        }
    }

    /**
     * @return Nodo[]
     */
    public function getHijos()
    {
        return $this->hijos;
    }

    public function loadHijos()
    {
        if (!isset($this->cuenta)) return;

        $this->hijos = [];
        foreach (PlanCuenta::find()->where(['padre_id' => $this->cuenta->id])->orderBy(['cod_ordenable' => SORT_ASC])->all() as $cuenta_hijo) {
            $hijo = new Nodo();
            $hijo->cuenta = $cuenta_hijo;
            $hijo->padre = $this;

            $this->hijos[] = $hijo;
        }

        foreach ($this->hijos as $hijo) {
            $hijo->loadHijos();
        }
    }

    public function loadMontoFromBalance()
    {
        if (!isset($this->balance) || empty($this->balance)) return;

        // root no puede estar asentado.

        // recorrer todos los hijos de esta generacion y cargar monto si hay en el balance.
        foreach ($this->hijos as $hijo) {
            foreach ($this->balance as $balance) {
                $stra = trim($hijo->cuenta->nombre);
                $strb = trim($balance['cuenta_nombre']);
                $ida = $hijo->cuenta->id;
                $idb = $balance['cuenta_id'];

                $data = "Compara {$stra} con {$strb}" . PHP_EOL;
                $handle = Yii::$app->session->get('handle');
                fwrite($handle, $data);

                if ($ida == $idb) {
                    $hijo->monto = (int)$balance['saldo'];
                }
            }
        }

        // Por cada hijo de la generacion actual, recursion
        foreach ($this->hijos as $hijo) {
            $hijo->loadMontoFromBalance();
        }
    }

    public function carryUpMonto()
    {
        if (empty($this->hijos) || !isset($this->hijos)) {
//            return $this->monto;
//            $this->padre->monto += (int)$this->monto;
            return;
        }

        foreach ($this->hijos as $hijo) {
//            $hijo->monto = $hijo->carryUpMonto();
            $hijo->carryUpMonto();
            $hijo->padre->monto += $hijo->monto;
        }
        return;
    }

    public function hasPadre()
    {
        return isset($this->padre);
    }
}