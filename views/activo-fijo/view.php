<?php

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\Venta;
use common\helpers\PermisosHelpers;
use common\helpers\ValorHelpers;
use kartik\detail\DetailView;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use kartik\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoFijo */

$this->title = 'Activo Fijo: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activo Fijos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-fijo-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a A. Fijos', ['index'/*, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= PermisosHelpers::getAcceso('contabilidad-activo-fijo-update') ? Html::a(Yii::t('app', 'Editar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('contabilidad-activo-fijo-delete') ? Html::a(Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'columns' => [
                        [
                            'attribute' => 'activo_fijo_tipo_id',
                            'value' => $model->activoFijoTipo->nombre,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'periodo_contable_id',
                            'value' => $model->empresaPeriodoContable->anho,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'costo_adquisicion',
                            'value' => ValorHelpers::numberFormat($model->costo_adquisicion, 0),
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'fecha_adquisicion',
                            'value' => Yii::$app->formatter->asDate($model->fecha_adquisicion, 'd-MM-Y'),
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'vida_util_fiscal',
                            'value' => $model->vida_util_fiscal,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'vida_util_restante',
                            'value' => $model->vida_util_restante,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'valor_fiscal_neto',
                            'value' => $model->valor_fiscal_neto,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'cuenta_id',
                            'value' => $model->cuenta->cod_completo . ' - ' . $model->cuenta->nombre,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
            ],
        ]);


        $actionId = Yii::$app->controller->action->id;
        $operacion = Yii::$app->request->getQueryParam('operacion');


        /** ------------------------ [inicio] Facturas relacionadas ------------------------ */
// Obtener compras o ventas y a la vez determinar el nombre para label de la columna del gridview.
        $allModels1 = $model->getCompras()->all();
        $allModels2 = $model->getVentas()->all();

        $columns = [
            [
                'class' => SerialColumn::className(),
            ],

            [
                'label' => 'Entidad',
                'value' => function ($model, $key, $index, $column) {
                    if ($model instanceof Venta || $model instanceof Compra)
                        return $model->entidad->razon_social;
                    return "";
                },
            ],
            [
                'label' => 'Nro Factura',
                'value' => function ($model, $key, $index, $column) {
                    if ($model instanceof Venta) {
                        return $model->getNroFacturaCompleto();
                    } elseif ($model instanceof Compra)
                        return $model->nro_factura;
                    return null;
                },
            ],
        ];

        $template = '{view}';
        $buttons = [];

        array_push($columns, [
            'class' => ActionColumn::class,
            'template' => $template,
            'buttons' => $buttons,
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $factura = $model instanceof Venta ? 'venta' : 'compra';
                    $url = Url::to([$factura . '/view', 'id' => $model->id]);
                    return $url;
                }
                return '';
            },
        ]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels1,
            'pagination' => false,
        ]);

        try {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => $columns,
                'hover' => true,
                'id' => 'grid',
                'panel' => ['type' => 'info', 'heading' => 'Facturas de Compra', 'footer' => false,],
                'toolbar' => [
                ]
            ]);
        } catch (Exception $exception) {
            throw $exception;
        }

        /** Para FacturaVenta */
        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels2,
            'pagination' => false,
        ]);

        try {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => $columns,
                'hover' => true,
                'id' => 'grid',
                'panel' => ['type' => 'info', 'heading' => 'Facturas de Venta', 'footer' => false,],
                'toolbar' => [
                ]
            ]);
        } catch (Exception $exception) {
            throw $exception;
        }
        /** ------------------------ [fin] Facturas relacionadas ------------------------ */

    } catch (Exception $e) {
        echo $e->getMessage();
    }

    $style = <<<CSS
table.detail-view th {
        width: 15%;
}
CSS;
    $this->registerCss($style);
    ?>

</div>
