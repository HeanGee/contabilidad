<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\search\ActivoFijoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activo-fijo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'activo_fijo_tipo_id') ?>

    <?= $form->field($model, 'empresa_id') ?>

    <?= $form->field($model, 'empresa_periodo_contable_id') ?>

    <?= $form->field($model, 'nombre') ?>

    <?php // echo $form->field($model, 'costo_adquisicion') ?>

    <?php // echo $form->field($model, 'fecha_adquisicion') ?>

    <?php // echo $form->field($model, 'coeficiente_revaluo') ?>

    <?php // echo $form->field($model, 'vida_util_fiscal') ?>

    <?php // echo $form->field($model, 'vida_util_restante') ?>

    <?php // echo $form->field($model, 'valor_fiscal_neto') ?>

    <?php // echo $form->field($model, 'valor_fiscal_revaluo') ?>

    <?php // echo $form->field($model, 'cuenta_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
