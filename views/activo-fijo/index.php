<?php

use backend\models\SessionVariables;
use backend\modules\contabilidad\models\ActivoFijoTipo;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ActivoFijoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Activos Fijos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-fijo-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    <div class="btn-toolbar">
        <?php $permisos = [
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-create'),
            'index-tipo' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-index'),
            'from-file' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-from-file'),
        ]; ?>
        <?= $permisos['create'] ? Html::a(Yii::t('app', 'Nuevo Activo Fijo'), ['create'], ['class' => 'btn btn-success']) : null ?>
        <?= $permisos['from-file'] ? Html::a(Yii::t('app', 'Desde Excel'), ['activo-fijo/from-file'], ['class' => 'btn btn-warning pull-right']) : null ?>
        <?= $permisos['index-tipo'] ? Html::a(Yii::t('app', 'Ir a A. Fijo Tipos'), ['activo-fijo-tipo/index'], ['class' => 'btn btn-info pull-right']) : null ?>
    </div>
    </p>

    <?php
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('contabilidad-activo-fijo-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('contabilidad-activo-fijo-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('contabilidad-activo-fijo-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'hover' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'nombre',
                [
                    'label' => 'Tipo de activo fijo',
                    'value' => 'activoFijoTipo.nombre',
                    'attribute' => 'activo_fijo_tipo_id',
                    'format' => 'raw',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(ActivoFijoTipo::find()->all(), 'id', 'nombre'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Filtro de periodo contable'],
                ],
                [
                    'label' => 'Periodo Contable',
                    'value' => 'empresaPeriodoContable.anho',
                    'attribute' => 'empresa_periodo_contable_id',
                    'format' => 'raw',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(EmpresaPeriodoContable::getEmpresaPeriodos(Yii::$app->session->get(SessionVariables::empresa_actual)), 'id', 'text'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Filtro de periodo contable'],
                ],
                [
                    'attribute' => 'cuenta_id',
                    'value' => function ($model) {
                        return "{$model->cuenta->cod_completo} - {$model->cuenta->nombre}";
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template
                ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
        throw $e;
    } ?>
    <?php Pjax::end(); ?>
</div>
