<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 21/01/19
 * Time: 02:36 PM
 */

use backend\modules\contabilidad\models\ActivoFijoAtributo;
use kartik\form\ActiveForm;
use kartik\helpers\Html;

/* @var $model ActivoFijoAtributo */
/* @var $form ActiveForm */
/* @var $key string */
?>

<td>
    <?= $form->field($model, "[$key]atributo")->textInput([
        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, "[{$key}]valor")->textInput([])->label(false) ?>
</td>

<td class="hidden" style="text-align: center">
    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'delete-atributo btn btn-danger btn-sm',
        'name' => "[{$key}]-delete_button",
        'title' => 'Eliminar atributo',
    ]) ?>
</td>
