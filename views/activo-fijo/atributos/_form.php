<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 21/01/19
 * Time: 02:33 PM
 */

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\ActivoFijoAtributo;
use kartik\form\ActiveForm;
use kartik\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $detalles ActivoFijoAtributo[] */
/* @var $form ActiveForm */
?>

<?php
(isset($detalles) && !empty($detalles)) || $detalles = [];
?>


<div class="activo-fijo-tipo-atributo">

    <fieldset>
        <legend class="text-info">
            <small>&nbsp;&nbsp;Atributos
                <div class="hidden pull-left">
                    <?php
                    // new detalle button
                    echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                        'id' => 'add-new-atributo',
                        'class' => 'pull-left btn btn-success'
                    ]);
                    ?>
                </div>
            </small>
        </legend>
    </fieldset>

    <?php

    echo '<table id="tabla-atributos" class="table table-condensed table-responsive">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Nombre</th>';
    echo '<th style="text-align: center;">Valor</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    \kartik\select2\Select2Asset::register($this);
    \yii\widgets\MaskedInputAsset::register($this);

    // existing detalles fields
    $skey = '';
    $key = 0;
    foreach ($detalles as $index => $detalle) {
        $key = $detalle->id != null ? $detalle->id - 1 : $index;
        echo '<tr class="fila-detalle">';
        echo $this->render('_detalle', [
            'key' => 'new' . ($key + 1),
            'form' => $form,
            'model' => $detalle,
        ]);
        echo '</tr>';
        $key++;
    }

    $modelTipoAtributo = new ActivoFijoAtributo();
    $html = $this->render('_detalle', [
        'key' => 'new_atributo',
        'form' => $form,
        'model' => $modelTipoAtributo,
    ]);

    $bluePrint = HtmlHelpers::trimmedHtml($html);

    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);

    $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
    ?>

    <?php
    $script_head = <<<JS
var detalle_k = $key;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);

    $script = <<<JS
$('#add-new-atributo').on('click', function () {
   detalle_k += 1;
   $('#tabla-atributos').find('tbody')
       .append('<tr class="fila-detalle">' + '$bluePrint'.replace(/new_atributo/g, 'new' + detalle_k) + '</tr>');
});

$(document).on('click', '.delete-atributo', function () {
    $(this).closest('tbody tr').remove();
});

$(document).ready(function () {

})
JS;
    $this->registerJs($script);
    ?>

</div>
