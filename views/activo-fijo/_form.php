<?php

use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\FlashMessageHelpsers;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model ActivoFijo */
/* @var $form yii\widgets\ActiveForm */

$forModal = Yii::$app->request->getQueryParam('formodal');
$actvFormOptions = ['id' => 'actfijo-form'];

if ($forModal == 'true') {
    ActiveFormDisableSubmitButtonsAsset::register($this);
    $actvFormOptions = [
        'id' => 'actfijo-modal-form',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons'],
    ];
}
?>

<div class="activo-fijo-form">

    <?php $form = ActiveForm::begin($actvFormOptions); ?>

    <?php
    $tiene_factura_compra = isset($model->compra);
    $action_id = Yii::$app->controller->action->id;

    try {
        $moneda_prefixs = [];
        foreach (\backend\models\Moneda::find()->all() as $moneda) {
            $moneda_prefixs["{$moneda->id}"] = "{$moneda->simbolo} ";
        }
        $actFijoTipoList = new Query();
        $actFijoTipoList = $actFijoTipoList
            ->select([
                'id' => 'MIN(tipo.id)',
                'text' => 'MIN(tipo.nombre)',
                'atributos' => "GROUP_CONCAT(CONCAT(atr.atributo, '-', atr.obligatorio))",
                'vida_util' => 'MIN(tipo.vida_util)',
                'cantidad_requerida' => 'MIN(tipo.cantidad_requerida)'
            ])
            ->from("cont_activo_fijo_tipo as tipo")
            ->leftJoin("cont_activo_fijo_tipo_atributo as atr", "tipo.id = atr.activo_fijo_tipo_id")
            ->groupBy("tipo.id")
            ->all();

//        $actFijoTipoList = ActivoFijoTipo::find()->select(['id' => 'id', 'text' => 'nombre', 'vida_util' => 'vida_util'])->asArray()->all();
        Yii::$app->session->set('debug', $actFijoTipoList);

        $rows = [
            [
                'autoGenerateColumns' => false,
                'columns' => 12,
                'attributes' => [
                    'nombre' => [
                        'type' => Form::INPUT_TEXT,
                        'columnOptions' => ['colspan' => '3'],
                        'options' => ['placeholder ' => 'Nombre']
                    ],
                    'cuenta_id' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '3'],
                        'value' => $form->field($model, 'cuenta_id')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'data' => PlanCuenta::getCuentaLista(),
                                'disabled' => true,
                            ],
                            'initValueText' => isset($model->cuenta) ? (ucfirst($model->cuenta->cod_completo . ' - ' . $model->cuenta->nombre)) : '',
                            'pluginEvents' => [
                                'change' => ""
                            ]
                        ])
                    ],
                    'costo_adquisicion' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '3'],
                        'value' => $form->field($model, 'costo_adquisicion')->widget(MaskedInput::className(), [
                            'clientOptions' => [
                                'rightAlign' => true,
                                'alias' => 'decimal',
                                'groupSeparator' => '.',
                                'radixPoint' => ',',
                                'autoGroup' => true,
                                'suffix' => ""
                            ],
                        ]),
                    ],
                    'fecha_adquisicion' => [
                        'type' => Form::INPUT_WIDGET,
                        'columnOptions' => ['colspan' => '3'],
                        'widgetClass' => DateControl::class,
                        'displayFormat' => 'php:d-M-Y',
                        'saveFormat' => 'php:Y-m-d',
                        'options' => [
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'language' => 'es',
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-MM-yyyy',
                                    'todayHighlight' => true,
                                    'weekStart' => '0',
                                ],
                            ],

                        ]
                    ],
                ]
            ],
            [
                'autoGenerateColumns' => false,
                'columns' => 12,
                'attributes' => [
                    'activo_fijo_tipo_id' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '3'],
                        'value' => $form->field($model, 'activo_fijo_tipo_id')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'data' => $actFijoTipoList,
                            ],
                            'initValueText' => !empty($model->activoFijoTipo->nombre) ? ucfirst($model->activoFijoTipo->nombre) : '',
                            'pluginEvents' => [
                                'change' => ""
                            ]
                        ])
                    ],
                    'vida_util_fiscal' => [
                        'type' => Form::INPUT_TEXT,
                        'columnOptions' => ['colspan' => '3'],
                        'options' => [
                            'placeholder ' => 'Vida Útil...',
                            'readonly' => true,
                        ],
                    ],
                    'vida_util_contable' => [
                        'type' => Form::INPUT_TEXT,
                        'columnOptions' => ['colspan' => '3'],
                        'options' => [
                            'placeholder ' => 'Vida Útil contable...',
                            'readonly' => false,
                        ],
                    ],
                    'moneda_id' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '3'],
                        'value' => $form->field($model, 'moneda_id')->widget(Select2::className(), [
                            'disabled' => isset($model->compra), // si tiene compra, se establece su moneda, por tanto no deja modificar
                            'options' => ['placeholder' => 'Seleccione una moneda'],
                            'pluginOptions' => [
                                'allowClear' => false,
                                'data' => Compra::getMonedas(),
                            ],
                            'initValueText' => !empty($model->moneda_id) ? ($model->moneda_id . ' - ' . $model->moneda->nombre) : ''
                        ])->label('Moneda' . (isset($model->compra) ? '<span class="popuptrigger popover-moneda-id-label text-info glyphicon glyphicon-info-sign" style="padding: 0 4px;"></span>' : '')),
                    ],
                ]
            ],
            [
                'autoGenerateColumns' => false,
                'columns' => 12,
                'attributes' => [
                    'valor_fiscal_neto' => [
                        'type' => Form::INPUT_TEXT,
                        'columnOptions' => ['colspan' => '3'],
                        'options' => ['placeholder ' => 'Valor Fiscal Neto...']
                    ],
                ]
            ]
        ];
        if ($forModal == 'true') {
//            echo $form->field($model, 'actfijo_selector')->widget(Select2::classname(), [
//                'options' => ['placeholder' => 'Seleccione un activo fijo ...'],
//                'initValueText' => $model->actfijo_selector != null ?
//                    $model->id . ' - ' . $model->nombre : '',
//                'pluginOptions' => [
//                    'allowClear' => true,
//                    'data' => ArrayHelper::map(ActivoFijo::find()->asArray()->all(), 'id', 'nombre'),
//                ],
//            ])->label('Activo Fijo');

            // Sin cuenta.
            $rows = [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 2,
                    'attributes' => [
                        'nombre' => [
                            'type' => Form::INPUT_TEXT,
//                            'columnOptions' => ['colspan' => '3'],
                            'options' => ['placeholder ' => 'Nombre']
                        ],
                        'costo_adquisicion' => [
                            'type' => Form::INPUT_RAW,
//                            'columnOptions' => ['colspan' => '3'],
                            'value' => $form->field($model, 'costo_adquisicion')->widget(MaskedInput::className(), [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'suffix' => "",
                                ],
                            ])
                        ],
                    ]
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 4,
                    'attributes' => [
                        'activo_fijo_tipo_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'activo_fijo_tipo_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => $actFijoTipoList,
                                ],
                                'initValueText' => !empty($model->activoFijoTipo->nombre) ? ucfirst($model->activoFijoTipo->nombre) : '',
                                'pluginEvents' => [
                                    'change' => ""
                                ]
                            ])
                        ],
                        'vida_util_fiscal' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '1'],
                            'options' => [
                                'placeholder ' => 'Vida Útil...',
                                'readonly' => true,
                            ],
                        ],
                        'vida_util_contable' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '1'],
                            'options' => [
                                'placeholder ' => 'Vida Útil Contable...',
                                'readonly' => false,
                            ],
                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 4,
                    'attributes' => [
                        'valor_fiscal_neto' => [
                            'type' => Form::INPUT_TEXT,
//                            'columnOptions' => ['colspan' => '3'],
                            'options' => ['placeholder ' => 'Valor Fiscal Neto...']
                        ],
                        'cantidad' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => '',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'fecha_adquisicion' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['style' => ['display' => 'none']]
                        ],
                    ]
                ],
//                [
//                    'autoGenerateColumns' => false,
//                    'columns' => 12,
//                    'attributes' => [
//                        'valor_fiscal_neto' => [
//                            'type' => Form::INPUT_TEXT,
//                            'columnOptions' => ['colspan' => '3'],
//                            'options' => ['placeholder ' => 'Valor Fiscal Neto...']
//                        ],
//                        'cantidad' => [
//                            'type' => Form::INPUT_WIDGET,
//                            'widgetClass' => MaskedInput::className(),
//                            'options' => [
//                                'clientOptions' => [
//                                    'rightAlign' => true,
//                                    'alias' => 'integer',
//                                    'groupSeparator' => '.',
//                                    'radixPoint' => '',
//                                    'autoGroup' => true,
//                                    'prefix' => "",
//                                ],
//                            ],
//                        ],
//                    ]
//                ]
            ];
        }


        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => $rows
        ]);

        // Atributos plantilla dinamicos
        $detalles = Yii::$app->session->get('cont_afijo_atributos', []);
        if (!empty($detalles))
            $detalles = $detalles['atributos'];
        Pjax::begin(['id' => 'pjax_atributos']);
        try {
            echo $this->render('atributos/_form', ['form' => $form, 'model' => $model, 'detalles' => $detalles]);
        } catch (\Exception $exception) {
            FlashMessageHelpsers::createWarningMessage($exception->getMessage());
            echo $exception;
        }
        Pjax::end();

    } catch (Exception $e) {
        echo $e;
        FlashMessageHelpsers::createWarningMessage($e->getMessage());
    }

    $boton_guardar_class = "";
    if (Yii::$app->controller->action->id == "create") {
        $boton_guardar_class = 'btn btn-success';
    } else {
        $boton_guardar_class = 'btn btn-primary';
    }

    echo Html::beginTag('div', ['class' => 'form-group text-right btn-toolbar']);
    $submitBtnOptions = ['data' => ['disabled-text' => 'Guardando...'], 'class' => $boton_guardar_class];
    echo Html::submitButton('Guardar', $forModal ? $submitBtnOptions : ['class' => $boton_guardar_class]);
    if (!in_array(Yii::$app->controller->action->id, ['create', 'update', 'delete', 'index'])) {
        echo Html::button('<span class="glyphicon glyphicon-arrow-right"></span>', [
            'id' => 'btn_forward',
            'style' => "display: yes;",
            'title' => 'Siguiente',
            'class' => 'btn btn-primary pull-right',
            'data-url' => Url::to(['manejar-desde-factura-compra'])
        ]);
        echo Html::button('<span class="glyphicon glyphicon-trash"></span>', [
            'id' => 'btn_delete_current',
            'style' => "display: yes;",
            'title' => 'Borrar actual',
            'class' => 'btn btn-warning pull-right',
            'data-url' => Url::to(['manejar-desde-factura-compra'])
        ]);
        echo Html::button('<span class="glyphicon glyphicon-arrow-left"></span>', [
            'id' => 'btn_rewind',
            'style' => "display: yes;",
            'title' => 'Anterior',
            'class' => 'btn btn-primary pull-right',
            'data-url' => Url::to(['manejar-desde-factura-compra'])
        ]);
        /*echo Html::button('<span class="glyphicon glyphicon-plus"></span>', [
            'id' => 'btn_new',
            'style' => "display: yes;",
            'title' => 'Nuevo',
            'class' => 'btn btn-success pull-right',
            'data-url' => Url::to(['manejar-desde-factura-compra'])
        ]);*/
    }
    echo Html::tag('br/');
    echo Html::endTag('div');
    ?>

    <!--    <div class="form-group">-->
    <!--        <? //= PermisosHelpers::getAcceso('contabilidad-activo-fijo-create') ? Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) : '' ?>-->
    <!--    </div>-->

    <?php ActiveForm::end(); ?>

    <?php
    $costo_adq_desde_factura = Yii::$app->session->get('costo_adq_desde_factura', null); // para calcular precio unitario si la cantidad es mayor a 1
    $moneda_prefixs = Json::encode($moneda_prefixs);
    $infoColor = \backend\helpers\HtmlHelpers::InfoColorHex(true);
    $url_loadAtributos = Json::htmlEncode(\Yii::t('app', Url::to(['load-atributos'])));
    $url_manejarDesdeCompra = Json::htmlEncode(\Yii::t('app', Url::to(['manejar-desde-factura-compra'])));
    $script = <<<JS
var costo_adq_desde_factura = "{$costo_adq_desde_factura}";
// obtener la id del formulario y establecer el manejador de eventos
$("#actfijo-modal-form").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
            $('#btn-hidden-simulator').click();
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$(document).on('change', '#activofijo-moneda_id', function () {
    /* En el post se va con el simbolo. Al guardar se quiere guardar con el simbolo. */
//    console.log($moneda_prefixs);
//    let moneda_field_id = '#activofijo-moneda_id';
//   $('#activofijo-costo_adquisicion').inputmask({
//        "alias": "numeric",
//        "digits": ($(moneda_field_id).select2('data')[0]['tiene_decimales'] === "si" ? 2 : 0),
//        "groupSeparator": ".",
//        "autoGroup": true,
//        "autoUnmask": true,
//        "unmaskAsNumber": true,
//        "radixPoint": ",",
//        "suffix": " " + ($moneda_prefixs)[$(moneda_field_id).val()],
//    }) 
});

function loadAtributos() {
    $.ajax({
        url: $url_loadAtributos,
        type:'get',
        data:{
        afijo_id: "{$model->id}",
            afijo_tipo_id:$('#activofijo-activo_fijo_tipo_id').val(),
    },
    success: function (result) {
        $.pjax.reload({container: "#pjax_atributos", async: false});
        $.pjax.reload({container: "#flash_message_id", async: false});
    }
});
}

$(document).off('change').on('change', '#activofijo-activo_fijo_tipo_id', function () {
    let vidaUtilField = $('#activofijo-vida_util_fiscal');
    vidaUtilField.val($(this).select2('data')[0]['vida_util']);

    let vidaUtilContable = $('#activofijo-vida_util_contable');
    if (vidaUtilContable.val() === '') {
        vidaUtilContable.val($(this).select2('data')[0]['vida_util']).trigger('change');
    } else if($(this).val() === '') {
        vidaUtilContable.val('').trigger('change');
    }
    
    let form = $('form[id^="actfijo-form"]');
    if (!form.length) form = $('form[id^="actfijo-modal-form"]');
    form.yiiActiveForm('validateAttribute', 'activofijo-vida_util_fiscal');
    
    if (['update', 'create'].includes('$action_id')) loadAtributos();
    else {
        if ($(this).val() === '') return;

        for (let i = document.getElementById("tabla-atributos").getElementsByTagName("tr").length - 1; i > 0; i--) {
            document.getElementById("tabla-atributos").deleteRow(i);
        }

        let atributos = $(this).select2('data')[0]['atributos'].split(',');
        let table = $("#tabla-atributos")[0], i = 0;
        for (let j = 0; j < atributos.length; j++) {
            let atributo = atributos[j],
                slices = atributo.split('-'),
                atrName = slices[0],
                obligat = slices[1];  // no se si es neceario

            table.insertRow();
            console.log(atrName);

            $("#tabla-atributos tr")[table.getElementsByTagName("tr").length - 1].innerHTML = '<td><div class="form-group highlight-addon field-activofijoatributo-new' + i + '-atributo required"><input type="text" id="activofijoatributo-new' + i + '-atributo" class="form-control" name="ActivoFijoAtributo[new' + i + '][atributo]" value="' + atrName + '" readonly=""><div class="help-block"></div></div></td><td><div class="form-group highlight-addon field-activofijoatributo-new' + i + '-valor"><input type="text" id="activofijoatributo-new' + i + '-valor" class="form-control" name="ActivoFijoAtributo[new' + i + '][valor]"><div class="help-block"></div></div></td>';
            i++;
        }
        
        let cantidadField = $("#activofijo-cantidad");
        let cantidadRequerida = $(this).select2('data')[0]['cantidad_requerida'];
        console.log(cantidadRequerida);
        if (cantidadRequerida === 'si') {
            cantidadField.parent().parent().css('display', '');
        } else {
            cantidadField.parent().parent().css('display', 'none');
        }

//        let tipo_id = $(this).val();
//        let costo_ad = $('#activofijo-costo_adquisicion').val().replace(/\./g, '').replace(/\,/g, '.');
//        let fecha_factura = $('#activofijo-fecha_adquisicion').val();
//        let url = $url_manejarDesdeCompra + '&formodal=true&costo_adquisicion=' + costo_ad +
//        '&fecha_factura=' + fecha_factura + '&tipo_id=' + tipo_id + '&goto=render_atributos';
//        $.get(
//            url,
//            {
//                nombre: $('#activofijo-nombre').val(),
//                vida_u_fiscal: $('#activofijo-vida_util_fiscal').val(),
//                val_fiscal_net: $('#activofijo-valor_fiscal_neto').val(),
//                cantidad: $().val('#activofijo-cantidad').val(),
//            },
//            function (data) {
//                $('.modal-body', modal).html("");
//                $('.modal-body', modal).html(data);
//                $('#modal').trigger('change');
//            }
//        )
    }
});

$(document).on('change', '#activofijo-cantidad', function () {
    if ('$action_id' === 'manejar-desde-factura-compra') {
        let precio_unitario = costo_adq_desde_factura / $(this).val();
        $('#activofijo-costo_adquisicion').val(precio_unitario.toFixed(2)).trigger('change');
        $('#activofijo-valor_fiscal_neto').val(precio_unitario.toFixed(2)).trigger('change');
    }
});

$(document).ready(function () {
    if (("$action_id") === 'update') {
        // Carga los atributos
        loadAtributos();

        let tiene_factura_compra = "$tiene_factura_compra";
        if (tiene_factura_compra === '1') {
            // $('#activofijo-cuenta_id').prop('disabled', true);
            $('#activofijo-costo_adquisicion').prop('readonly', true);
        } else {
            // $('#activofijo-cuenta_id').prop('disabled', false);
            $('#activofijo-costo_adquisicion').prop('readonly', false);
        }
    } else {
        // MaskedInput no puede ser puesto a readonly or disabled desde la configuracion del widget.
        if (!['index', 'view', 'create', 'update', 'delete'].includes("$action_id")) {
            // $('#activofijo-costo_adquisicion').prop('readonly', true);
            // $('#activofijo-fecha_adquisicion').parent().parent()[0].style.display = "none";
        }
    }

    if ($('#activofijo-moneda_id').prop('disabled')) {
        // TODO: utilizar el que Hernán puso en el core.
        /* 
    background-color: #d9edf7;
    color: #31708f;
    text-align: center;*/
        let css = {
            'background-color': '#$infoColor',
            'color': 'white',
            'text-align': 'center',
            'font-weight': 'bold'
        };
        let popoverTitle = 'Atención';
        let popoverContent = 'Este campo aparecerá deshabilitada cuando la moneda haya sido puesta por la factura de compra correspondiente.';
        applyPopOver($('span[class*="popover-moneda-id-label"]'), popoverTitle, popoverContent, {'title-css': css});
    }

    if (!['create', 'update'].includes('$action_id'))
        applyPopOver($('input#activofijo-costo_adquisicion'), 'Información', 'Si la cantidad es mayor a 1 y el precio unitario resulta con partes decimales, ingrese con dichos decimales. Se necesita poder comparar con el monto de la factura y determinar que sean iguales en precio total.', {'theme-color': 'info-l'});
});

$('#btn_forward').click(function () {
    let boton = $(this);
    let fecha_factura = $('#activofijo-fecha_adquisicion').val();
    let costo_adq = $('#activofijo-costo_adquisicion').val().replace(' Gs', '').replace(/\./g, '').replace(/\,/g, '.');
    let url = boton.data('url') + '&formodal=' + true + '&goto=forward' + '&fecha_factura=' + fecha_factura + '&costo_adquisicion=' + costo_adq;
    $.ajax({
        url: url,
        type: 'get',
        data: {},
        success: function (data) {
            $('.modal-body', modal).html("");
            $('.modal-body', modal).html(data);
            $('#modal').trigger('change');
        }
    })
});

$('#btn_rewind').click(function () {
    let boton = $(this);
    let fecha_factura = $('#activofijo-fecha_adquisicion').val();
    let costo_adq = $('#activofijo-costo_adquisicion').val().replace(' Gs', '').replace(/\./g, '').replace(/\,/g, '.');
    let url = boton.data('url') + '&formodal=' + true + '&goto=rewind' + '&fecha_factura=' + fecha_factura + '&costo_adquisicion=' + costo_adq;
    console.log(url);
    $.ajax({
        url: url,
        type: 'get',
        data: {},
        success: function (data) {
            $('.modal-body', modal).html("");
            $('.modal-body', modal).html(data);
            $('#modal').trigger('change');
        }
    })
});

$('#btn_new').click(function () {
    let boton = $(this);
    let fecha_factura = $('#activofijo-fecha_adquisicion').val();
    // let costo_adq = $('#activofijo-costo_adquisicion').val().replace(' Gs', '').replace(/\./g, '').replace(/\,/g, '.');
    $('#activofijo-costo_adquisicion').change('');
    let url = boton.data('url') + '&formodal=' + true + '&goto=new' + '&fecha_factura=' + fecha_factura; // + '&costo_adquisicion=' + costo_adq;
    console.log(url);
    $.ajax({
        url: url,
        type: 'get',
        data: {},
        success: function (data) {
            $('.modal-body', modal).html("");
            $('.modal-body', modal).html(data);
            $('#modal').trigger('change');
        }
    })
});

$('#btn_delete_current').click(function () {
    let boton = $(this);
    let fecha_factura = $('#activofijo-fecha_adquisicion').val();
    let costo_adq = $('#activofijo-costo_adquisicion').val().replace(' Gs', '').replace(/\./g, '').replace(/\,/g, '.');
    let url = boton.data('url') + '&formodal=' + true + '&goto=delete' + '&fecha_factura=' + fecha_factura + '&costo_adquisicion=' + costo_adq;
    $.ajax({
        url: url,
        type: 'get',
        data: {
            nombre: $('#activofijo-nombre').val()
        },
        success: function (data) {
            $('.modal-body', modal).html("");
            $('.modal-body', modal).html(data);
            $('#modal').trigger('change');
        }
    })
});

$('#activofijo-costo_adquisicion').keyup(function () {
    let valor = $(this).val().replace(' Gs', '').replace(/\./g, '').replace(/\,/g, '.');
    if (valor !== "") {
        valor = parseFloat(valor);
    } else {
        valor = 0.0;
    }
    console.log(valor);
    $('#activofijo-valor_fiscal_neto').val(valor);
});

$('#modal').on('shown.bs.modal', function () {
    $('input[name="ActivoFijo[nombre]"]').focus();
});
JS;

    $this->registerJs($script);
    ?>
</div>
