<?php

/* @var $this yii\web\View */

use backend\models\SessionVariables;
use backend\modules\contabilidad\models\ActivoFijoTipo;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>

<?php
isset($heading) || $heading = "Activos Fijos";
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'toolbar' => [],
        'hover' => true,
        'panel' => [
            'type' => 'info',
            'heading' => $heading,
            'footerOptions' => ['class' => ''],
            'beforeOptions' => ['class' => ''],
            'afterOptions' => ['class' => '']
        ],
        'panelFooterTemplate' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => "Nombre",
                'format' => 'raw',
                'attribute' => 'nombre',
                'value' => function ($model) {
                    $url = Url::to(['/contabilidad/activo-fijo/view', 'id' => $model->id]);
                    return Html::a($model->nombre, $url, []);
                }
            ],
            [
                'label' => 'Tipo de activo fijo',
//                'value' => 'activoFijoTipo.nombre',
                'value' => 'activoFijoTipo.nombre',
                'attribute' => 'activo_fijo_tipo_',
                'format' => 'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(ActivoFijoTipo::find()->all(), 'nombre', 'nombre'),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'Filtro de periodo contable'],
            ],
            [
                'label' => 'Periodo Contable',
                'value' => 'empresaPeriodoContable.anho',
                'attribute' => 'periodo_contable_',
                'format' => 'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(EmpresaPeriodoContable::getEmpresaPeriodos(Yii::$app->session->get(SessionVariables::empresa_actual)), 'id', 'text'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Filtro de periodo contable'],
            ],
            [
                'attribute' => 'cuenta_id',
                'value' => function ($model) {
                    return "{$model->cuenta->cod_completo} - {$model->cuenta->nombre}";
                }
            ],
        ],
    ]);
} catch (Exception $exception) {
    \common\helpers\FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    echo $exception->getMessage();
}
