<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 02/01/19
 * Time: 06:20 AM
 */

use common\helpers\FlashMessageHelpsers;
use kartik\builder\FormGrid;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $model \backend\modules\contabilidad\controllers\Archivo */

?>

<?php
try {
    $submit = Html::submitButton('Enviar', ['class' => 'btn btn-primary']);
    echo '<legend><small>Cargar desde Excel</small></legend>';
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'archivo' => [
                        'type' => Form::INPUT_FILE,
                        'value' => $form->field($model, 'archivo')->widget(FileInput::classname(),
                            [
                                'language' => 'es',
                                'pluginOptions' => ['showPreview' => false],
                            ]
                        ),
                        'label' => "Archivo (.xls, .xlsx)",
                    ],
//                    'reemplazar' => [
//                        'type' => Form::INPUT_RAW,
//                        'value' => $form->field($model, 'reemplazar')->widget(Select2::className(), [
//                            'options' => ['placeholder' => 'Seleccione una obligac...'],
//                            'data' => ['si' => "Sí", 'no' => "No"],
//                            'pluginOptions' => [
//                                'allowClear' => false,
//                            ],
//                            'initValueText' => ($model->reemplazar == 'si') ? "Sí" : 'No',
//                        ]),
//                    ]
                ],
            ],
            [
                'attributes' => [
                    'action' => [
                        'type' => Form::INPUT_RAW,
                        'value' => "<div class='form-group' style='margin-top: 20px'>{$submit}</div>"
                    ],
                ],
            ]
        ]
    ]);
} catch (Exception $exception) {
    echo $exception;
    FlashMessageHelpsers::createWarningMessage($exception->getMessage());
}
?>
