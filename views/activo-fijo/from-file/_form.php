<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 02/01/19
 * Time: 06:19 AM
 */

use backend\helpers\HtmlHelpers;
use kartik\form\ActiveForm;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $model \backend\modules\contabilidad\controllers\Archivo */

$this->title = "Crear activos fijos desde Excel";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activos Fijos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="activo-fijo-from-file">

    <p>
        <?= ""/*PermisosHelpers::getAcceso('contabilidad-activo-fijo-from-file') ?
            Html::a('Regresar a la lista', ['index',], ['class' => 'btn btn-info']) : null*/ ?>
    </p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $this->render('_fields', ['form' => $form, 'model' => $model]) ?>

    <!--    <div class="form-group">-->
    <!--        --><?php echo '';// Html::submitButton('Importar', ['class' => 'btn btn-success']) ?>
    <!--    </div>-->

    <?php ActiveForm::end(); ?>

    <?php

    // mostrar panel de info
    if (Yii::$app->request->isGet) {
        $content = <<<HTML
<h4><u>Para la columna de <strong>CUENTA CONTABLE</strong> del archivo Excel</u>:</h4>
<ul>
    <li>Se puede indicar tanto el nombre de la cuenta como el códico completo de la misma.</li>
    <li>El nombre debe coincidir con el nombre de una de las cuentas del sistema y no importa el uso de mayúscula y
        minúscula.
    </li>
    <li>Si la cuenta especificada no existe en el sistema, se aborta el proceso de creación.</li>
</ul>

<br/>
<h4><u>Para la columna de <strong>TIPO</strong> del archivo Excel</u>:</h4>
<ul>
    <li>El nombre debe coincidir con el nombre de los tipos de activo fijo del sistema y no importa el uso de mayúscula y
        minúscula.
    </li>
    <li>Si el tipo especificado no existe en el sistema, se aborta el proceso de creación.</li>
</ul>
HTML;
//        echo HtmlHelpers::BootstrapInfoPanel("INFORMACIONES", $content, "", true);
    }

    // mostrar panel de errores
    $sesion = Yii::$app->session;
    if ($sesion->has('errors_array')) {
        $errors = $sesion->get('errors_array');
        $html_li = '<ul style="list-style-type:circle">';
        foreach ($errors as $error) {
            $rowNro = $error['fila'];
            $msg = $error['mensaje'];
            $html_li .= "<li>$msg</li>";
        }
        $html_li .= '</ul>';
        $cantErrores = number_format(sizeof($errors), 0, '', '.');
        $panel = HtmlHelpers::BootstrapDangerPanel('<strong>MENSAJES DE ERRORES</strong>', $html_li, "Total de errores: {$cantErrores}");
        echo $panel;
    } elseif (!empty($activos_fijos_creados)) {
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $activos_fijos_creados,
            'pagination' => false,
        ]);

        echo '<fieldset><legend class="text-info"><small>Activos Fijos Creados</small></legend>';
        try {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'hover' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'nombre',
                    [
                        'label' => 'Tipo de activo fijo',
                        'value' => 'activoFijoTipo.nombre',
                        'attribute' => 'activo_fijo_tipo_id',
                        'format' => 'raw',
                        'filterInputOptions' => ['placeholder' => 'Filtro de periodo contable'],
                    ],
                    [
                        'label' => 'Periodo Contable',
                        'value' => 'empresaPeriodoContable.anho',
                        'attribute' => 'empresa_periodo_contable_id',
                        'format' => 'raw',
                        'filterInputOptions' => ['placeholder' => 'Filtro de periodo contable'],
                    ],
                    [
                        'attribute' => 'cuenta_id',
                        'value' => function ($model) {
                            return "{$model->cuenta->cod_completo} - {$model->cuenta->nombre}";
                        }
                    ],
                ],
            ]);
        } catch (\Exception $exception) {
            echo $exception;
            \common\helpers\FlashMessageHelpsers::createWarningMessage($exception->getMessage());
        }
        echo '</fieldset>';
    }

    // Mostrar link a archivo de ejemplo
    $link = '../modules/contabilidad/views/activo-fijo/from-file/Ejemplo_Importacion_Activo_Fijo.xlsx';
    echo HtmlHelpers::LinkToFilesOfInterest('Archivos de interés', $link, null, 'text');
    ?>

</div>
