<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoFijo */

$this->title = Yii::t('app', 'Nuevo Activo Fijo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activos Fijos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-fijo-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
