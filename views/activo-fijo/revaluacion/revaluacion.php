<?php

use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\FormRevaluoAFijo;
use backend\modules\contabilidad\models\PlanCuenta;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $activosFijos ActivoFijo[] */
/* @var $detallesAsiento AsientoDetalle[] */
/* @var $model FormRevaluoAFijo */

$this->title = Yii::t('app', 'Revaluación de activos fijos.');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activos Fijos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$debes = $haberes = [];
foreach ($detallesAsiento as $asiento) {
    if ($asiento->monto_debe != 0) $debes[] = $asiento;
    else $haberes[] = $asiento;
}
$asientos = array_merge($debes, $haberes);

if (sizeof($asientos) == 0)
?>
    <div class="revaluacion-form">
        <?php $form = ActiveForm::begin(['id' => 'revaluacion-form']) ?>

        <table class="table table-condensed" id="table-asiento-cierre">
            <tr>
                <th>Código</th>
                <th>Cuenta</th>
                <th style="text-align: right;">Debe</th>
                <th style="text-align: right;">Haber</th>
            </tr>

            <?php foreach ($asientos as $index => $asiento) { ?>
                <tr>
                    <td style="width: 10%"><?= ($index == sizeof($asientos) - 1) ? '' : $asiento->cuenta_id ?></td>
                    <td style="width: 50%"><?= ($index == sizeof($asientos) - 1) ?
                            $form->field($model, 'cuenta_contrapartida')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => PlanCuenta::getCuentaLista(true),
                                    'width' => '100%'
                                ],
                                'initValueText' => ($model->cuenta_contrapartida != '') ? $model->cuenta->getNombreConCodigo() : '',
                            ])->label(false) : $asiento->cuenta->nombre ?></td>
                    <td style="width: 20%"><?= $asiento->monto_debe ?></td>
                    <td style="width: 20%"><?= $asiento->monto_haber ?></td>
                </tr>
            <?php } ?>
        </table>

        <?php
        echo '<div class="form-group">';
        echo Html::submitButton('Generar', ['class' => 'btn btn-success']);
        echo '</div>';
        ?>

        <?php ActiveForm::end() ?>
    </div>

<?php ob_start(); // output buffer the javascript to register later ?>
    <script>
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }

        $tablas = $('.table-condensed');
        $($tablas[0].getElementsByTagName('tr')).each(function () {
            $columnas = $(this)[0].getElementsByTagName('td');
            if (typeof $columnas[0] !== 'undefined') {
                $($columnas[2]).css('text-align', 'right');
                $($columnas[3]).css('text-align', 'right');
                if ($columnas.length > 3 && $columnas[3].textContent !== '0') {
                    $($columnas[1]).css('text-align', 'right');
                }
                $($columnas[2]).text(numberWithCommas($($columnas[2]).text()));
                $($columnas[3]).text(numberWithCommas($($columnas[3]).text()));
            }
        });
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>