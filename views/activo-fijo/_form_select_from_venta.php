<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 16/08/2018
 * Time: 11:05
 */

use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

?>

<?php
ActiveFormDisableSubmitButtonsAsset::register($this);

$form = ActiveForm::begin([
    'id' => 'activo_fijo_selector_form',
    'options' => ['class' => 'disable-submit-button'],
]);

try {
    echo Select2::widget([
    ]);
} catch (Exception $exception) {
}
?>
    <br>
    <div class="panel panel-success">
        <!--        <div class="panel-heading">-->
        <!--            <h3 class="panel-title">Panel de Selección</h3>-->
        <!--        </div>-->
        <div class="panel-body">

            <div id="pickList"></div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['data' => ['disabled-text' => 'Guardando...'], 'class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
// <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
// <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
$glyph_add = Html::tag('span', "", ["class" => "glyphicon glyphicon-arrow-right", 'aria-hidden' => true]);
$glyph_rem = Html::tag('span', "", ["class" => "glyphicon glyphicon-arrow-left", 'aria-hidden' => true]);
$glyph_addAll = Html::tag('span', "", ["class" => "glyphicon glyphicon-plus", 'aria-hidden' => true]);
$glyph_remAll = Html::tag('span', "", ["class" => "glyphicon glyphicon-minus", 'aria-hidden' => true]);
$array = json_decode($json_pick_left);
$spanPopOver = (empty($array) ? "<span class='popovertrigger popuptrigger-factura-venta-id text-info glyphicon glyphicon-info-sign' style='padding: 0 4px;'></span>" : ''); // mostrar solo cuando la lista izq es vacia
$spanPopOver = "<span class='popovertrigger popuptrigger-factura-venta-id text-info glyphicon glyphicon-info-sign' style='padding: 0 4px;'></span>"; // mostrar siempre
$popoverContent = null;
$action = Yii::$app->controller->action->id;
if ($action == 'manejar-desde-factura-venta') {
    $popoverContent = 'La lista aparecerá vacía si:<br/><br/>' .
        '<ol>' .
        '<li>No existe ningún Activo Fijo para la empresa y periodo actuales.</li>' .
        '<li>Existen Activos Fijos para la empresa y periodo actuales pero éstos no tienen asociados una factura de compra; <strong>no se puede pretender vender activos fijos sin documentos que respalden su legal adquisición.</strong></li>' .
        '<li>Los Activos Fijos fueron devueltos por Nota de Crédito de Compra.</li>' .
        '<li>Los Activos Fijos retornados por Nota de Crédito de Venta fueron a su vez devueltos por Nota de Crédito de Compra.</li>' .
        '<li>No queda ningún Activo Fijo disponible para la venta.</li>' .
        '</ol>';
} elseif ($action == 'manejar-desde-nota-credito-venta') {
    $popoverContent = 'La lista aparecerá vacía si:<br/><br/>' .
        '<ol>' .
        '<li>Todos los activos fijos vendidos por la factura elegida ya fueron retornadas en su totalidad por una o más notas de crédito.</li>' .
        '</ol>';
} elseif ($action == 'manejar-desde-nota-credito-compra') {
    $popoverContent = 'La lista aparecerá vacía si:<br/><br/>' .
        '<ol>' .
        '<li>Todos los activos fijos comprados y registrados por la factura elegida fueron vendidos por una o más facturas de venta.</li>' .
        '<li>Todos los activos fijos comprados y registrados por la factura elegida fueron devueltos por una o más notas de crédito.</li>' .
        '<li>Los activos fijos comprados y registrados por la factura elegida fueron devueltos en parte por una o más notas de crédito y vendidos el resto por una o más facturas de venta.</li>' .
        '<li>Existen uno o más activos fijos de la empresa y perido actuales pero ninguno de éstos tienen una factura de compra registrada.</li>' .
        '</ol>';
}
$infoColor = \backend\helpers\HtmlHelpers::InfoColorHex(true);
$url_getTotalCostoAdq = \yii\helpers\Json::encode(\yii\helpers\Url::to(['get-total-costo-adq-ajax']));
$script = <<<JS
// Evitar que se haga submit con ENTER
$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });
    
    if ($('span.popovertrigger').length) {
        let css = {'background-color': '#$infoColor', 'color': 'white', 'text-align': 'center', 'font-weight': 'bold'};
        let popoverTitle = 'Atención';
        let popoverContent = "{$popoverContent}";
        applyPopOver($('span[class*="popuptrigger-factura-venta-id"]'), popoverTitle, popoverContent, {'title-css': css});
    }
});

// Picklist
(function ($) {

    $.fn.pickList = function (options) {
        var opts = $.extend({}, $.fn.pickList.defaults, options);

        this.fill = function () {
            var option_left = '';

            $.each(opts.data_left, function (key, val) {
                option_left += '<option data-id=' + val.id + '>' + val.text + '</option>';
            });
            this.find('.pickData').append(option_left);

            var option_right = '';

            $.each(opts.data_right, function (key, val) {
                option_right += '<option data-id=' + val.id + '>' + val.text + '</option>';
            });
            this.find('.pickListResult').append(option_right);
        };
        this.controll = function () {
            var pickThis = this;

            pickThis.find(".pAdd").on('click', function () {
                var p = pickThis.find(".pickData option:selected");
                p.clone().appendTo(pickThis.find(".pickListResult"));
                reload_left(p);
                p.remove();
            });

            pickThis.find(".pAddAll").on('click', function () {
                var p = pickThis.find(".pickData option");
                p.clone().appendTo(pickThis.find(".pickListResult"));
                reload_left(p);
                p.remove();
            });

            pickThis.find(".pRemove").on('click', function () {
                var p = pickThis.find(".pickListResult option:selected");
                p.clone().appendTo(pickThis.find(".pickData"));
                reload_right(p);
                p.remove();
            });

            pickThis.find(".pRemoveAll").on('click', function () {
                var p = pickThis.find(".pickListResult option");
                p.clone().appendTo(pickThis.find(".pickData"));
                reload_right(p);
                p.remove();
            });
        };

        this.getValues = function () {
            var objResult = [];
            this.find(".pickListResult option").each(function () {
                objResult.push({
                    id: $(this).data('id'),
                    text: this.text
                });
            });
            return objResult;
        };

        this.init = function () {
            var pickListHtml =
                "<div class='row'>" +
                "  <div class='col-sm-5'>" +
                "    <input class='input' id='input_left' type='text' placeholder='Filtrar' style='width: 200px;>" +
                "<label style='margin-left:15%'><strong>Disponibles{$spanPopOver}</strong></label>" +
                "	 <select class='form-control pickListSelect pickData' id='left_id' multiple></select>" +
                " </div>" +
                " <div class='col-sm-2 pickListButtons'>" +
                " <br><br><br><br><br>" +
                "	<button type='button' title='Seleccionar uno y agregar' class='pAdd btn btn-primary btn-sm'><span class=\"glyphicon glyphicon-arrow-right\" aria-hidden=\"true\"></span></button>" +
                "	<button type='button' title='Seleccionar uno y retirar' class='pRemove btn btn-primary btn-sm'><span class=\"glyphicon glyphicon-arrow-left\" aria-hidden=\"true\"></span></button>" +
                "   <button type='button' title='Agregar Todos' class='pAddAll btn btn-success btn-sm'><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span></button>" +
                "	<button type='button' title='Retirar Todos' class='pRemoveAll btn btn-danger btn-sm'><span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span></button>" +
                " </div>" +
                " <div class='col-sm-5'>" +
                " 	 <input class='input' id='input_right' type='text' placeholder='Filtrar' style='width: 200px;>" +
                "       <label style='margin-left:15%'><strong>Seleccionadas</strong></label>" +
                "    <select class='form-control pickListSelect pickListResult' id='right_id' multiple></select>" +
                " </div>" +
                "</div>";

            this.append(pickListHtml);

            this.fill();
            this.controll();
        };

        this.init();
        return this;
    };

    $.fn.pickList.defaults = {
        add: 'Agregar',
        addAll: 'Agregar Todo',
        remove: 'Borrar',
        removeAll: 'Borrar Todo'
    };


}(jQuery));

var pick = $("#pickList").pickList({
    data_left: $json_pick_left,
    data_right: $json_pick_right
});

$("#getSelected").click(function () {
    console.log(pick.getValues());
});

//filtro de picklist
jQuery.fn.filterByText = function (textbox) {
    return this.each(function () {
        var select = this;

        // Orden de lista en select
        var my_options = $(select).find('option');
        var selected = $(select).val();
        my_options.sort(function (a, b) {
            if (a.text.toLowerCase() > b.text.toLowerCase()) return 1;
            if (a.text.toLowerCase() < b.text.toLowerCase()) return -1;
            return 0
        });
        $(select).empty().append(my_options);
        $(select).val(selected);

        // genera lista auxiliar con datos originales

        var options = [];
        $(select).find('option').each(function () {
            options.push({
                value: $(this).data('id'),
                text: $(this).text()
            });
        });
        $(select).data('options', options);

        // Evento capturado para filtrar
        $(textbox).bind('change keyup', function () {
            var options = $(select).empty().data('options');
            var search = $.trim($(this).val());
            var regex = new RegExp(search, "gi");
            $.each(options, function (i) {
                var option = options[i];
                if (option.text.match(regex) !== null) {
                    $(select).append(
                        $('<option  data-id=' + option.value + '>').text(option.text).val(option.value)
                    );
                }
            });
        });
    });
};

//

$(function () {
    $('#left_id').filterByText($('#input_left'));
});

$(function () {
    $('#right_id').filterByText($('#input_right'));
});

// $(document).on('submit', '#activo_fijo_selector_form', function () {
//     var selected = [];
//     $('#right_id').find('option').each(function () {
//         selected.push($(this)[0].getAttribute('data-id'));
//     });
//
//     $.each(selected, function (i, v) {
//         var input = $("<input>").attr({"type": "hidden", "name": "selected[]"}).val(v);
//         $('#activo_fijo_selector_form').append(input);
//     });
//     return true;
// });

$("#activo_fijo_selector_form").on("beforeSubmit", function (e) {
    let form = $(this);
    let selected = [];
    $('#right_id').find('option').each(function () {
        selected.push($(this)[0].getAttribute('data-id'));
    });

    $.each(selected, function (i, v) {
        let input = $("<input>").attr({"type": "hidden", "name": "selected[]"}).val(v);
        $('#activo_fijo_selector_form').append(input);
    });
    
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
            $.ajax({
                url: $url_getTotalCostoAdq,
                success: function(result) {
                    if (($action) === 'manejar-desde-nota-credito-compra') {
                        let slices = result.split('|'),
                            fieldId = '#' + slices[0];
                        $(fieldId).val(slices[1]).trigger('change');
                    }
                }
            });
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

var reload_left = function (p) {
    var options_left = [];
    var options_right = [];
    options_left = $('#left_id').data('options');
    options_right = $('#right_id').data('options');
    $(p).each(function () {
        var id_or = $(this).data('id');

        $.each(options_left, function (i) { // cada elemento que habia originalmente
            var option = options_left[i];

            if (!(typeof option === 'undefined')) {
                if (id_or === option.value) {

                    options_right.push({
                        value: option.value,
                        text: option.text
                    });
                    options_left.splice(options_left.indexOf(option), 1);
                }
            }
        });
    });
};

var reload_right = function (p) {
    var options_right = [];
    options_right = $('#right_id').data('options');
    var options_left = [];
    options_left = $('#left_id').data('options');
    $(p).each(function () {
        var id_or = $(this).data('id');

        $.each(options_right, function (i) { // cada elemento que habia originalmente
            var option = options_right[i];

            if (!(typeof option === 'undefined')) {
                if (id_or === option.value) {
                    options_left.push({
                        value: option.value,
                        text: option.text
                    });
                    options_right.splice(options_right.indexOf(option), 1);
                }
            }
        });
    });
};

JS;

$this->registerJs($script);

$this->registerCss('.pickListButtons {
  padding: 10px;
  text-align: center;
}

.pickListButtons button {
    display: block;
    margin: auto;
    margin-bottom: 5px;
}

.pickListSelect {
  height: 200px !important;
}

.input {
    background-image: url(img/searchicon.png);
    background-position: 10px 12px;
    background-repeat: no-repeat;
    padding: 12px 20px 12px 42px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
}');
?>