<?php

use backend\modules\contabilidad\models\RetencionDetalleIvas;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Retencion */

$this->title = 'Datos de la Retencion ' . $model->nro_retencion;
$this->params['breadcrumbs'][] = ['label' => 'Retencions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retencion-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-retencion-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-retencion-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-retencion-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-retencion-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-retencion-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Reteciones', ['index', 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] && $model->bloqueado == 'no' ? Html::a('Modificar', ['update', 'id' => $model->id, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] && $model->bloqueado == 'no' ? Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Retenciones',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'columns' => [
                        [
                            'attribute' => 'fecha_emision',
                            'value' => $model->fecha_emision,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'nro_retencion',
                            'value' => $model->nro_retencion,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Nro Factura',
                            'value' => ($model->facturaCompra != null) ? $model->facturaCompra->nro_factura : $model->facturaVenta->getNroFacturaCompleto(),
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'label' => ($model->facturaCompra != null) ? 'Proveedor' : 'Cliente',
                            'value' => ($model->facturaCompra != null) ? $model->facturaCompra->entidad->razon_social : $model->facturaVenta->entidad->razon_social,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'R.U.C.',
                            'value' => ($model->facturaCompra != null) ? $model->facturaCompra->entidad->ruc : $model->facturaVenta->entidad->ruc,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'base_renta_cabezas',
                            'value' => $model->base_renta_cabezas,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'base_renta_toneladas',
                            'value' => $model->base_renta_toneladas,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'factor_renta_porc',
                            'value' => $model->factor_renta_porc,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'factor_renta_cabezas',
                            'value' => $model->factor_renta_cabezas,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'factor_renta_toneladas',
                            'value' => $model->factor_renta_toneladas,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
            ],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => RetencionDetalleIvas::find()->where(['retencion_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => 'Bases de Impuestos de la Retención',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'columns' => [
                [
                    'label' => 'Iva',
                    'value' => 'iva.porcentaje'
                ],
                [
                    'label' => 'Inpuesto',
                    'value' => 'baseFormatted'
                ],
                [
                    'label' => 'Factor de Retención',
                    'value' => 'factorFormatted'
                ],
//                [
//                    'label' => 'Nombre de cuenta',
//                    'value' => 'planCuenta.nombre'
//                ],
//                [
//                    'label' => 'Monto',
//                    'value' => 'subtotalFormat',
//                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
//
//                ],
//                [
//                    'label' => 'Debe / Haber',
//                    'value' => 'cta_contable'
//                ],
            ],
        ]);

        // Facturas relacionadas.
        echo $this->render('@backend/modules/contabilidad/views/facturas-relacionadas/_facturas_relacionadas', ['model' => $model]);
    } catch (Exception $e) {
        throw $e;
    } ?>

</div>
