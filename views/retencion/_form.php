<?php

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\TipoDocumentoSet;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\dialog\Dialog;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model Retencion */
///* @var $bases RetencionDetalleBaseIvas[] */
///* @var $factors RetencionDetalleFactorIvas[] */
?>

<div class="retencion-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success',
            "data" => (Yii::$app->controller->action->id == 'update') ? [
                'confirm' => 'Desea guardar los cambios?',
                'method' => 'post',
            ] : []
        ]) ?>
    </div>

    <?php try {
        $label_factura_id = <<< HTML
{$model->getAttributeLabel('factura_id')}
<span class="popuptrigger factura text-info glyphicon glyphicon-info-sign" style="padding: 0 4px;"></span>
HTML;
        $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
        $actionID = Yii::$app->controller->action->id;
//            echo $form->errorSummary($model);
        $data = null;
        if ($actionID == 'create') { // el select2 debe excluir facturas que ya tienen retencion.
            $data = $operacion == 'compra' ? Retencion::getComprasParaRetener(true, [], null, true, true, false, "", $model->fecha_emision) : Retencion::getVentasParaRetener(true, [], null, true, true, false, "", $model->fecha_emision);
        } else { // el select2 debe INCLUIR aquellas que tienen asociadas retencion
            $data = $operacion == 'compra' ? Retencion::getComprasParaRetener(true, [], null, true, false, false, "", $model->fecha_emision) : Retencion::getVentasParaRetener(true, [], null, true, false, false, "", $model->fecha_emision);
        }
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 5,
            'attributes' => [
                'fecha_emision' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => DateControl::class,
                    'options' => [
                        'type' => DateControl::FORMAT_DATE,
                        'ajaxConversion' => false,
                        'language' => 'es',
                        'widgetOptions' => [
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-MM-yyyy',
                                'todayHighlight' => true,
                                'weekStart' => '0',
                            ],
                        ],

                    ]
                ],
                'factura_id' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => $form->field($model, 'factura_id')->widget(Select2::className(), [
                        'data' => $data,
                        'options' => ['placeholder' => ''],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'pluginEvents' => [],
                    ])->label($label_factura_id),
                ],
                'nro_retencion' => [
                    'type' => Form::INPUT_RAW,
                    'value' => $form->field($model, 'nro_retencion')->widget(MaskedInput::className(), [
                        'mask' => '999-999-9999999',
                        'options' => [
                            'class' => 'form-control',
                            'disabled' => false,
                        ]
                    ]),
                    'options' => ['placeholder' => 'Ingrese nro de retencion...'],
                ],
                'timbrado_nro' => [
                    'type' => Form::INPUT_TEXT,
                    // Si es compra el timbrado para retencion es de la empresa actual, sino es de la entidad de la venta que habra sido puesto al crearse.
                    'value' => ($operacion == 'compra') ? (isset($model->timbrado) ? $model->timbrado->nro_timbrado : Retencion::getTimbradoRetencion()->nro_timbrado) :
                        (isset($model->timbrado) ? $model->timbrado->nro_timbrado : ''),
                    'options' => [
                        'readonly' => ($operacion == 'compra' || $actionID == 'update') ? true : false,
                    ]
                ],
//  // Se maneja automaticamente.
//                    'timbrado_detalle_id' => [
//                        'type' => Form::INPUT_RAW,
//                        'value' => $form->field($model, 'timbrado_detalle_id')->widget(Select2::className(), [
//                            'options' => ['placeholder' => 'Seleccione uno ...'],
//                            'initValueText' => !empty($model->timbrado_detalle_id) ? $model->timbradoDetalle->timbrado->nro_timbrado : "",
//                            'pluginOptions' => [
//                                'allowClear' => true,
//                            ]
//                        ])
//                    ],
            ],
        ]);

        $totales = [];
        /** @var IvaCuenta $iva_cta */
        foreach (IvaCuenta::find()->all() as $iva_cta) {
//                if ($iva_cta->iva->porcentaje != 0)
            $totales['iva-' . $iva_cta->iva->porcentaje . ''] = [
                'label' => 'Total ' . (($iva_cta->iva->porcentaje !== 0) ? ' ' . $iva_cta->iva->porcentaje . '%' : 'Exenta'),
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => MaskedInput::className(),
                'options' => [
                    'clientOptions' => [
                        'rightAlign' => true,
                        'alias' => 'decimal',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'autoGroup' => true
                    ],
                    'options' => [
                        'class' => 'form-control',
                    ]
                ],
            ];
        }
        $totales['total-factura'] = [
            'label' => 'Total Factura',
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => MaskedInput::className(),
            'options' => [
                'clientOptions' => [
                    'rightAlign' => true,
                    'alias' => 'decimal',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'autoGroup' => true
                ],
                'options' => [
                    'class' => 'form-control',
                ]
            ],
        ];
        // Ordenar campos
        uksort($totales, 'strnatcasecmp');

        echo Form::widget([
            'formName' => 'total',
            'columns' => 6,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['colspan' => '2'],
                'inputContainer' => ['colspan' => '2'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => $totales,
        ]);
        $boton = Html::label('Agregar', 'boton_add_timbrado', ['class' => 'control-label']) . '<br/>'
            . Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'id' => 'boton_add_timbrado',
                'style' => "display: yes;",
                'type' => 'button',
                'title' => 'Agregar Timbrado.',
                'class' => 'btn btn-success tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal2',
                'data-url' => Url::to(['timbrado/create-new-timbrado-retencion']),
                'data-pjax' => '0',
            ]);
        Pjax::begin(['id' => 'pjax_table_retenciones']);
        echo Html::beginTag('div', ['class' => 'panel panel-primary']);
        echo Html::beginTag('div', ['class' => 'panel-heading']);
        echo Html::beginTag('h3', ['class' => 'panel-title']) . 'PANEL DE RETENCIONES';
        echo Html::endTag('h3');
        echo Html::endTag('div');
        echo Html::beginTag('div', ['class' => 'panel-body']);
        echo Html::beginTag('table', ['class' => 'table table-condensed', 'id' => 'retencion']);
        echo Html::beginTag('thead');
        echo Html::beginTag('tr', ['class' => 'title-row']);
        echo Html::tag('th', "Retenciones (En Guaranies)", ['width' => '10%']);
        echo Html::tag('th', "Renta %" . HtmlHelpers::InfoHelpIcon('renta'), ['style' => "text-align: center;"]);
        echo Html::tag('th', 'Renta (cabezas)', ['style' => "text-align: center;"]);
        echo Html::tag('th', 'Renta (toneladas)', ['style' => "text-align: center;"]);
        // TODO: render de cabeceras por ivas.
        echo $this->render('_form_cabecera_ivas', ['model' => $model, 'form' => $form]);
        echo Html::endTag('tr');
        echo Html::endTag('thead');

        echo Html::beginTag('tbody');
        echo Html::beginTag('tr', ['class' => 'first-row']);
        echo Html::tag('td', 'Base de la Retención', ['style' => "font-weight: bold; text-align: left; padding-top: 15px; font-style: italic;"]);
        echo Html::beginTag('td', ['class' => 'base-sumable sumable renta-porc']);
        echo $form->field($model, 'base_renta_porc')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'base-sumable sumable renta-cabezas']);
        echo $form->field($model, 'base_renta_cabezas')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'base-sumable sumable renta-toneladas last-column']);
        echo $form->field($model, 'base_renta_toneladas')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        // TODO: renderizar columnas de base ivas.
//            echo $this->render('_form_base_ivas', ['model' => $model, 'form' => $form, 'bases' => $bases]);
        echo $this->render('_form_base_ivas', ['model' => $model, 'form' => $form]);
        echo Html::endTag('tr');

        echo Html::beginTag('tr', ['class' => 'last-row']);
        echo Html::tag('td', 'Factor de la Retención', ['style' => "font-weight: bold; text-align: left; padding-top: 15px; font-style: italic;"]);
        echo Html::beginTag('td', ['class' => 'factor-sumable sumable renta-porc']);
        echo $form->field($model, 'factor_renta_porc')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'factor-sumable sumable renta-cabezas']);
        echo $form->field($model, 'factor_renta_cabezas')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'factor-sumable sumable renta-toneladas last-column']);
        echo $form->field($model, 'factor_renta_toneladas')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        // TODO: renderizar columnas de factor iva.
//            echo $this->render('_form_factor_ivas', ['model' => $model, 'form' => $form, 'factors' => $factors]);
        echo $this->render('_form_factor_ivas', ['model' => $model, 'form' => $form]);
        echo Html::endTag('tr');

        echo Html::beginTag('tr', ['class' => 'sumatory-row']);
        echo Html::tag('td', 'Total Retenido', ['style' => "font-weight: bold; text-align: left; padding-top: 15px;"]);
        echo Html::beginTag('td', ['class' => 'sumatory']);
        echo MaskedInput::widget([
            'name' => 'total-renta-porc',
            'id' => 'total-renta-port',
//            'readonly' => true,
            'value' => '',
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'readonly' => true,
                'class' => 'form-control',
            ]
        ]);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'sumatory']);
        echo MaskedInput::widget([
            'name' => 'total-renta-cabezas',
            'id' => 'total-renta-cabezas',
//            'readonly' => true,
            'value' => '',
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'readonly' => true,
                'class' => 'form-control',
            ]
        ]);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'sumatory']);
        echo MaskedInput::widget([
            'name' => 'total-renta-toneladas',
            'id' => 'total-renta-toneladas',
//            'readonly' => true,
            'value' => '',
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'readonly' => true,
                'class' => 'form-control',
            ]
        ]);
        echo Html::endTag('td');
        // TODO: render campos totales por ivas.
        echo $this->render('_form_sumatory_ivas', ['model' => $model, 'form' => $form]);
        echo Html::endTag('tr');
        echo Html::endTag('tbody');
        echo Html::endTag('table');
        echo Html::endTag('div');
        echo Html::endTag('div');
        Pjax::end();

        echo Dialog::widget(); //TODO llevar al header del core para no repeterir código

    } catch (Exception $e) {
        print $e;
    } ?>

    <?php ActiveForm::end(); ?>

</div>
<?php
$CSS = <<<CSS
#retencion table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
#retencion tbody tr:hover {
    background-color: #ebffdd;
}
#retencion td, #retencion th {
    /*border: 1px solid #ddd;*/
    padding: 8px;
}
#retencion tr.last-row {
    /*border-bottom: 2px solid black;*/
}
#retencion tbody tr.first-row {
    /*border-top: 2px solid black;*/
}
CSS;
$this->registerCss($CSS);
?>
<?php
$porcentajes_iva = [];
$ivas_por = IvaCuenta::find()->all();
foreach ($ivas_por as $iva_por) /*if ($iva_por->iva->porcentaje != 0)*/
    $porcentajes_iva[] = $iva_por->iva->porcentaje;
asort($porcentajes_iva);
$porcentajes_iva = array_values($porcentajes_iva);
$porcentajes_iva = Json::encode($porcentajes_iva);
$empresa_id = Yii::$app->session->get('core_empresa_actual');
$periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');

$nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-tipodoc_set_retencion";
$parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
$tipodoc_set_retencion_id = "";
if ($parametro_sistema != null) {
    $tipodoc_set_retencion = TipoDocumentoSet::findOne(['id' => $parametro_sistema->valor]);
    if ($tipodoc_set_retencion != null) $tipodoc_set_retencion_id = $tipodoc_set_retencion->id;
}
$retencion_id = $model->id;

$url_getFactor = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-factor', 'operacion' => $operacion])));
$url_getTimbrados = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-timbrados', 'operacion' => $operacion])));
$url_getTotalesField = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-totales-fields', 'operacion' => $operacion])));
$url_getTotalIvaMonto = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-total-iva-monto', 'operacion' => $operacion])));
$url_getTimDetID = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-timbrado-detalle-id', 'operacion' => $operacion])));
$url_getFacturas = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-facturas', 'operacion' => $operacion])));
$url_getTimbrado = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-timbrado', 'operacion' => $operacion])));
$url_getPorcentajeIvasFactura = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-porcentaje-ivas-factura', 'operacion' => $operacion])));
$infoColor = HtmlHelpers::InfoColorHex(true);
$popupFactura = <<<HTML
<p>Se listan las facturas bajo las siguientes condiciones:</p>
<ul>
    <li>Todas las facturas en moneda nacional y extrangera emitidas hasta la fecha de emisión de la retención,
        &nbsp;especificada en el campo anterior.
    </li>
    <!--<li>Todas las facturas en moneda extrangera, si y sólo si está registrado en el sistema la-->
        <!--&nbsp;cotización correspondiente al día anterior a la fecha de emisión de la retención.-->
    <!--</li>-->
</ul>
<p>Los valores de la retención ingresados serán en moneda asociadas a la factura en cuestión.</p>
HTML;
$popupFactura = HtmlHelpers::trimmedHtml($popupFactura);


$script_head = <<<JS
var timbrado_detalle = "$model->timbrado_detalle_id";
var factura_id_selected = "{$model->factura_id}";
const retencion_id = '$model->id';
JS;
$this->registerJs($script_head, \yii\web\View::POS_HEAD);

$scripts = <<<JS
let actionID = "$actionID";
let tipo_documento_set_retencion_id = "$tipodoc_set_retencion_id";

function getTotalFieldNames() {
    const prefijo = "#total-iva-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]).concat(''));
    }
    return nombres;
}

/**
 * Muestra campos de iva en la cabecera.
 */
function renderFieldsTotalesIva() {
    let element_factura_id = document.getElementById('retencion-factura_id');
    let campos = getTotalFieldNames();
    if ($(element_factura_id).val() !== '') {
        $.ajax({
            url: $url_getTotalesField,
            type: 'get',
            data: {
                factura_id: $(element_factura_id).val(),
            },
            success: function (data) {
                console.log(data);
                $('#total-total-factura').val(data[data.length - 1]).trigger('change');
                $('#total-total-factura').prop('readonly', true);
                
                // poner retencion de RENTA
                if (actionID === 'create') {
                    $('#retencion-base_renta_porc').val(data[data.length - 2]).trigger('change');
                    $('#retencion-factor_renta_porc').val(30).trigger('change');
                }
                
                // poner retencion de IVA
                let i;
                for (i = 0; i < campos.length; i++) {
                    if (data.includes(campos[i])) {
                        $(campos[i]).parent().parent().parent()[0].style.display = "";
                        $(campos[i]).prop('readonly', true);
                        const regex = /#total-iva-([0-9]+)/gm;
                        let m = regex.exec(campos[i]);
                        let campo_iva_total = $(campos[i]); // asigno a variable porque sino se queja de que se usa variable mutable por la 'i'.
                        $.ajax({
                            url: $url_getTotalIvaMonto,
                            type: 'get',
                            data: {
                                porcentaje_iva: m[1],
                                factura_id: $('#retencion-factura_id').val(),
                                retencion_id: retencion_id,
                                action_id: actionID,
                            },
                            success: function (data) {
                                campo_iva_total.val(data).trigger('change');
                                let value = (data - (data / (1.0 + (m[1] / 100))));
                                if (m[1] !== '0') {
                                    $('td.iva_' + m[1] + ' :input.form-control')[0].value = parseFloat(value).toFixed(2); // fila de las bases
                                    $('td.iva_' + m[1] + ' :input.form-control').trigger('change');
                                }
                            }
                        });
                    } else {
                        $(campos[i]).val('').trigger('change');
                        $(campos[i]).parent().parent().parent()[0].style.display = "none";
                    }
                }
            }
        });
    }
}

/**
 * Muestra/oculta columnas de la tabla correspondiente a ivas.
 */
function showHideColumnsForIvas() {
    let factura_id = $('#retencion-factura_id').val();
    $.ajax({
        url: $url_getPorcentajeIvasFactura,
        type: 'get',
        data: {
            factura_id: factura_id,
        },
        success: function (data) { // retorna los porcentajes de ivas asociados a la factura.
            let porcentajes = JSON.parse("$porcentajes_iva");
            let arr = Object.values(porcentajes);
            arr.splice(arr.indexOf(0), 1);
            for (i = 0; i < arr.length; i++) { // se renderizan todos los campos por ivas y se ocultan aquellos cuyo iva asociado no concuerda con ninguno de los ivas asociados a la factura elegida.
                if (data.includes(arr[i])) {
                    $('td.iva_' + arr[i])[0].style.display = ""; // fila base
                    $('td.iva_' + arr[i])[1].style.display = ""; // fila factor
                    if (actionID === 'create') {
                        $('td.iva_' + arr[i] + ' :input.form-control')[1].value = 30.00; // fila factor
                        $('td.iva_' + arr[i] + ' :input.form-control').trigger('change');
                    } else {
                        let porc_iva = arr[i]; // desde succcess no funciona el arr[i]
                        
                        // Ajax para autocompletar ivas de la tabla si es update.
                        $.ajax({
                            url: $url_getFactor,
                            type: 'get',
                            data: {
                                iva_porcentaje: arr[i],
                                retencion_id: "$model->id",
                            },
                            success: function (data) {
                                console.log("factor es:", data);
                                $('td.iva_' + porc_iva + ' :input.form-control')[1].value = data; // fila factor. rellena con el valor correspondiente al detalleFactor
                                $('td.iva_' + porc_iva + ' :input.form-control').trigger('change');
                            }
                        });
                    }
                    $('td.iva_' + arr[i])[2].style.display = ""; // fila sumatory
                    $('th.iva_' + arr[i])[0].style.display = ""; // fila cabecera
                } else {
                    $('td.iva_' + arr[i])[0].style.display = "none"; // fila base
                    $('td.iva_' + arr[i])[1].style.display = "none"; // fila factor
                    $('td.iva_' + arr[i])[2].style.display = "none"; // fila sumatory
                    $('th.iva_' + arr[i])[0].style.display = "none"; // fila cabecera

                    // Limpiar campos
                    $('td.iva_' + arr[i] + ' :input.form-control')[0].value = ""; // fila base
                    $('td.iva_' + arr[i] + ' :input.form-control')[1].value = ""; // fila factor
                    $('td.iva_' + arr[i] + ' :input.form-control')[2].value = ""; // fila sumatory
                    $('td.iva_' + arr[i] + ' :input.form-control').trigger('change'); // fila sumatory
                }
            }
            
            // Completar campo de Renta %: Beatriz dice que es 30% del monto factura sin iva
            {
                let iva5 = $('#total-iva-5').val();
                let iva10 = $('#total-iva-10').val();
            }
            $.pjax.reload({container: "#flash_message_id", async: false});
        }
    });
}

function parm() {
    let fecha = $('#retencion-fecha_emision').val(); // sin el -disp, se obtiene en formato BD.
    // fecha = fecha.split('-').reverse().join('-');
    if ("{$actionID}" === 'create') {
        return {
            doMap: false,
            ids: "",
            condicion: null,
            concatRazonSocial: true,
            sinRetenciones: true,
            saldoGreaterThanZero: null, // al enviar false, se recibe desde el controller como 'false' y es string.
            entidad_id: "",
            fecha_hasta: fecha
        };
    } else {
        return {
            doMap: false,
            ids: "",
            condicion: null,
            concatRazonSocial: true,
            sinRetenciones: false,
            saldoGreaterThanZero: null, // al enviar false, se recibe desde el controller como 'false' y es string.
            entidad_id: "",
            fecha_hasta: fecha
        };
    }
}

$(document).on('change', '#retencion-fecha_emision-disp', function () {
    let factura_id = $('#retencion-factura_id');
    let fecha = $(this);

    factura_id.select2("destroy");
    factura_id.html("<option><option>");
    factura_id.select2({
        theme: 'krajee',
        placeholder: '',
        language: 'en',
        width: '100%',
        data: [],
    });

    // Tiene que definir si o si antes nro de retencion y una factura.
    if (fecha.val() !== "") {

        $.ajax({
            url: $url_getFacturas,
            type: 'get',
            data: parm(),
            success: function (data) {
                data = $.map(data, function (value, index) {
                    return {id: index, text: value};
                });

                // es el item necesario para que no se seleccione el primero por defecto.
                let arr = [{id: "", text: ''}];


                // Construir options para select2
                data.forEach(function (e, i) {
                    arr.push(e);
                });

                // Rellenar select2 con las opciones
                factura_id.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: arr,
                });

                if (factura_id_selected !== "")
                    factura_id.val(factura_id);
            }
        });
    }

});

//// Dejar como bck
//$(document).on('click', '.tiene_modal', (function (evt) {
//    
//    let nro = ($('#retencion-nro_retencion').val().substr(8, 15)).replace(new RegExp('_', 'g'), '');
//    
//    if (nro.length < 7) {
//        $("#modal").modal("hide");
//        $("modal-body").html("");
//        krajeeDialog.alert("El formato del número es incorrecto.");
//        evt.preventDefault();
//        return false;
//    }
//    
//    let boton = $(this);
//    let title = boton.attr('title');
//    let element = document.getElementById('retencion-nro_retencion');
//    let prefijo = $(element).val().substring(0, 7);
//    let lastgrp = $(element).val().substring(8, 15);
//    let factura_id = $("#retencion-factura_id").val();
//
//    if (factura_id === "" || lastgrp === "") {
//        $("#modal").modal("hide");
//        $("modal-body").html("");
//        krajeeDialog.alert("Falta definir " + (factura_id === "" ? 'una factura.' : 'el nro de retencion.'));
//        evt.preventDefault();
//        return false;
//    }
//
//    $.get(
//        boton.data('url') + '&prefijo=' + prefijo + '&nro=' + lastgrp + '&factura_id=' + factura_id + '&operacion=' + "$operacion",
//        function (data) {
//            let modal = $(boton.data('target'));
//            $('.modal-body', modal).html(data);
//            modal.modal();
//            $('.modal-header', modal).css('background', '#3c8dbc');
//            $('.modal-title', modal).html(title);
//        }
//    );
//}));

$('#retencion-factura_id').on('change', function () {
    getTimbrado();
    factura_id_selected = $(this).val();
    renderFieldsTotalesIva();
    showHideColumnsForIvas();

    // // ahora solamente existe 1 timbrado para retenciones
    // if ("{$operacion}" === 'venta') cargarTimbrados(); // el timbrado de retenciones de ventas dependen de la entidad asociada a la venta.
});

function getTimbrado() {
//    if ("{$actionID}" === 'create') {
    $.ajax({
        url: $url_getTimbrado,
        type: 'get',
        data: {
            factura_id: $('#retencion-factura_id').val(),
            nro_timbrado: $('#retencion-nro_timbrado').val(),
        },
        success: function (data) {
            let timbradoNroField = $('#retencion-timbrado_nro');
            if (data.error) {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
            if (data.result) {
                timbradoNroField.prop('readonly', true);
            } else {
                timbradoNroField.prop('readonly', false);                
            }
            timbradoNroField.val(data.result).trigger('change');
        }
    });
    // }
}

$('#retencion-nro_retencion').on('focusout', function () { // esto es para mostrar ocultar boton para agregar timbrado y etc...
    // cargarTimbrados(); // solamente existe 1 timbrado para retenciones.
    
    getTimbrado();
})

// Format nro retencion
    .keydown(function (event) {
        var text = $(this).val().replace(/_/g, "").replace(/-/g, ''); // borrar los underscores a la derecha
        var key = event.which;
        var esTeclaBorrar = key === 8;
        if (key > 57) key -= 96; else key -= 48; // convertir a un nro
        var lastgroupstart = 6; // index donde comienza el ultimo grupo de nro
        var lastgroup = text.substring(lastgroupstart, text.length); // el ultimo grupo de nro

        if (text.length > 5 && !esTeclaBorrar && (key > -1 && key < 10)) {
            if (lastgroup.length === 0) { // si es el 1er nro tecleado
                text = text.replaceAt(lastgroupstart, Array(7).join("0") + key);
            } else {
                var i, p = -1;
                // localizar donde comienza un nro <> 0
                for (i = 0; i < lastgroup.length; i++) {
                    if (lastgroup[i] !== '0') {
                        p = i;
                        break;
                    }
                }
                // si previamente no hubo ningún número, hacer un espacio forzadamente.
                if (lastgroup === '0000000') lastgroup = '000000';
                // extraer solamente los nros y concatenar el nro tecleado
                lastgroup = lastgroup.substring(p, lastgroup.length) + key; // si p > lastgroup.length, significa que no hubo ningún nro antes de keydown y no extraerá nada el substring.
                // agregar el ultimo nro tecleado, rellenando debidamente con ceros a la izquierda.
                text = text.replaceAt(lastgroupstart, Array((7 - lastgroup.length) + 1).join("0") + lastgroup)
            }
        }
        //if (parseInt(text.substring(lastgroupstart, text.length)) === 0){
        //   text = text.replaceAt(lastgroupstart, Array(7).join("0")+1);
        // }
        $(this).val(text);
    });
$('.sumable').on('change keyup', function () {
    let renta = $(".renta-porc :input.form-control");
    let cabeza = $(".renta-cabezas :input.form-control");
    let tonelada = $(".renta-toneladas :input.form-control");

    let total_retencion_renta = 0.0, total_retencion_rentacab = 0.0, total_retencion_rentatonelada = 0.0, i;

    renta_base = (renta[0].value !== "") ? parseFloat(renta[0].value.replace(/\./g, '').replace(/\,/g, '.')) : 0;
    renta_factor = (renta[1].value !== "") ? parseFloat(renta[1].value.replace(/\./g, '').replace(/\,/g, '.')) : 0;
    total_retencion_renta = Math.round(renta_base * renta_factor / (renta_factor >= 1 ? 100 : 1));
    delete renta_base;
    delete renta_factor;

    cabeza_base = (cabeza[0].value !== "") ? parseFloat(cabeza[0].value.replace(/\./g, '').replace(/\,/g, '.')) : 0;
    cabeza_factor = (cabeza[1].value !== "") ? parseFloat(cabeza[1].value.replace(/\./g, '').replace(/\,/g, '.')) : 0;
    total_retencion_rentacab = Math.round(cabeza_base * cabeza_factor/ (cabeza_factor >= 1 ? 100 : 1));
    delete cabeza_base;
    delete cabeza_factor;

    ton_base = (tonelada[0].value !== "") ? parseFloat(tonelada[0].value.replace(/\./g, '').replace(/\,/g, '.')) : 0;
    ton_factor = (tonelada[1].value !== "") ? parseFloat(tonelada[1].value.replace(/\./g, '').replace(/\,/g, '.')) : 0;
    total_retencion_rentatonelada = Math.round(ton_base * ton_factor/ (ton_factor >= 1 ? 100 : 1));
    delete ton_base;
    delete ton_factor;

    // TODO: Verificar si es simple sumatoria o se tiene que hacer como la columna de ivas.
    $('#total-renta-port').val(total_retencion_renta).trigger('change');
    $('#total-renta-cabezas').val(total_retencion_rentacab).trigger('change');
    $('#total-renta-toneladas').val(total_retencion_rentatonelada).trigger('change');

    let porcentajes = JSON.parse("$porcentajes_iva");
    let arr = Object.values(porcentajes);
    arr.splice(arr.indexOf(0), 1);
    for (i = 0; i < arr.length; i++) {
        if ($('td.iva_' + arr[i])[0].style.display === "") {
            let ivas = $('td.iva_' + arr[i] + '.sumable :input.form-control'), j;
            let iva_total = 0.0;
            iva_total = parseFloat(ivas[0].value.replace(/\./g, '').replace(/\,/g, '.')) * parseFloat(ivas[1].value.replace(/\./g, '').replace(/\,/g, '.')) / 100.0;
            // for(j=0; j<ivas.length; j++) {
            //     iva_total += ivas[j].value;
            // }
            $('#total-total-iva-' + arr[i] + '').val(parseFloat(iva_total).toFixed(2)).trigger('change');
        }
    }
});

$('.popover-title').css('background', '#ffff99');

// Metodo prototipo personal
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
};

$(document).ready(function () {
    $('.iva-__x__').hide();
    renderFieldsTotalesIva();
    showHideColumnsForIvas();
    // cargarTimbrados(); // solamente existe 1 timbrado para retenciones.

    // // Dejar como backup
    // let a = $("#retencion-nro_retencion").parent().parent()[0];
    // $('<div class="col-sm-1"><div class="form-group field-button-retencion-add-timbrado" style="text-align: center;">$boton<div class="help-block"></div></div></div>').insertAfter(a);

    if (['create', 'update'].includes("{$actionID}"))
        $('#retencion-fecha_emision-disp').focus();  

    applyPopOver($('span.factura'), 'Observación', "{$popupFactura}", {'placement': 'bottom', 'title-css': {'background-color' : "#{$infoColor}", 'color': 'white', 'font-weight': 'bold', 'text-align': 'center'}});
    applyPopOver($('span.renta'), 'Información', 'Por defecto es el 30% sobre el monto gravado de la factura.', {'placement': 'top', 'title-css': {'background-color' : "#{$infoColor}", 'color': 'white', 'font-weight': 'bold', 'text-align': 'center'}});
});
JS;
$this->registerJs($scripts);

$css = <<<CSS
.popover-title {
    background-color: #d9edf7;
    color: #31708f;
}
CSS;
$this->registerCss($css);

?>
