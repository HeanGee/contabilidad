<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\searchRetencionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="retencion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'fecha_emision') ?>

    <?= $form->field($model, 'nro_retencion') ?>

    <?= $form->field($model, 'base_renta_porc') ?>

    <?= $form->field($model, 'base_renta_cabezas') ?>

    <?php // echo $form->field($model, 'base_renta_toneladas') ?>

    <?php // echo $form->field($model, 'factor_renta_porc') ?>

    <?php // echo $form->field($model, 'factor_renta_cabezas') ?>

    <?php // echo $form->field($model, 'factor_renta_toneladas') ?>

    <?php // echo $form->field($model, 'factura_compra_id') ?>

    <?php // echo $form->field($model, 'factura_venta_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
