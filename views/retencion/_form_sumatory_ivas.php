<?php

use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\Retencion;
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

/**
 * Created by PhpStorm.
 * User: Hean Gky
 * Date: 14/07/2018
 * Time: 14:31
 */

/* @var $form yii\widgets\ActiveForm */
/* @var $model Retencion */

try {
    $columnas = [];
    $ivaCuentas = IvaCuenta::find()->all();

    /** @var IvaCuenta $ivaCuenta */
    foreach ($ivaCuentas as $ivaCuenta) {
        if ($ivaCuenta->iva->porcentaje != 0) {
            $columnas[$ivaCuenta->iva->porcentaje] =
                Html::beginTag('td', ['class' => 'sumatory iva_' . $ivaCuenta->iva->porcentaje . ' iva-__x__']) .
                MaskedInput::widget([
                    'name' => 'total-total-iva-' . $ivaCuenta->iva->porcentaje,
                    'id' => 'total-total-iva-' . $ivaCuenta->iva->porcentaje,
//                    'readonly' => true,
                    'value' => '',
                    'clientOptions' => [
                        'rightAlign' => true,
                        'alias' => 'decimal',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'autoGroup' => true
                    ],
                    'options' => [
                        'readonly' => true,
                        'class' => 'form-control',
                    ]
                ]) .
                Html::endTag('td');
        }
    }
    ksort($columnas);
    foreach ($columnas as $columna) {
        echo $columna;
    }
} catch (Exception $e) {
    throw $e;
}
