<?php

use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\models\CotizacionArchivo */
/* @var $form yii\widgets\ActiveForm */
/* @var $mensaje String */

$this->title = 'Carga de Retenciones';
?>

<div class="movimiento-form">

    <?php $form = ActiveForm::begin(['id' => 'subir_arch_form', 'options' => ['enctype' => 'multipart/form-data']]);

    try {
        $submit = PermisosHelpers::getAcceso('retencion-create-file') ? Html::submitButton('Enviar', ['class' => 'btn btn-primary']) : '';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'attributes' => [
                        'archivo' => [
                            'type' => Form::INPUT_FILE,
                            'value' => $form->field($model, 'archivo')->widget(FileInput::classname(),
                                [
                                    'language' => 'es',
                                    'pluginOptions' => ['showPreview' => false],
                                ]
                            ),
                            'label' => 'Archivo (.csv, .xls, .xlsx)'
                        ],
                    ],
                ],
                [
                    'attributes' => [
                        'action' => [
                            'type' => Form::INPUT_RAW,
                            'value' => '<div style="margin-top: 20px">' .
                                $submit .
                                '</div>'
                        ],
                    ],
                ]
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>


    <br/>
    <h3>Documentos de interés</h3>
    <h4><a href="../modules/contabilidad/views/retencion/archivos/ejemplo.xlsx">Ejemplo</a></h4>

    <br/>

    <?php ActiveForm::end(); ?>
    <?php
    if ($mensaje != "") : ?>
        <fieldset id="error-msg-fieldset">
            <legend>Panel de Mensajes</legend>
            <?= $mensaje ?>
        </fieldset>
    <?php endif;

    $script = <<<JS
$('#retencionarchivo-archivo').on('click', function () {
    $('fieldset#error-msg-fieldset').remove();
});
JS;
    $this->registerJs($script);
    ?>
</div>
