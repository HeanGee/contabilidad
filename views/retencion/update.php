<?php

/* @var $this yii\web\View */

/* @var $model backend\modules\contabilidad\models\Retencion */
///* @var $detalles_base RetencionDetalleBaseIvas[] */
///* @var $detalles_factor RetencionDetalleFactorIvas[] */

$this->title = 'Modificar Retencion: ' . $model->nro_retencion;
$this->params['breadcrumbs'][] = ['label' => 'Retenciones de ' . ucfirst(Yii::$app->getRequest()->getQueryParam('operacion')), 'url' => ['index', 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="retencion-update">

    <?= $this->render('_form', [
        'model' => $model,
//        'bases' => $detalles_base,
//        'factors' => $detalles_factor
    ]) ?>

</div>
