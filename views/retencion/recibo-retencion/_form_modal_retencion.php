<?php

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\RetencionDetalleBaseIvas;
use backend\modules\contabilidad\models\RetencionDetalleFactorIvas;
use backend\modules\contabilidad\models\TipoDocumentoSet;
use backend\modules\contabilidad\models\Venta;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model_retencion Retencion */
/* @var $bases RetencionDetalleBaseIvas[] */
/* @var $factors RetencionDetalleFactorIvas[] */
/* @var $factura_id string */
/* @var $retencion_id string */
?>
<div class="alert alert-warning">
    <ul>
        <li><strong>Atención!</strong> Esta acción se aplicará directamente en la base de datos.</li>
        <li><strong>Atención!</strong> Si no elige una retención existente, por defecto se crea una nueva.</li>
    </ul>
</div>
<div class="detalle-venta-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php
    $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
    $actionID = Yii::$app->controller->action->id;

    $form = ActiveForm::begin([
        'id' => '_form_modal_retencion',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['data' => ['disabled-text' => 'Guardando...'], 'class' => 'btn btn-success']) ?>
    </div>
    <div class="form-group">
        <?php try {
            echo $form->field($model_retencion, 'retencion_id_selector')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Seleccione una retencion ...'],
                'initValueText' => $model_retencion->retencion_id_selector != null ?
                    $model_retencion->id . ' - Retención Nª ' . $model_retencion->nro_retencion . ' del ' . date("d-m-Y", strtotime($model_retencion->fecha_emision)) :
                    '',
                'pluginOptions' => [
                    'allowClear' => true,
                    'ajax' => [
                        'url' => Url::to(['retencion/get-retenciones-by-factura', 'factura_id' => $factura_id, 'operacion' => $operacion]),
                        'dataType' => 'json',
                        'data' => new JsExpression("
                            function(params) { 
                                return {
                                    q:params.term, 
                                    id: '',
                                };
                            }
                        "),
                    ]
                ],
                'value' => !empty($model_retencion->retencion_id_selector) ? $model_retencion->retencion_id_selector : "",
            ]);
        } catch (Exception $exception) {
            print $exception;
        } ?>
    </div>

    <?php try {
//            echo $form->errorSummary($model_retencion);
        $campo_factura_id = $operacion == 'compra' ? 'factura_compra_id' : 'factura_venta_id';
        $query = $operacion == 'compra' ?
            Compra::find()->where(['compra.id' => $factura_id])->alias('compra')->joinWith('entidad as entidad')->select([
                'compra.id as id',
                "CONCAT((compra.nro_factura), ('('), (entidad.razon_social), (')')) AS text",
                'compra.entidad_id as entidad_id',
            ])->asArray()->all() :
            Venta::find()->where(['venta.id' => $factura_id])->alias('venta')->joinWith('entidad as entidad')->select([
                'venta.id as id',
                "CONCAT((venta.prefijo), ('-'), (venta.nro_factura), ('('), (entidad.razon_social), (')')) AS text",
                'venta.entidad_id as entidad_id',
            ])->asArray()->all();
        $data = ArrayHelper::map($query, 'id', 'text');

        echo Form::widget([
            'model' => $model_retencion,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'fecha_emision' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => DateControl::class,
                    'options' => [
                        'type' => DateControl::FORMAT_DATE,
                        'ajaxConversion' => false,
                        'language' => 'es',
                        'widgetOptions' => [
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-MM-yyyy',
                                'todayHighlight' => true,
                                'weekStart' => '0',
                            ],
                        ],

                    ]
                ],
                'factura_id' => [
                    'type' => Form::INPUT_RAW,
                    'value' => $form->field($model_retencion, 'factura_id')->widget(Select2::className(), [
                        'data' => $data,
                        'disabled' => true,
                        'options' => ['placeholder' => ''],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'pluginEvents' => [],
                    ]),
                ],
                'nro_retencion' => [
                    'type' => Form::INPUT_RAW,
                    'value' => $form->field($model_retencion, 'nro_retencion')->widget(MaskedInput::className(), [
                        'mask' => '999-999-9999999',
                        'options' => [
                            'class' => 'form-control',
                            'disabled' => false,
                        ]
                    ]),
                    'options' => ['placeholder' => 'Ingrese nro de retencion...'],
                    'label' => 'Nº de retención'
                ],
                'timbrado_nro' => [
                    'type' => Form::INPUT_TEXT,
                ],
//                'timbrado_detalle_id' => [
//                    'type' => Form::INPUT_HIDDEN
//                ]
            ],
        ]);

        $totales = [];
        /** @var IvaCuenta $iva_cta */
        foreach (IvaCuenta::find()->all() as $iva_cta) {
            if ($iva_cta->iva->porcentaje != 0)
                $totales['iva-' . $iva_cta->iva->porcentaje . ''] = [
                    'label' => 'Total ' . (($iva_cta->iva->porcentaje !== 0) ? ' ' . $iva_cta->iva->porcentaje . '%' : 'Exenta'),
                    'columnOptions' => ['colspan' => '2'],
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => MaskedInput::className(),
                    'options' => [
                        'clientOptions' => [
                            'rightAlign' => true,
                            'alias' => 'decimal',
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'autoGroup' => true
                        ],
                        'options' => [
                            'class' => 'form-control',
                        ]
                    ],
                ];
        }
        // Ordenar campos
        uksort($totales, 'strnatcasecmp');
        echo Form::widget([
            'formName' => 'total',
            'columns' => 6,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['colspan' => '2'],
                'inputContainer' => ['colspan' => '2'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => $totales,
        ]);

        $total_saldo['total-factura'] = [
            'label' => 'Total Factura',
            'columnOptions' => ['colspan' => '2'],
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => MaskedInput::className(),
            'options' => [
                'clientOptions' => [
                    'rightAlign' => true,
                    'alias' => 'decimal',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'autoGroup' => true
                ],
                'options' => [
                    'class' => 'form-control',
                ]
            ],
        ];
        $total_saldo['saldo-factura'] = [
            'label' => 'Saldo Factura',
            'columnOptions' => ['colspan' => '2'],
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => MaskedInput::className(),
            'options' => [
                'clientOptions' => [
                    'rightAlign' => true,
                    'alias' => 'decimal',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'autoGroup' => true
                ],
                'options' => [
                    'class' => 'form-control',
                ]
            ],
        ];
        echo Form::widget([
            'formName' => 'total',
            'columns' => 4,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['colspan' => '2'],
                'inputContainer' => ['colspan' => '2'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => $total_saldo,
        ]);

        $boton = Html::label('Agregar', 'boton_add_timbrado', ['class' => 'control-label']) . '<br/>'
            . Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'id' => 'boton_add_timbrado',
                'style' => "display: yes;",
                'type' => 'button',
                'title' => 'Agregar.',
                'class' => 'btn btn-success tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['timbrado/create-new-timbrado-retencion']),
                'data-pjax' => '0',
            ]);
        echo Html::beginTag('div', ['class' => 'panel panel-primary']);
//        echo Html::beginTag('div', ['class' => 'panel-heading']);
//        echo Html::beginTag('h3', ['class' => 'panel-title']) . 'PANEL DE RETENCIONES';
//        echo Html::endTag('h3');
//        echo Html::endTag('div');
        echo Html::beginTag('div', ['class' => 'panel-body']);
        echo Html::beginTag('table', ['class' => 'table table-condensed', 'id' => 'retencion']);
        echo Html::beginTag('thead');
        echo Html::beginTag('tr', ['class' => 'title-row']);
        echo Html::tag('th', "Retenciones (En Guaranies)", [/*'width' => '10%'*/]);
        echo Html::tag('th', 'Renta %', ['style' => "text-align: center;"]);
        echo Html::tag('th', 'Renta (cabezas)', ['style' => "text-align: center;"]);
        echo Html::tag('th', 'Renta (toneladas)', ['style' => "text-align: center;"]);
        // TODO: render de cabeceras por ivas.
        echo $this->render('_form_cabecera_ivas', ['model' => $model_retencion, 'form' => $form]);
        echo Html::endTag('tr');
        echo Html::endTag('thead');

        echo Html::beginTag('tbody');
        echo Html::beginTag('tr', ['class' => 'first-row']);
        echo Html::tag('td', 'Base de la Retención', ['style' => "font-weight: bold; text-align: left; padding-top: 15px; font-style: italic;"]);
        echo Html::beginTag('td', ['class' => 'base-sumable sumable renta-porc']);
        echo $form->field($model_retencion, 'base_renta_porc')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'base-sumable sumable renta-cabezas']);
        echo $form->field($model_retencion, 'base_renta_cabezas')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'base-sumable sumable renta-toneladas last-column']);
        echo $form->field($model_retencion, 'base_renta_toneladas')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        // TODO: renderizar columnas de ivas.
        echo $this->render('_form_base_ivas', ['model' => $model_retencion, 'form' => $form, 'bases' => $bases]);
        echo Html::endTag('tr');
        echo Html::beginTag('tr', ['class' => 'last-row']);
        echo Html::tag('td', 'Factor de la Retención', ['style' => "font-weight: bold; text-align: left; padding-top: 15px; font-style: italic;"]);
        echo Html::beginTag('td', ['class' => 'factor-sumable sumable renta-porc']);
        echo $form->field($model_retencion, 'factor_renta_porc')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'factor-sumable sumable renta-cabezas']);
        echo $form->field($model_retencion, 'factor_renta_cabezas')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'factor-sumable sumable renta-toneladas last-column']);
        echo $form->field($model_retencion, 'factor_renta_toneladas')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'class' => 'form-control',
            ]
        ])->label(false);
        echo Html::endTag('td');
        // TODO: renderizar columnas de iva.
        echo $this->render('_form_factor_ivas', ['model' => $model_retencion, 'form' => $form, 'factors' => $factors]);
        echo Html::endTag('tr');
        echo Html::beginTag('tr', ['class' => 'sumatory-row']);
        echo Html::tag('td', 'Total Retenido', ['style' => "font-weight: bold; text-align: left; padding-top: 15px;"]);
        echo Html::beginTag('td', ['class' => 'sumatory']);
        echo MaskedInput::widget([
            'name' => 'total-renta-porc',
            'id' => 'total-renta-port',
//            'readonly' => true,
            'value' => '',
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'readonly' => true,
                'class' => 'form-control',
            ]
        ]);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'sumatory']);
        echo MaskedInput::widget([
            'name' => 'total-renta-cabezas',
            'id' => 'total-renta-cabezas',
//            'readonly' => true,
            'value' => '',
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'readonly' => true,
                'class' => 'form-control',
            ]
        ]);
        echo Html::endTag('td');
        echo Html::beginTag('td', ['class' => 'sumatory']);
        echo MaskedInput::widget([
            'name' => 'total-renta-toneladas',
            'id' => 'total-renta-toneladas',
//            'readonly' => true,
            'value' => '',
            'clientOptions' => [
                'rightAlign' => true,
                'alias' => 'decimal',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'autoGroup' => true
            ],
            'options' => [
                'readonly' => true,
                'class' => 'form-control',
            ]
        ]);
        echo Html::endTag('td');
        // TODO: render campos totales por ivas.
        echo $this->render('_form_sumatory_ivas', ['model' => $model_retencion, 'form' => $form]);
        echo Html::endTag('tr');
        echo Html::endTag('tbody');
        echo Html::endTag('table');
        echo Html::endTag('div');
        echo Html::endTag('div');
    } catch (Exception $e) {
        print $e;
    } ?>

    <?php ActiveForm::end(); ?>

    <?php
    $CSS = <<<CSS
#retencion table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
#retencion tbody tr:hover {
    background-color: #ebffdd;
}
#retencion td, #retencion th {
    /*border: 1px solid #ddd;*/
    padding: 8px;
}
#retencion tr.last-row {
    /*border-bottom: 2px solid black;*/
}
#retencion tbody tr.first-row {
    /*border-top: 2px solid black;*/
}
CSS;
    $this->registerCss($CSS);

    $porcentajes_iva = [];
    $ivas_por = IvaCuenta::find()->all();
    foreach ($ivas_por as $iva_por) if ($iva_por->iva->porcentaje != 0) $porcentajes_iva[] = $iva_por->iva->porcentaje;
    asort($porcentajes_iva);
    $porcentajes_iva = array_values($porcentajes_iva);
    $porcentajes_iva = Json::encode($porcentajes_iva);
    $empresa_id = \Yii::$app->session->get('core_empresa_actual');
    $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
    $nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-tipodoc_set_retencion";
    $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
    $tipodoc_set_retencion_id = "";
    if ($parametro_sistema != null) {
        $tipodoc_set_retencion = TipoDocumentoSet::findOne(['id' => $parametro_sistema->valor]);
        if ($tipodoc_set_retencion != null) $tipodoc_set_retencion_id = $tipodoc_set_retencion->id;
    }
    $id_modeloActual = $model_retencion->id;
    $url_getFactor = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-factor', 'operacion' => $operacion])));
    $url_getTimDetID = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-timbrado-detalle-id', 'operacion' => $operacion])));
    $url_getTimbrado = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-timbrado', 'operacion' => $operacion])));
    $url_getTotalesField = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-totales-fields', 'operacion' => $operacion])));
    $url_getTotalIvaMonto = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-total-iva-monto', 'operacion' => $operacion])));
    //    $url_checkNroRetencion = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/check-nro-retencion-2', 'operacion' => $operacion])));
    $url_manejarRetenciones = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/manejar-retenciones', 'operacion' => $operacion])));
    $url_getFacturaByEntidad = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-facturas-by-entidad', 'operacion' => $operacion])));
    $url_getPorcentajeIvasFactura = Json::htmlEncode(\Yii::t('app', Url::to(['retencion/get-porcentaje-ivas-factura', 'operacion' => $operacion])));
    $scripts = <<<JS
function getTotalFieldNames() {
    const prefijo = "#total-iva-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}

function renderFieldsTotalesIva() {
    let id = "retencion-" + "factura_id";
    let element_factura_id = document.getElementById(id);
    let campos = getTotalFieldNames();
    if ($(element_factura_id).val() !== '') {
        $.ajax({
            url: $url_getTotalesField,
            type: 'get',
            data: {
                factura_id: $(element_factura_id).val(),
            },
            success: function (data) {
                $('#total-total-factura').val(data[data.length - 1]).trigger('change');
                $('#total-total-factura').prop('readonly', true);
                $('#total-saldo-factura').val(data[data.length - 3]).trigger('change');
                $('#total-saldo-factura').prop('readonly', true);
                if ($('#retencion-retencion_id_selector').val() === '') {
                    $('#retencion-base_renta_porc').val(data[data.length - 2]).trigger('change'); // lo que el usuario haya puesto es lo correcto.
                }
                let i, data2 = [];
                for (i = 0; i < data.length; i++) {
                    let regex0 = /(-disp)/gm;
                    let m0 = regex0.exec(data[i]);
                    if (m0 !== null)
                        if (m0[1] !== null) data2.push(data[i].substring(0, (data[i].length - m0[1].length)));
                        else data2.push(data[i]);
                    else
                        data2.push(data[i]);
                }
                for (i = 0; i < campos.length; i++) {
                    if (data2.includes(campos[i])) {
                        let campo_id = campos[i];
                        $(campo_id).parent().parent()[0].style.display = "block";
                        $(campo_id).prop('readonly', true);
                        const regex2 = /#total-iva-([0-9]+)/gm;
                        let m2 = regex2.exec(campos[i]);
                        let campo_iva_total = $(campos[i]); // asigno a variable porque sino se queja de que se usa variable mutable por la 'i'.
                        $.ajax({
                            url: $url_getTotalIvaMonto,
                            type: 'get',
                            data: {
                                porcentaje_iva: m2[1],
                                factura_id: $(element_factura_id).val(),
                            },
                            success: function (data) {
                                campo_iva_total.val(data).trigger('change');
                                $('td.iva_' + m2[1] + ' :input.form-control')[0].value = Math.round(data - (data / (1.0 + (m2[1] / 100)))); // fila de las bases
                                $('td.iva_' + m2[1] + ' :input.form-control').trigger('change');
                            }
                        });
                    } else {
                        $(campos[i]).val('').trigger('change');
                        $(campos[i]).parent().parent()[0].style.display = "none";
                    }
                }
            }
        });
    }
}

function showHideColumnsForIvas() {
    let id = '#retencion-' + "factura_id";
    let factura_id = $(id).val();
    $.ajax({
        url: $url_getPorcentajeIvasFactura,
        type: 'get',
        data: {
            factura_id: factura_id,
        },
        success: function (data) { // retorna los porcentajes de ivas asociados a la factura, excepto el iva 0
            if (data.length > 0) {
                let porcentajes = JSON.parse("$porcentajes_iva");
                let arr = Object.values(porcentajes);
                for (i = 0; i < arr.length; i++) { // se renderizan todos los campos por ivas y se ocultan aquellos cuyo iva asociado no concuerda con ninguno de los ivas asociados a la factura elegida.
                    if (data.includes(arr[i])) {
                        $('td.iva_' + arr[i])[0].style.display = ""; // fila base
                        $('td.iva_' + arr[i])[1].style.display = ""; // fila factor
                        let action_id = "$actionID";
                        if (action_id === 'create' || $('#retencion-retencion_id_selector').val() === "") {
                            $('td.iva_' + arr[i] + ' :input.form-control')[1].value = 30;
                            $('td.iva_' + arr[i] + ' :input.form-control').trigger('change');
                        }
                        else {
                            let porc_iva = arr[i]; // desde succcess no funciona el arr[i]
                            $.ajax({
                                url: $url_getFactor,
                                type: 'get',
                                data: {
                                    iva_porcentaje: arr[i],
                                    retencion_id: "$id_modeloActual",
                                },
                                success: function (data) {
                                    $('td.iva_' + porc_iva + ' :input.form-control')[1].value = data; // fila factor. rellena con el valor correspondiente al detalleFactor
                                    $('td.iva_' + porc_iva + ' :input.form-control').trigger('change');
                                }
                            });
                        }
                        $('td.iva_' + arr[i])[2].style.display = ""; // fila sumatory
                        $('th.iva_' + arr[i])[0].style.display = ""; // fila cabecera
                    } else {
                        $('td.iva_' + arr[i])[0].style.display = "none"; // fila base
                        $('td.iva_' + arr[i])[1].style.display = "none"; // fila factor
                        $('td.iva_' + arr[i])[2].style.display = "none"; // fila sumatory
                        $('th.iva_' + arr[i])[0].style.display = "none"; // fila cabecera
    
                        // Limpiar campos
                        $('td.iva_' + arr[i] + ' :input.form-control')[0].value = ""; // fila base
                        $('td.iva_' + arr[i] + ' :input.form-control')[1].value = ""; // fila factor
                        $('td.iva_' + arr[i] + ' :input.form-control')[2].value = ""; // fila sumatory
                        $('td.iva_' + arr[i] + ' :input.form-control').trigger('change'); // fila sumatory
                    }
                }
            } else {
                krajeeDialog.alert('Esta factura solamente registra monto por total exenta del iva.');
            }
            $.pjax.reload({container: "#flash_message_id", async: false});
        }
    });
}

//function seleccionarTimbrado() {
//    $.ajax({
//        url: $url_getTimDetID,
//        type: 'get',
//        data: {
//            nro_retencion: $('#retencion-nro_retencion').val(),
//            factura_id: $("#retencion-factura_id").val(),
//            operacion: "{$operacion}",
//            action_id: "{$actionID}",
//            retencion_id: "{$model_retencion->id}",
//        },
//        success: function (data) {
//            $.pjax.reload({container: "#flash_message_id", async: false});
//            if (data !== false && data !== "") {
//                // console.log('|'+data+'|');
//                $('#retencion-timbrado_detalle_id').val(data).trigger('change');
//            }
//        },
//    });
//}

$(document).ready(function () {
    $('.iva-__x__').hide();
    renderFieldsTotalesIva();
    showHideColumnsForIvas();
    // let a = $("#total-total-factura-disp").parent().parent().parent()[0];
    // $('<div class="col-sm-1"><div class="form-group field-button-retencion-add-timbrado" style="text-align: center;">"aqui va el boton"<div class="help-block"></div></div></div>').insertAfter(a);
    if ($('#retencion-retencion_id_selector').val() !== "") {
        $('#retencion-nro_retencion').prop('readonly', true);
        $('#retencion-fecha_emision-disp').prop('disabled', true);
    } else {
        $('#retencion-nro_retencion').prop('readonly', false);
        $('#retencion-fecha_emision-disp').prop('disabled', false);
    }
});

$('#retencion-' + "factura_id").on('change', function () {
    renderFieldsTotalesIva();
    showHideColumnsForIvas();
    // if ("{$operacion}" === 'venta') seleccionarTimbrado(); // el timbrado de retenciones de ventas dependen de la entidad asociada a la venta.
});
// $('#retencion-nro_retencion').on('focusout', function () {
//     // seleccionarTimbrado();
// });

$('.sumable').on('change keyup', function () {
    let renta = $(".renta-porc :input.form-control");
    let cabeza = $(".renta-cabezas :input.form-control");
    let tonelada = $(".renta-toneladas :input.form-control");

    let total_retencion_renta = 0.0, total_retencion_rentacab = 0.0, total_retencion_rentatonelada = 0.0, i;

    renta_base = (renta[0].value !== "") ? parseFloat(renta[0].value.replace(/\./g, '').replace(/\,/g, '.')) : 0;
    renta_factor = (renta[1].value !== "") ? parseFloat(renta[1].value.replace(/\./g, '').replace(/\,/g, '.')): 0;
    
    total_retencion_renta = Math.round(renta_base * renta_factor / (renta_factor >= 1 ? 100 : 1));
    delete renta_base;
    delete renta_factor;

    // TODO: Verificar si es simple sumatoria o se tiene que hacer como la columna de ivas.
    $('#total-renta-port').val(total_retencion_renta).trigger('change');
    $('#total-renta-cabezas').val(total_retencion_rentacab).trigger('change');
    $('#total-renta-toneladas').val(total_retencion_rentatonelada).trigger('change');

    let porcentajes = JSON.parse("$porcentajes_iva");
    let arr = Object.values(porcentajes);
    for (i = 0; i < arr.length; i++) {
        if ($('td.iva_' + arr[i])[0].style.display === "") {
            let ivas = $('td.iva_' + arr[i] + '.sumable :input.form-control'), j;
            let iva_total = 0.0;
            let formatted_multiplicando = ivas[0].value.replace('.', '').replace(',', '.');
            let formatted_multiplicador = ivas[1].value.replace('.', '').replace(',', '.');
            iva_total = parseFloat(formatted_multiplicando) * parseFloat(formatted_multiplicador) / 100.0;
            // for(j=0; j<ivas.length; j++) {
            //     iva_total += ivas[j].value;
            // }
            $('#total-total-iva-' + arr[i]).val(iva_total).trigger('change');
        }
    }
});

// obtener la id del formulario y establecer el manejador de eventos
$("form#_form_modal_retencion").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            // TODO: recargar la fila correspondiente.
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
            // TODO: Refrescar filas de facturas.
            let campos_monto = $('tr.fila-facturas :input[id$="monto"]'), i;            
            
            for (i = 0; i < campos_monto.length; i++) {
                let element_campo_monto = $(campos_monto[i]);
                const regex = /Detalles_new([0-9]+)_/gm;
                let m = regex.exec(element_campo_monto.attr('id'));
                let campo_factura_id = m[0] + "factura_compra_id";
                let element = document.getElementById(campo_factura_id);
                if (element === null)
                    campo_factura_id = m[0] + "factura_venta_id";


                let inputs_factura_id = $('.fila-facturas :input[name$="[factura_venta_id]"]');
                let array_factura_id = [], j;
                for (j = 0; j < (inputs_factura_id.length - campos_monto.length + i); j++) {
                    array_factura_id.push(inputs_factura_id[j].value);
                }
                $.ajax({
                    url: $url_getFacturaByEntidad,
                    type: 'get',
                    data: {
                        entidad_id: $('#recibo-entidad_id').val(),
                        facturas_id: array_factura_id,                        
                        fecha_recibo: $('#recibo-fecha').val(), // -disp envia d-m-Y, sin -disp envia Y-m-d
                        // TODO: decidir si enviar o no actionID del recibo, porque cuando actualiza el saldo, carga tambien todas las facturas ya que no envia el actionId
                    },
                    success: function (data) {
                        $('#' + m[0] + "factura_id").select2('destroy');
                        $('#' + m[0] + "factura_id").select2({
                            theme: 'krajee',
                            placeholder: '',
                            language: 'en',
                            width: '100%',
                            data: data,
                        });
                        $('#' + m[0] + "factura_id").attr("selected","selected");
                        $('#' + m[0] + "factura_id").trigger('change');
                    }
                });
            }
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$('#modal').on('shown.bs.modal', function () {
    // TODO: Comportamiento deseado luego de la animacion de aparicion del modal.
    let timbradoNroField = $('#retencion-timbrado_nro');
    if (timbradoNroField.val() !== '') {
        timbradoNroField.prop('readonly', true);
    } else {
        timbradoNroField.prop('readonly', false);                
    }
});

$('#retencion-retencion_id_selector').on('change', function() {
    let id_ret_selected = $(this).val();
    let url = $url_manejarRetenciones + '&factura_id=' + "$factura_id" + '&retencion_id=' + $('#retencion-retencion_id_selector').val();
    if (id_ret_selected === "") {
        url = url.concat('&quiere_crear_nuevo=' + "si");
    }
    console.log(url);
    $.ajax({
        url: url,
        type: 'get',
        data: {},
        success: function (data) {
            $('.modal-body', modal).html("");
            $('.modal-body', modal).html(data);
            $('#modal').trigger('change');
            
            let timbradoNroField = $('#retencion-timbrado_nro');
            if (timbradoNroField.val() !== '') {
                timbradoNroField.prop('readonly', true);
            } else {
                timbradoNroField.prop('readonly', false);                
            }
        }
    })
});

// Format nro retencion
$("#retencion-nro_retencion").keydown(function (event) {
    console.log('entra al evento');
    var text = $(this).val().replace(/_/g, "").replace(/-/g, ''); // borrar los underscores a la derecha
    var key = event.which;
    var esTeclaBorrar = key === 8;
    if (key > 57) key -= 96; else key -= 48; // convertir a un nro
    var lastgroupstart = 6; // index donde comienza el ultimo grupo de nro
    var lastgroup = text.substring(lastgroupstart, text.length); // el ultimo grupo de nro

    if (text.length > 5 && !esTeclaBorrar && (key > -1 && key < 10)) {
        if (lastgroup.length === 0) { // si es el 1er nro tecleado
            text = text.replaceAt(lastgroupstart, Array(7).join("0") + key);
        } else {
            var i, p = -1;
            // localizar donde comienza un nro <> 0
            for (i = 0; i < lastgroup.length; i++) {
                if (lastgroup[i] !== '0') {
                    p = i;
                    break;
                }
            }
            // si previamente no hubo ningún número, hacer un espacio forzadamente.
            if (lastgroup === '0000000') lastgroup = '000000';
            // extraer solamente los nros y concatenar el nro tecleado
            lastgroup = lastgroup.substring(p, lastgroup.length) + key; // si p > lastgroup.length, significa que no hubo ningún nro antes de keydown y no extraerá nada el substring.
            // agregar el ultimo nro tecleado, rellenando debidamente con ceros a la izquierda.
            text = text.replaceAt(lastgroupstart, Array((7 - lastgroup.length) + 1).join("0") + lastgroup)
        }
    }
    //if (parseInt(text.substring(lastgroupstart, text.length)) === 0){
    //   text = text.replaceAt(lastgroupstart, Array(7).join("0")+1);
    // }
    $(this).val(text);
}).focusout(function () {
    getTimbrado();
});

function getTimbrado() {
//    if ("{$actionID}" === 'create') {
    $.ajax({
        url: $url_getTimbrado,
        type: 'get',
        data: {
            factura_id: $('#retencion-factura_id').val(),
            nro_timbrado: $('#retencion-nro_timbrado').val(),
        },
        success: function (data) {
            let timbradoNroField = $('#retencion-timbrado_nro');
            if (data.error) {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
            if (data.result) {
                timbradoNroField.prop('readonly', true);
            } else {
                timbradoNroField.prop('readonly', false);                
            }
            timbradoNroField.val(data.result).trigger('change');
        }
    });
    // }
}

// Metodo prototipo personal
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
};
JS;

    $this->registerJs($scripts);
    ?>
</div>
