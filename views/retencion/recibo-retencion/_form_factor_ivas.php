<?php

use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\RetencionDetalleFactorIvas;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

/**
 * Created by PhpStorm.
 * User: Hean Gky
 * Date: 14/07/2018
 * Time: 14:31
 */

/* @var $form yii\widgets\ActiveForm */
/* @var $model Retencion */
/* @var $factors RetencionDetalleFactorIvas[] */

try {
    $columnas = [];
    $ivaCuentas = IvaCuenta::find()->all();
    $actionID = Yii::$app->controller->action->id;

    /** @var IvaCuenta $ivaCuenta */
    foreach ($ivaCuentas as $ivaCuenta) {
        if ($ivaCuenta->iva->porcentaje != 0) {
            $columnas[$ivaCuenta->iva->porcentaje] =
                Html::beginTag('td', ['class' => 'factor-sumable sumable iva_' . $ivaCuenta->iva->porcentaje . ' iva-__x__']) .
                Html::beginTag('div', ['class' => 'form-group field-retencion-form_iva-' . $ivaCuenta->iva->porcentaje]) .
                MaskedInput::widget([
                    'name' => 'retencion-factor_iva-' . $ivaCuenta->iva->porcentaje,
                    'id' => 'retencion-factor_iva-' . $ivaCuenta->iva->porcentaje,
//                    'readonly' => false,
                    'clientOptions' => [
                        'rightAlign' => true,
                        'alias' => 'decimal',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'autoGroup' => true
                    ],
                    'options' => [
                        'readonly' => false,
                        'class' => 'form-control',
                    ]
                ]) .
                Html::endTag('div') .
                Html::endTag('td');
        }
    }
    ksort($columnas);
    foreach ($columnas as $columna) {
        echo $columna;
    }
} catch (Exception $e) {
    throw $e;
}
