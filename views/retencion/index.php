<?php

use backend\modules\contabilidad\models\Retencion;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\searchRetencionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Retenciones de ' . ucfirst(Yii::$app->getRequest()->getQueryParam('operacion'));
$this->params['breadcrumbs'][] = $this->title;
$operacion = Yii::$app->request->getQueryParam('operacion');
?>
<div class="retencion-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-retencion-create') ?
            Html::a('Crear Nueva Retencion', ['create', 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?php try {

        $template = '';
        //    foreach(['view', 'update', 'delete', 'ver_detalles'] as $_v)
        foreach (['view', 'update', 'delete', 'bloquear', 'desbloquear'] as $_v)
            if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-retencion-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'class' => DataColumn::className(),
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 60px; text-align: right; padding:10px 7px 0px 0px;'],
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'fecha_emision',
                    'format' => ['date', 'php:d-m-Y'],
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'model' => $searchModel,
                        'attribute' => 'fecha_emision',
                        'language' => 'es',
                        'pickerButton' => false,
//                        'options' => [
//                            'style' => 'width:90px',
//                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'weekStart' => '0',
                        ],
                    ],
                    'contentOptions' => ['style' => 'width: 200px; text-align: center;'],
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'nro_retencion',
                    'contentOptions' => ['style' => 'width: 100px; text-align: center;'],
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'factura_id',
                    'format' => 'raw',
                    'value' => function ($model) use ($operacion) {
                        /** @var $model Retencion */
                        $factura_id = "factura_{$operacion}_id";
                        $factura = ($operacion == 'compra' ? $model->facturaCompra : $model->facturaVenta);
                        $nro_factura = ($operacion == 'compra') ? $factura->nro_factura : $factura->getNroFacturaCompleto();
                        $simbolo = $factura->moneda->simbolo;
                        $cotizacion = ($operacion == 'compra') ? $factura->cotizacion : $factura->valor_moneda;
                        $cotizacion = number_format($cotizacion, 2, ',', '.');
                        return Html::a($nro_factura, ["/contabilidad/$operacion/view", 'id' => $model->$factura_id], ['target' => '_blank', 'title' => "Factura {$factura->condicion}"]) . ($cotizacion != 0 ? " ({$simbolo}: {$cotizacion})" : '');
                    },
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => "Total",
                    'value' => function ($model) use ($operacion) {
                        /** @var Retencion $model */
                        $cotizacion = ($operacion == 'compra') ? $model->facturaCompra->cotizacion : $model->facturaVenta->valor_moneda;
                        $cotizacion = round($cotizacion, 2);
                        $cotizacion = ($cotizacion == 0 ? 1 : $cotizacion);
                        $total = number_format($model->total, 2, ',', '.');
                        $valorizado = number_format($model->total * $cotizacion, 2, ',', '.');
                        return $total . ($cotizacion != 0 ? " (Gs: {$valorizado})" : "");
                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 150px; text-align: right;'],
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => "Retención de RENTA",
                    'attribute' => 'base_renta_porc',
                    'value' => function ($model) use ($operacion) {
                        /** @var Retencion $model */
                        $cotizacion = ($operacion == 'compra') ? $model->facturaCompra->cotizacion : $model->facturaVenta->valor_moneda;
                        $cotizacion = round($cotizacion, 2);
                        $cotizacion = ($cotizacion == 0 ? 1 : $cotizacion);
                        $ret_renta = $model->base_renta_porc * $model->factor_renta_porc / 100;
                        $ret_renta_noval = number_format(round($ret_renta, 2), ($cotizacion == 1 ? 0 : 2), ',', '.');
                        $ret_renta_val = number_format(round($ret_renta * $cotizacion, 2), ($cotizacion == 1 ? 0 : 2), ',', '.');
                        return $ret_renta_noval . ($cotizacion != 1 && $ret_renta != 0 ? " (Gs: {$ret_renta_val})" : "");
                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 150px; text-align: right;'],
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => "Retención de IVA",
                    'attribute' => 'base_renta_porc',
                    'value' => function ($model) use ($operacion) {
                        /** @var Retencion $model */
                        $cotizacion = ($operacion == 'compra') ? $model->facturaCompra->cotizacion : $model->facturaVenta->valor_moneda;
                        $cotizacion = round($cotizacion, 2);
                        $total_ret_ivas = 0;
                        foreach ($model->detalles as $detalle) {
                            $total_ret_ivas += round($detalle->base * $detalle->factor / 100, 2);
                        }
                        $total_val = number_format(round($total_ret_ivas * $cotizacion, 2), ($cotizacion == 1 ? 0 : 2), ',', '.');
                        $total_noval = number_format(round($total_ret_ivas, 2), ($cotizacion == 1 ? 0 : 2), ',', '.');
                        return $total_noval . ($cotizacion != 1 && $total_ret_ivas != 0 ? " (Gs: {$total_val})" : "");
                    },
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 150px; text-align: right;'],
                ],
//                [
//                    'class' => DataColumn::className(),
//                    'label' => "Retención de IVA",
//                    'format' => 'raw',
//                    'value' => function ($model) {
//                        /** @var Retencion $model */
//                        $retencion_ivas = [];
//                        foreach ($model->detalles as $detalle) {
//                            $retencion_ivas[] = "
//                                <tr><td><strong>Base:</strong></td><td>".number_format($detalle->base, 2, ',', '.')."</td></tr>
//                                <tr><td><strong>% Ret:</strong></td><td>".number_format($detalle->factor, 2, ',', '.')."</td></tr>
//                                <tr><td><strong>IVA:</strong></td><td>".number_format($detalle->iva->porcentaje, 0, ',', '.')."%</td></tr>
//                            ";
//                        }
//                        $html = '
//                            <table class="table table-condensed">
//                                '.implode('', $retencion_ivas).'
//                            </table>
//                        ';
//                        return HtmlHelpers::trimmedHtml($html);
//                    },
//                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
//                ],
                [
                    'attribute' => 'asiento_id',
                    'label' => "Asentado?",
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var Retencion $retencion */
                        $retencion = $model;
                        $type = ($retencion->asiento_id != '') ? "success" : "warning";
                        $content = ($retencion->asiento_id != '') ? "Si" : "No";
                        $btnVer = ($retencion->asiento_id != '') ? Html::a("<span class='label label-info'>Ver</span>", ['/contabilidad/asiento/view/', 'id' => $retencion->asiento_id], ['target' => '_blank']) : null;
                        return "<label style='text-align:center;' class='label label-{$type}'>$content</label>&nbsp;&nbsp;$btnVer";
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'asiento_id',
                        'data' => ['si' => "SI", 'no' => "NO"],
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                            //'width' => '100px',
                        ],
                    ]),
                    'contentOptions' => ['style' => 'text-align:center;'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'update' => function ($url, $model, $index) {
                            return $model->bloqueado == 'no' ? Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>',
                                $url,
                                [
                                    'class' => 'btn_edit_retencion',
                                    'id' => $index,
                                    'title' => Yii::t('app', 'Editar'),
                                ]
                            ) : '';
                        },
                        'view' => function ($url, $model, $index) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-eye-open"></span>',
                                $url,
                                [
                                    'class' => 'btn_view_retencion',
                                    'id' => $index,
                                    'title' => Yii::t('app', 'Ver'),
                                ]
                            );
                        },
                        'delete' => function ($url, $model, $index) {
                            return $model->bloqueado == 'no' ? Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                $url,
                                [
                                    'class' => 'btn_edit_retencion',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => 'Está seguro de eliminar la retención ' . $model->nro_retencion . ' ?',
                                    ],
                                    'id' => $index,
                                    'title' => Yii::t('app', 'Borrar'),
                                ]
                            ) : '';
                        },
                        'bloquear' => function ($url, $model, $key) {
                            /** @var Retencion $model */
                            $url = ($model->bloqueado == 'no') ?
                                Html::a('<span>Bloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => Yii::t('app', 'Bloquear'),
                                        'data-confirm' => 'Está seguro que desea bloquear la retencion?'
                                    ]) : '';
                            return $url;
                        },
                        'desbloquear' => function ($url, $model, $key) {
                            /** @var Retencion $model */
                            $url = ($model->bloqueado == 'si') ?
                                Html::a('<span>Desbloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-success btn-xs',
                                        'title' => Yii::t('app', 'Desbloquear'),
                                        'data-confirm' => 'Está seguro que desea desbloquear la retencion?'
                                    ]) : '';
                            return $url;
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'update') {
                            // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                            $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
                            $url = Url::to(['retencion/update', 'id' => $model->id, 'operacion' => $operacion]);
                            return $url;
                        }
                        if ($action === 'view') {
                            // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                            $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
                            $url = Url::to(['retencion/view', 'id' => $model->id, 'operacion' => $operacion]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
                            $url = Url::to(['retencion/delete', 'id' => $model->id, 'operacion' => $operacion]);
                            return $url;
                        }
                        if ($action === 'bloquear') {
                            $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
                            $url = Url::to(['retencion/bloquear', 'id' => $model->id, 'operacion' => $operacion]);
                            return $url;
                        }
                        if ($action === 'desbloquear') {
                            $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
                            $url = Url::to(['retencion/desbloquear', 'id' => $model->id, 'operacion' => $operacion]);
                            return $url;
                        }
                        return '';
                    },
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw  $exception;
    } ?>
</div>
