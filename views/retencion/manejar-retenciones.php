<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 27/11/18
 * Time: 08:59 AM
 */

use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\RetencionDetalleBaseIvas;
use backend\modules\contabilidad\models\RetencionDetalleFactorIvas;

/* @var $model Retencion */
/* @var $bases RetencionDetalleBaseIvas[] */
/* @var $factors RetencionDetalleFactorIvas[] */
/* @var $factura_id string */
/* @var $retencion_id string */
?>

<div class="manejar-retenciones">

    <?= $this->render('_form', [
        'model' => $model,
        // Estos dos de abajo quizas no sea util, pero si util en update. quizas.
        'bases' => $bases,
        'factors' => $factors,
        'factura_id' => $factura_id,
        'retencion_id' => $retencion_id,
    ]) ?>

</div>
