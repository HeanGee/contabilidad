<?php

use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\Retencion;
use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * User: Hean Gky
 * Date: 14/07/2018
 * Time: 14:31
 */

/* @var $form yii\widgets\ActiveForm */
/* @var $model Retencion */

try {
    $ivaCuentas = IvaCuenta::find()->all();
    $columnas = [];
    /** @var IvaCuenta $ivaCuenta */
    foreach ($ivaCuentas as $ivaCuenta) {
        if ($ivaCuenta->iva->porcentaje != 0) {
            $columnas[$ivaCuenta->iva->porcentaje] = Html::tag('th', 'Impuesto IVA ' . $ivaCuenta->iva->porcentaje, ['class' => 'iva_' . $ivaCuenta->iva->porcentaje . ' iva-__x__', 'style' => "text-align: center;"]);

        }
    }
    ksort($columnas);
    foreach ($columnas as $columna) {
        echo $columna;
    }
} catch (Exception $e) {
    throw $e;
}
