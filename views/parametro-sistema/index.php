<?php

use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ParametroSistemaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $concepts array */

isset($concepts) || $concepts = ['todos' => "Todos"];

$this->title = 'Parametro Sistema';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="parametro-sistema-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-parametro-sistema-create') ?
            Html::a('Create Parametro Sistema', ['create'], ['class' => 'btn btn-success']) : null
        ?>
        <?= (Yii::$app->user->identity->getId() == 1) ?
            Html::a('Borrar Parámetro', 'javascript:void(0);', [
                'class' => 'delete-parametro btn btn-danger pull-right',
                'data' => [
                    'data-toggle' => 'modal',
                    'target' => '#modal',
                    'url' => Url::to(['delete-parametro']),
                    'title' => "Borrar parametro",
                ],
            ]) : null
        ?>
    </p>

    <?php try {
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-parametro-sistema-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'nombre',
                    'value' => 'nombre',
//                    'filter' => Select2::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'nombre',
//                        'data' => $concepts,
//                        'pluginOptions' => [
////                            'width' => '90px',
//                            'allowClear' => false,
//                        ],
//                        'initValueText' => 'todos',
//                    ]),
////                    'contentOptions' => ['style' => 'padding:8px 0px 0px 0px; text-align:center;']
                ],
                'valor',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (\Exception $exception) {
        echo $exception;
        FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    } ?>
</div>

<?php
ob_start(); // output buffer the javascript to register later ?>
<script>
    $(document).on('click', '.delete-parametro', function () {
        var boton = $(this);
        var title = boton.data('title');
        console.log('aqui');
        $.get(
            boton.data('url'),
            function (data) {
                var modal = $(boton.data('target'));
                console.log(modal, boton.data('url'));
                $('.modal-body', modal).html(data);
                modal.modal();
                if (title)
                    $('.modal-title', modal).html(title);
            }
        );
    });

    $("#modal").on('shown.bs.modal', function () {
        $(this).find('input').focus();
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
