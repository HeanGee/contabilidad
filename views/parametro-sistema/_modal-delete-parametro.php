<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 16/08/2018
 * Time: 11:05
 */

use backend\helpers\HtmlHelpers;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;

?>

<?php
ActiveFormDisableSubmitButtonsAsset::register($this);
$form_id = 'modal-delete-parm';
$form = ActiveForm::begin([
    'id' => 'modal-delete-parm',
    'options' => ['class' => 'disable-submit-button'],
]);

try {
    echo Form::widget([
        'formName' => 'kvform',
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'parametro' => [
                'type' => Form::INPUT_TEXT,
                'label' => "Parámetro",
                'options' => [
                    'placeholder' => "Máxima cantidad de letras: 255",
                    'maxlength' => 255,
                ],
            ],
        ],
    ]);
} catch (Exception $exception) {
    echo $exception->getMessage();
}
?>

    <br/>
    <div class="form-group">
        <?= HtmlHelpers::SubmitButton('Borrar', 'danger', '', ['confirm' => "Está seguro?"]) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php

$css = <<<CSS
.modal-header {
    background-color: #$ !important;
}
CSS;
$this->registerCss(str_replace('$', HtmlHelpers::DangerColorHex(), $css));

ob_start(); // output buffer the javascript to register later ?>
    <script>
        function clicked() {
            krajeeDialog.confirm("¿Está seguro de eliminar este parámetro?", function (result) {
                if (result) {
                    $('#submit-btn').click();
                } else {
                    $("#modal").modal("hide");
                    $("modal-body").html("");
                }
            });
        }
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>