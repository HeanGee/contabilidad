<?php

use common\helpers\PermisosHelpers;
use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ParametroSistema */

$this->title = "Parámetro <strong>$model->nombre</strong>";
$this->params['breadcrumbs'][] = ['label' => 'Parametro Sistema', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="parametro-sistema-view">

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-parametro-sistema-update') ?
            Html::a('Modicicar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null
        ?>
        <?= PermisosHelpers::getAcceso('contabilidad-parametro-sistema-delete') ?
            Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Está seguro de eliminar este elemento',
                    'method' => 'post',
                ],
            ]) : null
        ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => "Datos del <strong>$model->nombre</strong>",
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'id',
                'nombre',
                'valor',
            ],
        ]);
    } catch (\Exception $exception) {
        echo $exception;
        \common\helpers\FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    } ?>

</div>
