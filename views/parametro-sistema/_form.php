<?php

use backend\helpers\HtmlHelpers;
use common\helpers\FlashMessageHelpsers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ParametroSistema */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parametro-sistema-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php try {
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 2,
                    'attributes' => [
                        'nombre' => [
                            'type' => Form::INPUT_TEXT
                        ],
                        'valor' => [
                            'type' => Form::INPUT_TEXT
                        ],
                    ],
                ]
            ]
        ]);

        echo HtmlHelpers::SubmitButton();

    } catch (\Exception $exception) {
        echo $exception;
        FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
