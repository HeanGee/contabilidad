<?php

/* @var $this yii\web\View */
/* @var $model common\models\ParametroSistema */

$this->title = "Modificar parámetro <strong>$model->nombre</strong>";
$this->params['breadcrumbs'][] = ['label' => 'Parametro Sistema', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="parametro-sistema-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
