<?php

use backend\modules\contabilidad\models\Venta;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $model \backend\modules\contabilidad\controllers\ReporteVentaHechauka */

$this->title = 'Reporte de ventas para Hechauka';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="venta-index">

<?php $form = ActiveForm::begin([
    'id' => 'reporte-form'
]); ?>

    <div>
        <?php

        try {
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 6,
                'attributes' => [
                    'periodo' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => MaskedInput::className(),
                        'options' => [
                            'clientOptions' => ['alias' => 'mm/yyyy'],
                        ],
                        'label' => 'Periodo mes/año'
                    ],
                ]
            ]);
        } catch (Exception $e) {
            echo $e;
        } ?>
    </div>

    <div>
        <?php
        echo Html::submitButton('Generar reporte', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info']);
        ?>
    </div>

    <br/>
    <!--    <fieldset>-->
    <!--        <legend class="text-info">-->
    <!--            <small>Preview de Facturas</small>-->
    <!--        </legend>-->
        <?php
        \yii\widgets\Pjax::begin(['id' => 'grid-ventas']);
        $dataProvider = Yii::$app->session->get('dataProvider', null);
        if (isset($dataProvider)) {
            try {
                /*echo*/
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'hover' => true,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'label' => 'ID',
                            'headerOptions' => [
                                'width' => '5%'
                            ],
                            'value' => 'id',
                            'attribute' => 'id',
                        ],
                        [
                            'label' => 'Nro Comprobante',
                            'value' => function ($model) {
                                /** @var Venta $model */
                                return $model->getNroFacturaCompleto();
                            },
                            'attribute' => 'nroFacturaCompleto',
                        ],
                        [
                            'label' => 'Fecha Emisión',
                            'attribute' => 'fecha_emision',
                            'format' => ['date', 'php:d-m-Y'],
                        ],
                        [
                            'label' => 'Cliente',
                            'attribute' => 'nombre_entidad',
                            'value' => 'entidad.razon_social',
                        ],
                        [
                            'attribute' => 'total',
                            'value' => 'formattedTotal',
                            'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                        ],
                        [
                            'attribute' => 'saldo',
                            'value' => 'formattedSaldo',
                            'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                        ],
                        [
                            'label' => 'Tipos',
                            'attribute' => 'tipo_documento_id',
                            'value' => 'tipoDocumento.nombre',
                        ],
                        [
                            'label' => 'Timbrado',
                            'attribute' => 'timbrado_detalle_id',
                            'value' => 'timbradoDetalle.timbrado.nro_timbrado',
                        ],
                        [
                            'attribute' => 'moneda_id',
                            'value' => 'moneda.nombre',
                        ],
                        [
                            'attribute' => 'estado',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return '<label class="label label-' . (($model->estado == 'vigente') ? 'success">Vigente' : (($model->estado == 'faltante') ? 'warning">Faltante' : 'danger">Anulada')) . '</label>';
                            },
                            'contentOptions' => ['style' => 'text-align:center;'],
                        ],
                    ],
                ]);
            } catch (Exception $e) {
                echo $e;
            }
        }
        \yii\widgets\Pjax::end();
        ?>
    <!--    </fieldset>-->


<?php ActiveForm::end(); ?>

<?php
$urlLoadVentas = Json::htmlEncode(\Yii::t('app', Url::to(['load-ventas'])));
$JS_DOCUMENT_ON_READY = <<<JS
$(document).on('focusout', '#reporteventahechauka-periodo', function () {
    let periodo = $(this);
    $.ajax({
        url: $urlLoadVentas,
        type: 'get',
        data: {
            periodo: periodo.val()
        },
        success: function(data) {
            $.pjax.reload({container: "#grid-ventas", async: false});
        }
    });
});

$(document).ready(function () {  
    // Cada vez que haga submit, renderiza en nueva pestanha.
    document.getElementById('reporte-form').setAttribute("target", "_blank");
});
JS;

$JS_EVENTS = <<<JS
JS;

$scripts = [];
$scripts[] = $JS_DOCUMENT_ON_READY;
$scripts[] = $JS_EVENTS;
foreach ($scripts as $s) $this->registerJs($s);
?>