<?php

use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologicoEspecie */
/* @var $form ActiveForm */
?>

<div class="activo-biologico-especie-form">

    <?php $form = ActiveForm::begin(); ?>

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-index') ?
            Html::a('Ir a Especies', ['index'], ['class' => 'btn btn-info']) : null ?>
    </p>

    <?php try {
        echo Html::activeHiddenInput($model, "guardar_cerrar");
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => true,
                    'attributes' => [
                        'nombre' => [
                            'type' => Form::INPUT_TEXT,
                        ],
                        'descripcion' => [
                            'type' => Form::INPUT_TEXT,
                        ],
                    ],
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>

    <div class="form-group btn-toolbar">
        <?php
        if (PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-create') &&
            Yii::$app->controller->action->id == 'create' ||
            PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-update') &&
            Yii::$app->controller->action->id == 'update') {

            echo Html::submitButton('Guardar', ['class' => 'btn btn-success guardar pull-left']);
            if (Yii::$app->controller->action->id == 'create')
                echo Html::submitButton('Guardar y cerrar', ['class' => "btn btn-primary guardar-cerrar pull-left"]);
        }
        ?>
    </div>

    <?php

    $script = <<<JS
$(document).on('click', '.guardar-cerrar', function() {
    $('#activobiologicoespecie-guardar_cerrar').val('si').trigger('change');
});

$(document).on('click', '.guardar', function() {
    $('#activobiologicoespecie-guardar_cerrar').val('no').trigger('change');
});

$(document).on('focus', '.select2', function (e) {
  if (e.originalEvent) {
    $(this).siblings('select').select2('open');    
  } 
});
JS;
    $this->registerJs($script);

    ActiveForm::end(); ?>

</div>
