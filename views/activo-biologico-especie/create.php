<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologicoEspecie */

$this->title = "Registrar Nueva Especie";
$this->params['breadcrumbs'][] = ['label' => 'Especies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-biologico-especie-create">

    <?php try {
        echo $this->render('_form', [
            'model' => $model,
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>

</div>
