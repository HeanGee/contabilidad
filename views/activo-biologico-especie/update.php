<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologicoEspecie */

$this->title = 'Modificar Especie  ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Especies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="activo-biologico-especie-update">

    <?php try {
        echo $this->render('_form', [
            'model' => $model,
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>

</div>
