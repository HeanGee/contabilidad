<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ActivoBiologicoEspecieSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Especies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-biologico-especie-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    <div class="btn-toolbar">
        <?php $permisos = [
            'index-actbiol' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-index'),
            'index-clasifi' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-clasificacion-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-create'),
        ]; ?>
        <?= $permisos['create'] ? Html::a('Registrar Nueva Especie', ['create'], ['class' => 'btn btn-success']) : null ?>
        <?= $permisos['index-clasifi'] ? Html::a('Ir a Clasificaciones', ['activo-biologico-especie-clasificacion/index'], ['class' => 'btn btn-info pull-right']) : null ?>
        <?= $permisos['index-actbiol'] ? Html::a('Ir a A. Biologicos', ['activo-biologico/index'], ['class' => 'btn btn-info pull-right']) : null ?>
    </div>
    </p>

    <?php try {
        $template = "";

        foreach (['view', 'update', 'delete'] as $_v)
            if (\common\helpers\PermisosHelpers::getAcceso("activo-biologico-especie-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'nombre',
                'descripcion',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>
</div>
