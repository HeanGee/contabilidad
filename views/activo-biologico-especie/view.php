<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologicoEspecie */

$this->title = "Datos de Especie {$model->nombre}";
$this->params['breadcrumbs'][] = ['label' => 'Activo Biologico Especies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="activo-biologico-especie-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Especies', ['index'/*, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'id',
                'nombre',
                'descripcion',
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>

</div>
