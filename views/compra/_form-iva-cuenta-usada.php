<?php

use yii\helpers\Html;
use yii\helpers\Url;

$es_nota = !empty($es_nota);
?>

    <td style="width: 25%;">
        <?= $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->textInput([
            'class' => 'form-control plantilla',
        ])->label(false) ?>
    </td>

<?php
/** @var array $porcentajes_iva */
foreach ($porcentajes_iva as $ic) {
    echo '
        <td>
            ' . $form->field($ivaCtaUsada, "[{$key}]ivas[iva_" . $ic . "]")->textInput([
            "class" => "form-control agregarNumber",
            "clave" => "ivaMonto{$ic}",
            "disabled" => !$es_nota,
        ])->label(false)
        . '</td>
    ';
}

echo Html::tag('td', $form->field($ivaCtaUsada, "[{$key}]gnd_motivo")->textInput([
    "class" => "form-control",
    "readonly" => true,
    "disabled" => ($es_nota || true),
])->label(false), ['class' => "", 'style' => "text-align: left; "]);

if (!$es_nota) {
    ?>

    <td style="text-align: center;">
        <?= Html::button('<span class="fa fa-cubes" aria-hidden="true"></span>', [
            'class' => 'actfijo-manager btn btn-success modal-actfijo',
            'title' => 'Activo Fijo',
            'id' => "compraivacuentausada-{$key}-actfijo_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['activo-fijo/manejar-desde-factura-compra']),
            'data-pjax' => '0',
        ]) ?>
        <?= Html::button('<span class="fa fa-shield" aria-hidden="true"></span>', [
            'class' => 'poliza-manager btn btn-success modal-poliza',
            'title' => 'Póliza',
            'id' => "compraivacuentausada-{$key}-poliza_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['poliza/manejar-poliza-desde-compra']),
            'data-pjax' => '0',
        ]) ?>
        <?= Html::button('<span class="fa fa-dollar" aria-hidden="true"></span>', [
            'class' => 'prestamo-manager btn btn-success modal-cuota-prestamo',
            'title' => 'Préstamos',
            'id' => "compraivacuentausada-{$key}-cuota_prestamo_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['prestamo/manejar-cuota-prestamo-desde-compra']),
            'data-pjax' => '0',
        ]) ?>
        <?= Html::button('<span class="fa fa-dollar" aria-hidden="true"></span>', [
            'class' => 'prestamo-manager btn btn-success modal-prestamo',
            'title' => 'Préstamos',
            'id' => "compraivacuentausada-{$key}-prestamo_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['prestamo/manejar-prestamo-desde-compra']),
            'data-pjax' => '0',
        ]) ?>
        <?= Html::button('<span class="fa fa-paw" aria-hidden="true"></span>', [
            'class' => 'act-bio-compra-manager btn btn-success modal-act-bio-compra-manager',
            'title' => 'Compra de A. Biológicos',
            'id' => "compraivacuentausada-{$key}-actbio_compra_manager_btn",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['activo-biologico/comprar-act-bio']),
            'data-pjax' => '0',
        ]) ?>
        <?= Html::a('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', 'javascript:void(0);', [
            'class' => 'iva-cta-usada-remove-button btn btn-danger',
            'title' => 'Borrar esta fila'
        ]) ?>
    </td>


<?php
}
//Solo en caso de update se recargan todos los campos y recalcula
if (Yii::$app->controller->action->id == 'update' || $es_nota) {
    $string = <<<JS
    reloadAllNumberField();
JS;
    $this->registerJS($string);
}
?>