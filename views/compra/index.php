<?php

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\Venta;
use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\CompraSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facturas de Compra';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compra-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
		<?= PermisosHelpers::getAcceso('contabilidad-compra-create') ? Html::a('Registrar Factura de Compra', ['create'], ['class' => 'btn btn-success']) : '' ?>
		<?= (Yii::$app->user->identity->username == 'admin') ? Html::a('Localizar Facturas no balanceadas', ['find-unbalanced'], ['class' => 'btn btn-warning pull-right']) : '' ?>
    </p>

	<?php
	$template = '';
	foreach (['view', 'update', 'delete-with-obs', 'bloquear', 'desbloquear'] as $_v)
		if (PermisosHelpers::getAcceso("contabilidad-compra-{$_v}"))
			$template .= "&nbsp&nbsp&nbsp{{$_v}}";
	?>

	<?php
	try {
		echo GridView::widget([
            'panel' => ['type' => 'info', 'heading' => 'Detalles',],
            'toolbar' => [],
//            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_TOP,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],

			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'hover' => true,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				[
					'label' => 'ID',
					'headerOptions' => [
						'width' => '5%'
					],
					'value' => 'id',
					'attribute' => 'id',
				],
				[
					'format' => 'raw',
					'label' => 'Nro Comprobante',
					'value' => function ($model) {
						$text = $model->nro_factura;
						$url = Url::to(['update', 'id' => $model->id]);
						$options = [
							'target' => '_blank',
						];
						$link = HtmlHelpers::a($text, $url, $options);
						return $link;
					},
					'attribute' => 'nro_factura',
				],
                [
                    'attribute' => 'creado',
                    // format the value
                    'format' => ['date', 'php:d-m-Y H:m'],
                    'headerOptions' => ['class' => 'col-md-2'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'creado',
                        'language' => 'es',
                        'pjaxContainerId' => 'comprobante-list',
                        'pickerButton' => false,
                        'options' => [
                            'style' => 'width:90px',
                        ],
                        'pluginOptions' => [
                            'orientation' => 'bottom center',
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'weekStart' => 0,
                        ]
                    ])
                ],
				[ // the attribute
					'attribute' => 'fecha_emision',
					// format the value
					'format' => ['date', 'php:d-m-Y'],
					'headerOptions' => ['class' => 'col-md-2'],
					'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'fecha_emision_f',
						'language' => 'es',
						'pjaxContainerId' => 'comprobante-list',
						'pickerButton' => false,
						'options' => [
							'style' => 'width:90px',
						],
						'pluginOptions' => [
							'orientation' => 'bottom center',
							'autoclose' => true,
							'format' => 'dd-mm-yyyy',
							'todayHighlight' => true,
							'weekStart' => 0,
						]
					])
				],
				[ // the attribute
					'attribute' => 'fecha_emision',
					'label' => "Fecha de Emisión Hasta (Opcional)",
					// format the value
					'format' => ['date', 'php:d-m-Y'],
					'headerOptions' => ['class' => 'col-md-2'],
					'filter' => DatePicker::widget([
						'model' => $searchModel,
						'attribute' => 'fecha_emision_hasta',
						'language' => 'es',
						'pjaxContainerId' => 'comprobante-list',
						'pickerButton' => false,
						'options' => [
							'style' => 'width:90px',
						],
						'pluginOptions' => [
							'orientation' => 'bottom center',
							'autoclose' => true,
							'format' => 'dd-mm-yyyy',
							'todayHighlight' => true,
							'weekStart' => 0,
						],
					])
				],
				// [ // the attribute
				// 	'attribute' => 'fecha_vencimiento',
				// 	'label' => "Fecha de Vencimiento",
				// 	'format' => ['date', 'php:d-m-Y'],
				// 	'headerOptions' => ['class' => 'col-md-2'],
				// 	'filter' => DatePicker::widget([
				// 		'model' => $searchModel,
				// 		'attribute' => 'fecha_vencimiento',
				// 		'language' => 'es',
				// 		'pjaxContainerId' => 'comprobante-list',
				// 		'pickerButton' => false,
				// 		'options' => [
				// 			'style' => 'width:90px',
				// 		],
				// 		'pluginOptions' => [
				// 			'orientation' => 'bottom center',
				// 			'autoclose' => true,
				// 			'format' => 'dd-mm-yyyy',
				// 			'todayHighlight' => true,
				// 			'weekStart' => 0,
				// 		]
				// 	])
				// ],
				[
					'format' => 'raw',
					'label' => 'Proveedor',
					'attribute' => 'nombre_entidad',
					'value' => function ($model) {
						/** @var Venta $model */
						if (isset($model->entidad)) {
                            $text = "{$model->entidad->razon_social} <strong>(RUC: {$model->entidad->ruc})</strong>";
                            $url = Url::to(['/contabilidad/entidad/view', 'id' => $model->entidad_id]);
                            $options = ['target' => '_blank'];
                            $link = HtmlHelpers::a($text, $url, $options);
                            return $link;
						}
						return '';
					},
                    'pageSummary' => function ($summary, $data, $widget) {
                        return "Totales";
                    },
                    'contentOptions' => function ($model) {
//                        return ['style' => 'padding:8px 6px 0px 10px; text-align:left'];
                        return ['style' => 'padding:8px 6px 0px 6px; text-align:left; '];
                    },
				],
//				[
//					'value' => function ($model) {
//						/** @var Compra $model */
//						return $model->getTotal();
//					},
//					'attribute' => 'total',
//					'contentOptions' => ['style' => 'padding:8px 2px 0px 0px; text-align:right'],
//				],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Total',
                    'value' => function ($model) {
                        /** @var $model DetalleCompra */
                        return $model->total;
                    },
                    'attribute' => 'total',
                    'format' => 'integer',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'append' => '',
                        'prepend' => '',
                    ],
                    'contentOptions' => ['style' => 'width: 12%; text-align:right'],
                ],
				[
					'label' => 'Tipo',
					'attribute' => 'nombre_documento',
					'value' => 'tipoDocumento.nombre'
				],
				// [
				// 	'attribute' => 'autofactura',
				// 	'label' => "Autofactura?",
				// 	'format' => 'raw',
				// 	'value' => function ($model) {
				// 		/** @var Compra $model */
				// 		$type = ($model->cedula_autofactura != '') ? "success" : "warning";
				// 		$content = ($model->cedula_autofactura != '') ? "Si" : "No";
				// 		return "<label style='text-align:center;' class='label label-{$type}'>$content</label>";
				// 	},
				// 	'filterType' => Select2::class,
				// 	'filterWidgetOptions' => [
				// 		'model' => $searchModel,
				// 		'attribute' => 'autofactura',
				// 		'data' => ['si' => "SI", 'no' => "NO"],
				// 		'pluginOptions' => [
				// 			'placeholder' => "Todos",
				// 			'allowClear' => true,
				// 			'width' => '100px',
				// 		],
				// 	],
				// 	'contentOptions' => ['style' => 'text-align:center;'],
				// ],
				[
					'attribute' => 'timbrado',
					'value' => 'timbradoDetalle.timbrado.nro_timbrado'
				],
				// [
				// 	'attribute' => 'moneda_id',
				// 	'value' => 'moneda.nombre',
				// 	'filter' => Select2::widget([
				// 		'model' => $searchModel,
				// 		'attribute' => 'moneda_id',
				// 		'data' => Compra::getMonedas(true),
				// 		'pluginOptions' => [
				// 			'width' => '120px'
				// 		],
				// 		'initValueText' => 'todos',
				// 	])
				// ],
				[
					'attribute' => 'condicion',
					'value' => function ($model) {
						/** @var Compra $model */
						return ucfirst($model->condicion);
					},
					'label' => 'Condición',
					'filter' => Select2::widget([
						'model' => $searchModel,
						'attribute' => 'condicion',
						'data' => Compra::getCondiciones(),
						'pluginOptions' => [
							'width' => '120px'
						],
						'initValueText' => 'todos',
					])
				],
				[
					'attribute' => 'estado',
					'format' => 'raw',
					'value' => function ($model) {
						return '<label class="label label-' . (($model->estado == 'vigente') ? 'success">Vigente' : (($model->estado == 'faltante') ? 'warning">Faltante' : 'danger">Anulada')) . '</label>';
					},
					'filter' => Select2::widget([
						'model' => $searchModel,
						'attribute' => 'estado',
						'data' => Compra::getEstados(true),
//                        'hideSearch' => true,
						'pluginOptions' => [
							'width' => '120px',
						]
					]),
					'contentOptions' => ['style' => 'text-align:center;']
				],
				[
					'attribute' => 'para_iva',
					'format' => 'raw',
					'value' => function ($model) {
						return '<label class="label label-' . (($model->para_iva == 'si') ? 'success">SI' : 'warning">NO') . '</label>';
					},
					'filter' => Select2::widget([
						'model' => $searchModel,
						'attribute' => 'para_iva',
						'data' => [null => 'Todos', 'si' => "SI", 'no' => "NO"],
						'pluginOptions' => [
							'width' => '120px',
						]
					]),
					'contentOptions' => ['style' => 'text-align:center;']
				],
				// [
				// 	'attribute' => 'partida_correcto',
				// 	'format' => 'raw',
				// 	'value' => function ($model) {
				// 		/** @var Compra $model */
				// 		$compra = $model;
				// 		return '<label class="label label-' . ($compra->isPartidaCorrect() ? 'success">Si' : 'warning">No') . '</label>';
				// 	},
				// 	'contentOptions' => ['style' => 'text-align:center;'],
				// ],
				// [
				// 	'attribute' => 'asiento_id',
				// 	'label' => "Asentado?",
				// 	'format' => 'raw',
				// 	'value' => function ($model) {
				// 		/** @var Compra $compra */
				// 		$compra = $model;
				// 		$type = ($compra->asiento_id != '') ? "success" : "warning";
				// 		$content = ($compra->asiento_id != '') ? "Si" : "No";
				// 		$btnVer = ($compra->asiento_id != '') ? Html::a("<span class='label label-info'>Ver</span>", ['/contabilidad/asiento/view/', 'id' => $compra->asiento_id], ['target' => '_blank']) : null;
				// 		return "<label style='text-align:center;' class='label label-{$type}'>$content</label>&nbsp;&nbsp;$btnVer";
				// 	},
				// 	'filter' => Select2::widget([
				// 		'model' => $searchModel,
				// 		'attribute' => 'asiento_id',
				// 		'data' => ['si' => "SI", 'no' => "NO"],
				// 		'pluginOptions' => [
				// 			'placeholder' => "Todos",
				// 			'allowClear' => true,
				// 			'width' => '100px',
				// 		],
				// 	]),
				// 	'contentOptions' => ['style' => 'text-align:center;'],
				// ],
				[
					'class' => 'yii\grid\ActionColumn',
					'buttons' => [
//                            'delete' => function ($url, $model) {
//                                /** @var Compra $model */
//                                return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
//                                    'class' => '',
//                                    'title' => Yii::t('yii', 'Borrar'),
//                                    'data' => [
//                                        'confirm' => '¿Está seguro de eliminar este elemento?',
//                                        'method' => 'post',
//                                    ],
//                                ]) : '';
//                            },

						'delete-with-obs' => function ($url, $model) {
							/** @var Compra $model */
							return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-trash"></span>', 'javascript:void(0);', [
								'class' => 'borrar-factura-compra',
								'data' => [
//                                        'confirm' => '¿Está seguro de eliminar este elemento?',
									'data-toggle' => 'modal',
									'target' => '#modal',
									'url' => Url::to(['delete-with-obs', 'id' => $model->id]),
									'title' => Yii::t('yii', 'Escriba el motivo del borrado'),
								],
							]) : '';
						},

//                            'delete-with-obs' => function ($url, $model) {
//                                return $model->bloqueado == 'no' ?
//                                    Html::button("<span class=\"glyphicon glyphicon-trash\"></span>", [
//                                        'type' => 'button',
//                                        'title' => Yii::t('app', 'Borrar'),
//                                        'class' => 'borrar-factura-compra',
//                                        'data-toggle' => 'modal',
//                                        'data-target' => '#modal',
//                                        'data-url' => Url::to(['delete-with-obs', 'id' => $model->id]),
//                                        'data-pjax' => '0',
//                                        'data-title' => 'Indicar el motivo del borrado',
//                                    ]) : '';
//                            },
						'update' => function ($url, $model) {
							return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
								'title' => Yii::t('yii', 'Editar'),
							]) : '';
						},
						'view' => function ($url, $model) {
							return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
								'title' => Yii::t('yii', 'Ver'),
							]);
						},
						'bloquear' => function ($url, $model, $key) {
							/** @var Compra $model */
							$url = ($model->bloqueado == 'no') ?
								Html::a('<span>Bloquear</span>', $url,
									[
										'class' => 'btn btn-danger btn-xs',
										'title' => Yii::t('app', 'Bloquear'),
										'data-confirm' => 'Está seguro que desea bloquear la factura?'
									]) : '';
							return $url;
						},
						'desbloquear' => function ($url, $model, $key) {
							/** @var Compra $model */
							$url = ($model->bloqueado == 'si') ?
								Html::a('<span>Desbloquear</span>', $url,
									[
										'class' => 'btn btn-success btn-xs',
										'title' => Yii::t('app', 'Desbloquear'),
										'data-confirm' => 'Está seguro que desea desbloquear la factura?'
									]) : '';
							return $url;
						},
					],
					'template' => $template,
					'urlCreator' => function ($action, $model, $key, $index) {
						return Url::to([$action, 'id' => $model->id]);
					},
				],
			],
		]);
	} catch (Exception $e) {
		throw $e;
	} ?>
</div>

<?php
ob_start(); // output buffer the javascript to register later ?>
<script>
    $(document).on('click', '.borrar-factura-compra', function () {
        var boton = $(this);
        var title = boton.data('title');
        console.log('aqui');
        $.get(
            boton.data('url'),
            function (data) {
                var modal = $(boton.data('target'));
                console.log(modal, boton.data('url'));
                $('.modal-body', modal).html(data);
                modal.modal();
                if (title)
                    $('.modal-title', modal).html(title);
            }
        );
    });

    $("#modal").on('shown.bs.modal', function () {
        $(this).find('textarea').focus();
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
