<?php

use backend\modules\contabilidad\models\Compra;
use yii\web\View;

/* @var $this View */
/* @var $model Compra */

setlocale(LC_TIME, 'es_ES');
?>
<style type="text/css">
    .badge {
        padding: 1px 9px 2px;
        font-size: 12.025px;
        font-weight: bold;
        white-space: nowrap;
        color: #ffffff;
        background-color: #999999;
        -webkit-border-radius: 9px;
        -moz-border-radius: 9px;
        border-radius: 9px;
    }

    .badge:hover {
        color: #ffffff;
        text-decoration: none;
        cursor: pointer;
    }

    .badge-error {
        background-color: #b94a48;
    }

    .badge-error:hover {
        background-color: #953b39;
    }

    .badge-warning {
        background-color: #f89406;
    }

    .badge-warning:hover {
        background-color: #c67605;
    }

    .badge-success {
        background-color: #468847;
    }

    .badge-success:hover {
        background-color: #356635;
    }

    .badge-info {
        background-color: #3a87ad;
    }

    .badge-info:hover {
        background-color: #2d6987;
    }

    .badge-inverse {
        background-color: #333333;
    }

    .badge-inverse:hover {
        background-color: #1a1a1a;
    }
</style>
<?php if (Yii::$app->controller->action->id == 'update') : ?>
    <div class="pull-left" style="font-weight: bold; font-style: normal">
        <!--    <span class="badge badge-success">-->
        <span class="badge badge-success">Creado</span><?= strftime(" %e-%m-%Y, a las %H:%M:%S", strtotime($model->creado)) ?>
        <br/>
        <!--    </span>-->
        <!--    <span class="badge badge-warning">-->
        <span class="badge badge-warning">Modificado</span><?= strftime(" %e-%m-%Y, a las %H:%M:%S", strtotime($model->modificado)) ?>
        <br/>
        <span class="badge badge-info">Responsable</span><?= ' ' . $model->creado_por ?>
        <!--    </span>-->
    </div>
<?php endif; ?>
