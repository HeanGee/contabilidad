<?php

use common\helpers\ValorHelpers;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
?>
<?php Pjax::begin(['id' => 'asiento-simulator-cuota-prestamo']);
$sesion = Yii::$app->session;
$detalles = \Yii::$app->session->has('cont_detallecompra_sim-provider')
&& \Yii::$app->session->get('cont_detallecompra_sim-provider')->allModels !== '' ?
    \Yii::$app->session->get('cont_detallecompra_sim-provider')->allModels : [];
$hayDetalles = empty($detalles) ? "no" : "si";

?>

<?php
$cuotas = [];
foreach ($detalles as $detalle) {
    if (!in_array($detalle->prestamo_cuota_id, $cuotas) && $detalle->prestamo_cuota_id != '')
        $cuotas[] = $detalle->prestamo_cuota_id;
}
Yii::warning("cantidad de cuotas " . sizeof($cuotas));
foreach ($cuotas as $cuota) { ?>
    <div class="panel panel-primary" style="border-color: #9933CC !important;" id="asientoSimulator">
        <div class="panel-heading"
             style="background-color: #aa66cc !important; border-color: #aa66cc !important; color: white;"><h3
                    class="panel-title">Simulación de asiento de cuota de préstamo</h3></div>
        <div class="panel-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Código cuenta</th>
                    <th>Cuenta</th>
                    <th></th>
                    <th style="text-align: right;">Debe</th>
                    <th style="text-align: right;">Haber</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $total_debe = 0.0;
                $total_haber = 0.0;
                $cuentasDebe = [];
                $cuentasHaber = [];
                $filas = [];
                $array = [];
                /** @var \backend\modules\contabilidad\models\DetalleCompra $detalle */
                foreach ($detalles as $detalle) {
                    if ($detalle->prestamo_cuota_id == '' || $detalle->prestamo_cuota_id != $cuota) continue;

                    if ($detalle->cta_contable == 'debe') {
                        $index = searchForId($detalle->planCuenta->cod_completo, $filas, 'debe');
                        if ($index > -1 && $detalle->agrupar == 'si') {
                            $filas[$index]['suma'] += (float)$detalle->subtotal;
                        } else {
                            $filas[] = ['codigo' => $detalle->planCuenta->cod_completo, 'nombre' => $detalle->planCuenta->nombre, 'suma' => $detalle->subtotal, 'saldo' => 'debe'];
                        }

                        $total_debe += (float)$detalle->subtotal;
                    } else {
                        $index = searchForId($detalle->planCuenta->cod_completo, $filas, 'haber');

                        if ($index > -1 && $detalle->agrupar == 'si') {
                            $filas[$index]['suma'] += (float)$detalle->subtotal;
                        } else {
                            $filas[] = ['codigo' => $detalle->planCuenta->cod_completo, 'nombre' => $detalle->planCuenta->nombre, 'suma' => $detalle->subtotal, 'saldo' => 'haber'];
                        }

                        $total_haber += (float)$detalle->subtotal;
                    }
                }
                $enDebe = 0;
                $numberCellHtmlOptions = ["style" => "text-align: right;"];
                foreach ($filas as $fila) {
                    if ($fila['saldo'] === 'debe') {
                        echo Html::beginTag('tr');
                        if ($enDebe % 2 === 0) {
                            $enDebe++;
//                            echo '<tr style="border-top:2pt solid black">';
                            echo '<tr style="border-top:1pt solid black">';
                        } else {
                            echo Html::beginTag('tr');
                        }
                        echo Html::beginTag('td') . $fila['codigo'] . Html::endTag('td');
                        echo Html::beginTag('td') . $fila['nombre'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
//                        echo Html::beginTag('td') . (nf($fila['suma'])) . Html::endTag('td');
                        echo Html::beginTag('td', $numberCellHtmlOptions) . ValorHelpers::numberFormatMonedaSensitive($fila['suma'], 0) . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::endTag('tr');
                    } else {
                        if ($enDebe % 2 !== 0) $enDebe++;
                        echo Html::beginTag('tr');
                        echo Html::beginTag('tr');
                        echo Html::beginTag('td') . $fila['codigo'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td') . "a " . $fila['nombre'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td', $numberCellHtmlOptions) . ValorHelpers::numberFormatMonedaSensitive($fila['suma'], 0) . Html::endTag('td');
                        echo Html::endTag('tr');
                    }
                }

                echo '<tr style="border-top:2pt solid black">';
                echo Html::beginTag('td') . Html::endTag('td');
                echo Html::beginTag('td') . Html::endTag('td');
                echo Html::beginTag('td') . '<strong><b><i><font size="5">TOTAL:</font></i></b></strong>' . Html::endTag('td');
                echo Html::beginTag('td', $numberCellHtmlOptions) . '<strong><b><i><font size="5">' . ValorHelpers::numberFormatMonedaSensitive($total_debe, 0) . '</font></i></b></strong>' . Html::endTag('td');
                echo Html::beginTag('td', $numberCellHtmlOptions) . '<strong><b><i><font size="5">' . ValorHelpers::numberFormatMonedaSensitive($total_haber, 0) . '</font></i></b></strong>' . Html::endTag('td');
                echo Html::endTag('tr');
                ?>
                </tbody>
            </table>
        </div>
    </div>
<?php }
Pjax::end()
?>