<?php

use common\helpers\ValorHelpers;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
?>
<?php Pjax::begin(['id' => 'asiento-simulator-div']);
$sesion = Yii::$app->session;
$detalles = \Yii::$app->session->has('cont_detallecompra_sim-provider')
&& \Yii::$app->session->get('cont_detallecompra_sim-provider')->allModels !== '' ?
    \Yii::$app->session->get('cont_detallecompra_sim-provider')->allModels : [];
$hayDetalles = empty($detalles) ? "no" : "si";
$total_debe = 0.0;
$total_haber = 0.0;
function searchForId($id, $array, $saldo)
{
    foreach ($array as $key => $val) {
        if (array_key_exists('codigo', $val) && array_key_exists('saldo', $val) && $val['codigo'] === $id && $val['saldo'] === $saldo) {
            return $key;
        }
    }
    return -1;
}

?>
    <div class="panel panel-primary" id="asientoSimulator">
        <div class="panel-heading"><h3 class="panel-title">Simulación de asientos</h3></div>
        <div class="panel-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Código cuenta</th>
                    <th>Cuenta</th>
                    <th></th>
                    <th style="text-align: right;">Debe</th>
                    <th style="text-align: right;">Haber</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $cuentasDebe = [];
                $cuentasHaber = [];
                $filas = [];
                $array = [];
                /** @var \backend\modules\contabilidad\models\DetalleCompra $detalle */
                foreach ($detalles as $detalle) {
                    if ($detalle->prestamo_cuota_id != '') continue;

                    if ($detalle->cta_contable == 'debe') {
                        $index = searchForId($detalle->planCuenta->cod_completo, $filas, 'debe');
                        if ($index > -1) {
                            $filas[$index]['suma'] += (float)$detalle->subtotal;
                        } else {
                            $filas[] = ['codigo' => $detalle->planCuenta->cod_completo, 'nombre' => $detalle->planCuenta->nombre, 'suma' => $detalle->subtotal, 'saldo' => 'debe'];
                        }

                        $total_debe += (float)$detalle->subtotal;
                    } else {
                        $index = searchForId($detalle->planCuenta->cod_completo, $filas, 'haber');

                        if ($index > -1) {
                            $filas[$index]['suma'] += (float)$detalle->subtotal;
                        } else {
                            $filas[] = ['codigo' => $detalle->planCuenta->cod_completo, 'nombre' => $detalle->planCuenta->nombre, 'suma' => $detalle->subtotal, 'saldo' => 'haber'];
                        }

                        $total_haber += (float)$detalle->subtotal;
                    }
                }
                $enDebe = 0;
                $numberCellHtmlOptions = ["style" => "text-align: right;"];
                foreach ($filas as $fila) {
                    if ($fila['saldo'] === 'debe') {
                        echo Html::beginTag('tr');
                        if ($enDebe % 2 === 0) {
                            $enDebe++;
//                            echo '<tr style="border-top:2pt solid black">';
                            echo '<tr style="border-top:1pt solid black">';
                        } else {
                            echo Html::beginTag('tr');
                        }
                        echo Html::beginTag('td') . $fila['codigo'] . Html::endTag('td');
                        echo Html::beginTag('td') . $fila['nombre'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
//                        echo Html::beginTag('td') . (nf($fila['suma'])) . Html::endTag('td');
                        echo Html::beginTag('td', $numberCellHtmlOptions) . ValorHelpers::numberFormatMonedaSensitive($fila['suma'], 0) . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::endTag('tr');
                    } else {
                        if ($enDebe % 2 !== 0) $enDebe++;
                        echo Html::beginTag('tr');
                        echo Html::beginTag('tr');
                        echo Html::beginTag('td') . $fila['codigo'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td') . "a " . $fila['nombre'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td', $numberCellHtmlOptions) . ValorHelpers::numberFormatMonedaSensitive($fila['suma'], 0) . Html::endTag('td');
                        echo Html::endTag('tr');
                    }
                }

                echo '<tr style="border-top:2pt solid black">';
                echo Html::beginTag('td') . Html::endTag('td');
                echo Html::beginTag('td') . Html::endTag('td');
                echo Html::beginTag('td') . '<strong><b><i><font size="5">TOTAL:</font></i></b></strong>' . Html::endTag('td');
                echo Html::beginTag('td', $numberCellHtmlOptions) . '<strong><b><i><font size="5">' . ValorHelpers::numberFormatMonedaSensitive($total_debe, 0) . '</font></i></b></strong>' . Html::endTag('td');
                echo Html::beginTag('td', $numberCellHtmlOptions) . '<strong><b><i><font size="5">' . ValorHelpers::numberFormatMonedaSensitive($total_haber, 0) . '</font></i></b></strong>' . Html::endTag('td');
                echo Html::endTag('tr');
                ?>
                </tbody>
            </table>
        </div>
    </div>
<?php
$actionId = Yii::$app->controller->action->id;
$script = <<<JS
// Mostrar/ocultar dependiendo de si hay detalles con cuentas
$(document).ready(function () {
    var hayDetalles = "$hayDetalles";
    if(hayDetalles === "no")
        $('#asientoSimulator').hide();
    else
        $('#asientoSimulator').show();
    
    let submit_cerrar_button = $('#btn_submit_factura_compra_guardar_cerrar');
    let submit_button = $('#btn_submit_factura_compra');
    if (submit_button.length === 0)
        submit_button = $('#btn_submit_nota');
    
    // animacion de carga
    var _loader = $('#content-loading-widget');
    if (_loader.length) {
        _loader.fadeOut('fast', function() {
            $('#factura_form').css('opacity', 'initial').find('> fieldset').prop('disabled', false);
        });
    }
     // re-habilitar boton submit
    submit_button.prop('disabled', false);
    submit_cerrar_button.prop('disabled', false);
    
    let boton_text = '', actionId = "$actionId";
    if (actionId === 'create') boton_text = 'Guardar y siguiente';
    else boton_text = 'Guardar';
    
    let boton_cerrar_text = '';
    if (actionId === 'create') boton_cerrar_text = 'Guardar y Cerrar';
    else boton_cerrar_text = 'Guardar';
        
    submit_button.html(boton_text);
    submit_cerrar_button.html(boton_cerrar_text);
});
JS;
$this->registerJs($script);
Pjax::end()
?>