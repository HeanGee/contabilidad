<?php

use backend\modules\contabilidad\models\IvaCuenta;
use common\helpers\ParametroSistemaHelpers;
use kartik\dialog\Dialog;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * @var array $totales ;
 * @var \backend\modules\contabilidad\models\Compra $model
 */

$boton = Html::label('', 'boton_add_timbrado', ['class' => 'control-label']) . '<br/>'
    . Html::button('<i class="glyphicon glyphicon-plus"></i>', [
        'id' => 'boton_add_timbrado',
        'disabled' => true,
        'style' => "display: yes;",
        'type' => 'button',
        'title' => 'Agregar Timbrado.',
        'class' => 'btn btn-success add_timbrado',
        'data-toggle' => 'modal',
        'data-target' => '#modal',
        'data-url' => Url::to(['timbrado/create-new-timbrado-compra']),
        'data-pjax' => '0',
    ]) . '<br/><br/>';

$boton_update = Html::label('', 'boton_update_timbrado', ['class' => 'control-label']) . '<br/>'
    . Html::button('<i class="glyphicon glyphicon-pencil"></i>', [
        'id' => 'boton_update_timbrado',
        'disabled' => true,
        'style' => "display: yes;",
        'type' => 'button',
        'title' => 'Editar Timbrado.',
        'data-toggle' => 'modal',
        'data-target' => '#modal',
        'class' => 'btn btn-primary update_timbrado',
        'data-url' => Url::to(['compra/update-timbrado-compra']),
        'data-pjax' => '0',
    ]) . '<br/><br/>';
$urlPlantilla = Json::htmlEncode(\Yii::t('app', Url::to(['compra/cargar-plantilla'])));
$urlCalcDetalle = Json::htmlEncode(\Yii::t('app', Url::to(['compra/calc-detalle'])));
$urlUpdtDetalle = Json::htmlEncode(\Yii::t('app', Url::to(['compra/update-detalle'])));
$url_cotizacion = Json::htmlEncode(\Yii::t('app', Url::to(['detalle-compra/get-cotizacion'])));
$url_timbrado_load = Json::htmlEncode(\Yii::t('app', Url::to(['compra/cargar-timbrado'])));
$urlCargarDetalleHaber = Json::htmlEncode(\Yii::t('app', Url::to(['compra/cargar-detalle-haber'])));
$url_enableTotalField = Json::htmlEncode(\Yii::t('app', Url::to(['compra/enable-campos-totales'])));
$url_getPrestamosLibres = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/get-prestamos-libres-para-compra-ajax'])));
$url_manejarDesdeCompra = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/manejar-cuota-prestamo-desde-compra'])));
$url_getRazonSocialByRuc = Json::htmlEncode(\Yii::t('app', Url::to(['compra/get-razon-social-by-ruc'])));
$url_datosFacturaForNota = Json::htmlEncode(\Yii::t('app', Url::to(['compra-nota/get-datos-facturas-by-id'])));
$url_getCondicionFromDocType = Json::htmlEncode(\Yii::t('app', Url::to(['compra/condicion-from-tipo-doc'])));
$url_selectPrestamoParaFacturar = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/select-prestamo-para-facturar'])));
$url_getPlantillaSeguroADevengarId = Json::htmlEncode(\Yii::t('app', Url::to(['plantilla-compraventa/get-plantilla-seguro-a-devengar-id'])));
$url_getClienteAutofacturaByCedula = Json::htmlEncode(\Yii::t('app', Url::to(['get-cliente-autofactura-by-cedula'])));
$url_checkIfExistPrestamoSinFactura = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/prestamo-sin-fact-exists'])));
$iva_cuentas = IvaCuenta::find()->all();
$monto_totales = Yii::$app->getSession()->get('cont_monto_totales_compra');
$monto_totales = Json::encode($monto_totales);
$isNewRecord = $model->isNewRecord;
$es_nota = $model->tipo != 'factura';
$moneda_base_id = ParametroSistemaHelpers::getValorByNombre('moneda_base') == null ? 1 : ParametroSistemaHelpers::getValorByNombre('moneda_base');
$porcentajes_iva = [];
/** @var IvaCuenta $iva_cuenta */
foreach ($iva_cuentas as $iva_cuenta)
    $porcentajes_iva[] = $iva_cuenta->iva->porcentaje;
asort($porcentajes_iva);
$porcentajes_iva = array_values($porcentajes_iva);
$porcentajes_iva = Json::encode($porcentajes_iva);

//ID de la factura se guarda en variable
$actionId = Yii::$app->controller->action->id;
$facturaCompraId = null;
if (key_exists('id', Yii::$app->controller->actionParams))
    $facturaCompraId = Yii::$app->controller->actionParams['id'];

//Se carga modal
try {
    echo Dialog::widget();
} catch (Exception $e) {
}
$infoColor = \backend\helpers\HtmlHelpers::InfoColorHex(true);
$warnColor = \backend\helpers\HtmlHelpers::WarningColorHex(true);
$infoSpan = \backend\helpers\HtmlHelpers::InfoHelpIcon('tipo_documento');
$script = <<<JS
var id_to_focus = ""; //Variable para volver y hacer focus a input de donde proviene un evento

// Deshabilitar campos totales al inicio.
let campos = getTotalFieldNames();
for (i = 0; i < campos.length; i++) {
    campos[i] = '#' + campos[i] + '-disp';
}

function getTipoDocumentoId() {
    return esNota ? $('#compra-tipo').select2('data')[0]['tipo_doc_set_id'] : $('#compra-tipo_documento_id').val();
}

// Si es update, cargar los campos totales
const isNewRecord = "$isNewRecord".length;

var actionid = "$actionId";
if (actionid === 'update') {
    // Si es update, cargar los campos ivas
    var monto_totales = $monto_totales;
    if (monto_totales !== null) {
        monto_totales = Object.values(monto_totales);
        var i;
        for (i = 0; i < monto_totales.length; i++) {
            $('#' + Object.keys(monto_totales[i])).val(Object.values(monto_totales[i])).trigger('change');
        }
    }
}

// Metodo prototipo personal
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
};

function eliminarPlantillas() {
    let plantillas = $('.iva-cta-usada-remove-button');

    $.each(plantillas, function (index, value) {
        if ($(this).parent().parent()[0].id !== 'iva-cta-usada-new-block') {
            $(this).click();
        }
    });

    completarTotales();
    console.log('desde metodo eliminarPlantillas()');
    setTotalFactura();
}

function disableCotizacion() {
    var condicion = $('#compra-condicion').val();
    // Bloquear campo de cotización dependiendo de la moneda
    var cotizacion_field = $('#compra-cotizacion-disp');
    let cotiprop_field = $('#compra-cotizacion_propia');

    if (condicion === 'credito') {
        // cotizacion_field.prop("readonly", true);
        cotiprop_field.val('no').trigger('change');
        cotiprop_field.prop('disabled', true);
    } else {
        // cotizacion_field.prop("readonly", false);
        // cotiprop_field.val('si').trigger('change');
        cotiprop_field.val('no').prop('disabled', false);
    }

    if (parseInt($('#compra-moneda_id').val()) === $moneda_base_id) {
        // cotizacion_field.prop("readonly", true);
        cotizacion_field.val('');
        cotiprop_field.val('no').trigger('change');
        cotiprop_field.prop('disabled', true);
    }


}

//Retorna array con datos de la tabla de plantillas - ivas
function getDataTable() {
    var ivas = getIvas();
    var data_table = [];
    $('input.ivaMonto' + ivas[0]).each(
        function () {
            let id_prefijo = $(this).prop('id').replace(new RegExp('ivas-iva_0-disp', 'g'), '');
            var fila = {};
            for (let j = 0; j < ivas.length; j++) {
                fila['iva_' + ivas[j]] = $('#' + id_prefijo + 'ivas-iva_' + ivas[j] + '-disp').val();
            }
            fila["plantilla_id"] = $('#' + id_prefijo + 'plantilla_id').val();
            data_table.push(fila);
        }
    );

    return data_table;
}

$(document).on('focus', "input[type='text'], select", function(e) { loading = false; });  // los input de tipos select están cubiertos por spans por lo que el que gana foco es uno de los spans.

$('select[id^="compra"]').on('select2:opening', function(e) { loading = false; });

$('#boton_add_detalle').on('select2:opening', function(e) { loading = false; });

$('a.tiene_modal,a.ajaxDelete').on('click', function(e){ loading = false; });

$('input[id$="plantilla_id"]').on('select2:open', function(e){ loading = false; });

function readyToCalcDetalle() {
    if (['anulada', 'faltante'].includes($("#compra-estado").val())) return false;
    if ($('#compra-tipo_documento_id').val() === '') return false;
    let monedaField = $('#compra-moneda_id'); 
    if (monedaField.val() === '') return false;
    else if (monedaField.val() !== '1') {
        if($('#compra-cotizacion').val() === '') return false;
    }
    
    //recorre plantillas y verifica si hay uno seleccionado y con monto distinto de cero
    let plantillaValueEmpty = true,
        plantillas = $('input[id$="plantilla_id"]:not([id*="compraivacuentausada-__id__"])'),
        cant_plantillas = plantillas.length,
        cant_plantillas_vacias = 0;
    
    plantillas.each(function(i, plantilla) {
        if ($(plantilla).val() === '') {
            plantillaValueEmpty = true;
            return false;
        } else {
            let emptyValue = true;
            $(plantilla).parent().parent().parent().find('input:not([id$="plantilla_id"]):not([id$="gnd_motivo"]):not([id*="compraivacuentausada-__id__-ivas-iva"])').each(function(j, element) {
                if (element.value !== '' && element.value > 0) {
                    emptyValue = false;
                    return false;
                }
            });
            if (!emptyValue) {
                plantillaValueEmpty = false;
                return false;
            }
        }
    });
    return !plantillaValueEmpty;
}

function setTotalFactura(callReloadDetalle = true) {
    if (loading === true) {
        return;
    }
    
    if (!readyToCalcDetalle()) {
        console.log('no esta listo para settotalfactura');
        return;
    }
    
    var total_nombres = getTotalFieldNames();
    var i, factura_total = 0.0, vacio = 0;
    for (i = 0; i < total_nombres.length; i++) {
        var valor = $('#' + total_nombres[i] + '-disp').val();
        if (valor !== '') {
            factura_total += parseFloat(valor);
        } else vacio++;
    }
    var total_disp_field = $('#compra-total-disp');

    var totales = getTotalFieldValues();

    function checkCero(number) {
        return number === 0;
    }

    total_disp_field.val(factura_total);
    $('#compra-total').val(factura_total);
    if (true /*(vacio !== total_nombres.length || parseInt(total_disp_field.val()) > 0) && !totales.every(checkCero)*/) {

        if (callReloadDetalle) {
            // Adaptar la url dependiendo de si es nueva factura o update factura.
            var isNewRecords = "$isNewRecord".length;
            var url = $urlCalcDetalle;

            var moneda_id_field = $('#compra-moneda_id');
            var cotizacion_disp_field = $('#compra-cotizacion-disp');
            var tipo_doc_id = !esNota ? $('#compra-tipo_documento_id').val() : tipoDocumentoParaNota;
            if (tipo_doc_id !== '') {
                // animacion de carga
                var _loader = $('#content-loading-widget');
                if (_loader.length) {
                    _loader.fadeIn('fast');
                    $('#factura_form').css('opacity', 0.2).find('> fieldset').prop('disabled', true);
                }
                // inhabilitar temporalmente boton submit
                let submit_button = $('#btn_submit_factura_compra');
                let submit_cerrar_button = $('#btn_submit_factura_compra_guardar_cerrar');

                submit_button.prop('disabled', true);
                submit_button.html('Generando detalles y simulacion ... ');

                submit_cerrar_button.prop('disabled', true);
                submit_cerrar_button.html('Generando detalles y simulacion ... ');

                // calcular ivas y mostrar en los campos disableds de ivas
                calcularIvasYRellenar();
                $.ajax({
                    url: url,
                    type: 'post',
                    data: {
                        moneda_id: moneda_id_field.val(),
                        cotizacion: cotizacion_disp_field.val(),
                        cont_factura_total: total_disp_field.val(),
                        plantillas: getDataTable(),
                        tipo_docu_id: tipo_doc_id,
                        ivas: getIvas(),
                        totales_valor: getTotalFieldValues(),
                        estado_comprobante: $('#compra-estado').val(),
                        es_nota: esNota
                    },
                    success: function (data) {
                        if (data !== '') { // si retorna falso, aquí se vé como ''. si retorna true, aquí se vé como 1.
                            $.pjax.reload({container: "#detalles_grid", async: false});
                        } else {
                            $.pjax.reload({container: "#flash_message_id", async: false});
                        }
                        $.pjax.reload({container: "#flash_message_id", async: false});
                    }
                });
            }
        }
    }
}

function cargarTimbrado() {
    // No cargar timbrado si es sin timbrado
    let tipodocset = $('#compra-tipo_documento_id').select2('data')[0]['tipodocsetnombre'];
    if (tipodocset === 'NINGUNO') return false;

    var tipo_doc_id = getTipoDocumentoId();
    var nro_factura = $('#compra-nro_factura').val();
    var entidad_ruc = $('#compra-ruc').val();
    var fecha_emision = $('#compra-fecha_emision').val();
    if (nro_factura.split("_").join("").length === 15 && entidad_ruc !== "" && tipo_doc_id !== "") {
        $.ajax({
            url: $url_timbrado_load,
            type: 'post',
            data: {
                entidad_ruc: entidad_ruc,
                nro_factura: nro_factura,
                fecha_emision: fecha_emision,
                timbrado_vencido: $('#compra-timbrado_vencido').val(),
                tipodocumento_id: $('#compra-tipo_documento_id').val()
            },
            success: function (data) {
                if (data[0] === 'mostrar_boton') {
                    if (data[1].length > 0)
                        $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": data[1][0]['id'], "text": data[1][0]['text'], "vencido": data[1][0]['vencido']}});
                    else
                        $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": '', "text": ''}});
                    
                    //if (id_to_focus !== "")
                    //    $("#" + id_to_focus).focus();
                    
                    $('#boton_add_timbrado').prop('disabled', false);
                    $('#boton_update_timbrado').prop('disabled', true);
                } else {
                    try {
                        let newTim = localStorage.getItem('recentlyAddedTim');
                        if (newTim !== null) {
                            let slices = newTim.split(',');
                            $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": slices[0], "text": slices[1]}});
                            localStorage.removeItem('recentlyAddedTim');
                        }
                    } catch (err) {
                        console.log(err);
                    }
                    $('#boton_add_timbrado').prop('disabled', false);
                    $('#boton_update_timbrado').prop('disabled', false);
                }
            }
        });
        $('form[id^="factura_form"]').yiiActiveForm('validateAttribute', 'compra-nro_factura');
    } else {
        // $('#compra-button-add_id')[0].style.display = 'none';
        // $('#compra-timbrado').val('');
    }
};

function showHideCuotasField(condicion, evento = false) {
    if (condicion === 'contado' || condicion === '') {
        $('.field-compra-cant_cuotas')[0].style.display = "none";
        $('.field-compra-fecha_vencimiento')[0].style.display = "none";
        $('#compra-cant_cuotas, #compra-cant_cuotas-disp').val('');
        $('#compra-fecha_vencimiento, #compra-fecha_vencimiento-disp').val('');
    } else {
        $('.field-compra-cant_cuotas')[0].style.display = "";
        $('.field-compra-fecha_vencimiento')[0].style.display = "";

        // cliente quiere que precarge datos cuando condicion es cuota o credito.        
        // por defecto, cantidad cuota == 1
        $("#compra-cant_cuotas-disp").val('1').trigger('change');

        // por defecto, fecha vence en 30 dias posteriores a fecha emision.
        let monthNames = [
            "01", "02", "03",
            "04", "05", "06", "07",
            "08", "09", "10",
            "11", "12"
        ];
        let fecha_emision_string = $('#compra-fecha_emision').val().replace(new RegExp('-', 'g'), '/');
        fecha_emision_string = ((fecha_emision_string.split('/')).reverse()).join('/');
        let fecha_vencimiento = new Date(fecha_emision_string);
        fecha_vencimiento = new Date(new Date(fecha_vencimiento).setMonth(fecha_vencimiento.getMonth() + 1));

        // $.fn.kvDatepicker.defaults.format = 'dd-mm-yyyy';
        //
        // $("#compra-fecha_vencimiento-disp").kvDatepicker("update", fecha_vencimiento.getUTCDate() + '-' +
        //     monthNames[fecha_vencimiento.getMonth()]+'-'+ // getMonth() retorna index de un array de 12 elementos.
        //     fecha_vencimiento.getFullYear());

        let dia = fecha_vencimiento.getUTCDate();
        if (dia < 10) dia = '0' + dia;
        $('#compra-fecha_vencimiento').val(
            dia + '-' +
            monthNames[fecha_vencimiento.getMonth()] + '-' + // getMonth() retorna index de un array de 12 elementos.
            fecha_vencimiento.getFullYear()
        ).trigger('change');
    }
}

function getIvas() {
    return Object.values(JSON.parse("$porcentajes_iva"))
}

function getTotalFieldNames() {
    const prefijo = "totales_iva-iva-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}

function getImpuestosFieldNames() {
    const prefijo = "discriminados-impuesto-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}

function getGravadasFieldNames() {
    const prefijo = "discriminados-gravada-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}

function getTotalFieldValues() {
    var total_nombres = getTotalFieldNames();
    var i, total_valores = [];
    for (i = 0; i < total_nombres.length; i++) {
        total_valores.push($('#' + total_nombres[i] + '-disp').val());
    }
    return total_valores;
}

function loadCotizacionMoneda() {
    let moneda_field = $('#compra-moneda_id');
    $.ajax({
        url: $url_cotizacion,
        type: 'post',
        data: {
            fecha_emision: $('#compra-fecha_emision').val(),
            moneda_id: moneda_field.val()
        },
        success: function (data) {
            $.pjax.reload({container: "#flash_message_id", async: false});
            $('#compra-cotizacion-disp').val(data);
            $('#compra-cotizacion').val(data);

            disableCotizacion();
            console.log('desde function loadCotizacionMoneda');
            setTotalFactura();
        }
    });

    var campo_monedavalor_id = '#compra-cotizacion-disp';
    if (moneda_field.select2('data')[0]['tiene_decimales'] === 'si') {
        $(campo_monedavalor_id).inputmask({
            radixPoint: ",",
            prefix: "",
            digits: 2,
            autoGroup: true,
            groupSeparator: ".",
            rightAlign: true,
            autoUnmask: true,
            allowMinus: false,
            integerDigits: 12,
            digitsOptional: false,
            placeholder: "0"
        });
    } else {
        $(campo_monedavalor_id).inputmask({
            radixPoint: "",
            prefix: "",
            digits: 0,
            autoGroup: true,
            groupSeparator: ".",
            rightAlign: true,
            autoUnmask: true,
            allowMinus: false,
            digitsOptional: false,
            placeholder: "0"
        });
    }

    var condicion = $('#compra-condicion').val();
    showHideCuotasField(condicion, 'change');
    reloadAllNumberField(true);
    completarTotales();
}

function calcularIvasYRellenar() {
    let ctotales = getTotalFieldNames(), cimpuestos = getImpuestosFieldNames(), ivas = getIvas();
    let totales_iva = 0.0, i;
    for (i = 0; i < ctotales.length; i++) {
        let iva = ivas[i];
        if (iva !== '0') {
            let ctotal = document.getElementById(ctotales[i] + '-disp');
            let cgrav = document.getElementById(cimpuestos[i] + '-disp');
            if (ctotal !== null && cgrav !== null) {
                cgrav.value = ctotal.value - (ctotal.value / (1.0 + iva / 100.0));
                if ($('#compra-moneda_id').val() === '1') {
                    cgrav.value = Math.round(cgrav.value);
                }
                totales_iva += cgrav.value;
            }
        }
    }

    if ($('#compra-moneda_id').val() === '1') {
        totales_iva = Math.round(totales_iva);
    }
    $('#compra-_total_ivas-disp').val(totales_iva);
}

if (!esNota) {
    $('#compra-ruc').on('change', function () {
        var url_getRazonSocialByRuc = $url_getRazonSocialByRuc;
        $.ajax({
            url: url_getRazonSocialByRuc,
            type: 'post',
            data: {
                ruc: $(this).val(),
            },
            success: function (data) {
                $('#compra-nombre_entidad').val(data).trigger('change');
                $("#compra-timbrado_id").data({data: {"id": '', "text": ''}});
                $("#compra-timbrado_id").val('').trigger('change');
            }
        });
    });

    $('#compra-cotizacion_propia').change(function () {
        $('#compra-cotizacion-disp').prop('readonly', $(this).val() === 'no');

        let moneda_field = $('#compra-moneda_id');
        if ($('#compra-fecha_emision').val() !== '' && moneda_field.val() !== '')
            $.ajax({
                url: $url_cotizacion,
                type: 'post',
                data: {
                    fecha_emision: $('#compra-fecha_emision').val(),
                    moneda_id: moneda_field.val()
                },
                success: function (data) {
                    $('#compra-cotizacion-disp').val(data);
                    $('#compra-cotizacion').val(data);
                }
            });
    });

    $('#compra-timbrado_vencido').change(function () {
        //Prevenir que el usuario se olvide de elegir el timbrado apropiado.
        $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": '', "text": ''}});
        cargarTimbrado();
        if ($('#compra-timbrado_vencido').val() === "1") {
            eliminarPlantillas();
        }
    });
}

$('#compra-nombre_entidad').on('change', function () {
    var nro_factura_el = $('#compra-nro_factura');
    if ($('#compra-nombre_entidad').val() != '') {
        cargarTimbrado();
        nro_factura_el.prop('readonly', false);
    } else {
        nro_factura_el.val('');
        nro_factura_el.prop('readonly', true);
        if (esNota) {
            var fac_compra_el = $('#compra-factura_compra_id');
            fac_compra_el.data({data: {"id": '', "text": ''}});
            fac_compra_el.val('').trigger('change');
        }
    }
});

$('#compra-cotizacion-disp').on('change', function () {
    console.log('desde cotizacion on change');
    setTotalFactura();
});

// Código estático recurrente para abrir modales.
$(document).on('click', '.tiene_modal', (function () {
    var boton = $(this);
    var title = boton.data('title');
    $.get(
        boton.data('url'),
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

// Código estático recurrente para abrir modales.
$(document).on('click', '.add_timbrado', (function () {
    var boton = $(this);
    var title = boton.data('title');
    var tipo_documento_id = getTipoDocumentoId();
    var extra_url = '&entidadRuc=' + $('#compra-ruc').val() + '&prefijo=' + $('#compra-nro_factura').val().substring(0, 7) + '&nro=' + $('#compra-nro_factura').val().substring(8, 15) + '&tipoDocumentoId=' + tipo_documento_id + '&timbrado_id=' + $('#compra-timbrado_id').val();
    if (esNota)
        extra_url += '&submit=0&esNota=1';
    $.get(
        boton.data('url') + extra_url,
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

// Operaciones al borrar un detalle compra.
$(document).on('click', '.ajaxDelete', function (e) {
    e.preventDefault();
    var deleteUrl = $(this).attr('delete-url');
    krajeeDialog.confirm('Está seguro?',
        function (result) {
            if (result) {
                $.ajax({
                    url: deleteUrl,
                    type: 'post',
                    error: function (xhr, status, error) {
                        alert('There was an error with your request.' + xhr.responseText);
                    }
                }).done(function (data) {
                    $.pjax.reload({container: "#detalles_grid", async: false});
                });
            }
        }
    );
});

$(document).on('click', '.update_timbrado', (function (evt) {
    var boton = $(this);
    var title = boton.data('title');
    var tipo_documento_id = $('#compra-tipo_documento_id').val();
    var prefijo = $('#compra-nro_factura').val().substring(0, 7);
    var nro = $('#compra-nro_factura').val().substring(8, 15);
    var timbrado_id = $('#compra-timbrado_id').val();
    var modal = $(boton.data('target'));
    
    if (timbrado_id === '') {
        $("#modal").modal("hide");
        $("modal-body").html("");
        krajeeDialog.alert('No se ha seleccionado ningún timbrado.');
        return;
    }
    
    $.get(
        boton.data('url') + '&entidadRuc=' + $('#compra-ruc').val() + '&prefijo=' + prefijo + '&nro=' + nro + '&tipoDocumentoId=' + tipo_documento_id + '&timbradoId=' + timbrado_id + '&facturaCompraId=' + "$facturaCompraId",
        function (data) {
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

function manejarSeleccionFactura() {
    let main_data = $('#compra-factura_compra_id').select2('data')[0];
    $('#compra-emision_factura').val(main_data['fecha_emision']);
    $('#compra-moneda_nombre').val(main_data['moneda_nombre']);
    $('#compra-moneda_id').val(main_data['moneda_id']);
    $('#compra-cotizacion-disp').val(main_data['cotizacion']).trigger('change');
    $('#compra-nota_remision').val(main_data['nota_remision']).trigger('change');
    tipoDocumentoParaNota = main_data['tipo_documento_id'];
    digitosMonedaParaNota = main_data['tiene_decimales'] === 'si' ? 2 : 0;
    if (main_data['fecha_emision_factura_original'] != undefined) {
        fechaEmisionFacturaOriginal = main_data['fecha_emision_factura_original'];
    }
    $.ajax({
        url: $url_datosFacturaForNota,
        type: 'post',
        data: {
            factura_id: main_data['id'],
        },
        success: function (data) {
            $('#iva-cta-usadas').find('tbody tr:visible').remove();
            completarTotales();
            $('#compra-total-disp').val('0').trigger('change'); // completarTotales() no me limpia este campo
            $.each(data, function (i, e) {
                let index = eventForClick();
                $('#compraivacuentausada-new' + index + '-plantilla_id').val(i).trigger('change').prop('disabled', true);
                $.each(e, function (key, value) {
                    let _temp = $('#compraivacuentausada-new' + index + '-ivas-iva_' + key + '-disp');
                    _temp.val(value);
                    _temp.trigger('change');
                    _temp.inputmask("option", {max: main_data['tiene_decimales'] === 'si' ? parseFloat(value) : parseInt(value)});
                });
            });
        }
    });

}

// Poner el valor de la moneda según cotización, en el campo valor_moneda.
$('#compra-moneda_id').on('change', function () {
    loadCotizacionMoneda();
});

$('#compra-fecha_emision').on('change', function () {
    //Prevenir que el usuario se olvide de elegir el timbrado apropiado.
    $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": '', "text": ''}});
    id_to_focus = "";
    if (!esNota)
        loadCotizacionMoneda();
    cargarTimbrado();
});

// Desplegar select2 automáticamente al obtener focus.
$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        let selector = $(this).siblings('select');
        if (selector.attr('disabled') === false)
            selector.select2('open');
    }
});

// Mostrar/ocultar campos adicionales al cambiar condicion de factura
$('#compra-condicion').on('change', function () {
    var condicion = $('#compra-condicion').val();
    showHideCuotasField(condicion, 'change');
    $('#compra-moneda_id').trigger('change');
});

// Format nro factura
$("#compra-nro_factura").keydown(function (event) {
    // No cargar timbrado si es sin timbrado
    let tipodocset = $('#compra-tipo_documento_id').select2('data')[0]['tipodocsetnombre'];

    if (tipodocset !== 'NINGUNO') {
        var text = $(this).val().replace(/_/g, "").replace(/-/g, ''); // borrar los underscores a la derecha
        var key = event.which;
        var esTeclaBorrar = key === 8;
        if (key > 57) key -= 96; else key -= 48; // convertir a un nro
        var lastgroupstart = 6; // index donde comienza el ultimo grupo de nro
        var lastgroup = text.substring(lastgroupstart, text.length); // el ultimo grupo de nro

        if (text.length > 5 && !esTeclaBorrar && (key > -1 && key < 10)) {
            if (lastgroup.length === 0) { // si es el 1er nro tecleado
                text = text.replaceAt(lastgroupstart, Array(7).join("0") + key);
            } else {
                var i, p = -1;
                // localizar donde comienza un nro <> 0
                for (i = 0; i < lastgroup.length; i++) {
                    if (lastgroup[i] !== '0') {
                        p = i;
                        break;
                    }
                }
                // si previamente no hubo ningún número, hacer un espacio forzadamente.
                if (lastgroup === '0000000') lastgroup = '000000';
                // extraer solamente los nros y concatenar el nro tecleado
                lastgroup = lastgroup.substring(p, lastgroup.length) + key; // si p > lastgroup.length, significa que no hubo ningún nro antes de keydown y no extraerá nada el substring.
                // agregar el ultimo nro tecleado, rellenando debidamente con ceros a la izquierda.
                text = text.replaceAt(lastgroupstart, Array((7 - lastgroup.length) + 1).join("0") + lastgroup)
            }
        }
        //if (parseInt(text.substring(lastgroupstart, text.length)) === 0){
        //   text = text.replaceAt(lastgroupstart, Array(7).join("0")+1);
        // }
        $(this).val(text);
    } else {
        keepOnlyNumbers($(this));
        return true;
    }
});

//Para prevenir que el usuario se olvide de elegir el timbrado apropiado, se vacia el timbrado seleccionado.
var nf = '';
$("#compra-fecha_emision").focusout(function () {
    // $('#compra-nro_factura').change/.on('change') o $(doc).on('change', '#compra-nro_factura') no funcionaron extranhamente.
    if ($("#compra-nro_factura").val() !== nf)
        $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": '', "text": ''}});
    nf = '';
    cargarTimbrado();
    // id_to_focus = "compra-fecha_emision";
    // $("#"+id_to_focus).focus();
}).focusin(function () {
    nf = $(this).val();
});

// Abrir modal para agregar detalles al presionar tecla +
$(document).keypress(function (e) {
    if (document.activeElement.tagName !== "INPUT" &&
        (!$("#modal").hasClass('in') && (e.keycode === 43 || e.which === 43))) {
        $('#boton_add_detalle').click();
    } else if (document.activeElement.id.includes('compraivacuentausada') && (!$("#modal").hasClass('in') && (e.keycode === 43 || e.which === 43))) {
        eventForClick();
    }
});

$.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};

document.getElementById("compra-fecha_emision").onfocus = function() {
    $('#compra-fecha_emision').setCursorPosition(0);
};

// //Evitar que haga submit con ENTER
// $(document).on("keypress", 'form', function (e) {
//     var code = e.keyCode || e.which;
//    
//     //Si es botón para agregar timbrado, se habilita
//     if (code === 13 && document.getElementById("boton_add_timbrado") !== document.activeElement && $("#modal").hasClass('in')) {
//         e.preventDefault();
//         return false;
//     }
// });

function keepOnlyNumbers(selector) {
    let value = $(selector).val();
    value = value.replace(/[a-zA-Z\-\_]*/g, '');
    $(selector).val(value);
}

function formatNroFactura(selector, sinTimbrado = false) {
    if ($(selector).length) {
        // Destriur primero la mascara inicial
        $(selector).inputmask('remove');

        // Aplicar nueva mascara segun sea sin o con timbrado.
        if (!sinTimbrado) {
            // console.log('el normal');
            $(selector).inputmask({
                'mask': "999-999-9999999",
                'rightAlign': false
            });  //static mask
        } else {
            // Cambiar a una mascara de solo numeros
            //console.log('el numerico');
            keepOnlyNumbers(selector);
        }
    }
}

function changeMaskNroFactura() {
    let tipodocset = $('#compra-tipo_documento_id').select2('data')[0]['tipodocsetnombre'];
    console.log(tipodocset);
    if (tipodocset === 'NINGUNO') {
        formatNroFactura($('#compra-nro_factura'), true);
    } else {
        formatNroFactura($('#compra-nro_factura'), false);
    }
}

$('#compra-tipo_documento_id').on('change', function () {
    id_to_focus = "compra-ruc";
    // cambiar la mascara de nro de factura
    changeMaskNroFactura();
    
    // Mostrar campos para autofactura
    showHideFieldsAutofactura();
    
    // Des-seleccionar timbrado
    if ($(this).select2('data')[0]['tipodocsetnombre'] === 'NINGUNO')
        $('#compra-timbrado_id').val('').trigger('change');

    // establecer condicion de factura
    if ($(this).val() !== '') {
        $.ajax({
            url: $url_getCondicionFromDocType,
            type: 'post',
            data: {
                tipo_doc_id: $(this).val()
            },
            success: function (data) {
                $('#compra-condicion').val(data).trigger('change');
            }
        });
        console.log('desde tipo documento on change');
        setTotalFactura();
        cargarTimbrado();

        //Si es tipo documento para importación, se elimina las plantillas seleccionadas y solo se traen para importación
        if ($(this).select2('data')[0]['para_importacion'] === 'si') {
            eliminarPlantillas();
        }
    }
    
    // Bloquear campos de iva dependiendo del solo_exenta del tipo de documento
    $('input[id$="plantilla_id"]').trigger('change');

    //if ($(this).val() !== '' && $('#compra-nro_factura').val()!=''){
    //   $('#boton_add_timbrado').prop('disabled', false);
    //}
}).on('select2:close', function() {
    // para que habilite los campos del iva, si el codigo del _fieldset no logra adjuntar el evento tempranamente.
    if (actionid === 'create')
        $($('input[id$="plantilla_id"]:not([id*="compraivacuentausada-__id__-plantilla_id"])')[0]).trigger('change');
});

//$("#compra-nombre_entidad").on("focus", function () {
//    $('#compra-nro_factura').focus();
//});



$("#compra-cotizacion-disp").on("focus", function () {
    if ($(this).prop('readonly')) {
        if (document.getElementById("compra-cant_cuotas-disp").style.display !== "none") {
            $("#compra-cant_cuotas-disp").focus();
        } else {
            $('#iva-cta-usada-new-button').focus();
        }
    }
});

$(document).on('click', '.modal-poliza', (function () {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    $.get(
        boton.data('url') + '&compra_id=' + "$model->id" + "&formodal=" + true,
        function (data) {
            if (data !== "") {
                let modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                // if (title)
                $('.modal-title', modal).html(title);
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
                $("#modal").modal("hide");
                $("modal-body").html("");
            }
        }
    );
}));

$(document).on('click', '.modal-cuota-prestamo', (function () {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    let ruc = $('#compra-ruc').val();

    $.ajax({
        url: $url_getPrestamosLibres +"&compra_id={$model->id}&ruc=" + ruc + "&json_format=" + true,
        type: 'get',
        data: {},
        success: function (data) {
            if (data['results'].length > 0) {
                $.get(
                    $url_manejarDesdeCompra +'&compra_id=' + "$model->id" + "&ruc=" + ruc + "&fila_plantilla=" + boton_id.replace('cuota_prestamo_manager_button', ''),
                    {primera_vez: true},
                    function (data) {
                        if (data !== "") {
                            let modal = $(boton.data('target'));
                            $('.modal-body', modal).html(data);
                            $('.modal-header', modal).css('background', '#3c8dbc');
                            $('.modal-title', modal).html(title);
                            modal.modal();
                        } else {
                            $.pjax.reload({container: "#flash_message_id", async: false});
                            $("#modal").modal("hide");
                            $("modal-body").html("");
                        }
                    }
                );
            } else {
                $("#modal").modal("hide");
                $("modal-body").html("");
                krajeeDialog.alert('No se pudo encontrar ningún préstamo. Verifique si ha especificado el R.U.C. en el formulario de compras, que haya registrado previamente un prestamo y que éste tenga generado las cuotas.');
            }
        }
    });
}));

// TODO minx
$(document).on('click', '.modal-prestamo', (function () {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    let ruc = $('#compra-ruc').val();

    if (ruc === "") {
        $("#modal").modal("hide");
        $("modal-body").html("");
        krajeeDialog.alert('Debe especificar el R.U.C. primero. ');
        return;
    }

    let deducible = $('#' + boton_id.replace('prestamo_manager_button', 'plantilla_id')).select2('data')[0]['deducible'];

    $.ajax({
        url: $url_checkIfExistPrestamoSinFactura +"&compra_id={$model->id}&ruc=" + ruc,// + '&deducible=' + deducible,
        type: 'get',
        data: {},
        success: function (data) {
            if (data === true) {
                $.get(
                    $url_selectPrestamoParaFacturar +"&compra_id={$model->id}&ruc=" + ruc/* + '&deducible=' + deducible*/ + "&fila_plantilla=" + boton_id.replace('prestamo_manager_button', ''),
                    {},
                    function (data) {
                        if (data !== "") {
                            let modal = $(boton.data('target'));
                            $('.modal-body', modal).html(data);
                            $('.modal-header', modal).css('background', '#3c8dbc');
                            $('.modal-title', modal).html(title);
                            modal.modal();
                        } else {
                            $.pjax.reload({container: "#flash_message_id", async: false});
                            $("#modal").modal("hide");
                            $("modal-body").html("");
                        }
                    }
                );
            } else {
                $("#modal").modal("hide");
                $("modal-body").html("");
                krajeeDialog.alert('No se pudo encontrar ningún préstamo sin factura asociado a la entidad del R.U.C. indicado.');
            }
        }
    });
}));

// Código estático recurrente para abrir modales.
$(document).on('click', '.modal-act-bio-compra-manager', (function () {
    var boton = $(this);
    var title = boton.attr('title');
    let fila_id = boton.attr('id').replace('actbio_compra_manager_btn', '');
    let deducible = $('#' + fila_id + 'plantilla_id').select2('data')[0]['deducible'];
    let campo_iva_activo = "";
    $(':input[id^="' + fila_id + 'ivas-iva"]').each(function () {
        if ($(this).attr('disabled') !== 'disabled') {
            campo_iva_activo = $(this).attr('id');
            return false;
        }
    });
    let url = boton.data('url') + "&compra_id={$model->id}&fila_id=" + fila_id + '&deducible=' + deducible + '&campo_iva_activo=' + campo_iva_activo;

    $.get(
        url,
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            $('.modal-header', modal).css('background', '#3c8dbc');
            $('.modal-title', modal).html(title);
            modal.modal();
        }
    );
}));

$('#btn-hidden-simulator').on('click', function () {
    console.log('por btn-hidden-simulator');
    setTotalFactura();
});

$(document).on('click', '.modal-actfijo', (function (evt) {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    let id_field_plantilla = '#' + boton_id.replace(/actfijo_manager_button/g, 'plantilla_id');
    let plantilla_id = $(id_field_plantilla).val();

    let fecha_factura = $('#compra-fecha_emision').val();
    fecha_factura = ((fecha_factura.split('-')).reverse()).join('-');

    const regex = /compraivacuentausada-new([0-9]+)/gm;
    let m = regex.exec(boton.attr('id'));
    let fila_id = "";
    if (m !== null)
        fila_id = m[1];

    /** -------------- VERIFICAR QUE TENGA MONTO -------------- */
    let row = boton.parent().parent();
    let row_total = 0.0;
    let row_inputs = row[0].getElementsByClassName('agregarNumber');

    $(row_inputs).each(function (i, e) {
        if (e.value !== "") {
            const regex = /ivas-iva_([0-9]+)/gm;
            let m = regex.exec(e.id);
            if (m !== null) {
                let iva_porcentaje = parseFloat(m[1]);
                if (iva_porcentaje === 0)
                    row_total += parseFloat(e.value);
                else
                // Sin redondear sale 99.999999.... para e.value = 110.
                    row_total += Math.round(parseFloat(e.value) / ((iva_porcentaje + 100.0) / 100.0));
            }
        }
    });
    if (row_total === 0) {
        $("#modal").modal("hide");
        $("modal-body").html("");
        krajeeDialog.alert('Debe especificar un monto para esta fila.');
        evt.preventDefault();
        return false;
    }
    /** -------------------------------------------------------- */

    /** --------------- VERIFICAR QUE TENGA FECHA --------------- */
    if ($('#compra-fecha_emision').val() === "") {
        $("#modal").modal("hide");
        $("modal-body").html("");
        krajeeDialog.alert('Debe especificar Fecha de emisión de la Factura.');
        evt.preventDefault();
        return false;
    }
    /** -------------------------------------------------------- */

    $.get(
        boton.data('url') + '&goto=primera_vez' + '&plantilla_id=' + plantilla_id + '&fecha_factura=' + fecha_factura + "&formodal=" + true + '&costo_adquisicion=' + row_total + '&moneda_id=' + $('#compra-moneda_id').val(),
        function (data) {
            if (data !== "") {
                let modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                // if (title)
                $('.modal-title', modal).html(title);
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
                $("#modal").modal("hide");
                $("modal-body").html("");
            }
        }
    );
}));

function showHideFieldsAutofactura() {
    let tipoDocField = $('#compra-tipo_documento_id');
    if (tipoDocField.val() !== '' && tipoDocField.select2('data')[0]['autofactura'] === 'si') {
        let campos = $("#compra-nombre_completo_autofactura, #compra-cedula_autofactura");
        campos.parent().parent().removeClass('hidden');
        $(campos[0]).val(cedula_autofactura).trigger('change');
        $(campos[1]).val(nombre_autofactura).trigger('change');
    } else {
        let campos = $("#compra-nombre_completo_autofactura, #compra-cedula_autofactura");
        campos.parent().parent().addClass('hidden');
        campos.val('').trigger('change');
    }
}

$(document).on('change', '#compra-nombre_completo_autofactura', function() {
    if ($(this).val() !== '')
        nombre_autofactura = $(this).val();
});

$(document).on('change', '#compra-cedula_autofactura', function() {
    if ($(this).val() !== '')
        cedula_autofactura = $(this).val();
});

//Lunes 10 de junio, 2019: Jose dice que Magali ahora quiere que al cambiar obligacion, elimine todas las plantillas agregadas.
$(document).on('change', '#compra-obligacion_id', function(e) {
    $('table#iva-cta-usadas').find('a.iva-cta-usada-remove-button').each(function(i, e) {
        if ($(e).parent().find('button#compraivacuentausada-__id__-actbio_compra_manager_btn').length === 0)
            $(e).click();
    });
    $('#iva-cta-usada-new-button').click();
});

$('#compra-cedula_autofactura').on('change focusout', function() {
    let cedulaField = $(this);
    let tipoDocField = $('#compra-tipo_documento_id');
    let nombreField = $('#compra-nombre_completo_autofactura');
    if (tipoDocField.val() !== '' && tipoDocField.select2('data')[0]['autofactura'] == 'si') {
        if (cedulaField.val() === '')
            nombreField.val('').trigger('change');
        else {
            $.ajax({
                url: $url_getClienteAutofacturaByCedula, 
                type: 'get',
                data: {
                    cedula: cedulaField.val()
                },
                success: function (result) {
                    nombreField.val(result.data).trigger('change');
                    if (result.error.length) {
                        nombreField.val(result.error).trigger('change');
                    }
                    if (result.data.length) {
                        nombreField.prop('readonly', true);
                    } else {
                        nombreField.prop('readonly', false);                        
                    }
                }
            });
        }
    }
});

$(document).ready(function () {
    localStorage.removeItem('recentlyAddedTim');
    // Mostrar campos para autofactura
    // console.log('llamar showHideFieldsAutoFactura() desde document.ready');
    showHideFieldsAutofactura();
    
    // cambiar mascara de nrofactura segun sea sin o con timbrado
    changeMaskNroFactura();

    var a = $("#compra-timbrado_id").parent().parent()[0];
    if ("$actionId" === 'update')
        $('<div id = "compra-button-update_id" style="display: block; float:left; width:4%;" class="col-sm-2"><div class="form-group field-compra-button-add">$boton_update<div class="help-block"></div></div></div>').insertAfter(a);
    $('<div id = "compra-button-add_id" style="display: block; float:left; width:4%;" class="col-sm-1"><div class="form-group field-compra-button-add">$boton<div class="help-block"></div></div></div>').insertAfter(a);

    // Mostrar/ocultar campos para cuotas al inicio del formulario
    var condicion_field = $('#compra-condicion');
    if (condicion_field.length) {
        condicion_field.parent().parent()[0].style.display = "none";
        // Mostrar/ocultar campos para cuotas al inicio del formulario
        showHideCuotasField(condicion_field.val());
    }

    // Poner focus en tipo_documento_id si es form para create, para que se despliegue el select2.
    // Desplegar select2 de tipo_documento si es para crear.
    var tipo_doc = $('#compra-tipo_documento_id');
    if ("$actionId" === 'create') {
        tipo_doc.select2('open');
        
        $('input[id$="plantilla_id"]').each(function (i, e) {
            if ($(this).val() !== '')
                $(this).trigger('change');
        })
    }

    $('#compra-total-disp').prop('disabled', true);
    $('#compra-_total_ivas-disp').prop('disabled', true);
    $('#compra-_total_gravadas-disp').prop('disabled', true);

    cargarTimbrado();
    if (esNota) {
        $('#factura_form').on('beforeSubmit', function () {
            $('.select2-hidden-accessible').prop('disabled', false);
        });
        if (!isNewRecord) {
            $('#compra-tipo').prop('disabled', true);
            $('#compra-factura_compra_id').prop('disabled', true);
        }
    } else {
        disableCotizacion();
    }
    let css = {
        'title-css': {
            'background-color': '#$infoColor',
            'color': 'white',
            'text-align': 'center',
            'font-weight': 'bold'
        },
        'placement': 'top'
    }; //D9EDF7
    let content = 'Si la lista aparece vacía, verifique:<br/><br/>' +
        '<ol>' +
        '<li>Existe al menos una plantilla para compra registrada en el sistema.</li>' +
        '<li>Existe al menos una plantilla para compra asociada a la obligación seleccionada más arriba.</li>' +
        '</ol>';
    applyPopOver($('span.popupover-plantilla'), 'Atención', content, css);
    
    css = {
        'title-css': {
            'background-color': '#$warnColor',
            'color': 'white',
            'text-align': 'center',
            'font-weight': 'bold'
        },
        'placement': 'top'
    }; //D9EDF7
    content = 'Al cambiar de obligación, se eliminarán las plantillas agregadas previamente.';
    // PopOver sobre Tipo de documento.
    let label = $('label[for="compra-obligacion_id"]');
    let html = label.html() + '$infoSpan';
    label.html(html);
    applyPopOver(label, 'Atención', content, css);
    
    // PopOver sobre Tipo de documento.
    label = $('label[for="compra-tipo_documento_id"]');
    html = label.html() + '$infoSpan';
    label.html(html);
    let body = "Si desea poder visualizar los campos para autofactura, el Tipo de documento correspondiente a " +
     "Autofacturas debe estar parametrizado en la Empresa Actual.";
    applyPopOver($('span.tipo_documento'), 'Atención', body, {'theme-color': 'info-l', 'placement': 'top'});
});
JS;

$scripts [] = $script;

$script2 = "
// Al cambiar el monto de la factura recalcular los detalles.
$('#{}').keydown(function (event) {
    if (event.which === 9 || event.keycode === 9 || event.which === 13 || event.keycode === 13){
        $(this).val($(this).val()).trigger('change');
    }
});

//$('#{}').on('change', function () {
//console.log('desde \#\{\}: #{}');
//    setTotalFactura(true);
//});
";

$id_ivas_input = [];
foreach (array_keys($totales) as $nombre) {
    if (strpos($nombre, 'impuesto') === false) {
        $scripts[] = str_replace('{}', 'totales_iva-' . $nombre . '-disp', $script2);
        $id_ivas_input[] = 'totales_iva-' . $nombre . '-disp';
    }
}

$string_iva_disabled = "";
foreach ($id_ivas_input as $in) {
    $string_iva_disabled = $string_iva_disabled . "$('#" . $in . "').prop('disabled', true);";
}

$scripts[] = $string_iva_disabled;

foreach ($scripts as $script) $this->registerJs($script);

$JQ_VARIABLES = <<<JS
var loading = true;
var timbradoDetalleId = "$model->timbrado_detalle_id";
JS;

if ($es_nota && !empty($model->factura_compra_id)) {
    if (!empty($model->facturaCompra->tipoDocumento)) {
        $td_id = $model->facturaCompra->tipo_documento_id;
    } else {
        $_fac = $model->getFacturaCompra()->one();
        $td_id = $_fac->tipo_documento_id;
    }
    $_digitos = $model->moneda->tiene_decimales == 'si' ? 2 : 0;
    $fecha_em_fac_orig = $model->tipo == 'nota_debito' ? date('d-m-Y', strtotime($model->facturaCompra->facturaCompra->fecha_emision)) : '';
    $JQ_VARIABLES .= <<<JS
var tipoDocumentoParaNota = $td_id;
var digitosMonedaParaNota = $_digitos;
var fechaEmisionFacturaOriginal = "$fecha_em_fac_orig";
JS;
} else {
    $JQ_VARIABLES .= <<<JS
var tipoDocumentoParaNota;
var digitosMonedaParaNota;
var fechaEmisionFacturaOriginal;
var nombre_autofactura = '{$model->nombre_completo_autofactura}';
var cedula_autofactura = '{$model->cedula_autofactura}';
JS;
}
$this->registerJs($JQ_VARIABLES, \yii\web\View::POS_HEAD);
