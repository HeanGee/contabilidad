<?php

use backend\modules\contabilidad\models\Compra;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Timbrado */
/* @var $model_detalle backend\modules\contabilidad\models\TimbradoDetalle */
/* @var $form ActiveForm */
?>

<div class="alert alert-warning">
    <strong>Atención!</strong> Esta acción se aplicará directamente en la base de datos.
</div>

<div class="timbrado-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php
    $form = ActiveForm::begin([
        'id' => '_modalform_facturacompra_updatetimbrado',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]);

    try {
        $attributes = [
            'nro_timbrado' => [
                'type' => Form::INPUT_RAW,
                'value' => $form->field($model, 'nro_timbrado')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9', 'clientOptions' => ['repeat' => 8, 'greedy' => false]
                ])
            ],
            'fecha_inicio' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DatePicker::className(),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'language' => 'es',
//                    'hashVarLoadPosition' => View::POS_READY,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'weekStart' => '0',
                    ],
                    'disabled' => true,
                    'pluginEvents' => [
                    ]
                ],
            ],
            'fecha_fin' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DatePicker::className(),
                'options' => [
//                    'hashVarLoadPosition' => View::POS_READY,
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'language' => 'es',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'weekStart' => '0',
                    ],
                    'disabled' => true,
                    'pluginEvents' => [
                    ]
                ],
            ],
        ];
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => $attributes
        ]);

        $attributes = [
            'prefijo' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => MaskedInput::class,
                'options' => [
                    'name' => 'prefijo',
                    'mask' => '999-999',
                    'options' => [
                        'class' => 'form-control',
                        'disabled' => true
                    ]
                ],
                'label' => 'Prefijo'
            ],
            'nro_inicio' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder ' => 'Inicio...', 'disabled' => true],
                'label' => 'Numero inicio',
            ],
            'nro_fin' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder ' => 'Fin...', 'disabled' => true],
                'label' => 'Numero fin'
            ],
        ];
        echo Form::widget([
            'model' => $model_detalle,
            'form' => $form,
            'columns' => 3,
            'attributes' => $attributes
        ]);
    } catch (\Exception $e) {
        echo $e;
    }

    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $controller = Yii::$app->controller->id;
    $tipodocNotaCreditoId = Compra::getTipodocNotaCredito()->id;
    $tipodocNotaDebitoId = Compra::getTipodocNotaDebito()->id;
    $url_timbrado_load = Json::htmlEncode(\Yii::t('app', Url::to(["{$controller}/cargar-timbrado"])));
    $scripts = [];
    $scripts[] = <<<JS
// obtener la id del formulario y establecer el manejador de eventos
$("#_modalform_facturacompra_updatetimbrado").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
    .done(function (result) {
        $.pjax.reload({container: "#flash_message_id", async: false});
        $("#modal").modal("hide");
        cargarTimbrado2();
    });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

var cargarTimbrado2 = ("{$controller}" ==='compra-nota') ? function () {
    var getTipodocumentoId = function () {
        return (esCredito() ? "{$tipodocNotaCreditoId}" : "{$tipodocNotaDebitoId}");
    };
    var esCredito = function () {
        return ($('#compra-tipo').select2('data')[0]['id'] === "nota_credito");
    };
    console.log('cargarTimbrado de compra-nota/form');
    var tipo_doc_id = getTipodocumentoId();
    var nro_factura = $('#compra-nro_factura').val();
    var entidad_ruc = $('#compra-ruc').val();
    var fecha_emision = $('#compra-fecha_emision').val();
    if (nro_factura.split("_").join("").length === 15 && entidad_ruc !== "" && tipo_doc_id !== "") {
        $.ajax({
            url: $url_timbrado_load,
            type: 'post',
            data: {
                entidad_ruc: entidad_ruc,
                nro_factura: nro_factura,
                fecha_emision: fecha_emision,
                timbrado_vencido: '0',
                tipodocumento_id: tipo_doc_id,
            },
            success: function (data) {
                console.log(data);
                if (data === 'mostrar_boton') {
                    // $('#compra-button-add_id')[0].style.display = '';
                    // $('#compra-timbrado').val('');
                    $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": '', "text": ''}});
                    $('#boton_add_timbrado').prop('disabled', false);
                    $('#boton_update_timbrado').prop('disabled', true);
                } else {
                    if (data !== '') {
                        $("#compra-timbrado_id").data('select2').trigger('select', {
                            data: {
                                "id": data['id'],
                                "text": data['nro_timbrado'],
                                "vencido": data['vencido']
                            }
                        });
                        // $('#boton_add_timbrado').prop('disabled', true);
                        $('#boton_update_timbrado').prop('disabled', false);
                    }
                }
            }
        });
    } else {
        // $('#compra-button-add_id')[0].style.display = 'none';
        // $('#compra-timbrado').val('');
    }
} : function () {
    console.log('cargarTimbrado de _modalform_facturacompra_updatetimbrado', "{$controller}");
    var tipo_doc_id = $('#compra-tipo_documento_id').val();
    var nro_factura = $('#compra-nro_factura').val();
    var entidad_ruc = $('#compra-ruc').val();
    var fecha_emision = $('#compra-fecha_emision').val();
    if (nro_factura.split("_").join("").length === 15 && entidad_ruc !== "" && tipo_doc_id !== "") {
        $.ajax({
            url: $url_timbrado_load,
            type: 'post',
            data: {
                entidad_ruc: entidad_ruc,
                nro_factura: nro_factura,
                fecha_emision: fecha_emision,
                timbrado_vencido: $('#compra-timbrado_vencido').val(),
                tipodocumento_id: $('#compra-tipo_documento_id').val(),
            },
            success: function (data) {
                if (data === 'mostrar_boton') {
                    // $('#compra-button-add_id')[0].style.display = '';
                    // $('#compra-timbrado').val('');
                    $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": '', "text": ''}});
                    $('#boton_add_timbrado').prop('disabled', false);
                    $('#boton_update_timbrado').prop('disabled', true); 
                } else {
                    if (data !== ''){
                        $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": data[0]['id'], "text": data[0]['nro_timbrado']}});
                        $('#boton_add_timbrado').prop('disabled', true);       
                        $('#boton_update_timbrado').prop('disabled', false);
                    }
                }
            }
        });
    } else {
        // $('#compra-button-add_id')[0].style.display = 'none';
        // $('#compra-timbrado').val('');
    }
};
JS;

    foreach ($scripts as $script) $this->registerJs($script, View::POS_READY); ?>
</div>