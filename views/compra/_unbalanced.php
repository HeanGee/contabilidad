<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 01/03/19
 * Time: 11:38 AM
 */

/* @var array $diferentes */
/* @var $this \yii\web\View */
?>

<div class="unbalanced">
    <div class="form-group">
        <table class="table table-condensed">
            <tr>
                <th>Factura</th>
                <th>Fecha emisión</th>
                <th>Debe</th>
                <th>Haber</th>
                <th style="text-align: center">Acciones</th>
            </tr>
            <?php foreach ($diferentes as $diferente) { ?>
                <tr>
                    <td style="color: <?= ($diferente['debe'] != $diferente['haber']) ? "red" : "black" ?>"><?= $diferente['id'] ?></td>
                    <td style="color: <?= ($diferente['debe'] != $diferente['haber']) ? "red" : "black" ?>"><?= $diferente['fechaEmision'] ?></td>
                    <td style="color: <?= ($diferente['debe'] != $diferente['haber']) ? "red" : "black" ?>"><?= $diferente['debe'] ?></td>
                    <td style="color: <?= ($diferente['debe'] != $diferente['haber']) ? "red" : "black" ?>"><?= $diferente['haber'] ?></td>
                    <td style="text-align: center; color: <?= ($diferente['debe'] != $diferente['haber']) ? "red" : "black" ?>"><?= $diferente['ver'] ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

