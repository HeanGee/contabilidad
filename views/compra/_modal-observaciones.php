<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 16/08/2018
 * Time: 11:05
 */

use backend\helpers\HtmlHelpers;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;

?>

<?php
ActiveFormDisableSubmitButtonsAsset::register($this);

$form = ActiveForm::begin([
    'id' => 'modal-observacion-form',
    'options' => ['class' => 'disable-submit-button'],
]);

try {
    echo Form::widget([
        'formName' => 'kvform',
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'observaciones' => [
                'type' => Form::INPUT_TEXTAREA,
                'label' => "Motivo",
                'options' => [
                    'placeholder' => "Máxima cantidad de letras: 255",
                    'maxlength' => 255,
                    'style' => "margin: 0px 1.00781px 0px 0px; height: 377px; width: 570px;",
                ],
            ],
        ],
    ]);
} catch (Exception $exception) {
    echo $exception->getMessage();
}
?>

    <br/>
    <div class="form-group">
        <?= Html::button('Borrar', [
            'onclick' => "clicked();",
            'data' => ['disabled-text' => 'Borrando...',],
            'class' => 'btn btn-danger']) ?>

        <?= Html::submitButton('', [
            'id' => "submit-btn",
            'data' => ['disabled-text' => '',],
            'class' => 'hidden']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php

$css = <<<CSS
.modal-header {
    background-color: #$ !important;
}
CSS;
$this->registerCss(str_replace('$', HtmlHelpers::DangerColorHex(), $css));

ob_start(); // output buffer the javascript to register later ?>
    <script>
        function clicked() {
            krajeeDialog.confirm("¿Está seguro de eliminar esta factura?", function (result) {
                if (result) {
                    $('#submit-btn').click();
                } else {
                    $("#modal").modal("hide");
                    $("modal-body").html("");
                }
            });
        }
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>