<?php

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\TipoDocumento;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\checkbox\CheckboxX;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Compra */
/* @var $form yii\widgets\ActiveForm */

$action = Yii::$app->controller->action->id;
?>

<div class="compra-form">

    <?php $form = ActiveForm::begin(['id' => 'factura_form']); ?>

    <?php try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'condicion' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '1'],
                    'value' => $form->field($model, 'condicion')->hiddenInput()->label('')
                ],
                'guardarycerrar' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '1'],
                    'value' => $form->field($model, 'guardarycerrar')->hiddenInput()->label('')
                ],
            ]
        ]);

        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'tipo_documento_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'tipo_documento_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione un tipo de documento ...'],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                    'data' => TipoDocumento::getTiposDocumentoConId('proveedor', ($action == 'update' ? $model->tipo_documento_id : null), 'nota_credito'),
                                ],
                                'initValueText' => !empty($model->tipo_documento_id) ? ($model->tipo_documento_id . ' - ' . $model->tipoDocumento->nombre) : ''
                            ])
                        ],
                        'ruc' => [
                            'label' => 'R.U.C.',
                            'columnOptions' => ['colspan' => '1'],
                            'type' => Form::INPUT_TEXT,
                        ],
                        'nombre_entidad' => [
                            'label' => 'Entidad (Proveedor)',
                            'columnOptions' => ['colspan' => '2'],
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'readonly' => true
                            ]
                        ],
                        'nro_factura' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1', 'style' => 'width:15%;'],
                            'value' => $form->field($model, 'nro_factura')->widget(MaskedInput::className(), [
                                'mask' => '999-999-9999999',
                                'options' => [
                                    'class' => 'form-control',
                                    'readonly' => $model->isNewRecord ? true : false
                                ]
                            ]),
                            'options' => ['placeholder' => 'Ingrese nro de factura...'],
                        ],
                        'fecha_emision' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'fecha_emision')->widget(MaskedInput::className(), [
                                'name' => 'fecha_vencimiento',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ]),
                        ],
                        'timbrado_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2', 'style' => 'width:13%;'],
                            'value' => $form->field($model, 'timbrado_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione uno ...'],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                    'ajax' => [
                                        'url' => Url::to(['timbrado/get-timbrados-vigentes']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression("
                                                    function(params) { 
                                                        return {
                                                            q:params.term, 
                                                            ruc:$('#compra-ruc').val(),
                                                            fecha:$('#compra-fecha_emision').val(),
                                                            nro_factura:$('#compra-nro_factura').val(),
                                                            tipo_documento_id: $('#compra-tipo_documento_id').val(),
                                                            vencidos: $('#compra-timbrado_vencido').val()
                                                        };
                                                    }
                                                "),
                                    ]
                                ],
                                'initValueText' => !empty($model->timbrado_id) ? $model->getTimbrado() : ''
                            ])
                        ],
                        'timbrado_vencido' => [
                            'type' => \kartik\builder\Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1', 'style' => 'width:3%;'],
                            'value' => $form->field($model, 'timbrado_vencido')->widget(
                                CheckboxX::className(), [
                                'pluginOptions' => ['size' => 'md', 'threeState' => false]
                            ])->label('Tim. Vencido'),
                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
//                        'estado' => [
//                            'type' => Form::INPUT_RAW,
//                            'columnOptions' => ['colspan' => '2'],
//                            'value' => $form->field($model, 'estado')->widget(Select2::className(), [
//                                'data' => Compra::getEstados(),
//                                'options' => [
//                                    'placeholder' => 'Seleccione un estado ...',
//                                ],
//                                'pluginOptions' => [
//                                    'allowClear' => false,
//                                ],
//                            ])
//                        ],
                        'moneda_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '4'],
                            'value' => $form->field($model, 'moneda_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione una moneda'],
	                            'data' => Compra::getMonedas(true, false),
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                                'initValueText' => !($model->moneda_id != '') ? ($model->moneda_id . ' - ' . $model->moneda->nombre) : ''
                            ])
                        ],
                        'cotizacion' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'cotizacion')->widget(NumberControl::className(), [
                                'value' => 0.00,
                                'maskedInputOptions' => [
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'rightAlign' => false,
                                    'allowMinus' => false,
                                ],
                            ]),
                        ],
                        'cotizacion_propia' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'cotizacion_propia')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione una moneda'],
                                'pluginOptions' => [
                                    'data' => Compra::getCotizacionPropiaOptions(),
                                    'allowClear' => true,
                                ],
                                'initValueText' => isset($model->cotizacion_propia) ? ucfirst($model->cotizacion_propia) : "",
                            ]),
                        ],
                        //'nota_remision' => [
                        //    'type' => Form::INPUT_RAW,
                        //    'columnOptions' => ['colspan' => '2'],
                        //    'value' => $form->field($model, 'nota_remision')->widget(MaskedInput::className(), [
                        //        'mask' => '999-999-9999999',
                        //    ]),
                        //    'options' => ['placeholder' => 'Ingrese nota de remisión...'],
                        //],
                        'cant_cuotas' => [
                            'columnOptions' => ['colspan' => '2'],
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($model, 'cant_cuotas')->widget(NumberControl::className(), [
                                'value' => 0.00,
                                'maskedInputOptions' => [
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'rightAlign' => false,
                                    'allowMinus' => false,
                                ],
                            ]),
                        ],
                        'fecha_vencimiento' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'fecha_vencimiento')->widget(MaskedInput::className(), [
                                'name' => 'fecha_vencimiento',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ]),
                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 4,
                    'attributes' => [
                        'obligacion_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1'],
                            'value' => $form->field($model, 'obligacion_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione una obligac...'],
                                'pluginOptions' => [
                                    'data' => Compra::getObligacionesEmpresaActual(Yii::$app->session->get('core_empresa_actual'), ($action == 'update') ? $model : null),
                                    'allowClear' => false,
                                ],
                                'initValueText' => isset($model->obligacion_id) ? ucfirst($model->obligacion->nombre) : "",
                            ]),
                        ],
                        'para_iva' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1'],
                            'value' => $form->field($model, 'para_iva')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione una obligac...'],
                                'data' => ['si' => "Sí", 'no' => "No"],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                                'initValueText' => ($model->para_iva == 'si') ? "Sí" : 'No',
                            ]),
                        ],
                        'cedula_autofactura' => [
                            'label' => 'Cédula',
                            'columnOptions' => ['colspan' => '1'],
                            'type' => Form::INPUT_TEXT,
                        ],
                        'nombre_completo_autofactura' => [
                            'label' => 'Nombre',
                            'columnOptions' => ['colspan' => '1'],
                            'type' => Form::INPUT_TEXT,
                        ],
                    ],
                ],
            ]
        ]);
    } catch (Exception $e) {
        throw $e;
        echo $e->getMessage();
    };
    ?>

    <div style="display: none;" class="field-btn-simulator-hidden">
        <?php echo Html::button('', ['id' => 'btn-hidden-simulator']) ?>
    </div>

    <!--    Plantillas Dinamicas-->
    <?php echo $this->render('_fieldset', ['model' => $model, 'form' => $form]); ?>

    <?php
    $totales = [];
    $impuestos = [];
    $gravadas = [];
    /** @var IvaCuenta $iva_cta */
    foreach (IvaCuenta::find()->all() as $iva_cta) {
        $totales['iva-' . $iva_cta->iva->porcentaje . ''] = [
            'label' => 'Total ' . (($iva_cta->iva->porcentaje !== 0) ? $iva_cta->iva->porcentaje . '%' : 'Exenta'),
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => NumberControl::class,
            'columnOptions' => ['colspan' => '2'],
            'value' => 0.00,
            'options' => [
                'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'allowMinus' => false,
                ],
            ],
        ];
        if ($iva_cta->iva->porcentaje != 0) {
            $impuestos['impuesto-' . $iva_cta->iva->porcentaje . ''] = [
                'label' => 'I.V.A. ' . $iva_cta->iva->porcentaje . '%',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::class,
                'columnOptions' => ['colspan' => '2'],
                'options' => [
                    'value' => 0.00,
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'rightAlign' => true,
                        'allowMinus' => false,
                    ],
                    'disabled' => true,
                ],
            ];
            $gravadas['gravada-' . $iva_cta->iva->porcentaje . ''] = [
                'label' => 'Gravada ' . $iva_cta->iva->porcentaje . '%',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::class,
                'columnOptions' => ['colspan' => '2'],
                'options' => [
                    'value' => 0.00,
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'rightAlign' => true,
                        'allowMinus' => false,
                    ],
                    'disabled' => true,
                ],
            ];
        }
    }
    // Ordenar campos
    uksort($totales, 'strnatcasecmp');
    uksort($impuestos, 'strnatcasecmp');
    uksort($gravadas, 'strnatcasecmp');

    // concatenar para renderizar con Form::widget
    //    $totales = array_merge($totales, $impuestos);
    $discriminados = array_merge($impuestos, $gravadas);

    try {
        echo '<fieldset><legend>Sumatoria</legend></fieldset>';
        echo Form::widget([
            // formName is mandatory for non active forms
            // you can get all attributes in your controller
            // using $_POST['kvform']
            'formName' => 'totales_iva',

            // default grid columns
            'columns' => 12,

            // set global attribute defaults
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['colspan' => '2'],
                'inputContainer' => ['colspan' => '2'],
                'container' => ['class' => 'form-group'],
            ],

            'attributes' => $totales,
        ]);
        echo '<fieldset><legend>Discriminados</legend></fieldset>';
        echo Form::widget([
            // formName is mandatory for non active forms
            // you can get all attributes in your controller
            // using $_POST['kvform']
            'formName' => 'discriminados',

            // default grid columns
            'columns' => 12,

            // set global attribute defaults
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['colspan' => '2'],
                'inputContainer' => ['colspan' => '2'],
                'container' => ['class' => 'form-group'],
            ],

            'attributes' => $discriminados,
        ]);

        echo '<fieldset><legend>Totales</legend></fieldset>';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'attributes' => [
                        'array' => [
                            'columns' => 12,
                            'attributes' => [
                                'total' => [
                                    'type' => Form::INPUT_RAW,
                                    'value' => $form->field($model, 'total')->widget(NumberControl::className(), [
                                        'value' => 0.00,
                                        'maskedInputOptions' => [
                                            'groupSeparator' => '.',
                                            'radixPoint' => ',',
                                            'rightAlign' => false,
                                            'allowMinus' => false,
                                        ],
                                    ]),
                                    'columnOptions' => ['colspan' => '3']
                                ],
                                '_total_ivas' => [
                                    'type' => Form::INPUT_RAW,
                                    'value' => $form->field($model, '_total_ivas')->widget(NumberControl::className(), [
                                        'value' => 0.00,
                                        'maskedInputOptions' => [
                                            'groupSeparator' => '.',
                                            'radixPoint' => ',',
                                            'rightAlign' => false,
                                            'allowMinus' => false,
                                        ],
                                    ]),
                                    'columnOptions' => ['colspan' => '3']
                                ],
                                '_total_gravadas' => [
                                    'type' => Form::INPUT_RAW,
                                    'value' => $form->field($model, '_total_gravadas')->widget(NumberControl::className(), [
                                        'value' => 0.00,
                                        'maskedInputOptions' => [
                                            'groupSeparator' => '.',
                                            'radixPoint' => ',',
                                            'rightAlign' => false,
                                            'allowMinus' => false,
                                        ],
                                    ]),
                                    'columnOptions' => ['colspan' => '3']
                                ],
                            ]
                        ]
                    ],
                ],
            ]
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    ?>

    <?php
    $botonText = '';
    $botonClass = '';
    $botonCerrarClass = '';
    $botonCerrarText = '';
    if ($botonText = Yii::$app->controller->action->id == 'create') {
        $botonText = 'Guardar y Siguiente';
        $botonCerrarText = 'Guardar y Cerrar';
        $botonClass = 'btn btn-success';
        $botonCerrarClass = 'btn btn-primary';
    } else {
        $botonText = 'Guardar';
        $botonClass = 'btn btn-primary';
    } ?>

    <div class="form-group">
        <?= Html::submitButton($botonText, ['id' => 'btn_submit_factura_compra', 'class' => $botonClass,
            "data" => (Yii::$app->controller->action->id == 'update') ? [
                'confirm' => 'Desea guardar los cambios?',
                'method' => 'post',
            ] : []]) ?>

        <?php
        if (Yii::$app->controller->action->id == 'create') {
            echo Html::submitButton($botonCerrarText, ['id' => 'btn_submit_factura_compra_guardar_cerrar', 'class' => $botonCerrarClass,
                "data" => (Yii::$app->controller->action->id == 'update') ? [
                    'confirm' => 'Desea guardar los cambios?',
                    'method' => 'post',
                ] : [
                    'method' => 'post',
                ]
            ]);
        }
        ?>
    </div>

    <?php echo $this->render('_form_detalle', []); ?>
    <?php echo $this->render('_form_asiento_simulator', []); ?>
    <?php echo $this->render('_form_asientosim_cuota_prestamo', []); ?>

    <?php ActiveForm::end(); ?>
    <!--    Cargamos js dede otro archivo-->
    <?php echo $this->render('_form_js', ['totales' => $totales, 'model' => $model]); ?>

    <?php
    $this->registerJs('
        $("#btn_submit_factura_compra_guardar_cerrar").on("click", function (e) {
            $("#compra-guardarycerrar").val("si");
        });
        
        $("#btn_submit_factura_compra").on("click", function (e) {
            $("#compra-guardarycerrar").val("no");
        });
    ');
    ?>
</div>
