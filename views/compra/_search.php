<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\search\CompraSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compra-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tipo_documento_id') ?>

    <?= $form->field($model, 'fecha_emision') ?>

    <?= $form->field($model, 'condicion') ?>

    <?= $form->field($model, 'fecha_vencimiento') ?>

    <?php // echo $form->field($model, 'entidad_id') ?>

    <?php // echo $form->field($model, 'cotizacion_id') ?>

    <?php // echo $form->field($model, 'obligaciones') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'nro_factura') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
