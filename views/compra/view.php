<?php

use backend\modules\contabilidad\controllers\CompraController;
use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\DetalleCompra;
use backend\modules\contabilidad\models\PrestamoDetalleCompra;
use common\helpers\FlashMessageHelpsers;
use common\helpers\ValorHelpers;
use kartik\detail\DetailView;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Compra */

$this->title = "Datos de la compra " . $model->nro_factura;
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compra-view">

    <p>
        <?= CompraController::getAcceso('index') ? Html::a('Ir a Compras', ['index'], ['class' => 'btn btn-info']) : '' ?>
        <?= CompraController::getAcceso('update') && $model->bloqueado == 'no' ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= CompraController::getAcceso('delete-with-obs') && $model->bloqueado == 'no' ? Html::a('Borrar', 'javascript:void(0);', [
            'class' => 'btn btn-danger borrar-factura-compra',
            'data' => [
                'data-toggle' => 'modal',
                'target' => '#modal',
                'url' => Url::to(['delete-with-obs', 'id' => $model->id]),
                'title' => Yii::t('yii', 'Escriba el motivo del borrado'),
            ],
        ]) : '' ?>
        <?= "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"?>
        <?= CompraController::getAcceso('bloquear') && $model->bloqueado == 'no' ? Html::a('<span>Bloquear</span>', ['bloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'title' => Yii::t('app', 'Bloquear'),
                'data-confirm' => 'Está seguro que desea bloquear la factura?'
            ]) : '' ?>
        <?= CompraController::getAcceso('desbloquear') && $model->bloqueado == 'si' ? Html::a('<span>Desbloquear</span>', ['desbloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-success',
                'title' => Yii::t('app', 'Desbloquear'),
                'data-confirm' => 'Está seguro que desea desbloquear la factura?'
            ]) : '' ?>
    </p>

    <?php try {
        $attributes = [
            [
                'columns' => [
                    [
                        'label' => 'Nro de Factura',
                        'value' => $model->nro_factura,
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Total factura',
                        'value' => ValorHelpers::numberFormatMonedaSensitive($model->total, 2, $model->moneda),
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Timbrado',
                        'format' => "raw",
                        'value' => isset($model->timbradoDetalle) ? $model->timbradoDetalle->timbrado->nro_timbrado . " <strong>(id: {$model->timbradoDetalle->timbrado_id})</strong>" : '-sin timbrado-',
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Saldo',
                        'value' => ValorHelpers::numberFormatMonedaSensitive($model->saldo, 2, $model->moneda),
                        'valueColOptions' => ['style' => 'width:30%']
                    ],

                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Tipo de Documento',
                        'value' => $model->tipo_documento_id != null ? $model->tipoDocumento->nombre : '',
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Exenta',
                        'value' => ValorHelpers::numberFormatMonedaSensitive($model->exenta, 2, $model->moneda),
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Fecha de emisión',
                        'value' => date_create_from_format('Y-m-d', $model->fecha_emision) ?
                            date_create_from_format('Y-m-d', $model->fecha_emision)->format('d-m-Y') : "-no definido-",
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Gravada 10%',
                        'value' => ValorHelpers::numberFormatMonedaSensitive($model->gravada10, 2, $model->moneda),
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Entidad',
                        'format' => 'raw',
                        'value' => ($model->entidad_id) ? "{$model->entidad->razon_social} <strong>(RUC: {$model->entidad->ruc})</strong>" : '',
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Impuesto 10%',
                        'value' => ValorHelpers::numberFormatMonedaSensitive($model->impuesto10, 2, $model->moneda),
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Moneda',
                        'value' => ($model->moneda_id) ? $model->moneda->nombre : '',
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Gravada 5%',
                        'value' => ValorHelpers::numberFormatMonedaSensitive($model->gravada5, 2, $model->moneda),
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Empresa',
                        'value' => $model->empresa->razon_social,
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Impuesto 5%',
                        'value' => ValorHelpers::numberFormatMonedaSensitive($model->impuesto5, 2, $model->moneda),
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Estado',
                        'format' => 'raw',
                        'value' => '<label class="label label-' . (($model->estado == 'vigente') ? 'success">Vigente' : (($model->estado == 'faltante') ? 'warning">Faltante' : 'danger">Anulada')) . '</label>',
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Plantillas',
                        'value' => $model->plantillas,
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Creado',
                        'format' => 'raw',
                        'value' => "{$model->creado} <strong>(Por `{$model->creado_por}`)</strong>",
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                    [
                        'label' => 'Modificado',
                        'value' => $model->modificado,
                        'valueColOptions' => ['style' => 'width:30%']
                    ],
                ],
            ],
            [
                'columns' => [
                    [
                        'label' => 'Timbrado Detalle',
                        'value' => $model->timbrado_detalle_id,
                        'valueColOptions' => ['style' => 'width:100%']
                    ],
                ],
            ],
        ];

        if ($model->nombre_completo_autofactura != '') {
            $attributes[] =
                [
                    'columns' => [
                        [
                            'label' => 'Nombre del Autofacturado',
                            'value' => $model->nombre_completo_autofactura,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Cédula',
                            'value' => $model->cedula_autofactura,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ];
        }

        // cabecera
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Factura ' . $model->nro_factura,
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => $attributes

        ]);

        // detalles
        $dataProvider = new ActiveDataProvider([
            'query' => DetalleCompra::find()->where(['factura_compra_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'panel' => ['type' => 'info', 'heading' => 'Detalles', 'footer' => false,],
            'toolbar' => [],
            'hover' => true,
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
            'columns' => [
                'id',
                [
                    'class' => DataColumn::className(),
                    'label' => 'Código de cuenta',
                    'value' => 'planCuenta.cod_completo'
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Nombre de cuenta',
                    'value' => 'planCuenta.nombre',
                    'pageSummaryOptions' => [
                        'data-colspan-dir' => 'ltr',
                        'colspan' => 1,
                        'style' => 'text-align: right;',
                    ],
                    'pageSummary' => function ($summary, $data, $widget) {
                        return "Totales";
                    },
                    'contentOptions' => function ($model) {
                        if ($model->cta_contable == 'debe') return ['style' => 'padding:8px 6px 0px 10px; text-align:left'];
                        return ['style' => 'padding:8px 6px 0px 150px; text-align:left'];
                    },
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Debe',
                    'value' => function ($model) {
                        /** @var $model DetalleCompra */
                        if ($model->cta_contable == 'debe') return $model->subtotal;
                        return 0;
                    },
                    'format' => 'decimal',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'append' => '',
                        'prepend' => '',
                    ],
                    'contentOptions' => ['style' => 'width: 12%; text-align:right'],
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Haber',
                    'value' => function ($model) {
                        /** @var $model DetalleCompra */
                        if ($model->cta_contable == 'haber') return $model->subtotal;
                        return 0;
                    },
                    'format' => 'decimal',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'append' => '',
                        'prepend' => '',
                    ],
                    'contentOptions' => ['style' => 'width: 12%; text-align:right'],
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Cuenta principal ?',
                    'value' => function ($model) {
                        if ($model->cta_principal == 'si')
                            return ucfirst($model->cta_principal);
                        return '';
                    },
                    'contentOptions' => ['style' => 'width: 8%; text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;'],
                ],
            ],
        ]);

        // prestamos
        $query = PrestamoDetalleCompra::find()->where(['factura_compra_id' => $model->id]);
        if ($query->exists()) {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => false,
            ]);
            echo GridView::widget([
                'id' => 'grid-cuotas-facturadas',
                'dataProvider' => $dataProvider,
                'toolbar' => [],
                'hover' => true,
                'panel' => [
                    'type' => 'info',
                    'heading' => 'Cuotas de Préstamos Facturadas',
                    'footerOptions' => ['class' => ''],
                    'beforeOptions' => ['class' => ''],
                    'afterOptions' => ['class' => '']
                ],
                'panelFooterTemplate' => '',
                'columns' => [
                    [
                        'label' => 'Nº Crédito del Préstamo',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $url = Url::to(['prestamo/view', 'id' => $model->prestamoDetalle->prestamo_id]);
                            return Html::a($model->prestamoDetalle->prestamo_id, $url);
                        },
                        'contentOptions' => ['style' => 'text-align:center'],
                        'headerOptions' => ['style' => 'width:5%'],
                    ],
                    [
                        'label' => 'Nº Cuota',
                        'value' => 'prestamoDetalle.nro_cuota',
                        'contentOptions' => ['style' => 'text-align:center'],
                        'headerOptions' => ['style' => 'width:6%'],
                    ],
                    [
                        'label' => 'Capital',
                        'value' => 'capitalFormatted',
                        'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                        'headerOptions' => ['style' => 'width:15%'],

                    ],
                    [
                        'label' => 'Interés',
                        'value' => 'interesFormatted',
                        'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                        'headerOptions' => ['style' => 'width:15%'],

                    ],
                    [
                        'label' => 'Cuota',
                        'value' => 'montoCuotaFormatted',
                        'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                        'headerOptions' => ['style' => 'width:15%'],

                    ],
                ],
            ]);
        }

        // notas
        $query = Compra::find()->where(['factura_compra_id' => $model->id]);
        if ($query->exists()) {
            $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => false]);
            echo $this->render('../compra-nota/view/_view_notas',
                ['dataProvider' => $dataProvider, 'heading' => "Notas de Crédito"]);
        }

        // Mostrar activos fijos comprados
        $activosFijos = \backend\modules\contabilidad\models\ActivoFijo::findAll(['factura_compra_id' => $model->id]);
        if (!empty($activosFijos)) {
            echo $this->render('../activo-fijo/views/_view_actfijos', [
                'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $activosFijos, 'pagination' => false]),
                'heading' => 'Datos de Activos Fijos comprados'
            ]);
        }

        // Mostrar activos fijos biologicos comprados
        $actbioManagers = ActivoBiologicoStockManager::find()->where(['factura_compra_id' => $model->id]);
        if ($actbioManagers->exists()) {
            $allModels = [];
            /** @var ActivoBiologicoStockManager $manager */
            foreach ($actbioManagers->all() as $manager) {
                $allModels[] = $manager->activoBiologico;
            }
            echo $this->render('../activo-biologico/views/_view_actbios', [
                'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $allModels, 'pagination' => false]),
                'heading' => 'Datos de Activos Biológicos comprados'
            ]);

            $dataProvider = new ActiveDataProvider([
                'query' => $actbioManagers, 'pagination' => false,
            ]);
            echo GridView::widget([
                'id' => 'grid-test',
                'dataProvider' => $dataProvider,
                'toolbar' => [],
                'hover' => true,
                'panel' => [
                    'type' => 'info',
                    'heading' => 'Cantidades compradas por Activos Biologicos',
                    'footerOptions' => ['class' => ''],
                    'beforeOptions' => ['class' => ''],
                    'afterOptions' => ['class' => '']
                ],
                'panelFooterTemplate' => '',
                'columns' => [
                    [
                        'label' => 'Activo',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ActivoBiologicoStockManager $manager */
                            $manager = $model;
                            $activo = ActivoBiologico::findOne(['especie_id' => $manager->especie_id, 'clasificacion_id' => $manager->clasificacion_id]);
                            if (!isset($activo)) return '';

                            $url = Url::to(['activo-biologico/view', 'id' => $activo->id]);
                            $link = Html::a($activo->nombre, $url, []);
                            return $link;
                        },
                        'contentOptions' => ['style' => 'text-align:center'],
                        'headerOptions' => ['style' => 'text-align:center; width:5%'],
                    ],
                    [
                        'label' => 'Cantidad Comprada',
                        'value' => function ($model) {
                            /** @var ActivoBiologicoStockManager $manager */
                            $manager = $model;
                            return $manager->cantidad;
                        },
                        'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:center'],
                        'headerOptions' => ['style' => 'text-align:center; width:15%'],

                    ],
                ],
            ]);
        }

    } catch (Exception $exception) {
        echo $exception;
        FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    }

    echo $this->render('../auditoria/_auditoria', ['modelo' => $model]);
    echo $this->render('../auditoria/_auditoria_multiple_models', ['modelo' => new DetalleCompra(), 'ids' => $model->getDetallesId()]);
    //    echo $this->render('../auditoria/_auditoria_multiple_models', ['query' => AuditTrail::find()
    //        ->onCondition([
    //            'audit_trail.model_id' => ArrayHelper::map($model->getDetallesCompra()->all(), 'id', 'id'),
    //            'audit_trail.model' => get_class(new DetalleCompra()),
    //        ])]);
    ?>

</div>

<?php
ob_start(); // output buffer the javascript to register later ?>
    <script>
        $(document).on('click', '.borrar-factura-compra', function () {
            var boton = $(this);
            var title = boton.data('title');
            console.log('aqui');
            $.get(
                boton.data('url'),
                function (data) {
                    var modal = $(boton.data('target'));
                    console.log(modal, boton.data('url'));
                    $('.modal-body', modal).html(data);
                    modal.modal();
                    if (title)
                        $('.modal-title', modal).html(title);
                }
            );
        });

        $("#modal").on('shown.bs.modal', function () {
            $(this).find('textarea').focus();
        });
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>