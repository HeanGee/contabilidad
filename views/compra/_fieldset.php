<?php

use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use kartik\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Compra */
/* @var $form yii\widgets\ActiveForm */
/* @var $mostrar_cabecera boolean */

$es_nota = in_array($model->tipo, ['nota_credito', 'nota_debito']) ? $model->tipo : false;
$mostrar_cabecera = !empty($mostrar_cabecera);

?>
<fieldset>
    <legend><?php echo (!$es_nota ? ' &nbsp; ' : '') . 'Plantillas';
        // new parcel button
        if (!$es_nota)
            echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                'id' => 'iva-cta-usada-new-button',
                'class' => 'pull-left btn btn-success'
            ]);
        ?>
    </legend>
    <?php
    $iva_cuentas = IvaCuenta::find()->all();
    /** @var IvaCuenta $iva_cuenta */
    foreach ($iva_cuentas as $iva_cuenta)
        $porcentajes_iva[] = $iva_cuenta->iva->porcentaje;
    asort($porcentajes_iva);
    $porcentajes_iva = array_values($porcentajes_iva);
    $porcentajes_iva_json = Json::encode($porcentajes_iva);

    // parcel table
    $ivaCtaUsada = new CompraIvaCuentaUsada();
    $ivaCtaUsada->loadDefaultValues();
    echo '<table id="iva-cta-usadas" class="table table-condensed table-bordered">';
    echo '<thead>';
    echo '<tr>';
    echo '<th>Plantilla&nbsp;&nbsp;<span class="popuptrigger popupover-plantilla text-info glyphicon glyphicon-info-sign" style="padding: 0 4px;"></span></th>';
    foreach ($porcentajes_iva as $ic) {
        if ($ic == 0) {
            echo '<th>Exenta</th>';
        } else {
            echo '<th>Total ' . $ic . '%</th>';
        }
    }
    if (!$es_nota)
        echo '<th style="text-align: center">Motivo por el que se envia a GND</th>';
        echo '<th style="text-align: center">Acciones</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    \kartik\select2\Select2Asset::register($this);
    $noDeducible = $model->timbrado_vencido == 1 ? true : false;
    $paraImportacion = $model->tipo_documento_id != null && $model->tipoDocumento->para_importacion == 'si' ? true : false;
    $plantillaLista = PlantillaCompraventa::getPlantillasLista('compra', false, $noDeducible, $paraImportacion, $model->obligacion_id);
    $plantilla_data = !isset($plantillaLista) ? [] : Json::encode($plantillaLista);

    // existing parcels fields
    $i = 1;
    foreach ($model->_iva_ctas_usadas as $key => $_iva_cta) {
        echo '<tr>';
        echo $this->render('_form-iva-cuenta-usada', [
            'key' => 'new' . $i,
            'form' => $form,
            'ivaCtaUsada' => $ivaCtaUsada,
            'porcentajes_iva' => $porcentajes_iva,
            'es_nota' => $es_nota
        ]);
        echo '</tr>';
        $_s = <<<JS
            $('#compraivacuentausada-new$i-plantilla_id').select2({
                theme: 'krajee',
                placeholder: '',
                language: 'en',
                width: '100%',
                data: $plantilla_data
            });
            //console.log('llamando a generarEventoChangePlantilla()');
            generarEventoChangePlantilla('compraivacuentausada-new$i-plantilla_id', 'compraivacuentausada-new$i-ivas-iva_');
            reloadAllNumberField();
            $('#compraivacuentausada-new$i-plantilla_id').val($key).trigger('change');
            
JS;
        if ($es_nota) {
            $_s .= <<<JS
$('#compraivacuentausada-new$i-plantilla_id').prop('disabled', true);
JS;
        }
        $this->registerJs($_s);

        foreach ($_iva_cta as $ivaPorcentaje => $montoIva) {
            if (is_numeric($ivaPorcentaje)) // $ivactausda[plantilla_id][0/5/10/gnd_motivo] = some_value
                $this->registerJs("
                    $('#compraivacuentausada-new" . $i . "-ivas-iva_" . $ivaPorcentaje . "-disp').val(" . $montoIva . ").trigger('change');
                    $('#compraivacuentausada-new" . $i . "-ivas-iva_" . $ivaPorcentaje . "-disp').on('change', function () {
                        completarTotales();
                        console.log('desde fieldset.php linea 101');
                        setTotalFactura();
                    })
                    $('#compraivacuentausada-new" . $i . "-ivas-iva_" . $ivaPorcentaje . "-disp').prop('readonly', false);
                ");
            else
                $this->registerJs("$('#compraivacuentausada-new" . $i . "-gnd_motivo').val('{$montoIva}');");
        }

        $js = <<<JS
getIvas().forEach(function(e, i) {
    // console.log('se esta haciendo onchange iva montos');
    generarEventoChangeCampoIva("compraivacuentausada-new" + $i + "-ivas-iva_" + e);
});
JS;
        $this->registerJs($js);

        $i++;
    }

    // new parcel fields
    echo '<tr id="iva-cta-usada-new-block" style="display: none;">';
    echo $this->render('_form-iva-cuenta-usada', [
        'key' => '__id__',
        'form' => $form,
        'ivaCtaUsada' => $ivaCtaUsada,
        'porcentajes_iva' => $porcentajes_iva,
        'es_nota' => $es_nota
    ]);
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';

    $_s = <<<JS
if ("$es_nota".length) {
    var esNota = "$es_nota";
} else {
    const esNota = 0;
}
JS;
    $this->registerJs($_s, $this::POS_HEAD);

    $url_campos_totales = Json::htmlEncode(\Yii::t('app', Url::to(['compra/enable-campos-totales'])));
    $url_getPlSeguroADevengarId = Json::htmlEncode(\Yii::t('app', Url::to(['plantilla-compraventa/get-plantilla-seguro-a-devengar-id'])));
    $url_get_plantilla = Json::htmlEncode(\Yii::t('app', Url::to(['plantilla-compraventa/get-plantilla'])));
    $js_script = <<<JS
        $(document).ready(function () {
            completarTotales();
        });

        function generarInputNumber(elemento, reload = false) {
            let digitos = esNota ? digitosMonedaParaNota : ($('#compra-moneda_id').select2('data')[0]['tiene_decimales'] === 'si' ? 2 : 0);
            elemento.addClass('form-control agregarNumber ' + elemento.attr('clave'));
            let disp = reload ? '' : '-disp';
            if (!elemento.attr('id').includes("-disp")) {
                elemento.prop('id', elemento.attr('id') + '-disp');
            }
            // elemento.inputmask({
            //     "alias": "numeric",
            //     "digits": moneda_field.select2('data')[0]['tiene_decimales'] === 'si' ? 2 : 0,
            //     "groupSeparator": ".",
            //     "autoGroup": true,
            //     "autoUnmask": true,
            //     "unmaskAsNumber": true,
            //     "radixPoint": ","
            // });
            
            elemento.inputmask({
                alias: "numeric",
                digits: digitos,
                prefix: "",
                groupSeparator: ".",
                autoGroup: true,
                autoUnmask: true,
                unmaskAsNumber: true,
                radixPoint: ",",
                digitsOptional: false,
                placeholder: "0",
                rightAlign: true
            }); 
        }

        function getImpuestosCampoNombres() {
            const prefijo = "totales_iva-impuesto-";
            var porcentajes = $porcentajes_iva_json;
            var arr = Object.values(porcentajes);
            var i, nombres = [];
            for (i = 0; i < arr.length; i++) {
                nombres.push(prefijo.concat(arr[i]));
            }
            return nombres;
        }

        function getTotalCampoNombres(prefijo) {
            var porcentajes = $porcentajes_iva_json;
            var arr = Object.values(porcentajes);
            var i, nombres = [];
            for (i = 0; i < arr.length; i++) {
                nombres.push(prefijo.concat(arr[i]));
            }
            return nombres;
        }

        function generarEventoChangePlantilla(campo_id, prefijo) {// console.log('generarEventoChangePlantilla()');
            // console.log($('#' + campo_id));
            $('#' + campo_id).on('change', function () {
                if ($(this).select2('data').length) {
                    totalesIvaCamposShowHide($('#' + campo_id).val(), prefijo);
    
                    let pl_selector_id = $(this).attr('id');
                    
                    // Mostrar botón para seguros/pólizas si la plantilla es de seguros a vencer.
                    let button_id = pl_selector_id.replace(/plantilla_id/g, "poliza_manager_button");
                    // TODO: PARAMETRIZAR ID DE PLANTILLA PARA POLIZAS EN EL MÓDULO DEL SISTEMA.
                    if ($(this).select2('data')[0]['para_seguro_a_devengar'] === 'si') {
                        $('#' + button_id).prop('style', "display: yes");
                    } else  {
                        $('#' + button_id).prop('style', "display: none");
                    }
                    
                    // Mostrar botón para activo fijo.
                    button_id = pl_selector_id.replace(/plantilla_id/g, "actfijo_manager_button");
                    if ($(this).select2('data')[0]['activo_fijo'] === 'si') {
                        $('#' + button_id).prop('style', "display: yes");
                    } else  {
                        $('#' + button_id).prop('style', "display: none");
                    }
                    
                    // Mostrar botón para cuota de prestamo
                    button_id = pl_selector_id.replace(/plantilla_id/g, "cuota_prestamo_manager_button");
                    if ($(this).select2('data')[0]['para_cuota_prestamo'] === 'si') {
                        $('#' + button_id).prop('style', "display: yes");
                    } else  {
                        $('#' + button_id).prop('style', "display: none");
                    }
                    
                    // Mostrar botón para prestamo
                    button_id = pl_selector_id.replace(/plantilla_id/g, "prestamo_manager_button");
                    if ($(this).select2('data')[0]['para_prestamo'] === 'si') {
                        $('#' + button_id).prop('style', "display: yes");
                    } else  {
                        $('#' + button_id).prop('style', "display: none");
                    }
                    
                    // Mostrar botón para stock de act bio
                    button_id = pl_selector_id.replace(/plantilla_id/g, "actbio_compra_manager_btn");
                    if ($(this).select2('data')[0]['activo_biologico'] === 'si') {
                        $('#' + button_id).prop('style', "display: yes");
                    } else  {
                        $('#' + button_id).prop('style', "display: none");
                    }
                    
                    setTotalFactura();
                }
            })
        }

        function completarTotales() {
            let moneda_field = $('#compra-moneda_id');
            let ctotales = getTotalFieldNames(), cimpuestos = getImpuestosFieldNames(), cgravadas = getGravadasFieldNames(), ivas = getIvas();
            for (let j = 0; j <= ivas.length - 1; j++) {
                let _total = 0.0;
                $('input.ivaMonto' + ivas[j]).each(function () {
                    if (!isNaN($(this).val()) && $(this).val() !== '') {
                        const regex2 = /compraivacuentausada-new([0-9]+)-/gm;
                        let m2 = regex2.exec($(this).attr('id'));
                        let paraDescuento = $("#" + m2[0] + "plantilla_id").select2('data')[0]['para_descuento'];
                        if (paraDescuento === 'no')
                            _total += parseFloat($(this).val());
                        else
                            _total -= parseFloat($(this).val());
                    }
                });
                $('#totales_iva-iva-' + ivas[j] + '-disp').val(_total).trigger('change');
            }

            let totales_iva = 0.0;
            let totales_grv = 0.0;
            for (var i = 0; i < ctotales.length; i++) {
                let iva = ivas[i];
                if (iva !== '0') {
                    let ctotal = document.getElementById(ctotales[i] + '-disp');
                    let cimps = document.getElementById(cimpuestos[i] + '-disp');
                    let cgravs = document.getElementById(cgravadas[i] + '-disp');

                    if (ctotal !== null && cimps !== null) {
                        cimps.value = ctotal.value - (ctotal.value / (1.0 + iva / 100.0));
                        cgravs.value = ctotal.value -  cimps.value;
                        
                        // Mostrar 
                        if (moneda_field.val() === '1') {
                            cimps.value = Math.round(cimps.value);
                            cgravs.value = Math.round(cgravs.value);
                            ctotal.value = Math.round(ctotal.value);
                        }
                        totales_iva += cimps.value;
                        totales_grv += cgravs.value;
                    }
                }
            }

            if (moneda_field.val() === '1') {
                totales_iva = Math.round(totales_iva);
            }
            $('#compra-_total_ivas-disp').val(totales_iva);
            $('#compra-_total_gravadas-disp').val(totales_grv);
        }

        function generarEventoChangeCampoIva(campo_id) {
            $('#' + campo_id + '-disp').on('change', function () {
                completarTotales();
                setTotalFactura();
            })
        }

        function totalesIvaCamposShowHide(plantilla_id, prefijo) {
            
            $.ajax({
                url: $url_campos_totales,
                type: 'post',
                data: {
                    plantilla_id: plantilla_id,
                    prefijo_campo: prefijo
                },
                success: function (data) {
                    
                    // Habilitar boton para gnd_motivo.
                    if ($('#' + prefijo.replace('ivas-iva_', '') + 'plantilla_id').select2('data')[0]['deducible'] === 'no') {
                        $('#' + prefijo.replace('ivas-iva_', '') + 'gnd_motivo').attr('readonly', false);
                        $('#' + prefijo.replace('ivas-iva_', '') + 'gnd_motivo').attr('disabled', false);
                    } else {
                        $('#' + prefijo.replace('ivas-iva_', '') + 'gnd_motivo').attr('readonly', true);
                        $('#' + prefijo.replace('ivas-iva_', '') + 'gnd_motivo').attr('disabled', true);
                        $('#' + prefijo.replace('ivas-iva_', '') + 'gnd_motivo').val('');
                    }
                    
                    var i;
                    campos_i = getImpuestosCampoNombres();
                    var campos = getTotalCampoNombres(prefijo);
                    for (i = 0; i < campos.length; i++) {
                        campos[i] = '#' + campos[i] + '-disp';
                    }

                    for (i = 0; i < campos.length; i++) {
                        let plantilla_selector_id = prefijo.replace('ivas-iva_', 'plantilla_id');
                        let para_prestamo = $('#' + plantilla_selector_id).select2('data')[0]['para_prestamo'];
                        let para_cuota_pr = $('#' + plantilla_selector_id).select2('data')[0]['para_cuota_prestamo'];
                        
                        if (para_prestamo === 'no') {
                            if (para_cuota_pr === 'no') {
                                if (data.includes(campos[i])) {
                                    $(campos[i]).prop('disabled', false);
                                } else {
                                    // TODO: Ver si hacer trigger('change'), pero si es update no deberia.
                                    $(campos[i]).val('');
                                    $(campos[i]).prop('disabled', true);
                                }
                            } else {
                                $(campos[i]).prop('disabled', false);
                            }
                        }
                        else if (para_prestamo === 'si') {
                            $(campos[i]).prop('disabled', false);
                        }
                    }
                    
                    // Deshabilitar ivas si el valor de `solo_exento` del tidoc es 'si'
                    if ($("#compra-tipo_documento_id").select2('data')[0]['solo_exento'] === 'si') {
                        $("input[id*='" + prefijo +"']").each(function(i, e) {
                            const regex = /compraivacuentausada-new([0-9]+)-ivas-iva_([0-9]+)/gm;
                            let m2 = regex.exec($(this).attr('id'));
                            //console.log(m2);
                            if (m2[2] !== '0') {
                                $(e).prop('disabled', true);
                                $(e).val('').trigger('change');
                            }
                        });
                    }

                    completarTotales();
                }
            });
        }

        function reloadAllNumberField(reload = false) {
            $('input.agregarNumber').each(function () {
                generarInputNumber($(this), reload);
            });
        }

        // add parcel button
        var iva_cta_usada_k = $i - 1;
        var plantilla_data = $plantilla_data;
        
        function eventForClick(open = false) {
            iva_cta_usada_k += 1;
            $('#iva-cta-usadas').find('tbody')
                .append('<tr>' + $('#iva-cta-usada-new-block').html().replace(/__id__/g, 'new' + iva_cta_usada_k) + '</tr>');

            let new_plantilla_field = $('#compraivacuentausada-new' + iva_cta_usada_k + '-plantilla_id');
            
            let para_importacion = $('#compra-tipo_documento_id').select2('data')[0]['para_importacion'] === 'si';
            let alert_message = para_importacion ? 'No hay plantillas configuradas para importación.' : 'No hay plantillas no deducibles.';
            
            $.ajax({
                url: $url_get_plantilla,
                type: 'get',
                data: {
                    tipo: 'compra',
                    noDeducible: $('#compra-timbrado_vencido').val() == "1" ? "si" : "no",
                    paraImportacion: $('#compra-tipo_documento_id').select2('data')[0]['para_importacion'],
                    obligacion_id: $('#compra-obligacion_id').val()
                },
                success: function (data) {
                    new_plantilla_field.select2({
                        theme: 'krajee',
                        placeholder: '',
                        language: 'en',
                        width: '100%',
                        data: data
                    });

                    // Obtener facturas por ruc
                    // if (data.length === 0) {
                    //     krajeeDialog.alert(alert_message);
                    //
                    //     // $('#Detalles_new' + k + '_delete_button').click();
                    //
                    //     return false;
                    // }
                   
                    generarEventoChangePlantilla('compraivacuentausada-new' + iva_cta_usada_k + '-plantilla_id', 'compraivacuentausada-new' + iva_cta_usada_k + '-ivas-iva_');

                    reloadAllNumberField();
        
                    var campos = getTotalCampoNombres('compraivacuentausada-new' + iva_cta_usada_k + '-ivas-iva_');
                    for (i = 0; i < campos.length; i++) {
                        $('#' + campos[i] + '-disp').prop('disabled', true);
                        generarEventoChangeCampoIva(campos[i]);
                    }
        
                    if (open) {
                        new_plantilla_field.focus().select2('open');
                    }
                    return iva_cta_usada_k;
                }
            });
        }

        $('#iva-cta-usada-new-button').on('click', function () {
            eventForClick(true);
        });

        // remove parcel button
        $(document).on('click', '.iva-cta-usada-remove-button', function () {
            $(this).closest('tbody tr').remove();
            
            // TODO: Borrar de la session, la poliza guardada
            // TODO: Borrar de la session, el activo fijo correspondiente al id de la plantilla de esta fila.
            completarTotales();
            console.log('desde fieldset linea 421');
            setTotalFactura();
            //reload();
        });

JS;
    // OPTIONAL: click add when the form first loads to display the first new row
    // Poner la plantilla usada en la ultima factura creada, solo si es create y en la sesion existe.
    // se junta porque eventoForClick() es solo para compra y cuando es create (o model->newRecordd)
    if (!$es_nota && !Yii::$app->request->isPost && $model->isNewRecord) {
        $_temp_p_id = '';
        if (Yii::$app->session->has('cont_compra_valores_usados')) {
            $valoresUsados = Yii::$app->session->get('cont_compra_valores_usados');
            $_temp_p_id = $valoresUsados['plantilla_id'];
        }
        $js_script .= <<<JS
eventForClick();
$('input[id$="plantilla_id"]:not([id*="compraivacuentausada-__id__-plantilla_id"])').each(function(i, e) {
    if($(e).length > 0)
        $(e).val('$_temp_p_id').trigger('change');
});
JS;
    }

    //    // Poner la plantilla usada en la ultima factura creada, solo si es create y en la sesion existe.
    //    if (Yii::$app->controller->action->id == 'create' && Yii::$app->session->has('cont_compra_valores_usados')) {
    //        $valoresUsados = Yii::$app->session->get('cont_compra_valores_usados');
    //        $_temp_p_id = $valoresUsados['plantilla_id'];
    //        $js_script .= <<<JS
    //$('#compraivacuentausada-new' + iva_cta_usada_k + '-plantilla_id').val($_temp_p_id).trigger('change');
    //JS;
    //    }
    $this->registerJs($js_script);
    ?>
</fieldset>