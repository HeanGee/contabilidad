<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 06/03/19
 * Time: 03:45 PM
 */

/* @var $this \yii\web\View */
?>

<?php

\yii\widgets\Pjax::begin(['id' => 'cuentas-pjax']);
echo Yii::$app->session->get('cuentas');
\yii\widgets\Pjax::end();

$url = \yii\helpers\Json::encode(\yii\helpers\Url::to(['calculate-counts']));
$script = <<<JS
$(document).ready(function() {
    setInterval(function() {
        $.ajax({
            type: 'get',
            url: $url,
            success: function(results) {
                $.pjax.reload({container: "#cuentas-pjax", async: true});
            }
        });
}, 5000);
        
});
JS;
$this->registerJs($script);