<?php

use backend\modules\contabilidad\models\ReciboPrestamoDetalle;
use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ReciboPrestamo */

$this->title = "Recibo #{$model->numero}";
$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="recibo-prestamo-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-delete'),
            'bloquear' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-bloquear'),
            'desbloquear' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-desbloquear'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Recibos', ['index'/*, 'operacion' => Yii::$app->request->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] && $model->bloqueado == 'no' ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] && $model->bloqueado == 'no' ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Está seguro?',
                'method' => 'post',
            ],
        ]) : null ?>
        <?= "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" ?>
        <?= $permisos['bloquear'] && $model->bloqueado == 'no' ? Html::a('<span>Bloquear</span>', ['bloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'title' => Yii::t('app', 'Bloquear'),
                'data-confirm' => 'Está seguro que desea bloquear el recibo?'
            ]) : '' ?>
        <?= $permisos['bloquear'] && $model->bloqueado == 'si' ? Html::a('<span>Desbloquear</span>', ['desbloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-success',
                'title' => Yii::t('app', 'Desbloquear'),
                'data-confirm' => 'Está seguro que desea desbloquear el recibo?'
            ]) : '' ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Recibo',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'numero',
                ['label' => 'Total', 'attribute' => 'formattedTotal'],
                ['label' => 'Fecha', 'attribute' => 'formattedFecha'],
                [
                    'attribute' => 'entidad_id',
                    'value' => $model->entidad->razon_social
                    // credit: https://www.yiiframework.com/wiki/706/relational-data-fields-display-and-edit-with-the-yii2-detail-view-extension
                ],
            ],
        ]);

        echo $this->render('view-details/visor-recibo-detalles', ['model' => $model]);

        echo $this->render('../auditoria/_auditoria', ['modelo' => $model]);
        echo $this->render('../auditoria/_auditoria_multiple_models', ['modelo' => new ReciboPrestamoDetalle(), 'ids' => $model->getDetallesId()]);

    } catch (Exception $exception) {
        print $exception->getMessage();
    } ?>

</div>
