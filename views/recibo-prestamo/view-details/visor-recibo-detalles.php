<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 24/10/18
 * Time: 10:21 AM
 */

use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use kartik\helpers\Html;
use yii\data\ArrayDataProvider;

/* @var $model \backend\modules\contabilidad\models\ReciboPrestamo */

?>

<?php
$columns = [
    [
        'class' => SerialColumn::className(),
    ],

    [
        'label' => 'Nº Crédito Préstamo',
        'format' => 'raw',
        'value' => function ($model) {
            $url = "index.php?r=contabilidad/prestamo/view&id=" . $model->prestamoDetalle->prestamo_id;
            return Html::a($model->prestamoDetalle->prestamo_id, $url, ['title' => "Ver datos del Prestamo Nº {$model->prestamoDetalle->prestamo_id}"]);
        }
    ],
    [
        'label' => 'ID Cuota',
        'value' => 'prestamo_detalle_id',
    ],
    [
        'label' => 'Nº Cuota',
        'value' => 'prestamoDetalle.nro_cuota',
    ],
    [
        'label' => 'Capital',
        'value' => 'capitalFormatted',
        'contentOptions' => ['style' => 'text-align:right'],
    ],
    [
        'label' => 'Interés',
        'value' => 'interesFormatted',
        'contentOptions' => ['style' => 'text-align:right'],
    ],
    [
        'label' => 'Cuota',
        'value' => 'cuotaFormatted',
        'contentOptions' => ['style' => 'text-align:right'],
    ],
];

$template = '{view}';
$buttons = [];

$dataProvider = new ArrayDataProvider([
    'allModels' => $model->reciboPrestamoDetalle,
    'pagination' => false,
]);

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'hover' => true,
        'id' => 'grid',
        'panel' => ['type' => 'info', 'heading' => 'Cuotas', 'footer' => false,],
        'toolbar' => [
        ]
    ]);

} catch (Exception $exception) {
    print $exception->getMessage();
}

