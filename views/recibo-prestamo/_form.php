<?php

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\dialog\Dialog;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ReciboPrestamo */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \backend\modules\contabilidad\models\ReciboPrestamo */
?>

<div class="recibo-prestamo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    try {
        echo "<br/><legend class='text-info'><small>Datos del recibo</small></legend>";
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'fecha' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '1'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'name' => 'fecha',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ],
                            'label' => "Fecha",
                        ],
                        'numero' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => \kartik\number\NumberControl::className(),
                            'options' => [
                                'maskedInputOptions' => [
                                    'groupSeparator' => '',
                                    'radixPoint' => '',
                                    'allowMinus' => false,
                                ],
                            ],
                        ],
                        'ruc' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '2'],
                        ],
                        'razon_social' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '4'],
                            'options' => [
                                'readonly' => true,
                            ],
                        ],
                        'total' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::class,
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                    'readonly' => true,
                                ],
                            ],
                            'label' => "Monto de la Cuota"
                        ]
                    ],
                ],
//                [
//                    'autoGenerateColumns' => false,
//                    'columns' => 12,
//                    'attributes' => [
//                        'prestamo_id_selector' => [
//                            'type' => Form::INPUT_WIDGET,
//                            'columnOptions' => ['colspan' => '2'],
//                            'widgetClass' => Select2::className(),
//                            'options' => [
//                                'options' => ['placeholder' => 'Seleccione un prestamo ...'],
//                                'initValueText' => $model->prestamo_id_selector != null ?
//                                    'Nro Crédito ' . $model->prestamo_id_selector : '',
//                                'pluginOptions' => [
//                                    'allowClear' => false,
//                                ]
//                            ],
//                        ],
//                        'total' => [
//                            'type' => Form::INPUT_WIDGET,
//                            'columnOptions' => ['colspan' => '2'],
//                            'widgetClass' => MaskedInput::class,
//                            'options' => [
//                                'clientOptions' => [
//                                    'rightAlign' => false,
//                                    'alias' => 'decimal',
//                                    'groupSeparator' => '.',
//                                    'radixPoint' => ',',
//                                    'autoGroup' => true
//                                ],
//                                'options' => [
//                                    'class' => 'form-control',
//                                    'readonly' => true,
//                                ],
//                            ],
//                        ]
//                    ],
//                ],
            ]
        ]);

//        \yii\widgets\Pjax::begin(['id' => 'cuotas_grid']);
//        echo $this->render('prestamo-detalles-eliminar-proxmnte-tirar/_form_detalle', ['model' => $model, 'form' => $form,]);
//        \yii\widgets\Pjax::end();
        $boton = /*Html::beginTag('fieldset', ['id' => 'w3']) .
            Html::beginTag('div', ['class' => 'row']) .*/
            Html::beginTag('div', ['class' => 'col-sm-1']) .
            Html::beginTag('div', ['class' => 'form-group highlight-addon field-prestamo-btn']) .
            Html::tag('label', 'Prestamos') .
            Html::button('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-dollar" aria-hidden="true"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', [
                'class' => 'prestamo-manager btn btn-warning modal-cuota-prestamo',
                'title' => 'Préstamos',
                'id' => "btn-manejar-cuota-prestamo",
                'style' => "display: ;",
                'type' => 'button',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['prestamo/manejar-prestamo-desde-recibo']),
                'data-pjax' => '0',
            ]) .
            Html::tag('div', '', ['class' => 'help-block']) .
            Html::endTag('div') .
            Html::endTag('div')/* .
            Html::endTag('div') .
            Html::endTag('fieldset')*/
        ; ?>

        <div class="form-group"><br/>
            <?php
            $btn_text = Yii::$app->controller->action->id == 'create' ? 'Crear' : 'Guardar';
            $btn_class = $btn_text == 'Crear' ? 'btn btn-success' : 'btn btn-primary';
            echo Html::submitButton($btn_text, ['class' => $btn_class,
                "data" => (Yii::$app->controller->action->id == 'update') ? [
                    'confirm' => 'Desea guardar los cambios?',
                    'method' => 'post',
                ] : []
            ]) ?>
        </div>

        <legend class="text-info">
            <small>Asiento muestra</small>
        </legend>
        <?php echo Html::button('SIMULAR ASIENTO', [
            'style' => "display: none;",
            'class' => 'btn btn-primary btn-simular-asiento',
        ]);
        echo $this->render('asiento-simulado/_form_asientosim_cuota_prestamo', []);

        echo Dialog::widget();

    } catch (Exception $exception) {
        print $exception->getMessage();
    }
    ?>

    <?php ActiveForm::end(); ?>

    <?php
    $action = Yii::$app->controller->action->id;
    $controller = Yii::$app->controller->id;
    $url_getRazonSocial = Json::htmlEncode(\Yii::t('app', Url::to(['entidad/get-column-value-by', 'column' => 'razon_social'])));
    $url_getPrestamosLibres = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/get-prestamos-libres-para-recibo-ajax'])));
    $url_showCuotas = Json::htmlEncode(\Yii::t('app', Url::to(['recibo-prestamo/show-cuotas'])));

    $pre_scripts = <<<JS
//var prestamo_selected = "$model->prestamo_id_selector";
//var recibo_id = "$model->id";
//
//function applyDateInput() {
//    $(":input[data-inputmask-alias]").inputmask();
//}
//
//function removeNumberInput() {
//    $(":input.monto").inputmask('remove');
//
//    $(":input.nro-cuota").inputmask('remove');
//}
//
//function applyNumberInput() {
//    $(":input.monto").inputmask({
//        "alias": "numeric",
//        "digits": 2,
//        "groupSeparator": ".",
//        "autoGroup": true,
//        "autoUnmask": true,
//        "unmaskAsNumber": true,
//        "radixPoint": ",",
//        "digitsOptional": false,
//        "placeholder": "0",
//        "rightAlign": true
//    });
//
//    $(":input.nro-cuota").inputmask({
//        "alias": "numeric",
//        "digits": 0,
//        "groupSeparator": ".",
//        "autoGroup": true,
//        "autoUnmask": true,
//        "unmaskAsNumber": true,
//        "radixPoint": ",",
//        "digitsOptional": false,
//        "placeholder": "0",
//        "rightAlign": false
//    });
//
//    // $(":input.monto").number( true, 2, ',', '.' );
//}
//
//function removeCheckBoxExtraLabels() {
//    $(".selected").not(document.getElementById('PrestamoDetalle___new___selected')).each(function () {
//
//        $('label[for=' + $(this).attr('id') + ']').remove();
//    });
//}
//
// function removeCuotas() {
//     let ruc = $('#reciboprestamo-ruc');
//     let sel = $('#reciboprestmao-prestamo_id_selector');
//     if (ruc.val() === "" || sel.val() === "" || sel.length === 0) {
//         $('tr.fila-cuotas').closest('tbody tr').remove();
//     }
// }

// function toDeutschFormat(n) {
//     if (n === "") return 0;
//     return (n.replace(/\./, '')).replace(',', '.');
// }

// function updateTotalRecibo() {
//     let field_total = $('#reciboprestamo-total');
//     let total = 0.0;
//    
//     $('input[id$="monto_cuota_saldo"]').each(function() {
//    
//         let id = ($(this).attr('id')).replace('monto_cuota_saldo', 'selected');
//         let checkbx = document.getElementById(id);
//         let field_mcsaldo = $(this);
//        
//         if (checkbx.checked) {
//             total += field_mcsaldo.val();
//         }
//     });
//
//     field_total.val(total).trigger('change');
// }

// function onchange_monto_cuota_saldo() {
//     $('input[id$="monto_cuota_saldo"]').change(function() {
//         let field_total = $('#reciboprestamo-total');
//         let total = 0.0;
//        
//         $('input[id$="monto_cuota_saldo"]').each(function() {
//        
//             let id = ($(this).attr('id')).replace('monto_cuota_saldo', 'selected');
//             let checkbx = document.getElementById(id);
//             let field_mcsaldo = $(this);
//            
//             if (checkbx.checked) {
//                 total += field_mcsaldo.val();
//             }
//         });
//    
//         field_total.val(total).trigger('change');
//     });
// }
JS;
    $this->registerJs($pre_scripts, \yii\web\View::POS_HEAD);

    $url_getPrestamosLibres = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/get-prestamos-libres-para-recibo-ajax'])));
    $url_manejarDesdeRecibo = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/manejar-cuota-prestamo-desde-recibo'])));
    $url_simularAsiento = Json::htmlEncode(\Yii::t('app', Url::to(['simular-asiento'])));
    $scripts = <<<JS
$(document).on('click', '.modal-cuota-prestamo', (function () {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    let ruc = $('#reciboprestamo-ruc').val();
    
    $.ajax({
        url: $url_getPrestamosLibres + "&recibo_id={$model->id}&ruc=" + ruc + "&json_format=" + true,
        type: 'get',
        data: {},
        success: function(data) {
            if (data['results'].length > 0) {
                $.get(
                    $url_manejarDesdeRecibo + '&recibo_id=' + "$model->id" + "&ruc=" + ruc + '&primera_vez=' + true,
                    {},
                    function (data) {
                        if (data !== "") {
                            let modal = $(boton.data('target'));
                            $('.modal-body', modal).html(data);
                            modal.modal();
                            $('.modal-header', modal).css('background', '#3c8dbc');
                            // if (title)
                            $('.modal-title', modal).html(title);
                        } else {
                            $.pjax.reload({container: "#flash_message_id", async: false});
                            $("#modal").modal("hide");
                            $("modal-body").html("");
                        }
                    }
                );                
            } else {
                $("#modal").modal("hide");
                $("modal-body").html("");
                krajeeDialog.alert('No se pudo encontrar ningún préstamo. Verifique si ha especificado el R.U.C. en el formulario de recibo, que haya registrado previamente un prestamo y que éste tenga generado las cuotas.');
            }
        }
    });
    
}));

$(document).on('change', '#reciboprestamo-ruc', function() {
    // let select2 = $('#reciboprestamo-prestamo_id_selector');
    // let selected = prestamo_selected === "" ? select2.val() : prestamo_selected;
    //
    // select2.select2("destroy");
    // select2.html("<option><option>");
    // select2.select2({
    //     theme: 'krajee',
    //     placeholder: '',
    //     language: 'en',
    //     width: '100%',
    //     data: [],
    // });

    $.ajax({
        type: 'get',
        url: $url_getRazonSocial + '&attribute=ruc&value=' + $('#reciboprestamo-ruc').val(),
        data: {},
        success: function(data) {
            // mostrar razon social
            $('#reciboprestamo-razon_social').val(data).trigger('change');
            
//            // rellenar select2
//            $.ajax({
//                type: 'get',
//                url: $url_getPrestamosLibres + "&recibo_id=" + recibo_id + "&ruc=" + $('#reciboprestamo-ruc').val(),
//                data: {},
//                success: function(data) {
//                    let arr = [{id: "", text: ''}]; // es el item necesario para que no se seleccione el primero por defecto.
//                    
//                    if (data['results'].length === 0) {
//                        $.pjax.reload({container: "#flash_message_id", async: false});
//                        $('tr.fila-cuotas').closest('tbody tr').remove();
//                    }
//                    else {
//                        // Construir options para select2
//                        data['results'].forEach(function (e, i) {
//                            arr.push(e);
//                        });
//        
//                        // Rellenar select2 con las opciones
//                        select2.select2({
//                            theme: 'krajee',
//                            placeholder: '',
//                            language: 'en',
//                            width: '100%',
//                            data: arr,
//                        });
//        
//                        if (selected !== "")
//                            select2.val(selected).trigger('change');
//                    }
//                }
//            });
        }
    });
});

//$(document).on('change', '#reciboprestamo-prestamo_id_selector', function() {
//    $.ajax({
//        type: 'get',
//        url: $url_showCuotas,
//        data: {
//            prestamo_id: $('#reciboprestamo-prestamo_id_selector').val(),
//        },
//        success: function(data) {
//            if (data.error) {
//                krajeeDialog.alert(data.result);
//            }
//            $.pjax.reload({container: "#cuotas_grid", async: false});
//            applyDateInput();
//            applyNumberInput();
//            removeCheckBoxExtraLabels();
//            updateTotalRecibo();
//            onchange_monto_cuota_saldo();
//        }
//    });
//});
//
// $(document).on('change', '.selected', function() {
//     updateTotalRecibo();
// });
//
// $(document).on('click', '#btn-recalcular-todo', function() {
//     $('#reciboprestamo-prestamo_id_selector').trigger('change');
// });

function simularAsiento()
{
    $.ajax({
        url: $url_simularAsiento,
        type: 'get',
        success: function(result) {
            $.pjax.reload({container: "#asiento-simulator-cuota-prestamo", async: false});
            if (result.errors) {
                krajeeDialog.alert(result.errors);
            }
        }
    });
}

$(document).on('click', '.btn-simular-asiento', function() {
    simularAsiento();
});

$(document).ready(function() {
    $('#reciboprestamo-ruc').trigger('change'); // para que carque los prestamos en select2.
    // removeCuotas();
    // applyDateInput();
    // applyNumberInput();
    
    let parent = $('#reciboprestamo-total').parent().parent()/*.parent().parent()*/[0];
    $('$boton').insertAfter(parent);
    
    if ("$action" === 'update' && "$controller" === 'recibo-prestamo') {
        simularAsiento();
    }
});
JS;
    $this->registerJs($scripts);
    ?>
</div>