<?php

use backend\modules\contabilidad\models\ReciboPrestamo;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ReciboPrestamoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recibos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-prestamo-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-create') ?
            Html::a('Registrar Nuevo Recibo', ['create'], ['class' => 'btn btn-success']) : null ?>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-prestamo-index') ?
            Html::a('Ir a Prestamos', ['prestamo/index'], ['class' => 'btn btn-info']) : null ?>
    </p>

    <?php try {
        $template = "";

        foreach (['view', 'update', 'delete', 'bloquear', 'desbloquear'] as $_v)
            if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-recibo-prestamo-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'numero',
                ],

                [
                    'attribute' => 'fecha',
                    'value' => 'formattedFecha'
                ],

                [
                    'label' => "Razón Social",
                    'attribute' => 'razon_social_s',
                    'value' => 'entidad.razon_social'
                ],

                [
                    'attribute' => 'total',
                    'value' => 'formattedTotal',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        /** @var ReciboPrestamo $model */
                        'update' => function ($url, $model) {
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Editar'),
                            ]) : '';
                        },
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('yii', 'Ver'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            /** @var ReciboPrestamo $model */
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => '',
                                'title' => Yii::t('yii', 'Borrar'),
                                'data' => [
                                    'confirm' => '¿Está seguro de eliminar este elemento?',
                                    'method' => 'post',
                                ],
                            ]) : '';
                        },
                        'bloquear' => function ($url, $model, $key) {
                            /** @var ReciboPrestamo $model */
                            $url = ($model->bloqueado == 'no') ?
                                Html::a('<span>Bloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => Yii::t('app', 'Bloquear'),
                                        'data-confirm' => 'Está seguro que desea bloquear el recibo?'
                                    ]) : '';
                            return $url;
                        },
                        'desbloquear' => function ($url, $model, $key) {
                            /** @var ReciboPrestamo $model */
                            $url = ($model->bloqueado == 'si') ?
                                Html::a('<span>Desbloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-success btn-xs',
                                        'title' => Yii::t('app', 'Desbloquear'),
                                        'data-confirm' => 'Está seguro que desea desbloquear el recibo?'
                                    ]) : '';
                            return $url;
                        },
                    ],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>
</div>
