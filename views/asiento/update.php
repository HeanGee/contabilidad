<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Asiento */
/* @var $detalles backend\modules\contabilidad\models\AsientoDetalle[] */

$this->title = 'Modificar Asiento';
$this->params['breadcrumbs'][] = ['label' => 'Asientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="asiento-update">

    <?= $this->render('_form', [
        'model' => $model,
        'detalles' => $detalles
    ]) ?>

</div>
