<?php

use backend\models\Moneda;
use backend\modules\contabilidad\models\AsientoDetalle;
use kartik\detail\DetailView;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Asiento */

$this->title = 'Detalle';
$this->params['breadcrumbs'][] = ['label' => 'Asientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asiento-view">
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Asiento',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'attribute' => 'empresa_id',
                    'value' => ($model->empresa_id) ? $model->empresa->nombre : ''
                ],
                [
                    'attribute' => 'periodo_contable_id',
                    'value' => ($model->periodo_contable_id) ? $model->periodoContable->anho : ""
                ],
                [
                    'attribute' => 'fecha',
                    'value' => date_create_from_format('Y-m-d', $model->fecha)->format('d-m-Y')
                ],
                [
                    'attribute' => 'monto_debe',
                    'value' => $model->montoDebeFormatted,
                    'label' => 'Monto'
                ],
                [
                    'attribute' => 'descripcion',
                    'value' => $model->concepto
                ]
            ]
        ]);

        $dataProvider = new ArrayDataProvider([  # al usar query con union y hacer order by en cada parte, no funciona.
            'allModels' => array_merge(
                AsientoDetalle::find()
                    ->alias('detalle')
                    ->leftJoin('cont_plan_cuenta cta', 'cta.id = detalle.cuenta_id')
                    ->where(['detalle.asiento_id' => $model->id])
                    ->andFilterWhere([">", 'detalle.monto_debe', 0])
                    ->orderBy(['cta.cod_ordenable' => SORT_ASC])
                    ->all(),
                AsientoDetalle::find()
                    ->alias('detalle')
                    ->leftJoin('cont_plan_cuenta cta', 'cta.id = detalle.cuenta_id')
                    ->where(['detalle.asiento_id' => $model->id])
                    ->andFilterWhere([">", 'detalle.monto_haber', 0])
                    ->orderBy(['cta.cod_ordenable' => SORT_ASC])->all()
            ),
            'pagination' => false
        ]);

        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => 'Detalles',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
            'columns' => [
                [
                    'class' => DataColumn::class,
                    'attribute' => 'asiento_id',
                    'value' => 'cuenta.cod_completo',
                ],
                [
                    'class' => DataColumn::class,
                    'attribute' => 'cuenta_id',
                    'value' => 'cuenta.nombre',
                    'pageSummaryOptions' => [
                        'data-colspan-dir' => 'ltr',
                        'colspan' => 1,
                        'style' => 'text-align: right;',
                    ],
                    'pageSummary' => function ($summary, $data, $widget) {
                        return "Totales";
                    },
                ],
                [
                    'class' => DataColumn::class,
                    'attribute' => 'monto_debe',
                    'format' => 'decimal',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'prepend' => Moneda::findOne(['id' => 1])->simbolo . ' ',
                        'append' => '',
                    ],
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'class' => DataColumn::class,
                    'attribute' => 'monto_haber',
                    'format' => 'decimal',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'prepend' => Moneda::findOne(['id' => 1])->simbolo . ' ',
                        'append' => '',
                    ],
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ]
            ],
        ]);

        echo $this->render('@backend/modules/contabilidad/views/facturas-relacionadas/_facturas_relacionadas', ['model' => $model]);

        echo '<div class="auditoria">';

        echo $this->render('../auditoria/_auditoria', ['modelo' => $model]);
        echo $this->render('../auditoria/_auditoria_multiple_models', ['modelo' => new AsientoDetalle(), 'ids' => $model->getDetallesId()]);
        echo '</div>';

    } catch (\Exception $e) {
        echo $e;
        /* TODO: manejar */
    }

    $script = <<<JS
$('table.kv-grid-table td:nth-child(3)').filter(function () {// el parametro numerico para nth-child() especifica a que columna referenciar.
    return $(this).text() === "0";
}).each(function (i, td) {
    let ctaNombreCell = $(td).closest('td').prev('td');
    ctaNombreCell.prop('align', 'right');
    ctaNombreCell.html('a ' + ctaNombreCell.html()); // concatenar 'a ' como suele hacer beatriz patinho
});
JS;
    $this->registerJs($script);
    ?>
</div>
