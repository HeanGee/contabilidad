<?php

use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Asiento */
/* @var $detalles backend\modules\contabilidad\models\AsientoDetalle[] */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asiento-form">

    <?php
    $anho = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'))->anho;

        $_fecha = date('d-m-Y');
        if(!empty($model->fecha)){
            $_fecha = new DateTime($model->fecha);
            $_fecha = $_fecha->format('d-m-Y');
        }
        $form = ActiveForm::begin(['id' => 'form-asiento-detalle']);
        try{
            $picker_layout = <<< HTML
{remove} {picker} {input}
HTML;
            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'fecha' => [
                                'type' => Form::INPUT_WIDGET,
                                'columnOptions' => ['colspan' => '2'],
                                'widgetClass' => DateControl::class,
                                'label' => 'Fecha',
                                'options' => [
                                    'type' => DateControl::FORMAT_DATE,
                                    'ajaxConversion' => false,
                                    'language' => 'es',
                                    'widgetOptions' => [
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd-MM-yyyy',
                                            'todayHighlight' => true,
                                            'startDate' => "01-01-{$anho}",
                                            'endDate' => "31-12-{$anho}",
                                            'weekStart' => '0',
                                        ],
                                    ],
                                ]
                            ],
                            'concepto' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '10'],
                                'options' => [ 'placeholder ' => 'Concepto...' ]
                            ],
                        ]
                    ]
                ]
            ]);

            $nuevo_btn = Html::a('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', 'javascript:void(0);', [
                'id' => 'nuevo-detalle-btn',
                'class' => 'pull-right btn btn-success btn-xs'
            ]);
            $detalle_modelo = new AsientoDetalle();
            echo Html::beginTag('table', ['id' => 'asiento-detalles', 'class' => 'table table-striped table-hover']);
            echo Html::beginTag('thead');
            echo Html::beginTag('tr');
            echo Html::tag('th', $detalle_modelo->getAttributeLabel('Cuenta'));
            echo Html::tag('th', $detalle_modelo->getAttributeLabel('monto_debe'), ['style' => ['width' => '20%', 'text-align' => 'center']]);
            echo Html::tag('th', $detalle_modelo->getAttributeLabel('monto_haber'), ['style' => ['width' => '20%', 'text-align' => 'center']]);
            echo Html::tag('th', $nuevo_btn, ['style' => ['width' => '40px', 'text-align' => 'center']]);
            echo Html::endTag('tr');
            echo Html::endTag('thead');
            echo Html::beginTag('tbody');

            foreach($detalles as $_key => $_detalle){
                echo Html::beginTag('tr');
                echo $this->render('detalle/_form', [
                    'model' => $_detalle,
                    'index' => $_key,
                    'form' => $form
                ]);
                echo Html::endTag('tr');
            }

            // nuevos detalles
            echo Html::beginTag('tr', ['id' => 'detalle-blueprint', 'style' => ['display' => 'none']]);
            echo $this->render('detalle/_form', [
                'model' => $detalle_modelo,
                'index' => '__id__',
                'form' => $form,
            ]);
            echo Html::endTag('tr');
            echo Html::endTag('tbody');

            // Totales
            echo Html::beginTag('tbody', ['class' => 'kv-page-summary-container']);
            echo Html::beginTag('tr', ['class' => 'kv-page-summary warning']);
            echo Html::tag('td', 'Diferencia: <span>0</span> Gs.', ['id' => 'diferencia-totales', 'style' => ['vertical-align' => 'bottom']]);
            echo Html::beginTag('td');
            echo NumberControl::widget([
                'name' => 'total-monto_debe',
                'id' => 'total-monto_debe',
                'readonly' => true,
                'value' => !empty($model->monto_debe) ? $model->monto_debe : '',
                'maskedInputOptions' => [
                    'prefix' => 'Gs ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 0
                ],
                'displayOptions' => [
                    'class' => 'form-control kv-monospace',
                    'placeholder' => 'Debe...'
                ]
            ]);
            echo Html::endTag('td');
            echo Html::beginTag('td');
            echo NumberControl::widget([
                'name' => 'total-monto_haber',
                'id' => 'total-monto_haber',
                'readonly' => true,
                'value' => !empty($model->monto_haber) ? $model->monto_haber : '',
                'maskedInputOptions' => [
                    'prefix' => 'Gs ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 0
                ],
                'displayOptions' => [
                    'class' => 'form-control kv-monospace',
                    'placeholder' => 'Haber...'
                ]
            ]);
            echo Html::endTag('td');
            echo Html::tag('td', '&nbsp;');
            echo Html::endTag('tr');

            echo Html::endTag('tbody');
            echo Html::endTag('table');

        }catch (\Exception $e){
            print $e;
        }

        $css = <<<CSS
table.table-striped th:first-child { border-radius: 4px 0 0 0; }
table.table-striped th:last-child { border-radius: 0 4px 0 0; }
.table-striped th {
	color: #fff;
	font-weight: bold;
	background-color: #1b1b1b;
	background-repeat: repeat-x;
	vertical-align: middle;
	background-image: linear-gradient(to bottom,#444,#222);
}
#asiento-detalles .form-group { margin-bottom: 0; }
/*#asiento-detalles tr > td { padding-bottom: 0; }*/
.kv-page-summary { font-weight: bold; }
CSS;
        $this->registerCss($css);
        $contador = isset($_key) ? $_key : 0;
        $script = <<<JS
function generarInputNumber(elemento) {
    var padre = elemento.parent('div');
    var oculto = $('<div>').css('display', 'none');
    oculto.append(elemento);
    padre.append(oculto);
    var visible = $('<input>')
        .attr('id', elemento.attr('id') + '-disp')
        .attr('name', elemento.attr('id') + '-disp')
        .attr('type', 'text')
        .css('text-align', 'right')
        .addClass('form-control kv-monospace sumar')
        .val(elemento.val());
    padre.prepend(visible);
    elemento.numberControl({
        "displayId" : elemento.attr('id') + "-disp",
        "maskedInputOptions" : {
            "alias" : "numeric",
            "digits" : 0, 
            "groupSeparator" : ".",
            "autoGroup" : true,
            "autoUnmask" : true,
            "unmaskAsNumber" : true,
            "prefix" : "Gs ",
            "radixPoint" : ","}
    });
}
function number_format(number, decimals, dec_point, thousands_sep){
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
var contador = parseInt({$contador});
var blueprint = $('#detalle-blueprint');
blueprint.find('#asientodetalle-__id__-cuenta_id').select2('destroy');
$('#nuevo-detalle-btn').click(function() {
    contador++;
    blueprint.before('<tr>' + blueprint.html().replace(/__id__/g, contador) + '</tr>');
    $('#asientodetalle-' + contador + '-cuenta_id').select2({
        'theme' : 'krajee',
        'placeholder' : 'Seleccione...',
        'allowClear' : true,
        'width' : $('table th:first').width()  // -16 por los paddings /* TODO: ver el tema del responsive */
    });
    $('#form-asiento-detalle').yiiActiveForm('add', {
        id: 'asientodetalle-' + contador + '-cuenta_id',
        name: 'asientodetalle-' + contador + '-cuenta_id',
        container: '.field-' + 'asientodetalle-' + contador + '-cuenta_id',
        input: '#' + 'asientodetalle-' + contador + '-cuenta_id',
        error: '.help-block',
        validate:  function (attribute, value, messages, deferred, form) {
            yii.validation.required(value, messages, {message: "Cuenta no puede estar vacío."});
        }
    });
    $.each(['monto_debe','monto_haber'], function(i, e) {
        generarInputNumber($('#asientodetalle-' + contador + '-' + e));
    });
});
$(document).on('click', '.eliminar-detalle-btn', function () { /* TODO: alert antes de borrar */
    let fila = $(this).closest('tbody tr');
    fila.find('.sumar').val('');
    var elementos = {
        'debe' : $('input[name$="monto_debe-disp"].sumar').first(),
        'haber' : $('input[name$="monto_haber-disp"].sumar').first()
    };
    $.each(elementos, function(i, e) {
        if($(e).length){
            $(e).trigger('change');
        }else{
            $('#asiento-monto_' + i + '-disp').val('');
        }
    });
    fila.remove();
});
$('input.agregarNumber:visible').each(function() {
    generarInputNumber($(this));
});
var formulario = $('#form-asiento-detalle');
formulario.on('beforeValidate', function (e) {
    $('#form-asiento-detalle').yiiActiveForm('remove', 'asientodetalle-__id__-cuenta_id');
    return true;
});
$(document).on('change keyup', 'input.sumar', function() {
    const regex = /monto_([a-z]+)-disp/gm;
    var m = regex.exec($(this).attr('name'));
    var inputs = $('input[name$="monto_' + m[1] + '-disp"].sumar');
    var monto = 0;
    $.each(inputs, function(i, e) {
        var valor = parseInt($(e).val());
        if(!isNaN(valor))
            monto += valor;
    });
    $('#total-monto_' + m[1] + '-disp').val(monto).trigger('change');
});
$.each(['debe','haber'], function(i, e) {
    generarInputNumber($('#asientodetalle-' + contador + '-' + e));
});
formulario.on('beforeSubmit', function (e) {
    $.each(['debe','haber'], function(i, e) {
        $('#total-monto_' + e + '-disp').trigger('blur');
    });
    return true;
});
$(document).on('keyup',  function(e) {
    if (e.keyCode == 43)
        $('#nuevo-detalle-btn').click();
});
$(document).on('change', '#total-monto_debe, #total-monto_haber', function() {
  let debe =  parseInt($('#total-monto_debe').val());
  let haber = parseInt($('#total-monto_haber').val());
  let diff = Math.abs((!isNaN(debe) ? debe : 0) - (!isNaN(haber) ? haber : 0));
  let elemento = $('#diferencia-totales span');
  elemento.empty().html(number_format(diff, 0, ',', "."));
});

// Abrir modal para agregar detalles al presionar tecla +
$(document).keypress(function (e) {
    if (document.activeElement.tagName !== "INPUT" &&
        (!$("#modal").hasClass('in') && (e.keycode === 43 || e.which === 43))) {
        $('#nuevo-detalle-btn').click();
    }
});
JS;
        $this->registerJs($script);

    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
