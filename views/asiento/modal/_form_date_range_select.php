<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 29/10/18
 * Time: 01:06 PM
 */

use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */

?>

<?php try {

    ActiveFormDisableSubmitButtonsAsset::register($this);

    $form = ActiveForm::begin([
        'id' => 'fecha-range-selector',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']]);


    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'fecha_rango' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\date\DatePicker::className(),
                'options' => [
                    'language' => 'es',
                    'pluginOptions' => [
                        'allowClear' => true,
                        'autoApply' => true,
                        'format' => 'dd-mm-yyyy',
                        'autoclose' => true,
                    ],
                ],
            ],
        ]
    ]);

    echo \yii\helpers\Html::submitButton('Borrar', ['class' => 'btn btn-warning']);

    ActiveForm::end();

    $script = <<<JS
$("form#fecha-range-selector").on("beforeSubmit", function (e) {
    var form = $(this);
    let url = form.attr("action") + "&submit=true";
    
    krajeeDialog.confirm('Realmente desea borrar asientos del ' + $('#templatemodel-fecha_rango').val() + ' ?', function (result) {
        if (result) {
            $.post(
                url,
                form.serialize()
            )
                .done(function (result) {
                    console.log('aqui');
                    form.parent().html(result.message);
                    $.pjax.reload({container: "#flash_message_id", async: false});
                    $(".modal").modal("hide");
                    $(".modal-body").html("");
                });
            return false;
        } else {
            form.parent().html(result.message);
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
        }
    });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});
JS;
    $this->registerJs($script);

} catch (Exception $e) {
    echo $e;
}
?>
