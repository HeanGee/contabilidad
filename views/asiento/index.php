<?php

use backend\modules\contabilidad\models\Asiento;
use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\AsientoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $showAllEmpresa boolean */

$this->title = 'Asientos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asiento-index">

    <?php //Pjax::begin(); // TODO: Preguntar a Miguel para que era el Pjax. ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    <div class="btn-toolbar">
        <?php $permisos = [
            'create' => PermisosHelpers::getAcceso('contabilidad-asiento-create'),
            'delete-asientos' => PermisosHelpers::getAcceso('contabilidad-asiento-borrar-asientos'),
            'asiento-compra' => PermisosHelpers::getAcceso('contabilidad-asiento-create-from-factura'),
            'asiento-venta' => PermisosHelpers::getAcceso('contabilidad-asiento-create-from-factura'),
            'show-all-empresa' => PermisosHelpers::getAcceso('contabilidad-asiento-show-all-empresa'),
            'asto-cuota-prest' => PermisosHelpers::getAcceso('contabilidad-prestamo-asiento-generar-asiento-cuota'),
        ]; ?>
        <?= $permisos['create'] ? Html::a('Crear Asiento', ['create'], ['class' => 'btn btn-success pull-left']) : null ?>
        <?= $permisos['delete-asientos'] ? Html::button('Borrar asientos', [
            'class' => 'btn btn-warning borrar-asiento pull-right',
            'title' => 'Borrar asientos',
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['borrar-asientos']),
            'data-pjax' => '0',
        ]) : null ?>
        <?= $permisos['asto-cuota-prest'] ? Html::a('Asto. Cuota Préstamos', ['/contabilidad/prestamo-asiento/generar-asiento-cuota'], ['class' => 'btn btn-info pull-right']) : null ?>
        <?= $permisos['show-all-empresa'] ? (
        !$showAllEmpresa ?
            Html::a('Ver todos', ['index', 'showAllEmpresa' => true], ['class' => 'btn btn-warning pull-right']) :
            Html::a('De la empresa ACTUAL', ['index', 'showAllEmpresa' => false], ['class' => 'btn btn-info pull-right'])
        ) : null ?>
        <?= $permisos['asiento-venta'] ? Html::a('Asiento Venta', ['create-from-factura', 'operacion' => 'venta'], ['class' => 'btn btn-info pull-right']) : null ?>
        <?= $permisos['asiento-compra'] ? Html::a('Asiento Compra', ['create-from-factura', 'operacion' => 'compra'], ['class' => 'btn btn-info pull-right']) : null ?>
    </div>
    </p>

    <?php
    try {
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'periodo_contable_id',
                'value' => function ($model) {
                    return $model->periodoContable->anho;
                }
            ],
            [
                'attribute' => 'fecha',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'fecha',
                    'language' => 'es',
                    'pickerButton' => false,
                    'options' => [
                        'style' => 'width:90px',
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'todayBtn' => true,
                    ],
                ]),
                'format' => 'html',
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->fecha));
                }
            ],
        ];

        if (PermisosHelpers::getAcceso('contabilidad-asiento-show-all-empresa') && $showAllEmpresa) {
            $columns = array_merge($columns, [
                [
                    'attribute' => 'empresa_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return "{$model->empresa->razon_social} <strong>({$model->empresa->ruc})</strong>";
                    },
                ],
            ]);
        }

        $columns = array_merge($columns, [
            [
                'attribute' => 'concepto',
                'value' => 'concepto',
                'label' => 'Concepto / Cuenta',
            ],
            [
                'attribute' => 'monto_debe',
                'label' => 'Total',
                'value' => function ($model) {
                    return number_format($model->monto_debe, 0, ',', '.');
                },
                'contentOptions' => ['style' => 'text-align:right'],
            ],
        ]);

        $template = '{view} &nbsp {update} &nbsp {delete}';
        $buttons = [
            'delete' => function ($url, $model, $index) {
                if ($model instanceof Asiento && $model->isBorrable()) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        $url,
                        [
                            'class' => 'btn_delete_asiento',
                            'id' => $index,
                            'title' => Yii::t('app', 'Borrar'),
                            'data-confirm' => 'Esta seguro',
                            'data-method' => 'post'
                        ]
                    );
                } else return null;
            },
            'view' => function ($url, $model, $index) {
                return Html::a(
                    '<span class="glyphicon glyphicon-eye-open"></span>',
                    $url,
                    [
                        'class' => 'btn_view_asiento',
                        'id' => $index,
                        'title' => Yii::t('app', 'Ver'),
                    ]
                );
            },
            'update' => function ($url, $model, $index) {
                if ($model instanceof Asiento && (
                        !$model->getVentas()->exists() &&
                        !$model->getCompras()->exists() &&
                        $model->asiento_id == null
                    ))
                    return Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        $url,
                        [
                            'class' => 'btn_edit_asiento',
                            'id' => $index,
                            'title' => Yii::t('app', 'Editar'),
                        ]
                    );
                return null;
            },
        ];
        array_push($columns, [
            'class' => ActionColumn::class,
            'template' => $template,
            'buttons' => $buttons,
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'delete') {
                    $url = Url::to(['delete', 'id' => $model->id]);
                    return $url;
                } else if ($action === 'view') {
                    $url = Url::to(['view', 'id' => $model->id]);
                    return $url;
                } else if ($action === 'update') {
                    $url = Url::to(['update', 'id' => $model->id]);
                    return $url;
                }
                return '';
            },
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ]);

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $columns,
            'hover' => true,
        ]);

        $script = <<<JS
$(document).on('click', '.borrar-asiento ', (function () {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    
    $.ajax({
        url: boton.data('url'),
        type: 'get',
        data: {},
        success: function(data) {
            let modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            $('.modal-header', modal).css('background', '#3c8dbc');
            $('.modal-title', modal).html(title);
        }
    });
}));
JS;
        $this->registerJs($script);

    } catch (\Exception $e) {
        echo $e;
    }
    ?>

    <?php //Pjax::end(); ?>
</div>
