<?php

use backend\modules\contabilidad\controllers\ReporteLibromayor;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $model \backend\modules\contabilidad\controllers\Reporte */

$this->title = 'Reporte Libro Mayor';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="venta-index">

    <?php $form = ActiveForm::begin([
        'id' => 'reporte-form'
    ]); ?>

    <div class="text-right btn-toolbar">
        <?php
        echo Html::submitButton('Ver PDF', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info pull-left']);
        echo Html::button('Ver EXCEL', ['id' => 'bajar_xls', 'formtarget' => '_blank', 'class' => 'btn btn-warning pull-left']);

        ?>
    </div>

    <br/>

    <div>
        <?php
        try {
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 3,
                'attributes' => [
                    'paper_size' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ReporteLibromayor::getPaperSizes(),
                            'options' => [
                                'placeholder' => 'Por favor Seleccione Uno',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'label' => 'Hoja'
                    ],
                    'orientation' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => [\kartik\mpdf\Pdf::ORIENT_LANDSCAPE => 'Horizontal', \kartik\mpdf\Pdf::ORIENT_PORTRAIT => 'Vertical'],
                            'options' => [
                                'placeholder' => 'Por favor Seleccione Uno',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'label' => 'Orientación'
                    ],
                    'fecha_rango' => [
                        'type' => Form::INPUT_WIDGET,
                        'columnOptions' => ['colspan' => '1'],
                        'widgetClass' => DateRangePicker::className(),
                        'options' => [
                            'model' => $model,
                            'attribute' => 'fecha_rango',
                            'convertFormat' => true,
                            'language' => 'es',
                            'pluginOptions' => [
                                'allowClear' => true,
                                'autoApply' => true,
//                                'timePicker'=>true,
                                'locale' => ['format' => 'd-m-Y'],

                            ],

                        ],
                    ],
                    'tipo_doc' => [
                        'type' => Form::INPUT_HIDDEN
                    ]
                ]
            ]);
        } catch (Exception $e) {
            echo $e;
        } ?>

        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Panel de Selección</h3>
            </div>
            <div class="panel-body">

                <div id="pickList"></div>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $css = <<<CSS
.tooltip > .tooltip-inner {
    background-color: green;
}

/* Tooltip on top */
.tooltip.top > .tooltip-arrow {
    border-top: 5px solid green;
}

/* Tooltip on bottom */
.tooltip.bottom > .tooltip-arrow {
    border-bottom: 5px solid green;
}

/* Tooltip on left */
.tooltip.left > .tooltip-arrow {
    border-left: 5px solid green;
}

/* Tooltip on right */
.tooltip.right > .tooltip-arrow {
    border-right: 5px solid green;
}
CSS;
    $this->registerCss($css);

    $glyph_add = Html::tag('span', "", ["class" => "glyphicon glyphicon-arrow-right", 'aria-hidden' => true]);
    $glyph_rem = Html::tag('span', "", ["class" => "glyphicon glyphicon-arrow-left", 'aria-hidden' => true]);
    $glyph_addAll = Html::tag('span', "", ["class" => "glyphicon glyphicon-plus", 'aria-hidden' => true]);
    $glyph_remAll = Html::tag('span', "", ["class" => "glyphicon glyphicon-minus", 'aria-hidden' => true]);

    $script = <<<JS
$('#reportelibromayor-cuentas').on('select2:open', function () {
    $('#s2-togall-reportelibromayor-cuentas').remove();
});

$('.field-reportelibromayor-cuentas').tooltip({
    title: "Vacío para todos",
    placement: "auto",

    // background: "#EBFFDD",
});

// Picklist
(function ($) {

    $.fn.pickList = function (options) {
        var opts = $.extend({}, $.fn.pickList.defaults, options);

        this.fill = function () {
            var option_left = '';

            $.each(opts.data_left, function (key, val) {
                option_left += '<option data-id=' + val.id + '>' + val.text + '</option>';
            });
            this.find('.pickData').append(option_left);

            var option_right = '';

            $.each(opts.data_right, function (key, val) {
                option_right += '<option data-id=' + val.id + '>' + val.text + '</option>';
            });
            this.find('.pickListResult').append(option_right);
        };
        this.controll = function () {
            var pickThis = this;

            pickThis.find(".pAdd").on('click', function () {
                var p = pickThis.find(".pickData option:selected");
                p.clone().appendTo(pickThis.find(".pickListResult"));
                reload_left(p);
                p.remove();
            });

            pickThis.find(".pAddAll").on('click', function () {
                var p = pickThis.find(".pickData option");
                p.clone().appendTo(pickThis.find(".pickListResult"));
                reload_left(p);
                p.remove();
            });

            pickThis.find(".pRemove").on('click', function () {
                var p = pickThis.find(".pickListResult option:selected");
                p.clone().appendTo(pickThis.find(".pickData"));
                reload_right(p);
                p.remove();
            });

            pickThis.find(".pRemoveAll").on('click', function () {
                var p = pickThis.find(".pickListResult option");
                p.clone().appendTo(pickThis.find(".pickData"));
                reload_right(p);
                p.remove();
            });
        };

        this.getValues = function () {
            var objResult = [];
            this.find(".pickListResult option").each(function () {
                objResult.push({
                    id: $(this).data('id'),
                    text: this.text
                });
            });
            return objResult;
        };

        this.init = function () {
            var pickListHtml =
                "<div class='row'>" +
                "  <div class='col-sm-5'>" +
                "    <input class='input' id='input_left' type='text' placeholder='Filtrar' style='width: 200px;>" +
                "<label style='margin-left:15%'>&nbsp;&nbsp;Disponibles</label>" +
                "	 <select class='form-control pickListSelect pickData' id='left_id' multiple></select>" +
                " </div>" +
                " <div class='col-sm-2 pickListButtons'>" +
                " <br><br><br><br><br>" +
                "	<button type='button' title='Seleccionar uno y agregar' class='pAdd btn btn-primary btn-sm'><span class=\"glyphicon glyphicon-arrow-right\" aria-hidden=\"true\"></span></button>" +
                "	<button type='button' title='Seleccionar uno y retirar' class='pRemove btn btn-primary btn-sm'><span class=\"glyphicon glyphicon-arrow-left\" aria-hidden=\"true\"></span></button>" +
                "   <button type='button' title='Agregar Todos' class='pAddAll btn btn-success btn-sm'><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span></button>" +
                "	<button type='button' title='Retirar Todos' class='pRemoveAll btn btn-danger btn-sm'><span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span></button>" +
                " </div>" +
                " <div class='col-sm-5'>" +
                " 	 <input class='input' id='input_right' type='text' placeholder='Filtrar' style='width: 200px;>" +
                "       <label style='margin-left:15%'>&nbsp;&nbsp;Seleccionadas</label>" +
                "    <select class='form-control pickListSelect pickListResult' id='right_id' multiple></select>" +
                " </div>" +
                "</div>";

            this.append(pickListHtml);

            this.fill();
            this.controll();
        };

        this.init();
        return this;
    };

    $.fn.pickList.defaults = {
        add: 'Agregar',
        addAll: 'Agregar Todo',
        remove: 'Borrar',
        removeAll: 'Borrar Todo'
    };


}(jQuery));

var pick = $("#pickList").pickList({
    data_left: $json_pick_left,
    data_right: $json_pick_right
});

$("#getSelected").click(function () {
    console.log(pick.getValues());
});

//filtro de picklist
jQuery.fn.filterByText = function (textbox) {
    return this.each(function () {
        var select = this;

        // Orden de lista en select
        var my_options = $(select).find('option');
        var selected = $(select).val();
        my_options.sort(function (a, b) {
            if (a.text.toLowerCase() > b.text.toLowerCase()) return 1;
            if (a.text.toLowerCase() < b.text.toLowerCase()) return -1;
            return 0
        });
        $(select).empty().append(my_options);
        $(select).val(selected);

        // genera lista auxiliar con datos originales

        var options = [];
        $(select).find('option').each(function () {
            options.push({
                value: $(this).data('id'),
                text: $(this).text()
            });
        });
        $(select).data('options', options);

        // Evento capturado para filtrar
        $(textbox).bind('change keyup', function () {
            var options = $(select).empty().data('options');
            var search = $.trim($(this).val());
            var regex = new RegExp(search, "gi");
            $.each(options, function (i) {
                var option = options[i];
                if (option.text.match(regex) !== null) {
                    $(select).append(
                        $('<option  data-id=' + option.value + '>').text(option.text).val(option.value)
                    );
                }
            });
        });
    });
};

//

$(function () {
    $('#left_id').filterByText($('#input_left'));
});

$(function () {
    $('#right_id').filterByText($('#input_right'));
});

$(document).on('submit', '#reporte-form', function () {
    var selected = [];
    $('#right_id').find('option').each(function () {
        selected.push($(this)[0].getAttribute('data-id'));
    });

    console.log(selected);
    
    let borrar = true;
    
    let form = $('#reporte-form');
    form.find('input[name="selected[]"]').remove();
    
    $.each(selected, function (i, v) {
        var input = $("<input>").attr({"type": "hidden", "name": "selected[]"}).val(v);
        form.append(input);
    });
    return true;
});

var reload_left = function (p) {
    var options_left = [];
    var options_right = [];
    options_left = $('#left_id').data('options');
    options_right = $('#right_id').data('options');
    $(p).each(function () {
        var id_or = $(this).data('id');

        $.each(options_left, function (i) { // cada elemento que habia originalmente
            var option = options_left[i];

            if (!(typeof option === 'undefined')) {
                if (id_or === option.value) {

                    options_right.push({
                        value: option.value,
                        text: option.text
                    });
                    options_left.splice(options_left.indexOf(option), 1);
                }
            }
        });
    });
};

var reload_right = function (p) {
    var options_right = [];
    options_right = $('#right_id').data('options');
    var options_left = [];
    options_left = $('#left_id').data('options');
    $(p).each(function () {
        var id_or = $(this).data('id');

        $.each(options_right, function (i) { // cada elemento que habia originalmente
            var option = options_right[i];

            if (!(typeof option === 'undefined')) {
                if (id_or === option.value) {
                    options_left.push({
                        value: option.value,
                        text: option.text
                    });
                    options_right.splice(options_right.indexOf(option), 1);
                }
            }
        });
    });
    
    // options_left = options_left.sort(compareSecondColumn);
};
JS;

       //funcion para botones de pdf y xls
        $JS_DOCUMENT_ON_READY = <<<JS
$(document).ready(function () {
    
    document.getElementById('reporte-form').setAttribute("target", "_blank");
    // Cada vez que haga submit, renderiza en nueva pestanha.
    $('#btn_submit').on('click', function() {
        $('#reportelibromayor-tipo_doc').val('pdf');
    });
    
    // para descargar el libro en formato xls.
    $('#bajar_xls').on('click', function() {
        $('#reportelibromayor-tipo_doc').val('xls');
        $('#reporte-form').submit();
    });
});
JS;
       $scripts = [];
       $scripts[] = $JS_DOCUMENT_ON_READY;
       foreach ($scripts as $s) $this->registerJs($s);

    $this->registerJs($script);


       $this->registerCss('.
  pickListButtons {
    padding: 10px;
    text-align: center;
}

.pickListButtons button {
    display: block;
    margin: auto;
    margin-bottom: 5px;
}

.pickListSelect {
    height: 200px !important;
}

.input {
    background-image: url(img/searchicon.png);
    background-position: 10px 12px;
    background-repeat: no-repeat;
    padding: 12px 20px 12px 42px;
    border: 1px solid #ddd;
    margin-bottom: 12px;
}');
    ?>
