<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 3/13/18
 * Time: 5:58 PM
 */

use backend\models\Empresa;
use backend\modules\contabilidad\controllers\ReporteLibromayor;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;

/** @var $model ReporteLibromayor */
?>

<div class="header" style="width: 100%;">

    <div class="reporte-titulo">
        <h3 class="reporte-titulo-h3">LIBRO MAYOR</h3>
    </div>

    <div class="reporte-titulo-contenido">
        <table class="reporte-titulo-tabla-contenido" style="width: 100%;">
            <thead>
            <tr>
                <th>Desde</th>
                <th>Hasta</th>
                <th>Responsable</th>
                <th>Generado el</th>
                <th>Empresa</th>
                <th style="text-align: center">Periodo Contable</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?= date_create_from_format('Y-m-d', $model->fecha_desde)->format('d-m-Y') ?></td>
                <td><?= date_create_from_format('Y-m-d', $model->fecha_hasta)->format('d-m-Y') ?></td>
                <td><?= Yii::$app->user->identity->username ?></td>
                <td><?= strftime('%d-%m-%Y a las %H:%M:%S', time()) ?></td>
                <td><?= Empresa::findOne(['id' => \Yii::$app->session->get('core_empresa_actual')])->razon_social ?></td>
                <td style="text-align: center"><?= EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')])->anho ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <br/>
</div>