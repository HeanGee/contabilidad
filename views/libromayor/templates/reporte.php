<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 2/3/18
 * Time: 5:15 PM
 */

use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\ValorHelpers;
use yii\helpers\Html;

/* @var $data array */

?>

<?php
?>
<div class="contents" style="width: 100%;">
    <table width="100%" CLASS="contenido table-condensed">
        <thead>
        <tr class="fila-cabecera">
            <th class="primera-columna" style="width: 10%">ASIENTO</th>
            <th>FECHA</th>
            <th>DETALLE</th>
            <th style="text-align: right">DEBE</th>
            <th style="text-align: right">HABER</th>
            <th style="text-align: right">SALDO</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $debes = 0.0;
        $haberes = 0.0;
        $saldo = 0.0;
        foreach ($data as $cta_id => $datos) {
            $cta = PlanCuenta::findOne(['id' => $cta_id]);
            echo Html::beginTag('tr', ['class' => 'fila-datos']);
            echo Html::tag('td', $cta->cod_completo, ['style' => 'font-weight: bold;']);
            echo Html::tag('td', $cta->nombre, ['style' => 'font-weight: bold;']);
            echo Html::tag('td', "", []);

            $contador = 0;
            $print = [];
            $_debe = 0.0;
            $_haber = 0.0;
            $_saldo = 0.0;
            $saldo = 0.0;

            foreach ($datos as $_ => $detalle) {
                $contador++;

                $isCtaAcreedor = (in_array((int)explode('.', $cta->cod_completo)[0], [2, 3])) ? true : false;
                $saldo += (float)$detalle['debe'] * ($isCtaAcreedor ? -1 : 1);
                $saldo -= (float)$detalle['haber'] * ($isCtaAcreedor ? -1 : 1);

                if ($contador < sizeof($datos))
                    $print[] = Html::beginTag('tr', []);
                else
                    $print[] = Html::beginTag('tr', [/*'class' => "ultima-fila-datos"*/]);

                $print[] = Html::tag('td', $detalle['asiento_id'], ['class' => 'number-field', 'style' => "border-bottom: " . (($_ == sizeof($datos) - 1) ? '2' : '1') . "px dotted #BABABA;"]);
                $print[] = Html::tag('td', date_create_from_format('Y-m-d', $detalle['fecha'])->format('d-m-Y'), ['style' => "text-align: right; border-bottom: " . (($_ == sizeof($datos) - 1) ? '2' : '1') . "px dotted #BABABA;"]);
                $print[] = Html::tag('td', $detalle['descripcion'], ['style' => "border-bottom: " . (($_ == sizeof($datos) - 1) ? '2' : '1') . "px dotted #BABABA;"]);
                $print[] = Html::tag('td', ValorHelpers::numberFormat($detalle['debe'], 0), ['class' => 'number-field', 'style' => "border-bottom: " . (($_ == sizeof($datos) - 1) ? '2' : '1') . "px dotted #BABABA;"]);
                $print[] = Html::tag('td', ValorHelpers::numberFormat($detalle['haber'], 0), ['class' => 'number-field', 'style' => "border-bottom: " . (($_ == sizeof($datos) - 1) ? '2' : '1') . "px dotted #BABABA;"]);
                $print[] = Html::tag('td', ValorHelpers::numberFormat($saldo, 0), ['class' => 'number-field', 'style' => "border-bottom: " . (($_ == sizeof($datos) - 1) ? '2' : '1') . "px dotted #BABABA" . (($_ == sizeof($datos) - 1) ? '2' : '1') . ";"]);

                $debes += (float)$detalle['debe'];
                $haberes += (float)$detalle['haber'];
                $_debe += (float)$detalle['debe'];
                $_haber += (float)$detalle['haber'];
                $_saldo = $_debe - $_haber;
            }

            echo Html::endTag('tr');
            print_r($print);

            echo '<tr class="ultima-fila-datos">';
            echo '<td colspan="3" style="text-align: left; font-weight: bold;">SUMA:</td>';
            echo Html::tag('td', ValorHelpers::numberFormat($_debe, 0), ['style' => 'text-align:right; font-weight: bold;']);
            echo Html::tag('td', ValorHelpers::numberFormat($_haber, 0), ['style' => 'text-align:right; font-weight: bold;']);
            echo Html::tag('td', "" /*ValorHelpers::numberFormat($_saldo, 0)*/, ['style' => 'text-align:right; font-weight: bold;']);
            echo '</tr>';
        }
        ?>
        <tr class="fila-resumen">
            <td colspan="3" style="text-align: left;"><strong>TOTALES:</strong></td>
            <td style="text-align: right; font-weight: bold;"><?= ValorHelpers::numberFormat($debes, 0) ?></td>
            <td style="text-align: right; font-weight: bold;"><?= ValorHelpers::numberFormat($haberes, 0) ?></td>
            <td style="text-align: right; font-weight: bold;"><?= "" // ValorHelpers::numberFormat($debes - $haberes, 0)  ?></td>
        </tr>
        <tr class="ultima-fila">
            <td colspan="5">&nbsp;</td>
            <td style="text-align: right; font-style: italic;">Listado Concluído</td>
        </tr>
        </tbody>
    </table>
</div>
