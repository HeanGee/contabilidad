<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 3/13/18
 * Time: 5:58 PM
 */
?>

<div class="footer" style="width: 100%;">
    <table width="100%">
        <tr>
            <td width="33%" style="font-style: italic; font-size: 11px"><strong>Fecha de generación:</strong> <?php
                echo strftime('%A, %d de %B del %Y a las %H:%M:%S', time()) ?></td>
            <td width="33%" style="font-style: italic; text-align: center; font-size: 11px">
                <strong>Responsable:</strong> <?= Yii::$app->user->identity->username ?></td>
            <td width="33%" style="text-align: right; font-style: italic; font-size: 11px; font-style: ">
                <strong>Página</strong> {PAGENO}/{nbpg}
            </td>
        </tr>
    </table>
</div>