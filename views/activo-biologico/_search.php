<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\search\ActivoBiologicoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activo-ganado-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'empresa_id') ?>

    <?= $form->field($model, 'periodo_contable_id') ?>

    <?= $form->field($model, 'tipo') ?>

    <?= $form->field($model, 'stock_final') ?>

    <?php // echo $form->field($model, 'precio_unitario') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
