<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologico */

$this->title = "Activo Ganado {$model->especie->nombre} ID {$model->id}";
$this->params['breadcrumbs'][] = ['label' => 'Activo Biologico', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="activo-ganado-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a A. Bio.', ['index'/*, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Realmente desea borrar este Activo Ganado?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                ['attribute' => 'especie_id', 'value' => $model->especie->nombre],
                ['attribute' => 'stock_inicial', 'value' => $model->getFormattedStockInicial()],
                ['attribute' => 'stock_actual', 'value' => $model->getFormattedStockActual()],
                ['attribute' => 'stock_final', 'value' => $model->getFormattedStockFinal()],
                ['attribute' => 'precio_unitario', 'value' => $model->getFormattedPrecioUnitario()],
                ['attribute' => 'total', 'value' => $model->getFormattedTotal()],
            ],
        ]);

        // Facturas relacionadas.
        echo $this->render('@backend/modules/contabilidad/views/facturas-relacionadas/_facturas_relacionadas', ['model' => $model]);

    } catch (Exception $exception) {
        print $exception;
    } ?>

</div>
