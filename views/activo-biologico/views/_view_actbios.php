<?php

/* @var $this yii\web\View */

use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>

<?php
isset($heading) || $heading = "Activos Biológicos";
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'toolbar' => [],
        'hover' => true,
        'panel' => [
            'type' => 'info',
            'heading' => $heading,
            'footerOptions' => ['class' => ''],
            'beforeOptions' => ['class' => ''],
            'afterOptions' => ['class' => '']
        ],
        'panelFooterTemplate' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'nombre',
                'format' => 'raw',
                'value' => function ($model) {
                    $url = Url::to(['/contabilidad/activo-biologico/view', 'id' => $model->id]);
                    return Html::a($model->nombre, $url, []);
                }
            ],
            [
                'attribute' => 'clasificacion_id',
                'value' => 'clasificacionNombre',
                'format' => 'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(ActivoBiologicoEspecieClasificacion::find()->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])->all(), 'nombre', 'nombre'),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'Filtro por Clasificación...'],
            ],
            [
                'attribute' => 'stock_inicial',
                'value' => 'formattedStockInicial',
                'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
            ],
            [
                'attribute' => 'stock_actual',
                'value' => 'formattedStockActual',
                'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
            ],
            [
                'attribute' => 'stock_final',
                'value' => 'formattedStockFinal',
                'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
            ],
            [
                'attribute' => 'precio_unitario',
                'value' => 'formattedPrecioUnitario',
                'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
            ],
            [
                'attribute' => 'precio_unitario_actual',
                'value' => 'formattedPrecioUnitarioActual',
                'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
            ],
            [
                'attribute' => 'total',
                'value' => 'formattedTotal',
                'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
            ],
        ],
    ]);
} catch (Exception $exception) {
    \common\helpers\FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    echo $exception->getMessage();
}
