<?php

use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\dialog\Dialog;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologico */
/* @var $form ActiveForm */
?>

<div class="activo-ganado-form">

    <p><?= PermisosHelpers::getAcceso('contabilidad-activo-biologico-index') ?
            Html::a("Ir a Activos Biológicos", ['index'], ['class' => 'btn btn-info']) : null ?></p>

    <?php $form = ActiveForm::begin(); ?>

    <?php try {
        echo Html::activeHiddenInput($model, "guardar_cerrar");
        echo Html::activeHiddenInput($model, "id");
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'especie_id' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'], // usar con el sub-autoGenerateColumns = false,
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => ActivoBiologicoEspecie::getEspeciesLista(),
                                ],
                                'options' => ['placeholder' => 'Seleccione una opción...'],
                                'initValueText' => ActivoBiologicoEspecie::getEspecieNombre(isset($model->especie_id) ? $model->especie_id : ''),
                            ]
                        ],
                        'clasificacion_id' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'options' => ['placeholder' => 'Seleccione una opción...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
//                                    'ajax' => [
//                                        'url' => Url::to(['activo-biologico-especie-clasificacion/get-clasificaciones-ajax']),
//                                        'dataType' => 'json',
//                                        'data' => new JsExpression("function(params) { return {q:params.term,id:$('#activobiologico-especie_id').val()}; }")
//                                    ]
                                ],
                                'initValueText' => ActivoBiologicoEspecieClasificacion::getClasificacionNombre(isset($model->clasificacion_id) ? $model->clasificacion_id : ''),
                            ]
                        ],
                        'sexo' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => ActivoBiologico::getSexoList(),
                                ],
                                'options' => ['placeholder' => 'Seleccione una opción...'],
                                'initValueText' => ActivoBiologico::getSexoName($model->sexo),
                            ]
                        ],
                        'stock_inicial' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => \yii\widgets\MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => '',
                                    'autoGroup' => true
                                ],
                            ],
                        ],
                        'precio_unitario' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => \yii\widgets\MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'stock_actual' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => \yii\widgets\MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => '',
                                    'autoGroup' => true
                                ],
                            ],
                        ],
                        'stock_final' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => \yii\widgets\MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => '',
                                    'autoGroup' => true
                                ],
                            ],
                        ],
                        'precio_unitario_actual' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => \yii\widgets\MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                            ],
                        ],
                    ]
                ]
            ],
        ]);

        if (PermisosHelpers::getAcceso('contabilidad-activo-biologico-create') &&
            PermisosHelpers::getAcceso('contabilidad-activo-biologico-update')) {
            $botonText = '';
            $botonClass = '';
            if (Yii::$app->controller->action->id == 'create') {
                $botonText = 'Guardar';
                $botonClass = 'btn btn-success';
            } else {
                $botonText = 'Actualizar';
                $botonClass = 'btn btn-primary';
            }
            echo Html::beginTag('div', ['class' => 'form-group btn-toolbar']);
            echo Html::submitButton($botonText, ['class' => "{$botonClass} guardar pull-left"]);
            if (Yii::$app->controller->action->id == 'create')
                echo Html::submitButton('Guardar y cerrar', ['class' => "btn btn-primary guardar-cerrar pull-left"]);
            echo Html::endTag('div');
        }
        echo Dialog::widget();

        $action = Yii::$app->controller->action->id;
        $_old_stock_final = array_key_exists('stock_final', $model->oldAttributes) ? $model->oldAttributes['stock_final'] : 0;
        $_old_stock_actual = array_key_exists('stock_actual', $model->oldAttributes) ? $model->oldAttributes['stock_actual'] : 0;
        $_old_stock_inicial = array_key_exists('stock_inicial', $model->oldAttributes) ? $model->oldAttributes['stock_inicial'] : 0;
        $script_head = <<<JS
var _clasificacion_selected = "$model->clasificacion_id";
JS;
        $this->registerJs($script_head, \yii\web\View::POS_HEAD);

        $url_setTotal = \yii\helpers\Json::encode(Yii::t('app', \yii\helpers\Url::to(['calculate-total'])));
        $url_getClasifByEspecie = \yii\helpers\Json::encode(Yii::t('app', \yii\helpers\Url::to(['activo-biologico-especie-clasificacion/get-clasificaciones-ajax'])));
        $script = <<<JS
//function calcularTotal() {
//    let preciou = $('#activobiologico-precio_unitario');
//    let stock = $('#activobiologico-stock_inicial');
//
//    // if (preciou.val() !== "" && stock.val() !== "") {
//    $.ajax({
//        url: $url_setTotal,
//        type: 'get',
//        data: {
//            precio_unitario: preciou.val(),
//            stock: stock.val()
//            id: "$model->id",
//        },
//        success: function (data) {
//            let total = $('#activobiologico-total');
//            total.parent().find('div.help-block').html('');
//            total.parent().removeClass('has-error');
//            total.parent().addClass('has-success');
//            total.val(data);
//        },
//        error: function (request, status, error) {
//            let total = $('#activobiologico-total');
//            total.val('0');
//            total.parent().find('div.help-block').html(request.responseText);
//            total.parent().removeClass('has-success');
//            total.parent().addClass('has-error');
//        },
//    });
//    // }
//}

// $(document).on('keyup', '#activobiologico-stock_inicial, #activobiologico-precio_unitario', function () {
//     calcularTotal();
// });

$(document).on('click', '.guardar-cerrar', function () {
    $('#activobiologico-guardar_cerrar').val('si').trigger('change');
});

$(document).on('click', '.guardar', function () {
    $('#activobiologico-guardar_cerrar').val('no').trigger('change');
});

$(document).on('keyup', '#activobiologico-stock_inicial', function (evt) {
    // stock_actual y stock_final sufren una modificacion en una cantidad igual a la diferencia que hay entre
    // el stock_inicial que tenia el modelo inicialmente y el stock_inicial definido en el formulario.
    
    let dif_stock_inicial = parseInt($(this).val().replace(/\./g, '')) - ($_old_stock_inicial);
    let actual = dif_stock_inicial + ($_old_stock_actual);
    let final = dif_stock_inicial + ($_old_stock_final);
    $('#activobiologico-stock_actual').val(actual).trigger('change');
    $('#activobiologico-stock_final').val(final).trigger('change');
});


$(document).on('keyup', '#activobiologico-precio_unitario', function() {
    if ("$action" === 'create') {
        $('#activobiologico-precio_unitario_actual').val($(this).val()).trigger('change');
    }
});

$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        $(this).siblings('select').select2('open');
    }
});

$(document).on('change', '#activobiologico-clasificacion_id', function () {
    let select2 = $(this).select2('data');
    let val = $(this).val();
    if (select2.length > 0 && val !== '') {
        _clasificacion_selected = val;
    }
});

$(document).on('change', '#activobiologico-especie_id', function () {
    let clasificacionId = $('#activobiologico-clasificacion_id');
    // let selected = clasificacionId.val() !== "" ? clasificacionId.val() : _clasificacion_selected;

    clasificacionId.val('').trigger('change');
    clasificacionId.select2("destroy");
    clasificacionId.html("<option><option>");
    clasificacionId.select2({
        theme: 'krajee',
        placeholder: '',
        language: 'en',
        width: '100%',
        data: [],
    });

    $.ajax({
        url: $url_getClasifByEspecie,
        type: 'get',
        data: {
            id: $('#activobiologico-especie_id').val(),
        },
        success: function (data) {
            let arr = [{id: "", text: ''}]; // es el item necesario para que no se seleccione el primero por defecto.

            if (data.length === 0) {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
            else {
                let element_exist = false;
                // Construir options para select2
                data['results'].forEach(function (e, i) {
                    arr.push(e);
                    if (e['id'] === _clasificacion_selected)
                        element_exist = true
                });

                // Rellenar select2 con las opciones
                clasificacionId.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: arr,
                    allowClear: true
                });

                console.log(arr);
                // si selected esta en el nuevo conj. de opciones, se selecciona de nuevo.
                //  de lo contrario, queda vacio solito.
                if (element_exist)
                    clasificacionId.val(_clasificacion_selected).trigger('change');

            }
        },
        error: function (request, status, error) {

        }
    });
});

$(document).ready(function () {
    $('#activobiologico-especie_id').trigger('change'); // para que cargue las clasificaciones
    $('#activobiologico-clasificacio_id').val(_clasificacion_selected);
    $('#activobiologico-total').prop('readonly', true);
    $('#activobiologico-stock_final').prop('readonly', true);
    $('#activobiologico-stock_actual').prop('readonly', true);
    $('#activobiologico-precio_unitario_actual').prop('readonly', true);
    // calcularTotal();
});
JS;
        $this->registerJs($script);

    } catch (Exception $exception) {
        throw $exception;
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
