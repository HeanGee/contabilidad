<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologico */

$this->title = 'Crear Nuevo Activo Biológico';
$this->params['breadcrumbs'][] = ['label' => 'Activos Biológicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-ganado-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
