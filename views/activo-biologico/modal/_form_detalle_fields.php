<?php

use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use yii\helpers\Html;

/* @var $model ActivoBiologicoStockManager */
?>

<?php
$action = Yii::$app->controller->action->id;
$isNota = in_array($action, ['devolver-act-bio', 'retornar-act-bio']);
?>

<td width="25%">
    <?= $form->field($model, 'especie_id')->textInput([
        'id' => "ActivoBiologicoStockManager_{$key}_especie_id",
        'name' => "ActivoBiologicoStockManager[$key][especie_id]",
        'class' => "form-control",
    ])->label(false) ?>
</td>

<td width="25%">
    <?= $form->field($model, 'clasificacion_id')->textInput([
        'id' => "ActivoBiologicoStockManager_{$key}_clasificacion_id",
        'name' => "ActivoBiologicoStockManager[$key][clasificacion_id]",
        'class' => "form-control",
    ])->label(false) ?>
</td>

<td width="15%">
    <?= $form->field($model, 'cantidad')->textInput([
        'id' => "ActivoBiologicoStockManager_{$key}_cantidad",
        'name' => "ActivoBiologicoStockManager[$key][cantidad]",
        'class' => "form-control",
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<td width="25%">
    <?= $form->field($model, 'precio_unitario')->textInput([
        'id' => "ActivoBiologicoStockManager_{$key}_precio_unitario",
        'name' => "ActivoBiologicoStockManager[$key][precio_unitario]",
        'class' => "form-control",
        'readonly' => $isNota,
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<?php if (!$isNota) : ?>
<td style="text-align: center; padding: 2.4% 0 0 0;">
    <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', 'javascript:void(0);', [
        'class' => 'delete-detalle',
        'id' => "ActivoBiologicoStockManager_{$key}_delete_button",
        'title' => 'Eliminar fila',
    ]) ?>
</td>
<?php endif; ?>