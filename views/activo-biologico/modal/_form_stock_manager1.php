<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 14/11/18
 * Time: 01:43 PM
 */

use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $allModels ActivoBiologicoStockManager[] */

?>
<div class="activo-biologico-stock-manager-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'stock-manager-form',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <fieldset>
        <legend>&nbsp;Detalles
            <div class="pull-right">
                <?php
                // new detalle button
                echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                    'id' => 'new-detalle',
                    'class' => 'pull-left btn btn-success'
                ]);
                ?>
            </div>
        </legend>

        <?php
        \kartik\select2\Select2Asset::register($this);

        $query = ActivoBiologico::find();
        $query->alias('bio');
        $query->joinWith('especie as especie');
        $query->select(['bio.id', 'especie.nombre as text', 'bio.especie_id as especie_id', 'bio.clasificacion_id as clasificacion_id']);

        $actbios = [
            ['id' => '', 'text' => ''],
        ];
        $actbios = array_merge($actbios, $query->asArray()->all());
        $actbios = \yii\helpers\Json::encode($actbios);

        echo '<table id="tabla-detalles" class="table table-condensed table-bordered">';
        echo '<thead>';
        echo '<tr>';
        echo '<th style="text-align: center;">A. Biológico</th>';
        echo '<th style="text-align: center;">Cantidad</th>';
        echo '<th style="text-align: center;">Acciones</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        try {
            // existing detalles fields
            $key = 0;
            foreach ($allModels as $index => $model) {
                $key = $model->id != null ? $model->id - 1 : $index;
                echo '<tr class="fila-detalles">';
                echo $this->render('_form_detalle_fields', [
                    'key' => 'new' . ($key + 1),
                    'form' => $form,
                    'model' => $model,
                ]);
                echo '</tr>';
                $key++;
            }

            // new detalles fields
            $model = new ActivoBiologicoStockManager();
            echo '<tr id="new-detalle-block" style="display: none;">';
            echo $this->render('_form_detalle_fields', [
                'key' => '__new__',
                'form' => $form,
                'model' => $model,
            ]);
            echo '</tr>';
            echo '</tbody>';
            echo '</table>';

        } catch (Exception $exception) {
            print $exception->getMessage();
        } ?>

    </fieldset>

    <?php
    echo Html::submitButton('Guardar', ['class' => 'btn btn-info']);
    ActiveForm::end();

    $script_head = <<<JS
var detalle_k = $key;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);

    $script = <<<JS
function construirSelect2() {
    $(':input[id$="_activo_biologico_id"]').not(document.getElementById('ActivoBiologicoStockManager___new___activo_biologico_id')).each(function () {
        let e = $(this);
        console.log($(e).attr("id"), typeof $(e).attr('select2') === "undefined");
        
        if (typeof $(e).attr('select2') === "undefined") {
            $(e).select2({
                theme: 'krajee',
                placeholder: '',
                language: 'en',
                width: '100%',
                data: $actbios,
            });
        }
    });
}

$('#new-detalle').on('click', function () {
    detalle_k += 1;
    $('#tabla-detalles').find('tbody')
        .append('<tr class="fila-detalles">' + $('#new-detalle-block').html().replace(/__new__/g, 'new' + detalle_k).replace(/__new_class__/g, '') + '</tr>');

    construirSelect2();
});

// no se usa pero se deja como backup
$(document).on('click', '.delete-detalle', function () {
    $(this).closest('tbody tr').remove();
});

$("form#stock-manager-form").on("beforeSubmit", function (e) {
    console.log('a');
    var form = $(this);
    let url = form.attr("action") + "&submit=true";

    $.post(
        url,
        form.serialize()
    )
    .done(function (result) {
        form.parent().html(result.message);
        $.pjax.reload({container: "#flash_message_id", async: false});
        $("#modal").modal("hide");
        $("modal-body").html("");
    });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});
JS;
    $this->registerJs($script); ?>

</div>