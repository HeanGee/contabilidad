<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 29/10/18
 * Time: 01:06 PM
 */

use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */

?>

<?php
try {

    ActiveFormDisableSubmitButtonsAsset::register($this);

    $form = ActiveForm::begin([
        'id' => '_form_traspaso_periodo',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]);

    $data = [];
    $periodo_actual = \backend\modules\contabilidad\models\EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'));
    foreach (\backend\modules\contabilidad\models\EmpresaPeriodoContable::find()->where([
        'activo' => "Si",
        'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
    ])->andWhere(['>', 'anho', $periodo_actual->anho])->all() as $periodo) {
        $data[$periodo->id] = "$periodo->anho";
    }

    echo $form->field($model, 'periodo_contable_id')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Seleccione un prestamo ...'],
        'data' => $data,
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]);

    $boton = \yii\helpers\Html::submitButton('Traspasar', ['class' => 'btn btn-warning pull-left']);
    $html = <<<HTML
<div class="btn-toolbar">
$boton
</div>
HTML;
    echo $html;

    ActiveForm::end();

    $script = <<<JS
$("form#_form_traspaso_periodo").on("beforeSubmit", function (e) {
    var form = $(this);
    let url = form.attr("action") + "&submit=true";
    
    $.post(
        url,
        form.serialize()
    )
    .done(function (result) {
        form.parent().html(result.message);        
        $.pjax.reload({container: "#flash_message_id", async: false});
        $("#modal").modal("hide");
        $("modal-body").html("");
    });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});
JS;
    $this->registerJs($script);


} catch (Exception $exception) {
    print $exception;
}
?>
