<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 14/11/18
 * Time: 01:43 PM
 */

use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */

?>

<?php
try {

    ActiveFormDisableSubmitButtonsAsset::register($this);

    $form = ActiveForm::begin([
        'id' => '_form_traspaso_periodo',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]);

    // cuerpito
    $action = Yii::$app->controller->action->id;
    $isNota = in_array($action, ['devolver-act-bio', 'retornar-act-bio']);
    echo '<fieldset>';
    {
        echo '<legend>&nbsp;Detalles';
        echo '<div class="pull-right">';
        // new detalle button
        echo !$isNota ? Html::a('<span class="glyphicon glyphicon-plus"></span>', 'javascript:void(0);', [
            'id' => 'new-detalle',
            'class' => 'pull-left btn btn-success btn-sm'
        ]) : null;
        echo '</div>';
        echo '</legend>';

        \kartik\select2\Select2Asset::register($this);

        $query = ActivoBiologicoEspecie::find();
        $query->select(['id', 'nombre as text']);
        $query->where([
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')
        ]);

        $especies = [
            ['id' => '', 'text' => ''],
        ];
        $especies = array_merge($especies, $query->asArray()->all());
        $especies = \yii\helpers\Json::encode($especies);

        $query = \backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion::find();
        $query->select(['id', 'nombre as text']);
        $query->where([
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')
        ]);

        $clasif = [
            ['id' => '', 'text' => ''],
        ];
        $clasif = array_merge($clasif, $query->asArray()->all());
        $clasif = \yii\helpers\Json::encode($clasif);

        $_comprar_o_vender_txt = ($action == 'comprar-act-bio') ? "a comprar" : (($action == 'vender-act-bio') ? 'a vender' : '');
        $_tipo_precio_txt = ($action == 'comprar-act-bio') ? "Unitario" : (($action == 'vender-act-bio') ? 'de Venta' : (($action == 'devolver-act-bio') ? 'de Compra' : 'de Venta'));

        echo '<table id="tabla-detalles" class="table table-condensed table-responsive">';
        echo '<thead>';
        echo '<tr>';
        echo '<th style="text-align: center;">Especie</th>';
        echo '<th style="text-align: center;">Clasificación</th>';
        echo "<th style='text-align: center;'>Cantidad {$_comprar_o_vender_txt}</th>";
        echo "<th style='text-align: center;'>Precio {$_tipo_precio_txt}</th>";
        echo !$isNota ? '<th style="text-align: center; width: 10%"></th>' : null;
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        try {
            // existing detalles fields
            $key = 0;
            $sessionData = Yii::$app->session->get($session_key);
            $dataProvider = $sessionData['dataProvider'];
            $allModels = $dataProvider->allModels;
            foreach ($allModels as $index => $model) {
                $key = $model->id != null ? $model->id - 1 : $index;
                echo '<tr class="fila-detalles">';
                echo $this->render('_form_detalle_fields', [
                    'key' => 'new' . ($key + 1),
                    'form' => $form,
                    'model' => $model,
                ]);
                echo '</tr>';
                $key++;
            }

            // new detalles fields
            $model = new ActivoBiologicoStockManager();
            $model->loadDefaultValues();
            $model->especie_id = 0;
            $model->clasificacion_id = 0;
            $model->cantidad = 0;
            $model->precio_unitario = 0;
            echo '<tr id="new-detalle-block" style="display: none;">';
            echo $this->render('_form_detalle_fields', [
                'key' => '__new__',
                'form' => $form,
                'model' => $model,
            ]);
            echo '</tr>';
            echo '</tbody>';
            echo '</table>';

        } catch (Exception $exception) {
            print $exception->getMessage();
        }
    }
    echo '</fieldset>';
    // cuerpito-fin

    echo Html::submitButton('Guardar', ['id' => 'btn-stock-manager-submit', 'class' => 'btn btn-success']);
    ActiveForm::end();

    $css = <<<CSS
.modal-dialog {
   width:600px;
}
CSS;

    $this->registerCss($css);

    $script_head = <<<JS
var detalle_k = $key;
var fila_id = "$fila_id";
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);

    $url_getClasifByEspecie = \yii\helpers\Json::encode(Yii::t('app', \yii\helpers\Url::to(['activo-biologico-especie-clasificacion/get-clasificaciones-ajax'])));
    $url_getValorParaPlantilla = \yii\helpers\Json::encode(Yii::t('app', \yii\helpers\Url::to(['get-valor-para-plantilla'])));
    $script = <<<JS
function construirSelect2(key) {
    $(':input[id$="' + key + '_especie_id"]').not(document.getElementById('ActivoBiologicoStockManager___new___especie_id')).each(function () {
        let e = $(this);
        
        if (typeof $(e).attr('select2') === "undefined") {
            $(e).select2({
                theme: 'krajee',
                placeholder: '',
                language: 'en',
                width: '100%',
                data: $especies,
            });
        }
    });
    
    $(':input[id$="' + key + '_clasificacion_id"]').not(document.getElementById('ActivoBiologicoStockManager___new___clasificacion_id')).each(function () {
        let e = $(this);
        
        if (typeof $(e).attr('select2') === "undefined") {
            $(e).select2({
                theme: 'krajee',
                placeholder: '',
                language: 'en',
                width: '100%',
                data: $clasif,
                allowClear: true,
            });
        }
    });
}

function dejarSoloNros(value, permitirDecimal = false) {
    // value = value.replace(/\D/g, '');
    // value = (!permitirDecimal) ?
    //  value.replace(/[\`\~\!\@\#\$\%\^\&\*\(\)\_\+\=\-\[\]\|\{\}\'\"\<\>\,\.\/\?\¨\´\¿\Ç\|\º\ª]/g, '') :
    //  value.replace(/[\`\~\!\@\#\$\%\^\&\*\(\)\_\+\=\-\[\]\|\{\}\'\"\<\>\,\/\?\¨\´\¿\Ç\|\º\ª]/g, '');
    return value;
}

$(document).on('change', ':input[id$="cantidad"]', function() {
    let value = $(this).val();
    
    $(this).val(dejarSoloNros(value));
});

$(document).on('change', ':input[id$="precio_unitario"]', function() {
    let value = $(this).val();
    
    $(this).val(dejarSoloNros(value, true));
});

$(document).on('change', ':input[id$="especie_id"]', function () {
    let this_id = $(this).attr('id');
    let clasificacionId = $('#' + this_id.replace('especie_id', 'clasificacion_id'));
    // let selected = clasificacionId.val() !== "" ? clasificacionId.val() : _clasificacion_selected;

    clasificacionId.select2("destroy");
    clasificacionId.html("<option><option>");
    clasificacionId.select2({
        theme: 'krajee',
        placeholder: '',
        language: 'en',
        width: '100%',
        data: [],
    });
    clasificacionId.val('').trigger('change');

    $.ajax({
        url: $url_getClasifByEspecie,
        type: 'get',
        data: {
            id: $('#' + this_id).val(),
        },
        success: function (data) {
            let arr = [{id: "", text: ''}]; // es el item necesario para que no se seleccione el primero por defecto.

            if (data.length === 0) {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
            else {
                // Construir options para select2
                data['results'].forEach(function (e, i) {
                    arr.push(e);
                });

                // Rellenar select2 con las opciones
                clasificacionId.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: arr,
                    allowClear: true,
                });
                
                clasificacionId.select2('open');
                // si selected esta en el nuevo conj. de opciones, se selecciona de nuevo.
                //  de lo contrario, queda vacio solito.
                // if (selected !== "")
                //     clasificacionId.val(selected).trigger('change');

            }
        },
        error: function (request, status, error) {

        }
    });
});

$('#new-detalle').on('click', function () {
    detalle_k += 1;
    $('#tabla-detalles').find('tbody')
        .append('<tr class="fila-detalles">' + $('#new-detalle-block').html().replace(/__new__/g, 'new' + detalle_k).replace(/__new_class__/g, '') + '</tr>');

    construirSelect2(detalle_k);
});

// no se usa pero se deja como backup
$(document).on('click', '.delete-detalle', function () {
    $(this).closest('tbody tr').remove();
});

$(document).on('focusout', ':input[id$="precio_unitario"]', function() {
    $('#btn-stock-manager-submit').focus();
});

$("form#_form_traspaso_periodo").on("beforeSubmit", function (e) {
    var form = $(this);
    let url = form.attr("action") + "&submit=true";
    
    $('#' + "$campo_iva_activo").val('');
    
    $.post(
        url,
        form.serialize()
    )
    .done(function (result) {
        $.ajax({
            url: $url_getValorParaPlantilla,
            type: 'get',
            success: function(result) {
                $('#' + "$campo_iva_activo").val(result).trigger('change');
            }
        });
        form.parent().html(result.message);        
        $.pjax.reload({container: "#flash_message_id", async: false});
        $("#modal").modal("hide");
        $("modal-body").html("");
    });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$(document).ready(function() {
    // TODO: 
    construirSelect2('');
})
JS;
    $this->registerJs($script);

} catch (Exception $exception) {
    print $exception;
}
?>
