<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologico */

$this->title = 'Modificar Activos Biológico: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Activos Biológicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="activo-ganado-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
