<?php

use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ActivoBiologicoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activos Biológicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-biologico-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    <div class="btn-toolbar">
        <?php $permisos = [
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-create'),
            'index-clasifi' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-clasificacion-index'),
            'index-especie' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-index'),
        ]; ?>
        <?= $permisos['create'] ? Html::a('Registrar Nuevo Activo Biológico', ['create'], ['class' => 'btn btn-success pull-left']) : null ?>
        <?= $permisos['index-clasifi'] ? Html::a('Ir a Clasificaciones', ['activo-biologico-especie-clasificacion/index'], ['class' => 'btn btn-info pull-right']) : null ?>
        <?= $permisos['index-especie'] ? Html::a('Ir a Especies', ['activo-biologico-especie/index'], ['class' => 'btn btn-info pull-right']) : null ?>
        <?php /*echo Html::button('Traspasar Todos', [
            'class' => 'traspaso btn btn-warning pull-right',
            'title' => 'Traspasar Todos al nuevo Periodo Contable',
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['traspasar-al-periodo']),
            'data-pjax' => '0',
        ]); */ ?>
    </div>
    </p>

    <?php try {
        $template = '';
        //        foreach (['view', 'update', 'delete', 'traspasar_a'] as $_v) // traspaso se implementara en tarea aparte.
        foreach (['view', 'update', 'delete',] as $_v)
            if (PermisosHelpers::getAcceso("activo-biologico-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'especie_id',
                    'value' => 'especie.nombre',
                    'format' => 'raw',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(ActivoBiologicoEspecie::find()->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])->all(), 'nombre', 'nombre'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Filtro por Especie...'],
                ],
                [
                    'attribute' => 'clasificacion_id',
                    'value' => 'clasificacionNombre',
                    'format' => 'raw',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(ActivoBiologicoEspecieClasificacion::find()->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])->all(), 'nombre', 'nombre'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Filtro por Clasificación...'],
                ],
                [
                    'attribute' => 'stock_inicial',
                    'value' => 'formattedStockInicial',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'attribute' => 'stock_actual',
                    'value' => 'formattedStockActual',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'attribute' => 'stock_final',
                    'value' => 'formattedStockFinal',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'attribute' => 'precio_unitario',
                    'value' => 'formattedPrecioUnitario',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'attribute' => 'precio_unitario_actual',
                    'value' => 'formattedPrecioUnitarioActual',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'attribute' => 'total',
                    'value' => 'formattedTotal',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],

                //                [
                //                    'attribute' => 'activo_biologico_padre_id',
                //                    'value' => function ($model) {
                //                        if ($model instanceof ActivoBiologico) {
                //                            return isset($model->activo_biologico_padre_id) ? $model->activo_biologico_padre_id : "";
                //                        }
                //                        return "";
                //                    },
                //                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'update' => function ($url, $model, $index) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>',
                                $url,
                                [
                                    'class' => 'update_btn',
                                    'id' => "update_btn_{$model->id}",
                                    'title' => Yii::t('app', 'Editar'),
                                ]
                            );
                        },
                        'view' => function ($url, $model, $index) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-eye-open"></span>',
                                $url,
                                [
                                    'class' => 'view_btn',
                                    'id' => "view_btn_{$model->id}",
                                    'title' => Yii::t('app', 'Ver'),
                                ]
                            );
                        },
                        'delete' => function ($url, $model, $index) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                $url,
                                [
                                    'class' => 'delete_btn',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => 'Realmente desea borrar?',
                                    ],
                                    'id' => "delete_btn_{$model->id}",
                                    'title' => Yii::t('app', 'Borrar'),
                                ]
                            );
                        },
                        'traspasar_a' => function ($url, $model, $index) {
                            return Html::button("Traspasar al", [
                                'class' => 'btn btn-warning btn-xs traspaso-individual',
                                'title' => Yii::t('app', "Traspasar el activo actual al nuevo periodo contable"),
                                'type' => 'button',
                                'style' => 'margin-left: 5px',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal',
                                'data-url' => Url::to(['traspasar-al-periodo', 'id' => $model->id, 'submit' => false]),
                                'data-pjax' => '0',
                            ]);
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'update') {
                            $url = Url::to(['activo-biologico/update', 'id' => $model->id]);
                            return $url;
                        }
                        if ($action === 'view') {
                            $url = Url::to(['activo-biologico/view', 'id' => $model->id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = Url::to(['activo-biologico/delete', 'id' => $model->id]);
                            return $url;
                        }
                        if ($action === 'traspasar_a') {
                            $url = Url::to(['activo-biologico/traspasar-al-periodo', 'id' => $model->id]);
                            return $url;
                        }
                        return '';
                    },
                    'template' => $template,
                ],
            ],
        ]);

        $script = <<<JS
$(document).on('click', '.traspaso, .traspaso-individual', (function () {
    let boton = $(this);
    
    $.ajax({
        url: boton.data('url'),
        type: 'get',
        data: {},
        success: function(data) {
            let modal = $(boton.data('target'));
            modal.modal();
            $('.modal-body', modal).html(data);
            $('.modal-header', modal).css('background', '#3c8dbc');
            $('.modal-title', modal).html(boton.attr('title'));
        }
    });
}));
JS;
        $this->registerJS($script);

    } catch (Exception $exception) {
        throw $exception;
    } ?>
</div>
