<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion */

$this->title = 'Registrar Nueva Clasificación';
$this->params['breadcrumbs'][] = ['label' => 'Clasificaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-biologico-especie-clasificacion-create">

    <?php try {
        echo $this->render('_form', [
            'model' => $model,
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>

</div>
