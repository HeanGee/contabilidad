<?php

use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ActivoBiologicoEspecieClasificacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clasificaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-biologico-especie-clasificacion-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    <div class="btn-toolbar">
        <?php $permisos = [
            'index-especie' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-index'),
            'index-actbiol' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-clasificacion-create'),
        ]; ?>
        <?= $permisos['create'] ? Html::a('Registrar nueva Clasificación', ['create'], ['class' => 'btn btn-success']) : null ?>
        <?= $permisos['index-especie'] ? Html::a('Ir a Especies', ['activo-biologico-especie/index'], ['class' => 'btn btn-info pull-right']) : null ?>
        <?= $permisos['index-actbiol'] ? Html::a('Ir a A. Biológicos', ['activo-biologico/index'], ['class' => 'btn btn-info pull-right']) : null ?>
    </div>
    </p>

    <?php try {
        $template = "";

        foreach (['view', 'update', 'delete'] as $_v)
            if (\common\helpers\PermisosHelpers::getAcceso("activo-biologico-especie-clasificacion-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'nombre',
                'descripcion',
                [
                    'attribute' => 'activo_biologico_especie_id',
                    'value' => 'activoBiologicoEspecie.nombre',
                    'format' => 'raw',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(ActivoBiologicoEspecie::find()->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])->all(), 'id', 'nombre'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Filtro por Clasificación...'],
                    'label' => "Especie"
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>
</div>
