<?php

use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activo-biologico-especie-clasificacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-activo-biologico-especie-clasificacion-index') ?
            Html::a('Ir a Clasificaciones', ['index'], ['class' => 'btn btn-info']) : null ?>
    </p>

    <?php try {
        echo Html::activeHiddenInput($model, "guardar_cerrar");
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => true,
                    'attributes' => [
                        'nombre' => [
                            'type' => Form::INPUT_TEXT,
                        ],
                        'descripcion' => [
                            'type' => Form::INPUT_TEXT,
                        ],
                        'activo_biologico_especie_id' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => ActivoBiologicoEspecie::getEspeciesLista(),
                                ],
                                'options' => ['placeholder' => 'Seleccione una opción...'],
                                'initValueText' => ActivoBiologicoEspecie::getEspecieNombre(isset($model->activo_biologico_especie_id) ? $model->activo_biologico_especie_id : ''),
                            ],
                            'label' => "Especie"
                        ],
                    ],
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>

    <div class="form-group btn-toolbar">
        <?php
        echo Html::submitButton('Guardar', ['class' => 'btn btn-success guardar pull-left']);
        if (Yii::$app->controller->action->id == 'create')
            echo Html::submitButton('Guardar y cerrar', ['class' => "btn btn-primary guardar-cerrar pull-left"]);
        ?>
    </div>

    <?php

    $script = <<<JS
$(document).on('click', '.guardar-cerrar', function() {
    $('#activobiologicoespecieclasificacion-guardar_cerrar').val('si').trigger('change');
});

$(document).on('click', '.guardar', function() {
    $('#activobiologicoespecieclasificacion-guardar_cerrar').val('no').trigger('change');
});
JS;
    $this->registerJs($script);

    ActiveForm::end(); ?>

</div>
