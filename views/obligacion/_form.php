<?php

use backend\modules\contabilidad\models\Obligacion;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Obligacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obligacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 4,
            'attributes' => [
                'nombre' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Nombre...'],
                ],
                'cantidad_salario_max' => [
                    'type' => Form::INPUT_RAW,
                    'value' => $form->field($model, 'cantidad_salario_max')->widget(NumberControl::className(), [
                        'maskedInputOptions' => [
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'rightAlign' => true,
                            'allowMinus' => false,
                            'alias' => 'numeric',
                            'digits' => 0,
                            'autoUnmask' => true,
                            'unmaskAsNumber' => true,
                        ],
                    ]),
                ],
                'estado' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => Obligacion::getEstados(false),
                    ],
                ],
                'para_factura' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => ['si' => "Si", 'no' => "No"],
                    ],
                ],
            ],
        ]);
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 5,
            'autoGenerateColumns' => false,
            'attributes' => [
                'descripcion' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Descripción...'],
                ],
            ],
        ]);
    } catch (Exception $e) {
        print $e;
    }
    ?>

    <div class="form-group">
        <?php $class = Yii::$app->controller->action->id == 'create' ? 'btn btn-success' : 'btn btn-primary';
        echo Html::submitButton('Guardar', ['class' => $class]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
