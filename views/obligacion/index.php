<?php

use backend\modules\contabilidad\models\Obligacion;
use common\helpers\PermisosHelpers;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ObligacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obligaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obligacion-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-obligacion-create') ?
            Html::a('Crear Nueva Obligación', ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?php try {
        $template = '';
        $template .= PermisosHelpers::getAcceso('contabilidad-obligacion-view') ? '{view} &nbsp&nbsp&nbsp' : '';
        $template .= PermisosHelpers::getAcceso('contabilidad-obligacion-update') ? '{update} &nbsp&nbsp&nbsp' : '';
        $template .= PermisosHelpers::getAcceso('contabilidad-obligacion-delete') ? '{delete} &nbsp&nbsp&nbsp' : '';
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'nombre',
                'descripcion',
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => Obligacion::getEstados(true),
//                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '120px'
                        ]
                    ]),
                ],
                [
                    'attribute' => 'para_factura',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->para_factura == 'si') ? 'success">Si' : 'danger">No') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'para_factura',
                        'data' => ['si' => 'Si', 'no' => 'No'],
//                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '120px',
                            'placeholder' => "Todos",
                            'allowClear' => true,
                        ]
                    ]),
                ],

                [
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center'],
                    'header' => 'Acciones',
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $e) {
        print $e;
    } ?>
</div>
    <!---->
<?php
//$CSS = <<<CSS
//.table tbody tr:hover {
//    background-color: #ebffdd;
//}
//CSS;
//$this->registerCss($CSS);
//?>