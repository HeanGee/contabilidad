<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Obligacion */

$this->title = 'Detalles de la obligación ' . ucfirst($model->nombre);
$this->params['breadcrumbs'][] = ['label' => 'Obligacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obligacion-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-obligacion-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-obligacion-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-obligacion-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-obligacion-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-obligacion-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Obligaciones', ['index'], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Realmente desea BORRAR esta obligación?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'nombre',
                [
                    'attribute' => 'cantidad_salario_max',
                    'value' => ($model->cantidad_salario_max != '') ? number_format($model->cantidad_salario_max, 0, ',', '.') : ''
                ],
                'descripcion',
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => ($model->estado == 'activo') ? "<label class='label label-success'>$model->formattedEstado</label>" : "<label class='label label-danger'>$model->formattedEstado</label>",
                ],
            ],
        ]);
    } catch (Exception $e) {
        print $e;
    } ?>

</div>
