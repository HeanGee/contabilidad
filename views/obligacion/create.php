<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Obligacion */

$this->title = 'Crear Nueva Obligación';
$this->params['breadcrumbs'][] = ['label' => 'Obligacioness', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obligacion-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
