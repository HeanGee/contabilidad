<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Obligacion */

$this->title = 'Modificar Obligacion ' . ucfirst($model->nombre);
$this->params['breadcrumbs'][] = ['label' => 'Obligaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="obligacion-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
