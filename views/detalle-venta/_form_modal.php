<?php

use backend\modules\contabilidad\models\PlanCuenta;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\DetalleVenta */
/* @var $form kartik\form\ActiveForm */
?>

<div class="detalle-venta-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => '_form_modal_detalleventa',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <?php try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [       // 2 column layout
                'plan_cuenta_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true),'id', 'text'),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno',
                        ],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'hint' => 'Seleccione una cuenta',
                    'label' => 'Cuenta'
                ],
                'subtotal' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => 'Monto'
                ],
                'cta_contable' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => ['debe'=>'Debe', 'haber'=>'Haber'],
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno',
                        ],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'hint' => 'Seleccione una cuenta',
                    'label' => 'Debe/Haber'
                ],
                'cta_principal' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                        'pluginEvents' => [
                            'change' => ""
                        ],
                        'data' => ['si' => 'Si', 'no' => 'No'],
                        'initValueText' => 'No',
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                ],
            ]
        ]);
    } catch (Exception $e) {
    } ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['data' => ['disabled-text' => 'Guardando...'], 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $scripts = [];

    $scripts[] = <<<JS
// obtener la id del formulario y establecer el manejador de eventos
$("form#_form_modal_detalleventa").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#detallesventa_grid", async: false});
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
            $(':button.btn-simular').click();
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$('#modal').on('shown.bs.modal', function () {
    if ($('#detalleventa-plan_cuenta_id').val().length === 0)
        $('#detalleventa-plan_cuenta_id').select2('open');
});
JS;

    foreach($scripts as $script) $this->registerJs($script);
    ?>
</div>
