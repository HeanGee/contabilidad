<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\DetalleVentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detalle Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalle-venta-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Detalle Venta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php

    $template = '';
    foreach(['view', 'update', 'delete'] as $_v)
        if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-detalle-venta-{$_v}"))
            $template .= "&nbsp{{$_v}}";
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'factura_venta_id',
                'iva_id',
                'subtotal',
                'plan_cuenta_id',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $e) {
        throw $e;
    }?>
</div>
