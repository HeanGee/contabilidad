<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 05/07/2018
 * Time: 14:16
 */

use backend\modules\contabilidad\models\Obligacion;
use backend\modules\contabilidad\models\PlantillaCompraventaObligacion;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $dataProvider ActiveDataProvider */
?>

<?php try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'id' => 'plantilla-compraventa-obligacion-grid',
        'pjax' => false, // es necesario sii se va a usar en javascript, para referenciarle mediante id por ej.
//        'pjaxSettings' => [
//            'options' => [
//                'id' => 'empresa_obligaciones_pjax_grid'
//            ],
//            'loadingCssClass' => false,
//        ],
        'panel' => [
            'type' => 'primary',
            'heading' => 'Obligaciones',
            'after' => '<em>* Para confirmar los datos de las obligaciones se debe guardar la plantilla.</em>'
        ],
        'striped' => true,
        'hover' => true,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'headerOptions' => [
                    'class' => 'kartik-sheet-style',
                    'onchange' => '
                        $.ajax({
                            type: "POST",
                            url: "index.php?r=contabilidad/plantilla-compraventa/guardar-obligaciones-en-session&all=true",
                            data: {seleccionados: $("#plantilla-compraventa-obligacion-grid").yiiGridView("getSelectedRows")},
                            success: function (data) {
                            }
                        });'
                ],
                'checkboxOptions' => function ($model) {
                    $skey = 'cont_plantilla_en_edicion';
                    $checked = false;
                    if (Yii::$app->session->has($skey)) {
                        $plantilla_obligs = PlantillaCompraventaObligacion::findAll(['plantilla_compraventa_id' => \Yii::$app->session->get($skey)]);
                        foreach ($plantilla_obligs as $plantilla_oblig) {
                            if ($model instanceof Obligacion && $model->id == $plantilla_oblig->obligacion_id) {
                                $checked = true;
                            }
                        }
                    } else {
                        if (Yii::$app->controller->action->id == 'create' && Yii::$app->session->has('obligaciones_seleccionados')) {
                            $obligs_seleccs = Yii::$app->session->get('obligaciones_seleccionados');
                            foreach ($obligs_seleccs as $oblig_id) {
                                if ($model instanceof Obligacion && $model->id == $oblig_id)
                                    $checked = true;
                            }
                        }
                    }

                    return [
                        'onchange' => '
                            $.ajax({
                                type: "POST",
                                url: "index.php?r=contabilidad/plantilla-compraventa/guardar-obligaciones-en-session&checked=" + this.checked,
                                data: {id_registro_seleccionado: ' . $model->id . '},
                                success: function (data) {
                                }
                            });',
                        'checked' => $checked
                    ];
                },
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'nombre',
                'label' => 'Nombre',
                'value' => 'nombre',
                'hidden' => false,
            ],
            'descripcion',
        ],
        'toolbar' => []
    ]);
} catch (Exception $e) {
    print $e;
}
?>