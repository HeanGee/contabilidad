<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\TipoDocumentoSet */

$this->title = Yii::t('app', 'Editar Tipo Documento Set: ' . $model->nombre, [
    'nameAttribute' => '' . $model->nombre,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipos Documentos SET'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Editar');
?>
<div class="tipo-documento-set-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
