<?php

use backend\modules\contabilidad\controllers\TipoDocumentoSetController;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\TipoDocumentoSetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tipos Documento SET');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-documento-set-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-set-create') ?
            Html::a(Yii::t('app', 'Nuevo Tipo Documento SET'), ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?php
    $template = '';
    foreach (['view', 'update', 'delete'] as $_v)
        if (TipoDocumentoSetController::getAcceso("{$_v}"))
            $template .= "&nbsp&nbsp&nbsp{{$_v}}";

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'value' => 'id',
                    'width' => '60px',
                ],
                'nombre',
                'detalle',
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'label' => 'Estado',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => [
                            'activo' => 'Activo',
                            'incativo' => 'Inactivo',
                            'todos' => 'Todos'
                        ],
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template
                ],
            ],
        ]);
    } catch (Exception $e) {

    } ?>
    <?php Pjax::end(); ?>
</div>
