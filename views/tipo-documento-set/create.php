<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\TipoDocumentoSet */

$this->title = Yii::t('app', 'Nuevo Tipo Documento SET');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Documento Sets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-documento-set-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
