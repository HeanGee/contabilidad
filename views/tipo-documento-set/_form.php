<?php

use backend\modules\contabilidad\controllers\TipoDocumentoSetController;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\TipoDocumentoSet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-documento-set-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'detalle')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= TipoDocumentoSetController::getAcceso('create') ? Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
