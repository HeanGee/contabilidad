<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\TipoDocumentoSet */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipos Documentos SET'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-documento-set-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-set-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-set-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-set-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-set-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-set-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a T. de Doc. SET', ['index'/*, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a(Yii::t('app', 'Editar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a(Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php
    echo DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'enableEditMode' => false,
        'fadeDelay' => true,
        'panel' => [
            'heading' => 'Datos',
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
//            'id',
            'nombre',
            'detalle',
            [
                'label' => 'Estado',
                'format' => 'raw',
                'value' => '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>',
            ],
        ],
    ]);

    $style = <<<CSS
table.detail-view th {
        width: 25%;
}
CSS;
    $this->registerCss($style);
    ?>

</div>
