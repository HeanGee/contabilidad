<?php

use yii\helpers\Html;

use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;

use backend\modules\contabilidad\models\Entidad;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Timbrado */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="timbrado-form">

        <?php
        $form = ActiveForm::begin();

        try {
            foreach (['fecha_inicio', 'fecha_fin'] as $_fecha) {
                if (!empty($model->$_fecha))
                    ${$_fecha} = new DateTime($model->$_fecha);
            }

            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'attributes' => [
                            'nro_timbrado' => [
                                'type' => Form::INPUT_TEXT,
                                'options' => ['placeholder ' => 'Numero de timbrado...']
                            ],
                            'ruc' => [
                                'label' => 'R.U.C.',
                                'type' => Form::INPUT_TEXT,
                            ],
                            'nombre_entidad' => [
                                'label' => 'Entidad',
                                'type' => Form::INPUT_TEXT,
                                'options' => [
                                    'readonly' => true
                                ]
                            ],
                            'fecha_inicio' => [
                                'type' => Form::INPUT_RAW,
                                'placeholder' => 'Fecha inicio...',
                                'value' => $form->field($model, 'fecha_inicio')->widget(DatePicker::className(), [
                                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                    'language' => 'es',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd-mm-yyyy'
                                    ],
                                    'options' => [
                                        'value' => !empty($fecha_inicio) ? $fecha_inicio->format('d-m-Y') : (Yii::$app->controller->action->id == 'create' ? date('d-m-Y') : null),
                                    ]
                                ])
                            ],
                            'fecha_fin' => [
                                'type' => Form::INPUT_RAW,
                                'placeholder' => 'Fecha fin...',
                                'value' => $form->field($model, 'fecha_fin')->widget(DatePicker::className(), [
                                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                    'language' => 'es',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'dd-mm-yyyy'
                                    ],
                                    'options' => [
                                        'value' => !empty($fecha_fin) ? $fecha_fin->format('d-m-Y') : (Yii::$app->controller->action->id == 'create' ? date('d-m-Y', strtotime('+1 years')) : null),
                                    ]
                                ])
                            ]
                        ]
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            /* TODO: manejar */
        }
        ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php echo $this->render('_form_detalle', ['model' => $model]);
        ActiveForm::end();
        ?>
    </div>

<?php
$url_getRazonSocialByRuc = Json::htmlEncode(\Yii::t('app', Url::to(['timbrado/get-razon-social-by-ruc'])));

$script = <<<JS
$(document).on('click', '.tiene_modal', (function () {
    var boton = $(this);
    var title = boton.data('title');
    $.get(
        boton.data('url'),
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));
$(document).on('click', '.ajaxDelete', function (e) {
    var delete_btn = $(this);
    krajeeDialog.confirm("¿Está seguro de eliminar este Detalle?", function (result) {
        if (result) {
            $.post(
                delete_btn.attr('delete-url')
            )
                .done(function (result) {
                    $.pjax.reload({container: "#detalle_timbrado_gridview", async: false});
                    $.pjax.reload({container: "#flash_message_id", async: false});
                });
        }
    });
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$('#timbrado-ruc').on('change', function () {
    var url_getRazonSocialByRuc = $url_getRazonSocialByRuc;
    $.ajax({
        url: url_getRazonSocialByRuc,
        type: 'post',
        data: {
            ruc: $(this).val(),
        },
        success: function (data) {
            $('#timbrado-nombre_entidad').val(data).trigger('change');
        }
    });
});
JS;

$this->registerJs($script);

$css = "
        .btn-sin-estilo {
            border: 0;
            padding: 0 1px;
            background-color: inherit;
            color: #337ab7;
        }
        .btn-sin-estilo:hover, 
        .btn-sin-estilo:active, 
        .btn-sin-estilo:focus { color: #72afd2; }
        ";
$this->registerCss($css);
?>