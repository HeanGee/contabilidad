<?php

use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Timbrado */
/* @var $model_detalle backend\modules\contabilidad\models\TimbradoDetalle */
/* @var $form ActiveForm */
?>

<div class="timbrado-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php
    $form = ActiveForm::begin([
        'id' => '_modalform_facturacompra_addtimbrado',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]);

    try {
        $attributes = [
            'nro_timbrado' => [
                'type' => Form::INPUT_RAW,
                'value' => $form->field($model, 'nro_timbrado')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9', 'clientOptions' => ['repeat' => 8, 'greedy' => false]
                ])
            ],
            'fecha_inicio' => [
                'type' => Form::INPUT_RAW,
                'value' => $form->field($model, 'fecha_inicio')->widget(MaskedInput::className(), [
                    'name' => 'fecha_inicio',
                    'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                ]),
            ],
            'fecha_fin' => [
                'type' => Form::INPUT_RAW,
                'value' => $form->field($model, 'fecha_fin')->widget(MaskedInput::className(), [
                    'name' => 'fecha_fin',
                    'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                ]),
            ],
        ];

        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => $attributes
        ]);

        $attributes = [
            'prefijo' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => MaskedInput::class,
                'options' => [
                    'name' => 'prefijo',
                    'mask' => '999-999',
                    'options' => [
                        'class' => 'form-control',
                        'disabled' => true
                    ]
                ],
                'label' => 'Prefijo'
            ],
            'nro_inicio' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder ' => 'Inicio...'],
                'label' => 'Numero inicio'
            ],
            'nro_fin' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder ' => 'Fin...'],
                'label' => 'Numero fin'
            ],
        ];
        echo Form::widget([
            'model' => $model_detalle,
            'form' => $form,
            'columns' => 3,
            'attributes' => $attributes
        ]);
    } catch (\Exception $e) {
        echo $e;
    }

    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $url_completarfechas = Json::htmlEncode(\Yii::t('app', Url::to(['timbrado/get-fechas-timbrado'])));

    $scripts = [];
    $scripts[] = <<<JS
    $('#timbrado2-nro_timbrado').focus();

// obtener la id del formulario y establecer el manejador de eventos
$("#_modalform_facturacompra_addtimbrado").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
    .done(function (result) {
        $.pjax.reload({container: "#flash_message_id", async: false});
        $("#modal").modal("hide");
        if (result!==false){
            //Verificar que el número de factura esté en el rango del timbrado cargado
            var nro_factura = parseInt($('#compra-nro_factura').val().substring(8, 15));
            if( nro_factura >= parseInt(result['nro_inicio']) && nro_factura <= parseInt(result['nro_fin']) ){
                $('#compra-timbrado').val(result['timbrado']);
                // $('#compra-button-add_id')[0].style.display='none';
                // $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": result['id'], "text": result['timbrado']}});
                let timID = result['id'],
                    timNo = result['timbrado'];
                localStorage.setItem('recentlyAddedTim', [timID, timNo]);
            }
        }
        $('#compra-fecha_emision').trigger('change');
    });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$('#modal').on('shown.bs.modal', function () {
    $('input[name="Timbrado2[nro_timbrado]"]').focus();
});

function completarFechas() {
    $.ajax({
        url: $url_completarfechas,
        type: 'post',
        data: {
            nro_timbrado: $('#timbrado2-nro_timbrado').val(),
        },
        success: function (data) {
            if (data !== '') {
                let fechas = data.split('|');
                
                const fechaInicioField = $('#timbrado2-fecha_inicio');
                const fechaFinField = $('#timbrado2-fecha_fin');

                if (fechas[0]) {
                    fechaInicioField.val(fechas[0]).trigger('change');
                    $('#timbrado2-fecha_inicio-kvdate').css('pointer-events', 'none');
                }
                if (fechas[1]) { 
                    fechaFinField.val(fechas[1]).trigger('change');
                    $('#timbrado2-fecha_fin-kvdate').css('pointer-events', 'none');
                }
                $('#timbradodetalle-nro_inicio').focus();
                $(".datepicker").hide();
            } else {
                $('#timbrado2-fecha_inicio-kvdate').css('pointer-events', '');
                $('#timbrado2-fecha_fin-kvdate').css('pointer-events', '');
            }
        }
    });
}

$("#timbrado2-nro_timbrado").on("change", function (e) {
    completarFechas();
});

$('#timbrado2-fecha_inicio, #timbrado2-fecha_fin').on('focus', function () {
    completarFechas();
});
JS;

    foreach ($scripts as $script) $this->registerJs($script, View::POS_READY); ?>
</div>