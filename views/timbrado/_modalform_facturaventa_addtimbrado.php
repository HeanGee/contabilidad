<?php

use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model \backend\modules\contabilidad\controllers\Timbrado2 */
/* @var $model_detalle backend\modules\contabilidad\models\TimbradoDetalle */
/* @var $form ActiveForm */
?>

<div class="timbrado-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php
    $form = ActiveForm::begin([
        'id' => '_modalform_facturaventa_addtimbrado',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]);

    try {
        $attributes = [
            'nro_timbrado' => [
                'type' => Form::INPUT_RAW,
                'value' => $form->field($model, 'nro_timbrado')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9', 'clientOptions' => ['repeat' => 8, 'greedy' => false]
                ])
            ],
            'fecha_inicio' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DatePicker::className(),
                'options' => [
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'language' => 'es',
//                    'hashVarLoadPosition' => View::POS_READY,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy'
                    ],
                    'pluginEvents' => [
                        'focusout' => 'function () { completarFechas(); }',
                    ]
                ],
            ],
            'fecha_fin' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DatePicker::className(),
                'options' => [
//                    'hashVarLoadPosition' => View::POS_READY,
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'language' => 'es',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy'
                    ],
                    'pluginEvents' => [
                        'focusout' => 'function () { completarFechas(); }',
                    ]
                ],
            ],
//            'fecha_inicio' => [
//                'type' => Form::INPUT_WIDGET,
//                'widgetClass' => DateControl::class,
//                'options' => [
//                    'type' => DateControl::FORMAT_DATE,
//                    'ajaxConversion' => false,
//                    'language' => 'es',
//                    'widgetOptions' => [
//                        'pluginOptions' => [
//                            'autoclose' => true,
//                            'format' => 'dd-MM-yyyy',
//                            'todayHighlight' => true,
//                            'weekStart' => '0',
//                            'enableAjaxValidation' => false,
//                        ],
//                        'pluginEvents' => [
//                            'focusout' => 'function () { completarFechas(); }'
//                        ]
//                    ],
//
//                ]
//            ],
//            'fecha_fin' => [
//                'type' => Form::INPUT_WIDGET,
//                'widgetClass' => DateControl::class,
//                'options' => [
//                    'type' => DateControl::FORMAT_DATE,
//                    'ajaxConversion' => false,
//                    'language' => 'es',
//                    'widgetOptions' => [
//                        'pluginOptions' => [
//                            'autoclose' => true,
//                            'format' => 'dd-MM-yyyy',
//                            'todayHighlight' => true,
//                            'weekStart' => '0',
//                            'enableAjaxValidation' => false,
//                        ],
//                        'pluginEvents' => [
//                            'focusout' => 'function () { completarFechas(); }'
//                        ]
//                    ],
//
//                ]
//            ],
        ];
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => $attributes
        ]);

        $attributes = [
            'prefijo' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => MaskedInput::class,
                'options' => [
                    'name' => 'prefijo',
                    'mask' => '999-999',
                    'options' => [
                        'class' => 'form-control',
//                        'readonly' => 'true',
                    ],
                ],
                'label' => 'Prefijo'
            ],
            'nro_inicio' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder ' => 'Inicio...'],
                'label' => 'Numero inicio'
            ],
            'nro_fin' => [
                'type' => Form::INPUT_TEXT,
                'options' => ['placeholder ' => 'Fin...'],
                'label' => 'Numero fin'
            ],
        ];
        echo Form::widget([
            'model' => $model_detalle,
            'form' => $form,
            'columns' => 3,
            'attributes' => $attributes
        ]);
    } catch (\Exception $e) {
        echo $e;
    }

    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $url_completarfechas = Json::htmlEncode(\Yii::t('app', Url::to(['timbrado/get-fechas-timbrado'])));

    $JS_FUNCTIONS = <<<JS
function completarFechas() {
    $.ajax({
        url: $url_completarfechas,
        type: 'post',
        data: {
            nro_timbrado: $('#timbrado2-nro_timbrado').val(),
        },
        success: function (data) {
            if (data !== '') {
                let fechas = data.split('|');
                $('#timbrado2-fecha_inicio').val(fechas[0]).trigger('change');
                $('#timbrado2-fecha_fin').val(fechas[1]).trigger('change');
                $('#timbrado2-fecha_inicio-kvdate').css('pointer-events', 'none');
                $('#timbrado2-fecha_fin-kvdate').css('pointer-events', 'none');
                $('#timbradodetalle-nro_inicio').focus();
                $(".datepicker").hide();
            } else {
                // $('#timbrado2-fecha_inicio').val('').trigger('change');
                // $('#timbrado2-fecha_fin').val('').trigger('change');
                $('#timbrado2-fecha_inicio-kvdate').css('pointer-events', '');
                $('#timbrado2-fecha_fin-kvdate').css('pointer-events', '');
            }
        }
    });
}
JS;

    $JS_DOCREADYS = <<<JS

JS;

    $JS_EVENTS = <<<JS
// obtener la id del formulario y establecer el manejador de eventos
$("#_modalform_facturaventa_addtimbrado").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");

            // recargar select2 de factura. prefijos se cargan dependiendo del tipo de documento.
            $('#venta-tipo_documento_id').trigger('change');
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$('#modal').on('shown.bs.modal', function () {
    $('input[id="timbrado2-nro_timbrado"]').focus();
});

$('#timbrado2-nro_timbrado').on('change', function () {
    completarFechas();
});

$('#timbrado2-fecha_inicio, #timbrado2-fecha_fin').on('focus', function () {
    completarFechas();
    // if ($(this).parent().css("pointer-events") === "none") {
    //     $(".datepicker").hide();
    //     $('#timbradodetalle-nro_inicio').focus();
    // }
});
JS;

    $this->registerJs($JS_FUNCTIONS);
    $this->registerJs($JS_DOCREADYS);
    $this->registerJs($JS_EVENTS); ?>
</div>
