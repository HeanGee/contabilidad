<?php

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $model \backend\modules\contabilidad\models\Timbrado */
/* @var $this yii\web\View */
?>

<?php Pjax::begin(['id' => 'detalle_timbrado_gridview']);

$columns = [
    [
        'class' => SerialColumn::className(),
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Tipo Documento SET',
        'value' => 'tipoDocumentoSet.nombre',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Prefijo',
        'value' => 'prefijo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Nro Inicio',
        'value' => 'nro_inicio',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Nro Fin',
        'value' => 'nro_fin',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Nro Actual',
        'value' => 'nro_actual',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
];

$template = '{update} &nbsp {delete}';
$buttons = [
    'delete' => function ($url, $model, $index) {
        return Html::a(
            '<span class="glyphicon glyphicon-trash"></span>',
            false,
            [
                'class' => 'ajaxDelete',
                'delete-url' => $url,
                'id' => $index,
                'title' => Yii::t('app', 'Borrar')
            ]
        );
    },
    'update' => function ($url, $model) {
        return Html::a(
            '<span class="glyphicon glyphicon-pencil"></span>',
            false,
            [
                'title' => 'Editar detalle',
                'class' => 'tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => $url,
                'data-pjax' => '0',
                'data-title' => 'Editar detalle'
            ]
        );
    }
];

array_push($columns, [
    'class' => ActionColumn::class,
    'template' => $template,
    'buttons' => $buttons,
    'urlCreator' => function ($action, $model, $key, $index) {
        if ($action === 'delete') {
            $url = Url::to(['timbrado-detalle/delete-detalle', 'index' => $index]);
            return $url;
        } elseif ($action === 'update') {
            $url = Url::to(['timbrado-detalle/edit-detalle', 'index' => $index]);
            return $url;
        }
        return '';
    },
    'contentOptions' => ['style' => 'font-size: 90%;'],
    'headerOptions' => ['style' => 'font-size: 90%;'],
]);

$skey = 'cont_detalletimbrado-provider';
$dataProvider = Yii::$app->session->has($skey) ? Yii::$app->session->get($skey) : new ArrayDataProvider([
    'allModels' => [],
    'pagination' => false,
]);
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'hover' => true,
        'id' => 'grid',
        'panel' => ['type' => 'primary', 'heading' => 'Detalles', 'footer' => false,],
        'toolbar' => [
            'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'id' => 'boton_add_detalle',
                'type' => 'button',
                'title' => 'Agregar Cuenta. Utilice el signo \'+\' para acceder.',
                'class' => 'btn btn-success tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['timbrado-detalle/add-detalle']),
                'data-pjax' => '0',
                'data-title' => 'Agregar cuenta'
            ]),
        ]
    ]);
} catch (Exception $e) {
    echo $e;
}
?>

<?php Pjax::end(); ?>