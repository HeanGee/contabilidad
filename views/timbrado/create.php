<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Timbrado */

$this->title = 'Cargar Timbrado';
$this->params['breadcrumbs'][] = ['label' => 'Timbrados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timbrado-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
