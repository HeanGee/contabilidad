<?php

use common\helpers\PermisosHelpers;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Timbrado */

$this->title = 'Modificar Timbrado';
$this->params['breadcrumbs'][] = ['label' => 'Timbrados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="timbrado-update">

    <?= PermisosHelpers::getAcceso('contabilidad-timbrado-unificar-talonario') ?
        Html::beginTag('div', ['class' => ' btn-toolbar']) .
        Html::a('Unificar Talonario',
            ['unificar-talonario', 'id' => Yii::$app->request->getQueryParam('id')],
            [
                'class' => "btn btn-warning pull-right",
                'data' => [
                    'confirm' => "Está seguro de unificar los talonarios?",
                    'method' => "post",
                ]
            ]) .
        Html::endTag('div') : null ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
