<?php

use backend\modules\contabilidad\models\TimbradoDetalle;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Timbrado */

$this->title = 'Detalle de Timbrado';
$this->params['breadcrumbs'][] = ['label' => 'Timbrados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timbrado-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-timbrado-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-timbrado-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-timbrado-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-timbrado-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-timbrado-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Timbrados', ['index'/*, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quiere eliminar?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Timbrado',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'nro_timbrado',
                [
                    'attribute' => 'entidad_id',
                    'value' => $model->entidad->razon_social
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'value' => $model->fecha_inicio != null ? date('d-m-Y', strtotime($model->fecha_inicio)) : null
                ],
                [
                    'attribute' => 'fecha_fin',
                    'value' => $model->fecha_fin != null ? date('d-m-Y', strtotime($model->fecha_fin)) : null
                ]
            ]
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => TimbradoDetalle::find()->where(['timbrado_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => 'Detalles',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'columns' => [
                'id',
                [
                    'attribute' => 'tipo_documento_set_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return "{$model->tipoDocumentoSet->nombre} <strong>(id: {$model->tipo_documento_set_id})</strong>";
                    },
                ],
                'prefijo',
                'nro_inicio',
                'nro_fin',
                'nro_actual'
            ],
        ]);

    } catch (\Exception $e) {
        print $e;
        /* TODO: manejar */
    }
    ?>

</div>
