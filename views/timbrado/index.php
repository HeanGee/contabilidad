<?php

use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\TimbradoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Timbrados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timbrado-index">

    <?php Pjax::begin(); ?>

    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-timbrado-create') ?
            Html::a('Cargar Timbrado', ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?php
    try {
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (PermisosHelpers::getAcceso("contabilidad-timbrado-{$_v}"))
                $template .= "&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'value' => 'id',
                    'width' => '60px',
                ],
                'nro_timbrado',
                [
                    'attribute' => 'entidad_tmp',
                    'value' => 'entidad.razon_social'
                ],
                [
                    'attribute' => 'fecha_inicio',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha_inicio',
                        'language' => 'es',
                        'pickerButton' => false,
//                        'options' => [
//                            'style' => 'width:90px',
//                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'weekStart' => 0,
                        ],
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model->fecha_inicio != null ? date('d-m-Y', strtotime($model->fecha_inicio)) : null;
                    }
                ],
                [
                    'attribute' => 'fecha_fin',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha_fin',
                        'language' => 'es',
                        'pickerButton' => false,
//                        'options' => [
//                            'style' => 'width:90px',
//                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'weekStart' => 0,
                        ],
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model->fecha_fin != null ? date('d-m-Y', strtotime($model->fecha_fin)) : null;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [],
                    'template' => $template,
                ]
            ],
        ]);

    } catch (\Exception $e) {
        echo $e;
        \common\helpers\FlashMessageHelpsers::createWarningMessage($e->getMessage());
    }

    Pjax::end();
    ?>
</div>
