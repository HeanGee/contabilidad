<?php

use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\TraspasoPeriodo;

/* @var $this yii\web\View */
/* @var $model TraspasoPeriodo */

$debes = $haberes = [];
foreach ($model->asientos as $asiento) {
    if ($asiento->monto_haber != 0) {
        $_asiento = new AsientoDetalle();
        $_asiento->setAttributes($asiento->attributes);
        $_asiento->monto_debe = $_asiento->monto_haber;
        $_asiento->monto_haber = 0;
        $debes[] = $_asiento;
    } else {
        $_asiento = new AsientoDetalle();
        $_asiento->setAttributes($asiento->attributes);
        $_asiento->monto_haber = $_asiento->monto_debe;
        $_asiento->monto_debe = 0;
        $haberes[] = $_asiento;
    }
}
$asientos = array_merge($debes, $haberes)
?>

<table class="table table-condensed" id="table-asiento-apertura">
    <tr>
        <th>Código</th>
        <th>Cuenta</th>
        <th style="text-align: center">Debe</th>
        <th style="text-align: center">Haber</th>
    </tr>

    <?php foreach ($asientos as $asiento) { ?>
        <tr>
            <td style="width: 20%"><?= $asiento->cuenta->cod_completo ?></td>
            <td style="width: 40%"><?= $asiento->cuenta->nombre ?></td>
            <td style="width: 20%"><?= $asiento->monto_debe ?></td>
            <td style="width: 20%"><?= $asiento->monto_haber ?></td>
        </tr>
    <?php } ?>
</table>
