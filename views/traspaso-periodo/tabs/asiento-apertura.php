<?php

use backend\modules\contabilidad\models\TraspasoPeriodo;

/* @var $this yii\web\View */
/* @var $model TraspasoPeriodo */
?>

<table class="table table-condensed" id="table-asiento-cierre">
    <tr>
        <th>Código</th>
        <th>Cuenta</th>
        <th style="text-align: center">Debe</th>
        <th style="text-align: center">Haber</th>
    </tr>

    <?php foreach ($model->asientos as $asiento) { ?>
        <tr>
            <td style="width: 20%"><?= $asiento->cuenta->cod_completo ?></td>
            <td style="width: 40%"><?= $asiento->cuenta->nombre ?></td>
            <td style="width: 20%"><?= $asiento->monto_debe ?></td>
            <td style="width: 20%"><?= $asiento->monto_haber ?></td>
        </tr>
    <?php } ?>
</table>
