<?php

use backend\modules\contabilidad\models\TraspasoPeriodo;

/* @var $this yii\web\View */
/* @var $model TraspasoPeriodo */

$this->title = 'Traspaso de Periodo';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('form', ['model' => $model]); ?>

<?= $this->render('preview-asiento', ['model' => $model]); ?>

