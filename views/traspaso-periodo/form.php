<?php

use backend\modules\contabilidad\models\TraspasoPeriodo;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\checkbox\CheckboxX;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model TraspasoPeriodo */

$this->title = 'Traspaso de Periodo';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['id' => 'traspaso-periodo-form']); ?>

<legend class="text-info">
    <small>Datos</small>
</legend>

<?php
try {
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'autoGenerateColumns' => false,
                'columns' => 12,
                'attributes' => [
                    'asiento_cierre' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'asiento_cierre')->widget(CheckboxX::className(), [
                            'pluginOptions' => ['size' => 'lg', 'threeState' => false],
                        ]),
                    ],
                    'asiento_apertura' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'asiento_apertura')->widget(CheckboxX::className(), [
                            'pluginOptions' => ['size' => 'lg', 'threeState' => false]
                        ]),
                    ],
                    'siguiente_periodo_id' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'siguiente_periodo_id')->widget(Select2::className(), [
                            'options' => ['placeholder' => ''],
                            'id' => 'periodo_anterior_select2',
                            'pluginOptions' => [
                                'data' => $model->getPeriodoActual()->getNextPeriodos(),
                                'allowClear' => true,
                            ],
                            'initValueText' => ($model->siguiente_periodo_id != '') ? "{$model->getSiguientePeriodo()->anho} - {$model->getSiguientePeriodo()->empresa->razon_social}" : '',
                        ]),
                    ],
                ]
            ],
        ],
    ]);

    echo '<div class="form-group">';
    echo Html::submitButton('Generar', ['class' => 'btn btn-success']);
    echo '</div>';
} catch (Exception $exception) {
    echo $exception;
}
?>

<?php ActiveForm::end(); ?>

<?php ob_start(); // output buffer the javascript to register later ?>
<script>
    $(document).on('change', '#traspasoperiodo-asiento_apertura', function () {
        $('form[id^="traspaso-periodo-form"]').yiiActiveForm('validateAttribute', 'traspasoperiodo-siguiente_periodo_id');
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
