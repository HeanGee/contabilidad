<?php

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\TraspasoPeriodo;
use kartik\tabs\TabsX;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model TraspasoPeriodo */
?>

<legend class="text-info">
    <small>Vista previa Asiento</small>
</legend>

<?php Pjax::begin(['id' => 'preview-asiento-pjax']); ?>

<?php
try {
    $items = [
        [
            'label' => '<i class="fa fa-list"></i> Asiento de Cierre',
            'content' => $this->render('tabs/asiento-cierre', ['model' => $model]),
            'active' => true,
            'linkOptions' => ['id' => 'tab-asiento-cierre'],
        ],
        [
            'label' => '<i class="fa fa-list"></i> Asiento de Apertura',
            'content' => $this->render('tabs/asiento-apertura', ['model' => $model]),
            'active' => false,
            'linkOptions' => ['id' => 'tab-asiento-apertura'],
        ],
    ];

    echo TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_ABOVE,
        'bordered' => false,
        'encodeLabels' => false
    ]);
} catch (Exception $exception) {
    echo HtmlHelpers::BootstrapDangerPanel('Error en el pjax de preview-asiento', $exception->getMessage(), false, true);
}
?>

<?php Pjax::end(); ?>

<?php ob_start(); // output buffer the javascript to register later ?>
<script>
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $tablas = $('.table-condensed');
    $($tablas[0].getElementsByTagName('tr')).each(function () {
        $columnas = $(this)[0].getElementsByTagName('td');
        if (typeof $columnas[0] !== 'undefined') {
            $($columnas[2]).css('text-align', 'right');
            $($columnas[3]).css('text-align', 'right');
            if ($columnas.length > 3 && $columnas[3].textContent !== '0') {
                $($columnas[1]).css('text-align', 'right');
            }
            $($columnas[2]).text(numberWithCommas($($columnas[2]).text()));
            $($columnas[3]).text(numberWithCommas($($columnas[3]).text()));
        }
    });
    $($tablas[1].getElementsByTagName('tr')).each(function () {
        $columnas = $(this)[0].getElementsByTagName('td');
        if (typeof $columnas[0] !== 'undefined') {
            $($columnas[2]).css('text-align', 'right');
            $($columnas[3]).css('text-align', 'right');
            if ($columnas.length > 3 && $columnas[3].textContent !== '0') {
                $($columnas[1]).css('text-align', 'right');
            }
            $($columnas[2]).text(numberWithCommas($($columnas[2]).text()));
            $($columnas[3]).text(numberWithCommas($($columnas[3]).text()));
        }
    });
</script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
