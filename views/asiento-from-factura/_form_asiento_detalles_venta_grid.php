<?php

use common\helpers\FlashMessageHelpsers;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $totaldebe string */
/* @var $totalhaber string */
/* @var $totalCdebe string */
/* @var $totalChaber string */
/* @var $asientoIndep array */
?>
<?php Pjax::begin(['id' => 'grid_asientos_detalle_venta']); ?>
<?php

$columns = [
    [
        'class' => SerialColumn::className(),
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],

    [
        'class' => DataColumn::class,
        'label' => 'Código cuenta',
        'value' => 'cuenta.cod_completo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'class' => DataColumn::class,
        'label' => 'Cuenta',
        'value' => 'cuenta.nombre',
        'pageSummaryOptions' => [
            'data-colspan-dir' => 'ltr',
            'colspan' => 1,
            'style' => 'text-align: right;',
        ],
        'pageSummary' => function ($summary, $data, $widget) {
            return "Totales";
        },
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'class' => DataColumn::class,
        'label' => 'Debe',
        'value' => 'monto_debe',
        'format' => 'decimal',
        'pageSummary' => true,
        'pageSummaryFunc' => GridView::F_SUM,
        'pageSummaryOptions' => [
            'append' => '',
            'prepend' => '',
        ],
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'class' => DataColumn::class,
        'label' => 'Haber',
        'value' => 'monto_haber',
        'format' => 'decimal',
        'pageSummary' => true,
        'pageSummaryFunc' => GridView::F_SUM,
        'pageSummaryOptions' => [
            'append' => '',
            'prepend' => '',
        ],
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
];

//$template = '{update} &nbsp {delete}';
$template = '';
$buttons = [];

//array_push($columns, [
//    'class' => ActionColumn::class,
//    'template' => $template,
//    'buttons' => $buttons,
//    'contentOptions' => ['style' => 'font-size: 90%;'],
//    'headerOptions' => ['style' => 'font-size: 90%;'],
//]);

try {
    $sessionKey1 = 'cont_asiento_from_factura_venta_detalle-provider';
    $sessionKey2 = 'cont_asiento_from_factura_costo_venta_detalle-provider';
    $dataProvider1 = Yii::$app->getSession()->has($sessionKey1) ? Yii::$app->getSession()->get($sessionKey1) : new ArrayDataProvider([
        'allModels' => [],
        'pagination' => false,
    ]);
    $dataProvider2 = Yii::$app->getSession()->has($sessionKey2) ? Yii::$app->getSession()->get($sessionKey2) : new ArrayDataProvider([
        'allModels' => [],
        'pagination' => false,
    ]);

    if ($dataProvider1->allModels != null) {
        echo GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider1,
            'columns' => $columns,
            'panel' => ['type' => 'primary', 'heading' => 'Asiento de Facturas', 'footer' => false,],
            'toolbar' => [],
            'hover' => true,
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
        ]);
    }

    if ($dataProvider2->allModels != null) {
        echo GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider2,
            'columns' => $columns,
            'panel' => ['type' => 'primary', 'heading' => 'Asientos por costo de venta', 'footer' => false,],
            'toolbar' => [],
            'hover' => true,
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
        ]);
    }

    if (!empty($asientoIndep)) {
        echo $this->render('_form-asientos-independientes', [
            'asientoIndep' => $asientoIndep,
            'form' => $form,
        ]);
    }

    if (isset($asientoRecibo) && !empty($asientoRecibo)) {
        $dataProvider = new ArrayDataProvider(['allModels' => $asientoRecibo, 'pagination' => false]);
        echo GridView::widget([
            'id' => 'grid-asiento-recibo',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'success',
                'heading' => 'Asiento de recibos',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
            'columns' => $columns,
        ]);
    }

} catch (Exception $e) {
    echo $e;
    FlashMessageHelpsers::createWarningMessage($e->getMessage());
}
?>
<?php Pjax::end() ?>

<?php
$CSS = <<<CSS
.float-right {
  float: right;
}
.child {
  /*border: 1px solid indigo;*/
  padding-left: 1rem;
}
CSS;
$this->registerCss($CSS);

$script = <<<JS
/**
 *  Alinea nombre de cuenta a la derecha y agrega prefijo 'a ' si es para haber.
 */
$('table.kv-grid-table td:nth-child(4)').filter(function () {// el parametro numerico para nth-child() especifica a que columna referenciar.
    return $(this).text() === "0";
}).each(function (i, td) {
    let ctaNombreCell = $(td).closest('td').prev('td');
    ctaNombreCell.prop('align', 'right');
    ctaNombreCell.html('a ' + ctaNombreCell.html()); // concatenar 'a ' como suele hacer beatriz patinho
});
JS;
$this->registerJs($script);
?>