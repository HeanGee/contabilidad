<?php

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Asiento */
/* @var $detalles backend\modules\contabilidad\models\AsientoDetalle[] */
/* @var $form yii\widgets\ActiveForm */
/* @var $totaldebe string */
/* @var $totalhaber string */
/* @var $totalCdebe string */
/* @var $totalChaber string */
/* @var $asientoIndep array */
/* @var $asientoRecibo array|null */
?>

<div class="asiento-form">
    <?php

    $form = ActiveForm::begin([]);

    echo '<div class="form-group">';
    echo \common\helpers\PermisosHelpers::getAcceso('contabilidad-asiento-index') ?
        Html::a('Ir a Asientos', ['index'], ['class' => 'btn btn-info']) : null;
    echo '</div>';

    echo '<fieldset>';
    {
        echo '<legend class="text-info">Parámetros</legend>';
        try {

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 2,
                'attributes' => [
                    'fecha_rango' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => DateRangePicker::className(),
                        'options' => [
                            'model' => $model,
                            'attribute' => 'fecha_rango',
                            'convertFormat' => true,
                            'language' => 'es',
                            'pluginOptions' => [
                                'allowClear' => true,
                                'autoApply' => true,
//                                'timePicker'=>true,
                                'locale' => ['format' => 'd-m-Y'],

                            ],

                        ],
                    ],
                    'descripcion' => [
                        'type' => Form::INPUT_TEXT,
                    ],
                ]
            ]);

            if (Yii::$app->request->getQueryParam('operacion') == 'compra')
                echo FormGrid::widget([
                    'model' => $model,
                    'form' => $form,
                    'autoGenerateColumns' => false,
                    'rows' => [
                        [
                            'autoGenerateColumns' => true,
                            'attributes' => [
                                'devengamientos' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => SwitchInput::className(),
                                    'options' => [
                                        'pluginOptions' => [
                                            'onText' => 'Si',
                                            'offText' => 'No',
                                            'offColor' => 'warning',
                                            'onColor' => 'success',
                                            'size' => 'small',
                                            'handleWidth' => '50',
                                            'labelWidth' => '50',
                                        ],
                                    ],
                                ],
                                'concepto_devengamiento' => [
                                    'type' => Form::INPUT_TEXT,
                                ],
                                'prestamo' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => SwitchInput::className(),
                                    'options' => [
                                        'pluginOptions' => [
                                            'onText' => 'Si',
                                            'offText' => 'No',
                                            'offColor' => 'warning',
                                            'onColor' => 'success',
                                            'size' => 'small',
                                            'handleWidth' => '50',
                                            'labelWidth' => '50',
                                        ],
                                    ],
                                ],
                                'prestamo_concepto' => [
                                    'type' => Form::INPUT_TEXT,
                                ],
                            ]
                        ],
//                        [
//                            'autoGenerateColumns' => true,
//                            'attributes' => [
//                                'devengamientos2' => [
//                                    'type' => Form::INPUT_WIDGET,
//                                    'widgetClass' => SwitchInput::className(),
//                                    'options' => [
//                                        'pluginOptions' => [
//                                            'onText' => 'Si',
//                                            'offText' => 'No',
//                                            'offColor' => 'warning',
//                                            'onColor' => 'success',
//                                            'size' => 'small',
//                                            'handleWidth' => '50',
//                                            'labelWidth' => '50',
//                                        ],
//                                    ],
//                                ],
//                                'concepto_devengamiento2' => [
//                                    'type' => Form::INPUT_TEXT,
//                                ],
//
//                                // para devengamiento de prestamos, agregar un campo switch para devengamientos_prestamo(si/no) y su correspondiente text input
//                            ],
//                        ],
                    ],
                ]);
        } catch (Exception $e) {
            echo $e;
        }

        echo Html::a('Vista Previa', ['asiento/create-from-factura', 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')], [
                'class' => 'btn btn-primary',
                'data' => [
                    'method' => 'post',
//        'confirm' => 'Are you sure?',
//        'params'=>['operacion'=>Yii::$app->getRequest()->getQueryParam('operacion')], // mas parametros al $_POST
                ]
            ]) . '&nbsp;&nbsp;';
        echo Html::a('Generar y guardar', ['asiento/create-from-factura', 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion'), 'tipo_operacion' => 'generar_guardar'], [
            'class' => 'btn btn-success',
            'data' => [
                'method' => 'post',
//        'confirm' => 'Are you sure?',
//        'params'=>['operacion'=>Yii::$app->getRequest()->getQueryParam('operacion'), 'tipo_operacion' => 'generar_guardar'],
            ]
        ]);

        echo Html::tag('br/');
        $view = Yii::$app->getRequest()->getQueryParam('operacion') == 'venta' ? '_form_asiento_detalles_venta_grid' : '_form_asiento_detalles_compra_grid';
        $vars = Yii::$app->getRequest()->getQueryParam('operacion') == 'venta' ? [
            'totaldebe' => $totaldebe,
            'totalhaber' => $totalhaber,
            'totalCdebe' => $totalCdebe,
            'totalChaber' => $totalChaber,
            'asientoIndep' => $asientoIndep,
            'asientoRecibo' => $asientoRecibo,
            'form' => $form,
        ] : [
            'totaldebe' => $totaldebe,
            'totalhaber' => $totalhaber,
            'asientoIndep' => $asientoIndep,
            'asientoRecibo' => $asientoRecibo,
            'form' => $form,
        ];

    }
    echo '</fieldset>';

    echo '<fieldset>';
    {
        echo '<legend class="text-info">Asientos</legend>';
        echo $this->render($view, $vars);
    }
    echo '</fieldset>';

    ActiveForm::end() ?>

    <?php
    $hintFor_devengamientos2 = "Las cuotas devengadas que fueron documentadas mediante recibos en lugar de facturas.";
    $hintFor_tprestamo = "Los préstamos que no tienen facturas de compra asociadas.";
    $color = \backend\helpers\HtmlHelpers::InfoColorHex(true);
    $JS = <<<JS
$(document).ready(function () {
    //  // Se deja como bckp, pero se implemento un mejor codigo, dentro de cada _form_asiento_detalles_compra/venta.php
    // $($('#grid_asientos_detalle_compra, #grid_asientos_detalle_compra, #grid_asientos_detalle_venta')[0].getElementsByTagName('tr')).each(function () {
    //     if (typeof $(this)[0].getElementsByTagName('td')[0] !== 'undefined') {
    //         if ($(this)[0].getElementsByTagName('td').length > 3 && $(this)[0].getElementsByTagName('td')[3].textContent === '0') {
    //             $($(this)[0].getElementsByTagName('td')[2]).css('text-align', 'right');
    //         }
    //     }
    // });


    let a = $('#templatemodel-devengamientos2').parent().parent().parent().parent().parent();
    a.removeClass('col-sm-6');
    a.addClass('col-sm-3');

    let b = $("#templatemodel-concepto_devengamiento2").parent().parent();
    b.removeClass('col-sm-6');
    b.addClass('col-sm-3');

    $('<div class="col-sm-6"></div>').insertBefore(a);

    let css = {'background-color': '#$color', 'color': 'white', 'text-align': 'center', 'font-weight': 'bold'};
    let label = $('label[for="templatemodel-devengamientos2"]');
    let labelText = label.html();
    labelText += "<span class=\"popuptrigger popuptrigger-devengamientos2 text-info glyphicon glyphicon-info-sign\" style=\"padding: 0 0 0 10px;\"></span>";
    label.html(labelText);
    applyPopOver($('span.popuptrigger-devengamientos2'), 'Se refiere a:', "{$hintFor_devengamientos2}", {'title-css': css, 'placement':'top'});

    label = $('label[for="templatemodel-prestamo"]');
    labelText = label.html();
    labelText += "<span class=\"popuptrigger popuptrigger-prestamo text-info glyphicon glyphicon-info-sign\" style=\"padding: 0 0 0 10px;\"></span>";
    label.html(labelText);
    applyPopOver($('span.popuptrigger-prestamo'), 'Se refiere a:', "{$hintFor_tprestamo}", {'title-css': css, 'placement': 'top'});
});
JS;

    $this->registerJs($JS);
    ?>
</div>