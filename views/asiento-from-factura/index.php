<?php

use backend\modules\contabilidad\models\Asiento;
use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\AsientoFromFacturaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asientos de ' . ucfirst(Yii::$app->getRequest()->getQueryParam('operacion'));
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="asiento-index">

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= PermisosHelpers::getAcceso('contabilidad-asiento-create-from-factura') ?
                Html::a('Crear Asiento', [
                    'create-from-factura',
                    'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')
                ], ['class' => 'btn btn-success']) : null ?>
        </p>

        <?php
        try {
            $columns = [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'periodo_contable_id',
                    'value' => function ($model) {
                        return $model->periodoContable->anho;
                    }
                ],
                [
                    'attribute' => 'fecha',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha',
                        'language' => 'es',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                        ],
                    ]),
                    'format' => 'html',
                    'value' => function ($model) {
                        return date('d-m-Y', strtotime($model->fecha));
                    }
                ],
                'concepto:ntext',
                [
                    'attribute' => 'monto_debe',
                    'label' => 'Total',
                    'value' => function ($model) {
                        return number_format($model->monto_debe, 0, ',', '.');
                    }
                ],
            ];
            $template = '{view} &nbsp {delete}';
            $buttons = [
                'delete' => function ($url, $model, $index) {
                    if ($model instanceof Asiento && $model->isBorrable()) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            $url,
                            [
                                'class' => 'btn_delete_asiento',
                                'id' => $index,
                                'title' => Yii::t('app', 'Borrar'),
                                'data-confirm' => 'Esta seguro',
                            ]
                        );
                    } else return null;
                },
                'view' => function ($url, $model, $index) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url,
                        [
                            'class' => 'btn_view_asiento',
                            'id' => $index,
                            'title' => Yii::t('app', 'Ver'),
                        ]
                    );
                },
            ];
            array_push($columns, [
                'class' => ActionColumn::class,
                'template' => $template,
                'buttons' => $buttons,
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'delete') {
                        $url = Url::to(['asiento/delete-from-factura', 'id' => $model->id, 'tipo_op' => Yii::$app->getRequest()->getQueryParam('operacion')]);
                        return $url;
                    } elseif ($action === 'view') {
                        $url = Url::to(['asiento/view-from-factura', 'id' => $model->id, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')]);
                        return $url;
                    }
                    return '';
                },
                'contentOptions' => ['style' => 'font-size: 90%;'],
                'headerOptions' => ['style' => 'font-size: 90%;'],
            ]);

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $columns,
                'hover' => true,
            ]);

//        echo GridView::widget([
//            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
//            'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],
//
////                    'id',
////                    'empresa_id',
//                [
//                    'attribute' => 'periodo_contable_id',
//                    'value' => function ($model) {
//                        return $model->periodoContable->anho;
//                    }
//                ],
//                [
//                    'attribute' => 'fecha',
//                    'filter' => DatePicker::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'fecha',
//                        'language' => 'es',
//                        'pluginOptions' => [
//                            'autoclose' => true,
//                            'format' => 'dd-mm-yyyy',
//                            'todayHighlight' => true,
//                            'todayBtn' => true,
//                        ],
//                    ]),
//                    'format' => 'html',
//                    'value' => function ($model) {
//                        return date('d-m-Y', strtotime($model->fecha));
//                    }
//                ],
//                'concepto:ntext',
//                [
//                    'attribute' => 'monto_debe',
//                    'label' => 'Total',
//                    'value' => function ($model) {
//                        return number_format($model->monto_debe, 0, ',', '.');
//                    },
//                    'contentOptions' => ['style' => 'text-align: right;'],
//                ],
//
//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'template' => $template,
//                    ''
//                ],
//            ],
//        ]);

        } catch (\Exception $e) {
            echo $e;
        }
        ?>
        <?php Pjax::end(); ?>
    </div>

<?php
//$CSS = <<<CSS
//.table tbody tr:hover {
//    background-color: #ebffdd;
//}
//CSS;
//$this->registerCss($CSS);
//?>