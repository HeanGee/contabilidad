<?php

use common\helpers\FlashMessageHelpsers;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $totaldebe string */
/* @var $totalhaber string */
/* @var $asientoIndep array */
?>
<?php Pjax::begin(['id' => 'grid_asientos_detalle_compra']); ?>
<?php

$columns = [
    [
        'class' => SerialColumn::className(),
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],

    [
        'class' => DataColumn::class,
        'label' => 'Código cuenta',
        'value' => 'cuenta.cod_completo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'class' => DataColumn::class,
        'label' => 'Cuenta',
        'value' => 'cuenta.nombre',
        'pageSummaryOptions' => [
            'data-colspan-dir' => 'ltr',
            'colspan' => 1,
            'style' => 'text-align: right;',
        ],
        'pageSummary' => function ($summary, $data, $widget) {
            return "Totales";
        },
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'class' => DataColumn::class,
        'label' => 'Debe',
        'value' => 'monto_debe',
        'format' => 'decimal',
        'pageSummary' => true,
        'pageSummaryFunc' => GridView::F_SUM,
        'pageSummaryOptions' => [
            'append' => '',
            'prepend' => '',
        ],
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'class' => DataColumn::class,
        'label' => 'Haber',
        'value' => 'monto_haber',
        'format' => 'decimal',
        'pageSummary' => true,
        'pageSummaryFunc' => GridView::F_SUM,
        'pageSummaryOptions' => [
            'append' => '',
            'prepend' => '',
        ],
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
];

$template = '{update} &nbsp {delete}';
$buttons = [];

//array_push($columns, [
//    'class' => ActionColumn::class,
//    'template' => $template,
//    'buttons' => $buttons,
//    'contentOptions' => ['style' => 'font-size: 90%;'],
//    'headerOptions' => ['style' => 'font-size: 90%;'],
//]);

try {
    $sessionKey = 'cont_asiento_from_factura_compra_detalle-provider';
    $sessionKey2 = 'cont_asiento_from_factura_devengamientos-provider';
    $sessionKey3 = 'cont_asiento_from_factura_prestamo-provider';
    $dataProvider = Yii::$app->getSession()->has($sessionKey) ? Yii::$app->getSession()->get($sessionKey) : new ArrayDataProvider([
        'allModels' => [],
        'pagination' => false,
    ]);
    $dataProvider2 = Yii::$app->getSession()->has($sessionKey2) ? Yii::$app->getSession()->get($sessionKey2) : new ArrayDataProvider([
        'allModels' => [],
        'pagination' => false,
    ]);
    $data3 = Yii::$app->session->get($sessionKey3, null);
    // TODO: Descomentar para confirmar y verificar el total debe y haber.
//    if ($totalhaber != '' && $totaldebe != '') {
//        $lastRow = new AsientoDetalle();
//        $lastRow->monto_debe = $totaldebe;
//        $lastRow->monto_haber = $totalhaber;
//        $dataProvider->allModels[] = $lastRow;
//    }

    if ($dataProvider->allModels != null) {
        echo GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'success',
                'heading' => 'Asiento de Facturas',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
        ]);
    }

    if (!empty($asientoIndep)) {
        echo $this->render('_form-asientos-independientes', [
            'asientoIndep' => $asientoIndep,
            'form' => $form,
        ]);
    }

    if ($dataProvider2->allModels != null) {
        echo GridView::widget([
            'id' => 'grid-dev',
            'dataProvider' => $dataProvider2,
            'columns' => $columns,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'success',
                'heading' => 'Asiento de Devengamiento de Seguros',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
        ]);
    }

    if (isset($data3)) {
        echo GridView::widget([
            'id' => 'grid-prestamo',
            'dataProvider' => $data3['dataProvider'],
            'columns' => $columns,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'success',
                'heading' => 'Asiento de Préstamos',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
        ]);
    }

    $skey4 = "cont_asiento_from_factura_cuota_recibo-provider";
    $dataArray = Yii::$app->session->get($skey4);
    if (!empty($dataArray)) {
        foreach ($dataArray as $data) {
            $prestamo = \backend\modules\contabilidad\models\Prestamo::findOne($data['prestamo_id']);
            echo GridView::widget([
                'id' => 'grid-prestamo',
                'dataProvider' => $data['detalles_asiento'],
                'columns' => $columns,
                'toolbar' => [],
                'hover' => true,
                'panel' => [
                    'type' => 'success',
                    'heading' => "Asientos por Recibos de Cuotas de Préstamos Nº {$prestamo->id}",
                    'footerOptions' => ['class' => ''],
                    'beforeOptions' => ['class' => ''],
                    'afterOptions' => ['class' => '']
                ],
                'panelFooterTemplate' => '',
                'showPageSummary' => true,
                'pageSummaryPosition' => GridView::POS_BOTTOM,
                'pageSummaryRowOptions' => [
                    'class' => 'kv-page-summary warning',
                    'style' => 'text-align: right;',
                ],
            ]);
        }
    }

    if (isset($asientoRecibo) && !empty($asientoRecibo)) {
        $dataProvider = new ArrayDataProvider(['allModels' => $asientoRecibo, 'pagination' => false]);
        echo GridView::widget([
            'id' => 'grid-asiento-recibo',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'success',
                'heading' => 'Asiento de recibos',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
            'columns' => $columns,
        ]);
    }
} catch (Exception $e) {
    echo $e;
    FlashMessageHelpsers::createWarningMessage($e->getMessage());
}
?>
<?php Pjax::end() ?>

<?php
$CSS = <<<CSS
.float-right {
  float: right;
}
.child {
  /*border: 1px solid indigo;*/
  padding-left: 1rem;
}
CSS;
$this->registerCss($CSS);

$script = <<<JS
/**
 *  Alinea nombre de cuenta a la derecha y agrega prefijo 'a ' si es para haber.
 */
$('table.kv-grid-table td:nth-child(4)').filter(function () {// el parametro numerico para nth-child() especifica a que columna referenciar.
    return $(this).text() === "0";
}).each(function (i, td) {
    let ctaNombreCell = $(td).closest('td').prev('td');
    ctaNombreCell.prop('align', 'right');
    ctaNombreCell.html('a ' + ctaNombreCell.html()); // concatenar 'a ' como suele hacer beatriz patinho
});
JS;
$this->registerJs($script);
?>