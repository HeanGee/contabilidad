<?php

/* @var $this yii\web\View */

use common\helpers\FlashMessageHelpsers;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\data\ArrayDataProvider;

/* @var $form kartik\form\ActiveForm */
/* @var $asientoIndep array */
?>

<?php try { ?>

    <?php

    $columns = [
        [
            'class' => SerialColumn::className(),
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],

        [
            'label' => 'Código cuenta',
            'value' => 'cuenta.cod_completo',
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'label' => 'Cuenta',
            'value' => 'cuenta.nombre',
            'pageSummaryOptions' => [
                'data-colspan-dir' => 'ltr',
                'colspan' => 1,
                'style' => 'text-align: right;',
            ],
            'pageSummary' => function ($summary, $data, $widget) {
                return "Totales";
            },
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'label' => 'Debe',
            'value' => 'monto_debe',
            'format' => 'decimal',
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'pageSummaryOptions' => [
                'append' => '',
                'prepend' => '',
            ],
            'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'label' => 'Haber',
            'value' => 'monto_haber',
            'format' => 'decimal',
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'pageSummaryOptions' => [
                'append' => '',
                'prepend' => '',
            ],
            'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
    ];

    $template = '';
    $buttons = [];
    ?>

    <?php foreach ($asientoIndep as $key => $asientoData) : ?>

        <?php
        $dataProvider = new ArrayDataProvider([
            'allModels' => $asientoData['detalles'],
            'pagination' => false,
        ]);
        echo GridView::widget([
            'id' => "grid-asiento-indep-{$key}",
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'hover' => true,
            'panel' => [
                'type' => 'success',
                'heading' => $asientoData['tituloGrid'],
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
            'toolbar' => [],
        ]);
        ?>

    <?php endforeach; ?>

<?php } catch (Exception $exception) {
    echo $exception;
    FlashMessageHelpsers::createWarningMessage("Error desde archivo " . __FILE__ . ": " . $exception->getMessage());
}