<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Asiento */
/* @var $detalles backend\modules\contabilidad\models\AsientoDetalle[] */
/* @var $totaldebe string */
/* @var $totalhaber string */
/* @var $totalCdebe string */
/* @var $totalChaber string */
/* @var $asientoIndep array */
/* @var $asientoRecibo array|null */

$yii_app = Yii::$app;
$operacion = ucfirst($yii_app->request->getQueryParam('operacion'));
$this->title = "Crear Asiento de {$operacion}";
$this->params['breadcrumbs'][] = ['label' => 'Asientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asiento-create">

    <?= $this->render('_form', [
        'model' => $model,
        'totaldebe' => $totaldebe,
        'totalhaber' => $totalhaber,
        'totalCdebe' => $totalCdebe,
        'totalChaber' => $totalChaber,
        'asientoIndep' => $asientoIndep,
        'asientoRecibo' => $asientoRecibo,
    ]) ?>

</div>
