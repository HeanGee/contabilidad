<?php

use backend\modules\contabilidad\models\TipoDocumentoSet;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $modelo \backend\modules\contabilidad\models\TimbradoDetalle */

?>
<div class="timbrado-detalle-form">

<?php
    ActiveFormDisableSubmitButtonsAsset::register($this);

    $form = ActiveForm::begin([
        'id' => 'timbrado_detalle_form',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]);

    try{
        echo FormGrid::widget([
            'model' => $modelo,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows'=>[
                [
                    'attributes' => [
                        'tipo_documento_set_id' => [
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($modelo, 'tipo_documento_set_id')->widget(Select2::className(), [
                                'data' => TipoDocumentoSet::getTiposDocumentoSet(),
                                'language' => 'es',
                                'options' => [
                                    'placeholder' => 'Seleccione...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'tags' => true
                                ],
                                'initValueText' => !empty($modelo->tipo_documento_set_id) ? $modelo->tipoDocumentoSet->nombre : ''
                            ])
                        ],
                        'prefijo' => [
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($modelo, 'prefijo')->widget(MaskedInput::class, [
                                'name' => 'prefijo',
                                'mask' => '999-999'
                            ])
                        ]
                    ]
                ],
                [
                    'attributes' => [
                        'nro_inicio' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder '=> 'Inicio...']
                        ],
                        'nro_fin' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder '=> 'Fin...']
                        ],
//                        'nro_actual' => [
//                            'type' => Form::INPUT_TEXT,
//                            'options' => ['placeholder '=> 'Actual...']
//                        ]
                    ]
                ]
            ]
        ]);

    }catch (\Exception $e){
        print $e;
    }

    $btn = 'success';
    $texto = 'Agreg';
    if(!empty($modelo->id)){
        $btn = 'primary';
        $texto = 'Edit';
    }

    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', "{$texto}ar"), ['data' => ['disabled-text' => "{$texto}ando..."], 'class' => "btn btn-{$btn}"]) ?>
    </div>

    <?php ActiveForm::end();

    $script =
        <<<JS
    $("#timbrado_detalle_form").on("beforeSubmit", function(e) {
        var form = $(this);
        $.post(
            form.attr("action")+"&submit=true",
            form.serialize()
        )
        .done(function(result) {
            $.pjax.reload({container:"#detalle_timbrado_gridview", async:false});
            $.pjax.reload({container:"#flash_message_id", async:false});
            $("#modal").modal("hide");      
            $(".modal-body").empty();         
        });
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    })
    .on("submit", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });
JS;
    $this->registerJs($script);
    ?>

</div>
