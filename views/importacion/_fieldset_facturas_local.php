<?php
/* @var $model \backend\modules\contabilidad\models\Importacion
 * @var IvaCuenta $iva_cuenta
 */

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\ImportacionDetalleFacturaLocalExterior;
use backend\modules\contabilidad\models\IvaCuenta;
use kartik\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

?>

    <fieldset>
        <legend>&nbsp;&nbsp; Facturas Locales
            <?= Html::a(
                '<i class="glyphicon glyphicon-plus"></i>',
                'javascript:void(0);',
                [
                    'id' => 'factura-local-new-button',
                    'class' => 'pull-left btn btn-success'
                ]
            ); ?>
        </legend>
        <table id="factura-local" class="table table-condensed table-bordered" style="table-layout: fixed;">
            <thead>
            <tr>
                <th class="texto_centrado" style="width: 34%;">Facturas/Comprobantes</th>
                <th class="texto_centrado" style="width: 9.66%;">Monto</th>

                <?php
                $divisor = 30 / sizeof($porcentajes_iva);
                foreach ($porcentajes_iva as $ic)
                    if ($ic == 0)
                        echo '<th class="texto_centrado"  style="width: ' . $divisor . '%;">Exenta</th>';
                    else
                        echo '<th class="texto_centrado"  style="width: ' . $divisor . '%;">Total ' . $ic . '%</th>';
                ?>

                <th class="texto_centrado" style="width: 9.66%;">Cotización</th>
                <th class="texto_centrado" style="width: 16.66%;">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            //TODO solo traer facturas que correspondan a importación y que no estén en otra importación.
            $facturaLocalLista = Compra::getFacturasByImportacion(true, false, []);
            $factura_local_data = !isset($facturaLocalLista) ? [] : Json::encode($facturaLocalLista);
            $importacionDetalle = new ImportacionDetalleFacturaLocalExterior();
            $importacionDetalle->loadDefaultValues();

            if ($actionId == 'create')
                $url_get_data_factura = Json::htmlEncode(\Yii::t('app', Url::to(['importacion/get-data-factura', 'local' => true])));
            else
                $url_get_data_factura = Json::htmlEncode(\Yii::t('app', Url::to(['importacion/get-data-factura', 'local' => true, 'importacionId' => $model->id])));

            //Agregar facturas existentes
            $i = 1;
            /** @var ImportacionDetalleFacturaLocalExterior $local */
            foreach ($model->arrayLocal as $key => $local) {
                echo '<tr>';
                echo $this->render('_form-local-iva-cuenta-usada', [
                    'key' => 'new' . $i,
                    'form' => $form,
                    'importacionDetalle' => $local,
                    'porcentajes_iva' => $porcentajes_iva,
                ]);
                echo '</tr>';

                $facturaCompuesta = 'plantilla_' . $local->plantilla_id . '_compra_' . $local->factura_compra_id;

                $digists = 0;
                if ($local->facturaCompra->moneda->tiene_decimales == 'si')
                    $digists = 2;

                $_s = <<<JS
                    generarEventoChangeFactura("local-new"+$i+"-plantilla_factura_id", true);
                    //totalesIvaComplete("$facturaCompuesta", "local-new"+$i+"-ivas-iva_");
                    if (typeof idslocal === 'undefined') {
                        let idslocal = '';
                    }
                    idslocal = getTotalCampoNombres("local-new"+$i+"-ivas-iva_");
                    idslocal.push("local-new"+$i+"-monto");
                    idslocal.push("local-new"+$i+"-cotizacion");
                    construirMaskedInput($digists, null, idslocal);
JS;
                $this->registerJs($_s);
                $i++;
            }
            ?>
            </tbody>
        </table>
    </fieldset>

<?php

$html = $this->render('_form-local-iva-cuenta-usada', [
    'key' => '__id__',
    'form' => $form,
    'importacionDetalle' => $importacionDetalle,
    'porcentajes_iva' => $porcentajes_iva,
]);

// remover todos los \n \r y combinacion de ellos segun:
// https://stackoverflow.com/questions/5258543/remove-all-the-line-breaks-from-the-html-source
$output = str_replace(array("\r\n", "\r"), "\n", $html);
$lines = explode("\n", $output);
$substrings = array();

foreach ($lines as $i => $line) {
    if (!empty($line))
        $substrings[] = trim($line);
}
$html_without_enter_and_spaces = implode($substrings);
$bluePrint = Json::encode($html_without_enter_and_spaces);

$script = <<<JS
var local_iva_cta_usada_k = $i;

$('#factura-local-new-button').on('click', function (e) {
    local_iva_cta_usada_k += 1;
    $('#factura-local').find('tbody')
        .append('<tr>' + 
        ($bluePrint).replace(/__id__/g, 'new' + local_iva_cta_usada_k) + '</tr>');
    
    $.ajax({
        url: $url_get_data_factura,
        type: 'post',
        data: {
            seleccionados: getValuesInput('local', 'plantilla_factura_id')
        },
        success: function (data) {
            if(data.length > 0) {
                construirSelect2(getSelectorForTab(local_iva_cta_usada_k,'plantilla_factura_id','', 'local'), false, data);
            } else {
                $('#local-new' + local_iva_cta_usada_k + '-remove').click();
                krajeeDialog.alert('No existen facturas para agregar.');
                e.preventDefault();
                return false;
            }
        }
    });
    
    generarEventoChangeFactura("local-new"+local_iva_cta_usada_k+"-plantilla_factura_id", true);
});

// no se usa pero se deja como backup
$(document).on('click', '.local-iva-cta-usada-remove-button', function () {
    $(this).closest('tbody tr').remove();
});

$(document).ready(function() {
    let tabla_local_field = $('#factura-local');
    if (tabla_local_field[0].rows.length > 1) {
        var plantillas_locales = []; 
        $.ajax({
            url: $url_get_data_factura,
            type: 'post',
            data: {
                seleccionados: null
            },
            success: function (data) {
                plantillas_locales = data;
            },
            async: false
        });
        
        //Se construye los select 2 de la tabla en caso de update
        for(let i = 1; i < tabla_local_field[0].rows.length; i++){
            let id_input = tabla_local_field[0].rows[i].cells[0].children[0].children[0].id.replace(/local-new/g, '').replace(/-plantilla_factura_id/g, '');
            construirSelect2(getSelectorForTab(id_input,'plantilla_factura_id','', 'local'), false, plantillas_locales);
            removerValorDeArray(plantillas_locales, tabla_local_field[0].rows[i].cells[0].children[0].children[0].value);
            
            let cotizacion_field = $('#local-new' + i + '-cotizacion');
            let plantilla_factura_field = $('#local-new' + i + '-plantilla_factura_id');
            if (plantilla_factura_field.select2('data')[0]['tiene_decimales'] === 'si'){
                cotizacion_field.val(plantilla_factura_field.select2('data')[0]['cotizacion']);
                cotizacion_field.prop('disabled', false);
            }else {
                cotizacion_field.prop('disabled', true);
                cotizacion_field.val(null);
            }
        }
    }
})
JS;
$this->registerJs($script);
?>