<?php

use backend\models\Moneda;
use common\models\ParametroSistema;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Compra */

try {
    /** @var \backend\modules\contabilidad\models\Importacion $model */
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'contentBefore' => '<legend class="text-info"><small>Datos Iniciales</small></legend>',
                'autoGenerateColumns' => false,
                'columns' => 12,
                'attributes' => [
                    'fecha' => [
                        'columnOptions' => ['colspan' => '2'],
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'fecha')->widget(MaskedInput::className(),
                            [
                                'name' => 'fecha',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ]
                        ),
                    ],
                    'numero_despacho' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'numero_despacho')->textInput(['placeholder' => 'Número Despacho']),
                    ],
                    'exportador' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'exportador')->textInput(['placeholder' => 'Exportador']),
                    ],
                    'condicion' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'condicion')->widget(Select2::className(), [
                            'data' => ['fob' => 'FOB', 'cif' => 'CIF'],
                            'initValueText' => $model->condicion,
                            'options' => [
                                'placeholder' => 'Condición',
                            ],
                            'hideSearch' => true,
                        ])
                    ],
                ]
            ],
            [
                'contentBefore' => '<legend class="text-info"><small>Valores del despacho</small></legend>',
                'autoGenerateColumns' => false,
                'columns' => 12,
                'attributes' => [
                    'iva' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'iva')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'indi' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'indi')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'derecho_aduanero' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'derecho_aduanero')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'servicio_valoracion' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'servicio_valoracion')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'iracis_generaral' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'iracis_generaral')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'tasa_interv_aduanera' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'tasa_interv_aduanera')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'repos_gtos_adm_annp' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'repos_gtos_adm_annp')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'cdap_comis_can_annp' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'cdap_comis_can_annp')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'cannon_informatico_sistema_sofia' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'cannon_informatico_sistema_sofia')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'sofia_comis_can_annp' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'sofia_comis_can_annp')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'multas_varias' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'multas_varias')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'impuesto_selectivo_consumo' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'impuesto_selectivo_consumo')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'arancel_consular' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'arancel_consular')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'despacho_moneda_id' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'despacho_moneda_id')->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione una moneda'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'data' => Moneda::getAll()
                            ],
                            'initValueText' => !empty($model->despacho_moneda_id) ? ($model->despacho_moneda_id . ' - ' . $model->despachoMoneda->nombre) : ''
                        ])
                    ],
                    'despacho_cotizacion' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'despacho_cotizacion')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'digits' => 0,
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'flete_costo' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'flete_costo')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                    'seguro_costo' => [
                        'type' => Form::INPUT_RAW,
                        'columnOptions' => ['colspan' => '2'],
                        'value' => $form->field($model, 'seguro_costo')->widget(NumberControl::className(), [
                            'value' => 0,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'radixPoint' => ',',
                                'rightAlign' => false,
                                'allowMinus' => false,
                            ],
                        ]),
                    ],
                ]
            ],
            [
                'contentBefore' => '<legend class="text-info"><small>Medios de transporte</small></legend>',
                'attributes' => [
                    'medio_transporte' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'medio_transporte')
                            ->radioList(
                                [
                                    'terrestre' => 'Terrestre',
                                    'aereo' => 'Aereo',
                                    'maritimo' => 'Marítimo'
                                ], ['inline' => true])
                            ->label('Medio de transporte')
                    ],
                ]
            ],
            [
                'contentBefore' => '<legend class="text-info"><small>Criterios de retención</small></legend>',
                'autoGenerateColumns' => false,
                'columns' => 3,
                'attributes' => [
                    'criterio_retencion' => [
                        'type' => Form::INPUT_RAW,
                        'value' => $form->field($model, 'criterio_retencion')
                            ->radioList(
                                [
                                    'pago_neto' => 'Criterio de retención pago neto',
                                    'absorcion' => 'Criterio de retención absorción'
                                ], ['inline' => true])
                            ->label('Criterio de retención')
                    ],
                ]
            ],
        ]]);
//    echo '<legend class="text-info"><small>Cotización</small></legend>';
//    echo 'Si el despacho maneja una moneda o varias ...';
} catch (Exception $e) {
    echo $e->getMessage();
}

$monedaBase = ParametroSistema::getMonedaBaseId();
$script = <<<JS
function manageDespachoFields(fleteField) {
    let seguroField = $('#importacion-seguro_costo-disp'),
        monedaField = $('#importacion-despacho_moneda_id'),
        cotizacField = $('#importacion-despacho_cotizacion-disp'),
        form = $('form[id="importacion_form"]');

    form.yiiActiveForm('validateAttribute', 'importacion-seguro_costo');
    form.yiiActiveForm('validateAttribute', 'importacion-despacho_moneda_id');
    form.yiiActiveForm('validateAttribute', 'importacion-despacho_cotizacion');

    if (fleteField.val() === '') {
        seguroField.val('').trigger('change');
        monedaField.val('').trigger('change');
        cotizacField.val('').trigger('change');

        // seguroField.parent().parent()[0].style.display = "none";
        // monedaField.parent().parent()[0].style.display = "none";
        cotizacField.parent().parent()[0].style.display = "none";
    } else {
        //seguroField.val('$model->seguro_costo').trigger('change');
//        monedaField.val('$model->despacho_moneda_id').trigger('change');
        cotizacField.val('$model->despacho_cotizacion').trigger('change');
        
        //seguroField.parent().parent()[0].style.display = "";
        // monedaField.parent().parent()[0].style.display = "";
    }
}

function manageCampoCotizacion() {
    let cotizacField = $('#importacion-despacho_cotizacion-disp'),
        monedaField = $('#importacion-despacho_moneda_id');
    
    if (monedaField.val() !== '$monedaBase' && monedaField.val() !== '') {
        cotizacField.val('$model->despacho_cotizacion').trigger('change');
        cotizacField.parent().parent()[0].style.display = "";
    } else {
        cotizacField.val('').trigger('change');
        cotizacField.parent().parent()[0].style.display = "none";
    }
}

function manageMedioTransporte(){
    let condicion_field = $('#importacion-condicion');
    if (condicion_field.val() === 'fob'){
        $(':input[value="maritimo"]').attr('disabled', true);
        $(':input[value="aereo"]').attr('disabled', true);
        $(':input[value="terrestre"]').attr('disabled', false);
        
        $(':input[value="terrestre"]').attr('checked', true);
    } else if (condicion_field.val() === 'cif') {
        $(':input[value="maritimo"]').attr('checked', false);
        $(':input[value="aereo"]').attr('checked', false);
        $(':input[value="terrestre"]').attr('checked', false);
        
        $(':input[value="terrestre"]').attr('disabled', true);
        $(':input[value="maritimo"]').attr('disabled', false);
        $(':input[value="aereo"]').attr('disabled', false);
    }
}

$(document).on('change', '#importacion-despacho_moneda_id', function () {
    manageCampoCotizacion();
    
    let config_with_decimal = {
        radixPoint: ",",
        prefix: "",
        digits: 2,
        autoGroup: true,
        groupSeparator: ".",
        rightAlign: true,
        autoUnmask: true,
        allowMinus: false
    };
    let config_without_decimal = {
        radixPoint: "",
        prefix: "",
        digits: 2,
        autoGroup: true,
        groupSeparator: ".",
        rightAlign: true,
        autoUnmask: true,
        allowMinus: false
    };
    if ($('#importacion-despacho_moneda_id').select2('data')[0]['tiene_decimales'] === 'si') {
        $('#importacion-flete_costo-disp').inputmask(config_with_decimal);
        $('#importacion-seguro_costo-disp').inputmask(config_with_decimal);
    } else {
        $('#importacion-flete_costo-disp').inputmask(config_without_decimal);
        $('#importacion-seguro_costo-disp').inputmask(config_without_decimal);
    }
});

$(document).on('change', '#importacion-flete_costo-disp', function () {
    manageDespachoFields($(this));
});

// $(document).on('change', '#importacion-condicion', function () {
//     manageMedioTransporte($(this));
// });

$(document).ready(function () {
    manageDespachoFields($('#importacion-flete_costo-disp'));
    // manageMedioTransporte();
});
JS;
$this->registerJs($script);