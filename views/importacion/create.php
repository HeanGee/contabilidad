<?php


/* @var $this yii\web\View */
/* @var $model \backend\modules\contabilidad\models\Importacion */
/* @var $tabReporte int */

$this->title = 'Nueva Importación';
$this->params['breadcrumbs'][] = ['label' => 'Importaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="importacion-create">
    <?= $this->render('_form', [
        'model' => $model,
        'tabReporte' => $tabReporte
    ]) ?>
</div>
