<?php

use backend\modules\contabilidad\helpers\ParametroSistemaHelpers;
use backend\modules\contabilidad\models\AsientoDetalle;
use backend\modules\contabilidad\models\PlanCuenta;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use kartik\helpers\Html;
use kartik\number\NumberControl;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \backend\modules\contabilidad\models\Importacion */
$actionId = Yii::$app->controller->action->id;
try {
    $btnNewTabReport = $actionId == 'update' ?
        Html::a(Yii::t('app', 'Ver Reporte en Otra Pestaña'), ['reporte', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank']) :
        '';
    $btnNewTabReportExcel = $actionId == 'update' ?
        Html::a(Yii::t('app', 'Ver EXCEL'), ['reporte-excel', 'id' => $model->id], ['class' => 'btn btn-success', 'target' => '_blank']) :
        '';

    echo '<legend class="text-info">
            <small>Reporte y Guardado  &nbsp' . $btnNewTabReport . ' ' . $btnNewTabReportExcel . '</small>
        </legend>';

    if ($actionId == 'update') {
        $url = Url::toRoute(['importacion/reporte', 'id' => $model->id]);
        echo Html::tag('iframe', "", ['src' => $url, 'width' => '100%', 'height' => '500px']);

        echo '<legend class="text-info"><small>Simulación de Asiento</small></legend>';
        Pjax::begin(['id' => 'grid_asientos_detalle']);
        //Simulacion de asiento
        try {
            $columns = [
                [
                    'class' => SerialColumn::className(),
                    'contentOptions' => ['style' => 'font-size: 90%;'],
                    'headerOptions' => ['style' => 'font-size: 90%;'],
                ],
                [
                    'label' => 'Código cuenta',
                    'value' => 'cuenta.cod_completo',
                    'contentOptions' => ['style' => 'font-size: 90%;'],
                    'headerOptions' => ['style' => 'font-size: 90%;'],
                ],
                [
                    'label' => 'Cuenta',
                    'value' => 'cuenta.nombre',
                    'contentOptions' => ['style' => 'font-size: 90%;'],
                    'headerOptions' => ['style' => 'font-size: 90%;'],
                ],
                [
                    'label' => 'Debe',
                    'value' => 'montoDebeFormatted',
                    'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
                    'headerOptions' => ['style' => 'font-size: 90%;'],
                ],
                [
                    'label' => 'Haber',
                    'value' => 'montoHaberFormatted',
                    'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
                    'headerOptions' => ['style' => 'font-size: 90%; width: 20%'],
                ],
            ];

            //Asientos detalles
            $totalDebe = 0;
            $totalHaber = 0;

            $ctaGNDmulta = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-periodo-{$model->periodo_contable_id}-cta_importacion_gnd_multa");
            $cuentaMercImportadas = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-periodo-{$model->periodo_contable_id}-cta_importacion_mercaderias_importadas");
            $cuentaIva10 = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-periodo-{$model->periodo_contable_id}-cta_importacion_iva_10");
            $cuentaProvExterior = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-periodo-{$model->periodo_contable_id}-cta_importacion_prov_exterior");
            $cuentaHaber = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-periodo-{$model->periodo_contable_id}-cta_importacion_haber");

            $itemsTr = $model->getItemsLocalExteriorTr();
            $ivasTr = $model->getIvasTr();

            $asiento_detalles = [];
            //DEBE
            $asiento_detalle = new AsientoDetalle();
            $asiento_detalle->cuenta_id = $cuentaMercImportadas;
            $asiento_detalle->monto_debe = $itemsTr['totalitem'] - $model->multas_varias;
            $asiento_detalle->monto_haber = 0.0;
            $asiento_detalles[] = $asiento_detalle;
            $totalDebe += round($asiento_detalle->monto_debe);

            $asiento_detalle = new AsientoDetalle();
            $asiento_detalle->cuenta_id = $cuentaIva10;
            $asiento_detalle->monto_debe = $ivasTr['total'];
            $asiento_detalle->monto_haber = 0.0;
            $asiento_detalles[] = $asiento_detalle;
            $totalDebe += round($asiento_detalle->monto_debe);

            //GND multas
            $asiento_detalle = new AsientoDetalle();
            $asiento_detalle->cuenta_id = $ctaGNDmulta;
            $asiento_detalle->monto_debe = $model->multas_varias;
            $asiento_detalle->monto_haber = 0.0;
            $asiento_detalles[] = $asiento_detalle;
            $totalDebe += round($asiento_detalle->monto_debe);

            if ($model->criterio_retencion == 'absorcion') {
                $ctaRetencionIvaDebe = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-cta_importacion_retencion_iva_debe");
                $ctaRetencionIvaHaber = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-cta_importacion_retencion_iva_haber");
                $ctaRetencionRentaDebe = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-cta_importacion_retencion_renta_debe");
                $ctaRetencionRentaHaber = ParametroSistemaHelpers::getValorByNombre("core_empresa-{$model->empresa_id}-cta_importacion_retencion_renta_haber");
                if ($ctaRetencionIvaDebe != '' && $ctaRetencionIvaHaber != '' && $ctaRetencionRentaDebe != '' && $ctaRetencionRentaHaber != '') {
                    $resultRetenciones = $model->getRetencionesTr();
                    $totalRenta = 0;
                    foreach ($resultRetenciones['arraydatarenta'] as $item) {
                        $totalRenta += $item['total'];
                    }

                    $totalIva = 0;
                    foreach ($resultRetenciones['arraydataiva'] as $item) {
                        $totalIva += $item['total'];
                    }

                    //DEBE
                    $asiento_detalle = new AsientoDetalle();
                    $asiento_detalle->cuenta_id = $ctaRetencionIvaDebe;
                    $asiento_detalle->monto_debe = $totalIva;
                    $asiento_detalle->monto_haber = 0.0;
                    $asiento_detalles[] = $asiento_detalle;
                    $totalDebe += round($asiento_detalle->monto_debe);

                    $asiento_detalle = new AsientoDetalle();
                    $asiento_detalle->cuenta_id = $ctaRetencionRentaDebe;
                    $asiento_detalle->monto_debe = $totalRenta;
                    $asiento_detalle->monto_haber = 0.0;
                    $asiento_detalles[] = $asiento_detalle;
                    $totalDebe += round($asiento_detalle->monto_debe);

                    //HABER
                    $asiento_detalle = new AsientoDetalle();
                    $asiento_detalle->cuenta_id = $ctaRetencionIvaHaber;
                    $asiento_detalle->monto_debe = 0.0;
                    $asiento_detalle->monto_haber = $totalIva;
                    $asiento_detalles[] = $asiento_detalle;
                    $totalDebe += round($asiento_detalle->monto_haber);

                    $asiento_detalle = new AsientoDetalle();
                    $asiento_detalle->cuenta_id = $ctaRetencionRentaHaber;
                    $asiento_detalle->monto_debe = 0.0;
                    $asiento_detalle->monto_haber = $totalRenta;
                    $asiento_detalles[] = $asiento_detalle;
                    $totalDebe += round($asiento_detalle->monto_haber);
                }
            }

            //HABER
            $asiento_detalle = new AsientoDetalle();
            $asiento_detalle->cuenta_id = $cuentaProvExterior;
            $asiento_detalle->monto_debe = 0.0;
            $asiento_detalle->monto_haber = $itemsTr['totalitemexterior'];;
            $asiento_detalles[] = $asiento_detalle;
            $totalHaber += round($asiento_detalle->monto_haber);

            $asiento_detalle = new AsientoDetalle();
            $asiento_detalle->cuenta_id = $cuentaHaber;
            $asiento_detalle->monto_debe = 0.0;
            $asiento_detalle->monto_haber = round($itemsTr['totalitem']) + round($ivasTr['total']) - round($itemsTr['totalitemexterior']);
            $asiento_detalles[] = $asiento_detalle;
            $totalHaber += round($asiento_detalle->monto_haber);

            $dataProvider =
                new ArrayDataProvider([
                    'allModels' => $asiento_detalles,
                    'pagination' => false,
                ]);
            //FIN//

            if ($dataProvider->allModels != null) {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'hover' => true,
                    'id' => 'grid',
                    'panel' => ['type' => 'primary', 'heading' => 'Asientos', 'footer' => false,],
                    'toolbar' => [
                        'content' => ''
                    ]
                ]);
                echo Html::beginTag('div', ['class' => 'parent']);
                echo Html::beginTag('div', ['class' => 'float-right child']);
                echo Html::label('Total Haber: ', 'total-haber-disp');
                echo NumberControl::widget([
                    'name' => 'total-haber',
                    'id' => 'total-haber',
                    'readonly' => true,
                    'value' => $totalHaber,
                    'class' => 'form-control',
                    'maskedInputOptions' => [
                        'prefix' => '₲ ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'digits' => 0
                    ],
                    'displayOptions' => [
                        'class' => 'form-control kv-monospace float-right',
                        'placeholder' => 'Total Haber...',
                        'style' => [
                            'font-weight' => 'bold',
                        ]
                    ]
                ]);
                echo Html::endTag('div');
                echo Html::beginTag('div', ['class' => 'float-right child']);
                echo Html::label('Total Debe: ', 'total-debe-disp');
                echo NumberControl::widget([
                    'name' => 'totalcdebe',
                    'id' => 'total-debe',
                    'readonly' => true,
                    'value' => $totalDebe,
                    'class' => 'form-control',
                    'maskedInputOptions' => [
                        'prefix' => '₲ ',
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'digits' => 0
                    ],
                    'displayOptions' => [
                        'class' => 'form-control kv-monospace float-right',
                        'placeholder' => 'Total Debe...',
                        'style' => [
                            'font-weight' => 'bold',
                        ]
                    ]
                ]);
                echo Html::endTag('div');
                echo Html::tag('br');
                echo Html::tag('br');
                echo Html::tag('br');
                echo Html::tag('br');
                echo Html::endTag('div');
            }
//
        } catch (Exception $e) {
            Pjax::end();
            throw $e;
        }
        Pjax::end();
    }

    echo $form->field($model, 'guardarycerrar')->hiddenInput()->label('');

    echo '
    <div class="form-group">
        ' .
        Html::submitButton('Guardar y Salir', ['class' => 'btn btn-success', 'id' => 'guardar_salir_id']) . ' ' .
        Html::submitButton('Guardar y Mostrar PDF', ['class' => 'btn btn-primary', 'id' => 'guardar_mostrar_pdf_id']) . '
    </div>';
} catch (Exception $e) {
    echo $e->getMessage();
}

$CSS = <<<CSS
.float-right {
  float: right;
}
.child {
  /*border: 1px solid indigo;*/
  padding-left: 1rem;
}
CSS;
$this->registerCss($CSS);

$JS = <<<JS
$(document).ready(function () {
    { // Alinea a la derecha los nombres de las cuentas del 'debe'.
        let gridAsiento = $('#grid_asientos_detalle');
        if (gridAsiento.length) {
            $(gridAsiento[0].getElementsByTagName('tr')).each(function () {
                if (typeof $(this)[0].getElementsByTagName('td')[0] !== 'undefined') {
                    if ($(this)[0].getElementsByTagName('td').length > 3 && $(this)[0].getElementsByTagName('td')[3].textContent === '0') {
                        $($(this)[0].getElementsByTagName('td')[2]).css('text-align', 'right');
                    }
                }
            });
        }
    }
});

$("#guardar_salir_id").on("click", function (e) {
    $("#importacion-guardarycerrar").val("si");
});

$("#guardar_mostrar_pdf_id").on("click", function (e) {
    $("#importacion-guardarycerrar").val("no");
});
JS;

$this->registerJs($JS);