<?php

use kartik\builder\FormGrid;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Compra */

try {
    echo '<legend class="text-info"><small>Carga de Facturas / Comprobantes</small></legend>';

    echo $this->render('_fieldset_facturas_exterior',
        [
            'model' => $model,
            'form' => $form,
            'porcentajes_iva' => $porcentajes_iva,
            'porcentajes_iva_json' => $porcentajes_iva_json,
            'actionId' => $actionId
        ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

