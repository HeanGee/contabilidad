<?php
/**
 * Created by PhpStorm.
 * User: hdmedina
 * Date: 18/12/18
 * Time: 11:40 PM
 */

use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use common\helpers\ValorHelpers;

/**
 * @var \backend\modules\contabilidad\models\Importacion $model
 */

$valorDespacho = 0;
$recambio = 0;

$valorDespacho = $model->getValorDespacho();

$empresa = $model->empresa;
if ($empresa->tipo == 'juridica') {
    $razonSocial = $empresa->razon_social;
} else {
    $razonSocial = $empresa->primer_apellido . ' ' . $empresa->segundo_apellido . ', ' . $empresa->primer_nombre . ' ' . $empresa->segundo_nombre;
}
?>

<div style="width: 100%;">
    <br/>
    <h1 class="titulo1"><?= 'MERCADERIAS IMPORTADAS' ?></h1>

    <table width="100%">
        <tbody>
        <tr>
            <td>
                <h1 class="titulo2"><?= 'EMPRESA: ' . strtoupper($razonSocial) ?></h1>
            </td>
            <td>
                <h1 class="titulo2"><?= 'VIA: ' . strtoupper($model->medio_transporte) ?></h1>
            </td>
        </tr>
        <tr>
            <td>
                <h1 class="titulo2"><?= 'EXPORTADOR: ' . strtoupper($model->exportador) ?></h1>
            </td>
            <td>
                <h1 class="titulo2"><?= 'CONDICIÓN: ' . strtoupper($model->condicion) ?></h1>
            </td>
        </tr>
        </tbody>
    </table>

    <h1 class="titulo3"><?= 'DESPACHO N°: ' . $model->numero_despacho ?></h1>
    <h1 class="titulo3"><?= 'FECHA: ' . Yii::$app->formatter->asDate($model->fecha, 'd-MM-Y') ?></h1>

    <table border="1" style="border-collapse: collapse; width: 100%;">
        <tbody>
        <tr>
            <th class="cabecera">Ítem</th>
            <th class="cabecera">Monto</th>
            <th class="cabecera">Cotización</th>
            <th class="cabecera">Total</th>
        </tr>
        <tr>
            <td>Valor Despacho</td>
            <td></td>
            <td></td>
            <td class='monto'><?= ValorHelpers::numberFormatMonedaSensitive(round($valorDespacho)) ?></td>
        </tr>

        <?php
        $result = $model->getItemsLocalExteriorTr();
        echo $result['trblock'];
        $totalItem = $result['totalitem'];
        $totalItemRecambio = $result['totalItemRecambio'];

        $recambio = ValorHelpers::numberFormatMonedaSensitive($totalItem / $totalItemRecambio);
        ?>
        <tr>
            <td style="border: none;"></td>
            <td style="border: none;"></td>
            <td><b>TOTAL:</b></td>
            <td class='monto'><b><?= ValorHelpers::numberFormatMonedaSensitive(round($totalItem)) ?></b></td>
        </tr>
        </tbody>
    </table>
    <h6><b>RECAMBIO: </b><?= $recambio ?></h6>

    <h1 class="titulo2"><?= 'IVA' ?></h1>
    <table border="1" style="border-collapse: collapse; width: 100%;">
        <tbody>
        <tr>
            <th class="cabecera">Ítem</th>
            <th class="cabecera">Total</th>
        </tr>
        <tr>
            <td>IMPUESTO</td>
            <td class='monto'><?= ValorHelpers::numberFormatMonedaSensitive($model->iva) ?></td>
        </tr>
        <?php
        $resultIva = $model->getIvasTr();
        echo $resultIva['trblock'];
        $total = $resultIva['total'];
        ?>
        <tr>
            <td><b>TOTAL</b></td>
            <td class='monto'><b><?= ValorHelpers::numberFormatMonedaSensitive(round($total)) ?></b></td>
        </tr>
        </tbody>
    </table>


    <?php
    $resultRetenciones = $model->getRetencionesTr();
    if (($model->condicion == 'cif' or $model->condicion == 'fob') && !empty($resultRetenciones['rentatr'])) {
        echo '<h1 class="titulo2">RETENCIÓN RENTA</h1>';
        echo '
            <table border="1" style="border-collapse: collapse; width: 100%;">
                <tbody>
                    <tr>
                        <th class="cabecera">Ítem</th>
                        <th class="cabecera">Monto</th>
                        <th class="cabecera">%</th>
                        <th class="cabecera">Renta 3%</th>
                        <th class="cabecera">Cotización</th>
                        <th class="cabecera">Total</th>
                    </tr>
                        ' . $resultRetenciones['rentatr'] . '
                        <tr>
                        <th class="monto" style="border: none;"></th>
                        <th class="monto" style="border: none;"></th>
                        <th class="monto" style="border: none;"></th>
                        <th class="monto" style="border: none;"></th>
                        <th class="monto">TOTAL</th>
                        <th class="monto">' . ValorHelpers::numberFormatMonedaSensitive($resultRetenciones['totalRetencionRenta']) . '</th>
                    </tr>
                </tbody>
            </table>';
    }

    if ($model->condicion == 'fob' && !empty($resultRetenciones['ivatr'])) {
        echo '<h1 class="titulo2">RETENCIÓN IVA</h1>';
        echo '
        <table border="1" style="border-collapse: collapse; width: 100%;">
            <tbody>
                <tr>
                    <th class="cabecera">Ítem</th>
                    <th class="cabecera">Monto</th>
                    <th class="cabecera">%</th>
                    <th class="cabecera">Renta 3%</th>
                    <th class="cabecera">Cotización</th>
                    <th class="cabecera">Total</th>
                </tr>
                ' . $resultRetenciones['ivatr'] . '
                <tr>
                    <th class="monto" style="border: none;"></th>
                    <th class="monto" style="border: none;"></th>
                    <th class="monto" style="border: none;"></th>
                    <th class="monto" style="border: none;"></th>
                    <th class="monto">TOTAL</th>
                    <th class="monto">' . ValorHelpers::numberFormatMonedaSensitive($resultRetenciones['totalRetencionIva']) . '</th>
                </tr>
            </tbody>
        </table>';
    }

    echo !empty($resultRetenciones['totalpagar']) ? '<h1 class="tituloDerecha">TOTAL A PAGAR: ' . ValorHelpers::numberFormatMonedaSensitive(round($resultRetenciones['totalpagar'])) . '</h1>' : '';

    if ($model->iracis_generaral != null && $model->iracis_generaral != 0) {
        echo '
        <br>
        <table border="1" style="border-collapse: collapse; width: 100%;">
            <tbody>
                <tr>
                    <th class="cabecera">Retención IRACIS</th>
                    <th class="monto">' . ValorHelpers::numberFormatMonedaSensitive($model->iracis_generaral) . '</th>
                </tr>
            </tbody>
        </table>';
    }
    ?>
</div>
