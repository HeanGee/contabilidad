<?php

use kartik\checkbox\CheckboxX;
use yii\helpers\Html;

$importacionDetalle->loadDefaultValues();
?>
    <td>
        <?= $form->field($importacionDetalle, "[{$key}]plantilla_factura_id")
            ->textInput([
                'class' => 'form-control factura_compra_local',
                'id' => 'local-' . $key . '-plantilla_factura_id',
                'name' => 'local[' . $key . '][plantilla_factura_id]'
            ])->label(false) ?>
    </td>
    <td>
        <?= $form->field($importacionDetalle, "[{$key}]monto")
            ->textInput([
                'class' => 'form-control monto_local',
                'id' => 'local-' . $key . '-monto',
                'name' => 'local[' . $key . '][monto]',
                "disabled" => true,
            ])->label(false) ?>
    </td>

<?php
/** @var array $porcentajes_iva */
foreach ($porcentajes_iva as $ic) {
    echo '
        <td>
            ' . $form->field($importacionDetalle, "[{$key}]ivas[iva_" . $ic . "]")
            ->textInput([
                "class" => "form-control monto_local",
                "disabled" => true,
                'id' => 'local-' . $key . '-ivas-iva_' . $ic,
                'name' => 'local[' . $key . '][ivas][iva_' . $ic . ']'
            ])->label(false)
        . '</td>
    ';
}
?>

    <td>
        <?= $form->field($importacionDetalle, "[{$key}]cotizacion")
            ->textInput([
                'class' => 'form-control cotizacion_local',
                'id' => 'local-' . $key . '-cotizacion',
                'name' => 'local[' . $key . '][cotizacion]',
                "disabled" => true,
            ])->label(false) ?>
    </td>

    <td style="text-align: center;">
        <div>
            <div style="float:left; margin-left: 5%;">
                <?php
                echo $form->field($importacionDetalle, "[{$key}]absorber_impuesto")->checkbox([
                    'class' => 'form-control absorber_impuesto',
                    'id' => 'local-' . $key . '-absorber_impuesto',
                    'name' => 'local[' . $key . '][absorber_impuesto]',
                ])->label('Abs. Impuesto')
                ?>
            </div>
            <?php
            $displayCosto = 'none';
            if ($importacionDetalle->plantilla_id != null && strpos($importacionDetalle->plantilla->para_importacion, 'otros_gastos')) {
                $displayCosto = 'true';
            } ?>
            <div style="float:left; margin-left: 5%; display: <?= $displayCosto ?> ">
                <?php
                echo $form->field($importacionDetalle, "[{$key}]incluir_costo")->checkbox([
                    'class' => 'form-control incluir_costo',
                    'id' => 'local-' . $key . '-incluir_costo',
                    'name' => 'local[' . $key . '][incluir_costo]',
                ])->label('Incl. Costo')
                ?>
            </div>
            <div style="float: right; margin-right: 5%;">
                <?= Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', 'javascript:void(0);', [
                    'class' => 'local-iva-cta-usada-remove-button btn btn-danger btn-sm',
                    'id' => 'local-' . $key . '-remove',
                    'title' => 'Borrar esta fila'
                ]) ?>
            </div>
            <br>
        </div>
    </td>
<?php
$js = <<<JS
    $("#local_$key-absorber_impuesto").parent().attr("data-toggle","tooltip").attr("title","Este campo hace que el impuesto sea absorbido como costo.");
JS;
$this->registerJs($js);
?>