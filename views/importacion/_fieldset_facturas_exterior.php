<?php
/* @var $model \backend\modules\contabilidad\models\Importacion
 * @var IvaCuenta $iva_cuenta
 */

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\ImportacionDetalleFacturaLocalExterior;
use backend\modules\contabilidad\models\IvaCuenta;
use kartik\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

?>

    <fieldset>
        <legend>&nbsp;&nbsp; Facturas del Exterior
            <?= Html::a(
                '<i class="glyphicon glyphicon-plus"></i>',
                'javascript:void(0);',
                [
                    'id' => 'factura-exterior-new-button',
                    'class' => 'pull-left btn btn-success'
                ]
            ); ?>
        </legend>
        <table id="factura-exterior" class="table table-condensed table-bordered" style="table-layout: fixed;">
            <thead>
            <tr>
                <th class="texto_centrado" style="width: 40%;">Facturas/Comprobantes</th>
                <th class="texto_centrado" style="width: 15%;">Monto</th>

                <?php
                $divisor = 40 / sizeof($porcentajes_iva);
                foreach ($porcentajes_iva as $ic) {
                    if ($ic == 0)
                        echo '<th class="texto_centrado"  style="width: ' . 15 . '%;">Exenta</th>';
//                    else
//                        echo '<th class="texto_centrado"  style="width: ' . $divisor . '%;">Total ' . $ic . '%</th>';
                } ?>
                <th class="texto_centrado" style="width: 15%;">Cotización</th>
                <th class="texto_centrado" style="width: 15%;">Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            //TODO solo traer facturas del exterior que no estén en otra importación
            //TODO Factura credito, exenta
            $facturaExteriorLista = Compra::getFacturasByImportacion(false, false, []);
            $factura_exterior_data = !isset($facturaExteriorLista) ? [] : Json::encode($facturaExteriorLista);
            $importacionDetalle = new ImportacionDetalleFacturaLocalExterior();
            $importacionDetalle->loadDefaultValues();

            if ($actionId == 'create')
                $url_get_data_factura = Json::htmlEncode(\Yii::t('app', Url::to(['importacion/get-data-factura', 'local' => false])));
            else
                $url_get_data_factura = Json::htmlEncode(\Yii::t('app', Url::to(['importacion/get-data-factura', 'local' => false, 'importacionId' => $model->id])));

            //Agregar facturas existentes TODO
            $i = 0;
            /** @var ImportacionDetalleFacturaLocalExterior $exterior */
            foreach ($model->arrayExterior as $key => $exterior) {
                $i++;
//                Yii::warning('entra a for', $i);
                echo '<tr>';
                echo $this->render('_form-exterior-iva-cuenta-usada', [
                    'key' => 'new' . $i,
                    'form' => $form,
                    'importacionDetalle' => $exterior,
                    'porcentajes_iva' => $porcentajes_iva,
                ]);
                echo '</tr>';

                $digists = 0;
                if ($exterior->facturaCompra->moneda->tiene_decimales == 'si')
                    $digists = 2;

                $_s = <<<JS
                    generarEventoChangeFactura("exterior-new" + $i + "-plantilla_factura_id", false);
                    if (typeof idsexterior === 'undefined') {
                        let idsexterior = '';
                    }
                    idsexterior = ["exterior-new"+$i+"-ivas-iva_0", "exterior-new"+$i+"-monto", "exterior-new"+$i+"-cotizacion"];
                    construirMaskedInput($digists, null, idsexterior);
                    $("#exterior-new"+$i+"-ivas-iva_0").val($("#exterior-new"+$i+"-monto").val());
JS;
                $this->registerJs($_s);
            }
            ?>
            </tbody>
        </table>
    </fieldset>
<?php

$html = $this->render('_form-exterior-iva-cuenta-usada', [
    'key' => '__id__',
    'form' => $form,
    'importacionDetalle' => $importacionDetalle,
    'porcentajes_iva' => $porcentajes_iva,
]);

// remover todos los \n \r y combinacion de ellos segun:
// https://stackoverflow.com/questions/5258543/remove-all-the-line-breaks-from-the-html-source
$output = str_replace(array("\r\n", "\r"), "\n", $html);
$lines = explode("\n", $output);
$substrings = array();

foreach ($lines as $ii => $line) {
    if (!empty($line))
        $substrings[] = trim($line);
}
$html_without_enter_and_spaces = implode($substrings);
$bluePrint = Json::encode($html_without_enter_and_spaces);

$script =
    <<<JS
var exterior_iva_cta_usada_k = $i;
$('#factura-exterior-new-button').on('click', function (e) {
    console.log($i);
    exterior_iva_cta_usada_k += 1;
    $('#factura-exterior').find('tbody')
        .append('<tr>' +
         ($bluePrint).replace(/__id__/g, 'new' + exterior_iva_cta_usada_k) + '</tr>');
    
    $.ajax({
        url: $url_get_data_factura,
        type: 'post',
        data: {
            seleccionados: getValuesInput('exterior', 'plantilla_factura_id')
        },
        success: function (data) {
            if(data.length > 0) {
                construirSelect2(getSelectorForTab(exterior_iva_cta_usada_k,'plantilla_factura_id','', 'exterior'), false, data);
            } else {
                $('#exterior-new' + exterior_iva_cta_usada_k + '-remove').click();
                krajeeDialog.alert('No existen facturas para agregar.');
                e.preventDefault();
                return false;
            }
        }
    });
    
    generarEventoChangeFactura("exterior-new" + exterior_iva_cta_usada_k + "-plantilla_factura_id", false);
});

// no se usa pero se deja como backup
$(document).on('click', '.exterior-iva-cta-usada-remove-button', function () {
    $(this).closest('tbody tr').remove();
});

$(document).ready(function () {
    let tabla_exterior_field = $('#factura-exterior');
    if(tabla_exterior_field[0].rows.length > 1){
        var plantillas_exterior = []; 
        $.ajax({
            url: $url_get_data_factura,
            type: 'post',
            data: {
                seleccionados: null
            },
            success: function (data) {
                plantillas_exterior = data;
            },
            async: false
        });
        
        //Se construye los select 2 de la tabla en caso de update
        for(let i = 1; i < tabla_exterior_field[0].rows.length; i++){
            let id_input = tabla_exterior_field[0].rows[i].cells[0].children[0].children[0].id.replace(/exterior-new/g, '').replace(/-plantilla_factura_id/g, '');
            construirSelect2(getSelectorForTab(id_input,'plantilla_factura_id','', 'exterior'), false, plantillas_exterior);
            removerValorDeArray(plantillas_exterior, tabla_exterior_field[0].rows[i].cells[0].children[0].children[0].value);
            
            let plantilla_factura_field = $('#exterior-new' + i + '-plantilla_factura_id');
            let cotizacion_field = $('#exterior-new' + i + '-cotizacion');
            if (plantilla_factura_field.select2('data')[0]['tiene_decimales'] === 'si'){
                cotizacion_field.prop('disabled', false);
            }else {
                cotizacion_field.prop('disabled', true);
                cotizacion_field.val(null);
            }
        }
    }
})
JS;
$this->registerJs($script); ?>