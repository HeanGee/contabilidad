<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Importacion */
/* @var $tabReporte int*/

$this->title = Yii::t('app', 'Importación Nro: {nro}', [
    'nro' => $model->numero,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Importaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="importacion-update">

    <?= $this->render('_form', [
        'model' => $model,
        'tabReporte' => $tabReporte
    ]) ?>

</div>
