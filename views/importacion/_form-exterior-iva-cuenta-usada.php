<?php

use yii\helpers\Html;

?>
<td>
    <?= $form->field($importacionDetalle, "[{$key}]plantilla_factura_id")
        ->textInput([
            'class' => 'form-control factura_compra_exterior',
            'id' => 'exterior-' . $key . '-plantilla_factura_id',
            'name' => 'exterior[' . $key . '][plantilla_factura_id]'
        ])->label(false) ?>
</td>
<td>
    <?= $form->field($importacionDetalle, "[{$key}]monto")
        ->textInput([
            'class' => 'form-control monto_exterior',
            'id' => 'exterior-' . $key . '-monto',
            'name' => 'exterior[' . $key . '][monto]',
            "disabled" => true,
        ])->label(false) ?>
</td>
<?php
/** @var array $porcentajes_iva */
foreach ($porcentajes_iva as $ic) {
    if ($ic == 0) {
        echo '
        <td>
            ' . $form->field($importacionDetalle, "[{$key}]ivas[iva_" . $ic . "]")
                ->textInput([
                    "class" => "form-control monto_exterior",
                    "disabled" => true,
                    'id' => 'exterior-' . $key . '-ivas-iva_' . $ic,
                    'name' => 'exterior[' . $key . '][ivas][iva_' . $ic . ']'
                ])->label(false)
            . '</td>
    ';
    }
}
?>

<td>
    <?= $form->field($importacionDetalle, "[{$key}]cotizacion")
        ->textInput([
            'class' => 'form-control cotizacion_exterior',
            'id' => 'exterior-' . $key . '-cotizacion',
            'name' => 'exterior[' . $key . '][cotizacion]',
            "disabled" => true,
        ])->label(false) ?>
</td>

<td style="text-align: center;">
    <?= Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', 'javascript:void(0);', [
        'class' => 'exterior-iva-cta-usada-remove-button btn btn-danger btn-sm',
        'id' => 'exterior-' . $key . '-remove',
        'title' => 'Borrar esta fila'
    ]) ?>
</td>