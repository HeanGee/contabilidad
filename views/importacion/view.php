<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\contabilidad\controllers\ImportacionController;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Importacion */

$this->title = 'Importación Nro: ' . $model->numero;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Importaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
    <div class="importacion-view">

        <p>
            <?= ImportacionController::getAcceso('update') ? Html::a(Yii::t('app', 'Editar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
            <?= ImportacionController::getAcceso('delete') ? Html::a(Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) : '' ?>
        </p>

        <?php
        try {
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
//                'id',
                    'numero',
                    'numero_despacho',
                    [
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->fecha, 'dd-MM-Y');
                        },
                        'attribute' => 'fecha',
                    ],
                    'estado',
                    'criterio_retencion',
                    'medio_transporte',
                    'cal_retencion_fact_local_flete_seguro',
                    //'empresa_id',
                    //'periodo_contable_id',
                ],
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        } ?>

    </div>
<?php
$css = <<<CSS
.detail-view>tbody>tr>th{
width: 30%;
}
CSS;
$this->registerCss($css);

?>