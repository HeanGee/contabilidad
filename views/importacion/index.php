<?php

use backend\modules\contabilidad\controllers\ImportacionController;
use backend\modules\contabilidad\models\Importacion;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ImportacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Importaciones');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="importacion-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= ImportacionController::getAcceso('create') ? Html::a('Registrar Importacion', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php
    $template = '';
    $template = $template . (ImportacionController::getAcceso('view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (ImportacionController::getAcceso('update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (ImportacionController::getAcceso('delete') ? '{delete}' : '');
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                'id',
                'numero',
                'numero_despacho',
                [ // the attribute
                    'attribute' => 'fecha',
                    // format the value
                    'format' => ['date', 'php:d-m-Y'],
                    'headerOptions' => ['class' => 'col-md-2'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha_',
                        'language' => 'es',
                        'pluginOptions' => [
                            'orientation' => 'bottom center',
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'weekStart' => 0,
                        ]
                    ])
                ],
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'vigente') ? 'success">Vigente' : '') . '</label>';
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => Importacion::getEstados(true),
//                        'hideSearch' => true,
                    ]),
                    'contentOptions' => ['style' => 'text-align:center;']
                ],
//                'criterio_retencion',
//                'medio_transporte',
                //'cal_retencion_fact_local_flete_seguro',
                //'empresa_id',
                //'periodo_contable_id',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center'],
                    'header' => 'Acciones',
                ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>
</div>
