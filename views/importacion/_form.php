<?php

use backend\modules\contabilidad\models\IvaCuenta;
use kartik\dialog\Dialog;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\tabs\TabsX;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInputAsset;

/* @var $this yii\web\View */
/* @var $model \backend\modules\contabilidad\models\Importacion */
/* @var $form yii\widgets\ActiveForm */
/* @var IvaCuenta $iva_cuenta */
/* @var $tabReporte int */

$actionId = Yii::$app->controller->action->id;
echo Dialog::widget();
?>

<div class="importacion-form">

    <?php $form = ActiveForm::begin(['id' => 'importacion_form']);

    //Select2 hide, para cargar estilos y js.
    //Se cargan estilos y js de MaskedInput
    try {
        echo '<div style="display: none;">';
        echo Select2::widget([
            'data' => [],
            'name' => 'state_10',
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        echo '</div>';

        MaskedInputAsset::register($this);
    } catch (Exception $e) {
    }

    $iva_cuentas = IvaCuenta::find()->all();
    foreach ($iva_cuentas as $iva_cuenta) $porcentajes_iva[] = $iva_cuenta->iva->porcentaje;
    asort($porcentajes_iva);
    $porcentajes_iva = array_values($porcentajes_iva);
    $porcentajes_iva_json = Json::encode($porcentajes_iva);

    $items = [
        [
            'label' => '<i class="glyphicon glyphicon-list"></i> Condiciones',
            'content' => $this->render('_tab_condiciones', ['model' => $model, 'form' => $form]),
            'active' => $tabReporte == 0,
            'linkOptions' => array('id' => 'condiciones_tab')
        ],
        [
            'label' => '<i class="glyphicon glyphicon-list"></i> Facturas del exterior',
            'content' => $this->render('_tab_facturas_exterior',
                [
                    'model' => $model,
                    'form' => $form,
                    'porcentajes_iva' => $porcentajes_iva,
                    'porcentajes_iva_json' => $porcentajes_iva_json,
                    'actionId' => $actionId
                ]),
            'visible' => true,
            'linkOptions' => array('id' => 'facturas_exterior_tab')
        ],
        [
            'label' => '<i class="glyphicon glyphicon-list"></i> Facturas Locales',
            'content' => $this->render('_tab_facturas_local',
                [
                    'model' => $model,
                    'form' => $form,
                    'porcentajes_iva' => $porcentajes_iva,
                    'porcentajes_iva_json' => $porcentajes_iva_json,
                    'actionId' => $actionId
                ]),
            'linkOptions' => array('id' => 'facturas_locales_tab')
        ],
        [
            'label' => '<i class="glyphicon glyphicon-list"></i> Reporte y Guardado',
            'content' => $this->render('_tab_reporte_guardado', ['model' => $model, 'form' => $form]),
            'linkOptions' => array('id' => 'reporte_guardado_tab'),
            'active' => $tabReporte == 1,
        ],

    ];

    try {
        echo TabsX::widget([
            'items' => $items,
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'id' => 'tabs'
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    ActiveForm::end(); ?>
</div>
<?php
$url_campos_totales = Json::htmlEncode(\Yii::t('app', Url::to(['importacion/enable-campos-totales'])));
$url_ivas_totales = Json::htmlEncode(\Yii::t('app', Url::to(['importacion/get-ivas-totales'])));
$url_get_data_factura = Json::htmlEncode(\Yii::t('app', Url::to(['importacion/get-data-factura'])));

$scripts = <<<JS
function construirSelect2(selector, disabled = true, select_data) {
    $(selector)
        .each(function () {
            let e = $(this);
            if (!$(e).data('select2')) {
                $(e).select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: select_data,
                    disabled: disabled
                });
            }
        });
}
/**
 * Construye y retorna un selector de jQuery.
 *
 * @param tab           String. Indica la tabla (concepto) cuyos campos se desea filtrar.
 * @param contains      String. Sirve para restringir al jQuery. Es el nro que representa la fila de la tabla.
 * @param field         String. Indica el campo sobre el cual construir el selector.
 *                      Normalmente es uno de los atributos del modelo.
 * @param classs        String. Sirve para construir el selector solamente por inputs que tengan la clase classs.
 * @param tab           String. Indica si es para facturas locales o exteriores.
 * @return {string}     El selector jQuery.
 */
function getSelectorForTab(contains = '', field = '', classs = '' , tab) {
    let selector = '';
    
    selector = ':input[id^="'+tab+'"][id*="new' + contains + '"]';

    if (field !== '') {
        selector = selector + '[id$="' + field + '"]';
    }
    if (classs !== '') {
        selector = selector + '.' + classs;
    }
    return selector.concat(':not([type*="hidden"])');
}
function construirMaskedInput(digits, clase=null, ids=null) {
    if (clase!==null) 
        $(':input.' + clase).inputmask({
            "alias": "numeric",
            "digits": digits,
            "groupSeparator": ".",
            "autoGroup": true,
            "autoUnmask": true,
            "unmaskAsNumber": true,
            "radixPoint": ",",
            "digitsOptional": false,
            "placeholder": "0",
            "rightAlign": true,
            readonly: true
        });
    else if (ids !== null && Array.isArray(ids)){
        let idstring = "#" + ids.join(", #");
        $(idstring).inputmask({
            "alias": "numeric",
            "digits": digits,
            "groupSeparator": ".",
            "autoGroup": true,
            "autoUnmask": true,
            "unmaskAsNumber": true,
            "radixPoint": ",",
            "digitsOptional": false,
            "placeholder": "0",
            "rightAlign": true,
            readonly: true
        });
    }
}
function getTotalCampoNombres(prefijo) {
    var porcentajes = $porcentajes_iva_json;
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}
function totalesIvaCamposShowHide(plantilla_id, prefijo) {
    $.ajax({
        url: $url_campos_totales,
        type: 'post',
        data: {
            plantilla_id: plantilla_id,
            prefijo_campo: prefijo
        },
        success: function (data) {
            var campos = getTotalCampoNombres(prefijo);
            for (let i = 0; i < campos.length; i++) {
                let id = '#' + campos[i];
                if (data.includes(id)) {
                    $(id).prop('disabled', false);
                } else {
                    $(id).val('');
                    $(id).prop('disabled', true);
                }
            }
        }
    });
}
function totalesIvaComplete(id_compuesta, prefijo) {
    $.ajax({
        url: $url_ivas_totales,
        type: 'post',
        data: {
            id_compuesta: id_compuesta,
            prefijo_campo: prefijo
        },
        success: function (data) {
            var campos = getTotalCampoNombres(prefijo);
            for (let i = 0; i < campos.length; i++) {
                let id = '#' + campos[i];
                if(typeof data[id] !== 'undefined') {
                    $(id).val(data[id]);
                } else {
                    $(id).val('');
                }
            }
        }
    });
}
function completeMontos(campo_id, local) {
    let campo_field = $('#' + campo_id);
    let digits = 0;
    
    if (campo_field.select2('data')[0]['tiene_decimales'] !== 'no')
        digits = 2;
    
    $('#' + campo_id.replace(/plantilla_factura_id/g, 'monto')).val(campo_field.select2('data')[0]['monto']);
    if (local){
        totalesIvaComplete(campo_field.val(), campo_id.replace(/plantilla_factura_id/g, 'ivas-iva_'));
    
        let ids = getTotalCampoNombres(campo_id.replace(/plantilla_factura_id/g, 'ivas-iva_'));
        ids.push(campo_id.replace(/plantilla_factura_id/g, 'monto'));
        ids.push(campo_id.replace(/plantilla_factura_id/g, 'cotizacion'));
        construirMaskedInput(digits, null, ids);
    }else {
        $('#' + campo_id.replace(/plantilla_factura_id/g, 'ivas-iva_0')).val(campo_field.select2('data')[0]['monto']);
        
        let ids = [campo_id.replace(/plantilla_factura_id/g, 'ivas-iva_0'), campo_id.replace(/plantilla_factura_id/g, 'monto'), campo_id.replace(/plantilla_factura_id/g, 'cotizacion')];
        construirMaskedInput(digits, null, ids);
    }
    
    let cotizacion_field = $('#' + campo_id.replace(/plantilla_factura_id/g, 'cotizacion'));
    if (campo_field.select2('data')[0]['tiene_decimales'] === 'si'){
        cotizacion_field.val(campo_field.select2('data')[0]['cotizacion']);
        cotizacion_field.prop('disabled', false);
    }else {
        cotizacion_field.prop('disabled', true);
        cotizacion_field.val(null);
    }
}

function mostrarOcultarCosto(campo_id, local) {
    let campo_field = $('#' + campo_id);
    let incluir_costo_field = $('#' + campo_id.replace(/plantilla_factura_id/g, 'incluir_costo'));
    if (campo_field.select2('data')[0]['para_importacion'].includes("otros_gastos") && local){
        incluir_costo_field.parent().parent().parent().parent().css("display", "block");
    } else {
        incluir_costo_field.parent().parent().parent().parent().css("display", "none");
        incluir_costo_field.prop( "checked", false );
    }
}

function generarEventoChangeFactura(campo_id, local) {
    $('#' + campo_id).on('change', function () {
        completeMontos(campo_id, local);
        mostrarOcultarCosto(campo_id, local);
    });
}
function getValuesInput(prefijo,contiene){
    let inputs = $(':input[id*="'+contiene+'"][id^="'+prefijo+'"]:not([id*="__id__"])');
    let ids = [];
    for (let i = 0; i < inputs.length; i++) {
        if (inputs[i].value !== '') 
            ids.push(inputs[i].value);
    }
    return ids;
}
function removerValorDeArray(array, value){
     for(let i = 0; i < array.length; i++){
         if (parseInt(array[i].id) === parseInt(value)) {
             array.splice(i, 1);
         }
     }
}

//Ante fallo de validación hacemos que vuelva a la pestaña indicada.
$('#importacion_form').on('afterValidate', function (event, messages, errorAttributes) {
    var scrollPosision = null;
    $(errorAttributes).each(function (key, attribute) {
        if (attribute.name === 'numero_despacho' || attribute.name === 'fecha' || attribute.name === 'medio_transporte' ||
        attribute.name === 'criterio_retencion'){
            $('#condiciones_tab').click();
            return false;
        }
        
        // Save the position of the first errored element
        scrollPosision = scrollPosision || $(attribute.input).offset().top;
    });
});
JS;
$this->registerJs($scripts);

$css = <<<CSS
.texto_centrado{
    text-align: center;
}
CSS;
$this->registerCss($css); ?>

