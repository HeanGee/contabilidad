<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 24/07/2018
 * Time: 10:14
 */

use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form kartik\form\ActiveForm */
/* @var $model Venta */
/* @var $key string */
/* @var $ivaCtaUsada VentaIvaCuentaUsada */
/* @var $porcentajesIva array */
/* @var $es_nota bool */
$es_nota = !empty($es_nota);
?>

<?php

if (Yii::$app->controller->id == 'venta')
    echo Html::tag('td', $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->textInput([])->label(false),
        ['id' => "plantillas-selector-field-{$key}", 'class' => 'plantilla-selector', "style" => "width: 25%;"]
    );
else {
    echo "<td id='plantillas-selector-field-{$key}' class='plantilla-selector' style='width: 25%;'>";
    echo $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->textInput([])->label(false);
    echo $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->hiddenInput([])->label(false);
    echo '</td>';
}

foreach ($porcentajesIva as $pi) {
    echo Html::tag('td', $form->field($ivaCtaUsada, "[{$key}]ivas[iva_{$pi}]")->textInput([
        "class" => "form-control agregarNumber",
        "clave" => "ivaMonto{$pi}",
        "readonly" => true,
        "disabled" => true,
    ])->label(false), ['class' => "column-iva-{$pi}", 'style' => "text-align: right; "]);
}

echo Html::tag('td', $form->field($ivaCtaUsada, "[{$key}]gnd_motivo")->textInput([
    "class" => "form-control",
    "readonly" => true,
    "disabled" => true,
])->label(false), ['class' => "", 'style' => "text-align: left; "]);

if (!$es_nota) {
    echo Html::tag('td',
        Html::button('<span class="fa fa-cubes" aria-hidden="true"></span>', [
            'class' => 'actfijo-manager btn btn-success modal-actfijo',
            'title' => 'Activos Fijos a vender.',
            'id' => "ventaivacuentausada-{$key}-actfijo_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['activo-fijo/manejar-desde-factura-venta']),
            'data-pjax' => '0',
        ]) . "&nbsp;" .
        Html::button('<span class="fa fa-paw" aria-hidden="true"></span>', [
            'class' => 'modal-act-bio-venta-manager btn btn-success',
            'title' => 'Activos Biológicos a vender.',
            'id' => "ventaivacuentausada-{$key}-actbio_venta_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['activo-biologico/vender-act-bio']),
            'data-pjax' => '0',
        ]) . "&nbsp;" .
        Html::button('<span class="fa fa-dollar" aria-hidden="true"></span>', [
            'class' => 'set_gnd_motivo_manager_btn btn btn-warning',
            'title' => 'GND Motivo',
            'id' => "ventaivacuentausada-{$key}-set_gnd_motivo_manager_btn",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['set_gnd-motivo']),
            'data-pjax' => '0',
        ]) . "&nbsp;" .
        Html::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', [
            'class' => 'iva-cta-usada-remove-button btn btn-danger',
            'id' => "iva-cta-usada-{$key}-remove-button",
        ]),
        ['style' => 'text-align: center; ']
    );
} else {
    echo '<td style="text-align: center; ">';
    echo Html::button('<span class="fa fa-cubes" aria-hidden="true"></span>', [
            'class' => 'actfijo-nota-credito-manager btn btn-success modal-actfijo-nota-credito',
            'title' => 'Activos Fijos a Recuperar',
            'id' => "ventaivacuentausada-{$key}-actfijo_for_nota_credito_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['activo-fijo/manejar-desde-nota-credito-venta']),
            'data-pjax' => '0',
        ]) . "&nbsp;" .
        Html::button('<span class="fa fa-paw" aria-hidden="true"></span>', [
            'class' => 'modal-act-bio-nota-credito-manager btn btn-success',
            'title' => 'Activos Biológicos a Recuperar',
            'id' => "ventaivacuentausada-{$key}-actbio_nota_credito_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['activo-biologico/retornar-act-bio']),
            'data-pjax' => '0',
        ]) . "&nbsp;";
    echo '</td>';
}
?>