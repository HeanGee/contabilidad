<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 07/06/2018
 * Time: 9:42
 */

/* @var $model backend\modules\contabilidad\models\Venta */

use backend\modules\contabilidad\models\ParametroSistema as ParamContabilidad;
use backend\modules\contabilidad\models\Venta;
use common\helpers\ParametroSistemaHelpers;
use yii\helpers\Json;
use yii\helpers\Url;


/** ***************************** VARIABLES ***************************** */

/** @var  \backend\modules\contabilidad\models\IvaCuenta $iva_por */
$url_datosFacturaForNota = Json::htmlEncode(\Yii::t('app', Url::to(['venta-nota/get-datos-facturas-by-id'])));
$es_nota = in_array($model->tipo, ['nota_credito', 'nota_debito']) ? $model->tipo : false;
$ivas_por = \backend\modules\contabilidad\models\IvaCuenta::find()->all();
$porcentajes_iva = [];
foreach ($ivas_por as $iva_por)
    $porcentajes_iva[] = $iva_por->iva->porcentaje;

asort($porcentajes_iva);
$porcentajes_iva = array_values($porcentajes_iva);
$porcentajes_iva = Json::encode($porcentajes_iva);
$actionId = Yii::$app->controller->action->id;
$moneda_base_id = ParametroSistemaHelpers::getValorByNombre('moneda_base') == null ? 1 : ParametroSistemaHelpers::getValorByNombre('moneda_base');
$prefijo_anterior = Yii::$app->session->has('cont_valores_usados') ? Yii::$app->session->get('cont_valores_usados')['timbrado_detalle_id'] : "";
$empresa_id = Yii::$app->session->get('core_empresa_actual');
$periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
$param_cont_key = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-fact_venta_paralelo";
$permitir_nro_manual = ParamContabilidad::getValorByNombre($param_cont_key); // devuelve si/no o ''

if ($permitir_nro_manual == '')
    $permitir_nro_manual = 'no';

$url = Json::htmlEncode(\Yii::t('app', Url::to(['detalle-venta/get-cotizacion'])));
$url_nextnro = Json::htmlEncode(\Yii::t('app', Url::to(['venta/get-next-nro'])));
$url_simular = Json::htmlEncode(\Yii::t('app', \yii\helpers\Url::to(['venta/simular'])));
$urlCalcDetalle = Json::htmlEncode(\Yii::t('app', Url::to(['venta/calc-detalle'])));
$url_getRazonSocialByRuc = Json::htmlEncode(\Yii::t('app', Url::to(['venta/get-razon-social-by-ruc'])));
$url_getMotivoNoVigencia = Json::htmlEncode(\Yii::t('app', Url::to(['venta/get-motivo-no-vigencia'])));
$url_getPrefijosEmpActual = Json::htmlEncode(\Yii::t('app', Url::to(['timbrado/get-prefs-emp-actual'])));
$url_getCondicionFromDocType = Json::htmlEncode(\Yii::t('app', Url::to(['venta/condicion-from-tipo-doc'])));

/** ***************************** JSCRIPT VARIABLES ***************************** */
$JQ_VARIABLES = <<<JS
var prefijo_selected = "$prefijo_anterior";
var permitir_nro_manual = "$permitir_nro_manual";
var action_id = "$actionId";
var campo_moneda = '#venta-moneda_id';
var campo_tipodoc = '#venta-tipo_documento_id';
var i;
if ("$es_nota".length) {
    var esNota = "$es_nota";
} else {
    const esNota = 0;
}
JS;

// Se espera que jamas lance excepcion porque si es propenso a tal, ya se captura en el test de isNotaCreable() del controller.
$tipodocNotaCreditoId = null;
$tipodocNotaDebitoId = null;
if (Yii::$app->controller->id == 'venta-nota') {
    $tipodocNotaCreditoId = Venta::getTipodocNotaCredito()->id;
    $tipodocNotaDebitoId = Venta::getTipodocNotaDebito()->id;
}

if ($es_nota && !empty($model->factura_venta_id)) {
    if (!empty($model->facturaVenta->tipoDocumento)) {
        $td_id = $model->facturaVenta->tipo_documento_id;
    } else {
        $_fac = $model->getFacturaVenta()->one();
        $td_id = $_fac->tipo_documento_id;
    }
    $_decimales = $model->moneda->tiene_decimales;
    $fecha_em_fac_orig = $model->tipo == 'nota_debito' ? date('d-m-Y', strtotime($model->facturaVenta->facturaVenta->fecha_emision)) : '';
    $JQ_VARIABLES .= <<<JS
var tipoDocumentoParaNota = $td_id;
var decimalesMonedaParaNota = "$_decimales";
var fechaEmisionFacturaOriginal = "$fecha_em_fac_orig";
JS;
} else {
    $JQ_VARIABLES .= <<<JS
var tipoDocumentoParaNota;
var decimalesMonedaParaNota;
var fechaEmisionFacturaOriginal;
JS;
}


/** ***************************** JSCRIPT FUNCTIONS ***************************** */
$JS_FUNCTIONS = <<<JS
// Cuando el documento es credito y la divisa es extranjera bloquear el input tasa de cambio (se usa si o si la tasa de la set)

function disableValorMoneda() {
    if (!esNota) {
        let element = $('#venta-moneda_id');
        let element2 = $("#venta-cotizacion_propia");
        let element3 = $("#venta-valor_moneda-disp");

        if (element.val() === "$moneda_base_id") {
            if (element2.val() !== 'no') element2.val('no').trigger('change'); // al cambiar a no, ya pone la cotizacion.
            element2.prop('disabled', true);
            element3.val("").trigger('change');
            $('.field-venta-valor_moneda').parent()[0].style.display = "none";
        }
        else {
            if ($('#venta-condicion').val() === 'credito' && element.val() !== "$moneda_base_id") {
                console.log('... y condicion es credito');
                element2.val('no').trigger('change'); // al cambiar a no, ya pone cotizacion
                element2.prop('disabled', true); // al cambiar a no, ya pone la cotizacion.
                $('.field-venta-valor_moneda').parent()[0].style.display = "";
            }
            else {
                element2.prop('disabled', false);
                $('.field-venta-valor_moneda').parent()[0].style.display = "";
                setCotizacion();
            }
        }
    }
}

// Show/Hide de campos necesarios cuando la condición de venta es 'credito'
function showHideCuotasField(condicion) {
    if (condicion === 'contado' || condicion === '') {
        $('.field-venta-cant_cuotas')[0].style.display = "none";
        $('.field-venta-fecha_vencimiento')[0].style.display = "none";

        $('#venta-cant_cuotas, #venta-cant_cuotas-disp').val('');
        $('#venta-fecha_vencimiento').val('');
    }
    else {
        console.log('es credito');
        if ($('.field-venta-cant_cuotas').length)
            $('.field-venta-cant_cuotas')[0].style.display = "";
        if ($('.field-venta-fecha_vencimiento').length)
            $('.field-venta-fecha_vencimiento')[0].style.display = "";

        // cliente quiere que precarge datos cuando condicion es cuota o credito.        
        // por defecto, cantidad cuota == 1
        $("#venta-cant_cuotas-disp").val('1').trigger('change');

        setFechaVencimiento();
    }
}

function setFechaVencimiento() {
    // por defecto, fecha vence en 30 dias posteriores a fecha emision.
    let monthNames = [
        "01", "02", "03",
        "04", "05", "06", "07",
        "08", "09", "10",
        "11", "12"
    ];
    let fecha_emision_string = $('#venta-fecha_emision').val().replace(new RegExp('-', 'g'), '/');
    fecha_emision_string = ((fecha_emision_string.split('/')).reverse()).join('/');
    let fecha_vencimiento = new Date(fecha_emision_string);
    fecha_vencimiento = new Date(new Date(fecha_vencimiento).setMonth(fecha_vencimiento.getMonth() + 1));

    // $.fn.kvDatepicker.defaults.format = 'dd-mm-yyyy';

    // $("#venta-fecha_vencimiento-disp").kvDatepicker("update", fecha_vencimiento.getUTCDate() + '-' +
    //     monthNames[fecha_vencimiento.getMonth()]+'-'+ // getMonth() retorna index de un array de 12 elementos.
    //     fecha_vencimiento.getFullYear());

    let dia = fecha_vencimiento.getUTCDate();
    if (dia < 10) dia = '0' + dia;
    $('#venta-fecha_vencimiento').val(
        dia + '-' +
        monthNames[fecha_vencimiento.getMonth()] + '-' + // getMonth() retorna index de un array de 12 elementos.
        fecha_vencimiento.getFullYear()
    ).trigger('change');
}

function obtenerTotalFactura() { // al modificar este, tambien modificar en _form_detalle.php
    var factura_total = 0.0;

    // calcular suma de todos los totales
    let ivas = getIvas();
    ivas.forEach(function (element) {
        $('td.column-iva-' + element + ' :input').each(function () {
            let id = '#' + $(this).attr('id');
            const regex = /#ventaivacuentausada-new([0-9]+)-/gm;
            let m = regex.exec(id);
            if (m !== null) {
                if ($(this).val() !== "")
                    factura_total += (parseFloat($(this).val()));
            }
        })
    });

    return factura_total;
}

function hayPlantillaSeleccionada() {
    let hay = false;
    $('td.plantilla-selector :input').each(function () {
        if ($(this).val() !== "") hay = true;
    });
    return hay;
}

function simular() {
    $.ajax({
        url: $url_simular,
        type: 'post',
        data: {
            moneda_id: $('#venta-moneda_id').val(),
            valor_moneda: $('#venta-valor_moneda-disp').val(),
            cont_factura_total: obtenerTotalFactura(),
            plantillas: getDataTable(),
            // hoy: !esNota ? $('#venta-fecha_emision').val() : (esNota === 'nota_debito' ? fechaEmisionFacturaOriginal : $('#venta-emision_factura').val()),
            hoy: !esNota ? $('#venta-fecha_emision').val() : $('#venta-emision_factura').val(),
            estado_comprobante: $('#venta-estado').val(),
            es_nota: esNota
        },
        success: function (data) {
            if (data !== "") {
                $.pjax.reload({container: "#asiento-simulator-div", async: false});
                $.pjax.reload({container: "#asiento-simulator-costo-div", async: false});
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
        }
    });
}

function readyToCalcDetalle() {
    if (['anulada', 'faltante'].includes($("#venta-estado").val())) return false;
    if ($('#venta-tipo_documento_id').val() === '') return false;
    let monedaField = $('#venta-moneda_id'); 
    if (monedaField.val() === '') return false;
    else if (monedaField.val() !== '1') {
        if($('#venta-valor_moneda').val() === '') return false;
    }
    
    //recorre plantillas y verifica si hay uno seleccionado y con monto distinto de cero
    let plantillaValueEmpty = true,
        plantillas = $('input[id$="plantilla_id"]'),
        cant_plantillas = plantillas.length,
        cant_plantillas_vacias = 0;
    plantillas.each(function(i, plantilla) {
        if ($(plantilla).val() === '') {
            cant_plantillas_vacias++;
            if (cant_plantillas_vacias === cant_plantillas) {
                plantillaValueEmpty = true;
                return false;
            }
        } else {
            let emptyValue = true;
            $(plantilla).parent().parent().parent().find('input:not([id$="plantilla_id"]):not([id$="gnd_motivo"])').each(function(j, element) {
                if (element.value !== '' && element.value > 0) {
                    emptyValue = false;
                    return false;
                }
            });
            if (!emptyValue) {
                plantillaValueEmpty = false;
                return false;
            }
        }
    });
    if (plantillaValueEmpty) return false;
    
    return true;
}

// calcula sumatoria de totales por iva, establece total de la factura y recarga los divs para simulaciones.
function setTotalFactura() { //console.log('setTotalFactura()');
    if (readyToCalcDetalle()) {
        // inhabilitar temporalmente boton submit
        $('#btn_submit_factura_venta').prop('disabled', true);
        $('#btn_submit_close_factura_venta').css('display', 'none');
        $('#btn_submit_factura_venta').html('Generando detalles y simulacion ... ');

        //calcular ivas y mostrar en los campos disableds de ivas
        // calcularIvasYRellenar();
        // $('#venta-factura_venta_id').select2('data')[0]
        let tipo_docu_id = !esNota ? $('#venta-tipo_documento_id').val() :
         ($('#venta-tipo').select2('data')[0]['id'] === "nota_credito" ? "{$tipodocNotaCreditoId}" : "{$tipodocNotaDebitoId}");
        $.ajax({
            url: $urlCalcDetalle,
            type: 'post',
            data: {
                moneda_id: $('#venta-moneda_id').val(),
                valor_moneda: $('#venta-valor_moneda' + (!esNota ? '-disp' : '')).val(),
                cont_factura_total: obtenerTotalFactura(),
                plantillas: getDataTable(),
                tipo_docu_id: tipo_docu_id,
                ivas: getIvas(),
                estado_comprobante: $('#venta-estado').val(),
                es_nota: esNota
                // totales_valor: getTotalFieldValues(),
                // plantilla_id: $('#venta-plantilla_id').val(),
            },
            success: function (data) {
                if (data !== '') {// si retorna falso, aquí se vé como ''. si retorna true, aquí se vé como 1.
                    $.pjax.reload({container: "#detallesventa_grid", async: false});
                    simular();
                } else {
                    $.pjax.reload({container: "#flash_message_id", async: false});
                    
                    $('#btn_submit_factura_venta').prop('disabled', false);
                    $('#btn_submit_close_factura_venta').css('display', '');
                    $('#btn_submit_factura_venta').html('Guardar y siguiente');
                }

                // re-habilitar boton submit LO REHABILITA EN _form_asientoSimulator_costo.php en document.onReady
                // $('#btn_submit_factura_venta').prop('disabled', false);
                // $('#btn_submit_factura_venta').html('Guardar y siguiente');
            }
        });
    }
}

//Retorna array con datos de la tabla de plantillas - ivas
function getDataTable() {
    let ivas = getIvas();
    let data_table = [];
    $('td.plantilla-selector :input:not([type="hidden"])').each(function () {
        if ($(this).val() !== "") { // tiene seleccionado una plantilla
            let id_prefijo = (/#ventaivacuentausada-new([0-9]+)-/g).exec('#' + $(this).prop('id'));
            if (id_prefijo != null) {
                let fila = {};
                for (let j = 0; j < ivas.length; j++) {
                    fila['iva_' + ivas[j]] = $(id_prefijo[0] + 'ivas-iva_' + ivas[j]).val();
                }
                fila["plantilla_id"] = $(this).val();
                data_table.push(fila);
            }
        }
    });
    return data_table;
}

function getIvas() {
    return Object.values(JSON.parse("$porcentajes_iva"))
}

function getTotalFieldNames() {
    const prefijo = "total-iva-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}

function getGravadasFieldNames() {
    const prefijo = "total-gravada-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}

function getTotalFieldValues() {
    var total_nombres = getTotalFieldNames();
    var i, total_valores = [];
    for (i = 0; i < total_nombres.length; i++) {
        total_valores.push($('#' + total_nombres[i] + '-disp').val());
    }
    return total_valores;
}

let campos = getTotalFieldNames();
let campos_g = getGravadasFieldNames();

for (i = 0; i < campos.length; i++) {
    campos[i] = '#' + campos[i] + '-disp'; // construir id
}

// funcion utilizada por disableFieldsByEstado()
function disableFields(disable, limpiar) {
    var campos_totales = getTotalFieldNames(), campos_gravadas = getGravadasFieldNames(), i;
    var selects = '#venta-moneda_id, #venta-cotizacion_propia, #venta-para_iva, #venta-obligacion_id';
    var texts = '#venta-cant_cuotas-disp, #venta-fecha_vencimiento, #venta-condicion' + (!esNota ? ', #venta-ruc' : '');

    let venta_estado = $('#venta-estado');
    let venta_fecha_emision = $('#venta-fecha_emision');

    // si action es create y estado es faltante, limpiar campos totales.
    if (limpiar /*&& venta_estado.val() === 'faltante'*/) {
        $(selects + ', ' + texts).val('').trigger('change');
        for (i = 0; i < campos_totales.length; i++) {
            // por campos totales
            let campo = document.getElementById(campos_totales[i] + '-disp');
            if (campo !== null) {
                $('#' + campos_totales[i] + '-disp').val('');
            }
            // por campos gravadas
            let campograv = document.getElementById(campos_gravadas[i] + '-disp'); // campos_gravadas puede contener: total-gravada-0-disp...
            if (campograv !== null) { // ...por eso se pregunta si el elemento Html existe o no.
                $('#' + campos_gravadas[i] + '-disp').val('');
            }
        }
    }

    $(texts).prop('readonly', disable);
    $(selects).prop('disabled', disable);
    
    venta_fecha_emision.prop('disabled', (venta_estado.val() === 'faltante'));
    if(venta_estado.val() === 'faltante') venta_fecha_emision.val('').trigger('change');
    
    $('#boton_add_timbrado').prop('disabled', disable);
    $('#iva-cta-usada-new-button').prop('disabled', disable);
    // $('button.iva-cta-usada-remove-button').prop('disabled', disable); // no hace falta si al eliminar la fila, desaparece tambien.

    if (disable === true) $('input.agregarNumber').prop('readonly', disable);
    else {
        // $('input.agregarNumber').prop('readonly', disable);
        $('td.plantilla-selector :input').each(function () {
            if ($(this).val() !== "") {
                // $(this).trigger('change'); // para que lance el evento que habilita los campos segun plantilla.
                let id = $(this).attr('id');
                let regex = /ventaivacuentausada-new([0-9]+)-/gm;
                let m = regex.exec(id);
                if (m !== null && m[1] !== null) {
                    habilitarCamposIvasSegunPlantilla('#' + m[0]);
                }
            }
        });
        
        // elegir la primera opcion para 'para_iva'
        let select2Opts = $('#venta-para_iva option');
        $('#venta-para_iva').data('select2').trigger('select', {data: {"id": select2Opts[1].value, "text": select2Opts[1].text}});
        
        // elegir la primera opcion para 'obligacio_id'
        select2Opts = $('#venta-obligacion_id option');
        select2Opts.each(function(index, option) {
            if (option.text.toLowerCase() === 'iracis') {
                $('#venta-obligacion_id').data('select2').trigger('select', {data: {"id": option.value, "text": option.text}});
                return false;
            }
        });
        
        select2Opts = $('#venta-tipo_documento_id option');
        select2Opts.each(function(index, option) {
            if (option.value === '1') {
                let field = $('#venta-tipo_documento_id');
                if (!disable && field.val() === '')
                    field.data('select2').trigger('select', {data: {"id": option.value, "text": option.text}});
                return false;
            }
        });
         
        select2Opts = $('#venta-moneda_id option');
        select2Opts.each(function(index, option) {
            if (option.value === '1') {
                let field = $('#venta-moneda_id');
                if (!disable && field.val() === '')
                    field.data('select2').trigger('select', {data: {"id": option.value, "text": option.text}});
                return false;
            }
        });
    }

    if (!esNota)
        $('td.plantilla-selector :input').prop('disabled', disable);

    var campo_monedavalor = $('#venta-valor_moneda-disp');
    if (campo_monedavalor.length) {
        if (disable === false) {
            //console.log('disableValorMoneda() llamando desde disableFields()');
            disableValorMoneda(); // para activar campo, hay que tener en cuenta varias condiciones.
        } else {
            // if (venta_estado.val() === 'faltante') $('button.iva-cta-usada-remove-button').click();
            campo_monedavalor.prop("readonly", disable);
            campo_monedavalor.val('');
        }
    }

    // faltante === anulada excepto en fecha_emision que es required para anulada.
    let campoObs = $('#venta-observaciones');
    if (venta_estado.val() !== 'vigente') {
        $('button.iva-cta-usada-remove-button').click();
        
        if (!esNota) {
            campoObs.parent().parent().parent().parent().css('display', '');
            if ('$actionId' === 'create') {
                $("#venta-tipo_documento_id").val('').trigger('change');
                let url = $url_getMotivoNoVigencia;
                $.ajax({url: url, type: 'get', success: function(result) {
                        console.log(result);
                        campoObs.val(result).trigger('change');
                        campoObs.focus();
                    }
                });
            }
        }
    } else {
        if (!esNota) {
            campoObs.parent().parent().parent().parent().css('display', 'none');
            campoObs.val('').trigger('change');
            if ('$actionId' === 'create') $("#venta-tipo_documento_id").val('').trigger('change');
        }
    }

    // desactivar campos totales
    for (i = 0; i < campos_totales.length; i++) {
        $('#' + campos_totales[i] + '-disp').attr('readonly', disable);
    }

    venta_estado.select2('close');
}

// Verificar estado de la factura y establece readonly de los campos.
function disableFieldsByEstado() {
    let venta_estado = $('#venta-estado');
    let includes = ['anulada', 'faltante'].includes(venta_estado.val());

    if (includes)
        disableFields(true, true);
    else
        disableFields(false, false);
}

// Metodo replaceAt personalizado
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
};

// function calcularIvasYRellenar() {
//     let ctotales = getTotalFieldNames(), cgravadas = getGravadasFieldNames(), ivas = getIvas();
//     let totales_iva = 0.0, i;
//     for (i = 0; i < ctotales.length; i++) {
//         let iva = ivas[i];        
//         if (iva !== '0') {
//             let ctotal = document.getElementById(ctotales[i] + '-disp');
//             let cgrav = document.getElementById(cgravadas[i] + '-disp');
//             if (ctotal !== null && cgrav !== null) {
//                 cgrav.value = ctotal.value - (ctotal.value / (1.0 + iva / 100.0));
//                 if ($('#venta-moneda_id').val() === '1') {
//                     cgrav.value = Math.round(cgrav.value);
//                 }
//                 totales_iva += cgrav.value;
//             }
//         }
//     }
//
//     if ($('#venta-moneda_id').val() === '1') {
//         totales_iva = Math.round(totales_iva);
//     }
//     $('#venta-_total_ivas-disp').val(totales_iva);
// }
function getTipoDocumento() {
    return esNota ? $('#venta-tipo').select2('data')[0]['tipo_doc_set_id'] : $('#venta-tipo_documento_id').val();
}

function cargarPrefijos() {
    let fecha_factura = $('#venta-fecha_emision').val();
    fecha_factura = ((fecha_factura.split('-')).reverse()).join('-');
    let tim_det_id_pref = $('#venta-timbrado_detalle_id_prefijo');
    let selected = prefijo_selected === "" ? tim_det_id_pref.val() : prefijo_selected;
    let tim_vencido = $("#venta-usar_tim_vencido").val(); // 0: no checked,   1: checked.

    // Vaciar prefijos. RECORDAR UTILIZAR ESTE ESQUEMA DE VACIADO/RELLENADO que funciona bien
    tim_det_id_pref.select2("destroy");
    tim_det_id_pref.html("<option><option>");
    tim_det_id_pref.select2({
        theme: 'krajee',
        placeholder: '',
        language: 'en',
        width: '100%',
        data: [],
    });

    // if (fecha_factura !== "") {
    $.ajax({
        url: $url_getPrefijosEmpActual +'&q=&id=' + getTipoDocumento() + '&fecha_factura=' + fecha_factura + '&vencido=' + tim_vencido,
        type: 'get',
        data: {
            model_id: "$model->id",
            action_id: "$actionId",
            es_tipo_set: esNota,
        },
        success: function (data) {
            let arr = [{id: "", text: ''}]; // es el item necesario para que no se seleccione el primero por defecto.

            if (data.length === 0) {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
            else {
                // Construir options para select2
                data['results'].forEach(function (e, i) {
                    arr.push(e);
                });

                // Rellenar select2 con las opciones
                tim_det_id_pref.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: arr,
                });

                // si selected esta en el nuevo conj. de opciones, se selecciona de nuevo.
                //  de lo contrario, queda vacio solito.
                if (selected !== "")
                    tim_det_id_pref.val(selected).trigger('change');

            }
        },
    });
    // }
}

function manejarSeleccionFactura() {
    console.log('entro en manejarSeleccionFactura');
    let main_data = $('#venta-factura_venta_id').select2('data')[0];
    $('#venta-emision_factura').val(main_data['fecha_emision']);
    $('#venta-moneda_nombre').val(main_data['moneda_nombre']);
    $('#venta-moneda_id').val(main_data['moneda_id']);
    $('#venta-valor_moneda-disp').val(main_data['valor_moneda']).trigger('change');
    $('#venta-nota_remision').val(main_data['nota_remision']).trigger('change');
    tipoDocumentoParaNota = main_data['tipo_documento_id'];
    decimalesMonedaParaNota = main_data['tiene_decimales'];
    if (main_data['fecha_emision_factura_original'] != undefined) {
        fechaEmisionFacturaOriginal = main_data['fecha_emision_factura_original'];
    }
    $.ajax({
        url: $url_datosFacturaForNota,
        type: 'post',
        data: {
            factura_id: main_data['id'],
        },
        success: function (data) {
            $('#plantillas-table').find('tbody tr:visible').remove();
            completarTotales();
            $('#compra-total-disp').val('0').trigger('change'); // completarTotales() no me limpia este campo
            $.each(data, function (i, e) {
                let index = generarNuevoIvaCuentaUsada();
                $('#ventaivacuentausada-new' + index + '-plantilla_id').val(i).trigger('change').prop('disabled', true);
                $.each(e, function (key, value) {
                    let _temp = $('#ventaivacuentausada-new' + index + '-ivas-iva_' + key);
                    _temp.val(value);
                    _temp.trigger('change');
                    _temp.inputmask("option", {max: decimalesMonedaParaNota === 'si' ? parseFloat(value) : parseInt(value)});
                    _temp.trigger('keyup');
                });
            });
        }
    });

}
JS;


$url_getDefaultDetalle = Json::htmlEncode(\Yii::t('app', Url::to(['venta/get-default-detalles'])));
/** ***************************** JSCRIP DOCUMENT ON READYs ***************************** */
$JS_DOC_ON_READYs = <<<JS
// Código estático recurrente para abrir modales.
$(document).on('click', '.tiene_modal', (function () {
    var boton = $(this);
    var title = boton.data('title');
    $.get(
        boton.data('url'),
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));
$(document).on('click', '.tiene_modal2', (function () {
    var boton = $(this);
    var title = boton.data('title');
    var select2Prefijo = $('#venta-timbrado_detalle_id_prefijo');
    var hayOpciones = select2Prefijo.select2('data').length !== 0;
    var prefijoSinGuion = hayOpciones ? select2Prefijo[0].options[select2Prefijo[0].selectedIndex].text.split('-') : "";
    let extra_params = '&prefijo=' + prefijoSinGuion + '&tipoDocumentoId=' + getTipoDocumento();
    if (esNota)
        extra_params += '&esTipoSet=1';
    $.get(
        boton.data('url') + extra_params,
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

$(document).on('click', '.modal-actfijo', (function (evt) {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    let id_field_plantilla = '#' + boton_id.replace(/actfijo_manager_button/g, 'plantilla_id');
    let plantilla_id = $(id_field_plantilla).val();
    let fecha_factura = $('#venta-fecha_emision').val();
    fecha_factura = ((fecha_factura.split('-')).reverse()).join('-');
    
    /** -------------- VERIFICAR QUE TENGA MONTO -------------- */
    let row = boton.parent().parent();
    let row_total = 0.0;
    let row_inputs = row[0].getElementsByClassName('agregarNumber');
    $(row_inputs).each(function (i, e) {
        if (e.value !== "")
            row_total += parseFloat(e.value);
    });
    if (row_total === 0) {
        $("#modal").modal("hide");
        $("modal-body").html("");
        krajeeDialog.alert('Debe especificar un monto para esta fila.');
        evt.preventDefault();
        return false;
    }
    /** -------------------------------------------------------- */
    
    $.get(
        boton.data('url') + '&venta_id=' + "$model->id" + '&plantilla_id=' + plantilla_id + '&fecha_factura=' + fecha_factura + "&formodal=" + true + '&costo_adquisicion=' + row_total,
        function (data) {
            if (data !== "") {
                let modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                // if (title)
                $('.modal-title', modal).html(title);
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
                $("#modal").modal("hide");
                $("modal-body").html("");
            }
        }
    );
}));

// Código estático recurrente para abrir modales.
$(document).on('click', '.modal-act-bio-venta-manager', (function () {
    var boton = $(this);
    var title = boton.attr('title');
    let fila_id = boton.attr('id').replace('actbio_venta_manager_button', '');
    let deducible = $('#' + fila_id + 'plantilla_id').select2('data')[0]['deducible'];
    let campo_iva_activo = "";
    $(':input[id^="'+fila_id+'ivas-iva"]').each(function() {
        if ($(this).attr('disabled') !== 'disabled') {
            campo_iva_activo = $(this).attr('id'); return false;
        }
    });
    let url = boton.data('url') + "&venta_id={$model->id}&fila_id=" + fila_id + '&deducible=' + deducible + '&campo_iva_activo=' + campo_iva_activo;
    
    $.get(
        url,
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            $('.modal-header', modal).css('background', '#3c8dbc');
            $('.modal-title', modal).html(title);
            modal.modal();
        }
    );
}));

// Operaciones al borrar un detalle venta.
$(document).on('click', '.ajaxDelete', function (e) {
    e.preventDefault();
    var deleteUrl = $(this).attr('delete-url');
    krajeeDialog.confirm('Está seguro?',
        function (result) {
            if (result) {
                $.ajax({
                    url: deleteUrl,
                    type: 'post',
                    error: function (xhr, status, error) {
                        alert('There was an error with your request.' + xhr.responseText);
                    }
                }).done(function (data) {
                    $.pjax.reload({container: "#detallesventa_grid", async: false});
                    $(':button.btn-simular').click();
                });
            }
        }
    );
});

// Desplegar select2 automáticamente al obtener focus.
$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        let selector = $(this).siblings('select');
        if (!selector.prop('disabled'))
            $(selector).select2('open');
    }
});

$(document).on('click', '#detalle-defecto', function() {
    $.ajax({
        url: $url_getDefaultDetalle,
        type: 'get',
        data: {
            'venta_id': "{$model->id}"
        },
        success: function(result) {
            if (result.error) {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
            $.pjax.reload({container: "#detallesventa_grid", async: false});
            simular();
        },
    });
});

$(document).on('click', '.obtener-motivo', function(evt) {
    evt.preventDefault();
    evt.stopImmediatePropagation();
    
    if ($(this).parent().parent().parent().css('display') !== 'none') {
        let url = $url_getMotivoNoVigencia;
        let campoObs = $('#venta-observaciones');
        $.ajax({url: url, type: 'get', success: function(result) {
                console.log(result);
                campoObs.val(result).trigger('change');
                campoObs.focus();
            }
        });
    }
});

$(document).ready(function () {
    // Para que carge el nro_factura con el siguiente
    let prefijoField = $('#venta-timbrado_detalle_id_prefijo');
    if (prefijoField.val().length) prefijoField.trigger('change');
    
    // Para que ponga la razon_social en el campo
    let rucField = $('#venta-ruc');
    if (rucField.val().length) rucField.trigger('change');
    
    let condicionField = $('#venta-condicion');
    if (condicionField.length) condicionField.parent().parent()[0].style.display="none";
    
    let condicion = condicionField.val();
    if (!esNota) {
        //console.log('llamando disableValorMoneda() desde document.ready()');
        disableFieldsByEstado();
        // 22 febrero 2019, Viernes: Se comenta porque ya se ejecuta desde dentro de disableFieldsByEstado.
        // 22 febrero 2019: Ademas, antes de comentar, no se desactivaba el campo cotizacion_propia al editar venta no vigente pero al desactivar ya hace bien.
        // disableValorMoneda(); // ya se encuentra dentro de disableFields() que esta dentro de disableFieldsByEstado()
        cargarPrefijos();
        // Mostrar/ocultar campos para cuotas al inicio del formulario
        showHideCuotasField(condicion);
    } else {
        $('#nota_form').on('beforeSubmit', function() {
            $('.select2-hidden-accessible').prop('disabled', false);
        });
        let plantillas_table = $('#plantillas-table');
        plantillas_table.find('input[id*="-ivas-iva_"]:first').trigger('keyup'); // trampa para que calcule el total de factura
        $('[id$="-plantilla_id"]', plantillas_table).prop('disabled', true);
        // if (!isNewRecord) {
        //     $('#venta-tipo').prop('disabled', true);
        //     $('#venta-factura_venta_id').prop('disabled', true);
        // }
    }

    // Readonly al campo nro_factura
    let campo_nro_factura = $('#venta-nro_factura');
    let campo_tipodoc = $('#venta-tipo_documento_id');
    campo_nro_factura.prop('readonly', (permitir_nro_manual === 'no'));
    // $('#venta-nro_factura').prop('disabled', true); // si es disabled, no va al post.

    // Desplegar select2 de tipo_documento si es para crear.
    // if ($('#venta-tipo_documento_id').length && $('#venta-tipo_documento_id').val().length === 0)
    campo_tipodoc.select2('open');
    if (("$actionId") === 'update') {
        campo_tipodoc.select2('close');
        $.pjax.reload({container: "#flash_message_id", async: false}); // para mostrar msg si la plantilla no pertenece mas a la obligacion
    }
    
    // Mostrar/ocultar campos para cuotas al inicio del formulario
    // showHideCuotasField(condicion);

    // Incrustar boton entre campos.
    var a = campo_nro_factura.parent().parent()[0];
    $('<div id = "button_facturaventa_addtimbrado" style="display: block;" class="col-sm-1"><div class="form-group field-button_facturaventa_addtimbrado" style="text-align: center;">$boton<div class="help-block"></div></div></div>').insertAfter(a);
    if (campo_tipodoc.val() === '') {
        $('#boton_add_timbrado').prop('disabled', true);
    }

    if (action_id === 'update') {
        // simular();
        $('#detalle-defecto').click();
    } else {
        // Agregar una fila de plantilla si es create y dejar todos los campos en readonly.
        if ($('tr.fila-data').length === 0) 
            $('#iva-cta-usada-new-button').click();
        let pattern = '-ivas-iva_';
        $("input[id*=" + pattern + "]").prop('readonly', true);
        $('input[id$="plantilla_id"]').each(function (i, e) {
            if ($(this).val() !== '')
                $(this).trigger('change');
        })
    }
});

var actionid = "$actionId";
if (actionid === 'update') {
    // forzar selección del prefijo si es update.
    $('select#venta-timbrado_detalle_id_prefijo').find('option').each(function () { // recorrer todos los items del select2.
        if ($(this).text() === "$model->prefijo") {
            $('#venta-timbrado_detalle_id_prefijo').val($(this).val()).trigger('change');
            return false; // es como break;
        }
    });
}

// ocultar campo condicion al inicio, porque su presencia es necesario pero no para visualizar
//    y los campos totales, ocultar todos al inicio.
$(document).ready(function () {
//    if ($('#venta-condicion').length)
//        $('#venta-condicion').parent().parent()[0].style.display="none";
//    
//    disableFieldsByEstado();
//    
//    // Agregar una fila de plantilla si es create y dejar todos los campos en readonly.
//    let action_id = "$actionId";
//    if (action_id === "create") {
//        if ($('tr.fila-data').length === 0) 
//            $('#iva-cta-usada-new-button').click();
//        let pattern = '-ivas-iva_';
//        $("input[id*=" + pattern + "]").prop('readonly', true);
//    }
});
JS;


/** ***************************** JSCRIPT EVENTS ***************************** */

$url_get_plantilla = Json::htmlEncode(\Yii::t('app', Url::to(['plantilla-compraventa/get-plantilla'])));
$JS_EVENTS = <<<JS
$.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};

document.getElementById("venta-fecha_emision").onfocus = function() {
    $('#venta-fecha_emision').setCursorPosition(0);
};

$("#venta-usar_tim_vencido").on('change', function () {

    let value = $(this).val();

    let msg = "";

    if (value === '0') {
        msg = "Se eliminarán las plantillas agregadas actualment porque las mismas corresponden a Gastos No Deducibles.";
    } else {
        msg = "Se eliminarán las plantillas agregadas actualmente porque las mismas corresponden a Gastos No Deducibles.";
    }

    // if ($('.fila-data').length > 0) {
    //     krajeeDialog.confirm(msg, function (result) {
    //
    //     });
    // }

    $('.iva-cta-usada-remove-button').click();
    cargarPrefijos();
    $('#iva-cta-usada-new-button').click();
});

// Solamente para aplicar inputmask
$('#venta-moneda_id').on('change', function () { console.log('moneda_id changed');
    disableValorMoneda();
    // cambiar mascara del campo valor_moneda segun moneda.
    var campo_monedavalor_id = '#venta-valor_moneda-disp';
    if ($(this).select2('data')[0]['tiene_decimales'] === 'si') {
        $(campo_monedavalor_id).inputmask({
            radixPoint: ",",
            prefix: "",
            digits: 2,
            autoGroup: true,
            groupSeparator: ".",
            rightAlign: true,
            autoUnmask: true,
            allowMinus: false
        });
    } else {
        $(campo_monedavalor_id).inputmask({
            radixPoint: ",",
            prefix: "",
            digits: 2,
            autoGroup: true,
            groupSeparator: ".",
            rightAlign: true,
            autoUnmask: true,
            allowMinus: false
        });
    }
});

// Mostrar/ocultar campos adicionales al cambiar condicion de factura y
//      poner fecha de vencimiento igual a la fecha de emision mas 1 mes y
//      la cantidad de cuotas por defecto a 1.
$('#venta-condicion, #venta-fecha_emision').on('change', function () {
    var condicion = $('#venta-condicion').val();

    // en la funcion showHideCuotasField(..) tambien esta implementado el poner la fecha de vencimiento
    showHideCuotasField(condicion);

    if ($(this).attr('id') === 'venta-condicion') { //console.log('venta_condicion changed');
        disableValorMoneda();
    }
    // else if ($(this).attr('id') === 'venta-fecha_emision-disp') { setCotizacion(); } // movido al evento standalone de venta-fecha_emision
});

$('#venta-fecha_emision').on('change', function () {
    cargarPrefijos();
    //console.log('fecha_emision changed');
    setCotizacion();

    if ($('#venta-condicion').val() === 'credito') {
        setFechaVencimiento();
    }
});

$('#venta-tipo_documento_id').on('change', function () {
    cargarPrefijos();

    // establecer condicion de factura
    if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
        if ($(this).val() !== '') {
            $.ajax({
                url: $url_getCondicionFromDocType,
                type: 'post',
                data: {
                    tipo_doc_id: $(this).val(),
                },
                success: function (data) {
                    $('#venta-condicion').val(data).trigger('change');
                    $.pjax.reload({container: "#flash_message_id", async: false});
                    setTotalFactura(); // si es nota de credito contado emitida, hay que invertir partida.
                }
            });

            $('#boton_add_timbrado').prop('disabled', false);
        } else {
            $('#boton_add_timbrado').prop('disabled', true);
        }
    }
});

// Format nro factura
$("#venta-nro_factura").keydown(function (event) {
    var key = event.which;
    var esTeclaBorrar = key === 8;

    if (key !== 9) { // si no es tab
        var text = $(this).val().replace(/_/g, "").replace(/-/g, ''); // borrar los underscores a la derecha
        if (key > 57) key -= 96; else key -= 48; // convertir a un nro
        // var lastgroupstart = 6; // index donde comienza el ultimo grupo de nro
        var lastgroupstart = 0; // index donde comienza el ultimo grupo de nro
        var lastgroup = text.substring(lastgroupstart, text.length); // el ultimo grupo de nro

        // console.log('text: '+text);
        // console.log('longitud: '+text.length);
        // console.log('key: '+key);
        // console.log('lastgroup: ' + lastgroup);

        // if (text.length > 5 && !esTeclaBorrar && (key > -1 && key < 10)) {
        if (!esTeclaBorrar && (key > -1 && key < 10)) {

            if (lastgroup.length === 0) { // si es el 1er nro tecleado
                text = text.replaceAt(lastgroupstart, Array(7).join("0") + key);
                $(this).val(text);
            } else {
                var i, p = -1;
                // localizar donde comienza un nro <> 0
                for (i = 0; i < lastgroup.length; i++) {
                    if (lastgroup[i] !== '0') {
                        p = i;
                        break;
                    }
                }
                // si previamente no hubo ningún número, hacer un espacio forzadamente.
                if (lastgroup === '0000000') lastgroup = '000000';
                // extraer solamente los nros y concatenar el nro tecleado
                lastgroup = lastgroup.substring(p, lastgroup.length) + key; // si p > lastgroup.length, significa que no hubo ningún nro antes de keydown y no extraerá nada el substring.
                // console.log('lastgroup: ' + lastgroup);
                // agregar el ultimo nro tecleado, rellenando debidamente con ceros a la izquierda.
                // text = text.replaceAt(lastgroupstart, Array((7 - lastgroup.length) + 1).join("0") + lastgroup)
                //text = (Array((7 - lastgroup.length) + 1).join("0") + lastgroup);
                text = text.replaceAt(lastgroupstart, Array((7 - lastgroup.length) + 1).join("0") + lastgroup)
            }

            // console.log('ultimo grupo final: '+lastgroup);
            // console.log('final: '+text);
        }
        $(this).val(text);
    }
});

// Abrir modal para agregar detalles al presionar tecla +
$(document).keypress(function (e) {
    // console.log(document.activeElement.tagName);
    if (document.activeElement.tagName !== "INPUT" &&
        (!$("#modal").hasClass('in') && (e.keycode === 43 || e.which === 43))) {
        $('#boton_add_detalle').click();
    } else if (document.activeElement.id.includes('ventaivacuentausada') && (!$("#modal").hasClass('in') && (e.keycode === 43 || e.which === 43))) {
        $('#iva-cta-usada-new-button').click();
    }
});

function setCotizacion() {
    $.ajax({
        url: $url,
        type: 'post',
        data: {
            fecha_emision: $('#venta-fecha_emision').val(),
            moneda_id: $('#venta-moneda_id').val(),
        },
        success: function (data) {
            if (parseFloat(data) % 1 === 0) {
                data = parseFloat(data) - (parseFloat(data) % 1);
            }
            $('#venta-valor_moneda-disp').val(data).trigger('change');
            $('#venta-valor_moneda').val(data).trigger('change');
            // console.log("desde venta-fecha_emision-disp, #venta-moneda_id luego del success");
            // lanzar evento para actualizar campos gravadas 
            // let ctots = getTotalFieldNames(), i;
            // for (i = 0; i < ctots.length; i++) {
            //     let ctot = document.getElementById(ctots[i] + '-disp');
            //     if (ctot !== null)
            //         $('#' + ctots[i]).trigger('change');
            // }
            g_aplicarNumberInput();
            setTotalFactura();
        }
    });
}

if (!esNota) {
    $('#venta-cotizacion_propia').on('change', function () { console.log('venta-cotizacion_propia changed');
        $('#venta-valor_moneda-disp').prop('readonly', $(this).val() === 'no');
        //console.log('cotizacion_propia changed');
        setCotizacion();
    });

    // Al presionar tab desde el campo valor_moneda, generar detalles.
    document.getElementById('venta-valor_moneda-disp').addEventListener('keydown', function (event) {
        if (event.which === 9) {
            setTotalFactura();
        }
    }, false);

    // // Evento para campo total factura.
    // document.getElementById("venta-total-disp").addEventListener('keydown', function (event) {
    //     if (event.which === 9) {
    //         console.log('desde el campo total factura keydown tecla tab');
    //         setTotalFactura();
    //     }
    // }, false);
}

$('#venta-timbrado_detalle_id_prefijo').on('change', function () {

    let element1 = $('#venta-nro_factura');

    // Para usar en cargarPrefijo.
    prefijo_selected = $(this).val();
    let nro_setted = element1.val();

    if ($(this).val() !== '' && $(this).val() !== null) {
        var url = $url_nextnro;//+'&isExistentRecord='+"$model->id";
        $.ajax({
            url: url,
            type: 'post',
            data: {
                id_detalle: $(this).val(),
                model_id: "$model->id",
                action_id: "$actionId"
            },
            success: function (data) {
                var sel = document.getElementById("venta-timbrado_detalle_id_prefijo");
                var prefijo = sel.options[sel.selectedIndex].text;
                var data_array = data.split("|");
                var nro_actual = data_array[0];

                // TODO: No se porque esta logica pero se deja comentado por si sirva

                // if (nro_setted === '') {
                //     var lastgroup = Array((7 - nro_actual.length) + 1).join("0") + nro_actual;
                //     element1.val(lastgroup).trigger('change'); // el trigger para ejecutar validacion
                // }
                // else {
                //     element1.val(nro_setted).trigger('change');
                // }

                if (nro_actual !== "") {
                    var lastgroup = Array((7 - nro_actual.length) + 1).join("0") + nro_actual;
                    element1.val(lastgroup).trigger('change'); // el trigger para ejecutar validacion
                }

                $('#venta-nro_timbrado').val(data_array[1]);
            }
        });
    } else {
        element1.val('');
        $('#venta-nro_timbrado').val('');
    }
});

// // Habilitar campos totales segun plantilla
// $('#venta-plantilla_id').on('change', function () {
//     console.log('desde plantillaid onchange se llama a totalesIvaFieldShowHide.');
// });

// Desactivar algunos campos segun estado, al cambiar estado y al iniciar formulario.
$('#venta-estado').on('change', function () { console.log('venta-estado changed');
    if (!['anulada', 'faltante'].includes($(this).val())) {
        let actionId = "$actionId";
        if (actionId === 'create') {
            $('#venta-tipo_documento_id').select2('open');
            $('#venta-fecha_emision').val(sessionStorage.getItem('fecha_usada')).trigger('change');
            sessionStorage.removeItem('fecha_usada');
        }
    } else {
        let fecha_usada = $('#venta-fecha_emision').val();
        sessionStorage.setItem('fecha_usada', fecha_usada);
    }
    disableFieldsByEstado();
    // TODO: Borrar detalles de la vista solo si es create. sino dejar.
});

$('#venta-ruc').on('change', function () {
    var url_getRazonSocialByRuc = $url_getRazonSocialByRuc;
    $.ajax({
        url: url_getRazonSocialByRuc,
        type: 'post',
        data: {
            ruc: $(this).val(),
        },
        success: function (data) {
            $('#venta-nombre_entidad').val(data);
        }
    });
});

// let ctotales = getTotalFieldNames(), cgravadas = getGravadasFieldNames(), ivas = getIvas();
// let totales_iva = 0.0;
// for (i=0; i<ctotales.length; i++) {
//     if (ivas[i] !== '0') {
//         let ctotal = document.getElementById(ctotales[i] + '-disp');
//         let cgrav = document.getElementById(cgravadas[i] + '-disp');
//         let iva = ivas[i];
//         // ctotal.addEventListener('change', function () {
//         //     let valor = ctotal.value;
//         //     cgrav.value = valor / (1.0+iva/100.0);
//         //     if ($('#venta-moneda_id').val() === '1') {
//         //         cgrav.value = Math.round(cgrav.value);
//         //     }
//         // });
//         ctotal.addEventListener('keydown', function (event) {
//             if (event.which === 9) {
//                 let totales_iva = 0, i;
//                 for (i=0; i<cgravadas.length; i++) {
//                     if (document.getElementById(cgravadas[i]+'-disp') !== null)
//                         totales_iva += $('#' + cgravadas[i] + '-disp').val();
//                 }
//                 let valor = ctotal.value;
//                 cgrav.value = valor - (valor / (1.0+iva/100.0));
//                 // totales_iva = totales_iva + cgrav.value;
//                 if ($('#venta-moneda_id').val() === '1') {
//                     cgrav.value = Math.round(cgrav.value);
//                     totales_iva = Math.round(totales_iva);
//                 }
//                 $('#venta-_total_ivas-disp').val(totales_iva);
//             }
//         }, false);    
//     }
//    
// }

$(document).on('click', ':button.btn-simular', function () {
    simular();
});
JS;

$this->registerJs($JQ_VARIABLES, \yii\web\View::POS_HEAD);
$this->registerJs($JS_FUNCTIONS);
$this->registerJs($JS_DOC_ON_READYs);
$this->registerJs($JS_EVENTS);
