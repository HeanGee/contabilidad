<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Venta */

$this->title = 'Nueva Venta';
$this->params['breadcrumbs'][] = ['label' => 'Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="venta-create">

    <?php
    $es_nota = in_array($model->tipo, ['nota_credito', 'nota_debito']) ? $model->tipo : false;
    if (!$es_nota) {

        echo Html::beginTag('div', ['class' => 'btn-toolbar']);

        {
            echo Html::a('<span class="glyphicon glyphicon-arrow-right"></span>',
                "javascript:void(0);",
                ['id' => 'btn_next', 'class' => 'btn btn-primary pull-right navegate', 'title' => 'Siguiente']);

            echo Html::a('<span class="glyphicon glyphicon-plus"></span>',
                ['create'],
                ['id' => 'btn_new', 'class' => 'btn btn-success pull-right', 'title' => 'Nuevo']);

            echo Html::a('<span class="glyphicon glyphicon-arrow-left"></span>',
                "javascript:void(0);",
                ['id' => 'btn_prev', 'class' => 'btn btn-primary pull-right navegate', 'title' => 'Anterior']);
        }

        echo Html::endTag('div');
    }
    ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

$url_naver = \yii\helpers\Url::to(['navegate-from-create']);

$SCRIPTS = <<<JS

$(".navegate").on('click', function() {
    
    let select2 = document.getElementById('venta-timbrado_detalle_id_prefijo');
    let prefijo = select2.options[select2.selectedIndex].text;
    let nro_fac = $('#venta-nro_factura').val();
    
    let goto = $(this).attr('id') === 'btn_prev' ? 'prev' : 'next';
    
    $.get(
        "$url_naver" + '&goto=' + goto + '&prefijo=' + prefijo + '&nro_fac=' + nro_fac,
        function( data ) {
            console.log(data);
        }
    );
});

JS;

$this->registerJs($SCRIPTS);