<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 24/07/2018
 * Time: 8:32
 */

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $form kartik\form\ActiveForm */
/* @var $model Venta */
/** @var IvaCuenta[] $iva_ctas */
$es_nota = $model->tipo != 'factura';

$iva_ctas = IvaCuenta::find()->all();
foreach ($iva_ctas as $iva_cuenta)
    $porcentajes_iva[] = $iva_cuenta->iva->porcentaje;
asort($porcentajes_iva);
$porcentajes_iva = array_values($porcentajes_iva);
$porcentajes_iva_json = Json::encode($porcentajes_iva);

$ivaCtaUsada = new VentaIvaCuentaUsada();
$ivaCtaUsada->loadDefaultValues();

echo Html::beginTag('fieldset');

echo Html::beginTag('leyend', []);
echo "<div class='form-group btn-toolbar'>";
{
    if (!$es_nota) {
        echo Html::button('<i class="glyphicon glyphicon-plus"></i>', [
            'id' => 'iva-cta-usada-new-button',
            'class' => 'pull-left btn btn-success',
        ]);

        // Si es update, habilitar boton que trae los detalles por defecto
        if (Yii::$app->controller->id == 'venta' && Yii::$app->controller->action->id == 'update') {
            echo Html::button('Mostrar detalles originales', [
                'id' => 'detalle-defecto',
                'class' => 'pull-right btn btn-info hidden',
            ]);
        }
    }
}
echo "</div>";
echo Html::endTag('leyend');

echo Html::beginTag('table', ['class' => 'table table-condensed table-bordered', 'id' => 'plantillas-table']);

echo Html::beginTag('thead');
echo Html::beginTag('tr', ['class' => 'title-row']);
echo Html::tag('th', 'Plantilla&nbsp;&nbsp;' . HtmlHelpers::InfoHelpIcon('info-plantilla'), ['style' => 'text-align: center; ']);
foreach ($iva_ctas as $iva_cta) {
    if ($iva_cta->iva->porcentaje == 0)
        echo Html::tag('th', 'Exenta', ['class' => '', 'style' => "text-align: center; "]);
    else
        echo Html::tag('th', 'I.V.A. ' . $iva_cta->iva->porcentaje . '%', ['class' => '', 'style' => "text-align: center; "]);
}
//if (!$es_nota) {
echo Html::tag('th', 'Motivo por el que se envia a GND', ['style' => "text-align: center; "]);
echo Html::tag('th', 'Acciones', ['style' => "text-align: center; "]);
echo Html::endTag('tr');
echo Html::endTag('thead');

echo Html::beginTag('tbody');
// Mostrar filas por plantillas
$key = 1;
foreach ($model->_iva_ctas_usadas as $index => $_iva_cta) { // $index == plantilla_id
    echo Html::beginTag('tr', ['class' => 'fila-data']);
    echo $this->render('_form_plantillas_filas', [
        'key' => 'new' . $key,
        'form' => $form,
        'ivaCtaUsada' => $ivaCtaUsada,
        'porcentajesIva' => $porcentajes_iva,
        'es_nota' => $es_nota
    ]);
    echo Html::endTag('tr');

    $noDeducible = $model->usar_tim_vencido == 1 ? true : false;
    $plantilla_data = Json::encode(PlantillaCompraventa::getPlantillasLista('venta', false, $noDeducible, false, $model->obligacion_id));
    // TODO: no esta trayendo las plantillas....
    $_s = <<<JS
        $('#ventaivacuentausada-new$key-plantilla_id').select2({
            theme: 'krajee',
            placeholder: '',
            language: 'en',
            width: '100%',
            data: $plantilla_data
        });

        l_onchangePlantilla('#ventaivacuentausada-new$key-plantilla_id');
        l_setOnchangeEventNumberInput('#ventaivacuentausada-new$key-');
        g_aplicarNumberInput();
        //console.log($plantilla_data);        
        $('#ventaivacuentausada-new$key-plantilla_id').val($index).trigger('change'); // setear el id de plantilla, una vez que se haya configurado el evento.
        
        $(document).on('click', '#iva-cta-usada-new$key-remove-button', function () {
            $(this).closest('tbody tr').remove();
            completarTotales();
            setTotalFactura();
        });
JS;
    $this->registerJs($_s);

    foreach ($_iva_cta as $indice => $porcentaje) {
        if (is_numeric($indice)) // $ivactausda[plantilla_id][0/5/10/gnd_motivo] = some_value
            $this->registerJs("
    //            $('#ventaivacuentausada-new" . $key . "-ivas-iva_" . $indice . "').on('change', function () { // en vez de esto, hace lo mismo l_setOnchangeEventNumberInput(prefijo).
    //                completarTotales();
    //            })
    
                // Cargar montos en las casillas de la plantilla.
                $('#ventaivacuentausada-new" . $key . "-ivas-iva_" . $indice . "').val(" . $porcentaje . ");
            ");
        else
            $this->registerJs("$('#ventaivacuentausada-new" . $key . "-gnd_motivo').val('" . $porcentaje . "');");
    }
    $key++;
}
// Fila ejemplar
echo Html::beginTag('tr', ['style' => "display: none;", 'id' => 'fila-template']);
echo $this->render('_form_plantillas_filas', [
    'key' => 'new',
    'form' => $form,
    'ivaCtaUsada' => $ivaCtaUsada,
    'porcentajesIva' => $porcentajes_iva,
    'es_nota' => $es_nota
]);
echo Html::endTag('tr');
echo Html::endTag('tbody');

echo Html::endTag('table');

echo Html::endTag('fieldset');
?>

<?php
$url_get_plantilla = Json::htmlEncode(\Yii::t('app', Url::to(['plantilla-compraventa/get-plantilla'])));
ob_start(); // output buffer the javascript to register later ?>
    <script>
        let detalle = <?php echo isset($key) ? $key : 0; ?>;
        //let data = <?php //echo Json::encode(PlantillaCompraventa::getPlantillasLista('venta', false, false)); ?>//;
        //let pl_no_deducibles = <?php //echo Json::encode(PlantillaCompraventa::getPlantillasLista('venta', false, true)); ?>//;
        let entro_primera_vez = true;

        function generarInputNumber(elemento, tiene_decimales) {
            elemento.inputmask({
                "alias": "numeric",
                "digits": tiene_decimales === "si" ? 2 : 0,
                "groupSeparator": ".",
                "autoGroup": true,
                "autoUnmask": true,
                "unmaskAsNumber": true,
                "radixPoint": ",",
                "digitsOptional": false,
                "placeholder": "0",
                "rightAlign": true
            });
        }

        function habilitarCamposIvasSegunPlantilla(prefijo) {
            // Habilitar boton para gnd_motivo.
            if ($(prefijo + 'plantilla_id').select2('data')[0]['deducible'] === 'no') {
                $(prefijo + 'gnd_motivo').attr('readonly', false);
                $(prefijo + 'gnd_motivo').attr('disabled', false);
            } else {
                $(prefijo + 'gnd_motivo').attr('readonly', true);
                $(prefijo + 'gnd_motivo').attr('disabled', true);
                $(prefijo + 'gnd_motivo').val('');
            }

            // Habilitar campos de montos.
            if (!['anulada', 'faltante'].includes($('#venta-estado').val())) {
                $.ajax({
                    url: <?= Json::htmlEncode(\Yii::t('app', Url::to(['venta/get-ivas-de-plantilla'])))?>,
                    type: 'get',
                    data: {
                        plantilla_id: $(prefijo + 'plantilla_id').val(),
                    },
                    success: function (result) {
                        let i;
                        let ivas = getIvas();
                        for (i = 0; i < ivas.length; i++) {
                            if (result.includes(ivas[i])) {
                                $(prefijo + 'ivas-iva_' + ivas[i]).attr('readonly', false);
                                $(prefijo + 'ivas-iva_' + ivas[i]).attr('disabled', false);
                            } else {
                                $(prefijo + 'ivas-iva_' + ivas[i]).attr('readonly', true);
                                $(prefijo + 'ivas-iva_' + ivas[i]).attr('disabled', true);
                                $(prefijo + 'ivas-iva_' + ivas[i]).val("");
                            }
                        }
                    },
                });
            }
        }

        function completarTotales() {
            let ivas = getIvas();
            let total_iva = 0.0;
            let total_gravada = 0.0;
            let total_factura = 0.0;
            ivas.forEach(function (element) {
                let sum = 0.0;
                let iva = 0.0;
                let gravada = 0.0;
                $('td.column-iva-' + element + ' :input').each(function () {
                    const regex = /ventaivacuentausada-new([0-9]+)-ivas-iva_([0-9]+)/gm;
                    const regex2 = /ventaivacuentausada-new([0-9]+)-/gm;
                    let m = regex.exec($(this).attr('id'));
                    let m2 = regex2.exec($(this).attr('id'));
                    if (m !== null && $(this).val() !== "") {
                        let paraDescuento = $("#" + m2[0] + "plantilla_id").select2('data')[0]['para_descuento'];
                        if (paraDescuento === 'si')
                            sum -= parseFloat($(this).val());
                        else
                            sum += parseFloat($(this).val());
                    }
                });
                // console.log("total de la columna de " + element + " es: " + sum);
                total_factura += sum;
                // console.log("total de factura es: " + total_factura);
                $('#totals-iva-' + element + '-disp').val(sum);
                if (element !== 0) {
                    // 5500 - (5500 / ((10 + 100) / 100))
                    iva = sum - (sum / ((element + 100) / 100));
                    gravada = sum - iva;
                    $('#ivas-iva-' + element + '-disp').val(iva);
                    $('#ivas-gravada-' + element + '-disp').val(gravada);
                    total_iva += parseFloat(iva);
                    total_gravada += parseFloat(gravada);
                }
            });
            let toFix = $('#venta-moneda_id').val() === 1 ? 0 : 2;
            total_iva = Number((total_iva).toFixed(toFix));
            total_gravada = Number((total_gravada).toFixed(toFix));
            total_factura = Number((total_factura).toFixed(toFix));
            $('#venta-_total_ivas-disp').val(total_iva).trigger('change');
            $('#venta-_total_gravadas-disp').val(total_gravada).trigger('change');
            $('#venta-total-disp').val(total_factura).trigger('change');
        }

        function g_aplicarNumberInput() {
            let tiene_decimales = esNota ? decimalesMonedaParaNota : $('#venta-moneda_id').select2('data')[0]['tiene_decimales'];
            // campos ivas por plantillas
            $('input.agregarNumber').each(function () {
                generarInputNumber($(this), tiene_decimales);
            });

            // campos sumatoria y discriminadas
            $('.totales :input.form-control').each(function () {
                generarInputNumber($(this), tiene_decimales);
            });
            $('.ivas :input.form-control').each(function () {
                generarInputNumber($(this), tiene_decimales);
            });

            // campos total y total_ivas
            generarInputNumber($('#venta-total-disp'), tiene_decimales);
            generarInputNumber($('#venta-_total_ivas-disp'), tiene_decimales);
            generarInputNumber($('#venta-_total_gravadas-disp'), tiene_decimales);
        }

        function l_aplicarNumberInput(element) {
            let tiene_decimales = esNota ? decimalesMonedaParaNota : $('#venta-moneda_id').select2('data')[0]['tiene_decimales'];
            generarInputNumber(element, tiene_decimales);
        }

        function g_setOnchangeEventNumberInput() {
            $('input.agregarNumber').each(function () {
                $(this).on('change', function () {
                    completarTotales();
                    setTotalFactura();
                });
            });
        }

        function l_setOnchangeEventNumberInput(prefijo) {
            let ivas = getIvas();
            ivas.forEach(function (element) {
                let id = prefijo + 'ivas-iva_' + element;
                $(id).on('change', function () {
                    completarTotales();
                    setTotalFactura();
                });
                $(id).keyup(function (event) {
                    let factura_total = obtenerTotalFactura();
                    // Redondear a entero si es guarani
                    if ($('#venta-moneda_id').val() === '1')
                        factura_total = Math.round(factura_total);
                    // mostrar total en el campo total factura
                    $('#venta-total-disp').val(factura_total);
                });
            });
        }

        /** Proximamente a eliminar. */
        function g_onchangePlantilla() {
            $('td.plantilla-selector :input').each(function () {
                let id = '#' + $(this).attr('id');
                const regex = /#ventaivacuentausada-new([0-9]+)-/gm;
                let m = regex.exec(id);
                if (m !== null) {
                    $(this).on('change', function () {
                        habilitarCamposIvasSegunPlantilla(m[0]);
                    })
                }
            });
            completarTotales(); // para que actualice el total y los demas, si es que la plantilla es de descuento.
        }

        function l_onchangePlantilla(id) {
            $(id).on('change', function () {
                const regex = /#ventaivacuentausada-new([0-9]+)-/gm;
                let m = regex.exec(id);
                if (m !== null) {
                    completarTotales(); // para que actualice el total y los demas, si es que la plantilla es de descuento.
                    habilitarCamposIvasSegunPlantilla(m[0]);

                    if ($(this).select2('data').length) { // puede haber casos donde no va a estar seleccionado ninguna plantilla porque ya no pertenece a la obligacion

                        // Mostrar botón para activo fijo.
                        let id_btn_to_show = m[0] + 'actfijo_manager_button';
                        if ($(this).select2('data')[0]['activo_fijo'] === 'si') {
                            $(id_btn_to_show).prop('style', "display: yes");
                        } else {
                            $(id_btn_to_show).prop('style', "display: none");
                        }

                        // Mostrar botón para activo biologico.
                        id_btn_to_show = m[0] + 'actbio_venta_manager_button';
                        if ($(this).select2('data')[0]['activo_biologico'] === 'si') {
                            $(id_btn_to_show).prop('style', "display: yes");
                        } else {
                            $(id_btn_to_show).prop('style', "display: none");
                        }
                    }

                    //console.log('llamando setTotalFactura() desde l_onchangePlantilla()');
                    setTotalFactura();
                }
            });
        }

        $(document).ready(function () {
            // g_onchangePlantilla();
            g_setOnchangeEventNumberInput();

            completarTotales();
            setTotalFactura();

            let css = {
                'title-css': {
                    'background-color': '#<?= HtmlHelpers::InfoColorHex(true) ?>',
                    'color': 'white',
                    'text-align': 'center',
                    'font-weight': 'bold'
                }
            }; //D9EDF7
            let content = 'Si la lista aparece vacía, verifique:<br/><br/>' +
                '<ol>' +
                '<li>Existe al menos una plantilla para venta registrada en el sistema.</li>' +
                '<li>Existe al menos una plantilla para venta asociada a la obligación seleccionada más arriba.</li>' +
                '</ol>';
            applyPopOver($('span.info-plantilla'), 'Atención', content, css);
        });

        $('#iva-cta-usada-new-button').on('click', generarNuevoIvaCuentaUsada);

        function generarNuevoIvaCuentaUsada() {
            $('#plantillas-table').find('tbody')
                .append('<tr class="fila-data">' + $('#fila-template').html().replace(/new/g, 'new' + detalle) + '</tr>');

            /** //////////////////////////////////////////////////////////////////////// */

            $.ajax({
                url: <?= $url_get_plantilla ?>,
                type: 'get',
                data: {
                    tipo: 'venta',
                    noDeducible: $('#venta-timbrado_vencido').val() === "1" ? "si" : "no",
                    paraImportacion: $('#venta-tipo_documento_id').select2('data')[0]['para_importacion'],
                    obligacion_id: $('#venta-obligacion_id').val()
                },
                success: function (data) {
                    // let plantillas = data;
                    // if ($("#venta-usar_tim_vencido").val() === '1') {
                    //     plantillas = pl_no_deducibles;
                    // }

                    $('#ventaivacuentausada-new' + detalle + '-plantilla_id').select2({
                        theme: 'krajee',
                        placeholder: '',
                        language: 'en',
                        width: '100%',
                        data: data,
                    });

                    l_onchangePlantilla('#ventaivacuentausada-new' + detalle + '-plantilla_id');
                    l_setOnchangeEventNumberInput('#ventaivacuentausada-new' + detalle + '-');
                    let prefijo = 'ventaivacuentausada-new' + detalle + '-ivas-iva_';
                    $("input[id^=" + prefijo + "]").prop('readonly', true);
                    g_aplicarNumberInput();

                    $(document).on('click', '#iva-cta-usada-new' + detalle + '-remove-button', function () {
                        $(this).closest('tbody tr').remove();
                        completarTotales();
                        setTotalFactura();
                    });

                    detalle++;
                    if (entro_primera_vez === false) {
                        $('td.plantilla-selector :input').last().select2('open');
                    }
                    entro_primera_vez = false;
                }
            });
            // return detalle - 1;
        }

    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>