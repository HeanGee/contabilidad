<?php

use backend\modules\contabilidad\models\Venta;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\VentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facturas de Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="venta-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-venta-create') ?
            Html::a('Registrar Factura de Venta', ['create'], ['id' => 'boton_crear_factura', 'class' => 'btn btn-success']) : null ?>
    </p>

    <?php
    $template = '';
    //    foreach (['view', 'update', 'delete'] as $_v)
    foreach (['view', 'update', 'bloquear', 'desbloquear'] as $_v)
        if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-venta-{$_v}"))
            $template .= "&nbsp&nbsp&nbsp{{$_v}}";
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
//            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
//            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'hover' => true,
//            'striped' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'ID',
                    'headerOptions' => [
                        'width' => '5%'
                    ],
                    'value' => 'id',
                    'attribute' => 'id',
                ],
                [
                    'label' => 'Nro Comprobante',
                    'value' => 'nro_completo',
                    'attribute' => 'nro_completo',
                ],
                [
                    'attribute' => 'fecha_emision',
                    // format the value
                    'format' => ['date', 'php:d-m-Y'],
                    'headerOptions' => ['class' => 'col-md-2'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha_emision',
                        'language' => 'es',
                        'pickerButton' => false,
                        'options' => [
                            'style' => 'width:90px',
                        ],
                        'pluginOptions' => [
                            'orientation' => 'bottom center',
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'weekStart' => 0,
                        ]
                    ])
                ],
                [
                    'attribute' => 'fecha_emision',
                    'label' => "Fecha de Emisión Hasta (Opcional)",
                    'format' => ['date', 'php:d-m-Y'],
                    'headerOptions' => ['class' => 'col-md-2'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha_emision_hasta',
                        'language' => 'es',
                        'pickerButton' => false,
                        'options' => [
                            'style' => 'width:90px',
                        ],
                        'pluginOptions' => [
                            'orientation' => 'bottom center',
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'weekStart' => 0,
                        ]
                    ])
                ],
//                [
//                    'label' => 'Creado',
//                    'attribute' => 'creado',
//                    'format' => ['date', 'php:d-m-Y'],
//                    'filter' => DatePicker::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'creado',
//                        'language' => 'es',
//                        'pluginOptions' => [
//                            'orientation' => 'bottom center',
//                            'autoclose' => true,
//                            'format' => 'dd-mm-yyyy',
//                            'todayHighlight' => true,
//                            'weekStart' => '0',
//                        ]
//                    ])
//                ],
                [
                    'label' => 'Cliente',
                    'attribute' => 'nombre_entidad',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var Venta $model */
                        if (isset($model->entidad))
                            return "{$model->entidad->razon_social} <strong>(RUC: {$model->entidad->ruc})</strong>";
                        return '';
                    },
                ],
                [
                    'attribute' => 'total',
                    'value' => 'formattedTotal',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'attribute' => 'saldo',
                    'value' => 'formattedSaldo',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'label' => 'Tipos',
//                    'attribute' => 'nombre_documento',
//                    'value' => 'tipoDocumento.nombre',
                    'attribute' => 'tipo_documento_id',
                    'value' => 'tipoDocumento.nombre',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'tipo_documento_id',
                        'data' => Venta::getTipoDocumentos(),
//                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '250px'
                        ],
                        'initValueText' => 'todos',
                    ])
                ],
                [
                    'label' => 'Timbrado',
                    'attribute' => 'nro_timbrado',
                    'value' => 'nro_timbrado',
//                    'filter' => Select2::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'nro_timbrado',
//                        'data' => Venta::getTimbradoNroLista(),
////                        'hideSearch' => true,
//                        'pluginOptions' => [
//                            'width' => '120px'
//                        ],
//                    ])
                ],
//                [
//                    'label' => 'Código Venta',
//                    'value' => 'id',
//                    'attribute' => 'id'
//                ],
                [
                    'attribute' => 'moneda_id',
                    'value' => 'moneda.nombre',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'moneda_id',
                        'data' => Venta::getMonedas(true),
//                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '120px',
                        ],
                        'initValueText' => 'todos',
                    ])
                ],
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'vigente') ? 'success">Vigente' : (($model->estado == 'faltante') ? 'warning">Faltante' : 'danger">Anulada')) . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => Venta::getEstados(true),
//                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '120px',
                        ]
                    ]),
                ],
                [
                    'attribute' => 'partida_correcto',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var Venta $venta */
                        $venta = $model;
                        return '<label class="label label-' . ($venta->isPartidaCorrect() ? 'success">Si' : 'warning">No') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                ],
                [
                    'attribute' => 'asiento_id',
                    'label' => "Asentado?",
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var Venta $model */
                        $type = ($model->asiento_id != '') ? "success" : "warning";
                        $content = ($model->asiento_id != '') ? "Si" : "No";
                        $btnVer = ($model->asiento_id != '') ? Html::a("<span class='label label-info'>Ver</span>", ['/contabilidad/asiento/view/', 'id' => $model->asiento_id]) : null;
                        return "<label style='text-align:center;' class='label label-{$type}'>$content</label>&nbsp;&nbsp;$btnVer";
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'asiento_id',
                        'data' => ['si' => "SI", 'no' => "NO"],
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                            'width' => '100px',
                        ],
                    ]),
                    'contentOptions' => ['style' => 'text-align:center;'],
                ],
//                [
//                    'attribute' => 'condicion',
//                    'value' => 'condicion',
//                    'filter' => Select2::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'condicion',
//                        'data' => Venta::getCondiciones(true),
////                        'hideSearch' => true,
//                        'pluginOptions' => [
//                            'width' => '120px'
//                        ]
//                    ])
//                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        /** @var Venta $model */
                        'update' => function ($url, $model) {
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Editar'),
                            ]) : '';
                        },
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('yii', 'Ver'),
                            ]);
                        },
                        'bloquear' => function ($url, $model, $key) {
                            /** @var Venta $model */
                            $url = ($model->bloqueado == 'no') ?
                                Html::a('<span>Bloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => Yii::t('app', 'Bloquear'),
                                        'data-confirm' => 'Está seguro que desea bloquear la factura?'
                                    ]) : '';
                            return $url;
                        },
                        'desbloquear' => function ($url, $model, $key) {
                            /** @var Venta $model */
                            $url = ($model->bloqueado == 'si') ?
                                Html::a('<span>Desbloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-success btn-xs',
                                        'title' => Yii::t('app', 'Desbloquear'),
                                        'data-confirm' => 'Está seguro que desea desbloquear la factura?'
                                    ]) : '';
                            return $url;
                        },
                    ],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $e) {
        throw $e;
    } ?>
</div>

<?php
// DESDE TIPO DE DOCUMENTOS. HECHO POR MIGUEL.
$css = <<<CSS
.kartik-sheet-style {
	background: #ffffff;
	background: -moz-linear-gradient(top, #ffffff 0%, #f5f5f5 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f5f5f5));
	background: -webkit-linear-gradient(top, #ffffff 0%,#f5f5f5 100%);
	background: -o-linear-gradient(top, #ffffff 0%,#f5f5f5 100%);
	background: -ms-linear-gradient(top, #ffffff 0%,#f5f5f5 100%);
	background: linear-gradient(to bottom, #ffffff 0%,#f5f5f5 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f5f5f5',GradientType=0 );
}

.kv-grid-table tbody tr:hover {
    background-color: #ebffdd;
}
CSS;
$this->registerCss($css);


/** ********************************** VARIABLES ********************************** **/
$ivas_por = \backend\modules\contabilidad\models\IvaCuenta::find()->all();
$porcentajes_iva = [];
foreach ($ivas_por as $iva_por) {
    $porcentajes_iva[] = $iva_por->iva->porcentaje;
}
asort($porcentajes_iva);
$porcentajes_iva = array_values($porcentajes_iva);
$porcentajes_iva = Json::encode($porcentajes_iva);
$url_deleteTotalesFromSession = Json::htmlEncode(\Yii::t('app', Url::to(['venta/delete-totales-from-session'])));
/** ********************************** VARIABLES ********************************** **/


/** ********************************** JS_FUNCTIONS ********************************** **/
$JS_FUNCTIONS = <<<JS
function totalFieldNames() {
    const prefijo = "total-iva-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}
JS;
/** ********************************** JS_FUNCTIONS ********************************** **/


/** ********************************** JS_DOCUMENT_ON_READYs ********************************** **/
$JS_DOCUMENT_ON_READYs = <<<JS
// Remover totales iva de la session
$.ajax({
    url: $url_deleteTotalesFromSession,
    type: 'post',
    data: {
        campos_totales_nombre: totalFieldNames(),
    },
    success: function (data) {
    }
});
JS;
/** ********************************** JS_DOCUMENT_ON_READYs ********************************** **/


/** ********************************** JS_EVENTS ********************************** **/
$JS_EVENTS = <<<JS
// Abrir modal para agregar detalles al presionar tecla +
$(document).keypress(function (e) {
    if (document.activeElement.tagName !== "INPUT" && (e.keycode === 43 || e.which === 43)) {
        document.getElementById('boton_crear_factura').click();
    }
});
JS;
/** ********************************** JS_EVENTS ********************************** **/

$scripts = [];
array_push($scripts, $JS_FUNCTIONS);
array_push($scripts, $JS_DOCUMENT_ON_READYs);
array_push($scripts, $JS_EVENTS);

foreach ($scripts as $s) $this->registerJs($s); ?>
