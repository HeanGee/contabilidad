<?php

use backend\modules\contabilidad\models\Venta;
use common\models\User;
use kartik\daterange\DateRangePicker;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\View;

/* @var $this View */
/* @var $model Venta */
?>

<?php
try {
    $subquery = (new Query)
        ->select(['entry_id'])
        ->from('audit_trail')
        ->where(['model_id' => $model->id])
        ->andWhere("model LIKE 'backend_\modules_\contabilidad_\models_\Venta'");
    $dataProvider = new ActiveDataProvider([
        'query' => \bedezign\yii2\audit\models\AuditTrail::find()
            ->select(['*'])
            ->where("entry_id in ({$subquery->createCommand(Yii::$app->db)->rawSql})")
            ->andWhere("(model LIKE 'backend_\modules_\contabilidad_\models_\Venta' OR model LIKE 'backend_\modules_\contabilidad_\models_\DetalleVenta')"),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    echo GridView::widget([
        'id' => 'grid-auditoria',
        'dataProvider' => $dataProvider,
        'panel' => [
            'type' => 'info',
            'heading' => "<label class='label label-info'>HISTORIAL de " . get_class($model) . "</label>",
        ],
        'columns' => [
            'id',
            'entry_id',
            [
                'attribute' => 'user_id',
                'label' => Yii::t('audit', 'Usuario'),
                'class' => 'yii\grid\DataColumn',
                'options' => ['width' => '150px'],
                'value' => function ($data) {
                    $usuario = User::findOne($data->user_id);
                    return $usuario->username;
                },
                'format' => 'raw',
            ],
            'model_id',
            [
                'attribute' => 'action',
                'filter' => ['CREATE' => 'CREADO', 'UPDATE' => 'MODIFICADO', 'DELETE' => "BORRADO"],
                'value' => function ($model) {
                    return ucwords($model->action);
                }
            ],
            [

                'attribute' => 'field',
                'format' => 'raw',
                'value' => function ($model) {
                    $array = ($model->model)::getAttributesLabelText();
                    $key = $model->field;
                    $msg = '';

                    if (array_key_exists($key, $array))
                        return $array[$key];
                    else {
                        if ($key == '' || !isset($key) || empty($key))
                            $msg = "<label class='label label-warning'>null</label>";
                        else
                            $msg = "<label class='label label-warning'>El campo <strong>{$key}</strong> no existe.</label>";
                    }

                    return $msg;
                }
            ],
            [
                'label' => Yii::t('audit', 'Diff'),
                'value' => function ($model) {
                    $old = explode("\n", $model->old_value);
                    $new = explode("\n", $model->new_value);

                    foreach ($old as $i => $line) {
                        $old[$i] = rtrim($line, "\r\n");
                    }
                    foreach ($new as $i => $line) {
                        $new[$i] = rtrim($line, "\r\n");
                    }

                    $diff = new \Diff($old, $new);
                    $return = $diff->render(new \Diff_Renderer_Html_Inline);
                    return $return; //$no_diferencias ? $return : str_replace(['<del>', '</del>', '<ins>', '</ins>'], '', $return);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'created',
                'label' => 'Fecha',
                'options' => ['width' => '150px'],
                'format' => 'html',
                'filter' => DateRangePicker::widget([
                    'name' => 'date_range_1',
                    'useWithAddon' => false,
                    'language' => 'es',
                    'hideInput' => true,
                    //'presetDropdown' => true,
                    'pluginOptions' => [
                        'locale' => ['format' => 'DD-MM-YYYY'],
                        'separator' => '-',
                        'opens' => 'left'
                    ]
                ])
            ],
        ],
    ]);
} catch (Exception $exception) {
    echo $exception;
}
