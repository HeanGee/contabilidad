<?php

use backend\modules\contabilidad\models\DetalleVenta;
use common\helpers\ValorHelpers;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
?>

<?php Pjax::begin(['id' => 'asiento-simulator-div']);

/** @var DetalleVenta[] $detalles */

$sesion = Yii::$app->session;
$detalles = $sesion->has('cont_detalleventa_sim-provider')
&& $sesion->get('cont_detalleventa_sim-provider')->allModels !== '' ?
    $sesion->get('cont_detalleventa_sim-provider')->allModels : [];
$hayDetalles = empty($detalles) ? "no" : "si";
$total_debe = 0.0;
$total_haber = 0.0;

function searchForId($id, $array, $saldo)
{
    foreach ($array as $key => $val) {
        if (array_key_exists('codigo', $val) && array_key_exists('saldo', $val) && $val['codigo'] === $id && $val['saldo'] === $saldo) {
            return $key;
        }
    }
    return -1;
}

function nf($n)
{
    $n = number_format($n, '2', '.', '');
    $n = $n - ((int)$n) > 0 ? number_format($n, '2', ',', '.')
        : number_format((int)$n, '0', ',', '.');

    return $n;
}

?>
<div class="panel panel-primary" id="asientoSimulator" style="display: none;">
    <div class="panel-heading"><h3 class="panel-title">Simulación de asientos</h3></div>
    <div class="panel-body">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Código</th>
                <th>Cuenta</th>
                <th></th>
                <th style="text-align: right">Debe</th>
                <th style="text-align: right">Haber</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $cuentasDebe = [];
            $cuentasHaber = [];
            $filas = [];
            $html_options = ["style" => "text-align: right;", "class" => "borde-medio"];
            if ($hayDetalles === 'si') {
                foreach ($detalles as $detalle) {
                    if ($detalle->cta_contable == 'debe') {
                        $index = searchForId($detalle->planCuenta->cod_completo, $filas, 'debe');
                        if ($index > -1) {
                            $filas[$index]['suma'] += (float)$detalle->subtotal;
                        } else {
                            $filas[] = ['codigo' => $detalle->planCuenta->cod_completo, 'nombre' => $detalle->planCuenta->nombre, 'suma' => $detalle->subtotal, 'saldo' => 'debe'];
                        }

                        $total_debe += (float)$detalle->subtotal;
                    } else {
                        $index = searchForId($detalle->planCuenta->cod_completo, $filas, 'haber');

                        if ($index > -1) {
                            $filas[$index]['suma'] += (float)$detalle->subtotal;
                        } else {
                            $filas[] = ['codigo' => $detalle->planCuenta->cod_completo, 'nombre' => $detalle->planCuenta->nombre, 'suma' => $detalle->subtotal, 'saldo' => 'haber'];
                        }

                        $total_haber += (float)$detalle->subtotal;
                    }
                }

                $enDebe = 0;
                foreach ($filas as $fila) {
                    if ($fila['saldo'] === 'debe') {
                        echo Html::beginTag('tr');
                        if ($enDebe % 2 === 0) {
                            $enDebe++;
//                            echo '<tr style="border-top:2pt solid black">';
                            echo '<tr style="border-top:1pt solid black">';
                        } else {
                            echo Html::beginTag('tr');
                        }
                        echo Html::beginTag('td') . $fila['codigo'] . Html::endTag('td');
                        echo Html::beginTag('td') . $fila['nombre'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
//                        echo Html::beginTag('td') . (nf($fila['suma'])) . Html::endTag('td');
                        echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormatMonedaSensitive($fila['suma'], 0) . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::endTag('tr');
                    } else {
                        if ($enDebe % 2 !== 0) $enDebe++;
                        echo Html::beginTag('tr');
                        echo Html::beginTag('tr');
                        echo Html::beginTag('td') . $fila['codigo'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td') . "a " . $fila['nombre'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormatMonedaSensitive($fila['suma'], 0) . Html::endTag('td');
                        echo Html::endTag('tr');
                    }
                }
                echo '<tr style="border-top:2pt solid black">';
                echo Html::beginTag('td') . Html::endTag('td');
                echo Html::beginTag('td') . Html::endTag('td');
                echo Html::beginTag('td') . '<strong><b><i><font size="5">TOTAL:</font></i></b></strong>' . Html::endTag('td');
                echo Html::beginTag('td', $html_options) . '<strong><b><i><font size="5">' . ValorHelpers::numberFormatMonedaSensitive($total_debe, 0) . '</font></i></b></strong>' . Html::endTag('td');
                echo Html::beginTag('td', $html_options) . '<strong><b><i><font size="5">' . ValorHelpers::numberFormatMonedaSensitive($total_haber, 0) . '</font></i></b></strong>' . Html::endTag('td');
                echo Html::endTag('tr');
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<?php

$scripts = [];

$scripts[] = <<<JS
$(document).ready(function () {
    // Mostrar/ocultar dependiendo de si hay detalles con cuentas
    var hayDetalles = "$hayDetalles";
    // if(hayDetalles === "no")
    //     $('#asientoSimulator').hide();
    // else
    //     $('#asientoSimulator').show();
    
    if(hayDetalles === "si")
        $('#asientoSimulator').show();
});
JS;

foreach ($scripts as $script) $this->registerJs($script);

Pjax::end();

$CSS = <<<CSS
td.borde_medio, tr.borde_medio {
    border-right: 2px solid black;
    border-left: 2px solid black;
}
CSS;
$this->registerCss($CSS);
?>
