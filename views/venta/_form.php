<?php

use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\Venta;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\checkbox\CheckboxX;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Venta */
/* @var $form kartik\form\ActiveForm */

$action = Yii::$app->controller->action->id;
?>

<div class="venta-form">
    <?php $form = ActiveForm::begin(); ?>

    <!--    <?php //echo $form->errorSummary($model); ?>-->

    <?php
    $boton = Html::label('Agregar', 'boton_add_timbrado', ['class' => 'control-label']) . '<br/>'
        . Html::button('<i class="glyphicon glyphicon-plus"></i>', [
            'id' => 'boton_add_timbrado',
            'style' => "display: yes;",
            'type' => 'button',
            'title' => 'Agregar.',
            'class' => 'btn btn-success tiene_modal2',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['timbrado/create-new-timbrado']),
            'data-pjax' => '0',
        ]);
    try {
//        $estado_excluir = Yii::$app->controller->action->id == 'create' ? 'anulada' : '';
        $estado_excluir = '';


        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 11,
                    'attributes' => [
                        'tipo_documento_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'tipo_documento_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione un tipo de documento'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => TipoDocumento::getTiposDocumentoConId('cliente'),
                                ],
                                'initValueText' => !empty($model->tipo_documento_id) ? ($model->tipo_documento_id . ' - ' . $model->tipoDocumento->nombre) : '',
//                                'pluginEvents' => [
//                                    'change' => "function(){
//                                        $('#venta-timbrado_detalle_id_prefijo').val('').trigger('change');
//                                    }"
//                                ]
                            ])
                        ],
                        'fecha_emision' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'fecha_emision')->widget(MaskedInput::className(), [
                                'name' => 'fecha_emision',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ]),
                        ],
                        'timbrado_detalle_id_prefijo' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'timbrado_detalle_id_prefijo')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione uno ...'],
//                                'initValueText' => !empty($model->timbrado_detalle_id_prefijo) ? $model->timbrado_detalle_id_prefijo . ' - ' . $model->prefijo : '', // buscar por id en select2 no funciona
                                'initValueText' => !empty($model->timbrado_detalle_id_prefijo) ? $model->prefijo /*. ' (' . $model->nro_timbrado . ')'*/ : '',
                                'pluginOptions' => [
                                    'allowClear' => true,
//                                    'ajax' => [
//                                        'url' => Url::to(['timbrado/get-prefs-emp-actual']),
//                                        'dataType' => 'json',
//                                        'data' => new JsExpression("
//                                            function(params) {
//                                                return {
//                                                    q:params.term,
//                                                    id:$('#venta-tipo_documento_id').val(),
//                                                    fecha_factura: $('#venta-fecha_emision').val(),
//
//                                                };
//                                            }
//                                        "),
//                                    ]
                                ]
                            ])
                        ],
                        'nro_factura' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1'],
                            'value' => $form->field($model, 'nro_factura')->widget(MaskedInput::className(), [
//                                    'mask' => '999-999-9999999',
                                'mask' => '9999999',
                            ]),
                            'options' => [
                                'placeholder' => 'Ingrese nro de factura...',
                            ],
                        ],
                        'nro_timbrado' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '2'],
                            'options' => [
                                'readonly' => true,
                                'disabled' => true,
                            ]
                        ],
                        'estado' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1'],
                            'value' => $form->field($model, 'estado')->widget(Select2::className(), [
                                'data' => Venta::getEstados(false, $estado_excluir),
                                'options' => [
                                    'placeholder' => 'Seleccione un estado ...',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                            ])
                        ],
                        'usar_tim_vencido' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1', 'style' => 'width:6%;'],
                            'value' => $form->field($model, 'usar_tim_vencido')->widget(CheckboxX::className(), [
                                'pluginOptions' => ['size' => 'lg', 'threeState' => false]
                            ]),
                        ],
                    ],
                ],
            ],
        ]);
    } catch (Exception $e) {
        throw $e;
    } ?>

    <?php
    try {
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'condicion' => [
//                            'type' => Form::INPUT_RAW,
//                            'columnOptions' => ['colspan' => '1'],
//                            'value' => $form->field($model, 'condicion')->widget(Select2::className(), [
//                            'data' => ['contado' => '1 - Contado', 'credito' => '2 - Credito'],
//                                'data' => Venta::getCondiciones(),
//                                'options' => ['disabled' => true],
//                                'disabled' => true,
//                                'pluginEvents' => [
//                                    'change' => "function(){}"
//                                ]
//                            ])
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '1'],
                            'value' => $form->field($model, 'condicion'),
                        ],
                        'ruc' => [
                            'label' => 'R.U.C.',
                            'columnOptions' => ['colspan' => '2'],
                            'type' => Form::INPUT_TEXT,
                        ],
                        'nombre_entidad' => [
                            'label' => 'Entidad',
                            'columnOptions' => ['colspan' => '4'],
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'readonly' => true,
                                'disabled' => true,
                            ]
                        ],
                        'moneda_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'moneda_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione una moneda'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => Venta::getMonedas(),
                                ],
                                'initValueText' => !empty($model->moneda_id) ? ($model->moneda_id . ' - ' . $model->moneda->nombre) : ''
                            ])
                        ],
                        'valor_moneda' => [
                            'columnOptions' => ['colspan' => '2'],
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($model, 'valor_moneda')->widget(NumberControl::className(), [
                                'value' => 0.00,
                                'maskedInputOptions' => [
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'rightAlign' => true,
                                    'allowMinus' => false,
                                ],
                            ]),
                        ],
                        'cotizacion_propia' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'cotizacion_propia')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione una moneda'],
                                'pluginOptions' => [
                                    'data' => Venta::getCotizacionPropiaOptions(),
                                    'allowClear' => true,
                                ],
                                'initValueText' => isset($model->cotizacion_propia) ? ucfirst($model->cotizacion_propia) : "",
                            ]),
                        ],
                    ],
                ],
            ],
        ]);

        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 8,
            'attributes' => [
                'obligacion_id' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => $form->field($model, 'obligacion_id')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Seleccione una obligac...'],
                        'pluginOptions' => [
                            'data' => Venta::getObligacionesEmpresaActual(Yii::$app->session->get('core_empresa_actual'), ($action == 'update') ? $model : null),
                            'allowClear' => false,
                        ],
                        'initValueText' => isset($model->obligacion) ? ucfirst($model->obligacion->nombre) : "",
                    ]),
                ],
                'para_iva' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => $form->field($model, 'para_iva')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Seleccione una obligac...'],
                        'data' => ['si' => "Sí", 'no' => "No"],
                        'pluginOptions' => [
                            'allowClear' => false,
                        ],
                        'initValueText' => ($model->para_iva == 'si') ? "Sí" : 'No',
                    ]),
                ],
                'cant_cuotas' => [
                    'label' => 'Cant. Cuotas',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::class,
                    'options' => [
                        'readonly' => true,
                        'class' => 'form-control .total-class',
                        'value' => 0.00,
                        'maskedInputOptions' => [
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'rightAlign' => true,
                            'allowMinus' => false,
                        ],
                    ],
                ],
                'fecha_vencimiento' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => $form->field($model, 'fecha_vencimiento')->widget(MaskedInput::className(), [
                        'name' => 'fecha_vencimiento',
                        'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                    ]),
                ],
            ],
        ]);
//        echo $form->field($model, 'observaciones')->textInput(['maxlength' => 128, 'placeholder' => 'Describa el motivo por el cual el estado no es vigente (max 128 caracteres)'])->label("Motivo de No-Vigente");
        echo '<fieldset><legend>Motivo de no Vigencia</legend>';
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 12,
            'attributes' => [
                'observaciones' => [
                    'type' => Form::INPUT_TEXT,
                    'columnOptions' => ['colspan' => '10'],
                    'options' => ['maxlength' => 128, 'placeholder' => 'Describa el motivo por el cual el estado no es vigente (max 128 caracteres)'],
                    'label' => false,
                ],
                'actions' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => Html::a('Obtener motivo', ['#'], ['href' => 'javascript:void(0)', 'class' => 'obtener-motivo btn btn-primary'])
                ]
            ]
        ]);
        echo '</fieldset>';

        echo $this->render('_form_plantillas', ['model' => $model, 'form' => $form]);
    } catch (Exception $e) {
        echo $e;
    }

    $totales = [];
    $gravadas = [];
    $ivas = [];
    /** @var IvaCuenta $iva_cta */
    foreach (IvaCuenta::find()->all() as $iva_cta) {
        $totales['iva-' . $iva_cta->iva->porcentaje . ''] = [
            'label' => 'Total ' . (($iva_cta->iva->porcentaje !== 0) ? ' ' . $iva_cta->iva->porcentaje . '%' : 'Exenta'),
            'type' => Form::INPUT_WIDGET,
            'widgetClass' => NumberControl::class,
            'columnOptions' => ['colspan' => '2'],
            'options' => [
                'value' => 0.00,
                'maskedInputOptions' => [
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'rightAlign' => true,
                    'allowMinus' => false,
                ],
                'readonly' => true,
                'disabled' => true,
            ],
        ];
        if ($iva_cta->iva->porcentaje != 0) {
            $gravadas['gravada-' . $iva_cta->iva->porcentaje . ''] = [
                'label' => 'Gravada ' . $iva_cta->iva->porcentaje . '%',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::class,
                'columnOptions' => ['colspan' => '2'],
                'options' => [
                    'value' => 0.00,
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'rightAlign' => true,
                        'allowMinus' => false,
                    ],
                    'readonly' => true,
                    'disabled' => true,
                ],
            ];
            $ivas['iva-' . $iva_cta->iva->porcentaje . ''] = [
                'label' => 'I.V.A. ' . $iva_cta->iva->porcentaje . '%',
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::class,
                'columnOptions' => ['colspan' => '2'],
                'options' => [
                    'value' => 0.00,
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'rightAlign' => true,
                        'allowMinus' => false,
                    ],
                    'readonly' => true,
                    'disabled' => true,
                ],
            ];
        }
    }
    // Ordenar campos
    uksort($totales, 'strnatcasecmp');
    uksort($gravadas, 'strnatcasecmp');
    uksort($ivas, 'strnatcasecmp');

    // concatenar para renderizar con Form::widget
    $ivas_gravadas = array_merge($ivas, $gravadas);

    try {
        echo '<fieldset><legend>Sumatoria</legend></fieldset>';
        echo Form::widget([
            // formName is mandatory for non active forms
            // you can get all attributes in your controller
            // using $_POST['kvform']
            'formName' => 'totals',

            // default grid columns
            'columns' => 8,

            // set global attribute defaults
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['colspan' => '2'],
                'inputContainer' => ['colspan' => '2'],
                'container' => ['class' => 'form-group totales'],
            ],

            'attributes' => $totales,
        ]);

        echo '<fieldset><legend>Discriminadas</legend></fieldset>';
        echo Form::widget([
            'formName' => 'ivas',

            // default grid columns
            'columns' => 8,

            // set global attribute defaults
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
//                'labelOptions' => ['colspan' => '3'],
//                'inputContainer' => ['colspan' => '3'],
                'container' => ['class' => 'form-group ivas'],
            ],

            'attributes' => $ivas_gravadas,
        ]);

        echo '<fieldset><legend>Totales</legend></fieldset>';
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 6,
            'attributes' => [       // 1 column layout
                'total' => [
                    'label' => 'Total Factura',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::class,
                    'options' => [
                        'readonly' => true,
                        'disabled' => true,
                        'class' => 'form-control total-class',
                        'value' => 0.00,
                        'maskedInputOptions' => [
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'rightAlign' => true,
                            'allowMinus' => false,
                        ],
                    ],
                ],
                '_total_ivas' => [
                    'label' => 'Total I.V.A.',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::class,
                    'options' => [
                        'readonly' => true,
                        'disabled' => true,
                        'class' => 'form-control .total-class',
                        'value' => 0.00,
                        'maskedInputOptions' => [
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'rightAlign' => true,
                            'allowMinus' => false,
                        ],
                    ],
                ],
                '_total_gravadas' => [
                    'label' => 'Total Gravada',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::class,
                    'options' => [
                        'readonly' => true,
                        'disabled' => true,
                        'class' => 'form-control .total-class',
                        'value' => 0.00,
                        'maskedInputOptions' => [
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'rightAlign' => true,
                            'allowMinus' => false,
                        ],
                    ],
                ],
            ],
        ]);

        echo '<fieldset><legend>&nbsp;</legend></fieldset>';
    } catch (Exception $e) {
    }
    ?>

    <?php $botonText = '';
    $botonClass = '';
    if ($botonText = Yii::$app->controller->action->id == 'create') {
        $botonText = 'Guardar y Siguiente';
        $botonClass = 'btn btn-success';
    } else {
        $botonText = 'Guardar';
        $botonClass = 'btn btn-primary';
    } ?>

    <div class="form-group btn-toolbar">
        <?= Html::submitButton($botonText, [
            'id' => 'btn_submit_factura_venta',
            'class' => $botonClass,
            "data" => (Yii::$app->controller->action->id == 'update') ? [
                'confirm' => 'Desea guardar los cambios?',
                'method' => 'post',
            ] : []]) ?>
        <?php if (Yii::$app->controller->action->id == 'create') {
            echo Html::a('Guardar y Cerrar', ['create', 'guardar_salir' => true], [
                'class' => 'btn btn-primary',
                'id' => 'btn_submit_close_factura_venta',
                "data" => (Yii::$app->controller->action->id == 'update') ? [
                    'confirm' => 'Desea guardar los cambios?',
                    'method' => 'post',
                ] : [
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        <?= Html::button('', ['class' => 'btn-simular hidden']) ?>
    </div>

    <?php

    echo $this->render('_form_detalle', []);

    echo $this->render('_form_asientoSimulator', [/*'venta' => $model*/]);

    echo $this->render('_form_asientoSimulator_costo', [/*'venta' => $model*/]);

    ActiveForm::end();

    // Cargamos js dede otro archivo
    echo $this->render('_form2_js', ['totales' => $totales, 'model' => $model, 'boton' => $boton]); ?>

</div>