<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Venta */

$this->title = 'Modificar Factura de venta ' . $model->getNroFacturaCompleto();
$this->params['breadcrumbs'][] = ['label' => 'Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="venta-update">

    <?php
    $es_nota = in_array($model->tipo, ['nota_credito', 'nota_debito']) ? $model->tipo : false;
    if (!$es_nota) {
        echo Html::beginTag('div', ['class' => 'btn-toolbar']);
        echo $this->render('alter-data', ['model' => $model]);
        echo Html::a('<span class="glyphicon glyphicon-arrow-right"></span>',
            ['navegate', 'id' => $model->id, 'goto' => 'next'], ['id' => 'btn_next', 'class' => 'btn btn-primary pull-right', 'title' => 'Siguiente']);
        echo Html::a('<span class="glyphicon glyphicon-plus"></span>',
            ['create'], ['id' => 'btn_new', 'class' => 'btn btn-success pull-right', 'title' => 'Nuevo']);
        echo Html::a('<span class="glyphicon glyphicon-arrow-left"></span>',
            ['navegate', 'id' => $model->id, 'goto' => 'prev'], ['id' => 'btn_prev', 'class' => 'btn btn-primary pull-right', 'title' => 'Anterior']);
        echo Html::endTag('div');
        echo "</br>";
    }
    ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

$scripts = <<<JS
JS;

$this->registerJs($scripts);
?>

