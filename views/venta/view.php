<?php

use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\Venta;
use common\helpers\ValorHelpers;
use kartik\detail\DetailView;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Venta */

$this->title = "Datos de la venta " . $model->getNroFacturaCompleto();
$this->params['breadcrumbs'][] = ['label' => 'Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="venta-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-venta-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-venta-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-venta-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-venta-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-venta-delete'),
            'bloquear' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-venta-bloquear'),
            'desbloquear' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-venta-desbloquear'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Regresar a Ventas', ['index',], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] && $model->bloqueado == 'no' ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <!--        --><?= $permisos['delete'] ? '' : ''//Html::a('Borrar', ['delete', 'id' => $model->id], [
        //            'class' => 'btn btn-danger',
        //            'data' => [
        //                'confirm' => 'Estás seguro de borrar este elemento?',
        //                'method' => 'post',
        //            ],
        //        ]) : null     ?>
        <?= "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" ?>
        <?= $permisos['bloquear'] && $model->bloqueado == 'no' ? Html::a('<span>Bloquear</span>', ['bloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'title' => Yii::t('app', 'Bloquear'),
                'data-confirm' => 'Está seguro que desea bloquear la factura?'
            ]) : '' ?>
        <?= $permisos['bloquear'] && $model->bloqueado == 'si' ? Html::a('<span>Desbloquear</span>', ['desbloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-success',
                'title' => Yii::t('app', 'Desbloquear'),
                'data-confirm' => 'Está seguro que desea desbloquear la factura?'
            ]) : '' ?>
    </p>

    <?php
    try {
        // cabecera
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Factura ' . $model->prefijo . '-' . $model->nro_factura,
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'columns' => [
                        [
                            'label' => 'Nro de Factura',
                            'value' => $model->getNroFacturaCompleto(),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Total factura',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->total, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Timbrado',
                            'value' => isset($model->timbradoDetalle) ? $model->timbradoDetalle->timbrado->nro_timbrado : '-sin timbrado-',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Saldo',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->saldo, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],

                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Tipo de Documento',
                            'value' => $model->tipo_documento_id != null ? $model->tipoDocumento->nombre : '',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Exenta',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->exenta, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Fecha de emisión',
                            'value' => date_create_from_format('Y-m-d', $model->fecha_emision) ?
                                date_create_from_format('Y-m-d', $model->fecha_emision)->format('d-m-Y') : "-no definido-",
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Gravada 10%',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->gravada10, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Entidad',
                            'format' => 'raw',
                            'value' => ($model->entidad_id) ? "{$model->entidad->razon_social} <strong>(RUC: {$model->entidad->ruc})</strong>" : '',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Impuesto 10%',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->impuesto10, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Moneda',
                            'value' => ($model->moneda_id) ? $model->moneda->nombre : '',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Gravada 5%',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->gravada5, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Empresa',
                            'value' => $model->empresa->razon_social,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Impuesto 5%',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->impuesto5, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Estado',
                            'format' => 'raw',
                            'value' => '<label class="label label-' . (($model->estado == 'vigente') ? 'success">Vigente' : (($model->estado == 'faltante') ? 'warning">Faltante' : 'danger">Anulada')) . '</label>',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Plantillas',
                            'value' => $model->plantillas,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Creado',
                            'format' => 'raw',
                            'value' => "{$model->creado} <strong>(Por `{$model->creado_por}`)</strong>",
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Modificado',
                            'value' => $model->modificado,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Observaciones',
                            'value' => $model->observaciones,
                            'valueColOptions' => ['style' => 'width:100%']
                        ],
                    ],
                ],
            ]
        ]);

        // detalles
        $dataProvider = new ActiveDataProvider([
            'query' => DetalleVenta::find()->where(['factura_venta_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'panel' => ['type' => 'info', 'heading' => 'Detalles', 'footer' => false,],
            'toolbar' => [],
            'hover' => true,
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
            'columns' => [
                'id',
                [
                    'class' => DataColumn::className(),
                    'label' => 'Código de cuenta',
                    'value' => 'planCuenta.cod_completo'
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Nombre de cuenta',
                    'value' => 'planCuenta.nombre',
                    'pageSummaryOptions' => [
                        'data-colspan-dir' => 'ltr',
                        'colspan' => 1,
                        'style' => 'text-align: right;',
                    ],
                    'pageSummary' => function ($summary, $data, $widget) {
                        return "Totales";
                    },
                    'contentOptions' => function ($model) {
                        if ($model->cta_contable == 'debe') return ['style' => 'padding:8px 6px 0px 10px; text-align:left'];
                        return ['style' => 'padding:8px 6px 0px 150px; text-align:left'];
                    },
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Debe',
                    'value' => function ($model) {
                        /** @var $model DetalleVenta */
                        if ($model->cta_contable == 'debe') return $model->subtotal;
                        return 0;
                    },
                    'format' => 'decimal',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'append' => '',
                        'prepend' => '',
                    ],
                    'contentOptions' => ['style' => 'width: 12%; text-align:right'],
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Haber',
                    'value' => function ($model) {
                        /** @var $model DetalleVenta */
                        if ($model->cta_contable == 'haber') return $model->subtotal;
                        return 0;
                    },
                    'format' => 'decimal',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'append' => '',
                        'prepend' => '',
                    ],
                    'contentOptions' => ['style' => 'width: 12%; text-align:right'],
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Cuenta principal ?',
                    'value' => function ($model) {
                        if ($model->cta_principal == 'si')
                            return ucfirst($model->cta_principal);
                        return '';
                    },
                    'contentOptions' => ['style' => 'width: 8%; text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;'],
                ],
            ],
        ]);

        // notas
        $query = Venta::find()->where(['factura_venta_id' => $model->id]);
        if ($query->exists()) {
            $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => false]);
            echo $this->render('../venta-nota/view/_view_notas',
                ['dataProvider' => $dataProvider, 'heading' => "Notas de Crédito"]);
        }

        // Mostrar activos fijos vendidos
        $activosFijos = \backend\modules\contabilidad\models\ActivoFijo::findAll(['factura_venta_id' => $model->id]);
        if (!empty($activosFijos)) {
            echo $this->render('../activo-fijo/views/_view_actfijos', [
                'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $activosFijos, 'pagination' => false]),
                'heading' => 'Datos deActivos Fijos vendidos'
            ]);
        }

        // Mostrar activos fijos biologicos vendidos
        $actbioManagers = ActivoBiologicoStockManager::find()->where(['factura_venta_id' => $model->id])->andWhere(['IS', 'nota_venta_id', null]);
        if ($actbioManagers->exists()) {
            $allModels = [];
            /** @var ActivoBiologicoStockManager $manager */
            foreach ($actbioManagers->all() as $manager) {
                $allModels[] = $manager->activoBiologico;
            }
            echo $this->render('../activo-biologico/views/_view_actbios', [
                'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $allModels, 'pagination' => false]),
                'heading' => 'Datos de Activos Biológicos vendidos'
            ]);

            $actbioManagers = ActivoBiologicoStockManager::find()->where(['factura_venta_id' => $model->id]);
            $dataProvider = new ActiveDataProvider([
                'query' => $actbioManagers, 'pagination' => false,
            ]);
            echo GridView::widget([
                'id' => 'grid-test',
                'dataProvider' => $dataProvider,
                'toolbar' => [],
                'hover' => true,
                'panel' => [
                    'type' => 'info',
                    'heading' => 'Cantidades vendidas/retornadas por Activos Biologicos',
                    'footerOptions' => ['class' => ''],
                    'beforeOptions' => ['class' => ''],
                    'afterOptions' => ['class' => '']
                ],
                'panelFooterTemplate' => '',
                'columns' => [
                    [
                        'label' => 'Activo',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ActivoBiologicoStockManager $manager */
                            $manager = $model;
                            $activo = ActivoBiologico::findOne(['especie_id' => $manager->especie_id, 'clasificacion_id' => $manager->clasificacion_id]);
                            if (!isset($activo)) return '';

                            $url = Url::to(['activo-biologico/view', 'id' => $activo->id]);
                            $link = Html::a($activo->nombre, $url, []);
                            return $link;
                        },
                        'pageSummaryOptions' => [
                            'data-colspan-dir' => 'ltr',
                            'colspan' => 1,
                            'style' => 'text-align: right;',
                        ],
                        'pageSummary' => function ($summary, $data, $widget) {
                            return "Total";
                        },
                        'contentOptions' => ['style' => 'text-align:center'],
                        'headerOptions' => ['style' => 'text-align:center; width:5%'],
                    ],
                    [
                        'label' => 'Cantidad Vendida',
                        'format' => 'decimal',
                        'value' => function ($model) {
                            /** @var ActivoBiologicoStockManager $manager */
                            $manager = $model;
                            if (isset($manager->nota_venta_id))
                                return -$manager->cantidad;
                            return $manager->cantidad;
                        },
                        'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:center'],
                        'headerOptions' => ['style' => 'text-align:center; width:15%'],

                    ],
                    [
                        'format' => 'raw',
                        'value' => function ($model) {
                            /** @var ActivoBiologicoStockManager $manager */
                            $manager = $model;
                            if (isset($manager->nota_venta_id)) {
                                $url = Url::to(['venta-nota/view', 'id' => $manager->nota_venta_id]);
                                $a = Html::a($manager->notaVenta->getNroFacturaCompleto(), $url, ['target' => '_blank']);
                                return "Retornada mediante nota {$a}";
                            }
                            return "Vendida por esta factura";
                        },
                    ],
                ],
            ]);
        }

        echo $this->render('../auditoria/_auditoria', ['modelo' => $model]);
        echo $this->render('../auditoria/_auditoria_multiple_models', ['modelo' => new DetalleVenta(), 'ids' => $model->getDetallesId()]);

    } catch (\Exception $e) {
        print $e;
        /* TODO: manejar */
    }
    ?>

</div>
