<?php

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $venta_model backend\modules\contabilidad\models\Venta */

?>

<div id="cuotas_div" style="display: <?php echo $venta_model->condicion != 'credito' ? 'none' : 'yes' ?>">
<!--<div id="cuotas_div">-->
    <?php Pjax::begin(['id' => 'cuotas_grid']) ?>
    <?php
    $cuotas = \Yii::$app->session->get('cont_cuotas_venta-provider')->allModels;
    $hayCuotas = empty($cuotas) ? "no" : "si";

    $columns = [
        [
            'label' => 'Nº Cuota',
            'value' => 'nro_cuota',
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'value' => function ($venta_model) {
                return number_format($venta_model->monto, '2', ',', '.');
            },
            'label' => 'Monto',
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'label' => 'Fecha de Vencimiento',
            'value' => 'fecha_vencimiento',
            'format' => ['date', 'php:d-m-Y'],
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        false,
                        [
                            'title' => 'Editar Cuota',
                            'class' => 'tiene_modal',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'data-url' => $url,
                            'data-pjax' => '0',
                            'data-title' => 'Editar Cuota'
                        ]
                    );
                }
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'update') {
                    $url = Url::to(['/contabilidad/cuota-venta/update-cuota', 'indice' => $index]);
                    return $url;
                }
                return '';
            },
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ]
    ];
    try {
        echo GridView::widget([
            'dataProvider' => Yii::$app->getSession()->get('cont_cuotas_venta-provider'),
            'columns' => $columns,
            'hover' => true,
            'id' => 'grid',
            'panel' => ['type' => 'primary', 'heading' => 'Cuotas', 'footer' => false,],
            'toolbar' => [
                'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                    'type' => 'button',
                    'id' => 'add_cuotas_id',
                    'title' => 'Agregar Cuotas',
                    'class' => 'btn btn-success tiene_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'data-url' => Url::to(['cuota-venta/add-cuotas']),
                    'data-pjax' => '0',
                    'data-title' => 'Agregar Cuotas'])
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>
    <?php Pjax::end() ?>

    <?php
    $url = \yii\helpers\Json::htmlEncode(\Yii::t('app', Url::to(['venta/borrar-cuotas-session'])));
    $script = <<<JS
// Mostrar/ocultar panel de cuotas según condición factura y borra de sesión las cuotas
$('#venta-condicion').on('change', function () {
    var condicion = $('#venta-condicion').val();
    if (condicion === 'contado') {
        $('#cuotas_div').hide();

        // Borrar de sesión las cuotas ingresadas.
        $.ajax({
            url: $url,
            type: 'post',
            success: function (data) {
                $.pjax.reload({container: "#cuotas_grid", async: false});
            }
        });
        
    }
    else {
        $('#cuotas_div').show();
        //Mostrar modal para carga de cuotas
        // if (localStorage.getItem("carga_promira_vez_cuotas")==='true'){
        //     document.getElementById("add_cuotas_id").click();
        //      localStorage.setItem("carga_promira_vez_cuotas", false);
        // }

    }
});

// Al principio, si no hay cuotas en la sesion, no mostrar el grid.
var hayCuotas = "$hayCuotas";
if (hayCuotas === "no")
    $('#cuotas_div').hide();
else
    $('#cuotas_div').show();
JS;
    $this->registerJs($script);
    ?>

</div>