<?php

use backend\modules\contabilidad\models\PolizaDevengamiento;
use common\helpers\FlashMessageHelpsers;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Poliza */

$this->title = 'Póliza ' . $model->nro_poliza;
$this->params['breadcrumbs'][] = ['label' => 'Pólizas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poliza-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Pólizas', ['index'/*, 'operacion' => Yii::$app->request->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar Póliza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Borrar esta Póliza', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Realmente desea borrar esta Póliza?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos principales',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'attribute' => 'fecha_desde',
                    'value' => date('d-m-Y', strtotime($model->fecha_desde))
                ],
                [
                    'attribute' => 'fecha_hasta',
                    'value' => date('d-m-Y', strtotime($model->fecha_hasta))
                ],
                'nro_poliza',
                [
                    'label' => 'Sección',
                    'value' => isset($model->seccion) ? $model->seccion->nombre : "-no definido-",
                ],
            ],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => PolizaDevengamiento::find()->where(['poliza_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => 'Devengamientos',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'columns' => [
                [
                    'label' => 'Mes Año',
                    'value' => function ($model, $key, $index, $column) {
                        return date_create_from_format('Y-m', $model->anho_mes)->format('m-Y');
                    },
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:center'],
                    'headerOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:center'],
                ],
                [
                    'label' => 'Días',
                    'value' => 'dias',
                    'contentOptions' => ['style' => 'width: 90px; padding:8px 6px 0px 0px; text-align:center'],
                    'headerOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:center'],
                ],
                [
                    'label' => 'Importe',
                    'value' => function ($model, $key, $index, $column) {
                        return number_format($model->monto, '0', ',', '.');
                    },
                    'contentOptions' => ['style' => 'width: 190px; padding:8px 6px 0px 0px; text-align:right'],
                    'headerOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:center'],

                ],
                [
                    'label' => '¿Asentado?',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        $msg = isset($model->asiento) ? 'Sí' : 'No';
                        $class = isset($model->asiento) ? 'label label-success' : 'label label-danger';
                        $span = "<label class='{$class}'>{$msg}</label>";
                        return $span;
                    },
                    'contentOptions' => ['style' => 'width: 190px; padding:8px 6px 0px 0px; text-align:center'],
                    'headerOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:center'],
                ],
            ],
        ]);

        // Facturas relacionadas
        echo $this->render('@backend/modules/contabilidad/views/facturas-relacionadas/_facturas_relacionadas', ['model' => $model]);
    } catch (Exception $exception) {
        FlashMessageHelpsers::createWarningMessage($exception);
    } ?>

</div>
