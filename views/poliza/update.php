<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Poliza */
/* @var $compra_id int */

$this->title = 'Modificar Póliza: ' . $model->nro_poliza;
$this->params['breadcrumbs'][] = ['label' => 'Pólizas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="poliza-update">

    <?= $this->render('_form', [
        'model' => $model,
        'compra_id' => $compra_id,
    ]) ?>

</div>
