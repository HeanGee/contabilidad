<?php

use backend\modules\contabilidad\models\Poliza;
use backend\modules\contabilidad\models\PolizaSeccion;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model Poliza */
/* @var $form ActiveForm */
/* @var $compra_id int */

?>

<?php
$forModal = Yii::$app->request->getQueryParam('formodal');
$actvFormOptions = ['id' => 'poliza-form'];

if ($forModal == 'true') {
    ActiveFormDisableSubmitButtonsAsset::register($this);
    $actvFormOptions = [
        'id' => 'poliza-modal-form',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons'],
    ];
}
?>
    <div class="poliza-form">

        <?php $form = ActiveForm::begin($actvFormOptions); ?>

        <?php
        try {
            if ($forModal == 'true')
                echo $form->field($model, 'poliza_id_selector')->widget(Select2::classname(), [
                    'options' => ['placeholder' => 'Seleccione una póliza ...'],
                    'initValueText' => $model->poliza_id_selector != null ?
                        $model->id . ' - ' . $model->nro_poliza : '',
                    'pluginOptions' => [
                        'allowClear' => true,
//                        'data' => Poliza::getPolizasLibres(true),
                        'ajax' => [
                            'url' => Url::to(['poliza/get-polizas-libres-ajax', 'compra_id' => $compra_id]),
                            'dataType' => 'json',
                            'data' => new JsExpression("
                            function(params) { 
                                return {
                                    q:params.term, 
                                    id: '',
                                };
                            }
                        "),
                        ]
                    ],
                ])->label('Póliza');

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 3,
                'attributes' => [
                    'nro_poliza' => [
                        'type' => Form::INPUT_TEXT,
                        'label' => 'Número',
                    ],
                    'fecha_desde' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => DateControl::class,
                        'options' => array(
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'language' => 'es',
                            'widgetOptions' => array(
                                'pluginOptions' => array(
                                    'autoclose' => true,
                                    'format' => 'dd-MM-yyyy',
                                    'todayHighlight' => true,
                                    'weekStart' => '0',
                                ),
                            ),

                        )
                    ],
                    'fecha_hasta' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => DateControl::class,
                        'options' => array(
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'language' => 'es',
                            'widgetOptions' => array(
                                'pluginOptions' => array(
                                    'autoclose' => true,
                                    'format' => 'dd-MM-yyyy',
                                    'todayHighlight' => true,
                                    'weekStart' => '0',
                                ),
                            ),

                        )
                    ],
//                      // Este campo no es requerido mostrar en la vista porque es calculado, a partir de la fehca desde y hasta.
//                    'importe_diario' => [
//                        'type' => Form::INPUT_WIDGET,
//                        'widgetClass' => MaskedInput::className(),
//                        'options' => [
//                            'clientOptions' => [
//                                'rightAlign' => true,
//                                'alias' => 'decimal',
////                                'alias' => 'integer',
//                                'groupSeparator' => '.',
//                                'radixPoint' => ',',
//                                'autoGroup' => true,
//                                'prefix' => "",
//                            ],
//                        ],
//                    ],
                    'forma_devengamiento' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'pluginOptions' => [
                                'data' => Poliza::getFormaDevengamientoLista(true),
                            ],
                            'options' => ['placeholder' => 'Seleccione una opción...'],
                            'initValueText' => Poliza::selectFormaDevengamiento($model->forma_devengamiento, true),
                        ]
                    ],
                    'seccion_id' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'pluginOptions' => [
                                'data' => PolizaSeccion::getSecciones(true),
                            ],
                            'options' => ['placeholder' => 'Seleccione una opción...'],
                            'initValueText' => PolizaSeccion::selectSeccion($model->seccion_id, true),
                        ]
                    ],
                ],
            ]);
        } catch (Exception $exception) {
            throw  $exception;
        }

        $boton_guardar_class = "";
        if (Yii::$app->controller->action->id == "create") {
            $boton_guardar_class = 'btn btn-success';
        } else {
            $boton_guardar_class = 'btn btn-primary';
        }

        echo Html::beginTag('div', ['class' => 'form-group']);
        $submitBtnOptions = ['data' => ['disabled-text' => 'Guardando...'], 'class' => $boton_guardar_class];
        echo Html::submitButton('Guardar', $forModal ? $submitBtnOptions : ['class' => $boton_guardar_class]);
        echo Html::endTag('div');
        ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$action_id = Yii::$app->controller->action->id;
$url_manejarPolizaDesdeCompra = Json::htmlEncode(\Yii::t('app', Url::to(['poliza/manejar-poliza-desde-compra'])));
$script = <<<JS
if (("$action_id") === 'create') {
    document.getElementById('poliza-nro_poliza').focus();
}

// obtener la id del formulario y establecer el manejador de eventos
$("#poliza-modal-form").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$('#poliza-poliza_id_selector').on('change', function() {
    let poliza_id = $(this).val();
    console.log('poliza_id: ' + poliza_id);
    let url = $url_manejarPolizaDesdeCompra + '&compra_id=' + "$compra_id" + '&formodal=' + true + "&poliza_id=" + poliza_id;
    if (poliza_id === "") {
        url = url.concat('&quiere_crear_nuevo=' + "si");
    }
    $.ajax({
        url: url,
        type: 'get',
        data: {},
        success: function (data) {
            $('.modal-body', modal).html("");
            $('.modal-body', modal).html(data);
            $('#modal').trigger('change');
        }
    })
});

$(document).ready(function() {
    // let forModal = "$forModal";
    // if (forModal === 'true') {
    //     $('#poliza-importe_diario').parent().parent()[0].remove()
    // }
});
JS;
$this->registerJs($script);
