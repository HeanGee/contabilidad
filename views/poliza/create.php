<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Poliza */
/* @var $compra_id int */

$this->title = 'Crear Nueva Póliza';
$this->params['breadcrumbs'][] = ['label' => 'Pólizas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poliza-create">

    <?= $this->render('_form', [
        'model' => $model,
        'compra_id' => $compra_id,
    ]) ?>

</div>
