<?php

use backend\modules\contabilidad\models\Poliza;
use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\PolizaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Polizas';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="poliza-index">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div>
            <?php
            /**
             * Mini formulario que postea un model de TempModel con un unico atributo $fecha_hasta, a la action actionReporte().
             *
             * Cuando entra al index, se instancia un nuevo TempModel y establece su atributo $fecha_hasta igual al
             * fecha_hasta de la clave PolizaSearch que va en el $_GET (si existe).
             *
             * El campo del TempModel, $fecha_hasta, se establece cuando uno cambia el filtro Fecha Hasta del index.
             *
             * El botón "Reporte" es en realidad el botón de submit del miniformulario, el cual envía los datos a través del $_POST.
             * Desde actionReporte(), se instancia otro nuevo TempModel y hace un ->load(...) con los datos del $_POST.
             * Así, el TempModel instanciado desde actionReporte() tendrá establecido su $fecha_hasta dependiendo de si
             * el usuario haya seteado algun valor en el filtro Fecha Hasta.
             *
             */
            $form = ActiveForm::begin([
                'id' => 'login-form-horizontal',
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
                'action' => 'index.php?r=contabilidad%2Fpoliza/reporte',
            ]);
            ?>

            <?= Html::activeHiddenInput($model, "fecha_hasta"); ?>
            <?= ""//$form->field($model, 'fecha_hasta')->input(Form::INPUT_TEXTAREA, ['style' => "display: none;"])->label(false)    ?>
            <div class="text-right btn-toolbar">
                <?php $permisos = [
                    'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-create'),
                    'recalc' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-recalcular-devengamientos'),
                    'reporte' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-reporte'),
                    'index-secc' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-seccion-index'),
                ]; ?>
                <?= $permisos['create'] ? Html::a('Registrar nueva Póliza', ['create'], ['id' => 'btn_poliza_new', 'class' => 'btn btn-success']) : null ?>
                <?= $permisos['recalc'] ? Html::a('Recalcular', ['recalcular-devengamientos'], ['id' => 'btn_poliza_new', 'class' => 'btn btn-warning pull-right']) : null ?>
                <?= $permisos['reporte'] ? Html::submitButton('Devengar Todos', ['id' => 'btn_submit', 'class' => 'btn btn-success pull-right']) : null ?>
                <?= $permisos['index-secc'] ? Html::a('Ir a Secciones', ['poliza-seccion/index'/*, 'operacion' => Yii::$app->request->getQueryParam('operacion')*/], ['class' => 'btn btn-info pull-right']) : null ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <br/>

        <?php try {
            $template = '';
            foreach (['view', 'update', 'delete', 'calcular'] as $_v)
                if (PermisosHelpers::getAcceso("contabilidad-poliza-{$_v}"))
                    $template .= "&nbsp&nbsp&nbsp{{$_v}}";

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'hover' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'nro_poliza',
                    [
                        'attribute' => 'fecha_desde',
                        'format' => ['date', 'php:d-m-Y'],
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'fecha_desde',
                            'language' => 'es',
                            'pickerButton' => false,
//                            'options' => [
//                                'style' => 'width:90px',
//                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-mm-yyyy',
                                'todayHighlight' => true,
                            ]
                        ])
                    ],
                    [
                        'attribute' => 'fecha_hasta',
                        'format' => ['date', 'php:d-m-Y'],
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'fecha_hasta',
                            'language' => 'es',
                            'pickerButton' => false,
//                            'options' => [
//                                'style' => 'width:90px',
//                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-mm-yyyy',
                                'todayHighlight' => true,
                            ]
                        ])
                    ],

                    [
                        'attribute' => 'importe_diario',
                        'value' => 'importeDiarioFormatted',
                        'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'update' => function ($url, $model, $index) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                    $url,
                                    [
                                        'class' => 'btn-edit-poliza',
                                        'id' => 'btn-edit-poliza' . $index,
                                        'title' => Yii::t('app', 'Editar'),
                                    ]
                                );
                            },
                            'view' => function ($url, $model, $index) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-eye-open"></span>',
                                    $url,
                                    [
                                        'class' => 'btn-view-poliza',
                                        'id' => 'btn-view-poliza' . $index,
                                        'title' => Yii::t('app', 'Ver'),
                                    ]
                                );
                            },
                            'delete' => function ($url, $model, $index) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-trash"></span>',
                                    $url,
                                    [
                                        'class' => 'btn-delete-poliza',
                                        'data' => [
                                            'method' => 'post',
                                            'confirm' => 'Está seguro de eliminar la póliza ' . $model->nro_poliza . ' ?',
                                        ],
                                        'id' => 'btn-delete-poliza' . $index,
                                        'title' => Yii::t('app', 'Borrar'),
                                    ]
                                );
                            },
                            'calcular' => function ($url, $model, $index) {
                                if ($model instanceof Poliza && isset($model->factura_compra_id))
                                    return Html::a(
                                        '<span class="btn btn-success btn-xs">Devengar</span>',
                                        $url,
                                        [
                                            'class' => 'btn-calc-poliza',
                                            'id' => 'btn-calc-poliza' . $model->id,
                                            'title' => Yii::t('app', 'Excel'),
                                        ]
                                    );
                                return "";
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'update') {
                                // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                                $url = Url::to(['poliza/update', 'id' => $model->id]);
                                return $url;
                            }
                            if ($action === 'view') {
                                // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                                $url = Url::to(['poliza/view', 'id' => $model->id]);
                                return $url;
                            }
                            if ($action === 'delete') {
                                $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
                                $url = Url::to(['poliza/delete', 'id' => $model->id]);
                                return $url;
                            }
                            if ($action === 'calcular') {
                                $url = Url::to(['poliza/calcular', 'id' => $model->id]);
                                return $url;
                            }
                            return '';
                        },
                        'template' => $template,
                    ],
                ],
            ]);
        } catch (Exception $exception) {
            throw new $exception;
        }

        $scripts = <<<JS

JS;
        $this->registerJs($scripts);
        ?>
    </div>
    <!---->
<?php
//$CSS = <<<CSS
//.kv-grid-table tbody tr:hover {
//    background-color: #ebffdd;
//}
//CSS;
//$this->registerCss($CSS);
//?>