<?php

use kartik\form\ActiveForm;
use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \backend\modules\contabilidad\models\auxiliar\ArandukaFamiliares */
?>

<td width="26%; !important" style="font-weight: normal;">
    <?= $form->field($model, "[{$key}]tipo_familia")->textInput([])->label(false) ?>
</td>

<td width="9%; !important" style="font-weight: normal;">
    <?= $form->field($model, "[{$key}]nro_identificacion")->textInput([])->label(false) ?>
</td>

<td>
    <?= $form->field($model, "[{$key}]nombre_completo")->textInput([])->label(false) ?>
</td>

<td>
    <?= $form->field($model, "[{$key}]fecha_nacimiento")->textInput([])->label(false) ?>
</td>

<td>
    <?= $form->field($model, "[{$key}]regimen_matrimonial")->textInput([])->label(false) ?>
</td>

<td class=" " style="text-align: center">
    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'delete-familiares btn btn-danger btn-sm',
        'id' => "[{$key}]delete_button",
        'title' => 'Eliminar fila',
    ]) ?>
</td>