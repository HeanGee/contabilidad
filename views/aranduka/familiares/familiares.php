<?php

use backend\modules\contabilidad\models\auxiliar\Aranduka;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */

$action = Yii::$app->controller->action->id;
?>

<div class="familiares">

    <fieldset>
        <legend>&nbsp;&nbsp;Agregar Familiares
            <div class="pull-left">
                <?php
                // new detalle button
                echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                    'id' => 'new-familiares',
                    'class' => 'pull-left btn btn-success'
                ]);
                ?>
            </div>
        </legend>
    </fieldset>

    <?php

    echo '<table id="tabla-familiares" class="table table-condensed table-responsive">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Tipo de Familiar</th>';
    echo '<th style="text-align: center;">Nro de Identificación</th>';
    echo '<th style="text-align: center;">Nombre Completo</th>';
    echo '<th style="text-align: center;">Fecha de Nacimiento</th>';
    echo '<th style="text-align: center;">Régimen Matrimonial</th>';
    echo '<th style="text-align: center;">Acciones</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    \kartik\select2\Select2Asset::register($this);
    \yii\widgets\MaskedInputAsset::register($this);

    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);

    $tipos_familiar = [['id' => '', 'text' => '']];
    $tipos_familiar[] = ['id' => Aranduka::CONYUGE, 'text' => "Cónyuge"];
    $tipos_familiar[] = ['id' => Aranduka::HIJOS, 'text' => "Hijos"];
    $tipos_familiar[] = ['id' => Aranduka::PADRES, 'text' => "Padres"];
    $tipos_familiar[] = ['id' => Aranduka::HERMANOS, 'text' => "Hermanos"];
    $tipos_familiar[] = ['id' => Aranduka::ABUELOS, 'text' => "Abuelos"];
    $tipos_familiar[] = ['id' => Aranduka::SUEGROS, 'text' => "Suegros"];
    $tipos_familiar[] = ['id' => Aranduka::OTRAS_PERSONAS, 'text' => "Otras Personas respecto a las cuales exista la obligación legal de prestar alimentos"];
    $tipos_familiar = Json::encode($tipos_familiar);

    $regimenes_matrimonial = [['id' => '', 'text' => '']];
    $regimenes_matrimonial[] = ['id' => Aranduka::RM_ADM_CONJUNTA, 'text' => 'Comunidad de gananciales bajo administración conjunta'];
    $regimenes_matrimonial[] = ['id' => Aranduka::RM_PART_DIFERIDA, 'text' => 'Regimen de participación diferida'];
    $regimenes_matrimonial[] = ['id' => Aranduka::RM_SEPARAC_BIENES, 'text' => 'Regimen de separación de bienes'];
    $regimenes_matrimonial[] = ['id' => Aranduka::RM_UNION_DE_HECHO, 'text' => 'Unión de hecho'];
    $regimenes_matrimonial[] = ['id' => Aranduka::OTRAS_PERSONAS, 'text' => 'Otras Personas respecto a las cuales exista la obligación legal de prestar alimentos'];
    $regimenes_matrimonial = Json::encode($regimenes_matrimonial);

    $html = $this->render('familiares_field', [
        'key' => '__new__',
        'form' => $form,
        'model' => new \backend\modules\contabilidad\models\auxiliar\ArandukaFamiliares(),
    ]);
    $bluePrint = \backend\helpers\HtmlHelpers::trimmedHtml($html);

    $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
    ?>

    <?php

    $script_head = <<<JS
var action_id = "$action";
var detalle_k_datos_consumo = 1;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);
    $script = <<<JS
function construirSelect2(selector, data) {
    // console.log('construir select2:', selector);
    $(selector)
        .each(function () {
            let e = $(this);

            if (!$(e).data('select2')) {
                $(e).select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: data,
                });
            }
        });
}

function construirMaskedInput(selector) {
    // console.log('aplicar maskedinput:', selector);
    $(selector).inputmask({
        alias: "date", inputFormat: "dd-mm-yyyy"
    });
}

function showFamiliaresPanel() {
    if ($('select[id$="agregar_familiares"]').val() === 'no') {
        $('.delete-familiares').click();
        $('.familiares').prop('hidden', true);
    } else {
        $('.familiares').prop('hidden', false);
        $('#new-familiares').click();
        $('[id$="tipo_familia"]').select2('open');
    }
}

$('#new-familiares').on('click', function () {
    detalle_k_datos_consumo += 1;
    $('#tabla-familiares').find('tbody')
        .append('<tr class="fila-detalle">' + ('$bluePrint').replace(/__new__/g, + detalle_k_datos_consumo) + '</tr>');

    construirSelect2($('input[id$="tipo_familia"]'), $tipos_familiar);
    construirSelect2($('input[id$="regimen_matrimonial"]'), $regimenes_matrimonial);
    construirMaskedInput($('input[id$="fecha_nacimiento"]'));
});

$(document).on('click', '.delete-familiares', function () {
    $(this).closest('tbody tr').remove();
});

$(document).on('change', 'select[id$="agregar_familiares"]', function() {
    showFamiliaresPanel();
});

$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        let selector = $(this).siblings('select');
        if (!selector.prop('disabled'))
            $(selector).select2('open');
    }
});

$(document).ready(function () {
    showFamiliaresPanel();
});
JS;
    $this->registerJs($script);
    ?>

</div>
