<?php
/* @var $this yii\web\View */

use backend\helpers\HtmlHelpers;
use common\helpers\FlashMessageHelpsers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\MaskedInput;

/* @var $model \backend\modules\contabilidad\models\auxiliar\Archivo */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Aranduka');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="aranduka-parse-to-json">
    <?php $form = ActiveForm::begin(['id' => 'aranduka-form', 'options' => ['enctype' => 'multipart/form-data']]) ?>

    <?php
    try {
        $submit = Html::submitButton('Enviar', ['class' => 'btn btn-primary']);
        echo '<legend><small>Cargar desde Excel</small></legend>';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'attributes' => [
                        'imageFiles' => [
                            'type' => Form::INPUT_FILE,
                            'value' => $form->field($model, 'imageFiles[]')->widget(FileInput::classname(),
                                [
                                    'language' => 'es',
                                    'pluginOptions' => ['showPreview' => false],
                                    'options' => ['multiple' => true],
                                ]
                            ),
                            'label' => "Excel de Ingresos",
                        ],
                        'imageFiles2' => [
                            'type' => Form::INPUT_FILE,
                            'value' => $form->field($model, 'imageFiles2[]')->widget(FileInput::classname(),
                                [
                                    'language' => 'es',
                                    'pluginOptions' => ['showPreview' => false],
                                    'options' => ['multiple' => true],
                                ]
                            ),
                            'label' => "Excel de Egresos",
                        ],
                    ],
                ],
                [
                    'attributes' => [

                        'ruc' => [
                            'type' => Form::INPUT_TEXT,
                        ],
                        'periodo' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => ['alias' => '9999'],
                            ],
                        ],
                        'agregar_familiares' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'options' => [
                                    'placeholder' => 'Por favor Seleccione Uno',
                                ],
                                'data' => ['si' => "Si", 'no' => "No"],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                            ],
                        ],
                    ]
                ],
            ]
        ]);

        echo $this->render('familiares/familiares', ['form' => $form]);

        echo "<div class='form-group'>{$submit}</div>";
    } catch (Exception $exception) {
        echo $exception;
        FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    }
    ?>

    <?php ActiveForm::end() ?>

    <?php
    if (Yii::$app->session->has('aranduka_errors')) {
        echo '<div class="errors">';
        echo '<legend class="text-danger"><small>Errores en el excel</small></legend>';
        echo '<ol>';
        foreach (Yii::$app->session->get('aranduka_errors') as $error) {
            echo "<li style='color: red'>$error.</li>";
        }
        echo '</ol>';
        echo '</div>';
    }
    ?>

    <?php
    // Mostrar link a archivo de ejemplo
    $directorio = '../modules/contabilidad/views/aranduka/interesting-files';
    $ficheros = array_values(scandir($directorio));
    unset($ficheros[0]);
    unset($ficheros[1]);
    // Fuente: https://stackoverflow.com/questions/4535846/php-adding-prefix-strings-to-array-values
    array_walk($ficheros, function (&$value, $key) {
        $value = "../modules/contabilidad/views/aranduka/interesting-files/{$value}";
    });
    echo HtmlHelpers::MultiLinksToFiles("Archivos de Interés", array_values($ficheros), 'text');
    ?>
</div>
