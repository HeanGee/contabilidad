<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoFijoTipo */

$this->title = 'Crear Nuevo Tipo';
$this->params['breadcrumbs'][] = ['label' => 'Tipos de Activo Fijo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-fijo-tipo-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
