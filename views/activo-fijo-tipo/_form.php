<?php

use backend\modules\contabilidad\models\ActivoFijoTipo;
use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoFijoTipo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activo-fijo-tipo-form">

    <?php
    $form = ActiveForm::begin();
    try {
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'attributes' => [
                        'nombre' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder ' => 'Nombre...']
                        ]
                    ]
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'cuenta_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '3'],
                            'value' => $form->field($model, 'cuenta_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => PlanCuenta::getCuentaLista(true)
                                ],
                                'initValueText' => !empty($model->cuenta) ? $model->cuenta->nombre : ''
                            ])
                        ],
                        'cuenta_depreciacion_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '3'],
                            'value' => $form->field($model, 'cuenta_depreciacion_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => PlanCuenta::getCuentaLista(true)
                                ],
                                'initValueText' => !empty($model->cuentaDepreciacion) ? $model->cuentaDepreciacion->nombre : ''
                            ])->label('Cuenta para Depreciación'),
                        ],
                        'vida_util' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '2'],
                            'options' => ['placeholder ' => 'Vida Útil...']
                        ],
                        'estado' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'estado')->widget(Select2::className(), [
                                'data' => ActivoFijoTipo::getValoresEnum('estado'),
                                'options' => ['placeholder' => 'Seleccione...'],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                                'initValueText' => !empty($model->estado) ? ucfirst($model->estado) : ''
                            ])
                        ],
                        'cantidad_requerida' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'cantidad_requerida')->widget(Select2::className(), [
                                'data' => ActivoFijoTipo::getValoresEnum('cantidad_requerida'),
                                'options' => ['placeholder' => 'Seleccione...'],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                                'initValueText' => !empty($model->cantidad_requerida) ? ucfirst($model->cantidad_requerida) : ''
                            ])
                        ],
                    ]
                ]
            ]
        ]);

        // Atributos plantilla dinamicos
        $detalles = Yii::$app->session->get('cont_afijotipo_atributos');
        echo $this->render('atributos/_form', ['form' => $form, 'model' => $model, 'detalles' => $detalles]);

    } catch (\Exception $e) {
        echo $e;
        FlashMessageHelpsers::createWarningMessage($e->getMessage());
    }
    ?>

    <div class="form-group">
        <?= PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-create') ? Html::submitButton($model->isNewRecord ? 'Crear' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$css = <<<CSS
table.tabla-parametros th, td {
    /*border: 1px solid black;*/
    overflow: hidden;
    width: 30px;
}
CSS;
$this->registerCss($css);

$script = <<<JS
function construirSelect2(selector, data, disabled = false) {
    // console.log('construir select2:', selector);
    $(selector)
        .each(function () {
            let e = $(this);

            if (!$(e).data('select2')) {
                $(e).select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: data,
                    disabled: disabled,
                });
            }
        });
}

$(document).ready(function () {
    $('#activofijotipo-nombre').focus();
});
JS;
$this->registerJs($script);
?>
