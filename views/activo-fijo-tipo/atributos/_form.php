<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 21/01/19
 * Time: 02:33 PM
 */

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\ActivoFijoAtributo;
use backend\modules\contabilidad\models\ActivoFijoTipo;
use backend\modules\contabilidad\models\ActivoFijoTipoAtributo;
use kartik\form\ActiveForm;
use kartik\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $detalles ActivoFijoTipoAtributo[] */
/* @var $form ActiveForm */
/* @var $model ActivoFijoTipo */
?>

<?php
(isset($detalles) && !empty($detalles)) || $detalles = [];
?>


<div class="activo-fijo-tipo-atributo">

    <fieldset>
        <legend class="text-info">
            <small>&nbsp;&nbsp;Agregar Atributos
                <div class="pull-left">
                    <?php
                    // new detalle button
                    echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                        'id' => 'add-new-atributo',
                        'class' => 'pull-left btn btn-success'
                    ]);
                    ?>
                </div>
            </small>
        </legend>
    </fieldset>

    <?php

    echo '<table id="tabla-atributos" class="table table-condensed table-responsive">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Nombre</th>';
    echo '<th style="text-align: center;">Obligatorio?</th>';
    echo '<th style="text-align: center;">Acciones</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    \kartik\select2\Select2Asset::register($this);
    \yii\widgets\MaskedInputAsset::register($this);

    // existing detalles fields
    $skey = '';
    $key = 0;
    foreach ($detalles as $index => $detalle) {
        $query = ActivoFijoAtributo::find()->where([
            'atributo' => $detalle->atributo
        ]);

        $key = $detalle->id != null ? $detalle->id - 1 : $index;
        echo '<tr class="fila-detalle">';
        echo $this->render('_detalle', [
            'key' => 'new-atributo' . ($key + 1),
            'form' => $form,
            'model' => $detalle,
            'activate_delete' => !$query->exists(), // habilitar boton delete si el atributo no fue usado aun en afijos.
        ]);
        echo '</tr>';
        $key++;
    }

    $modelTipoAtributo = new ActivoFijoTipoAtributo();
    $html = $this->render('_detalle', [
        'key' => 'new_atributo',
        'form' => $form,
        'model' => $modelTipoAtributo,
        'activate_delete' => true,
    ]);

    $bluePrint = HtmlHelpers::trimmedHtml($html);

    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);

    $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
    ?>

    <?php
    $select2Data = Json::encode([
        0 => ['id' => '', 'text' => ''],
        1 => ['id' => 'si', 'text' => "Sí"],
        2 => ['id' => 'no', 'text' => "No"]
    ]);
    $script_head = <<<JS
var detalle_k = $key;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);

    $script = <<<JS
$('#add-new-atributo').on('click', function () {
    detalle_k += 1;
    $('#tabla-atributos').find('tbody')
        .append('<tr class="fila-detalle">' + '$bluePrint'.replace(/new_atributo/g, 'new_' + detalle_k) + '</tr>');

    data = $select2Data;
    construirSelect2($('#activofijotipoatributo-new_' + detalle_k + '-obligatorio'), data);
    delete data;
});

$(document).on('click', '.delete-atributo', function () {
    $(this).closest('tbody tr').remove();
});

$(document).ready(function () {
    data = $select2Data;
    $('input[id$="obligatorio"]').each(function(i, e) {
        construirSelect2(e, data);
    });
})
JS;
    $this->registerJs($script);
    ?>

</div>
