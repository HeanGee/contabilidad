<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 21/01/19
 * Time: 02:36 PM
 */

use backend\modules\contabilidad\models\ActivoFijoTipo;
use kartik\form\ActiveForm;
use kartik\helpers\Html;

/* @var $model ActivoFijoTipo */
/* @var $form ActiveForm */
/* @var $key string */
/* @var $activate_delete boolean */
?>

<td>
    <?= $form->field($model, "[$key]atributo")->textInput([])->label(false) ?>
</td>

<td>
    <?= $form->field($model, "[{$key}]obligatorio")->textInput([])->label(false) ?>
</td>

<td class=" " style="text-align: center">
    <?= $activate_delete ? Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'delete-atributo btn btn-danger btn-sm',
        'name' => "[{$key}]-delete_button",
        'title' => 'Eliminar atributo',
    ]) : null ?>
</td>
