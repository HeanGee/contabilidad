<?php

use backend\modules\contabilidad\models\ActivoFijoTipo;
use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ActivoFijoTipoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tipos de Activo Fijo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-fijo-tipo-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
    <div class="btn-toolbar">
        <?php $permisos = [
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-create'),
            'index-actbiol' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-index'),
        ]; ?>
        <?= $permisos['create'] ? Html::a('Crear Nuevo Tipo', ['create'], ['class' => 'btn btn-success']) : null ?>
        <?= $permisos['index-actbiol'] ? Html::a('Ir a A. Fijos', ['activo-fijo/index'], ['class' => 'btn btn-info pull-right']) : null ?>
    </div>
    </p>

    <?php
    try {
        $template = '';
        $template = $template . (PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-view') ? '{view} &nbsp&nbsp&nbsp' : '');
        $template = $template . (PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-update') ? '{update} &nbsp&nbsp&nbsp' : '');
        $template = $template . (PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //                    'id',
                [
                    'attribute' => 'id',
                    'value' => 'id',
                    'label' => 'Código',
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'width' => '60px',
                ],
                'nombre',
                'vida_util',
                [
                    'attribute' => 'tmp_cuenta',
                    'value' => function ($model) {
                        return !empty($model->cuenta_id) ? ($model->cuenta->cod_completo . ' - ' . $model->cuenta->nombre) : '';
                    },
                ],
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => ['' => 'Todos'] + ActivoFijoTipo::getValoresEnum('estado'),
                        'hideSearch' => true
                    ])
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template
                ],
            ],
        ]);

    } catch (\Exception $e) {
        print $e;
    }
    ?>
</div>
