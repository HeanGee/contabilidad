<?php

use backend\modules\contabilidad\models\ActivoFijoTipoAtributo;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoFijoTipo */

$this->title = "Tipo de Activo Fijo: {$model->nombre}";
$this->params['breadcrumbs'][] = ['label' => 'Activo Fijo Tipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-fijo-tipo-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-activo-fijo-tipo-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Act. Fijo Tipos', ['index'/*, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de querer eliminar?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'columns' => [
                        [
                            'attribute' => 'nombre',
                            'value' => $model->nombre,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'vida_util',
                            'value' => $model->vida_util,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'cuenta_id',
                            'value' => !empty($model->cuenta_id) ? ($model->cuenta->cod_completo . ' - ' . $model->cuenta->nombre) : '',
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'estado',
                            'format' => 'raw',
                            'value' => '<span class="label label-' . (strtolower($model->estado) == 'activo' ? 'success">A' : 'danger">Ina') . 'ctivo</span>',
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
            ],
        ]);

        // Atributos asociados
        $dataProvider = new ActiveDataProvider([
            'query' => ActivoFijoTipoAtributo::find()->where(['activo_fijo_tipo_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => 'Atributos',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'columns' => [
                [
                    'label' => 'Nombre',
                    'value' => 'atributo'
                ],
                [
                    'label' => 'Es Obligatorio?',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->obligatorio == 'si' ? '<label class="label label-success">Sí</label>' : '<label class="label label-danger">No</label>';
                    }
                ],
            ],
        ]);

    } catch (\Exception $e) {

    } ?>

</div>
