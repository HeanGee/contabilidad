<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoFijoTipo */

$this->title = 'Modificar Tipo de Activo';
$this->params['breadcrumbs'][] = ['label' => 'Activo Fijo Tipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="activo-fijo-tipo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
