<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 16/08/2018
 * Time: 11:05
 */

use backend\modules\contabilidad\controllers\MesSelectorModel;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/** @var $model MesSelectorModel */

?>

<?php
ActiveFormDisableSubmitButtonsAsset::register($this);

$form = ActiveForm::begin([
    'id' => 'modal_mes_selector',
    'options' => ['class' => 'disable-submit-button'],
]);

try {
    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'codigo_set' => [
                'type' => Form::INPUT_TEXT,
            ],
        ],
    ]);
} catch (Exception $exception) {
    echo $exception->getMessage();
}
?>

    <div class="form-group">
        <?= Html::submitButton('Generar', ['data' => ['disabled-text' => 'Generando...'], 'class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$script = <<<JS
$("#activo_fijo_selector_form").on("beforeSubmit", function (e) {
    let form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});
JS;

//$this->registerJs($script);
?>