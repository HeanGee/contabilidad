<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 30/08/2018
 * Time: 12:06
 */

use backend\modules\contabilidad\models\PresentacionIva;
use common\helpers\PermisosHelpers;
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $model PresentacionIva */
/* @var $cabecera array */
/* @var $detalle array */
/* @var $tipo_declaracion string */

$this->title = "Declaración de {$model->monthYearToString()}.";
$this->params['breadcrumbs'][] = ['label' => 'Presentacion de I.V.A.', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$cabecera = Json::encode($cabecera);
$detalle = Json::encode($detalle);
//$source = $tipo_declaracion == PresentacionIva::$_ORIGINAL ? "../modules/contabilidad/views/presentacion-iva/marangatu_form.html" :
$source = "../modules/contabilidad/views/presentacion-iva/marangatu_form.html";
?>

<?php
echo '<div class="btn-toolbar">';
echo PermisosHelpers::getAcceso('contabilidad-presentacion-iva-index') ?
    Html::a("Regresar a Presentacion de I.V.A.", ['index'], ['class' => 'btn btn-info']) : null;
echo (PermisosHelpers::getAcceso('contabilidad-presentacion-iva-update') && $model->isUpdateable()) ?
    Html::a("Modificar", ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null;
echo '</div>';
echo Html::tag('br/');
?>
    <div class="panel panel-info">
        <div class="panel-heading">Visualización de los valores utilizando formulario de MARANGATÚ</div>
        <div class="panel-body">
            <iframe id="marangatu_iframe" src="<?= $source ?>"
                    style="width: 100%; height: 600px; background: #FFFFFF;"
                    frameBorder={0}
                    allowtransparency="true"></iframe>
        </div>
    </div>
<?php
$SCRIPTS = <<<JS
$(document).ready(function () {

});

$('#marangatu_iframe').on('load', function () {
    let iframe = $(this);

    // Limpiar cabecera
    iframe.contents().find('#numeroDocumento, #dv, #primerApellido, #nombre').val('').trigger('change');
    iframe.contents().find('#C1, #C2, #C5').prop('checked', false);

    // Limpiar campos de numeros.
    iframe.contents().find('input[id^="C"]').each(function () {
        $(this).val('');
    });

    // Completar campos cabecera
    iframe.contents().find('h4.ng-scope')[0].innerHTML = "DECLARACIÓN " + ($cabecera)['declaracion'];
    iframe.contents().find('#precab-f2-form')[0].innerHTML = ($cabecera)['nombre_formulario'];
    iframe.contents().find('#precab-f2-contrib')[0].innerHTML = ($cabecera)['contribuyente'];
    iframe.contents().find('#precab-f2-nro-control')[0].innerHTML = ($cabecera)['control'] + '-por definirse-';
    iframe.contents().find('#precab-f2-fecha')[0].innerHTML = ($cabecera)['timestamp'];
    iframe.contents().find('#precab-f2-presentado')[0].innerHTML = ($cabecera)['presentado_por'];
    iframe.contents().find('#primerApellido').val(($cabecera)['primer_apellido']).trigger('change');
    iframe.contents().find('#segundoApellido').val(($cabecera)['segundo_apellido']).trigger('change');
    iframe.contents().find('#nombre').val(($cabecera)['razon_social']).trigger('change');
    iframe.contents().find('#dv').val(($cabecera)['dv']).trigger('change');
    iframe.contents().find('#ruc').val(($cabecera)['ruc']).trigger('change');
    iframe.contents().find('#C1').prop('checked', ($cabecera)['C1']);
    iframe.contents().find('#C2').prop('checked', ($cabecera)['C2']);
    iframe.contents().find('#C5').prop('checked', ($cabecera)['C5']);
    $.each(iframe.contents().find('.row-periodo-fiscal').find('td'), function (index, element) {
        element.innerHTML = ($cabecera)['periodo_fiscal'][index];
    });

    // Completar campos de numeros.
    let query = $detalle;
    query.forEach(function (e) {
        let val = e['monto'];
        console.log("rubro: ",e['rubro'],' casilla: ', e['casilla'], ' valor: ', val);
        iframe.contents().find('input[id="C' + e['casilla'] + '"]').val(val);
    });
    
    // iframe.contents().find('input[id^="C"]').prop('readonly', false);
});
JS;
$this->registerJs($SCRIPTS);
?>