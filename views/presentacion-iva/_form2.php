<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 30/08/2018
 * Time: 12:06
 */

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\PresentacionIva;
use backend\modules\contabilidad\models\PresentacionIvaRubro;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $this \yii\web\View */
/* @var $model PresentacionIva */
/* @var $cabecera array */
/* @var $detalle array */
/* @var $tipo_declaracion string */

$this->title = "Modificar Declaración de {$model->monthYearToString()}.";
$this->params['breadcrumbs'][] = ['label' => 'Presentacion de I.V.A.', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\yii\widgets\MaskedInputAsset::register($this);
?>

    <div class="form-group btn-toolbar">
        <?= Html::a("Regresar a Presentacion de I.V.A.", ['index'], ['class' => 'btn btn-info']) . "<br/>" ?>
    </div>

<?php
$form = ActiveForm::begin(['id' => 'presentacion-iva-form']);

$source = "../modules/contabilidad/views/presentacion-iva/marangatu_form.html";
$html = <<<HTML
<div class="panel panel-success">
    <div class="panel-heading">Formulario 120 MARANGATÚ</div>
    <div class="panel-body">
        <iframe id="marangatu_iframe" src="{$source}"
                style="width: 100%; height: 600px; background: #FFFFFF;"
                frameBorder={0}
                allowtransparency="true"></iframe>
    </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-success">Guardar</button>
</div>
HTML;
$html = HtmlHelpers::trimmedHtml($html);
echo $html;

ActiveForm::end();

try {
    $presentacion_periodo_anterior = PresentacionIvaRubro::find()->alias('rubro')
        ->leftJoin("cont_presentacion_iva iva", 'rubro.presentacion_iva_id = iva.id')
        ->where([
            'iva.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'iva.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
            'tipo_declaracion' => $model->tipo_declaracion,
            'tipo_formulario' => $model->tipo_formulario,
            'periodo_fiscal' => date_create_from_format('m-Y', $model->periodo_fiscal)->sub(new DateInterval('P1M'))->format('m-Y'),
        ])->asArray()->all();
} catch (Exception $e) {
    $presentacion_periodo_anterior = [];
}

$cabecera = Json::encode($cabecera);
$detalle = Json::encode($detalle);
$detalle_periodo_anterior = Json::encode($presentacion_periodo_anterior);
$script = <<<JS
var detalle_pa = $detalle_periodo_anterior;
JS;
$this->registerJs($script, \yii\web\View::POS_HEAD);

$SCRIPTS = <<<JS
function construirMaskedInput(selector, number_type = 'integer', integer_digits = 17) {
    $(selector).inputmask({
        "alias": "numeric",
        "digits": (number_type === 'integer') ? 0 : 2,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true,
        // allowMinus: false,
        integerDigits: integer_digits,
        fractionalDigits: (number_type === 'integer') ? 0 : 2,
        removeMaskOnSubmit: true,
    });
}

function getNumbersFrom(string) {
    try {
        return string.match(/\d+/g).map(Number);
    } catch (err) {
        return [0];
    }
}

function between(number, min, max) {
    return (min <= number) && (number <= max);
}

function getRubro(casilla) {
    if (casilla <= 24) return 1;
    if (casilla <= 31) return 2;
    if (casilla <= 43) return 3;
    if (casilla <= 50) return 4;
    if (casilla <= 58) return 5;
    if (casilla <= 66) return 6;
}

function getColumna(casilla) {
    let rangos_col_I = [];
    rangos_col_I.push({'de': 10, 'a': 18});
    rangos_col_I.push({'de': 32, 'a': 34});
    rangos_col_I.push({'de': 51, 'a': 54});
    rangos_col_I.push({'de': 59, 'a': 64});

    let rangos_col_II = [];
    rangos_col_II.push({'de': 19, 'a': 21});
    rangos_col_II.push({'de': 35, 'a': 37});
    rangos_col_II.push({'de': 55, 'a': 58});
    rangos_col_II.push({'de': 65, 'a': 66});

    let rangos_col_III = [];
    rangos_col_III.push({'de': 22, 'a': 24});
    rangos_col_III.push({'de': 38, 'a': 43});

    let columna = "";

    rangos_col_I.forEach(function (rango, i) {
        if (between(casilla, rango['de'], rango['a'])) {
            columna = 'I';
            return false;
        }
    });

    if (columna === "") {
        rangos_col_II.forEach(function (rango, i) {
            if (between(casilla, rango['de'], rango['a'])) {
                columna = 'II';
                return false;
            }
        });
    }

    if (columna === "") {
        rangos_col_III.forEach(function (rango, i) {
            if (between(casilla, rango['de'], rango['a'])) {
                columna = 'III';
                return false;
            }
        });
    }

    return columna;
}

function getInciso(casilla) {
    let incisos_a = [10, 22, 25, 32, 35, 38, 44, 55, 59, 65];
    let incisos_b = [11, 19, 26, 33, 36, 39, 45, 51, 60, 66];
    let incisos_c = [12, 27, 40, 46, 52, 61];
    let incisos_d = [13, 28, 41, 47, 56, 62];
    let incisos_e = [14, 29, 34, 37, 42, 48, 53, 57, 63];
    let incisos_f = [15, 23, 30, 43, 49, 54, 64];
    let incisos_g = [16, 20, 31, 50, 58];
    let incisos_h = [17];
    let incisos_i = [18, 21, 24];

    if (incisos_a.includes(casilla)) return 'a';
    if (incisos_b.includes(casilla)) return 'b';
    if (incisos_c.includes(casilla)) return 'c';
    if (incisos_d.includes(casilla)) return 'd';
    if (incisos_e.includes(casilla)) return 'e';
    if (incisos_f.includes(casilla)) return 'f';
    if (incisos_g.includes(casilla)) return 'g';
    if (incisos_h.includes(casilla)) return 'h';
    if (incisos_i.includes(casilla)) return 'i';
}

function convertToInteger(nro) {
    return parseInt((nro !== "") ? nro : 0);
}

$(document).on('submit', 'form#presentacion-iva-form', function () {
    let form = $(this);
    let iframe = $('#marangatu_iframe');
    let contadores = {1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1};

    form.find('input.personal[type="hidden"]').remove();

    iframe.contents().find('input[id^="C"]').each(function (i, e) {
        let id = $(e).prop('id');
        let monto = convertToInteger($(e).val());
        // let casilla = parseInt(getNumbersFrom(id)[0]); if (casilla < 10 || casilla > 66 || monto === 0 || monto === "") return true;
        let casilla = parseInt(getNumbersFrom(id)[0]);
        if (casilla < 10 || casilla > 66) return true;
        if (casilla < 10 || casilla > 66) return true;
        let rubro = getRubro(casilla);
        let columna = getColumna(casilla);
        let inciso = getInciso(casilla);
        var input;
        let contador = contadores[rubro];

        // console.log('rubro ', rubro, ', inciso ', inciso, ' columna ', columna, 'casilla ', casilla);
        // pir es abreviatura de las iniciales de PresentacionIvaRubro.

        input = $("<input>").attr({
            "type": "hidden",
            'class': 'personal',
            "name": "PresentacionIvaRubro[pir" + casilla + "][rubro]"
        }).val(rubro);
        form.append(input);

        input = $("<input>").attr({
            "type": "hidden",
            'class': 'personal',
            "name": "PresentacionIvaRubro[pir" + casilla + "][inciso]"
        }).val(inciso);
        form.append(input);

        input = $("<input>").attr({
            "type": "hidden",
            'class': 'personal',
            "name": "PresentacionIvaRubro[pir" + casilla + "][columna]"
        }).val(columna);
        form.append(input);

        input = $("<input>").attr({
            "type": "hidden",
            'class': 'personal',
            "name": "PresentacionIvaRubro[pir" + casilla + "][monto]"
        }).val(monto);
        form.append(input);

        input = $("<input>").attr({
            "type": "hidden",
            'class': 'personal',
            "name": "PresentacionIvaRubro[pir" + casilla + "][casilla]"
        }).val(casilla);
        form.append(input);

        contadores[rubro]++;
    });

    (form.serializeArray()).forEach(function (e, i) {
        // let regex = /\[rubro1\]/gm;
        // let m = regex.exec(i.name);
        // if (m !== null && m[1] !== null) 

        // console.log(e);
        // if (e.name.indexOf('PresentacionIvaRubro[pir13]') !== -1) console.log(e);
    });

    return true;
});

$('#marangatu_iframe').on('load', function () {
    let iframe = $(this);

    // Limpiar cabecera
    iframe.contents().find('#numeroDocumento, #dv, #primerApellido, #nombre').val('').trigger('change');
    iframe.contents().find('#C1, #C2, #C5').prop('checked', false);

    // Limpiar campos de numeros.
    iframe.contents().find('input[id^="C"]').each(function () {
        $(this).val('');
    });

    // Widget masked input
    selector = $('#marangatu_iframe').contents().find('input.valornumero[id^="C"]');
    construirMaskedInput(selector);

    // Completar campos cabecera
    iframe.contents().find('h4.ng-scope')[0].innerHTML = "DECLARACIÓN " + ($cabecera)['declaracion'];
    iframe.contents().find('#precab-f2-form')[0].innerHTML = ($cabecera)['nombre_formulario'];
    iframe.contents().find('#precab-f2-contrib')[0].innerHTML = ($cabecera)['contribuyente'];
    iframe.contents().find('#precab-f2-nro-control')[0].innerHTML = ($cabecera)['control'] + '-por definirse-';
    iframe.contents().find('#precab-f2-fecha')[0].innerHTML = ($cabecera)['timestamp'];
    iframe.contents().find('#precab-f2-presentado')[0].innerHTML = ($cabecera)['presentado_por'];
    iframe.contents().find('#primerApellido').val(($cabecera)['primer_apellido']).trigger('change');
    iframe.contents().find('#segundoApellido').val(($cabecera)['segundo_apellido']).trigger('change');
    iframe.contents().find('#nombre').val(($cabecera)['razon_social']).trigger('change');
    iframe.contents().find('#dv').val(($cabecera)['dv']).trigger('change');
    iframe.contents().find('#ruc').val(($cabecera)['ruc']).trigger('change');
    iframe.contents().find('#C1').prop('checked', ($cabecera)['C1']);
    iframe.contents().find('#C2').prop('checked', ($cabecera)['C2']);
    iframe.contents().find('#C5').prop('checked', ($cabecera)['C5']);
    $.each(iframe.contents().find('.row-periodo-fiscal').find('td'), function (index, element) {
        element.innerHTML = ($cabecera)['periodo_fiscal'][index];
    });

    // Completar campos de numeros.
    let query = $detalle;
    query.forEach(function (e) {
        let val = e['monto'];
        iframe.contents().find('input[id="C' + e['casilla'] + '"]').val(val);
    });

    // Quitar readonly
    iframe.contents().find('input[id^="C"]').prop('readonly', false);

    // hacer readonly a algunos campos calculados
    // TODO: 

    // Eventos en campos del iframe
    iframe.contents().find('input[id^="C"]').keyup(function (evt) {
        if (!isNumberKey(evt.key)) return false;

        let id = $(this).prop('id');
        let casilla = parseInt(getNumbersFrom(id)[0]);
        if (casilla < 10 || casilla > 66) return true;

        let rubro = getRubro(casilla);
        let inciso = getInciso(casilla);
        switch (rubro) {
            case 1:
                setTotalRubro1();
                break;
            case 2:
                setTotalRubro2();
                break;
            case 3:
                setTotalRubro3();
                break;
            case 4:
                setTotalRubro4();
                break;
            case 5:
                setTotalRubro5();
                break;
            case 6:
                setTotalRubro6();
                break;
        }

    });
});

function isNumberKey(key) {
    let parsed = parseInt(key);
    return !isNaN(parsed) || key === 'Backspace' || key === 'Delete';
}

function prorratearIvaRubro1(iva = 10, input_gravada = null, input_impuesto = null) {
    try {
        let gravada = convertToInteger(input_gravada.val());
        let monto = gravada * (100 + iva) / 100 - gravada;
        input_impuesto.val(Math.round(monto)).trigger('change');
    } catch (err) {
        console.log('execion en calcularIva: ', err);
    }
}

function calcularIvaCreditoRubro3(input_grav5 = null, input_grav10 = null, input_impuesto = null) {
    try {
        let gravada5 = convertToInteger(input_grav5.val());
        let gravada10 = convertToInteger(input_grav10.val());
        let monto  = gravada5 * (100 + 5) / 100 - gravada5;
        monto += gravada10 * (100 + 10) / 100 - gravada10;
        input_impuesto.val(Math.round(monto)).trigger('change');
    } catch (error) {
        input_impuesto.val(0).trigger('change');
    }
}

function setTotalRubro1() {
    // COLUMNA I
    let iframe = $('#marangatu_iframe').contents();
    let suma = 0;
    for (let i = 10; i <= 17; i++) {
        let value = iframe.find('input[id="C' + i + '"]').val();
        suma += convertToInteger(value);
    }
    iframe.find('input[id="C18"]').val(suma).trigger('change');

    // COLUMNA II
    suma = 0;
    for (let i = 19; i <= 20; i++) {
        let value = iframe.find('input[id="C' + i + '"]').val();
        suma += convertToInteger(value);
    }
    iframe.find('input[id="C21"]').val(suma).trigger('change');
    prorratearIvaRubro1(5, iframe.find('input[id="C11"]'), iframe.find('input[id="C19"]'));
    prorratearIvaRubro1(5, iframe.find('input[id="C16"]'), iframe.find('input[id="C20"]'));

    // COLUMNA III
    suma = 0;
    for (let i = 22; i <= 23; i++) {
        let value = iframe.find('input[id="C' + i + '"]').val();
        suma += convertToInteger(value);
    }
    iframe.find('input[id="C24"]').val(suma).trigger('change');
    prorratearIvaRubro1(10, iframe.find('input[id="C10"]'), iframe.find('input[id="C22"]'));
    prorratearIvaRubro1(10, iframe.find('input[id="C15"]'), iframe.find('input[id="C23"]'));

    // Para Rubro 4 Inciso a Casilla 44
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C21"]').val());
    suma += convertToInteger(iframe.find('input[id="C24"]').val());
    iframe.find('input[id="C44"]').val(suma).trigger('change');
}

function setTotalRubro2() {
    let iframe = $('#marangatu_iframe').contents();
    let suma = 0;

    // Inciso C
    for (let i = 25; i <= 26; i++) {
        let value = iframe.find('input[id="C' + i + '"]').val();
        suma += convertToInteger(value);
    }
    iframe.find('input[id="C27"]').val(suma).trigger('change');

    // Inciso F
    suma = 0;
    for (let i = 28; i <= 29; i++) {
        let value = iframe.find('input[id="C' + i + '"]').val();
        suma += convertToInteger(value);
    }
    iframe.find('input[id="C30"]').val(suma).trigger('change');

    // Inciso g
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C27"]').val());
    suma += convertToInteger(iframe.find('input[id="C30"]').val());
    iframe.find('input[id="C31"]').val(suma).trigger('change');

    // Para Rubro 3 Inciso c Columna III
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C25"]').val());
    suma /= convertToInteger(iframe.find('input[id="C27"]').val());
    suma *= convertToInteger(iframe.find('input[id="C39"]').val());
    iframe.find('input[id="C40"]').val(Math.round(suma)).trigger('change');
}

function setTotalRubro3() {
    let iframe = $('#marangatu_iframe').contents();
    let suma = 0;
    
    // Casillas 32, 35, 38; 33, 36, 39; 34, 37, 42
    calcularIvaCreditoRubro3(iframe.find('input[id="C32"]'), iframe.find('input[id="C35"]'), iframe.find('input[id="C38"]'));
    calcularIvaCreditoRubro3(iframe.find('input[id="C33"]'), iframe.find('input[id="C36"]'), iframe.find('input[id="C39"]'));
    calcularIvaCreditoRubro3(iframe.find('input[id="C34"]'), iframe.find('input[id="C37"]'), iframe.find('input[id="C42"]'));

    // Casilla 40
    suma += convertToInteger(iframe.find('input[id="C25"]').val());
    suma /= convertToInteger(iframe.find('input[id="C27"]').val());
    suma *= convertToInteger(iframe.find('input[id="C39"]').val());
    iframe.find('input[id="C40"]').val(Math.round(suma)).trigger('change');

    // Casilla 43
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C38"]').val());
    suma += convertToInteger(iframe.find('input[id="C40"]').val());
    suma += convertToInteger(iframe.find('input[id="C41"]').val());
    suma += convertToInteger(iframe.find('input[id="C42"]').val());
    iframe.find('input[id="C43"]').val(Math.round(suma)).trigger('change');

    // Para Rubro 4 Inciso b Casilla 45
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C43"]').val());
    iframe.find('input[id="C45"]').val(Math.round(suma)).trigger('change');

}

function setTotalRubro4() {
    console.log('rubro4');
    let iframe = $('#marangatu_iframe').contents();
    let suma = 0;

    // Casilla 44
    suma += convertToInteger(iframe.find('input[id="C21"]').val());
    suma += convertToInteger(iframe.find('input[id="C24"]').val());
    iframe.find('input[id="C44"]').val(suma).trigger('change');

    // Casilla 45
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C43"]').val());
    iframe.find('input[id="C45"]').val(Math.round(suma)).trigger('change');

    // Casilla 46: Rubro 4 Inciso d del periodo anterior
    suma = 0;
    detalle_pa.forEach(function (e, i) {
        if (e['rubro'] === '4' && e['inciso'] === 'd') {
            suma += convertToInteger(e['monto']);
        }
    });
    iframe.find('input[id="C46"]').val(Math.round(suma)).trigger('change');

    // Casilla 47 y 48
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C44"]').val());
    suma -= convertToInteger(iframe.find('input[id="C45"]').val());
    suma -= convertToInteger(iframe.find('input[id="C46"]').val());
    iframe.find('input[id="C47"]').val(Math.round((suma < 0) ? Math.abs(suma) : 0)).trigger('change');
    iframe.find('input[id="C48"]').val(Math.round((suma > 0) ? suma : 0)).trigger('change');

    // Casilla 50
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C48"]').val());
    suma -= convertToInteger(iframe.find('input[id="C49"]').val());
    iframe.find('input[id="C50"]').val(Math.round(suma)).trigger('change');

    // Para Rubro 5 Casilla 55
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C50"]').val());
    iframe.find('input[id="C55"]').val(Math.round(suma)).trigger('change');
}

function setTotalRubro5() {
    let iframe = $('#marangatu_iframe').contents();
    let suma = 0;

    // Casilla 55
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C50"]').val());
    iframe.find('input[id="C55"]').val(Math.round(suma)).trigger('change');
    
    // Casilla 51: Rubro 5 Columna I Inciso f del periodo anterior
    suma = 0;
    detalle_pa.forEach(function (e, i) {
        if (e['rubro'] === '5' && e['columna'] === 'I' && e['inciso'] === 'f') {
            suma += convertToInteger(e['monto']);
        }
    });
    iframe.find('input[id="C46"]').val(Math.round(suma)).trigger('change');
    
    // Casilla 52: Retenciones Recibidas
    
    // Casilla 56: Multas por declaracion atrazada

    // Casilla 53
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C51"]').val());
    suma += convertToInteger(iframe.find('input[id="C52"]').val());
    iframe.find('input[id="C53"]').val(Math.round(suma)).trigger('change');

    // Casilla 57
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C55"]').val());
    suma += convertToInteger(iframe.find('input[id="C56"]').val());
    iframe.find('input[id="C57"]').val(Math.round(suma)).trigger('change');

    // Casilla 54 y 58
    suma = 0;
    suma += convertToInteger(iframe.find('input[id="C53"]').val());
    console.log(suma);
    suma -= convertToInteger(iframe.find('input[id="C57"]').val());
    console.log(suma);
    iframe.find('input[id="C54"]').val(Math.round((suma > 0) ? suma : 0)).trigger('change');
    iframe.find('input[id="C58"]').val(Math.round((suma < 0) ? Math.abs(suma) : 0)).trigger('change');
}

function setTotalRubro6() {
    // TODO:
}
JS;
$this->registerJs($SCRIPTS);
?>