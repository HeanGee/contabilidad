<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 04/01/19
 * Time: 09:54 AM
 */


use backend\modules\contabilidad\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\PresentacionIva;
use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

/* @var $this yii\web\View */

?>

<?php
echo Html::a('Match empresa y periodo', ['match-empresa-periodo'], [
    'class' => 'btn btn-warning btn-xs match-empresa-periodo',
]);

$script = <<<JS
msg = "Establecer empresa y periodo de los rubros iguales a los de su cabecera. Esto es debido a que inicialmente " +
    "solamente la cabecera almacenaba empresa y periodo pero los rubros no.";
applyPopOver($('a.match-empresa-periodo'), 'Es para:', msg, {
    'title-css': {
        'background-color': '#ffbb33',
        'font-weight': 'bold',
        'text-align': 'center'
    }, 'placement': 'top'
});
delete msg;
JS;
$this->registerJs($script);
?>
