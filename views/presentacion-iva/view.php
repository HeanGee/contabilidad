<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PresentacionIva */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Presentación Ivas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="presentacion-iva-view">

    <p>
        <?php
        $permisos = [
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-presentacion-iva-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-presentacion-iva-delete'),
        ];
        ?>
        <?= $permisos['update'] ? Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos ['delete'] ? Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                ['attribute' => 'nro_orden', 'value' => $model->nro_orden ? $model->nro_orden : ''],
//                'nro_orden',
                'tipo_declaracion',
                'periodo_contable_id',
                'empresa_id',
                'estado',
                'presentacion_iva_id',
            ],
        ]);
    } catch (Exception $exception) {

    } ?>

</div>
