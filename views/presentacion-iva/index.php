<?php

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\PresentacionIva;
use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\PresentacionIvaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Presentación de I.V.A.';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="presentacion-iva-index">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p class="btn-toolbar">
            <!--        <? //= Html::a('Create Presentacion Iva', ['create'], ['class' => 'btn btn-success']) ?>-->

            <?php echo PermisosHelpers::getAcceso('contabilidad-presentacion-iva-create') ?
                Html::button('Declarar I.V.A.', [
                    'id' => 'btn_new_iva_pres',
                    'type' => 'button',
                    'title' => Yii::t('app', 'Declaración de I.V.A.'),
                    'style' => 'margin-left: 5px',
                    'class' => 'btn btn-success tiene-modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'data-url' => Url::to(['create']),
                    'data-pjax' => '0',
                    'data-title' => "Declaración de I.V.A."]) : null; ?>
        </p>

        <?php try {
            $template = '';
            foreach (['view', 'update', 'delete', 'present', 'rectificate'] as $_v)
                if (PermisosHelpers::getAcceso("contabilidad-presentacion-iva-{$_v}"))
                    $template .= "&nbsp&nbsp&nbsp{{$_v}}";

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'hover' => true,
                'responsive' => true,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn',],

                    [
                        'attribute' => 'nro_orden',
                        'value' => function ($model) {
                            return $model->nro_orden ? $model->nro_orden : "- no definido -";
                        },
                        'width' => '90px',
                    ],
                    [
                        'attribute' => 'codigo_set',
                        'value' => function ($model) {
                            return $model->codigo_set ? $model->codigo_set : "- aún no presentado -";
                        },
                        'width' => '90px',
                    ],
                    [
                        'attribute' => 'periodo_fiscal',
                        'value' => function ($model) {
                            return $model->monthYearToString();
                        },
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'periodo_fiscal',
                            'language' => 'es',
                            'pickerButton' => false,
                            'options' => [
                                'style' => 'width:90px',
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'startView' => 'year',
                                'minViewMode' => 'months',
                                'format' => 'mm-yyyy',
                                'language' => 'es',
                            ]
                        ]),
                        'width' => '100px',
                    ],
                    [
                        'attribute' => 'tipo_declaracion',
                        'value' => function ($model) {
                            return str_replace('_', ' ', ucfirst($model->tipo_declaracion));
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'tipo_declaracion',
                            'initValueText' => 'Si',
                            'data' => PresentacionIva::getValoresEnum('tipo_declaracion', true, true),
                            'hideSearch' => false,
                        ]),
                    ],
                    [
                        'attribute' => 'estado',
                        'value' => function ($model) {
                            return str_replace('_', ' ', ucfirst($model->estado));
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'estado',
                            'initValueText' => 'Si',
                            'data' => PresentacionIva::getValoresEnum('estado', true, true),
                            'hideSearch' => false,
                        ])
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'update' => function ($url, $model, $index) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                    $url,
                                    [
                                        'class' => 'update_btn',
                                        'id' => "update_btn_{$model->id}",
                                        'title' => Yii::t('app', 'Editar'),
                                    ]
                                );
                            },
                            'view' => function ($url, $model, $index) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-eye-open"></span>',
                                    $url,
                                    [
                                        'class' => 'view_btn',
                                        'id' => "view_btn_{$model->id}",
                                        'title' => Yii::t('app', 'Ver'),
                                    ]
                                );
                            },
                            'delete' => function ($url, $model, $index) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-trash"></span>',
                                    "{$url}",
                                    !$model->isDeleteable() ? [] : [
                                        'class' => 'delete_btn',
                                        'data' => [
                                            'method' => 'post',
                                            'confirm' => 'RECUERDE: Las declaraciones ya presentadas no podrán eliminarse ni modificarse. Desea borrar esta declaración?',
                                        ],
                                        'id' => "delete_btn_{$model->id}",
                                        'title' => Yii::t('app', 'Borrar'),
                                    ]
                                );
                            },
                            'present' => function ($url, $model, $index) {
                                $class = $model->isPresented() ? ($model->isNextGenerable() ? "btn btn-success btn-xs" : "btn btn-info btn-xs") : "btn btn-success btn-xs";
                                $content = $model->isPresented() ? ($model->isNextGenerable() ? 'Generar Sigte' : 'Presentado') : "Presentar";
                                if (!$model->isPresented()) {
                                    return Html::button("Presentar", [
                                        'id' => "present_btn_{$model->id}",
                                        'type' => 'button',
                                        'title' => Yii::t('app', $content),
                                        'style' => 'margin-left: 5px',
                                        'class' => 'btn btn-success btn-xs tiene-modal',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#modal',
                                        'data-url' => Url::to(['present', 'id' => $model->id]),
                                        'data-pjax' => '0',
                                        'data-title' => $content
                                    ]);
                                }
                                return Html::a(
                                    Html::tag('span', $content, ['class' => $class]),
                                    $url,
                                    [
                                        'class' => 'present_btn',
                                        'data' => $model->isNextGenerable() ? [
                                            'method' => 'post',
                                        ] : [],
                                        'id' => "present_btn_{$model->id}",
                                        'title' => Yii::t('app', $content),
                                    ]
                                );
                            },
                            'rectificate' => function ($url, $model, $index) {
                                $class = !$model->isRectificable() ? "btn btn-warning btn-xs" : "btn btn-success btn-xs";
                                $content = !$model->isRectificable() ? 'No Rectificable' : "Rectificar";
                                return Html::a(
                                    Html::tag('span', $content, ['class' => $class]),
                                    $url,
                                    [
                                        'class' => 'rectificate_btn',
                                        'data' => $model->isRectificable() ? [
                                            'confirm' => 'Realmente desea rectificar esta declaración?',
                                        ] : [],
                                        'id' => "rectificate_btn_{$model->id}",
                                        'title' => Yii::t('app', 'Rectificar'),
                                    ]
                                );
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'update') {
                                $url = Url::to(['presentacion-iva/update', 'id' => $model->id]);
                                return $model->isUpdateable() ? $url : "";
                            }
                            if ($action === 'view') {
                                $url = Url::to(['presentacion-iva/view', 'id' => $model->id]);
                                return $url;
                            }
                            if ($action === 'delete') {
                                $url = $model->isDeleteable() ?
                                    Url::to(['presentacion-iva/delete', 'id' => $model->id]) :
                                    "";
                                return $url;
                            }
                            if ($action === 'present') {
                                $url = Url::to(['presentacion-iva/present', 'id' => $model->id]);
                                $url_next = Url::to(['presentacion-iva/generate-next', 'id' => $model->id]);
                                return !$model->isPresented() ? $url :
                                    ($model->isNextGenerable() ? $url_next : '');
                            }
                            if ($action === 'rectificate') {
                                $url = Url::to(['presentacion-iva/rectificate', 'id' => $model->id]);
                                return $model->isRectificable() ? $url : '';
                            }
                            return '';
                        },
                        'template' => $template,
                    ],
                ],
            ]);
        } catch (Exception $exception) {
            throw new ForbiddenHttpException($exception->getMessage());
        }

        if (PermisosHelpers::esSuperUsuario()) {
            $adminFunctions = $this->render('_admin_panel', []);
            $header = "PANEL DE ADMINISTRADOR " . HtmlHelpers::WarningHelpIcon('admin-panel');
            echo HtmlHelpers::BootstrapWarningPanel($header, $adminFunctions);
            $script = <<<JS
msg = "Muestra botones con funcionalidades muy poco frecuentes pero que deben alterar una gran cantidad de registros de " +
    "la base de datos de una sola vez.";
applyPopOver($('span.popuptrigger'), 'INFORMACIÓN', msg, {
    'title-css': {
        'background-color': '#ffbb33',
        'font-weight': 'bold',
        'text-align': 'center'
    }, 'placement': 'top'
});
delete msg;
JS;
            $this->registerJs($script);
        }
        ?>
    </div>

<?php
$SCRIPTS = <<<JS
$(document).on('click', '.tiene-modal', function () {
    var boton = $(this);
    var title = boton.data('title');
    $.get(
        boton.data('url'),
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
});

$(document).ready(function () {
    // Static anchor of certain columns
    let element = $('#w0-container')[0].getElementsByTagName('th');
    // element[1].style.width = "17.73%";
    // element[2].style.width = "18%";
    // element[3].style.width = "10%";
    // element[4].style.width = "14.34%";

    msg = "<u>Una presentación se puede borrar si:</u>" +
        "<ul>" +
        "<li>Es original y:" +
        "<ul>" +
        "<li>No está presentada aún.</li>" +
        "<li>No existen presentaciones siguientes.</li>" +
        "</ul>" +
        "</li>" +
        "<li>Es rectificativa y:" +
        "<ul>" +
        "<li>No está presentada aún.</li>" +
        "</ul>" +
        "</li>" +
        "</ul>";
    applyPopOver($('span.glyphicon-trash'), '¡Atención!', msg, {
        'title-css': {
            'background-color': '#ffbb33',
            'font-weight': 'bold',
            'text-align': 'center'
        }, 'placement': 'left',
        'container': 'section.content',
        'popover-css': {'max-width': '30%'},
    });
    delete msg;
});
JS;
$this->registerJs($SCRIPTS);
?>