<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PresentacionIva */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="presentacion-iva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nro_orden')->textInput() ?>

    <?= $form->field($model, 'tipo_declaracion')->dropDownList(['jurada_original' => 'Jurada original', 'jurada_rectificativa' => 'Jurada rectificativa', 'jurada_en_caracter_de_clausura_o_ces.' => 'Jurada en caracter de clausura o ces.',], ['prompt' => '']) ?>

    <?= $form->field($model, 'entidad_id')->textInput() ?>

    <?= $form->field($model, 'periodo_contable_id')->textInput() ?>

    <?= $form->field($model, 'empresa_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estado')->dropDownList(['borrador' => 'Borrador', 'presentado' => 'Presentado',], ['prompt' => '']) ?>

    <?= $form->field($model, 'presentacion_iva_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
