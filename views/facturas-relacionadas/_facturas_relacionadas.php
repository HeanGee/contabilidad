<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 11/08/2018
 * Time: 0:37
 */

use backend\modules\contabilidad\models\ActivoFijo;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\Venta;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;

/* @var $model \backend\models\BaseModel */
/* @var $model \backend\models\BaseModel */
?>

<?php
$actionId = Yii::$app->controller->action->id;
$operacion = Yii::$app->request->getQueryParam('operacion');


/** ------------------------ [inicio] Facturas relacionadas ------------------------ */
// Obtener compras o ventas y a la vez determinar el nombre para label de la columna del gridview.
$label = 'Cliente';
$allModels_venta = null;
$allModels_compra = null;
try {
    $allModels_venta = $model->getVentas()->all();
} catch (Exception $exception) {
    $allModels_venta = null;
}

try {
    $allModels_compra = $model->getCompras()->all();
} catch (Exception $exception) {
    $allModels_compra = null;
}

if (isset($allModels_compra)) {

    $label = 'Proveedor';
    if ($model instanceof ActivoFijo) $label = 'Comprador';

    $columns = [
        [
            'class' => SerialColumn::className(),
        ],

        'id',
        [
            'label' => $label,
            'value' => function ($model, $key, $index, $column) {
                if ($model instanceof Venta || $model instanceof Compra)
                    return $model->entidad->razon_social;
                return "";
            },
        ],
        array(
            'label' => 'Nro Factura',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                if ($model instanceof Venta) {
                    $url = Url::to(['venta/view', 'id' => $model->id]);
                    return \yii\helpers\Html::a($model->getNroFacturaCompleto(), $url);
                } elseif ($model instanceof Compra) {
                    $url = Url::to(['compra/view', 'id' => $model->id]);
                    return \yii\helpers\Html::a($model->nro_factura, $url);
                }
                return null;
            },
        ),
    ];

    $template = '{view}';
    $buttons = [];

//    array_push($columns, [
//        'class' => ActionColumn::class,
//        'template' => $template,
//        'buttons' => $buttons,
//        'urlCreator' => function ($action, $model, $key, $index) {
//            if ($action === 'view') {
//                $factura = $model instanceof Venta ? 'venta' : 'compra';
//                $url = Url::to([$factura . '/view', 'id' => $model->id]);
//                return $url;
//            }
//            return '';
//        },
//    ]);

    $dataProvider = new ArrayDataProvider([
        'allModels' => $allModels_compra,
        'pagination' => false,
    ]);

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'hover' => true,
            'id' => 'grid',
            'panel' => ['type' => 'info', 'heading' => 'Facturas de Compra relacionadas', 'footer' => false,],
            'toolbar' => [
            ]
        ]);
    } catch (Exception $exception) {
        print $exception;
    }
}

if (isset($allModels_venta)) {

    $label = 'Cliente';
    if ($model instanceof ActivoFijo) $label = 'Comprador';

    $columns = [
        [
            'class' => SerialColumn::className(),
        ],

        'id',
        [
            'label' => $label,
            'value' => function ($model, $key, $index, $column) {
                if ($model instanceof Venta || $model instanceof Compra)
                    return $model->entidad->razon_social;
                return "";
            },
        ],
        array(
            'label' => 'Nro Factura',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                if ($model instanceof Venta) {
                    $url = Url::to(['venta/view', 'id' => $model->id]);
                    return \yii\helpers\Html::a($model->getNroFacturaCompleto(), $url);
                } elseif ($model instanceof Compra) {
                    $url = Url::to(['compra/view', 'id' => $model->id]);
                    return \yii\helpers\Html::a($model->nro_factura, $url);
                }
                return null;
            },
        ),
    ];

    $template = '{view}';
    $buttons = [];

//    array_push($columns, [
//        'class' => ActionColumn::class,
//        'template' => $template,
//        'buttons' => $buttons,
//        'urlCreator' => function ($action, $model, $key, $index) {
//            if ($action === 'view') {
//                $factura = $model instanceof Venta ? 'venta' : 'compra';
//                $url = Url::to([$factura . '/view', 'id' => $model->id]);
//                return $url;
//            }
//            return '';
//        },
//    ]);

    $dataProvider = new ArrayDataProvider([
        'allModels' => $allModels_venta,
        'pagination' => false,
    ]);

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'hover' => true,
            'id' => 'grid',
            'panel' => ['type' => 'info', 'heading' => 'Facturas de Venta relacionadas', 'footer' => false,],
            'toolbar' => [
            ]
        ]);
    } catch (Exception $exception) {
        print $exception;
    }
}
/** ------------------------ [fin] Facturas relacionadas ------------------------ */
?>
