<?php

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Entidad */
/* @var $form kartik\form\ActiveForm */
/* @var $entidad int */
?>

<div class="entidad-form">

    <?php $form = ActiveForm::begin(['id' => 'form-entidad']);
    echo "<p>";
    echo \common\helpers\PermisosHelpers::getAcceso('contabilidad-entidad-index') ?
        Html::a("Regresar a la lista de Entidades", ['index'], ['class' => 'btn btn-info']) : null;
    echo "</p>";
    ?>

    <?php
    try {
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'razon_social' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'placeholder ' => 'Razón Social',
                                'id' => 'campo_razon_social',
                            ],
                            'columnOptions' => ['colspan' => '3'],
                        ],
                        'extranjero' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'data' => ['si' => 'Si', 'no' => 'No'],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                                'options' => ['placeholder' => 'Seleccione una opción...'],
                            ]
                        ],
                        'ruc' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'id' => 'campo_empresa-ruc',
                                'placeholder' => 'RUC...'
                            ],
                            'columnOptions' => ['colspan' => '3'],
                        ],
                        'digito_verificador' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'id' => 'campo_empresa-digito_verificador',
                                'readonly' => 'true',
                            ],
                            'columnOptions' => ['colspan' => '1'],
                            'label' => "D.V."
                        ],
                        'telefonos' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'id' => 'campo_telefonos',
                            ],
                            'columnOptions' => ['colspan' => '4'],
                        ],
                    ]
                ],

                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
//                        'tipo_entidad' => [
//                            'type' => Form::INPUT_RAW,
//                            'value' => $form->field($model, 'tipo_entidad')->widget(Select2::className(), [
//                                'data' => [1 => 'Cliente', 2 => 'Proveedor'],
//                                'initValueText' => $entidad == 1 ? "Cliente" : "Proveedor",
//                                'options' => [
//                                    'id' => 'campo_tipo_entidad',
//                                    'placeholder' => 'Tipo de entidad',
//                                ],
//                                'hideSearch' => true,
//                                'pluginOptions' => [
//                                    'allowClear' => true,
//                                ],
//                                'pluginEvents' => [
//                                    'change' => ""
//                                ]
//                            ])
//                        ],
                        'direccion' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['id' => 'campo_direccion',
                                'placeholder ' => 'Direccion...'
                            ],
                            'columnOptions' => ['colspan' => '10'],
                        ],
                        'es_contribuyente' => [
                            'type' => Form::INPUT_RAW,
                            'value' => $form->field($model, 'es_contribuyente')->widget(Select2::className(), [
                                'data' => ['Si' => 'Si', 'No' => 'No'],
                                'options' => ['placeholder' => 'Es contribuyente...?'],
                                'initValueText' => !empty($model->es_contribuyente) ? $model->es_contribuyente : '',
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                                'pluginEvents' => []
                            ]),
                            'columnOptions' => ['colspan' => '2'],
                        ],
                    ]
                ],
                [
                    'attributes' => [
                        'action' => [
                            'type' => Form::INPUT_RAW,
                            'value' => '<div style="margin-top: 20px">' .
                                Html::submitButton('Guardar', ['class' => 'btn btn-primary']) .
                                '</div>'
                        ],
                    ],
                ]
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>

<?php
//$digVerifUrl = \yii\helpers\Json::htmlEncode(\Yii::t('app', Url::to(['get-digito-verificador'])));
$script = <<<JS
$(document).on('change', '#entidad-extranjero', function() {
    $('form[id="form-entidad"]').yiiActiveForm('validateAttribute', 'entidad-ruc');
});

$('#campo_empresa-ruc').on('change keyup', function () {
        $.ajax({
            url: "index.php?r=contabilidad/entidad/get-digito-verificador&numero="+($(this).val()),
            type: 'post',
            // data: {
            //     numero: $(this).val()
            // },
            success: function (data) {
                $('#campo_empresa-digito_verificador').val(data);
            }
        });
    });
JS;
$this->registerJs($script);
?>
