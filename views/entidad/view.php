<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Entidad */

$this->title = "Datos de " . $model->tipo_entidad . " " . $model->razon_social . '.';
$this->params['breadcrumbs'][] = ['label' => $model->tipo_entidad, 'url' => ['index', 'entidad' => $model->tipo_entidad]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entidad-view">

    <p>
        <?= (\common\helpers\PermisosHelpers::getAcceso('contabilidad-entidad-index')) ?
            Html::a("Regresar a la lista de " . lcfirst($model->tipo_entidad) . ($model->tipo_entidad == 'Cliente' ? 's' : 'es'), ['index', 'entidad' => $model->tipo_entidad], ['class' => 'btn btn-info']) : ''; ?>
        <?= (\common\helpers\PermisosHelpers::getAcceso('contabilidad-entidad-update')) ?
            Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= (\common\helpers\PermisosHelpers::getAcceso('contabilidad-entidad-delete')) ?
            Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Estás seguro de eliminar esta entidad?',
                    'method' => 'post',
                ],
            ]) : '' ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Información General',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'razon_social',
                'ruc',
                'digito_verificador',
                'direccion',
                'telefonos',
                [
                    'attribute' => 'es_contribuyente',
                    'format' => 'raw',
                    'value' => $model->es_contribuyente == 'Si' ? '<span class="label label-success">Sí</span>' : '<span class="label label-danger">No</span>',
                ],
            ],
        ]);
    } catch (Exception $exception) {
        \common\helpers\FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    } ?>

</div>
