<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Entidad */

$this->title = "Modificar ".$model->tipo_entidad." ".$model->razon_social.'.';
$this->params['breadcrumbs'][] = ['label' => 'Entidad', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="entidad-update">

    <?= $this->render('_form', [
        'model' => $model,
//        'entidad' => $model->tipo_entidad,
    ]) ?>

</div>
