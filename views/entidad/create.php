<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Entidad */
/* @var $entidad int */

$this->title = "Crear Nueva Entidad";
$this->params['breadcrumbs'][] = ['label' => 'Entidad', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entidad-create">

    <?= $this->render('_form', [
        'model' => $model,
//        'entidad' => $entidad,
    ]) ?>

</div>
