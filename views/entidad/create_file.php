<?php

use backend\helpers\HtmlHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\EntidadArchivo */
/* @var $form backend\models\UploadForm */
/* @var $mensaje String */
/* @var $mensaje2 String */

$this->title = "Cargar entidades desde archivo";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="movimiento-form">

    <?php $form = ActiveForm::begin(['id' => 'subir_arch_form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= Html::a("Regresar a la Lista de Entidades", ['index',], ['class' => 'btn btn-info']); ?>

    <br/>
    <br/>
    <div class="panel panel-success">
        <div class="panel-heading"><b>Enlace a</b></div>
        <div class="panel-body">
            <a href="http://www.set.gov.py/portal/PARAGUAY-SET/InformesPeriodicos?folder-id=repository:collaboration:/sites/PARAGUAY-SET/categories/SET/Informes%20Periodicos/listado-de-ruc-con-sus-equivalencias"
               target="_blank">
                Listado de RUC con sus equivalencias.
            </a>
        </div>
    </div>

    <?= $form->field($model, 'archivo_txt[]')->fileInput(['multiple' => true]) ?>


    <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>

    <br/>

    <?php
    if (!empty($log)) {
        $html = "<ul>";
        foreach ($log as $item) {
            $html .= "<li>Error en la fila {$item['fila']}: {$item['msg']}</li>";
        }
        $html .= "</ul>";
        $body = HtmlHelpers::trimmedHtml($html);
        echo HtmlHelpers::BootstrapWarningPanel("Resumen de errores", $body, '', true);
    }
    ?>

    <?php
    if ($mensaje != "") {
        echo $mensaje;

    } else {
        ?>
        <br/>
    <?php } ?>

    <br/>
</div>
