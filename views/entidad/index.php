<?php

use common\helpers\PermisosHelpers;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\EntidadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entidad int */

//$this->title = $entidad.($entidad=='Cliente'?'s':'es');
$this->title = 'Entidades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entidad-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if (PermisosHelpers::getAcceso('contabilidad-entidad-create')) {
            // TODO: cambiar
            echo Html::a("Crear Nueva Entidad", ['create',], ['class' => 'btn btn-success']);
        } ?>
        <?php if (PermisosHelpers::getAcceso('contabilidad-entidad-entidad-from-file')) {
            echo '&nbsp&nbsp';
            echo Html::a("Entidades desde Archivo", ['entidad-from-file',], ['class' => 'btn btn-info']);
        } ?>
    </p>

    <?php
    try {
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (PermisosHelpers::getAcceso("contabilidad-entidad-{$_v}"))
                $template .= "&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//            'id',
                'razon_social',
                'ruc',
                'digito_verificador',
                'direccion',
                'telefonos',
//                'es_contribuyente',
                [
                    'attribute' => 'es_contribuyente',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->es_contribuyente == 'Si') ? 'success">Sí' : 'danger">No') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'es_contribuyente',
                        'data' => ['Todos' => 'Todos', 'Si' => 'Si', 'No' => 'No'],
                        'hideSearch' => true,
                        'pluginOptions' => [
//                            'width' => '120px'
                        ]
                    ])
                ],
                [
                    'attribute' => 'extranjero',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $icon_success = "<label class=\"label label-success\">Sí</label>";
                        $icon_fails = "<label class=\"label label-danger\">No</label>";
                        return ($model->extranjero == 'si' ? $icon_success : $icon_fails);
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'extranjero',
                        'data' => ['Todos' => 'Todos', 'si' => 'Si', 'no' => 'No'],
                        'hideSearch' => true,
                        'pluginOptions' => [
//                            'width' => '120px'
                        ]
                    ])
                ],
//                [
//                    'attribute' => 'tipo_entidad',
//                    'value' => function($model, $key, $index, $column) {
//                        return $model->tipo_entidad == 1 ? 'Cliente' : 'Proveedor';
//                    },
//                    'filter' => Select2::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'tipo_entidad',
//                        'data' => [1 => 'Cliente', 0 => 'Proveedor'],
//                        'pluginOptions' => [
//                            'width' => '120px'
//                        ]
//                    ])
//                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                    ],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $e) {
        \common\helpers\FlashMessageHelpsers::createWarningMessage($e->getMessage());
    } ?>
</div>
    <!---->
<?php
//$CSS = <<<CSS
//.table tbody tr:hover {
//    background-color: #ebffdd;
//}
//CSS;
//$this->registerCss($CSS);
//?>