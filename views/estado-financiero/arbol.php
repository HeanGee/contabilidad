<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 21/12/18
 * Time: 01:45 PM
 */

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $arboles \backend\modules\contabilidad\helpers\Nodo[] */
/* @var $balance \array */

$this->title = 'Arbol';
$this->params['breadcrumbs'][] = ['label' => 'Plan de Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$balance = $arboles[0]->balance;
?>
    <div class="btn-toolbar">
        <?= Html::a('Ir a Index', ['index'], ['class' => 'btn btn-info']) ?>
    </div>

    <!--    TABLA DE BALANCE-->
<?php if (isset($balance)) : ?>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th colspan="6" style="font-weight: bold; font-size: 14px; text-align: center">BALANCE DE SUMAS Y SALDOS
            </th>
        </tr>
        <tr>
            <th style="font-weight: bold; font-size: 14px; text-align: center">ID</th>
            <th style="font-weight: bold; font-size: 14px; text-align: center">CODIGO</th>
            <th style="font-weight: bold; font-size: 14px; text-align: center">NOMBRE</th>
            <th style="font-weight: bold; font-size: 14px; text-align: right">SUMA DEBE</th>
            <th style="font-weight: bold; font-size: 14px; text-align: right">SUMA HABER</th>
            <th style="font-weight: bold; font-size: 14px; text-align: right">SALDO (+/-)</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($balance as $item) {
            $pad_length = sizeof(explode('.', $item['cuenta_codigo']));
            $espacios = str_repeat('&emsp;', $pad_length * 2);
            ?>
            <tr>
                <td width="30px;"><?= $item['cuenta_id'] ?></td>
                <td width="120px;"><?= $item['cuenta_codigo'] ?></td>
                <td><?= $item['cuenta_nombre'] ?></td>
                <td style="text-align: right"><?= number_format($item['debe'], 0, '', '.') ?></td>
                <td style="text-align: right"><?= number_format($item['haber'], 0, '', '.') ?></td>
                <td style="text-align: right"><?= number_format($item['saldo'], 0, '', '.') ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

<?php else: ?>
    <h3 style="color: orangered; text-align: center;">No hay datos del balance, por tanto no se puede calcular.</h3>
    <br/>
<?php endif; ?>

    <!--TABLA DE CUENTAS CON MONTOS RETROALIMENTADOS-->
    <table class="table table-condensed">
        <thead>
        <tr>
            <th colspan="6" style="font-weight: bold; font-size: 14px; text-align: center">PLAN DE CUENTAS CON VALORES
            </th>
        </tr>
        <tr>
            <th>ID</th>
            <th>Codigo</th>
            <th>Nombre</th>
            <th>Valor</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($arboles as $nodo) : ?>
            <tr>
                <td><?= $nodo->cuenta->id ?></td>
                <td><?= $nodo->cuenta->cod_completo ?></td>
                <td><?= $nodo->cuenta->nombre ?></td>
                <td style="text-align: right"><?= number_format($nodo->monto, 0, '', '.') ?></td>
            </tr>

            <?php render($nodo, 3); ?>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php
/**
 * @param \backend\modules\contabilidad\helpers\Nodo $nodo
 */
function render($nodo, $factor = 3, $vertical = "|")
{
    foreach ($nodo->hijos as $hijo) {
        $pad = "&emsp;";
        $pad_length = strlen($pad);
        $slices_cant = sizeof(explode('.', $hijo->cuenta->cod_completo));
        $espacios = str_repeat($pad, (($slices_cant > 0) ? $slices_cant * $factor - $factor : 0));
        $slices = str_split($espacios, $pad_length);
        $newSlices = [];
        foreach ($slices as $key => $slice) {
            // formula para insertar verticales entre cada sangria
            if ($key % ($factor > 0 ? $factor : 1) == 0) {
                if ($factor > 0) $newSlices[] = $vertical;
            }
            $newSlices[] = $slice;
        }

        $espacios = empty($newSlices) ? $espacios : implode($newSlices);
        echo '<tr>';
        echo '<td>' . $hijo->cuenta->id . '</td>';
        echo '<td>' . $hijo->cuenta->cod_completo . '</td>';
        echo '<td>' . $espacios . $hijo->cuenta->nombre . '</td>';
        echo '<td style="text-align: right">' . number_format($hijo->monto, 0, '', '.') . '</td>';
        echo '</tr>';
        render($hijo, $factor);
    }
}

?>