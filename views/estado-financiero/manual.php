<?php

use yii\web\View;

/** @var $this View */
?>

<br/>
<fieldset>
    <legend class="text-info">
        <small>Instrucciones para dar formato a <strong>Notas Contables</strong>.</small>
    </legend>

    <ul>
        <li>
            <strong>Etiquetas básicas:</strong>
        </li>
    </ul>
    <br/>

    <table class="table table-condensed">
        <tr>
            <th>Etiqueta</th>
            <th>Descripcion</th>
            <th>Ejemplo</th>
            <th>Resultado</th>
        </tr>

        <tr>
            <td style="font-family: Consolas, monaco, monospace;"><?= "(neg)texto(/)" ?></td>
            <td>Pone en negrita.</td>
            <td><?= "esto es (neg)negrita(/)" ?></td>
            <td>esto es <strong>negrita</strong></td>
        </tr>
        <tr>
            <td style="font-family: Consolas, monaco, monospace;"><?= "(sub)texto(/)" ?></td>
            <td>Subraya texto.</td>
            <td><?= "esto es (sub)subrayado(/)" ?></td>
            <td>esto es <u>subrayado</u></td>
        </tr>
        <tr>
            <td style="font-family: Consolas, monaco, monospace;"><?= "(dobs)texto(/)" ?></td>
            <td>Subrayado doble.</td>
            <td><?= "esto es (dobs)subrayado doble(/)" ?></td>
            <td>esto es <span
                        style="text-decoration-line: underline; text-decoration-style: double;">subrayado doble</span>
            </td>
        </tr>
    </table>

    <ul>
        <li>
            <strong>Y cualquier combinación entre ellos también es permitido, sin importar el orden:</strong>
        </li>
    </ul>
    <br/>

    <table class="table table-condensed">
        <tr>
            <th>Etiquetas combinadas</th>
            <th>Descripcion</th>
            <th>Ejemplo</th>
            <th>Resultado</th>
        </tr>

        <tr>
            <td style="font-family: Consolas, monaco, monospace;"><?= "(neg-sub)texto(/)" ?></td>
            <td>Pone en negrita y lo subraya.</td>
            <td><?= "esto es (neg-sub)negrita y subrayado(/)" ?></td>
            <td>esto es <strong><span
                            style="text-decoration-line: underline; text-decoration-style: solid;">negrita y subrayado</span></strong>
            </td>
        </tr>
    </table>
</fieldset>
