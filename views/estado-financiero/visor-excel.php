<?php

use backend\helpers\HtmlHelpers;
use yii\web\View;

/** @var $this View */
?>

<?php

$directorio = '../web/uploads/contabilidad/files/estado-financiero/';
if (file_exists($directorio) && is_dir($directorio)) {
    $ficheros = array_values(scandir($directorio));
    unset($ficheros[0]);
    unset($ficheros[1]);
// Fuente: https://stackoverflow.com/questions/4535846/php-adding-prefix-strings-to-array-values
    array_walk($ficheros, function (&$value, $key) use ($directorio) {
        $value = "$directorio{$value}";
    });
    if (sizeof($ficheros)) {
        echo HtmlHelpers::MultiLinksToFiles("Archivos excel generados y subidos", array_values($ficheros), 'text');
    }
}
