<?php
/* @var $this yii\web\View */

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\EstadoFinanciero;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\daterange\DateRangePicker;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use kartik\helpers\Html;
use yii\web\View;

$this->title = "Estados Financieros";
$this->params['breadcrumbs'][] = $this->title;

/** @var $this View */
/** @var $model EstadoFinanciero */
/** @var $form ActiveForm */
?>

<div class="estado-financiero">

    <?= HtmlHelpers::BootstrapInfoPanel("Mensaje", "Como los campos son numerosos y esto aun esta en fase&nbsp
     de desarrollo, los campos aparecen rellenados con datos de prueba. Una vez finalizado el desarrollo, dichos datos&nbsp 
      junto con este panel de informacion seran removidos.", "", true, true) ?>

    <fieldset>
        <legend class="text-info">
            <small>Datos</small>
        </legend>

        <?php $form = ActiveForm::begin(['id' => 'estado-financiero-form']); ?>

        <?php try {
            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'rango' => [
                                'type' => Form::INPUT_WIDGET,
                                'columnOptions' => ['colspan' => '2'],
                                'widgetClass' => DateRangePicker::className(),
                                'options' => [
                                    'model' => $model,
                                    'attribute' => 'rango',
                                    'convertFormat' => true,
                                    'language' => 'es',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'autoApply' => true,
                                        'locale' => ['format' => 'd-m-Y'],
                                    ],
                                ],
                            ],
                            'contador' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '3'],
                                'label' => "Apellido/Nombre del contador",
                            ],
                            'ruc_contador' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '2'],
                                'label' => "R.U.C. del contador",
                            ],
                            'auditor' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '3'],
                                'label' => "Apellido/Nombre del auditor",
                            ],
                            'ruc_auditor' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '2'],
                                'label' => "R.U.C. del Auditor",
                            ],
                        ]
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'datos_entidad' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'base_preparacion' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'politicas_contables' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'composicion_cuentas_patrimoniales' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'composicion_cuentas_resultado' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'detalle_rentas' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'contingencias' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'identificacion_partes_vinculadas' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'hechos_posteriores_relevantes' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'otras_notas' => [
                                'type' => Form::INPUT_TEXTAREA,
                            ]
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'archivo' => [
                                'type' => Form::INPUT_FILE,
                                'value' => $form->field($model, 'archivo')->widget(FileInput::classname(),
                                    [
                                        'language' => 'es',
                                        'pluginOptions' => ['showPreview' => false],
                                        'options' => ['multiple' => false],
                                    ]
                                ),
                                'label' => "Archivo presentado " . HtmlHelpers::InfoHelpIcon('archivo-generado-help'),
                            ],
                        ],
                    ],
                ],
            ]);

            echo '<div class="form-group">';
            echo Html::submitButton('Generar/Guardar', ['class' => 'btn btn-primary']);
            echo '</div>';
        } catch (Exception $exception) {
            throw $exception;
        } ?>

        <?php ActiveForm::end(); ?>
    </fieldset>
</div>

<hr>

<div class="extras">
    <?= $this->render('visor-excel'); ?>
    <?= $this->render('manual') ?>
</div>

<?php ob_start(); ?>
    <script>
        applyPopOver($('.archivo-generado-help'), 'Información', 'Al especificar un archivo, el sistema procederá a guardar en el servidor en vez de recalcular y generar el reporte.', {
            'theme-color': 'info-l',
            'placement': 'top'
        });
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>