<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\EmpresaPeriodoContable */

$this->title = 'Crear nuevo periodo contable';
$this->params['breadcrumbs'][] = ['label' => 'Empresa Periodo Contables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-periodo-contable-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
