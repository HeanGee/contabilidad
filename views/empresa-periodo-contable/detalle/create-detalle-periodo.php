<?php


/* @var $this yii\web\View */
/* @var $model \backend\modules\contabilidad\models\EmpresaPeriodoContable */
/* @var $disable_periodo_anterior boolean */

?>
<div class="hijo-create">

    <?= $this->render('_form-detalle-periodo', [
        'model' => $model,
        'disable_periodo_anterior' => $disable_periodo_anterior,
    ]) ?>

</div>
