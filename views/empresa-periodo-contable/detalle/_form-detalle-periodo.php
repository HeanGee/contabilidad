<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 3/4/2017
 * Time: 13:40
 */

use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $model EmpresaPeriodoContable */
/* @var $disable_periodo_anterior boolean */

?>
<div class="empleado-hijo-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'periodo_detalle_form',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]);
    ?>

    <?php
    try {
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'attributes' => [
                        'anho' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '3'],
                            'value' => $form->field($model, 'anho')->widget(DatePicker::className(), [
                                'options' => [
                                    'id' => 'campo_periodo_contable',
                                ],
                                'language' => 'es',
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'autoApply' => true,
                                    'allowClear' => true,
                                    'startView' => 'year',
                                    'minViewMode' => 'years',
                                    'format' => 'yyyy',
                                ],
                                'pluginEvents' => [],
                            ]),
                        ],
                        'activo' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1'],
                            'value' => $form->field($model, 'activo')->widget(Select2::className(), [
                                'data' => ['Si' => 'Si', 'No' => 'No'],
                                'options' => ['placeholder' => ''],
                                'id' => 'activo_select2',
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ]
                            ])
                        ],
                        'periodo_anterior_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'periodo_anterior_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => ''],
                                'id' => 'periodo_anterior_select2',
                                'pluginOptions' => [
                                    'data' => EmpresaPeriodoContable::getOtherPeriodos($model),
                                    'allowClear' => true,
                                ],
                                'disabled' => $disable_periodo_anterior,
                                'initValueText' => (isset($model->periodoAnterior)) ? "{$model->periodoAnterior->anho} - {$model->empresa->razon_social}" : '',
                            ])
                        ],
                    ],
                ],
                [
                    'attributes' => [
                        'descripcion' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '10'],
                        ],
                    ]
                ],
            ],
        ]);
        if ($disable_periodo_anterior)
            echo $form->field($model, 'periodo_anterior_id')->hiddenInput();
    } catch (Exception $e) {
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['data' => ['disabled-text' => 'Guardando...'], 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end();

    $script =
        <<<JS
    $("form#periodo_detalle_form").on("beforeSubmit", function(e) {
        var form = $(this);
        $.post(
            form.attr("action")+"&submit=true",
            form.serialize()
        )
        .done(function(result) {
            form.parent().html(result.message);
            $.pjax.reload({container:"#periodos_grid", async:false});
            $.pjax.reload({container:"#flash_message_id", async:false});
            $("#modal").modal("hide");      
            $("modal-body").empty();         
        });
        return false;
    }).on("submit", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });
JS;
    $this->registerJs($script);
    ?>


</div>
