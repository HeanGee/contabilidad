<?php

use yii\helpers\Html;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\daterange\DateRangePicker;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\grid\GridView;

use backend\models\Empresa;

use common\helpers\PermisosHelpers;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\EmpresaPeriodoContable */
/* @var $form yii\widgets\ActiveForm */
///* @var $searchModel backend\modules\contabilidad\models\search\EmpresaPeriodoContableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="form-y-grid-form">

    <?php
    $empresa = Empresa::find()->where(['id' => $model->empresa_id]);
    $titulo_form = '';
    $titulo_list = '';
    if ($empresa->exists()) {
        $razon_social = $empresa->one()->razon_social;
        $titulo_form = 'Crear o modificar periodo contable para '.$razon_social;
        $titulo_list = 'Peridos contables asociados a '.$razon_social;
    }
    else {
        $titulo_form = 'Crear un nuevo periodo contable para la nueva empresa';
    }
    ?>

    <h3><?= $titulo_form?></h3>

    <?= /** Formulario de creación de periodos */
    $this->render('_form', [
        'model'=>$model,
    ]); ?>

    <hr>

    <h3><?= $titulo_list?></h3>

    <?= /** Vista de todos los periodos contables asociados a la empresa */
    GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'anho',
            [
                'attribute' => 'anho',
                'value' => 'anhoDesc',
            ],
            [
                'attribute' => 'activo',
                'value' => 'activo'
            ],
        ]
    ]) ?>

</div>
