<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\EmpresaPeriodoContable */

$this->title = "Periodo Contable {$model->anho}";
$this->params['breadcrumbs'][] = ['label' => 'Empresa Periodo Contables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-periodo-contable-view">

    <p>
        <?php $permisos = [
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-empresa-periodo-cotable-index'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-empresa-periodo-cotable-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-empresa-periodo-cotable-delete'),
        ]; ?>
        <?php $permisos['index'] ? Html::a('Ir a Periodos C.', ['index'], ['class' => 'btn btn-info']) : null ?>
        <?php $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?php $permisos['delete'] ? Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Información General',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'id',
                [
                    'attribute' => 'empresa_id',
                    'value' => $model->empresa->razon_social,
                    'label' => 'Empresa',
                ],
                'anho',
                [
                    'attribute' => 'activo',
                    'format' => 'raw',
                    'value' => $model->activo == 'Si' ? '<span class="label label-success">Sí</span>' : '<span class="label label-success">No</span>',
                ],
                'descripcion',
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
        \common\helpers\FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    } ?>

</div>
