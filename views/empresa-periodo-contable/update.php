<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\EmpresaPeriodoContable */

$this->title = 'Modificar Empresa Periodo Contable: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Empresa Periodo Contables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="empresa-periodo-contable-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
