<?php
//
//use backend\modules\contabilidad\models\EmpresaPeriodoContable;
//use kartik\builder\Form;
//use kartik\select2\Select2;
//use yii\helpers\Url;
//use yii\web\JsExpression;
//
///* @var $this yii\web\View */
///* @var $form yii\widgets\ActiveForm */
//?>
<!---->
<!--<div class="set-pc-session-form">-->
<!---->
<!--    --><?php
//    class PeriodoSession extends \yii\base\Model {
//        public $pc_id;
//
//        public function rules()
//        {
//            return [
//                [['pc_id'], 'safe'],
//                [['pc_id'], 'required'],
//                [['pc_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaPeriodoContable::className(), 'targetAttribute' => ['pc_id' => 'id']],
//            ];
//        }
//
//        public function attributeLabels()
//        {
//            return [
//                'pc_id' => Yii::t('app', 'Periodo contable'),
//            ];
//        }
//    }
//
//    $model = new EmpresaPeriodoContable();
//    if(isset(\Yii::$app->session->get('core_empresa_actual_pc')))
//        $model = EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
//
//    try {
//        echo Form::widget([
//            'model' => $model,
//            'form' => $form,
//            'columns' => 2,
//            'attributes' => [
//                'id' => [
//                    'type' => Form::INPUT_WIDGET,
//                    'label' => 'Periodo Contable',
//                    'widgetClass' => Select2::class,
//                    'options' => [
//                        'initValueText' => !empty($model->id) ? $model->anho : '',
//                        'options' => ['id' => 'campo_periodo_actual', 'placeholder' => 'Seleccione un periodo contable ...'],
//                        'pluginOptions' => [
//                            'allowClear' => true,
//                            'ajax' => [
//                                'url' => Url::to(['contabilidad/empresa-periodo-contable/get-pc-lista']),
//                                'dataType' => 'json',
//                                'data' => new JsExpression("function(params) { return {q:params.term,id:$('#empresasession-empresa_id').val()}; }")
//                            ]
//                        ],
//                        'pluginEvents' => [
//                            'change' => "function(){
//                                if($('#campo_periodo_actual').val().length > 0)
//                                    $('#boton_submit').click();
//                            }"
//                        ],
//                    ]
//                ],
//            ]
//        ]);
//    } catch (Exception $e) {
//        echo $e;
//    }
//    ?>
<!---->
<!--</div>-->
