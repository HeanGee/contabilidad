<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 20/09/2018
 * Time: 11:23
 */

use backend\modules\contabilidad\models\Prestamo;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/* @var $form ActiveForm */
/* @var $model Prestamo */
/* @var $title string */
/* @var $table_id string */
/* @var $table_content array */

?>

<?php \yii\widgets\MaskedInputAsset::register($this); ?>

<fieldset>
    <!--    <legend>&nbsp;--><? //= $title ?>
    <!--    </legend>-->
    <?php

    echo '<table class="table table-condensed tabla-parametros">';
    //    echo '<thead>';
    //    echo '<tr>';
    //    echo '<th width="20%" style="text-align: center;">Para</th>';
    //    echo '<th width="20%" style="text-align: center;">Cuenta</th>';
    //    echo '<th width="20%" style="text-align: center;">Para</th>';
    //    echo '<th width="20%" style="text-align: center;">Cuenta</th>';
    //    //        echo '<th style="text-align: center;"></th>';
    //    echo '</tr>';
    //    echo '</thead>';
    echo '<tbody>';

    $cantColDeseada = 1;
    $columnasColocadas = 0;
    $anchoCol = round(100 / $cantColDeseada);
    foreach ($table_content as $index => $content) {
        if (($index + 1) % $cantColDeseada == 0 && $columnasColocadas == 0) {
            Yii::warning('Comienza row');
            echo '<tr>';
        }

        {
            echo '<td align="left" style="padding: 0 0 13px 13px"><strong>';
            {
                echo $content['label'];
            }
            echo '</strong></td>';
//
            echo '<td>';
            {
                try {
//                    echo \yii\helpers\Html::label($content['label'], $content['attribute']);
                    if ($content['input'] == 'select')
                        echo $content['form']->field($content['model'], $content['attribute'])->widget(Select2::className(), [
                            'options' => ['placeholder' => 'Seleccione...'],
                            'data' => $content['select_data'],
                            'pluginOptions' => [
                                'allowClear' => $content['allowClear'],
                                'data' => $content['select_data_inside_pOption'],
                                'width' => '100%'
                            ],
                            'initValueText' => $content['initValueText'],
                        ])->label(false);

                    elseif ($content['input'] == 'text')
                        echo $content['form']->field($content['model'], $content['attribute'])->textInput(['class' => $content['input_text_class']])->label(false);

                    elseif ($content['input'] == 'error') {
                        echo $content['form']->field($content['model'], $content['attribute'])->textInput(['disabled' => true, 'value' => $content['msg']])->label(false);
                    }

                } catch (Exception $exception) {
                    print $exception;
                }
            }
            echo '</td>';
        }

        ++$columnasColocadas;
        $_index = ($index + 1);
        Yii::warning("columna colocada: {$columnasColocadas}. Index: {$_index}");
        if (($index + 1) % $cantColDeseada == 0 && $columnasColocadas == $cantColDeseada) {
            echo '</tr>';
            $columnasColocadas = 0;
            Yii::warning("Fin de fila. Resetea col: {$columnasColocadas}");
        }
    }

    if ($columnasColocadas != 0) {
        Yii::warning("Se termino con cantidad de columnas distinto al deseado; se agrega tr/");
        echo '</tr>';
    }

    echo '</tr>';
    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);

    $css = <<<CSS
.table > tbody > tr > td {
    vertical-align: middle;
}
table.tabla-parametros {
    /*border: 1px solid black;*/
    table-layout: fixed;
    width: 100%;
}

table.tabla-parametros th, td {
    /*border: 1px solid black;*/
    overflow: hidden;
    width: {$anchoCol}px;
}
CSS;
    $this->registerCss($css);

    $script = <<<JS
function applyInputMaskWidget() {
    $(':input.decimal').inputmask({
        "alias": "numeric",
        "digits": 2,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true
    });
}

$(document).ready(function() {
    applyInputMaskWidget();
});
JS;
    $this->registerJs($script);

    ?>

</fieldset>
