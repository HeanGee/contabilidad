<?php

use yii\helpers\Html;

use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\daterange\DateRangePicker;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\EmpresaPeriodoContable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-periodo-contable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    try {
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'anho' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'anho')->widget(DatePicker::className(), [
                                'options' => [
                                    'id' => 'campo_periodo_contable',
                                ],
                                'language' => 'es',
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'autoApply' => true,
                                    'allowClear' => true,
                                    'startView' => 'year',
                                    'minViewMode' => 'years',
                                    'format' => 'yyyy',
                                ],
                                'pluginEvents' => [],
                            ]),
                        ],
//                        'activo' => [
//                            'type' => Form::INPUT_RAW,
//                            'value' => $form->field($model, 'activo')->widget(SwitchInput::classname(), [
//                                'value' => ($model->activo == 'Si') ? true : false,
//                                'pluginOptions' => [
//                                    'onText' => 'Si',
//                                    'offText' => 'No',
//                                    'offColor' => 'danger',
//                                    'size' => 'small',
//                                    'handleWidth' => '40',
//                                    'labelWidth' => '40',
//                                ]
//                            ]),
//                            'widgetClass' => \kartik\switchinput\SwitchInput::className(),
//                            'options' => [
//                                'pluginOptions' => [
//                                    'onText' => 'Si',
//                                    'offText' => 'No',
//                                    'offColor' => 'danger',
//                                    'size' => 'small',
//                                    'handleWidth' => '40',
//                                    'labelWidth' => '40',
//                                ]
//                            ]
//                        ],
                        'descripcion' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '10'],
                        ],
                    ],
                ],
                [
                    'attributes' => [
                        'activo' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => SwitchInput::className(),
                            'options' => [
                                'pluginOptions' => [
                                    'onText' => 'Si',
                                    'offText' => 'No',
                                    'offColor' => 'danger',
                                    'size' => 'small',
                                    'handleWidth' => '40',
                                    'labelWidth' => '40',
                                ]
                            ]
                        ],
                    ]
                ],
                [
                    'attributes' => [
                        'action' => [
                            'type' => Form::INPUT_RAW,
                            'value' => '<div style="margin-top: 20px">' .
                                Html::submitButton('Guardar', ['class' => 'btn btn-primary']) .
                                '</div>'
                        ],
                    ],
                ]
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
