<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 20/04/2018
 * Time: 8:27
 */

use backend\models\Empresa;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\Rubro;
use backend\modules\contabilidad\models\Timbrado;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\TipoDocumentoSet;
use kartik\dialog\Dialog;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $model \backend\models\Empresa */

?>

<?php

$periodosProvider = new ActiveDataProvider([
    'query' => EmpresaPeriodoContable::find()->where("empresa_id = '{$model->id}'")->indexBy('id'),
    'pagination' => [
        'pageSize' => 20,
    ],
]);

try {
    $table_content = [];

    if (\Yii::$app->session->has('core_empresa_actual') && \Yii::$app->session->has('core_empresa_actual_pc') && \Yii::$app->session->get('core_empresa_actual') == $model->id) {
        {
            // PARA FACTURAS

            $table_content = [];

            // Motivo de no vigencia
            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-motivo_no_vigencia_venta";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $model->salario_minimo = $parametro_sistema->valor;
            } else {
                $model->salario_minimo = '';
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'motivo_no_vigencia_venta',
//            'input' => 'masked_input:numeric',
                'input' => 'text',
                'input_text_class' => 'text',
                'label' => "{$model->getAttributeLabel('motivo_no_vigencia_venta')}",
            ];

            #Ventas cargar en paralelo
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'permitir_carga_venta_paralelo',
                'input' => 'select',
                'allowClear' => false,
                'select_data' => ['si' => 'Si', 'no' => 'No'],
                'select_data_inside_pOption' => null,
                'initValueText' => null,
                'label' => $model->getAttributeLabel('permitir_carga_venta_paralelo'),
            ];

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-plantilla_seguro_a_devengar_id";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $plantilla = PlantillaCompraventa::findOne($parametro_sistema->valor);
                if (isset($plantilla))
                    $model->plantilla_seguro_a_devengar_id = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'plantilla_seguro_a_devengar_id',
                'input' => 'select',
                'select_data' => null,
                'allowClear' => true,
                'select_data_inside_pOption' => PlantillaCompraventa::find()->select(['id', 'text' => 'nombre'])->where(['estado' => 'activo'])
                    ->orderBy('nombre')->asArray()->all(),
                'initValueText' => ($model->plantilla_seguro_a_devengar_id != '') ?
                    (PlantillaCompraventa::findOne($model->plantilla_seguro_a_devengar_id)->nombre) : '',
                'label' => $model->getAttributeLabel('plantilla_seguro_a_devengar_id'),
            ];

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_seguros_pagados_id";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_seguros_pagados_id = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_seguros_pagados_id',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_seguros_pagados_id)) ?
                    (PlanCuenta::findOne($model->cuenta_seguros_pagados_id)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_seguros_pagados_id'),
            ];

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_resultado_diff_cambio_debe = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_resultado_diff_cambio_debe',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cta_resultado_diff_cambio_debe)) ?
                    (PlanCuenta::findOne($model->cta_resultado_diff_cambio_debe)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_resultado_diff_cambio_debe'),
            ];

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_resultado_diff_cambio_haber = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_resultado_diff_cambio_haber',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_resultado_diff_cambio_haber)) ?
                    (PlanCuenta::findOne($model->cta_resultado_diff_cambio_haber)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_resultado_diff_cambio_haber'),
            ];

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_error_redondeo_perdida";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_error_redondeo_perdida = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_error_redondeo_perdida',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cuenta_error_redondeo_perdida)) ?
                    (PlanCuenta::findOne($model->cuenta_error_redondeo_perdida)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_error_redondeo_perdida'),
            ];

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_error_redondeo_ganancia";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_error_redondeo_ganancia = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_error_redondeo_ganancia',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cuenta_error_redondeo_ganancia)) ?
                    (PlanCuenta::findOne($model->cuenta_error_redondeo_ganancia)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_error_redondeo_ganancia'),
            ];

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-coeficiente_costo";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $model->coeficiente_costo = $parametro_sistema->valor;
            } else {
                $model->coeficiente_costo = 0;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'coeficiente_costo',
//            'input' => 'masked_input:numeric',
                'input' => 'text',
                'input_text_class' => 'decimal',
                'label' => "{$model->getAttributeLabel('coeficiente_costo')}",
            ];

            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-id_tipo_documento_autofactura";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $document = TipoDocumento::findOne($parametro_sistema->valor);
                if (isset($document))
                    $model->id_tipo_documento_autofactura = $parametro_sistema->valor;
            }

            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'id_tipo_documento_autofactura',
                'input' => 'select',
                'select_data' => null,
                'allowClear' => true,
                'select_data_inside_pOption' => TipoDocumento::getTiposDocumentoConId('proveedor'),
                'initValueText' => (isset($model->id_tipo_documento_autofactura) && $model->id_tipo_documento_autofactura != '') ?
                    (TipoDocumento::findOne($model->id_tipo_documento_autofactura)->nombre) : '',
                'label' => $model->getAttributeLabel('id_tipo_documento_autofactura'),
            ];

            // Salario Minimo
            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-salario_minimo";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $model->salario_minimo = $parametro_sistema->valor;
            } else {
                $model->salario_minimo = '';
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'salario_minimo',
//            'input' => 'masked_input:numeric',
                'input' => 'text',
                'input_text_class' => 'decimal',
                'label' => "{$model->getAttributeLabel('salario_minimo')}",
            ];

            // Salario Minimos para autofacturas
//            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
//            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cantidad_salario_min";
//            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
//            if (isset($parametro_sistema)) {
//                $model->cantidad_salario_min = $parametro_sistema->valor;
//            } else {
//                $model->cantidad_salario_min = '';
//            }
//            $table_content[] = [
//                'form' => $form,
//                'model' => $model,
//                'attribute' => 'cantidad_salario_min',
////            'input' => 'masked_input:numeric',
//                'input' => 'text',
//                'input_text_class' => 'decimal',
//                'label' => "{$model->getAttributeLabel('cantidad_salario_min')}",
//            ];

            echo '<fieldset>';
            {
                echo '<legend class="text-info"><small>Facturas</small></legend>';
                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
            }
            echo '</fieldset>';
        }

        {
            // PARA RETENCIONES
            $empresa = Empresa::findOne(\Yii::$app->session->get('core_empresa_actual'));
            $entidad = Entidad::findOne(['ruc' => $empresa->ruc]);

            if (!isset($entidad)) {
                echo '<fieldset>';
                {
                    $table_content = [];
                    $table_content[] = [
                        'form' => $form,
                        'model' => $model,
                        'attribute' => 'tipodoc_set_retencion_id',
                        'input' => 'error',
                        'label' => "<span class='btn btn-danger'>No se puede establecer {$model->getAttributeLabel('tipodoc_set_retencion_id')}</span>",
                        'msg' => " << FALTA CREAR ENTIDAD Y ASOCIAR A LA EMPRESA ACTUAL >>",
                    ];
                    echo '<legend class="text-info"><small>Retenciones</small></legend>';
                    echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
                }
                echo '</fieldset>';
            } else {

                $table_content = [];
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-tipodoc_set_retencion";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $tipodoc_set = TipoDocumentoSet::findOne($parametro_sistema->valor);
                    if (isset($tipodoc_set))
                        $model->tipodoc_set_retencion_id = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'tipodoc_set_retencion_id',
                    'input' => 'select',
                    'select_data' => null,
                    'allowClear' => true,
                    'select_data_inside_pOption' => TipoDocumentoSet::getLista(true),
                    'initValueText' => (isset($model->tipodoc_set_retencion_id)) ?
                        (TipoDocumentoSet::findOne($model->tipodoc_set_retencion_id)->nombre) : '',
                    'label' => $model->getAttributeLabel('tipodoc_set_retencion_id'),
                ];

                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-timbrado_para_retenciones";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $model->timbrado_para_retenciones = $parametro_sistema->valor;
                }
                $timbrado = isset($model->timbrado_para_retenciones) ? Timbrado::findOne($model->timbrado_para_retenciones) : null;
                $initValueText = isset($timbrado) ? "{$timbrado->nro_timbrado} ({$timbrado->entidad->ruc} - {$timbrado->entidad->razon_social})" : "";

                $query = Timbrado::find()->alias('tim')
                    ->leftJoin('cont_timbrado_detalle as det', 'tim.id = det.timbrado_id')
                    ->where(['entidad_id' => $entidad->id])
                    ->andFilterWhere(['det.tipo_documento_set_id' => $model->tipodoc_set_retencion_id])
                    ->select(['tim.id', 'nro_timbrado'])
                    ->orderBy(['nro_timbrado' => SORT_ASC]);
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'timbrado_para_retenciones',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map($query->asArray()->all(), 'id', 'nro_timbrado'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => $initValueText,
                    'label' => $model->getAttributeLabel('timbrado_para_retenciones'),
                ];

                #cuenta retenciones emitidas
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_retenciones_emitidas";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                    if (isset($cuenta))
                        $model->cuenta_retenciones_emitidas = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'cuenta_retenciones_emitidas',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => ($model->cuenta_retenciones_emitidas != '') ?
                        (PlanCuenta::findOne($model->cuenta_retenciones_emitidas)->nombre) : '',
                    'label' => $model->getAttributeLabel('cuenta_retenciones_emitidas'),
                ];

                #cuenta retenciones recibidas
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_retenciones_recibidas";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                    if (isset($cuenta))
                        $model->cuenta_retenciones_recibidas = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'cuenta_retenciones_recibidas',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => ($model->cuenta_retenciones_recibidas != '') ?
                        (PlanCuenta::findOne($model->cuenta_retenciones_recibidas)->nombre) : '',
                    'label' => $model->getAttributeLabel('cuenta_retenciones_recibidas'),
                ];

                #cuenta retenciones emitidas renta
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_retenciones_renta_emitidas";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                    if (isset($cuenta))
                        $model->cuenta_retenciones_renta_emitidas = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'cuenta_retenciones_renta_emitidas',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => ($model->cuenta_retenciones_renta_emitidas != '') ?
                        (PlanCuenta::findOne($model->cuenta_retenciones_renta_emitidas)->nombre) : '',
                    'label' => $model->getAttributeLabel('cuenta_retenciones_renta_emitidas'),
                ];

                #cuenta retenciones recibidas renta
                $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_retenciones_renta_recibidas";
                $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
                if (isset($parametro_sistema)) {
                    $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                    if (isset($cuenta))
                        $model->cuenta_retenciones_renta_recibidas = $parametro_sistema->valor;
                }
                $table_content[] = [
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'cuenta_retenciones_renta_recibidas',
                    'input' => 'select',
                    'allowClear' => true,
                    'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                    'select_data_inside_pOption' => null,
                    'initValueText' => ($model->cuenta_retenciones_renta_recibidas != '') ?
                        (PlanCuenta::findOne($model->cuenta_retenciones_renta_recibidas)->nombre) : '',
                    'label' => $model->getAttributeLabel('cuenta_retenciones_renta_recibidas'),
                ];

                echo '<fieldset>';
                {
                    echo '<legend class="text-info"><small>Retenciones</small></legend>';
                    echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
                }
                echo '</fieldset>';
            }
        }

        {
            // PARA ACTIVOS FIJOS

            $table_content = [];
            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-criterio_revaluo";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $model->criterio_revaluo = $parametro_sistema->valor;
            }
            $data = [
                Empresa::CRITERIO_REVALUO_PERIODO_SIGUIENTE => "1. A partir del sigte. ejercicio fiscal",
                Empresa::CRITERIO_REVALUO_MES_SIGUIENTE => "2. A partir del mes sigte. a su adquisición"
            ];
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'criterio_revaluo',
                'input' => 'select',
                'select_data' => $data,
                'allowClear' => true,
                'select_data_inside_pOption' => null,
                'initValueText' => ($model->criterio_revaluo != '') ?
                    $data[$model->criterio_revaluo] : '',
                'label' => $model->getAttributeLabel('criterio_revaluo'),
            ];

            echo '<fieldset>';
            {
                echo '<legend class="text-info"><small>Activos Fijos</small></legend>';
                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
            }
            echo '</fieldset>';
        }

//        {
//            // PARA IMPORTACIONES
//
//            $table_content = [];
//            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_pago_salario_id";
//            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
//            if (isset($parametro_sistema)) {
//                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
//                if (isset($cuenta))
//                    $model->cuenta_pago_salario_id = $parametro_sistema->valor;
//            }
//            $table_content[] = [
//                'form' => $form,
//                'model' => $model,
//                'attribute' => 'cuenta_pago_salario_id',
//                'input' => 'select',
//                'allowClear' => true,
//                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
//                'select_data_inside_pOption' => null,
//                'initValueText' => (isset($model->cuenta_pago_salario_id)) ?
//                    (PlanCuenta::findOne($model->cuenta_pago_salario_id)->nombre) : '',
//                'label' => $model->getAttributeLabel('cuenta_pago_salario_id'),
//            ];
//
//            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cuenta_aporte_ips_id";
//            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
//            if (isset($parametro_sistema)) {
//                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
//                if (isset($cuenta))
//                    $model->cuenta_aporte_ips_id = $parametro_sistema->valor;
//            }
//            $table_content[] = [
//                'form' => $form,
//                'model' => $model,
//                'attribute' => 'cuenta_aporte_ips_id',
//                'input' => 'select',
//                'allowClear' => true,
//                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
//                'select_data_inside_pOption' => null,
//                'initValueText' => (isset($model->cuenta_aporte_ips_id)) ?
//                    (PlanCuenta::findOne($model->cuenta_aporte_ips_id)->nombre) : '',
//                'label' => $model->getAttributeLabel('cuenta_aporte_ips_id'),
//            ];
//
//            echo '<fieldset>';
//            {
//                echo '<legend class="text-info"><small>Importaciones</small></legend>';
//                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
//            }
//            echo '</fieldset>';
//        }

        {
            // PARA CUENTAS DE CIERRE DE INVENTARIO DE A.BIO
            // José sugirió que sea solo por empresa sin periodo el 6 de diciembre 2018.

            $table_content = [];
            $nombre = "core_empresa-{$model->id}-cuenta_ganado";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_ganado = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_ganado',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_ganado) && $model->cuenta_ganado != "") ?
                    (PlanCuenta::findOne($model->cuenta_ganado)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_ganado'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_costo_venta_gnd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_costo_venta_gnd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_costo_venta_gnd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_costo_venta_gnd) && $model->cuenta_costo_venta_gnd != "") ?
                    (PlanCuenta::findOne($model->cuenta_costo_venta_gnd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_costo_venta_gnd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_costo_venta_gd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_costo_venta_gd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_costo_venta_gd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_costo_venta_gd) && $model->cuenta_costo_venta_gd != "") ?
                    (PlanCuenta::findOne($model->cuenta_costo_venta_gd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_costo_venta_gd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_procreo";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_procreo = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_procreo',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_procreo) && $model->cuenta_procreo != "") ?
                    (PlanCuenta::findOne($model->cuenta_procreo)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_procreo'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_mortandad_gnd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_mortandad_gnd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_mortandad_gnd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_mortandad_gnd) && $model->cuenta_mortandad_gnd != '') ?
                    (PlanCuenta::findOne($model->cuenta_mortandad_gnd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_mortandad_gnd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_mortandad_gd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_mortandad_gd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_mortandad_gd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_mortandad_gd) && $model->cuenta_mortandad_gd != '') ?
                    (PlanCuenta::findOne($model->cuenta_mortandad_gd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_mortandad_gd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_consumo_gnd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_consumo_gnd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_consumo_gnd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_consumo_gnd) && $model->cuenta_consumo_gnd != '') ?
                    (PlanCuenta::findOne($model->cuenta_consumo_gnd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_consumo_gnd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_consumo_gd";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_consumo_gd = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_consumo_gd',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_consumo_gd) && $model->cuenta_consumo_gd != '') ?
                    (PlanCuenta::findOne($model->cuenta_consumo_gd)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_consumo_gd'),
            ];

            $nombre = "core_empresa-{$model->id}-cuenta_valorizacion_hacienda";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cuenta_valorizacion_hacienda = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cuenta_valorizacion_hacienda',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (isset($model->cuenta_valorizacion_hacienda) && $model->cuenta_valorizacion_hacienda != '') ?
                    (PlanCuenta::findOne($model->cuenta_valorizacion_hacienda)->nombre) : '',
                'label' => $model->getAttributeLabel('cuenta_valorizacion_hacienda'),
            ];

            echo '<fieldset>';
            {
                echo '<legend class="text-info"><small>Activo Biológico - Cierre de Inventario</small></legend>';
                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
            }
            echo '</fieldset>';
        }

        {
            //IMPORTACIONES
            $table_content = [];
            $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_retencion_renta_debe";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_retencion_renta_debe = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_retencion_renta_debe',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_retencion_renta_debe)) ?
                    (PlanCuenta::findOne($model->cta_importacion_retencion_renta_debe)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_retencion_renta_debe'),
            ];

            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_retencion_renta_haber";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_retencion_renta_haber = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_retencion_renta_haber',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_retencion_renta_haber)) ?
                    (PlanCuenta::findOne($model->cta_importacion_retencion_renta_haber)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_retencion_renta_haber'),
            ];

            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_retencion_iva_debe";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_retencion_iva_debe = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_retencion_iva_debe',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_retencion_iva_debe)) ?
                    (PlanCuenta::findOne($model->cta_importacion_retencion_iva_debe)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_retencion_iva_debe'),
            ];

            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_retencion_iva_haber";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_retencion_iva_haber = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_retencion_iva_haber',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_retencion_iva_haber)) ?
                    (PlanCuenta::findOne($model->cta_importacion_retencion_iva_haber)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_retencion_iva_haber'),
            ];

            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_gnd_multa";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_gnd_multa = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_gnd_multa',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_gnd_multa)) ?
                    (PlanCuenta::findOne($model->cta_importacion_gnd_multa)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_gnd_multa'),
            ];

            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_mercaderias_importadas";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_mercaderias_importadas = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_mercaderias_importadas',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_mercaderias_importadas)) ?
                    (PlanCuenta::findOne($model->cta_importacion_mercaderias_importadas)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_mercaderias_importadas'),
            ];

            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_iva_10";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_iva_10 = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_iva_10',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_iva_10)) ?
                    (PlanCuenta::findOne($model->cta_importacion_iva_10)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_iva_10'),
            ];

            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_prov_exterior";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_prov_exterior = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_prov_exterior',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_prov_exterior)) ?
                    (PlanCuenta::findOne($model->cta_importacion_prov_exterior)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_prov_exterior'),
            ];

            $nombre = "core_empresa-{$model->id}-periodo-{$periodo_id}-cta_importacion_haber";
            $parametro_sistema = ParametroSistema::findOne(['nombre' => $nombre]);
            if (isset($parametro_sistema)) {
                $cuenta = PlanCuenta::findOne($parametro_sistema->valor);
                if (isset($cuenta))
                    $model->cta_importacion_haber = $parametro_sistema->valor;
            }
            $table_content[] = [
                'form' => $form,
                'model' => $model,
                'attribute' => 'cta_importacion_haber',
                'input' => 'select',
                'allowClear' => true,
                'select_data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                'select_data_inside_pOption' => null,
                'initValueText' => (!empty($model->cta_importacion_haber)) ?
                    (PlanCuenta::findOne($model->cta_importacion_haber)->nombre) : '',
                'label' => $model->getAttributeLabel('cta_importacion_haber'),
            ];

            echo '<fieldset>';
            {
                echo '<legend class="text-info"><small>Importación</small></legend>';
                echo $this->render('_form_parametros', ['title' => 'Parámetros', 'table_content' => $table_content]);
            }
            echo '</fieldset>';
        }
    }

    echo GridView::widget([
        'dataProvider' => $periodosProvider,
        //    'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'id' => 'periodos_grid'
            ],
            'loadingCssClass' => false,
        ],
        'panel' => [
            'type' => 'primary',
            'heading' => 'Periodos contables',
            'after' => '<em>* Para confirmar los datos de los periodos se debe guardar la empresa.</em>'
        ],
        'striped' => true,
        'hover' => true,

        'columns' => [
            ['class' => \kartik\grid\SerialColumn::className()],

            'id',
            'anho',
            'activo',
            'descripcion',
            [
                'attribute' => 'periodo_anterior_id',
                'value' => function ($model) {
                    /** @var $model EmpresaPeriodoContable */
                    if ($model->periodo_anterior_id != '')
                        return "(ID: {$model->periodo_anterior_id}) - {$model->periodoAnterior->anho}";
                    return '';
                }
            ],

            [
                'class' => \kartik\grid\ActionColumn::className(),
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::button('<i class="glyphicon glyphicon-pencil"></i>', [
                            'type' => 'button',
                            'title' => Yii::t('app', 'Editar'),
                            'class' => 'tiene_modal btn-sin-estilo',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'data-url' => Url::to(['contabilidad/empresa-periodo-contable/update-periodo-empresa', 'periodo_id' => $model->id]),
                            'data-pjax' => '0',
                            'data-title' => "Editar"]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            Html::tag('i', '', ['class' => 'glyphicon glyphicon-trash', 'title' => 'Eliminar Periodo contable']),
                            Url::to(['/contabilidad/empresa-periodo-contable/delete-periodo-empresa', 'id' => $model->id]),
                            [
                                'class' => 'empresa-periodo-contable-delete-action',
                                'data' => [
                                    'pjax' => 0
                                ]
                            ]);
                    },
                ]
            ],
        ],
        'toolbar' => [
            [
                'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                    'id' => 'popup_nuevo_periodo-contable_id',
                    'type' => 'button',
                    'title' => Yii::t('app', 'Agregar Periodo contable'),
                    'class' => 'btn btn-success tiene_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'data-url' => Url::to(['/contabilidad/empresa-periodo-contable/add-periodo-contable', 'empresa_id' => $model->id]),
                    //                'data-url' => Url::to(['@backend/modules/contabilidad/empresa-periodo-contable/add-periodo-contable']),
                    //                'data-url' => Url::to(['empresa/add-periodo-contable']),
                    'data-pjax' => '0',
                    'data-title' => 'Agregar Periodo contable'])
            ],
        ],
    ]);

    try {
        $arraySelect = [
            'id' => "concat(obligacion.id,if(sub.id is not null, concat('_', sub.id), ''))",
            'nombre' => 'if(sub.nombre is not null, sub.nombre, obligacion.nombre)',
            'descripcion' => 'if(sub.descripcion is not null, sub.descripcion, obligacion.descripcion)',
        ];

        $query = new \yii\db\Query();
        $query->select($arraySelect)
            ->from('cont_obligacion as obligacion')->leftJoin('cont_sub_obligacion sub', 'sub.obligacion_id = obligacion.id');


        $query = (new Query())
            ->select(['id', 'nombre', 'descripcion'])
            ->from('cont_obligacion')
            ->where(['estado' => 'activo']);
//            ->union(
//                (new Query())
//                    ->select([
//                        'id' => "concat(obligacion.id,if(sub.id is not null, concat('_', sub.id), ''))",
//                        'nombre' => "if(sub.nombre is not null, CONCAT(sub.nombre, '(de la obligación ', obligacion.nombre, ')'), obligacion.nombre)",
//                        'descripcion' => 'if(sub.descripcion is not null, sub.descripcion, obligacion.descripcion)',
//                    ])
//                    ->from('cont_sub_obligacion sub')
//                    ->leftJoin('cont_obligacion obligacion', 'obligacion.id = sub.obligacion_id')
//                    ->where(['sub.estado' => "activo", 'obligacion.estado' => 'activo'])
//                    ->createCommand()->getRawSql()
//            );

        $empresaObligacionesDataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    $empresaRubrosDataProvider = new ActiveDataProvider([
        'query' => Rubro::find(),
    ]);
    echo $this->render('@backend/modules/contabilidad/views/empresa-obligacion/_form_empresa_obligaciones', [
        'dataProvider' => $empresaObligacionesDataProvider,
    ]);
    echo $this->render('@backend/modules/contabilidad/views/empresa-rubro/_form_empresa_rubros', [
        'dataProvider' => $empresaRubrosDataProvider,
    ]);

    if (\Yii::$app->session->has('core_empresa_actual') && \Yii::$app->session->has('core_empresa_actual_pc') && \Yii::$app->session->get('core_empresa_actual') == $model->id)
        echo $this->render('@backend/modules/contabilidad/views/plan-cuenta/empresa-plan-cuenta/_grid_cuentas_prestamo', []);

} catch (Exception $e) {
    echo $e;
}
?>

<?php

echo Dialog::widget();

$script =
    <<<JS
    $(document).on('click', '.tiene_modal', (function () {
        var boton = $(this);
        var title = boton.data('title');
        $.get(
            boton.data('url'),
            function (data) {
                var modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                if (title)
                    $('.modal-title', modal).html(title);
            }
        );
    }));
    $(document).on('click', 'a.empresa-periodo-contable-delete-action', function (e) {
        var delete_btn = $(this);
        krajeeDialog.confirm("¿Está seguro de eliminar !!!?", function (result) {
            if (result) {
                $.post(
                    delete_btn.attr('href')
                )
                    .done(function (result) {
                        $.pjax.reload({container: "#periodos_grid", async: false});
                        $.pjax.reload({container: "#flash_message_id", async: false});
                    });
            }
        });
        e.preventDefault();
        e.stopImmediatePropagation();
        return false;
    });

    $(document).on('click', '.modal_cuentas_prestamo', (function () {
        var boton = $(this);
        var title = boton.data('title');
        $.get(
            boton.data('url'),
            function (data) {
                var modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                if (title)
                    $('.modal-title', modal).html(title);
            }
        );
    }));
    
    // Operaciones al borrar un detalle venta.
    $(document).on('click', '.delete-cuenta-prestamo', function (e) {
        e.preventDefault();
        var deleteUrl = $(this).attr('delete-url');
        krajeeDialog.confirm('Está seguro?',
            function (result) {
                if (result) {
                    $.ajax({
                        url: deleteUrl,
                        type: 'post',
                        error: function (xhr, status, error) {
                            alert('There was an error with your request.' + xhr.responseText);
                        }
                    }).done(function (data) {
                        $.pjax.reload({container: "#grid_cuentas_prestamo", async: false});
                    });
                }
            }
        );
    });
JS;

$this->registerJs($script);

$css = "
        .btn-sin-estilo {
            border: 0;
            padding: 0 1px;
            background-color: inherit;
            color: #337ab7;
        }
        .btn-sin-estilo:hover, 
        .btn-sin-estilo:active, 
        .btn-sin-estilo:focus { color: #72afd2; }
        ";
$this->registerCss($css);

?>