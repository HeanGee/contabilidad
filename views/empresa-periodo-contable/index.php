<?php

use common\helpers\PermisosHelpers;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\EmpresaPeriodoContableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Periodos contables';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="empresa-periodo-contable-index">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?php if (PermisosHelpers::getAcceso('contabilidad-empresa-periodo-contable-create')) {
                echo Html::a('Crear periodo contable', ['create'], ['class' => 'btn btn-success']);
            } ?>
        </p>

        <?php
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (PermisosHelpers::getAcceso("contabilidad-entidad-{$_v}"))
                $template .= "&nbsp{{$_v}}";
        ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//            'id',
                'empresa_id', // No necesario
                [
                    'attribute' => 'anho',
                    'value' => 'anhoDesc',
                ],
                'descripcion',
                [
                    'attribute' => 'activo',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->activo == 'Si') ? 'success">Sí' : 'danger">No') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'activo',
                        'initValueText' => 'Si',
                        'data' => ['Si' => 'Si', 'No' => 'No', 'Todos' => 'Todos',],
                        'hideSearch' => true,
                        'pluginOptions' => [
//                            'width' => '120px'
                        ]
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [],
                    'template' => $template,
                ],
            ],
        ]); ?>
    </div>

<?php

$CSS = <<<CSS

.kv-grid-table tbody tr:hover {
    background-color: #ebffdd;
}

.table tbody tr:hover {
    background-color: #ebffdd;
}

CSS;

$this->registerCss($CSS);

?>