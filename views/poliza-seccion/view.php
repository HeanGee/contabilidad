<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PolizaSeccion */

$this->title = 'Sección ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Secciones para Póliza', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poliza-seccion-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-seccion-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-seccion-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-seccion-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-seccion-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-poliza-seccion-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Secciones', ['index'/*, 'operacion' => Yii::$app->request->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Realmente desea borrar esta sección?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'nombre',
                [
                    'attribute' => 'estado',
                    'value' => ucfirst($model->estado),
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>

</div>
