<?php

use common\helpers\PermisosHelpers;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\PolizaSeccionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Secciones para Póliza.';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poliza-seccion-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-poliza-seccion-index') ?
            Html::a('Ir a Pólizas', ['poliza/index'], ['class' => 'btn btn-info pull-right']) : null ?>
        <?= PermisosHelpers::getAcceso('contabilidad-poliza-seccion-create') ?
            Html::a('Registrar nueva Sección', ['create'], ['class' => 'btn btn-success']) : "" ?>
    </p>

    <?php try {
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (PermisosHelpers::getAcceso("poliza-seccion-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'nombre',
                [
                    'attribute' => 'estado',
                    'value' => function ($model) {
                        return ucfirst($model->estado);
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => ['activo' => "Activo", 'inactivo' => "Inactiovo"],
                        'hideSearch' => true,
                        'pluginOptions' => [],
                    ]),
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    } ?>
</div>
