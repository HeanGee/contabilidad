<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PolizaSeccion */

$this->title = 'Modificar Sección ' . ucfirst($model->nombre);
$this->params['breadcrumbs'][] = ['label' => 'Secciones para Póliza', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="poliza-seccion-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
