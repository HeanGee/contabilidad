<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PolizaSeccion */

$this->title = 'Crear nueva Sección';
$this->params['breadcrumbs'][] = ['label' => 'Secciones para Póliza', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poliza-seccion-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
