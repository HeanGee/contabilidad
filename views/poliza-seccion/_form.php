<?php

use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PolizaSeccion */
/* @var $form \kartik\form\ActiveForm */
?>

<div class="poliza-seccion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $action_id = Yii::$app->controller->action->id;
    try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 4,
            'attributes' => [
                'nombre' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Nombre debe ser único...'],
                ],
                'estado' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => ['activo' => 'Activo', 'inactivo' => "Inactivo"],
                        'options' => [
                            'placeholder' => 'Seleccione una sección...',
                        ],
                        'value' => isset($model->estado) ? $model->estado : 'activo',
                    ],
                ],
            ],
        ]);
    } catch (Exception $exception) {
        throw $exception;
    }
    $btn_class = Yii::$app->controller->action->id == 'create' ? 'btn btn-success' : 'btn btn-primary';
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar y siguiente', ['class' => $btn_class]) ?>
        <?php if (Yii::$app->controller->action->id == 'create') {
            echo Html::a('Guardar y Cerrar', ['create', 'guardar_cerrar' => true], [
                'class' => 'btn btn-primary',
                'data' => [
                    'method' => 'post',
                ],
            ]);
        } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$scripts = <<<JS
if (("$action_id") === "create") {
    $('#polizaseccion-nombre').focus();
}
JS;
$this->registerJs($scripts);
?>
