<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 3/13/18
 * Time: 5:58 PM
 */


use backend\models\Empresa; ?>
<h3 class="reporte-titulo-h3"><u>Libro <?= $tipoLibro ?> por Cuentas</u></h3>
<table class="table-header table-condensed">
    <tr>
        <td class="td-col1">DEL:</td>
        <td class="td-col2"><?= $desde ?></td>
        <?php
        if (isset($id_desde) && $id_desde != '') : ?>
            <td class='td-col'>Código desde:</td>
            <td class='td-col' style="text-align: left"><?= $id_desde ?></td>
        <?php else: ?>
            <td class='td-col'></td>
            <td class='td-col'></td>
        <?php endif; ?>
        <td class="td-col3">
            <?php
            $razon_social = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->razon_social;
            echo $razon_social;
            ?>
        </td>
    </tr>
    <tr>
        <td class="td-col1">AL:</td>
        <td class="td-col2"><?= $hasta ?></td>
        <?php
        if (isset($id_hasta) && $id_hasta != '') : ?>
            <td class='td-col'>Código hasta:</td>
            <td class='td-col' style="text-align: left"><?= $id_hasta ?></td>
        <?php else: ?>
            <td class='td-col'></td>
            <td class='td-col'></td>
        <?php endif; ?>
        <td class="td-col3">
            <?php
            $ruc = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->ruc;
            $dv = Empresa::getDigitoVerificador($ruc);
            echo "{$ruc}-{$dv}"
            ?>
        </td>
    </tr>
</table>

<table class="tg" style="undefined;table-layout: fixed; width: 1564px">
    <tr>
        <th class="tg-88nc" style="background-color: lightgrey; width: 160.003906px">CODIGO</th>
        <th class="tg-88nc" style="background-color: lightgrey; width: 500.003906px">CUENTA</th>
        <th class="tg-88nc" style="background-color: lightgrey; width: 156.003906px">EXENTAS</th>
        <th class="tg-88nc" style="background-color: lightgrey; width: 156.003906px">GRAV 10%</th>
        <th class="tg-88nc" style="background-color: lightgrey; width: 156.003906px">GRAV 5%</th>
        <th class="tg-88nc" style="background-color: lightgrey; width: 156.003906px">IVA 10%</th>
        <th class="tg-88nc" style="background-color: lightgrey; width: 156.003906px">IVA 5%</th>
        <th class="tg-88nc" style="background-color: lightgrey; width: 156.003906px">BASE IMP.</th>
        <th class="tg-88nc" style="background-color: lightgrey; width: 156.003906px">TOTAL</th>
    </tr>
</table>