<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 2/3/18
 * Time: 5:15 PM
 */

?>

<table class="tg" style="undefined;table-layout: fixed; width: 1564px">
    <tbody>
    <?php
    $cantTotal = 0;
    $cant = 0;
    $gravadas5 = 0;
    $gravadas10 = 0;
    $impuestos5 = 0;
    $impuestos10 = 0;
    $exentas = 0;
    $totales = 0;
    $base_imps = 0;
    $cuenta_contable = '';
    $cuenta_contable_n = '';
    $cta_contable_actual = '';

    foreach ($dataProvider as $index => $data) {

        $cta_contable_actual = $data['codigo_completo'];

        if ($cuenta_contable != $cta_contable_actual && $cuenta_contable != '') {
            $gravadas10 = number_format($gravadas10, 0, ',', '.');
            $gravadas5 = number_format($gravadas5, 0, ',', '.');
            $impuestos10 = number_format($impuestos10, 0, ',', '.');
            $impuestos5 = number_format($impuestos5, 0, ',', '.');
            $exentas = number_format($exentas, 0, ',', '.');
            $totales = number_format($totales, 0, ',', '.');
            $base_imps = number_format($base_imps, 0, ',', '.');

            echo "<tr>";
            {
                echo "<td class='tg-uasx' style='width: 160.003906px'>{$cta_contable_actual}</td>";
                echo "<td class='tg-uasx' style='width: 500.003906px'>{$cuenta_contable_n}</td>";
                echo "<td class='tg-uasxh' style='width: 156.003906px'>{$exentas}</td>";
                echo "<td class='tg-uasxh' style='width: 156.003906px'>{$gravadas10}</td>";
                echo "<td class='tg-uasxh' style='width: 156.003906px'>{$gravadas5}</td>";
                echo "<td class='tg-uasxh' style='width: 156.003906px'>{$impuestos10}</td>";
                echo "<td class='tg-uasxh' style='width: 156.003906px'>{$impuestos5}</td>";
                echo "<td class='tg-uasxh' style='width: 156.003906px'>{$base_imps}</td>";
                echo "<td class='tg-uasxh' style='width: 156.003906px'>{$totales}</td>";
            }
            echo "</tr>";

            $cant = 0;
            $gravadas5 = 0;
            $gravadas10 = 0;
            $impuestos5 = 0;
            $impuestos10 = 0;
            $exentas = 0;
            $totales = 0;
            $base_imps = 0;
        }

        $porcentaje = $data['porcentaje'];

        $gravada10 = ($data['gravada'] == '-') ? '-' : (($porcentaje == 10) ? (float)$data['gravada'] : 0);
        $gravadas10 += ($data['gravada'] == '-') ? 0 : $gravada10;
        $impuesto10 = ($data['impuesto'] == '-') ? '-' : (($porcentaje == 10) ? (float)$data['impuesto'] : 0);
        $impuestos10 += ($data['impuesto'] == '-') ? 0 : $impuesto10;

        $gravada5 = ($data['gravada'] == '-') ? '-' : (($porcentaje == 5) ? (float)$data['gravada'] : 0);
        $gravadas5 += ($data['gravada'] == '-') ? 0 : $gravada5;
        $impuesto5 = ($data['impuesto'] == '-') ? '-' : (($porcentaje == 5) ? (float)$data['impuesto'] : 0);
        $impuestos5 += ($data['impuesto'] == '-') ? 0 : $impuesto5;

        $exenta = ($data['exenta'] == '-') ? '-' : (float)$data['exenta'];
        $exentas += ($data['exenta'] == '-') ? 0 : $exenta;

        $total = (float)$data['total'] * (float)$data['cotizacion'];
        $totales += $total;

        $base_imp = ($data['gravada'] == '-') ? '-' : 0;
        $base_imps += ($data['gravada'] == '-') ? 0 : $base_imp;

        $cant++;
        $cantTotal++;

        $cuenta_contable = $cta_contable_actual;
        $cuenta_contable_n = $data['cuenta_nombre'];
    }

    $gravadas10 = number_format($gravadas10, 0, ',', '.');
    $gravadas5 = number_format($gravadas5, 0, ',', '.');
    $impuestos10 = number_format($impuestos10, 0, ',', '.');
    $impuestos5 = number_format($impuestos5, 0, ',', '.');
    $exentas = number_format($exentas, 0, ',', '.');
    $totales = number_format($totales, 0, ',', '.');
    $base_imps = number_format($base_imps, 0, ',', '.');

    echo "<tr>";
    {
        echo "<td class='tg-uasx' style='width: 160.003906px'>{$cta_contable_actual}</td>";
        echo "<td class='tg-uasx' style='width: 500.003906px'>{$cuenta_contable_n}</td>";
        echo "<td class='tg-uasxh' style='width: 156.003906px'>{$exentas}</td>";
        echo "<td class='tg-uasxh' style='width: 156.003906px'>{$gravadas10}</td>";
        echo "<td class='tg-uasxh' style='width: 156.003906px'>{$gravadas5}</td>";
        echo "<td class='tg-uasxh' style='width: 156.003906px'>{$impuestos10}</td>";
        echo "<td class='tg-uasxh' style='width: 156.003906px'>{$impuestos5}</td>";
        echo "<td class='tg-uasxh' style='width: 156.003906px'>{$base_imps}</td>";
        echo "<td class='tg-uasxh' style='width: 156.003906px'>{$totales}</td>";
    }
    echo "</tr>";

    echo '<tr class="closing-row">';
    {
        echo "<td colspan='9' class='tg-gh1y' style='font-style: italic; text-align: right'>LISTADO CONCLUÍDO</td>";
    }
    echo '</tr>';
    ?>
    </tbody>
</table>