<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 3/13/18
 * Time: 5:58 PM
 */


use backend\models\Empresa; ?>
<h3 class="reporte-titulo-h3"><u>Libro <?= $tipoLibro ?> por Cuentas</u></h3>
<table class="table-header table-condensed">
    <tr>
        <td class="td-col1">DEL:</td>
        <td class="td-col2"><?= $desde ?></td>
        <?php
        if (isset($id_desde) && $id_desde != '') : ?>
            <td class='td-col'>Código desde:</td>
            <td class='td-col' style="text-align: left; font-weight: normal !important;"><?= $id_desde ?></td>
        <?php else: ?>
            <td class='td-col'></td>
            <td class='td-col'></td>
        <?php endif; ?>
        <td class="td-col3">
            <?php
            $razon_social = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->razon_social;
            echo $razon_social;
            ?>
        </td>
    </tr>
    <tr>
        <td class="td-col1">AL:</td>
        <td class="td-col2"><?= $hasta ?></td>
        <?php
        if (isset($id_hasta) && $id_hasta != '') : ?>
            <td class='td-col'>Código hasta:</td>
            <td class='td-col' style="text-align: left; font-weight: normal !important;"><?= $id_hasta ?></td>
        <?php else: ?>
            <td class='td-col'></td>
            <td class='td-col'></td>
        <?php endif; ?>
        <td class="td-col3">
            <?php
            $ruc = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->ruc;
            $dv = Empresa::getDigitoVerificador($ruc);
            echo "{$ruc}-{$dv}"
            ?>
        </td>
    </tr>
</table>

<table class="tg" style="undefined;table-layout: fixed; width: 1110px">
    <tr>
        <th class="tg-x4jz" colspan="2" style="background-color: lightgrey;">DOCUMENTO</th>
        <th class="tg-x4jz" colspan="2"
            style="background-color: lightgrey;"><?php echo ($tipoLibro == 'Compra') ? 'PROV.' : 'CLIENTES'; ?> DE
            SERVICIOS/BIENES
        </th>
        <th class="tg-x4jz" colspan="8" style="background-color: lightgrey;">VALOR
            DE <?php echo strtoupper($tipoLibro) ?>S/SERVICIOS
        </th>
    </tr>
    <tr>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 70px;">NÚM</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 54px;">FECHA</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 250px;">RAZÓN SOCIAL</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 44px;">RUC</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 94px;">GRAVS.</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 34px;">%</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 94px;">IMPTS.</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 94px;">EXENT.</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 94px;">Ret. IVA</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 94px;">Ret. RENTA</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 94px;">TOTAL</th>
        <th class="tg-gh1y" style="background-color: lightgrey; width: 94px;">BASE IMP.</th>
    </tr>
</table>