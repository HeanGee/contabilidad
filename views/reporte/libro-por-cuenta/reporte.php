<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 2/3/18
 * Time: 5:15 PM
 */

?>

<table class="tg2" style="undefined;table-layout: fixed; width: 1110px">
    <?php
    $a = microtime(true);
    $codigo_completo = '';
    $cant = 0;
    $gravadas = 0;
    $impuestos = 0;
    $exentas = 0;
    $ret_ivas = 0;
    $ret_rentas = 0;
    $totales = 0;
    $base_imps = 0;
    $blockFin = false;
    $i = 0;
    foreach ($dataProvider as $index => $data) {
        $codigo_completo_actual = $data['codigo_completo'];
        if ($codigo_completo_actual == '') continue;

        $entro = false;

        if ($codigo_completo != $codigo_completo_actual) {
            if ($i > 1) {
                $precision = 0;
                $_gravadas = number_format($gravadas, $precision, ',', '.');
                $_impuestos = number_format($impuestos, $precision, ',', '.');
                $_exentas = number_format($exentas, $precision, ',', '.');
                $_ret_ivas = number_format($ret_ivas, $precision, ',', '.');
                $_ret_rentas = number_format($ret_rentas, $precision, ',', '.');
                $_totales = number_format($totales, $precision, ',', '.');
                $_base_imps = number_format($base_imps, $precision, ',', '.');
                echo "<tr class='resume-row'>";
                {
                    echo "<td colspan='2' class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>Ítems</td>";
                    echo "<td class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>{$cant}</td>";
                    echo "<td class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>Sumas:</td>";
                    echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_gravadas}</td>";
                    echo "<td class=\"tg-gh1y2\"></td>";
                    echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_impuestos}</td>";
                    echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_exentas}</td>";
                    echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_ret_ivas}</td>";
                    echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_ret_rentas}</td>";
                    echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_totales}</td>";
                    echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_base_imps}</td>";
                }
                echo "</tr>";
                echo "<tr><td colspan='12' style='padding-top: 10px;'></td></tr>";

                $gravadas = 0;
                $impuestos = 0;
                $exentas = 0;
                $ret_ivas = 0;
                $ret_rentas = 0;
                $totales = 0;
                $base_imps = 0;

                $blockFin = true;
            }

            if ($i < (sizeof($dataProvider) - 1)) {
                $blockFin = false;
                echo "<tr>";
                echo "<td style='font-size: 9px; color: #721c24' colspan='12'><strong>CUENTA: {$data['codigo_completo']} - {$data['cuenta_nombre']}</strong></td>";
                echo "</tr>";
                $cant = 0;
            }

            $codigo_completo = $codigo_completo_actual;
        }

        $gravadas += ($data['gravada'] == '-') ? 0 : (float)$data['gravada'];
        $impuestos += ($data['impuesto'] == '-') ? 0 : (float)$data['impuesto'];
        $exentas += ($data['exenta'] == '-') ? 0 : (float)$data['exenta'];
        $ret_ivas += 0;
        $ret_rentas += 0;
        $totales += ($data['total'] == '-') ? 0 : (float)$data['total'];
        $base_imps += 0;

        echo "<tr>";
        {
            $precision = 0;
            $_gravada = ($data['gravada'] == '-') ? '-' : number_format((float)$data['gravada'], $precision, ',', '.');
            $_impuesto = ($data['impuesto'] == '-') ? '-' : number_format((float)$data['impuesto'], $precision, ',', '.');
            $_exenta = ($data['exenta'] == '-') ? '-' : number_format((float)$data['exenta'], $precision, ',', '.');
            $_ret_iva = number_format(0, $precision, ',', '.');
            $_ret_renta = number_format(0, $precision, ',', '.');
            $_total = ($data['total'] == '-') ? '-' : number_format((float)$data['total'], $precision, ',', '.');
            $_base_imp = number_format(0, $precision, ',', '.');
            echo "<td  style=\"width: 70px; text-align: center\" class=\"tg-gh1y2\" >{$data['nro_factura']}</td>";
            echo "<td  style=\"width: 54px; text-align: center\" class=\"tg-gh1y2\" >{$data['fecha_emision']}</td>";
            echo "<td  style=\"width: 250px;\" class=\"tg-gh1y2\" >{$data['razon_social']}</td>";
            echo "<td  style=\"width: 44px; text-align: center\" class=\"tg-gh1y2\" >{$data['ruc']}</td>";
            echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >{$_gravada}</td>";
            echo "<td  style=\"width: 34px; text-align: center\" class=\"tg-gh1y2\" >{$data['porcentaje']}</td>";
            echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >{$_impuesto}</td>";
            echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >{$_exenta}</td>";
            echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >$_ret_iva</td>";
            echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >$_ret_renta</td>";
            echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >{$_total}</td>";
            echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >$_base_imp</td>";
        }
        echo "</tr>";

        $cant++;
        $i++;
    }
    if (!$blockFin) {
        $precision = 0;
        $_gravadas = number_format($gravadas, $precision, ',', '.');
        $_impuestos = number_format($impuestos, $precision, ',', '.');
        $_exentas = number_format($exentas, $precision, ',', '.');
        $_ret_ivas = number_format($ret_ivas, $precision, ',', '.');
        $_ret_rentas = number_format($ret_rentas, $precision, ',', '.');
        $_totales = number_format($totales, $precision, ',', '.');
        $_base_imps = number_format($base_imps, $precision, ',', '.');
        echo "<tr class='resume-row'>";
        {
            echo "<td colspan='2' class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>Ítems</td>";
            echo "<td class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>{$cant}</td>";
            echo "<td class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>Sumas:</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_gravadas}</td>";
            echo "<td class=\"tg-gh1y2\"></td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_impuestos}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_exentas}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_ret_ivas}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_ret_rentas}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_totales}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$_base_imps}</td>";
        }
        echo "</tr>";
        echo "<tr><td colspan='12' style='padding-top: 10px;'></td></tr>";
    }
    echo '<tr class="closing-row">';
    {
        echo "<td colspan='12' class='tg-gh1y2' style='font-style: italic; text-align: right'>LISTADO CONCLUÍDO</td>";
    }
    echo '</tr>';
    $b = microtime(true); ?>
</table>