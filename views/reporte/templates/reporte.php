<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 2/3/18
 * Time: 5:15 PM
 */

use backend\models\Moneda;
use backend\modules\contabilidad\controllers\Reporte;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use common\helpers\ValorHelpers;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data ActiveRecord[] */
/* @var $cuentas_principales array */
/* @var $valorizar string */
/* @var $agrupar string */
/* @var $separarVisualmente string */
/* @var $nro_facturas_libres array */
/* @var $tipoDocumentoSetIds array */
/* @var $model Reporte */


$session = Yii::$app->session;
$operacion = Yii::$app->getRequest()->getQueryParam('operacion');
$cliente_proveedor_text = $operacion == 'venta' ? 'CLIENTES' : 'PROVEEDORES';
$total_venta_compra_text = $operacion == 'venta' ? 'TOTAL VENTA' : 'TOTAL COMPRA';

(isset($resumen_plantilla) && $resumen_plantilla != '') || $resumen_plantilla = 'no';
?>

<?php
//Yii::warning(sizeof($data));
$array_resumen = [
    'gr10' => 0,
    'im10' => 0,
    'gr5' => 0,
    'im5' => 0,
    'ex' => 0,
    'to' => 0,
    'tt_10' => 0,
    'tt_5' => 0
];
$plantillas_descuento = [];
foreach (PlantillaCompraventa::findAll(['tipo' => $operacion, 'para_descuento' => 'si']) as $item) {
    $plantillas_descuento[] = $item->id;
}
if (!empty($data)) {
    $facturas_ya_procesadas = [];
    $resumen_por_cuenta = [];
    $resumenPorCuenta = false;
    foreach ($tipoDocumentoSetIds as $documentoSetId) { ?>
        <table width="100%" CLASS="contenido">
            <tr>
                <td class="numero"></td>
                <td class="fecha"></td>
                <td class="timbrado"></td>
                <td class="razon-social"></td>
                <td class="ruc"></td>
                <td class="gravadas-10"></td>
                <td class="impuestos-10"></td>
                <td class="total-10"></td>
                <td class="gravadas-5"></td>
                <td class="impuestos-5"></td>
                <td class="total-5"></td>
                <td class="exentas"></td>
                <td class="total"></td>
                <td class="valor-moneda"></td>
                <td class="cuenta"></td>
                <td class="tipo-comprobante"></td>
            </tr>
            <?php
            $total_gravs = 0.0;
            $total_imps = 0.0;
            $total_excs = 0.0;
            $totals = 0.0;
            $items = 0;
            $tt_gr10 = 0.0;
            $tt_gr5 = 0.0;
            $tt_im5 = 0.0;
            $tt_im10 = 0.0;
            $tts_10 = 0.0;
            $tts_5 = 0.0;

            $prefijo = ($data[0] instanceof Venta) ? $data[0]->getNroFacturaCompleto() : $data[0]->nro_factura;
            $prefijo = substr($prefijo, 0, 7);
            $cta_principal = $cuentas_principales[$data[0]->id];
            $separadoPorTipo = false;
            $tipodoc_models = [];
            $moneda_models = [];
            try {
                $resumen_por_cuenta = [  # usado cuando se agrupa primero por cuenta principal
                    'gr10' => 0,
                    'iv10' => 0,
                    'gr5' => 0,
                    'iv5' => 0,
                    'exent' => 0,
                    'total' => 0,
                    'tt_10' => 0,
                    'tt_5' => 0
                ];

                $facturaPrintedExist = false;
                foreach ($data as $key => $fila) {
                    $background_style = $fila->moneda_id != 1 ? 'style="background: #c1e2b3"' : '';

                    # En el primer ciclo de tipo de documento set, ya se debe imprimir todas las facturas anuladas y faltantes.
                    # Se espera que el primer tipo de documento set sea facturas emitidas.
                    # A partir del 2do tipo de documento set y en adelante, no deben aparecer las no vigentes.

                    if ($fila->tipo_documento_id != null) {
                        if (!array_key_exists($fila->tipo_documento_id, $tipodoc_models))
                            $tipodoc_models[$fila->tipo_documento_id] = $fila->tipoDocumento;

                        $tipoDoc = $tipodoc_models[$fila->tipo_documento_id];
                        if ($tipoDoc->tipo_documento_set_id != $documentoSetId) {
                            $facturaPrintedExist = false;
                            continue;
                        }
                    } elseif ($documentoSetId == Compra::getTipodocSetNotaCredito()->id) { # tipo de documento de la factura es null y el tipo de documento set en proceso es nota de credito.
                        $facturaPrintedExist = false;
                        continue;
                    }

                    if (in_array($fila->id, $facturas_ya_procesadas)) continue;

                    $facturas_ya_procesadas[] = $fila->id;

                    $current_pref = ($fila instanceof Venta) ? $fila->getNroFacturaCompleto() : $fila->nro_factura;
                    $current_pref = substr($current_pref, 0, 7);
                    $fecha = (isset($fila->fecha_emision) && $fila->fecha_emision != '') ? date('d-m-Y', strtotime($fila->fecha_emision)) : '';
                    if ($resumen_plantilla == 'no' && $separarVisualmente == 'prefijo' && $key > 1 && $current_pref != $prefijo) {
                        echo "<tr class=\"fila-sin-dato\"><td style=\"height: 20px;\"></td><td colspan='13'></td></tr>";
                    }
                    $current_cta_principal = $cuentas_principales[$fila->id];
                    if ($resumen_plantilla == 'no' && $separarVisualmente == 'cuenta_principal' && $key > 1 && $current_cta_principal != $cta_principal && $facturaPrintedExist) {
                        $resumen_por_cuenta['gr10'] = number_format($resumen_por_cuenta['gr10'], 0, ',', '.');
                        $resumen_por_cuenta['iv10'] = number_format($resumen_por_cuenta['iv10'], 0, ',', '.');
                        $resumen_por_cuenta['gr5'] = number_format($resumen_por_cuenta['gr5'], 0, ',', '.');
                        $resumen_por_cuenta['iv5'] = number_format($resumen_por_cuenta['iv5'], 0, ',', '.');
                        $resumen_por_cuenta['exent'] = number_format($resumen_por_cuenta['exent'], 0, ',', '.');
                        $resumen_por_cuenta['total'] = number_format($resumen_por_cuenta['total'], 0, ',', '.');
                        $resumen_por_cuenta['tt_10'] = number_format($resumen_por_cuenta['tt_10'], 0, ',', '.');
                        $resumen_por_cuenta['tt_5'] = number_format($resumen_por_cuenta['tt_5'], 0, ',', '.');
                        echo '<tr class="datos">';
                        {
                            echo "<td style='border-bottom: none !important; border-left: none !important;' colspan='3'></td>";
                            echo "<td style='background-color: #E9F4FF; text-align: center' colspan='2' ><strong>Subtotal</strong></td>";
                            echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['gr10']}</td>";
                            echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['iv10']}</td>";
                            echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['tt_10']}</td>";
                            echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['gr5']}</td>";
                            echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['iv5']}</td>";
                            echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['tt_5']}</td>";
                            echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['exent']}</td>";
                            echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['total']}</td>";
                            echo "<td style='border-bottom: none !important; border-right: none !important' colspan='3'></td>";
                        }
                        echo '</tr>';
                        echo "<tr class=\"fila-sin-dato\"><td style=\"height: 20px;\"></td><td colspan='13'></td></tr>";
                        $resumen_por_cuenta = [  //resetear
                            'gr10' => 0,
                            'iv10' => 0,
                            'gr5' => 0,
                            'iv5' => 0,
                            'exent' => 0,
                            'total' => 0,
                            'tt_10' => 0,
                            'tt_5' => 0
                        ];
                        if (!$resumenPorCuenta)
                            $resumenPorCuenta = true;
                    }
                    ?>
                    <tr class="datos">
                        <?php if (($fila instanceof Venta || $fila instanceof Compra) /*&& !in_array($fila->estado, ['anulada', 'faltante'])*/) {
                            $items++;
                            $facturaPrintedExist = true;

                            $isVenta = $fila instanceof Venta;
                            $isCompra = $fila instanceof Compra;
                            ?>
                            <td class="centrado" <?php echo $background_style ?>><?php echo $isVenta ? $fila->getNroFacturaCompleto() : $fila->nro_factura ?></td>
                            <td class="centrado" <?php echo $background_style ?>><?php echo $fecha ?></td>
                            <td class="centrado" <?php echo $background_style ?>>
                                <?php echo isset($fila->timbradoDetalle) ? $fila->timbradoDetalle->timbrado->nro_timbrado : '' ?></td>
                            <td class="" style="text-align: left" <?php echo $background_style ?>>
                                <?php echo ($fila->estado == 'vigente') ? $fila->entidad->razon_social : '' ?>
                            </td>
                            <td class="" style="text-align: left" <?php echo $background_style ?>>
                                <?php echo ($fila->estado == 'vigente') ? "{$fila->entidad->ruc}-{$fila->entidad->digito_verificador}" : null ?>
                            </td>
                            <?php
                            /** @var VentaIvaCuentaUsada[] $query */
                            $excs = 0.0;
                            $gravs5 = 0.0;
                            $gravs10 = 0.0;
                            $imps5 = 0.0;
                            $imps10 = 0.0;
                            $tt_10 = 0.0;
                            $tt_5 = 0.0;
                            $query = $isVenta ? VentaIvaCuentaUsada::findAll(['factura_venta_id' => $fila->id]) : CompraIvaCuentaUsada::findAll(['factura_compra_id' => $fila->id]);
                            foreach ($query as $q) {
                                if ($q->iva_cta_id != null) {
                                    if ($q->ivaCta->iva->porcentaje == 5) {
                                        $gravs5 += round(round($q->monto, 2) / (1.05) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1), 2);
                                        $imps5 += round((round($q->monto, 2) - (round($q->monto, 2) / (1.05))) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1), 2);
                                        $tt_5 += $gravs5 + $imps5;
                                    } elseif ($q->ivaCta->iva->porcentaje == 10) {
                                        $gravs10 += round(round($q->monto, 2) / (1.10) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1), 2);
                                        $imps10 += round((round($q->monto, 2) - (round($q->monto, 2) / (1.10))) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1), 2);
                                        $tt_10 += $gravs10 + $imps10;
                                    }
                                } else
                                    $excs += round($q->monto, 2) * (in_array($q->plantilla_id, $plantillas_descuento) ? -1 : 1);
                            }
                            if ($fila->estado == 'vigente') {
                                $cotizacion = ($fila instanceof Venta) ? $fila->valor_moneda : $fila->cotizacion;
                                if ($fila->moneda_id != null && $fila->moneda_id != 1 && $cotizacion) { // si no es guaranies
                                    $ayer = date('Y-m-d', strtotime('-1 day', strtotime($fila->fecha_emision)));
                                    $valor_moneda_factura = $valorizar == 'si' ? round($cotizacion, 2) : 1.0;
                                    $gravs5 = round($gravs5 * $valor_moneda_factura, 2);
                                    $gravs10 = round($gravs10 * $valor_moneda_factura, 2);
                                    $imps5 = round($imps5 * $valor_moneda_factura, 2);
                                    $imps10 = round($imps10 * $valor_moneda_factura, 2);
                                    $excs = round($excs * $valor_moneda_factura, 2);
                                    $fila->total = round($fila->total * $valor_moneda_factura, 2);
                                    $tt_10 = $gravs10 + $imps10;
                                    $tt_5 = $gravs5 + $imps5;
                                }
                            }
                            $total_gravs += round($gravs10, 0) + round($gravs5, 0);
                            $total_imps += round($imps10, 0) + round($imps5, 0);
                            $total_excs += round($excs, 0);
                            $tt_gr5 += round($gravs5, 0);
                            $tt_gr10 += round($gravs10, 0);
                            $tt_im5 += round($imps5, 0);
                            $tt_im10 += round($imps10, 0);
                            $totals += round($fila->total, 0);
                            $tts_10 += round($tt_10, 0);
                            $tts_5 += round($tt_5, 0);

                            if (!array_key_exists(1, $moneda_models))
                                $moneda_models[1] = Moneda::findOne(['id' => 1]);
                            $monedaModel = $moneda_models[1];

                            // acumular resumen por cuenta
                            $resumen_por_cuenta['gr10'] += round($gravs10, 2);
                            $resumen_por_cuenta['gr5'] += round($gravs5, 2);
                            $resumen_por_cuenta['iv10'] += round($imps10, 2);
                            $resumen_por_cuenta['iv5'] += round($imps5, 2);
                            $resumen_por_cuenta['exent'] += round($excs, 2);
                            $resumen_por_cuenta['total'] += round($fila->total, 2);
                            $resumen_por_cuenta['tt_10'] += round($tt_10, 2);
                            $resumen_por_cuenta['tt_5'] += round($tt_5, 2);

                            $moneda = ($fila->estado == 'vigente') ? ($valorizar == 'si' ? $monedaModel : $fila->moneda) : null;
                            $gravs5_show = ($fila->estado == 'vigente') ? ValorHelpers::numberFormatMonedaSensitive($gravs5, 2, $moneda) : 0;
                            $gravs10_show = ($fila->estado == 'vigente') ? ValorHelpers::numberFormatMonedaSensitive($gravs10, 2, $moneda) : 0;
                            $imps5_show = ($fila->estado == 'vigente') ? ValorHelpers::numberFormatMonedaSensitive($imps5, 2, $moneda) : 0;
                            $imps10_show = ($fila->estado == 'vigente') ? ValorHelpers::numberFormatMonedaSensitive($imps10, 2, $moneda) : 0;
                            $tot_10_show = ($fila->estado == 'vigente') ? ValorHelpers::numberFormatMonedaSensitive($gravs10 + $imps10) : 0;
                            $tot_5_show = ($fila->estado == 'vigente') ? ValorHelpers::numberFormatMonedaSensitive($gravs5 + $imps5) : 0;
                            $total_factura = ($fila->estado == 'vigente') ? ValorHelpers::numberFormatMonedaSensitive($fila->total, 2, $moneda) : 0;
                            $excs_show = ($fila->estado == 'vigente') ? ValorHelpers::numberFormatMonedaSensitive($excs, 2, $moneda) : 0;
                            ?>
                            <td class="align-right" <?php echo $background_style ?>><?php echo $gravs10_show ?></td>
                            <td class="align-right" <?php echo $background_style ?>><?php echo $imps10_show ?></td>
                            <td class="align-right total-10">                       <?php echo $tot_10_show ?></td>
                            <td class="align-right" <?php echo $background_style ?>><?php echo $gravs5_show ?></td>
                            <td class="align-right" <?php echo $background_style ?>><?php echo $imps5_show ?></td>
                            <td class="align-right total-5">                        <?php echo $tot_5_show ?></td>
                            <td class="align-right" <?php echo $background_style ?>><?php echo $excs_show ?></td>
                            <td class="align-right" <?php echo $background_style ?>><?php echo $total_factura ?></td>
                            <td class="align-right" <?php echo $background_style ?>><?php echo ($fila->estado == 'vigente') ? (($fila instanceof Venta) ? $fila->valor_moneda : $fila->cotizacion) : 0; ?></td>
                            <td class="centrado" <?php echo $background_style ?>><?= ($fila->estado == 'vigente') ? implode('<br/>', explode(' | ', $cuentas_principales[$fila->id])) : '-' ?></td>
                            <!--<?php //echo ucfirst($fila->condicion) ?>-->
                            <td class="centrado" <?php
                            if (!array_key_exists($fila->tipo_documento_id, $tipodoc_models))
                                $tipodoc_models[$fila->tipo_documento_id] = $fila->tipoDocumento;

                            $tipoDoc = $tipodoc_models[$fila->tipo_documento_id];
                            echo $background_style ?>><?php echo ($fila->estado == 'vigente') ? $tipoDoc->nombre : (($fila->tipo_documento_id != '') ? $tipoDoc->nombre : null)
                                ?></td>
                        <?php } ?>
                    </tr>
                    <?php
                    if ($resumen_plantilla == 'si') {
                        $cotizacion = ($fila instanceof Venta) ? $fila->valor_moneda : $fila->cotizacion;
                        $cotizacion = round($cotizacion, 2);
                        if ($cotizacion == '' || $cotizacion == 0.0) $cotizacion = 1.1; ?>
                        <tr class="datos resumen-por-plantilla-titulo">
                            <td colspan="3" style="border: none;"></td>
                            <td colspan="2"
                                style="font-weight: bold; text-align: center; background-color: #FFFFDC">
                                Plantillas
                            </td>
                            <td colspan="2"
                                style="text-align: center; font-weight: bold; color: black; background-color: #FFFFDC">
                                Total 10%
                            </td>
                            <td colspan="2"
                                style="text-align: center; font-weight: bold; color: black; background-color: #FFFFDC">
                                Total 5
                            </td>
                            <td colspan="2"
                                style="text-align: center; font-weight: bold; color: black; background-color: #FFFFDC">
                                Exentas
                            </td>
                            <td colspan="3" style="border: none;"></td>
                        </tr>
                        <?php foreach ($query as $ivaCtaUsada) { ?>
                            <tr class="datos resumen-por-plantilla">
                                <td colspan="3" style="border: none"></td>
                                <td colspan="2"
                                    style="text-align: left; font-weight: bold; color: black; background-color: #e9f4ff"><?= $ivaCtaUsada->plantilla->nombre ?></td>
                                <td colspan="2" style=" background-color: #e9f4ff"
                                    class="align-right"><?= (isset($ivaCtaUsada->ivaCta) && $ivaCtaUsada->ivaCta->iva->porcentaje == 10) ? number_format(round($ivaCtaUsada->monto), 0, ',', '.') : (0) ?></td>
                                <td colspan="2" style=" background-color: #e9f4ff"
                                    class="align-right"><?= (isset($ivaCtaUsada->ivaCta) && $ivaCtaUsada->ivaCta->iva->porcentaje == 5) ? number_format(round($ivaCtaUsada->monto), 0, ',', '.') : (0) ?></td>
                                <td colspan="2" style=" background-color: #e9f4ff"
                                    class="align-right"><?= !isset($ivaCtaUsada->ivaCta) ? number_format(round($ivaCtaUsada->monto), 0, ',', '.') : (0) ?></td>
                                <td colspan="3" style="border: none"></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="14" style="height: 10px; border: none"></td>
                        </tr>
                    <?php }
                    $prefijo = $current_pref;
                    $cta_principal = $current_cta_principal;
                }
            } catch (Exception $e) {
                \common\helpers\FlashMessageHelpsers::createWarningMessage($e->getMessage());
                Yii::warning($e);
                echo '</tr>';
            }
            if ($resumen_plantilla == 'no' && $separarVisualmente == 'cuenta_principal' && $resumenPorCuenta) {
                $resumen_por_cuenta['gr10'] = number_format($resumen_por_cuenta['gr10'], 0, ',', '.');
                $resumen_por_cuenta['iv10'] = number_format($resumen_por_cuenta['iv10'], 0, ',', '.');
                $resumen_por_cuenta['gr5'] = number_format($resumen_por_cuenta['gr5'], 0, ',', '.');
                $resumen_por_cuenta['iv5'] = number_format($resumen_por_cuenta['iv5'], 0, ',', '.');
                $resumen_por_cuenta['exent'] = number_format($resumen_por_cuenta['exent'], 0, ',', '.');
                $resumen_por_cuenta['total'] = number_format($resumen_por_cuenta['total'], 0, ',', '.');
                $resumen_por_cuenta['tt_10'] = number_format($resumen_por_cuenta['tt_10'], 0, ',', '.');
                $resumen_por_cuenta['tt_5'] = number_format($resumen_por_cuenta['tt_5'], 0, ',', '.');
                echo '<tr class="datos">';
                {
                    echo "<td style='border-bottom: none !important; border-left: none !important;' colspan='3'></td>";
                    echo "<td style='background-color: #E9F4FF; text-align: center' colspan='2' ><strong>Subtotal</strong></td>";
                    echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['gr10']}</td>";
                    echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['iv10']}</td>";
                    echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['tt_10']}</td>";
                    echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['gr5']}</td>";
                    echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['iv5']}</td>";
                    echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['tt_5']}</td>";
                    echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['exent']}</td>";
                    echo "<td style='background-color: #E9F4FF;' class=\"align-right\" $background_style >{$resumen_por_cuenta['total']}</td>";
                    echo "<td style='border-bottom: none !important; border-right: none !important' colspan='3'></td>";
                }
                echo '</tr>';
            }
            ?>

            <tr class="fila-resumen">
                <td colspan="14"></td>
            </tr>

            <!-- GENERACION DE ULTIMA FILA, SUMATORIA DE COLUMNAS RESPECTIVAS. -->
            <!-- Los totales son comunes a compra y venta. -->
            <?php
            $factor = ($documentoSetId == Compra::getTipodocSetNotaCredito()->id) ? -1 : 1;
            $array_resumen['gr10'] += round($tt_gr10 * $factor, 0);
            $array_resumen['im10'] += round($tt_im10 * $factor, 0);
            $array_resumen['gr5'] += round($tt_gr5 * $factor, 0);
            $array_resumen['im5'] += round($tt_im5 * $factor, 0);
            $array_resumen['ex'] += round($total_excs * $factor, 0);
            $array_resumen['to'] += round($totals * $factor, 0);
            $array_resumen['tt_10'] += round($tts_10 * $factor, 0);
            $array_resumen['tt_5'] += round($tts_5 * $factor, 0);
            //            Yii::warning(implode(', ', $array_resumen) . ' factor fue ' . $factor);
            ?>
            <tr class="fila-resumen">
                <td style="text-align: right;"><strong>Total items:</strong></td>
                <td class="align-right"><?php echo $items;
                    $alinear = $valorizar == 'si' ? "align-right" : "align-center" ?></td>
                <td class="align-right"></td>
                <td class="align-right"></td>
                <td style="text-align: right;"><strong>Totales:</strong></td>
                <td class="align-right"><?php echo ($valorizar == 'si') ? number_format($tt_gr10, 0, ',', '.') : '-multiple monedas-' ?></td>
                <td class="align-right"><?php echo ($valorizar == 'si') ? number_format($tt_im10, 0, ',', '.') : '-multiple monedas-' ?></td>
                <td class="align-right"><?php echo ($valorizar == 'si') ? number_format($tts_10, 0, ',', '.') : '-multiple monedas-' ?></td>
                <td class="align-right"><?php echo ($valorizar == 'si') ? number_format($tt_gr5, 0, ',', '.') : '-multiple monedas-' ?></td>
                <td class="align-right"><?php echo ($valorizar == 'si') ? number_format($tt_im5, 0, ',', '.') : '-multiple monedas-' ?></td>
                <td class="align-right"><?php echo ($valorizar == 'si') ? number_format($tts_5, 0, ',', '.') : '-multiple monedas-' ?></td>
                <td class="align-right"><?php echo ($valorizar == 'si') ? number_format($total_excs, 0, ',', '.') : '-multiple monedas-' ?></td>
                <td class="align-right"><?php echo ($valorizar == 'si') ? number_format($totals, 0, ',', '.') : '-multiple monedas-' ?></td>
                <td class="align-right"><?php ?></td>
                <td class="align-right"><?php ?></td>
                <td class="align-right"><?php ?></td>
            </tr>
            <tr class="">
                <td colspan="13"></td>
                <td style="text-align: right; font-style: italic">Listado Concluído</td>
            </tr>
            <tr>
                <td colspan="14" height="10px"></td>
            </tr>
        </table>
        <!--        <pagebreak/>-->
        <?php
    }
}
?>


    <!-- TABLA DE RESUMEN DE IVAS " -->
    <!--    <pagebreak/>-->
    <!--    <br/>-->
    <div style="width: 100%; text-align: center"><STRONG>RESUMEN DE I.V.A.</STRONG></div>
    <table width="100%" CLASS="contenido">
        <tbody>
        <tr class="cabecera arriba">
            <td class="borde_medio"><strong>NUMERO</strong></td>
            <td class="borde_medio"><strong>FECHA</strong></td>
            <td class="borde_medio"><strong>TIMB.</strong></td>
            <td class="borde_medio"><strong>RAZON SOCIAL</strong></td>
            <td class="borde_medio"><strong>R.U.C.</strong></td>
            <td class="borde_medio"><strong>GRAVS. 10%</strong></td>
            <td class="borde_medio"><strong>IMPS. 10%</strong></td>
            <td class="borde_medio"><strong>TOTAL 10%</strong></td>
            <td class="borde_medio"><strong>GRAVS. 5%</strong></td>
            <td class="borde_medio"><strong>IMP 5%</strong></td>
            <td class="borde_medio"><strong>TOTAL 5%</strong></td>
            <td class="borde_medio"><strong>EXENT.</strong></td>
            <td class="borde_medio"><strong>TOTAL</strong></td>
            <td class="borde_medio"><strong>COTIZ.</strong></td>
            <td class="borde_medio"><strong>CUENTA</strong></td>
            <td class="borde_medio"><strong>TIPO COMP.</strong></td>
        </tr>
        <tr>
            <td class="numero"></td>
            <td class="fecha"></td>
            <td class="timbrado"></td>
            <td class="razon-social"></td>
            <td class="ruc"></td>
            <td class="gravadas-10"></td>
            <td class="impuestos-10"></td>
            <td class="total-10"></td>
            <td class="gravadas-5"></td>
            <td class="impuestos-5"></td>
            <td class="total-5"></td>
            <td class="exentas"></td>
            <td class="total"></td>
            <td class="valor-moneda"></td>
            <td class="cuenta"></td>
            <td class="tipo-comprobante"></td>
        </tr>
        <tr class="datos">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['gr10'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['im10'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['tt_10'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['gr5'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['im5'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['tt_5'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['ex'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['to'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"></td>
            <td class="align-right"></td>
            <td class="align-right"></td>
        </tr>
        <tr class="datos">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="align-right" style="font-weight: bold">TOTAL IVA:</td>
            <td class="align-right"><?= ($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($array_resumen['im10'] + $array_resumen['im5'], '', Moneda::findOne(['id' => 1])) : '-multiple monedas-' ?></td>
            <td class="align-right"></td>
            <td class="align-right"></td>
            <td class="align-right"></td>
        </tr>
        </tbody>
    </table>

    <br/>

    <!-- TABLA DE RESUMEN EN CUADRO. -->
    <!--    <table class="resumen-recuadro" width="25%">-->
    <!--        <tr>-->
    <!--            <td>Total Exentas:</td>-->
    <!--            <td class="align-right">--><?= ''//($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($total_excs, '', Moneda::findOne(['id' => 1])) : '-multiple monedas-'              ?><!--</td>-->
    <!--        </tr>-->
    <!--        <tr>-->
    <!--            <td>Total Gravadas:</td>-->
    <!--            <td class="align-right">--><?= ''//($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($total_gravs, '', Moneda::findOne(['id' => 1])) : '-multiple monedas-'              ?><!--</td>-->
    <!--        </tr>-->
    <!--        <tr>-->
    <!--            <td>Base imponible:</td>-->
    <!--            <td class="align-right">--><?= ''//($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive(0, '', Moneda::findOne(['id' => 1])) : '-multiple monedas-'              ?><!--</td>-->
    <!--        </tr>-->
    <!--        <tr>-->
    <!--            <td>Total I.V.A. :</td>-->
    <!--            <td class="align-right">--><?= ''//($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($total_imps, '', Moneda::findOne(['id' => 1])) : '-multiple monedas-'              ?><!--</td>-->
    <!--        </tr>-->
    <!--        <tr class="subrayoado">-->
    <!--            <td></td>-->
    <!--            <td></td>-->
    <!--        </tr>-->
    <!--        <tr>-->
    <!--            <td>Total General:</td>-->
    <!--            <td class="align-right">--><?= ''//($valorizar == 'si') ? ValorHelpers::numberFormatMonedaSensitive($totals, '', Moneda::findOne(['id' => 1])) : '-multiple monedas-'              ?><!--</td>-->
    <!--        </tr>-->
    <!--        <tr>-->
    <!--            <td></td>-->
    <!--            <td></td>-->
    <!--        </tr>-->
    <!--        <tr>-->
    <!--            <td>Total Items:</td>-->
    <!--            <td class="align-right">--><?= ''//$items              ?><!--</td>-->
    <!--        </tr>-->
    <!---->
    <!--    </table>-->

<?php
if (!empty($nro_facturas_libres)) {
    ?>
    <!--    <pagebreak/>-->
    <br/>
    <div class="panel panel-danger" style="font-size: small;">
        <div class="panel-body">
            <h5 style="text-decoration: underline">
                <leyend>Lista de Facturas No Declaradas</leyend>
            </h5>
            <?php
            $msg = [];
            foreach ($nro_facturas_libres as $nro_libre_timbrado) {
                $subsecuencias = [];
                foreach ($nro_libre_timbrado['cortes'] as $key => $corte) {
                    if ($key % 2 == 0) {
                        if ($nro_libre_timbrado['nros_libres'][$corte] != $nro_libre_timbrado['nros_libres'][$nro_libre_timbrado['cortes'][$key + 1]]) {
                            $del = str_pad($nro_libre_timbrado['nros_libres'][$corte], 7, '0', STR_PAD_LEFT);
                            $al = str_pad($nro_libre_timbrado['nros_libres'][$nro_libre_timbrado['cortes'][$key + 1]], 7, '0', STR_PAD_LEFT);
                            $subsecuencias[] = "del {$nro_libre_timbrado['prefijo']}-{$del} al {$nro_libre_timbrado['prefijo']}-{$al}";
                        } else {
                            $nro = str_pad($nro_libre_timbrado['nros_libres'][$corte], 7, '0', STR_PAD_LEFT);
                            $subsecuencias[] = "{$nro_libre_timbrado['prefijo']}-{$nro}";
                        }
                    }
                }
                $msg[] = ['timbrado' => $nro_libre_timbrado['timbrado'], 'subsecuencias' => implode(', ', $subsecuencias)];
            }

            foreach ($msg as $item) {
                echo Html::tag('fieldset', Html::tag('leyend', "Timbrado: {$item['timbrado']}"));
                echo Html::beginTag('ul');
                echo Html::tag('li', "{$item['subsecuencias']}.");

                echo Html::endTag('ul');
            }
            ?>
        </div>
    </div>
<?php } ?>