<?php

use backend\models\Empresa;

/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 3/13/18
 * Time: 5:58 PM
 */


/* @var $this yii\web\View */
/* @var $fecha_obj DateTime */
/* @var $data \yii\db\ActiveRecord[] */
/* @var $del string */
/* @var $al string */

$logo_loc = dirname(__FILE__) . "/logo_sueldos_jornales.png";

class DateTimeHelpers
{
    public static function mesATexto($mes)
    {
        $meses = ['01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre',
            '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre'];
        return $meses[$mes];
    }
}

function printPhones($data)
{
    return sizeof($data) ? $data[0]->empresa->telefonos : null;
}

$dataSize = sizeof($data);
$fecha_desde = $dataSize ? $data[0]->fecha_emision : null;
$fecha_hasta = $dataSize ? $data[0]->fecha_emision : null;
$codigo_desde = $dataSize ? $data[0]->id : null;
$codigo_hasta = $dataSize ? $data[0]->id : null;
$empresa_ruc = $dataSize ? $data[0]->empresa->ruc : null;
$razon_social = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->razon_social;

$list = array_slice($data, 1, ($dataSize - 1));
foreach ($list as $item) {
    if ($item->fecha_emision > $fecha_hasta) $fecha_hasta = $item->fecha_emision;
    elseif ($item->fecha_emision < $fecha_desde) $fecha_desde = $item->fecha_emision;

    if ($item->id > $codigo_hasta) $codigo_hasta = $item->id;
    elseif ($item->id < $codigo_desde) $codigo_desde = $item->id;
}

$cliente_proveedor_text = Yii::$app->getRequest()->getQueryParam('operacion') == 'venta' ? 'CLIENTES' : 'PROVEEDORES';
$total_venta_compra_text = Yii::$app->getRequest()->getQueryParam('operacion') == 'venta' ? 'TOTAL VENTA' : 'TOTAL COMPRA';
?>

<div class="header" style="width: 100%;">

    <br/>

    <div class="reporte-titulo">
        <h3 class="reporte-titulo-h3">LIBRO
            <?php echo ((Yii::$app->getRequest()->getQueryParam('operacion') == 'venta') ? 'VENTA' : 'COMPRA') . ' (' . $razon_social . ')'; ?></h3>
    </div>

    <div>
        <h6 class="modelo">Modelo: <!--Sin quiebre ........ --> </h6>
        <table width="100%" class="encabezado">
            <tbody>
            <tr>
                <td class="del" style="padding-left: 55px;"><strong>Del: </strong>
                </td>
                <td class="del-text" style=""><?= $del ? $del : $fecha_desde ?></td>
                <td class="codigo-desde" style=""><strong>Código desde: </strong>
                </td>
                <td class="codigo-desde-text" style=""><?= $codigo_desde ?></td>
                <td class="rucc" style=""><strong>R.U.C.: </strong></td>
                <td class="rucc-text" style=""><?= $empresa_ruc ?></td>
            </tr>
            <tr>
                <td class="del" style="padding-left: 55px;"><strong>Al: </strong>
                </td>
                <td class="del-text" style=""><?= $al ? $al : $fecha_hasta ?></td>
                <td class="codigo-desde" style=""><strong>Código hasta: </strong>
                </td>
                <td class="codigo-desde-text" style=""><?= $codigo_hasta ?></td>
                <td class="rucc" style=""><strong>Teléfono: </strong></td>
                <td class="rucc-text" style=""><?= printPhones($data) ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <table width="100%" CLASS="contenido">
        <tbody>
        <!--         CARGA DE CABECERA-->
        <tr>
            <td class="numero"></td>
            <td class="fecha"></td>
            <td class="timbrado"></td>
            <td class="razon-social"></td>
            <td class="ruc"></td>
            <td class="gravadas-10"></td>
            <td class="impuestos-10"></td>
            <td class="total-10"></td>
            <td class="gravadas-5"></td>
            <td class="impuestos-5"></td>
            <td class="total-5"></td>
            <td class="exentas"></td>
            <td class="total"></td>
            <td class="valor-moneda"></td>
            <td class="cuenta"></td>
            <td class="tipo-comprobante"></td>
        </tr>
        <tr>
        <tr class="cabecera arriba">
            <td colspan="2" class="borde_medio"><strong>FACTURAS/NOTA</strong></td>
            <td COLSPAN="3" class="borde_medio"><strong><?= $cliente_proveedor_text ?></strong></td>
            <td COLSPAN="11" class="borde_medio"><strong><?= $total_venta_compra_text ?></strong></td>
        </tr>
        <tr class="cabecera arriba">
            <td class="borde_medio"><strong>NUMERO</strong></td>
            <td class="borde_medio"><strong>FECHA</strong></td>
            <td class="borde_medio"><strong>TIMB.</strong></td>
            <td class="borde_medio"><strong>RAZON SOCIAL</strong></td>
            <td class="borde_medio"><strong>R.U.C.</strong></td>
            <td class="borde_medio"><strong>GRAVS. 10%</strong></td>
            <td class="borde_medio"><strong>IMPS. 10%</strong></td>
            <td class="borde_medio"><strong>TOTAL 10%</strong></td>
            <td class="borde_medio"><strong>GRAVS. 5%</strong></td>
            <td class="borde_medio"><strong>IMP 5%</strong></td>
            <td class="borde_medio"><strong>TOTAL 5%</strong></td>
            <td class="borde_medio"><strong>EXENT.</strong></td>
            <td class="borde_medio"><strong>TOTAL</strong></td>
            <td class="borde_medio"><strong>COTIZ.</strong></td>
            <td class="borde_medio"><strong>CUENTA</strong></td>
            <td class="borde_medio"><strong>TIPO COMP.</strong></td>
        </tr>
        </tbody>
    </table>
</div>