<?php

use backend\models\Empresa;

$empresa = Empresa::findOne(Yii::$app->session->get('core_empresa_actual'));
if ($empresa->tipo == 'juridica') {
    $razonSocial = $empresa->razon_social;
} else {
    $razonSocial = $empresa->primer_apellido . ' ' . $empresa->segundo_apellido . ', ' . $empresa->primer_nombre . ' ' . $empresa->segundo_nombre;
}
$ruc = $empresa->ruc . '-' . $empresa->digito_verificador;
$desde = Yii::$app->formatter->asDate($fecha_desde, 'dd-MM-y');
$hasta = Yii::$app->formatter->asDate($fecha_hasta, 'dd-MM-y');
?>

<table class="tg" style="undefined;table-layout: fixed; width: 1118px">
    <colgroup>
        <col style="width: 473px">
        <col style="width: 144px">
        <col style="width: 140px">
        <col style="width: 141px">
        <col style="width: 146px">
        <col style="width: 149px">
        <col style="width: 141px">
        <col style="width: 142px">
        <col style="width: 142px">
    </colgroup>
    <tr>
        <th class="tg-91w8" colspan="9">Balance Impositivo</th>
    </tr>
    <tr>
        <td class="tg-0l74">1.- Identificación del Contribuyente</td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74">2.- Periodo Fiscal</td>
        <td class="tg-0l74"></td>
    </tr>
    <tr>
        <td class="tg-0l74">Razon Social o Apellidos</td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74" colspan="2">Nombres</td>
        <td class="tg-0l74" colspan="2">Identificador RUC</td>
        <td class="tg-uxaa">Desde</td>
        <td class="tg-uxaa">Hasta</td>
    </tr>
    <tr>
        <td class="tg-l6li" colspan="3"><b><?= strtoupper($razonSocial) ?></b></td>
        <td class="tg-91w8" colspan="2"></td>
        <td class="tg-91w8" colspan="2"><b><?= $ruc ?></b></td>
        <td class="tg-91w8"><b><?= $desde ?></b></td>
        <td class="tg-91w8"><b><?= $hasta ?></b></td>
    </tr>
    <tr>
        <td class="tg-0l74">3.- Identificación del Represente Legal</td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-uxaa">4.- Identificación del Contador</td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-uxaa">5.- Declarac.Jurada Utilizada</td>
        <td class="tg-uxaa"></td>
    </tr>
    <tr>
        <td class="tg-0l74">Apelidos/Nombres</td>
        <td class="tg-0l74"></td>
        <td class="tg-0l74"></td>
        <td class="tg-uxaa">Apellidos/Nombres</td>
        <td class="tg-0l74"></td>
        <td class="tg-uxaa">Identificador RUC</td>
        <td class="tg-0l74"></td>
        <td class="tg-uxaa">Formulario N°</td>
        <td class="tg-0l74">N° de Orden</td>
    </tr>
    <tr>
        <td class="tg-l6li"><b>.</b></td>
        <td class="tg-l6li"></td>
        <td class="tg-l6li"></td>
        <td class="tg-91w8" colspan="2"><b></b></td>
        <td class="tg-91w8" colspan="2"><b></b></td>
        <td class="tg-l6li"></td>
        <td class="tg-l6li"></td>
    </tr>
    <tr>
        <td class="tg-v1c5" rowspan="2" style="width: 300px;">Cuentas</td>
        <td class="tg-uxaa" colspan="2">Sumas</td>
        <td class="tg-uxaa" colspan="2">Saldos</td>
        <td class="tg-uxaa" colspan="2">Inventario</td>
        <td class="tg-uxaa" colspan="2">Resultados</td>
    </tr>
    <tr>
        <td class="tg-uxaa" style="width: 120px;">Débitos</td>
        <td class="tg-uxaa" style="width: 120px;">Créditos</td>
        <td class="tg-uxaa" style="width: 120px;">Deudor</td>
        <td class="tg-uxaa" style="width: 120px;">Acreedor</td>
        <td class="tg-uxaa" style="width: 120px;">Activo</td>
        <td class="tg-uxaa" style="width: 120px;">Pasivo</td>
        <td class="tg-uxaa" style="width: 120px;">Debe</td>
        <td class="tg-uxaa" style="width: 120px;">Haber</td>
    </tr>
</table>