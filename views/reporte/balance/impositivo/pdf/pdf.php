<table class="tg" style="undefined;table-layout: fixed; width: 1118px">
    <?php

    use backend\modules\contabilidad\controllers\ReporteBalance;
    use common\helpers\ValorHelpers;

    /* @var $model ReporteBalance */

    $totalDebe = 0;
    $totalHaber = 0;
    $totalDeudor = 0;
    $totalAcreedor = 0;
    $totalInventarioActivo = 0;
    $totalInventarioPasivo = 0;
    $totalResultadoDebe = 0;
    $totalResultadoHaber = 0;

    foreach ($data as $item) {
        $totalDebe += $item['total_debe'];
        $totalHaber += $item['total_haber'];
        $totalDeudor += $item['total_deudor'];
        $totalAcreedor += $item['total_acreedor'];
        $totalInventarioActivo += $item['total_inventario_activo'];
        $totalInventarioPasivo += $item['total_inventario_pasivo'];
        $totalResultadoDebe += $item['total_resultado_debe'];
        $totalResultadoHaber += $item['total_resultado_haber'];
        echo '
            <tr>
                <td class="tg-data" style="width: 300px; text-align: left">' . strtoupper($item['nombre'] . (($model->mostrar_codigo_cuenta == 'si') ? ' - ' . $item['cod_completo'] : '')) . '</td>
                <td class="tg-data" style="width: 120px;">' . ValorHelpers::numberFormatMonedaSensitive($item['total_debe']) . '</td>
                <td class="tg-data" style="width: 120px;">' . ValorHelpers::numberFormatMonedaSensitive($item['total_haber']) . '</td>
                <td class="tg-data" style="width: 120px;">' . ValorHelpers::numberFormatMonedaSensitive($item['total_deudor']) . '</td>
                <td class="tg-data" style="width: 120px;">' . ValorHelpers::numberFormatMonedaSensitive($item['total_acreedor']) . '</td>
                <td class="tg-data" style="width: 120px;">' . ValorHelpers::numberFormatMonedaSensitive($item['total_inventario_activo']) . '</td>
                <td class="tg-data" style="width: 120px;">' . ValorHelpers::numberFormatMonedaSensitive($item['total_inventario_pasivo']) . '</td>
                <td class="tg-data" style="width: 120px;">' . ValorHelpers::numberFormatMonedaSensitive($item['total_resultado_debe']) . '</td>
                <td class="tg-data" style="width: 120px;">' . ValorHelpers::numberFormatMonedaSensitive($item['total_resultado_haber']) . '</td>
            </tr>
        ';
    }

    $complementoDebe = 0;
    $complementoHaber = 0;
    $diff1 = $totalDebe - $totalHaber;
    if ($diff1 < 0) {
        $complementoDebe = -1 * $diff1;
        $totalDebe += $complementoDebe;
    } else {
        $complementoHaber = -1 * $diff1;
        $totalHaber += $complementoHaber;
    }

    $complementoDeudor = 0;
    $complementoAcreedor = 0;
    $diff2 = $totalDeudor - $totalAcreedor;
    if ($diff1 < 0) {
        $complementoDeudor = -1 * $diff2;
        $totalDeudor += $complementoDeudor;
    } else {
        $complementoAcreedor = -1 * $diff2;
        $totalAcreedor += $complementoAcreedor;
    }

    $complementoInventarioActivo = 0;
    $complementoInventarioPasivo = 0;
    $diff3 = $totalInventarioActivo - $totalInventarioPasivo;
    if ($diff1 < 0) {
        $complementoInventarioActivo = -1 * $diff3;
        $totalInventarioActivo += $complementoInventarioActivo;
    } else {
        $complementoInventarioPasivo = -1 * $diff3;
        $totalInventarioPasivo += $complementoInventarioPasivo;
    }

    $complementoResultadoDebe = 0;
    $complementoResultadoHaber = 0;
    $diff4 = $totalResultadoDebe - $totalResultadoHaber;
    if ($diff1 < 0) {
        $complementoResultadoDebe = $diff4;
        $totalResultadoDebe += $complementoResultadoDebe;
    } else {
        $complementoResultadoHaber = $diff4;
        $totalResultadoHaber += $complementoResultadoHaber;
    }
    ?>
    <!--  Perdida y ganancia  -->
    <tr>
        <td class="tg-data" style="width: 300px; text-align: left"><b>PERDIDAS Y GANANCIAS</b></td>
        <td class="tg-data" style="width: 120px;">
            <b><?= ValorHelpers::numberFormatMonedaSensitive($complementoDebe) ?></b>
        </td>
        <td class="tg-data" style="width: 120px;">
            <b><?= ValorHelpers::numberFormatMonedaSensitive($complementoHaber) ?></b>
        </td>
        <td class="tg-data" style="width: 120px;">
            <b><?= ValorHelpers::numberFormatMonedaSensitive($complementoDeudor) ?></b>
        </td>
        <td class="tg-data" style="width: 120px;">
            <b><?= ValorHelpers::numberFormatMonedaSensitive($complementoAcreedor) ?></b></td>
        <td class="tg-data"
            style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($complementoInventarioActivo) ?></b>
        </td>
        <td class="tg-data"
            style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($complementoInventarioPasivo) ?></b>
        </td>
        <td class="tg-data"
            style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($complementoResultadoDebe) ?></b>
        </td>
        <td class="tg-data"
            style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($complementoResultadoHaber) ?></b>
        </td>
    </tr>
    <!--  Totales  -->
    <tr>
        <td class="tg-data" style="width: 300px;"><b>TOTAL</b></td>
        <td class="tg-data" style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($totalDebe) ?></b>
        </td>
        <td class="tg-data" style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($totalHaber) ?></b>
        </td>
        <td class="tg-data" style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($totalDeudor) ?></b>
        </td>
        <td class="tg-data" style="width: 120px;">
            <b><?= ValorHelpers::numberFormatMonedaSensitive($totalAcreedor) ?></b></td>
        <td class="tg-data"
            style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($totalInventarioActivo) ?></b></td>
        <td class="tg-data"
            style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($totalInventarioPasivo) ?></b></td>
        <td class="tg-data"
            style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($totalResultadoDebe) ?></b></td>
        <td class="tg-data"
            style="width: 120px;"><b><?= ValorHelpers::numberFormatMonedaSensitive($totalResultadoHaber) ?></b></td>
    </tr>
</table>