<?php

use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $model \backend\modules\contabilidad\controllers\ReporteBalance */

$this->title = 'Reporte Balance Impositivo';
$this->params['breadcrumbs'][] = $this->title;
$periodoAnho = \backend\modules\contabilidad\models\EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'))->anho;
?>
<div class="balance-index">

    <?php $form = ActiveForm::begin([
        'id' => 'reporte-balance-form'
    ]); ?>

    <div class="text-right btn-toolbar">
        <?php
        echo Html::submitButton('Ver PDF', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info pull-left']);
        echo Html::button('Ver EXCEL', ['id' => 'bajar_xls', 'formtarget' => '_blank', 'class' => 'btn btn-warning pull-left']);

        ?>
    </div>

    <br/>

    <div>
        <?php
        try {
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 3,
                'attributes' => [
                    'mostrar_codigo_cuenta' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::class,
                        'options' => [
                            'data' => ['si' => "Si", 'no' => "No"],
                        ],
                    ],
                    'fecha_desde' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => DateControl::class,
                        'options' => [
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'language' => 'es',
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-MM-yyyy',
                                    'todayHighlight' => true,
                                    'weekStart' => '0',
                                    'startDate' => $periodoAnho != null ? '01-01-' . $periodoAnho : false,
                                    'endDate' => $periodoAnho != null ? '31-12-' . $periodoAnho : false,
                                ],
                            ],
                        ]
                    ],
                    'fecha_hasta' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => DateControl::class,
                        'options' => [
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'language' => 'es',
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-MM-yyyy',
                                    'todayHighlight' => true,
                                    'weekStart' => '0',
                                    'startDate' => $periodoAnho != null ? '01-01-' . $periodoAnho : false,
                                    'endDate' => $periodoAnho != null ? '31-12-' . $periodoAnho : false,
                                ],
                            ],
                        ]
                    ],
//                    'nivel' => [
//                        'type' => Form::INPUT_WIDGET,
//                        'widgetClass' => NumberControl::class,
//                        'options' => [
//                            'value' => 0.00,
//                            'maskedInputOptions' => [
//                                'groupSeparator' => '.',
//                                'radixPoint' => ',',
//                                'rightAlign' => true,
//                                'allowMinus' => false,
//                            ],
//                        ],
//                        'label' => 'Nivel de cuentas',
//                    ],
                    'tipo_doc' => [
                        'type' => Form::INPUT_HIDDEN
                    ]
                ]
            ]);
        } catch (Exception $e) {
            echo $e;
        } ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    //funcion para botones de pdf y xls
    $script = <<<JS
$(document).ready(function () {
    
    document.getElementById('reporte-balance-form').setAttribute("target", "_blank");
    // Cada vez que haga submit, renderiza en nueva pestanha.
    $('#btn_submit').on('click', function() {
        $('#reportebalance-tipo_doc').val('pdf');
    });
    
    // para descargar el libro en formato xls.
    $('#bajar_xls').on('click', function() {
        $('#reportebalance-tipo_doc').val('xls');
        $('#reporte-balance-form').submit();
    });
});
JS;

    $this->registerJs($script);
    ?>
