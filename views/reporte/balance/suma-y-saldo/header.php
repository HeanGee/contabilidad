<?php

use backend\models\Empresa;

$empresa = Empresa::findOne(\Yii::$app->session->get('core_empresa_actual'));
?>
<h3 class="titulo"><u>BALANCE DE SUMAS Y SALDOS</u></h3>
<table class="table-header table-condensed">
    <tr>
        <td class="td-col1">DEL:</td>
        <td class="td-col2"><?= "01-01-{$anho}" ?></td>
        <td class="td-col"></td>
        <td class="td-col3"><?= "{$empresa->razon_social}" ?></td>
    </tr>
    <tr>
        <td class="td-col1">AL:</td>
        <td class="td-col2"><?= "31-12-{$anho}" ?></td>
        <td class="td-col"></td>
        <td class="td-col3"><?= "{$empresa->ruc}-{$empresa->digito_verificador}" ?></td>
    </tr>
</table>

<table class="tg" style="undefined;table-layout: fixed; width: 1118px">
    <tr>
        <th class="tg-x4jz" rowspan="2" style="width: 149px;">CÓDIGO</th>
        <th class="tg-x4jz" rowspan="2" style="width: 378px;">NOMBRE</th>
        <th class="tg-gh1y" colspan="2">SUMAS</th>
        <th class="tg-gh1y" colspan="2">SALDOS</th>
    </tr>
    <tr>
        <td class="tg-gh1y" style="width: 150px;">DEBE</td>
        <td class="tg-gh1y" style="width: 150px;">HABER</td>
        <td class="tg-gh1y" style="width: 150px;">DEBE</td>
        <td class="tg-gh1y" style="width: 150px;">HABER</td>
    </tr>
</table>
<br/>
