<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 08/01/19
 * Time: 06:43 PM
 */

/** @var array $data */

/**
 * @param $string
 * @return string
 */
function nf($string)
{
    if ($string == '') return 0;
    return number_format($string, 0, ',', '.');
}

?>

<table class="tg2 table table-condensed" style="undefined;table-layout: fixed; width: 1118px">
    <?php
    $debeTotal = $haberTotal = $saldoDebeTotal = $saldoHaberTotal = 0;
    foreach ($data as $item) : ?>
        <?php $debeTotal += (int)$item['debe'];
        $haberTotal += (int)$item['haber'];
        $saldoDebeTotal += (int)$item['saldo_debe'];
        $saldoHaberTotal += (int)$item['saldo_haber']; ?>
        <tr>
            <td class="td-data" style="width: 130px;"><?= $item['cuenta_codigo'] ?></td>
            <td class="td-data" style="width: 378px;"><?= $item['cuenta_nombre'] ?></td>
            <td class="td-data monto" style="width: 150px;"><?= nf($item['debe']) ?></td>
            <td class="td-data monto" style="width: 150px;"><?= nf($item['haber']) ?></td>
            <td class="td-data monto" style="width: 150px;"><?= nf($item['saldo_debe']) ?></td>
            <td class="td-data monto" style="width: 150px;"><?= nf($item['saldo_haber']) ?></td>
        </tr>
    <?php endforeach; ?>
    <tr class="last-row">
        <td colspan="2">TOTALES:</td>
        <td class="td-data monto" style="width: 150px;"><?= nf($debeTotal) ?></td>
        <td class="td-data monto" style="width: 150px;"><?= nf($haberTotal) ?></td>
        <td class="td-data monto" style="width: 150px;"><?= nf($saldoDebeTotal) ?></td>
        <td class="td-data monto" style="width: 150px;"><?= nf($saldoHaberTotal) ?></td>
    </tr>
</table>