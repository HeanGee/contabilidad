<?php

use common\helpers\FlashMessageHelpsers;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $model \backend\modules\contabilidad\controllers\Reporte */

$this->title = 'Balance de Sumas y Saldos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="balance-index">

    <?php $form = ActiveForm::begin([
        'id' => 'reporte-balance-form'
    ]); ?>

    <div class="text-right btn-toolbar">
        <?php
        echo Html::submitButton('Ver PDF', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info pull-left']);
        echo Html::button('Ver EXCEL', ['id' => 'bajar_xls', 'formtarget' => '_blank', 'class' => 'btn btn-warning pull-left']);

        ?>
    </div>

    <div>
        <?php
        try {
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 6,
                'attributes' => [
                    'rango' => [
                        'type' => Form::INPUT_WIDGET,
                        'columnOptions' => ['colspan' => '1'],
                        'widgetClass' => DateRangePicker::className(),
                        'options' => [
                            'model' => $model,
                            'attribute' => 'fecha_rango',
                            'convertFormat' => true,
                            'language' => 'es',
                            'pluginOptions' => [
                                'allowClear' => true,
                                'autoApply' => true,
//                                'timePicker'=>true,
                                'locale' => ['format' => 'd-m-Y'],

                            ],

                        ],
                    ],
                    'tipo_doc' => [
                        'type' => Form::INPUT_HIDDEN
                    ]
                ]
            ]);
        } catch (Exception $e) {
            echo $e;
            FlashMessageHelpsers::createWarningMessage($e->getMessage());
        }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $script = <<<JS
$(document).ready(function () {
    
    document.getElementById('reporte-balance-form').setAttribute("target", "_blank");
    // Cada vez que haga submit, renderiza en nueva pestanha.
    $('#btn_submit').on('click', function() {
        $('#balancesumasaldo-tipo_doc').val('pdf');
    });
    
    // para descargar el libro en formato xls.
    $('#bajar_xls').on('click', function() {
        $('#balancesumasaldo-tipo_doc').val('xls');
        $('#reporte-balance-form').submit();
    });
});
JS;
    $this->registerJs($script);
    ?>
</div>
