<?php

use backend\modules\contabilidad\controllers\ReporteEstadoCuenta;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $model \backend\modules\contabilidad\controllers\ReporteEstadoCuenta */

$this->title = 'Estado de Cuenta de Clientes';
$this->params['breadcrumbs'][] = $this->title;
$periodoAnho = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'))->anho;
?>
<div class="estado-cuenta-index">

    <?php $form = ActiveForm::begin([
        'id' => 'reporte-estado-cuenta-cliente-form'
    ]); ?>

    <div class="text-right btn-toolbar">
        <?php
        echo Html::submitButton('Ver PDF', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info pull-left']);
        //        echo Html::button('Ver EXCEL', ['id' => 'bajar_xls', 'formtarget' => '_blank', 'class' => 'btn btn-warning pull-left']);

        ?>
    </div>

    <br/>

    <div>
        <?php
        try {
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 5,
                'attributes' => [
                    'tipo' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ReporteEstadoCuenta::getTipos(),
                            'options' => [
                                'placeholder' => 'Por favor Seleccione Uno',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'label' => 'Tipo de Reporte'
                    ],
                    'paper_size' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ReporteEstadoCuenta::getPaperSizes(),
                            'options' => [
                                'placeholder' => 'Por favor Seleccione Uno',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'label' => 'Hoja'
                    ],
                    'orientation' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => [\kartik\mpdf\Pdf::ORIENT_LANDSCAPE => 'Horizontal', \kartik\mpdf\Pdf::ORIENT_PORTRAIT => 'Vertical'],
                            'options' => [
                                'placeholder' => 'Por favor Seleccione Uno',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'label' => 'Orientación'
                    ],
                    'fecha_rango' => [
                        'type' => Form::INPUT_WIDGET,
                        'columnOptions' => ['colspan' => '1'],
                        'widgetClass' => DateRangePicker::className(),
                        'options' => [
                            'model' => $model,
                            'attribute' => 'fecha_rango',
                            'convertFormat' => true,
                            'language' => 'es',
                            'pluginOptions' => [
                                'allowClear' => true,
                                'autoApply' => true,
//                                'timePicker'=>true,
                                'locale' => ['format' => 'd-m-Y'],

                            ],

                        ],
                    ],
                    'entidad_id' => [
                        'type' => Form::INPUT_WIDGET,
                        'columnOptions' => ['colspan' => '2'],
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => Compra::getEntidades2([
                                'empresa_id' => Yii::$app->session->get('core_empresa_actual'),
                                'periodo_contable_id' => Yii::$app->session->get('core_empresa_actual'),
                                'forSelect2',
                            ]),
                            'options' => [
                                'placeholder' => 'Por favor Seleccione Uno',
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                'multiple' => true,
                            ],
                        ],
                    ],
                    'tipo_doc' => [
                        'type' => Form::INPUT_HIDDEN
                    ]
                ]
            ]);
        } catch (Exception $e) {
            echo $e;
        } ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    //funcion para botones de pdf y xls
    $script = <<<JS
$(document).ready(function () {
    
    document.getElementById('reporte-estado-cuenta-cliente-form').setAttribute("target", "_blank");
    // Cada vez que haga submit, renderiza en nueva pestanha.
    $('#btn_submit').on('click', function() {
        $('#reporteestadocuenta-tipo_doc').val('pdf');
    });
    
    // para descargar el libro en formato xls.
    // $('#bajar_xls').on('click', function() {
    //     $('#reporteestadocuenta-tipo_doc').val('xls');
    //     $('#reporte-estado-cuenta-proveedor-form').submit();
    // });
});
JS;

    $this->registerJs($script);
    ?>
