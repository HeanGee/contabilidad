<table style="undefined;table-layout: fixed; width: 100%">
    <?php

    use common\helpers\ValorHelpers;

    $entidadActual = null;
    $totalDebeGeneral = 0;
    $totalHaberGeneral = 0;
    $arrayResult = [];
    foreach ($data as $item) {
        $totalDebeItem = 0;
        $totalHaberItem = 0;
        if ($item['razon_social'] != $entidadActual) {
            $entidadActual = $item['razon_social'];
            $totalDebeActual = 0;
            $totalHaberActual = 0;
            $saldo = 0;
        }

        if ($item['venta_tipo'] == 'factura') {
            $total = $item['total'];
        } elseif ($item['venta_tipo'] == 'nota_credito') {
            $total = -1 * $item['total'];
        } elseif ($item['venta_tipo'] == 'nota_debito') {
            $total = $item['total'];
        }

        $saldo += $total;
        $totalHaberActual += $total;
        $totalHaberItem += $total;
        $totalHaberGeneral += $total;

        $totalRecibo = 0;
        $numeroRecibo = '';
        if ($item['fecha_recibo'] != null) {
            if ($item['venta_tipo'] == 'nota_credito') {
                $totalRecibo = -1 * $item['total_recibo'];
            } else {
                $totalRecibo = $item['total_recibo'];
            }

            $saldo -= $totalRecibo;
            $totalDebeActual += $totalRecibo;
            $totalDebeItem += $totalRecibo;
            $totalDebeGeneral += $totalRecibo;
            $numeroRecibo = $item['numero_recibo'];
        }

        if (isset($arrayResult[$item['entidad_id']]))
            $arrayResult[$item['entidad_id']] = ['ruc' => $item['ruc'], 'razon_social' => $item['razon_social'], 'total_debe' => $arrayResult[$item['entidad_id']]['total_debe'] + $totalDebeItem, 'total_haber' => $arrayResult[$item['entidad_id']]['total_haber'] + $totalHaberItem, 'saldo' => $arrayResult[$item['entidad_id']]['saldo'] + $saldo];
        else
            $arrayResult[$item['entidad_id']] = ['ruc' => $item['ruc'], 'razon_social' => $item['razon_social'], 'total_debe' => $totalDebeItem, 'total_haber' => $totalHaberItem, 'saldo' => $saldo];
    }

    foreach ($arrayResult as $id => $value) {
        //Cuando se cambia la entidad, se coloca totales por entidad
        echo '
            <tr>
                <td class="td" style="width: 15%;"><span>' . $value['ruc'] . '</span></td>
                <td class="td" style="width: 40%;"><span>' . $value['razon_social'] . '</span></td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_debe']) . '</td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_haber']) . '</td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_debe'] - $value['total_haber']) . '</td>
            </tr>
        ';
    }
    ?>
</table>
<div style="width: 100%; border-top: solid 2px #000000;"></div>
<table style="undefined;table-layout: fixed; width: 100%">
    <tr>
        <td class="td" style="width: 15%;"></td>
        <td class="td" style="width: 40%;"></td>
        <td class="td"
            style="text-align: right; width: 15%;"><?= ValorHelpers::numberFormatMonedaSensitive($totalDebeGeneral) ?></td>
        <td class="td"
            style="text-align: right; width: 15%;"><?= ValorHelpers::numberFormatMonedaSensitive($totalHaberGeneral) ?></td>
        <td class="td"
            style="text-align: right; width: 15%;"><?= ValorHelpers::numberFormatMonedaSensitive($totalDebeGeneral - $totalHaberGeneral) ?></td>
    </tr>
</table>
<div style="width: 100%; border-top: solid 2px #000000;"></div>
