<?php
/** @var \backend\modules\contabilidad\controllers\ReporteEstadoCuenta $model */

$username = Yii::$app->user->identity->username;
$hoy = date('d-m-Y');
$hoyhms = date('H:i:s');
?>
<div class="header" style="width: 100%;">
    <div class="reporte-titulo">
        <h3 class="reporte-titulo-h3">Estado de Cuenta</h3>
    </div>
    <h3 class="reporte-titulo-h3-2">De: <span
                style="font-weight: normal;"><?= date_create_from_format('Y-m-d', $model->fecha_desde)->format('d-m-Y') ?></span>
    </h3>
    <h3 class="reporte-titulo-h3-2">Hasta: <span
                style="font-weight: normal;"><?= date_create_from_format('Y-m-d', $model->fecha_hasta)->format('d-m-Y') ?></span>
    </h3>
</div>
<table class="tg" style="undefined;table-layout: fixed; width: 100%">
    <tr>
        <td class="cabecera" style="width: 15%; border-right: solid 1px #c0c0c0;"><span
                    class="cabecera-span">R.U.C.</span></td>
        <td class="cabecera" style="width: 40%; border-right: solid 1px #c0c0c0;"><span
                    class="cabecera-span">NOMBRE</span>
        </td>
        <td class="cabecera" style="width: 15%; border-right: solid 1px #c0c0c0;"><span
                    class="cabecera-span">MONTO COBRADO</span></td>
        <td class="cabecera" style="width: 15%; border-right: solid 1px #c0c0c0;"><span
                    class="cabecera-span">MONTO A COBRAR</span></td>
        <td class="cabecera" style="width: 15%;"><span class="cabecera-span">SALDO</span></td>
    </tr>
</table>