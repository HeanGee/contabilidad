<table style="undefined;table-layout: fixed; width: 100%">
    <?php

    use common\helpers\ValorHelpers;

    try {
        $entidadActual = null;
        $totalDebeGeneral = 0;
        $totalHaberGeneral = 0;
        foreach ($data['facturas'] as $item) {
            $totalDebeItem = 0;
            $totalHaberItem = 0;
            if ($item['razon_social'] != $entidadActual) {
                if ($entidadActual != null) {
                    #notas de credito sin factura
                    # NSF(o nsf): siglas de `Notas Sin Factura`
                    $entidadActual_nsf = null;
                    foreach ($data['notasSFactura'] as $key => $nota) {
                        if ($nota['razon_social'] != $entidadActual) continue;

                        $titulo = "NOTA CREDITO {$nota['nro_factura']} SIN FACTURA; ";
                        $total = -1 * $nota['total'];

                        $saldo += $total;
                        $totalHaberActual += abs($total);
                        $totalHaberItem += abs($total);
                        $totalHaberGeneral += abs($total);
                        $entidadActual_nsf = $nota['razon_social'];

                        echo '
                            <tr>
                                <td class="td" style="width: 15%;"><span>' . Yii::$app->formatter->asDate($nota['fecha_emision'], 'dd-MM-Y') . '<span style="color: #ffffff">------</span>' . $nota['nro_factura'] . '</span></td>
                                <td class="td" style="width: 40%;"><span>' . '<span style="color: #ffffff">------</span>' . $titulo . '</span></td>
                                <td class="td" style="text-align: right; width: 15%;">0</td>
                                <td class="td" style="text-align: right; width: 15%;"><span>' . ValorHelpers::numberFormatMonedaSensitive(abs($total)) . '</span></td>
                                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
                            </tr>
                        ';

                        unset($data['notasSFactura'][$key]);  # para disminuir carga en cada iteracion
                    }
                    //En la última itereación falta agregar el último item (notas sin factura)
                    if ($entidadActual_nsf != null) {
                        echo '
                            <tr>
                                <td class="td" style="width: 15%;"><span></span></td>
                                <td class="td" style="width: 40%;"><span></span></td>
                                <td class="td" style="text-align: right; width: 15%; border-top: dotted 1px #000000; border-bottom: dotted 1px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeItem) . '</td>
                                <td class="td" style="text-align: right; width: 15%; border-top: dotted 1px #000000; border-bottom: dotted 1px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberItem) . '</td>
                                <td class="td" style="text-align: right; width: 15%;"></td>
                            </tr>
                        ';
                    }
                    //Cuando se cambia la entidad, se coloca totales por entidad
                    echo '
                        <tr>
                            <td class="td" style="width: 15%;"><span></span></td>
                            <td class="td" style="width: 40%;"><span></span></td>
                            <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeActual) . '</td>
                            <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberActual) . '</td>
                            <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
                        </tr>
                    ';
                }

                $entidadActual = $item['razon_social'];
                $totalDebeActual = 0;
                $totalHaberActual = 0;
                $saldo = 0;
                //TITULO ENTIDAD
                echo '
                    <tr>
                        <td class="td-entidad" style="width: 15%;"><span>' . $item['ruc'] . '-' . $item['razon_social'] . '</span></td>
                        <td class="td-entidad" style="width: 40%;"></td>
                        <td class="td-entidad" style="width: 15%;"></td>
                        <td class="td-entidad" style="width: 15%;"></td>
                        <td class="td-entidad" style="width: 15%;"></td>
                    </tr>
                ';
            }

            $titulo = 'FACTURA CRÉDITO PROVEEDORES; ';
            $total = $item['total'];
            isset($saldo) || $saldo = 0;

            $saldo += $total;
            $totalDebeActual += $total;
            $totalDebeItem += $total;
            $totalDebeGeneral += $total;

            echo '
                <tr>
                    <td class="td" style="width: 15%;"><span>' . Yii::$app->formatter->asDate($item['fecha_emision'], 'dd-MM-Y') . '<span style="color: #ffffff">------</span>' . $item['nro_factura'] . '</span></td>
                    <td class="td" style="width: 40%;"><span>' . '<span style="color: #ffffff">------</span>' . $titulo . $item['nro_factura'] . '</span></td>
                    <td class="td" style="text-align: right; width: 15%;"><span>' . ValorHelpers::numberFormatMonedaSensitive($total) . '</span></td>
                    <td class="td" style="text-align: right; width: 15%;">0</td>
                    <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
                </tr>
            ';

            #recibos
            foreach ($data['recibos'][$item['factura_id']] as $recibo) {
                $totalRecibo = -1 * $recibo['total_recibo'];

                $saldo += $totalRecibo;
                $totalHaberActual += abs($totalRecibo);
                $totalHaberItem += abs($totalRecibo);
                $totalHaberGeneral += abs($totalRecibo);
                $numeroRecibo = $recibo['numero_recibo'];

                IF ($numeroRecibo == '7333')
                    Yii::warning("total haber actual: {$totalHaberActual}, total haber item: {$totalHaberItem}");

                echo '
                    <tr>
                        <td class="td" style="width: 15%;"><span>' . Yii::$app->formatter->asDate($item['fecha_emision'], 'dd-MM-Y') . '<span style="color: #ffffff">------</span>' . $item['nro_factura'] . '</span></td>
                        <td class="td" style="width: 40%;"><span>' . '<span style="color: #ffffff">------</span>' . 'PAGO COMPROB. Nº; ' . $item['nro_factura'] . ' s/recibo Nº: ' . $numeroRecibo . '</span></td>
                        <td class="td" style="text-align: right; width: 15%;">0</td>
                        <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive(abs($totalRecibo)) . '</td>
                        <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
                    </tr>
                ';
            }

            #notas
            foreach ($data['notas'][$item['factura_id']] as $nota) {
                $titulo = "NOTA CRÉDITO {$nota['nro_nota']} RECIBIDAS; ";
                $total = -1 * $nota['total'];

                $saldo += $total;
                $totalHaberActual += abs($total);
                $totalHaberItem += abs($total);
                $totalHaberGeneral += abs($total);

                echo '
                    <tr>
                        <td class="td" style="width: 15%;"><span>' . Yii::$app->formatter->asDate($item['fecha_emision'], 'dd-MM-Y') . '<span style="color: #ffffff">------</span>' . $item['nro_factura'] . '</span></td>
                        <td class="td" style="width: 40%;"><span>' . '<span style="color: #ffffff">------</span>' . $titulo . $item['nro_factura'] . '</span></td>
                        <td class="td" style="text-align: right; width: 15%;">0</td>
                        <td class="td" style="text-align: right; width: 15%;"><span>' . ValorHelpers::numberFormatMonedaSensitive(abs($total)) . '</span></td>
                        <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
                    </tr>
                ';
            }

            #retenciones
            foreach ($data['retenciones'][$item['factura_id']] as $retencion) {
                $titulo = "RETENCION {$retencion['nro_retencion']} EMITIDA; ";
                $total = -1 * $retencion['total_retencion'];

                $saldo += $total;
                $totalHaberActual += abs($total);
                $totalHaberItem += abs($total);
                $totalHaberGeneral += abs($total);

                echo '
                    <tr>
                        <td class="td" style="width: 15%;"><span>' . Yii::$app->formatter->asDate($item['fecha_emision'], 'dd-MM-Y') . '<span style="color: #ffffff">------</span>' . $item['nro_factura'] . '</span></td>
                        <td class="td" style="width: 40%;"><span>' . '<span style="color: #ffffff">------</span>' . $titulo . $item['nro_factura'] . '</span></td>
                        <td class="td" style="text-align: right; width: 15%;">0</td>
                        <td class="td" style="text-align: right; width: 15%;"><span>' . ValorHelpers::numberFormatMonedaSensitive(abs($total)) . '</span></td>
                        <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
                    </tr>
                ';
            }

            //Total por factura
            echo '
                <tr>
                    <td class="td" style="width: 15%;"><span></span></td>
                    <td class="td" style="width: 40%;"><span></span></td>
                    <td class="td" style="text-align: right; width: 15%; border-top: dotted 1px #000000; border-bottom: dotted 1px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeItem) . '</td>
                    <td class="td" style="text-align: right; width: 15%; border-top: dotted 1px #000000; border-bottom: dotted 1px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberItem) . '</td>
                    <td class="td" style="text-align: right; width: 15%;"></td>
                </tr>
            ';
        }

        //En la última itereación falta agregar el último item
        if ($entidadActual != null) {
            echo '
            <tr>
                <td class="td" style="width: 15%;"><span></span></td>
                <td class="td" style="width: 40%;"><span></span></td>
                <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeActual) . '</td>
                <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberActual) . '</td>
                <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
            </tr>
        ';
        }

        //Total general
        echo '
        <tr>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr>
            <td class="td" style="width: 15%; border-bottom: 4px double;"><span><b>Total general:</b></span></td>
            <td class="td" style="width: 40%;"><span></span></td>
            <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeGeneral) . '</td>
            <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberGeneral) . '</td>
            <td class="td" style="text-align: right; width: 15%;"></td>
        </tr>
    ';
    } catch (Exception $exception) {
        //throw $exception;
        Yii::$app->session->set('error', $exception->getMessage());
    }
    ?>

</table>