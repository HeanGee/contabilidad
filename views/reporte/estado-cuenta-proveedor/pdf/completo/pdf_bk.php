<table style="undefined;table-layout: fixed; width: 100%">
    <?php

    use common\helpers\ValorHelpers;

    try {
        $entidadActual = null;
        $totalDebeGeneral = 0;
        $totalHaberGeneral = 0;
        foreach ($data as $item) {
            $totalDebeItem = 0;
            $totalHaberItem = 0;
            if ($item['razon_social'] != $entidadActual) {
                if ($entidadActual != null) {
                    //Cuando se cambia la entidad, se coloca totales por entidad
                    echo '
                    <tr>
                        <td class="td" style="width: 15%;"><span></span></td>
                        <td class="td" style="width: 40%;"><span></span></td>
                        <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeActual) . '</td>
                        <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberActual) . '</td>
                        <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
                    </tr>
                ';
                }

                $entidadActual = $item['razon_social'];
                $totalDebeActual = 0;
                $totalHaberActual = 0;
                $saldo = 0;
                //TITULO ENTIDAD
                echo '
                <tr>
                    <td class="td-entidad" style="width: 15%;"><span>' . $item['ruc'] . '-' . $item['razon_social'] . '</span></td>
                    <td class="td-entidad" style="width: 40%;"></td>
                    <td class="td-entidad" style="width: 15%;"></td>
                    <td class="td-entidad" style="width: 15%;"></td>
                    <td class="td-entidad" style="width: 15%;"></td>
                </tr>
            ';
            }

            if ($item['compra_tipo'] == 'factura') {
                $titulo = 'FACTURA CRÉDITO PROVEEDORES; ';
                $total = $item['total'];
            } elseif ($item['compra_tipo'] == 'nota_credito') {
                $titulo = 'NOTA CRÉDITO RECIBIDAS; ';
                $total = -1 * $item['total'];
            } elseif ($item['compra_tipo'] == 'nota_debito') {
                $titulo = 'NOTA DÉBITO EMITIDAS; ';
                $total = $item['total'];
            }
            $saldo += $total;
            $totalDebeActual += (in_array($item['compra_tipo'], ['factura', 'nota_debito'])) ? abs($total) : 0;
            $totalDebeItem += (in_array($item['compra_tipo'], ['factura', 'nota_debito'])) ? abs($total) : 0;
            $totalDebeGeneral += (in_array($item['compra_tipo'], ['factura', 'nota_debito'])) ? abs($total) : 0;

            $totalHaberActual += (in_array($item['compra_tipo'], ['factura', 'nota_debito'])) ? 0 : abs($total);
            $totalHaberItem += (in_array($item['compra_tipo'], ['factura', 'nota_debito'])) ? 0 : abs($total);
            $totalHaberGeneral += (in_array($item['compra_tipo'], ['factura', 'nota_debito'])) ? 0 : abs($total);

            if ($item['compra_tipo'] == 'factura')
                echo '
            <tr>
                <td class="td" style="width: 15%;"><span>' . Yii::$app->formatter->asDate($item['fecha_emision'], 'dd-MM-Y') . '<span style="color: #ffffff">------</span>' . $item['nro_factura'] . '</span></td>
                <td class="td" style="width: 40%;"><span>' . '<span style="color: #ffffff">------</span>' . $titulo . $item['nro_factura'] . '</span></td>
                <td class="td" style="text-align: right; width: 15%;"><span>' . ValorHelpers::numberFormatMonedaSensitive($total) . '</span></td>
                <td class="td" style="text-align: right; width: 15%;">0</td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
            </tr>
        ';
            else
                echo '
            <tr>
                <td class="td" style="width: 15%;"><span>' . Yii::$app->formatter->asDate($item['fecha_emision'], 'dd-MM-Y') . '<span style="color: #ffffff">------</span>' . $item['nro_factura'] . '</span></td>
                <td class="td" style="width: 40%;"><span>' . '<span style="color: #ffffff">------</span>' . $titulo . $item['nro_factura'] . '</span></td>
                <td class="td" style="text-align: right; width: 15%;">0</td>
                <td class="td" style="text-align: right; width: 15%;"><span>' . ValorHelpers::numberFormatMonedaSensitive(abs($total)) . '</span></td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
            </tr>
        ';

            $totalRecibo = 0;
            $numeroRecibo = '';
            if ($item['fecha_recibo'] != null) {
                if ($item['compra_tipo'] == 'nota_credito') {
                    $totalRecibo = -1 * $item['total_recibo'];
                } else {
                    $totalRecibo = $item['total_recibo'];
                }

                $saldo -= $totalRecibo;
                $totalHaberActual += $totalRecibo;
                $totalHaberItem += $totalRecibo;
                $totalHaberGeneral += $totalRecibo;
                $numeroRecibo = $item['numero_recibo'];
            }

            if ($numeroRecibo != '')  #solamente si hay recibo
                echo '
                <tr>
                    <td class="td" style="width: 15%;"><span>' . Yii::$app->formatter->asDate($item['fecha_emision'], 'dd-MM-Y') . '<span style="color: #ffffff">------</span>' . $item['nro_factura'] . '</span></td>
                    <td class="td" style="width: 40%;"><span>' . '<span style="color: #ffffff">------</span>' . 'PAGO COMPROBANTE Nº; ' . $item['nro_factura'] . ' s/recibo Nº: ' . $numeroRecibo . '</span></td>
                    <td class="td" style="text-align: right; width: 15%;">0</td>
                    <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($totalRecibo) . '</td>
                    <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
                </tr>
            ';


            //Total por factura
            if ($item['compra_tipo'] == 'factura')
                echo '
                <tr>
                    <td class="td" style="width: 15%;"><span></span></td>
                    <td class="td" style="width: 40%;"><span></span></td>
                    <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeItem) . '</td>
                    <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberItem) . '</td>
                    <td class="td" style="text-align: right; width: 15%;"></td>
                </tr>
            ';
            else
                echo '
                <tr>
                    <td class="td" style="width: 15%;"><span></span></td>
                    <td class="td" style="width: 40%;"><span></span></td>
                    <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeItem) . '</td>
                    <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberItem) . '</td>
                    <td class="td" style="text-align: right; width: 15%;"></td>
                </tr>
            ';
        }

        //En la última itereación falta agregar el último item
        if ($entidadActual != null) {
            echo '
            <tr>
                <td class="td" style="width: 15%;"><span></span></td>
                <td class="td" style="width: 40%;"><span></span></td>
                <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeActual) . '</td>
                <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberActual) . '</td>
                <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($saldo) . '</td>
            </tr>
        ';
        }

        //Total general
        echo '
        <tr>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
            <td class="td"></td>
        </tr>
        <tr>
            <td class="td" style="width: 15%; border-bottom: 4px double;"><span><b>Total general:</b></span></td>
            <td class="td" style="width: 40%;"><span></span></td>
            <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalDebeGeneral) . '</td>
            <td class="td" style="text-align: right; width: 15%; border-top: solid 2px #000000; border-bottom: solid 2px #000000;">' . ValorHelpers::numberFormatMonedaSensitive($totalHaberGeneral) . '</td>
            <td class="td" style="text-align: right; width: 15%;"></td>
        </tr>
    ';
    } catch (Exception $exception) {
        Yii::$app->session->set('error', $exception->getMessage());
    }
    ?>

</table>