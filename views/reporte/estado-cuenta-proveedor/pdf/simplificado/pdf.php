<table style="undefined;table-layout: fixed; width: 100%">
    <?php

    use common\helpers\ValorHelpers;

    $entidadActual = null;
    $totalDebeGeneral = 0;
    $totalHaberGeneral = 0;
    $arrayResult = [];
    foreach ($data['facturas'] as $item) {
        $totalDebeItem = 0;
        $totalHaberItem = 0;

        if ($item['razon_social'] != $entidadActual) {
            $entidadActual = $item['razon_social'];
            $totalDebeActual = 0;
            $totalHaberActual = 0;
            $saldo = 0;
        }

        #if ($item['compra_tipo'] == 'factura') {
        $total = $item['total'];
        #} elseif ($item['compra_tipo'] == 'nota_credito') {
        #    $total = -1 * $item['total'];
        #} elseif ($item['compra_tipo'] == 'nota_debito') {
        #    $total = $item['total'];
        #}

        $saldo += $total;
        $totalDebeActual += $total;
        $totalDebeItem += $total;
        $totalDebeGeneral += $total;

        ///
        #recibos
        $totalRecibo = 0;
        foreach ($data['recibos'][$item['factura_id']] as $recibo) {
            $totalRecibo = -1 * $recibo['total_recibo'];

            $saldo += $totalRecibo;
            $totalHaberActual += abs($totalRecibo);
            $totalHaberItem += abs($totalRecibo);
            $totalHaberGeneral += abs($totalRecibo);
            $numeroRecibo = $recibo['numero_recibo'];
        }

        #notas
        foreach ($data['notas'][$item['factura_id']] as $nota) {
            $titulo = "NOTA CRÉDITO {$nota['nro_nota']} RECIBIDAS; ";
            $total = -1 * $nota['total'];

            $saldo += $total;
            $totalHaberActual += abs($total);
            $totalHaberItem += abs($total);
            $totalHaberGeneral += abs($total);
        }

        #retenciones
        foreach ($data['retenciones'][$item['factura_id']] as $retencion) {
            $titulo = "RETENCION {$retencion['nro_retencion']} EMITIDA; ";
            $total = -1 * $retencion['total_retencion'];

            $saldo += $total;
            $totalHaberActual += abs($total);
            $totalHaberItem += abs($total);
            $totalHaberGeneral += abs($total);
        }
        ///

//        $totalRecibo = 0;
//        $numeroRecibo = '';
//        if ($item['fecha_recibo'] != null) {
//            if ($item['compra_tipo'] == 'nota_credito') {
//                $totalRecibo = -1 * $item['total_recibo'];
//            } else {
//                $totalRecibo = $item['total_recibo'];
//            }
//
//            $saldo -= $totalRecibo;
//            $totalHaberActual += $totalRecibo;
//            $totalHaberItem += $totalRecibo;
//            $totalHaberGeneral += $totalRecibo;
//            $numeroRecibo = $item['numero_recibo'];
//        }

        if (isset($arrayResult[$item['entidad_id']]))
            $arrayResult[$item['entidad_id']] = ['ruc' => $item['ruc'], 'razon_social' => $item['razon_social'], 'total_debe' => $arrayResult[$item['entidad_id']]['total_debe'] + $totalDebeItem, 'total_haber' => $arrayResult[$item['entidad_id']]['total_haber'] + $totalHaberItem, 'saldo' => $arrayResult[$item['entidad_id']]['saldo'] + $saldo];
        else
            $arrayResult[$item['entidad_id']] = ['ruc' => $item['ruc'], 'razon_social' => $item['razon_social'], 'total_debe' => $totalDebeItem, 'total_haber' => $totalHaberItem, 'saldo' => $saldo];
    }

    foreach ($arrayResult as $id => $value) {
        //Cuando se cambia la entidad, se coloca totales por entidad
        echo '
            <tr>
                <td class="td" style="width: 15%;"><span>' . $value['ruc'] . '</span></td>
                <td class="td" style="width: 40%;"><span>' . $value['razon_social'] . '</span></td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_debe']) . '</td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_haber']) . '</td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_debe'] - $value['total_haber']) . '</td>
            </tr>
        ';
    }

    #notas de credito sin factura
    $entidadActual = null;
    $arrayResult = [];
    foreach ($data['notasSFactura'] as $key => $item) {
        $totalHaberItem = 0;
        if ($key == 0)
            echo '
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: left; border-bottom: double"><strong>NOTAS SIN FACTURA</strong></td>
                    </tr>
                ';

        if ($item['razon_social'] != $entidadActual) {
            $entidadActual = $item['razon_social'];
            $totalDebeActual = 0;
            $totalHaberActual = 0;
            $saldo = 0;
        }

        $titulo = "NOTA CREDITO {$item['nro_factura']} SIN FACTURA; ";
        $total = -1 * $item['total'];

        $saldo += $total;
        $totalHaberActual += abs($total);
        $totalHaberItem += abs($total);
        $totalHaberGeneral += abs($total);

        if (isset($arrayResult[$item['entidad_id']]))
            $arrayResult[$item['entidad_id']] = ['ruc' => $item['ruc'], 'razon_social' => $item['razon_social'], 'total_debe' => $totalDebeActual, 'total_haber' => $arrayResult[$item['entidad_id']]['total_haber'] + $totalHaberItem, 'saldo' => $arrayResult[$item['entidad_id']]['saldo'] + $saldo];
        else
            $arrayResult[$item['entidad_id']] = ['ruc' => $item['ruc'], 'razon_social' => $item['razon_social'], 'total_debe' => $totalDebeActual, 'total_haber' => $totalHaberItem, 'saldo' => $saldo];
    }

    foreach ($arrayResult as $id => $value) {
        //Cuando se cambia la entidad, se coloca totales por entidad
        echo '
            <tr>
                <td class="td" style="width: 15%;"><span>' . $value['ruc'] . '</span></td>
                <td class="td" style="width: 40%;"><span>' . $value['razon_social'] . '</span></td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_debe']) . '</td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_haber']) . '</td>
                <td class="td" style="text-align: right; width: 15%;">' . ValorHelpers::numberFormatMonedaSensitive($value['total_debe'] - $value['total_haber']) . '</td>
            </tr>
        ';
    }
    ?>
</table>
<div style="width: 100%; border-top: solid 2px #000000;"></div>
<table style="undefined;table-layout: fixed; width: 100%">
    <tr>
        <td class="td" style="width: 15%;"></td>
        <td class="td" style="width: 40%;"></td>
        <td class="td"
            style="text-align: right; width: 15%;"><?= ValorHelpers::numberFormatMonedaSensitive($totalDebeGeneral) ?></td>
        <td class="td"
            style="text-align: right; width: 15%;"><?= ValorHelpers::numberFormatMonedaSensitive($totalHaberGeneral) ?></td>
        <td class="td"
            style="text-align: right; width: 15%;"><?= ValorHelpers::numberFormatMonedaSensitive($totalDebeGeneral - $totalHaberGeneral) ?></td>
    </tr>
</table>
<div style="width: 100%; border-top: solid 2px #000000;"></div>
