$('button#btn_submit, button#bajar_xls').click(function () {
    $('a.close').click();
});

$(document).ready(function () {
    //
    $('#reporte-criterio1').trigger('change');

    // Cada vez que haga submit, renderiza en nueva pestanha.
    document.getElementById('reporte-form').setAttribute("target", "_blank");
    // agrupar();

    $('#btn_submit').on('click', function () {
        $('#reporte-tipo_doc').val('pdf');
    });
    $('#bajar_xls').on('click', function () {
        $('#reporte-tipo_doc').val('xls');
        $('#reporte-form').submit();
    });
    $('#importar_marangatu').on('click', function () {
        $('#reporte-tipo_doc').val('import-marangatu');
        $('#reporte-form').submit();
    });

    // $('select[id$="filtro"]').trigger('change'); // para ocultar o mostrar campos de filtrado por creado/modificado

    let span = "<span class='popuptrigger text-info glyphicon glyphicon-info-sign' style='padding: 0 4px;'></span>";
    let label = $('label[for="reporte-tipo_comprobante"]');
    let html = label.html();
    // label.html(html + "&nbsp;&nbsp;" + span);
    let msg = 'Si elige la opción <strong>Ambos pero agrupados</strong>, las configuraciones <u>Agrupar por prefijo?</u> y <u>Separar por Cuenta Principal</u> no tendrán efecto. Esto es para evitar separar o agrupar por más de un criterio.';
    // applyPopOver($('span.popuptrigger'), 'Tenga en cuenta que...', msg, {'placement': 'left'});
});

$(document).on('change', 'select[id*="criterio"]', function (evt) {
    let id = $(this).attr('id');
    let element = $('#' + id + 'fecha');

    //Limpiar campos y desactivar.
    if ($(this).val() === '' || ['factura.id', 'entidad.razon_social', 'cta.nombre', 'factura.nro_factura',].includes($(this).val())) {
        element.val('').trigger('change');
        element.val('').prop('disabled', true);
    } else if (['factura.creado', 'factura.fecha_emision'].includes($(this).val())) {
        element.val('').trigger('change');
        element.val('').prop('disabled', false);
    }

    //Ocultar o mostrar la fila de fechas.
    if ($(':input[id$="fecha"]:input[id*="criterio"]:disabled').length === 3) {
        element.parent().parent().parent().fadeOut();
    } else {
        element.parent().parent().parent().fadeIn();

        var today = new Date();
        var mm = today.getMonth() - 1,
            mm = (mm < 0 ? 11 : mm) + 1,
            mm = (mm < 10 ? '0' : '') + mm;
        today = new Date(today.getFullYear(), mm - 1, 1);
        var yyyy = today.getFullYear();

        today = '01-' + mm + '-' + yyyy + ' - ' + new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate() + '-' + mm + '-' + yyyy;
        element.val(today).trigger('change');
    }
});

$(document).on('change', 'select[id$="resumen_plantilla"]', function (evt) {
    let element = $('select[id$="separar_visualmente"]');
    if ($(this).val() === 'si') {
        element.val('').trigger('change');
        element.prop('disabled', true);
    } else if ($(this).val() === 'no') {
        element.prop('disabled', false);
    }
});