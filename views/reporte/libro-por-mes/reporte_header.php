<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 3/13/18
 * Time: 5:58 PM
 */


use backend\models\Empresa; ?>
<h3 class="reporte-titulo-h3"><u>Libro <?= $tipoLibro ?> por Mes</u></h3>
<table class="table-header table-condensed">
    <tr>
        <td class="td-col1">DEL:</td>
        <td class="td-col2"><?= $desde ?></td>
        <?php
        if (isset($id_desde) && $id_desde != '') : ?>
            <td class='td-col'>Código desde:</td>
            <td class='td-col' style="text-align: left; font-weight: normal !important;"><?= $id_desde ?></td>
        <?php else: ?>
            <td class='td-col'></td>
            <td class='td-col'></td>
        <?php endif; ?>
        <td class="td-col3">
            <?php
            $razon_social = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->razon_social;
            echo $razon_social;
            ?>
        </td>
    </tr>
    <tr>
        <td class="td-col1">AL:</td>
        <td class="td-col2"><?= $hasta ?></td>
        <?php
        if (isset($id_hasta) && $id_hasta != '') : ?>
            <td class='td-col'>Código hasta:</td>
            <td class='td-col' style="text-align: left; font-weight: normal !important;"><?= $id_hasta ?></td>
        <?php else: ?>
            <td class='td-col'></td>
            <td class='td-col'></td>
        <?php endif; ?>
        <td class="td-col3">
            <?php
            $ruc = Empresa::findOne(['id' => Yii::$app->session->get('core_empresa_actual')])->ruc;
            $dv = Empresa::getDigitoVerificador($ruc);
            echo "{$ruc}-{$dv}"
            ?>
        </td>
    </tr>
</table>

<table class="tg" style="undefined;table-layout: fixed; width: 1120px">
    <colgroup>
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
    </colgroup>
    <tr>
        <td class="tg-gh1y" style="width: 160px">MES</td>
        <td class="tg-gh1y" style="width: 160px">TOTAL</td>
        <td class="tg-gh1y" style="width: 160px">GRAVS 10.</td>
        <td class="tg-gh1y" style="width: 160px">IVA 10</td>
        <td class="tg-gh1y" style="width: 160px">GRAVS 5.</td>
        <td class="tg-gh1y" style="width: 160px">IVA 5</td>
        <td class="tg-gh1y" style="width: 160px">EXENT.</td>
    </tr>
</table>