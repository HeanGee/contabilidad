<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 2/3/18
 * Time: 5:15 PM
 */

?>

<table class="tg tg-body" style="undefined;table-layout: fixed; width: 1120px">
    <colgroup>
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
        <col style="width: 160px">
    </colgroup>
    <?php
    try {
        $mesToText = ['01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre',
            '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre'];
        $total = $grav10 = $grav5 = $iva10 = $iva5 = $exenta = 0;
        foreach ($dataProvider as $mes => $item) {
            $total += (float)$item['total'];
            $grav10 += (float)$item['gravada10'];
            $grav5 += (float)$item['gravada5'];
            $exenta += (float)$item['exenta'];
            $iva5 += (float)$item['iva5'];
            $iva10 += (float)$item['iva10'];
            ?>
            <tr>
                <td class="tg-gh1y" style="width: 160px"><?= $mesToText[$mes] ?></td>
                <td class="tg-gh1y monto"
                    style="width: 160px"><?= ($item['total']) ? number_format(round($item['total']), 0, ',', '.') : 0 ?></td>
                <td class="tg-gh1y monto"
                    style="width: 160px"><?= ($item['gravada10']) ? number_format(round($item['gravada10']), 0, ',', '.') : 0 ?></td>
                <td class="tg-gh1y monto"
                    style="width: 160px"><?= ($item['iva10']) ? number_format(round($item['iva10']), 0, ',', '.') : 0 ?></td>
                <td class="tg-gh1y monto"
                    style="width: 160px"><?= ($item['gravada5']) ? number_format(round($item['gravada5']), 0, ',', '.') : 0 ?></td>
                <td class="tg-gh1y monto"
                    style="width: 160px"><?= ($item['iva5']) ? number_format(round($item['iva5']), 0, ',', '.') : 0 ?></td>
                <td class="tg-gh1y monto"
                    style="width: 160px"><?= ($item['exenta']) ? number_format(round($item['exenta']), 0, ',', '.') : 0 ?></td>
            </tr>
        <?php };
    } catch (\Exception $exception) {
        echo "</tr>";
        \common\helpers\FlashMessageHelpsers::createWarningMessage($exception);
    }
    ?>
    <tr class="resume-row">
        <td class="tg-gh1y" style="width: 160px; font-weight: bold">TOTALES</td>
        <td class="tg-gh1y monto" style="width: 160px; font-weight: bold"><?= number_format($total, 0, ',', '.') ?></td>
        <td class="tg-gh1y monto"
            style="width: 160px; font-weight: bold"><?= number_format($grav10, 0, ',', '.') ?></td>
        <td class="tg-gh1y monto" style="width: 160px; font-weight: bold"><?= number_format($iva10, 0, ',', '.') ?></td>
        <td class="tg-gh1y monto" style="width: 160px; font-weight: bold"><?= number_format($grav5, 0, ',', '.') ?></td>
        <td class="tg-gh1y monto" style="width: 160px; font-weight: bold"><?= number_format($iva5, 0, ',', '.') ?></td>
        <td class="tg-gh1y monto"
            style="width: 160px; font-weight: bold"><?= number_format($exenta, 0, ',', '.') ?></td>
    </tr>
    <tr class="closing-row">
        <td colspan='7' class='tg-gh1y' style='font-style: italic; text-align: right'>LISTADO CONCLUÍDO</td>
    </tr>
</table>

