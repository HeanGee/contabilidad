<?php

use backend\modules\contabilidad\controllers\Reporte;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\Venta;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;

/* @var $model Reporte */
/* @var $this View */

$operacion = Yii::$app->request->queryParams['operacion'];
$this->title = 'Reporte por Entidad de ' . ucfirst($operacion);
$this->params['breadcrumbs'][] = $this->title;

$entidades = ($operacion == 'compra') ? Compra::getEntidadesEmpresaPeriodoActual() : Venta::getEntidadesEmpresaPeriodoActual();

$orderBy = [
    'id' => 'Orden de carga',
    'creado' => "Fecha de Creación",
    'modificado' => "Fecha de Última modificación",
    'fecha_emision' => 'Fecha de emisión',
    'nro_factura' => 'Número de factura',
    'ruc' => 'R.U.C',
    'razon_social' => 'Razón social',
];
?>
    <div class="venta-index">

<?php $form = ActiveForm::begin([
    'id' => 'reporte-form'
]); ?>

    <div class="text-right btn-toolbar">
        <?php if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro-por-cuentas')) {
            echo Html::submitButton('Ver PDF', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info pull-left']);
            echo Html::button('Ver EXCEL', ['id' => 'bajar_xls', 'formtarget' => '_blank', 'class' => 'btn btn-warning pull-left']);
            echo Html::a('Restablecer Filtro', ['reporte-libro-por-entidad', 'operacion' => $operacion], ['class' => 'btn btn-warning pull-left']);
        } ?>
    </div>

    <br/>

    <div>
        <?php

        try {
            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 6,
                        'attributes' => [
                            'cod_desde' => [
                                'type' => Form::INPUT_TEXT,
                            ],
                            'cod_hasta' => [
                                'type' => Form::INPUT_TEXT,
                            ],
                            '_entidades' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'columnOptions' => ['colspan' => '2'],
                                'options' => [
                                    'options' => [
                                        'placeholder' => 'Entidades...',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'multiple' => true,
                                        'data' => $entidades,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 6,
                        'attributes' => [
                            'moneda' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => Reporte::getMonedas(false),
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => ['allowClear' => false],
                                ],
                            ],
//                            'valorizar' => [
//                                'type' => Form::INPUT_WIDGET,
//                                'widgetClass' => Select2::className(),
//                                'options' => [
//                                    'data' => ['si' => "Si", 'no' => "No"],
//                                    'initValueText' => 'Todos',
//                                    'options' => [
//                                        'placeholder' => 'Por favor Seleccione Uno',
//                                    ],
//                                    'pluginOptions' => ['allowClear' => false],
//                                ],
//                            ],
                            'fecha_rango' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => DateRangePicker::className(),
                                'options' => [
                                    'model' => $model,
                                    'attribute' => 'fecha_rango',
                                    'convertFormat' => true,
                                    'language' => 'es',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'autoApply' => true,
                                        'locale' => ['format' => 'd-m-Y'],

                                    ],

                                ],
                            ],
                            'filtro' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => $orderBy,
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                    ],
                                ],
                                'label' => 'Filtro'
                            ],
                            'ascDesc' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => [SORT_ASC => 'Ascendente', SORT_DESC => 'Descendente'],
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => ['allowClear' => false],
                                ],
                            ],
                            'tipo_doc' => [
                                'type' => Form::INPUT_HIDDEN
                            ],
                        ]
                    ],
                ],
            ]);
        } catch (Exception $e) {
//            throw $e;
            FlashMessageHelpsers::createWarningMessage($e->getMessage());
            Yii::warning($e);
        } ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$script = <<<JS
$('#btn_submit').on('click', function () {
    $('#reporte-tipo_doc').val('pdf').trigger('change');
});

$('#bajar_xls').on('click', function () {
    $('#reporte-tipo_doc').val('xls').trigger('change');
    $('#reporte-form').submit();
});

$(document).ready(function () {
    // Cada vez que haga submit, renderiza en nueva pestanha.
    document.getElementById('reporte-form').setAttribute("target", "_blank");
});
JS;

$this->registerJs($script);
?>