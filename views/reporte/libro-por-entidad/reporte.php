<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 2/3/18
 * Time: 5:15 PM
 */

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\Venta;

/* @var $model \backend\modules\contabilidad\controllers\Reporte */

?>

<table class="tg2" style="undefined;table-layout: fixed; width: 1110px">
    <?php
    foreach ($dataProvider as $entidad_id => $facturas) {
        $cant = 0;
        $gravadas = 0;
        $impuestos = 0;
        $exentas = 0;
        $ret_ivas = 0;
        $ret_rentas = 0;
        $totales = 0;
        $base_imps = 0;
        $entidad = Entidad::findOne(['id' => $entidad_id]);
        $razon_social = isset($entidad) ? $entidad->razon_social : '';
        $ruc = isset($entidad) ? $entidad->ruc : '';
        $operacion = Yii::$app->request->getQueryParam('operacion');
        $text = ($operacion == 'compra' && $razon_social != '') ? 'PROVEEDOR:' :
            (($operacion == 'venta' && $razon_social != '') ? 'CLIENTE:' : 'DOCUMENTO ANULADO.');

        echo "<tr>";
        echo "<td style='font-size: 9px; color: #721c24' colspan='12'><strong>{$text} {$razon_social}</strong></td>";
        echo "</tr>";
        foreach ($facturas as $factura) {
            /** @var Compra|Venta $modelFactura */
            $modelFactura = $factura['factura'];
            $nro = ($operacion == 'compra') ? $modelFactura['nro_factura'] : "{$modelFactura['prefijo']}-{$modelFactura['nro_factura']}";
            $estado = $modelFactura['estado'];
            $cotizacion = ($operacion == 'compra') ? $modelFactura['cotizacion'] : $modelFactura['valor_moneda'];
            if ($cotizacion == '' || $model->valorizar == 'no') $cotizacion = 1;

            $fecha = $modelFactura['fecha_emision'];
            $fecha = implode('-', array_reverse(explode('-', $fecha)));
            $gravada10 = $factura['gravada10'] * $cotizacion;
            $gravada5 = $factura['gravada5'] * $cotizacion;
            $iva10 = $factura['iva10'] * $cotizacion;
            $iva5 = $factura['iva5'] * $cotizacion;
            $gravada = $gravada10 + $gravada5;
            $gravadas += $gravada;
            $porcentaje = ($iva10 > 0) ? '10' . (($iva5 > 0) ? ', 5' : '') : (($iva5 > 0) ? '5' : '');
            $impuesto = $iva5 + $iva10;
            $impuestos += $impuesto;
            $exenta = $factura['exenta'] * $cotizacion;
            $exentas += $exenta;
            $ret_iva = 0;
            $ret_ivas += $ret_iva;
            $ret_renta = 0;
            $ret_rentas += $ret_renta;
            $total = $factura['total'] * $cotizacion;
            $totales += $total;
            $base_imp = 0;
            $base_imps += $base_imp;

            {
                // FORMAT NUMBERS FOR VIEW
                $precision = 0;

                $gravada = number_format($gravada, $precision, ',', '.');
                $impuesto = number_format($impuesto, $precision, ',', '.');
                $exenta = number_format($exenta, $precision, ',', '.');
                $total = number_format($total, $precision, ',', '.');
                $ret_iva = number_format($ret_iva, $precision, ',', '.');
                $ret_renta = number_format($ret_renta, $precision, ',', '.');
                $base_imp = number_format($base_imps, $precision, ',', '.');
            }

            echo "<tr>";
            {
                echo "<td  style=\"width: 70px; text-align: center\" class=\"tg-gh1y2\" >{$nro}</td>";
                echo "<td  style=\"width: 54px; text-align: center\" class=\"tg-gh1y2\" >{$fecha}</td>";
                echo "<td  style=\"width: 250px;\" class=\"tg-gh1y2\" >{$razon_social}</td>";
                echo "<td  style=\"width: 44px; text-align: center\" class=\"tg-gh1y2\" >{$ruc}</td>";
                echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >{$gravada}</td>";
                echo "<td  style=\"width: 34px; text-align: center\" class=\"tg-gh1y2\" >{$porcentaje}</td>";
                echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >{$impuesto}</td>";
                echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >{$exenta}</td>";
                echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >0</td>";
                echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >0</td>";
                echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >{$total}</td>";
                echo "<td  style=\"width: 94px;\" class=\"tg-gh1y2 monto\" >0</td>";
            }
            echo "</tr>";
            $cant++;
        }
        $gravadas = ($model->valorizar == 'no') ? "- multimoneda -" : number_format($gravadas, $precision, ',', '.');
        $impuestos = ($model->valorizar == 'no') ? "- multimoneda -" : number_format($impuestos, $precision, ',', '.');
        $exentas = ($model->valorizar == 'no') ? "- multimoneda -" : number_format($exentas, $precision, ',', '.');
        $ret_ivas = ($model->valorizar == 'no') ? "- multimoneda -" : number_format($ret_ivas, $precision, ',', '.');
        $ret_rentas = ($model->valorizar == 'no') ? "- multimoneda -" : number_format($ret_rentas, $precision, ',', '.');
        $totales = ($model->valorizar == 'no') ? "- multimoneda -" : number_format($totales, $precision, ',', '.');
        $base_imps = ($model->valorizar == 'no') ? "- multimoneda -" : number_format($base_imps, $precision, ',', '.');
        echo "<tr class='resume-row'>";
        {
            echo "<td colspan='2' class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>Ítems</td>";
            echo "<td class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>{$cant}</td>";
            echo "<td class=\"tg-gh1y2\" style='text-align: center; font-weight: bold'>Sumas:</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$gravadas}</td>";
            echo "<td class=\"tg-gh1y2\"></td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$impuestos}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$exentas}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$ret_ivas}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$ret_rentas}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$totales}</td>";
            echo "<td class=\"tg-gh1y2 monto\" style='text-align: right; font-weight: bold'>{$base_imps}</td>";
        }
        echo "</tr>";
        echo "<tr><td colspan='12' style='padding-top: 10px;'></td></tr>";
    }
    echo '<tr class="closing-row">';
    {
        echo "<td colspan='12' class='tg-gh1y2' style='font-style: italic; text-align: right'>LISTADO CONCLUÍDO</td>";
    }
    echo '</tr>';
    ?>
</table>