<?php

/** @var $userByFactura array */
$x = microtime(true);
?>

<table class="tg" style="undefined;table-layout: fixed; width: 100%">
    <?php foreach ($dataProvider as $item) { ?>
        <tr>
            <td class="data-row"
                style="color: <?= ($item['agregado'] == 'si') ? "red" : "black" ?>;width: 12%; text-align: center;"><?= $item['nro_factura'] ?></td>
            <td class="data-row"
                style="color: <?= ($item['agregado'] == 'si') ? "red" : "black" ?>;width: 8%; text-align: center;"><?= $item['nro_timbrado'] ?></td>
            <td class="data-row"
                style="color: <?= ($item['agregado'] == 'si') ? "red" : "black" ?>;width: 13%; text-align: center;"><?= implode('-', array_reverse(explode('-', $item['fecha_emision']))) ?></td>
            <td class="data-row"
                style="color: <?= ($item['agregado'] == 'si') ? "red" : "black" ?>;width: 40%; text-align: <?= ($item['agregado'] == 'si') ? 'center' : 'left' ?>;"><?= $item['razon_social'] ?></td>
            <td class="data-row"
                style="color: <?= ($item['agregado'] == 'si') ? "red" : "black" ?>;width: 14%; text-align: <?= ($item['agregado'] == 'si') ? 'center' : 'right' ?>; padding-right: <?= ($item['total'] != 'NO-ENCONTRADO') ? '11px' : '0' ?>;"><?= ($item['total'] != 'NO-ENCONTRADO') ? number_format((float)$item['total'], 0, ',', '.') : $item['total'] ?></td>
            <td class="data-row"
                style="color: <?= ($item['agregado'] == 'si') ? "red" : "black" ?>;width: 13%; text-align: <?= ($item['agregado'] == 'si') ? 'center' : 'left' ?>; padding-left: 6px;"><?php if ($item['id'] != '' && isset($userByFactura[$item['id']])) echo $userByFactura[$item['id']]; elseif ($item['total'] == 'NO-ENCONTRADO') echo 'NO-ENCONTRADO';
                # hay algunas ventas que no tienen auditoria por haber sido migrados de preproduccion.  ?></td>
        </tr>
    <?php }
    $y = microtime(true);
    Yii::warning(($y - $x) . ' rendering');
    ?>
</table>