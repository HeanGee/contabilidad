<?php

use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $model \backend\modules\contabilidad\controllers\Reporte */
/* @var $this \yii\web\View */

$this->title = "Auditoría Libro Venta";
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="venta-index">

<?php $form = ActiveForm::begin([
    'id' => 'reporte-form'
]); ?>

    <div class="text-right btn-toolbar">
        <?php if (PermisosHelpers::getAcceso('contabilidad-reporte-auditoria-venta')) {
            echo Html::submitButton('Generar', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info pull-left']);
        } ?>
    </div>

    <br/>

    <div>
        <?php

        try {
            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 8,
                        'attributes' => [
                            'cod_desde' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '1'],
                            ],
                            'cod_hasta' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '1'],
                            ],
                            'fecha_rango' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => DateRangePicker::className(),
                                'columnOptions' => ['colspan' => '2'],
                                'options' => [
                                    'model' => $model,
                                    'attribute' => 'fecha_rango',
                                    'convertFormat' => true,
                                    'language' => 'es',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'autoApply' => true,
                                        'locale' => ['format' => 'd-m-Y'],

                                    ],

                                ],
                            ],
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 6,
                        'attributes' => [
                            'tipo_doc' => [
                                'type' => Form::INPUT_HIDDEN
                            ],
                        ]
                    ],
                ],
            ]);
        } catch (Exception $e) {
//            throw $e;
            FlashMessageHelpsers::createWarningMessage($e->getMessage());
            Yii::warning($e);
        } ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$script = <<<JS
$('#btn_submit').on('click', function () {
    $('#reporte-tipo_doc').val('pdf').trigger('change');
});

$('#bajar_xls').on('click', function () {
    $('#reporte-tipo_doc').val('xls').trigger('change');
    $('#reporte-form').submit();
});

$(document).ready(function () {
    // Cada vez que haga submit, renderiza en nueva pestanha.
    document.getElementById('reporte-form').setAttribute("target", "_blank");
});
JS;

$this->registerJs($script);
?>