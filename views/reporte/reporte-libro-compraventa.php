<?php

use backend\helpers\HtmlHelpers;
use backend\models\Moneda;
use backend\modules\contabilidad\assets\ReporteCompraVentaAsset;
use backend\modules\contabilidad\controllers\Reporte;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\Venta;
use common\helpers\FlashMessageHelpsers;
use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model Reporte */

$this->title = 'Reporte de ' . ucfirst($model->operacion) . 's';
$this->params['breadcrumbs'][] = $this->title;
$operacion = Yii::$app->request->queryParams['operacion'];

ReporteCompraVentaAsset::register($this);
?>
    <div class="venta-index">

<?php $form = ActiveForm::begin([
    'id' => 'reporte-form'
]); ?>

    <div class="text-right btn-toolbar">
        <?php if (PermisosHelpers::getAcceso('contabilidad-reporte-reporte-libro')) {
            echo Html::submitButton('Ver PDF', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info pull-left']);
            echo Html::button('Ver EXCEL', ['id' => 'bajar_xls', 'formtarget' => '_blank', 'class' => 'btn btn-warning pull-left']);
            echo Html::button('Importar a Marangatu', ['id' => 'importar_marangatu', 'class' => 'btn btn-success pull-left']);
            echo Html::a('Restablecer filtro', ['reporte-libro', 'operacion' => $operacion], ['class' => 'btn btn-primary pull-right']);
        } ?>
    </div>

    <br/>

    <div>
        <?php
        $data = [
            'id' => 'Orden de carga',
            'creado' => "Fecha de Creación",
            'modificado' => "Fecha de Última modificación",
            'fecha_emision__id' => 'Fecha y Orden de carga',
            'nro_factura' => 'Número de factura',
            'cuenta_principal' => 'Cuentas Principales',
        ];

        $query = new Query();
        $query->distinct('ob.id');
        $query->select(['ob.id', 'ob.nombre'])
            ->from("cont_factura_{$operacion} as fa")
            ->leftJoin('cont_obligacion as ob', 'ob.id = fa.obligacion_id')
            ->where([
                'fa.empresa_id' => Yii::$app->session->get("core_empresa_actual"),
                'fa.periodo_contable_id' => Yii::$app->session->get("core_empresa_actual_pc"),
                'fa.estado' => 'vigente',
            ]);
        $optionsObligacions = $query->all();

        if ($model->operacion == 'compra') {
            $data['entidad_id__nro_factura'] = 'Proveedor y Nro de factura';
        } elseif ($model->operacion == 'venta')
            $data['entidad_id__nro_factura'] = 'Cliente y Nro de factura';

        $monedas = Moneda::getMonedas();
        $moneda_todos['todos'] = 'TODOS';
        $monedas = array_merge($moneda_todos, $monedas);

        try {
            $query = null;
            if ($operacion == 'venta')
                $query = Venta::find()->alias('venta')->joinWith('entidad as entidad')->where([
                    'venta.estado' => 'vigente',
                    'venta.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                    'venta.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
                ])->select([
                    'entidad_id' => 'venta.entidad_id', // solo para que no se queje
                    'text' => 'entidad.razon_social',
                ])->distinct()->orderBy('text'); // por defecto aplica sobre la primera col especificada en select([...]): entidad_id
            else
                $query = Compra::find()->alias('compra')->joinWith('entidad as entidad')->where([
                    'compra.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
                    'compra.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc'),
                ])->select([
                    'entidad_id' => 'compra.entidad_id',
                    'text' => 'entidad.razon_social',
                ])->distinct()->orderBy('text');

            $entidades = ArrayHelper::map($query->asArray()->all(), 'entidad_id', 'text');

            $criterioData = [
                'factura.id' => "ORDEN DE CARGA",
                'factura.creado' => "CREADO",
                'factura.fecha_emision' => "FECHA DE EMISION",
                'entidad.razon_social' => "RAZON SOCIAL",
                'cta.nombre' => "CUENTA PRINCIPAL",
                'factura.nro_factura' => "NUMERO DE FACTURA",
            ];

            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'tipo_comprobante' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => ArrayHelper::map(TipoDocumento::getTiposDocumentoConId(($operacion == 'compra' ? 'proveedor' : 'cliente'), null, true), 'id', 'text'),
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                        'multiple' => true,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 6,
                        'attributes' => [
                            'paper_size' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => Reporte::getPaperSizes(),
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'label' => 'Hoja'
                            ],
                            'moneda' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => Reporte::getMonedas(false),
                                    'options' => [
                                        'placeholder' => 'Todos',
                                    ],
                                    'pluginOptions' => ['allowClear' => false],
                                ],
                            ],
                            'valorizar' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => ['si' => "Si", 'no' => "No"],
                                    'initValueText' => 'Todos',
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                            ],
                            'cod_desde' => [
                                'type' => Form::INPUT_TEXT,
                            ],
                            'cod_hasta' => [
                                'type' => Form::INPUT_TEXT,
                            ],
                            'para_iva' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => ['*' => "Todos", 'si' => "Si", 'no' => "No"],
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => ['allowClear' => false],
                                ],
                            ],
                            'tipo_doc' => [
                                'type' => Form::INPUT_HIDDEN
                            ],
                        ]
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 5,
                        'attributes' => [
                            'incluir_no_vigente' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'data' => ['si' => "Si", 'no' => "No"],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                    ],
                                ],
                            ],
                            'entidad_id' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'data' => $entidades,
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                ],
                            ],
                            'resumen_plantilla' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => ['si' => "Si", 'no' => "No"],
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => ['allowClear' => false],
                                ],
                            ],
                            'separar_visualmente' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => ['prefijo' => "PREFIJO", 'cuenta_principal' => "CUENTA PRINCIPAL (NOMBRE)"],
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'label' => "Separar Visualmente Por",
                            ],
                            'obligaciones' => [
                                'type' => Form::INPUT_WIDGET,
                                'columnOptions' => ['colspan' => '2'],
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => ArrayHelper::map($optionsObligacions, 'id', 'nombre'),
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => [
                                        'multiple' => true,
                                        'allowClear' => true,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 3,
                        'attributes' => [
                            'criterio1' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => $criterioData,
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                ],
                            ],
                            'criterio2' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => $criterioData,
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                ],
                            ],
                            'criterio3' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'data' => $criterioData,
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 3,
                        'attributes' => [
                            'criterio1fecha' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => DateRangePicker::className(),
                                'options' => [
                                    'disabled' => (!in_array($model->criterio1, ['factura.fecha_emision', 'factura.creado'])) ? true : false,
                                    'model' => $model,
                                    'attribute' => 'fecha_rango',
                                    'convertFormat' => true,
                                    'language' => 'es',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'autoApply' => true,
                                        'locale' => ['format' => 'd-m-Y'],

                                    ],

                                ],
                            ],
                            'criterio2fecha' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => DateRangePicker::className(),
                                'options' => [
                                    'model' => $model,
                                    'disabled' => (!in_array($model->criterio2, ['factura.fecha_emision', 'factura.creado'])) ? true : false,
                                    'attribute' => 'fecha_rango',
                                    'convertFormat' => true,
                                    'language' => 'es',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'autoApply' => true,
                                        'locale' => ['format' => 'd-m-Y'],

                                    ],

                                ],
                            ],
                            'criterio3fecha' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => DateRangePicker::className(),
                                'options' => [
                                    'disabled' => (!in_array($model->criterio3, ['factura.fecha_emision', 'factura.creado'])) ? true : false,
                                    'model' => $model,
                                    'attribute' => 'fecha_rango',
                                    'convertFormat' => true,
                                    'language' => 'es',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'autoApply' => true,
                                        'locale' => ['format' => 'd-m-Y'],
                                    ],

                                ],
                            ],
                        ],
                    ],

                ],
            ]);

            $content = <<<HTML
Si en el reporte no aparece una o más facturas, es posible que ninguno de los detalles&nbsp;
de esa factura esté configurado como <strong>cuenta principal</strong>, que a su vez es debido a las plantillas&nbsp;
que fueron creadas antes de la solicitud de implementación de las cuentas principales. 
HTML;
            $infoDiv = HtmlHelpers::BootstrapInfoAlert('Información!', $content);
//            echo Yii::$app->request->isGet ? "<br/><br/>{$infoDiv}" : null; // Jose pidio quitar.

        } catch (Exception $e) {
//            throw $e;
            FlashMessageHelpsers::createWarningMessage($e->getMessage());
            Yii::warning($e);
        } ?>
    </div>

<?php ActiveForm::end(); ?>