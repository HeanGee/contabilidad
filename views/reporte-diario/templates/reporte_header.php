<?php
/* @var $this yii\web\View */
/* @var $fecha_obj DateTime */
/* @var $data \yii\db\ActiveRecord[] */
/* @var $del string */

/* @var $al string */

use backend\models\Empresa;
use backend\modules\contabilidad\models\EmpresaPeriodoContable;

class DateTimeHelpers
{
    public static function mesATexto($mes)
    {
        $meses = ['01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre',
            '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre'];
        return $meses[$mes];
    }
}

function printPhones($data)
{
    return sizeof($data) ? $data[0]->empresa->telefonos : null;
}

$dataSize = sizeof($data);
$fecha_desde = $dataSize ? $data[0]->fecha : null;
$fecha_hasta = $dataSize ? $data[0]->fecha : null;
$empresa_ruc = $dataSize ? $data[0]->empresa->ruc : null;

$list = array_slice($data, 1, ($dataSize - 1));
foreach ($list as $item) {
    if ($item->fecha > $fecha_hasta) $fecha_hasta = $item->fecha;
    elseif ($item->fecha < $fecha_desde) $fecha_desde = $item->fecha;
}

?>

<div class="header" style="width: 100%;">
    <div class="reporte-titulo">
        <h3 class="reporte-titulo-h3">LIBRO DIARIO </h3>
    </div>

    <div class="reporte-titulo-contenido">
        <table class="reporte-titulo-tabla-contenido" style="width: 100%;">
            <thead>
            <tr>
                <th style="font-size: 11px;">Desde</th>
                <th style="font-size: 11px;">Hasta</th>
                <th style="font-size: 11px;">Responsable</th>
                <th style="font-size: 11px; text-align: center">Generado el</th>
                <th style="font-size: 11px; text-align: center;">Empresa</th>
                <th style="font-size: 11px; text-align: center">Periodo Contable</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="width: 13%; font-size: 11px;"><?= date_create_from_format('Y-m-d', $del ? $del : $fecha_desde)->format('d-m-Y') ?></td>
                <td style="width: 13%; font-size: 11px;"><?= date_create_from_format('Y-m-d', $al ? $al : $fecha_hasta)->format('d-m-Y') ?></td>
                <td style="width: 10%; font-size: 11px; "><?= Yii::$app->user->identity->username ?></td>
                <td style="width: 25%; font-size: 11px; text-align: center"><?= strftime('%d-%m-%Y a las %H:%M:%S', time()) ?></td>
                <td style="width: 25%; font-size: 11px; text-align: center; "><?= Empresa::findOne(['id' => \Yii::$app->session->get('core_empresa_actual')])->razon_social ?></td>
                <td style="width: 14%; font-size: 11px; text-align: center"><?= EmpresaPeriodoContable::findOne(['id' => \Yii::$app->session->get('core_empresa_actual_pc')])->anho ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <br/>
</div>

<div class="contents" style="width: 100%;">
    <table class="tg2 contenido" style="undefined;table-layout: fixed; width: 100%">
        <tr>
            <td class="asiento"></td>
            <td class="fecha"></td>
            <td class="cuenta"></td>
            <td class="debe"></td>
            <td class="haber"></td>
        </tr>

        <tr class="cabecera abajo arriba">
            <td style="width: 8%;"><strong>ASIENTO</strong></td>
            <td style="width: 8%;"><strong>FECHA</strong></td>
            <td style="width: "><strong>CUENTA</strong></td>
            <td style="width: 23%;"><strong>DEBE</strong></td>
            <td style="width: 23%;"><strong>HABER.</strong></td>
        </tr>
    </table>
</div>