<?php

/* @var $this yii\web\View */
/* @var $data \yii\db\ActiveRecord[] */

$total_gravs = 0.0;
$total_imps = 0.0;
$total_excs = 0.0;
$totals = 0.0;
$tt_gr10 = 0.0;
$tt_gr5 = 0.0;
$tt_im5 = 0.0;
$tt_im10 = 0.0;
$session = Yii::$app->session;
?>

<div class="contents" style="width: 100%;">
    <table class="tg2 contenido" style="undefined;table-layout: fixed; width: 100%">

        <!-- CARGA DE DATOS DE VENTA O COMPRA. -->
        <?php try { ?>
            <?php foreach ($data as $fila) : ?>
                <tr>
                    <td style="width: 8%" class="centrado"><?= $fila->id ?></td>
                    <td style="width: 8%" class="centrado"><?= $fila->fechaFormatted ?></td>
                    <td><?= $fila->concepto ?></td>
                    <td colspan="2"></td>
                </tr>

                <?php foreach ($fila->asientoDetalles as $detalle) : ?>
                    <tr>
                        <td colspan="2"></td>
                        <td><?= ($detalle->monto_debe > 0) ? $detalle->cuentaNombreDebe : $detalle->cuentaNombreHaber ?></td>
                        <td style="width: 23%" class="align-right"><?= $detalle->montoDebeFormatted ?></td>
                        <td style="width: 23%" class="align-right"><?= $detalle->montoHaberFormatted ?></td>
                    </tr>
                <?php endforeach; ?>

                <tr class="ultima-fila-datos">
                    <td style="text-align: center;"><strong>Suma: <strong></td>
                    <td colspan="2"></td>
                    <td class="total align-right"><?= $fila->totalDebe ?></td>
                    <td class="total align-right"><?= $fila->totalHaber ?></td>
                </tr>
                <tr>
                    <td colspan="5" style="height: 9px;"></td>
                </tr>
            <?php endforeach; ?>
        <?php } catch (Exception $e) {
            throw  $e;
        } ?>
        <tr class="ultima-fila">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: right; font-style: italic;">Listado Concluído</td>
        </tr>

    </table>
</div>

<br/>

