<?php

use backend\modules\contabilidad\controllers\ReporteDiario;
use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;

/* @var $model \backend\modules\contabilidad\controllers\ReporteDiario */

$this->title = 'Reporte de libro diario';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="venta-index">

<?php $form = ActiveForm::begin([
    'id' => 'reporte-form'
]); ?>

    <div>
        <?php echo PermisosHelpers::getAcceso('contabilidad-reporte-diario-get-params') ?
            Html::submitButton('Generar Reporte', ['id' => 'btn_submit', 'formtarget' => '_blank', 'class' => 'btn btn-info']) : null;
        ?>
    </div>

    <br/>

    <div>
        <?php
        $data = [
            'id' => 'Orden de carga',
            'fecha_emision__id' => 'Fecha y Orden de carga',
            'nro_factura' => 'Número de factura',
        ];

            $data['entidad_id__nro_factura'] = 'Cliente y Nro de factura';
            $model->tipo_reporte = true;
        try {
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 6,
                'attributes' => [
                    'tipo_reporte' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => SwitchInput::className(),
                        'options' => [
                            'pluginOptions' => [
                                'onText' => 'Pdf',
                                'offText' => 'Excel',
                                'offColor' => 'warning',
                                'onColor' => 'success',
                                'size' => 'small',
                                'handleWidth' => '50',
                                'labelWidth' => '50',
                            ],
                        ],
                    ],
                    'paper_size' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ReporteDiario::getPaperSizes(),
                            'options' => [
                                'placeholder' => 'Por favor Seleccione Uno',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'label' => 'Hoja'
                    ],
                    'orientacion' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ReporteDiario::getOrientaciones(),
                            'options' => [
                                'placeholder' => 'Por favor Seleccione Uno',
                            ],
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'label' => 'Orientación'
                    ],
                    'fecha_rango' => [
                        'type' => Form::INPUT_WIDGET,
                        'columnOptions' => ['colspan' => '2'],
                        'widgetClass' => DateRangePicker::className(),
                        'options' => [
                            'model' => $model,
                            'attribute' => 'fecha_rango',
                            'convertFormat' => true,
                            'language' => 'es',
                            'pluginOptions' => [
                                'allowClear' => true,
                                'autoApply' => true,
//                                'timePicker'=>true,
                                'locale' => ['format' => 'd-m-Y'],

                            ],

                        ],
                    ],
                ]
            ]);
        } catch (Exception $e) {
            echo $e;
        } ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$JS_FUNCTIONS = <<<JS
function showHideFechaRangoField() {
    // Show/Hide campo fecha_rango
    let campo = $('#reporte-fecha_rango');
    if ($('#reporte-filtro').val() !== 'fecha_rango') {
        campo.val('').trigger('change');
        $('.field-reporte-fecha_rango')[0].style.display = "none";
        // campo.prop('disabled', true);
    } else {
        $('.field-reporte-fecha_rango')[0].style.display = "";
        // campo.prop('disabled', false);
    }
}
JS;

$JS_DOCUMENT_ON_READY = <<<JS
$(document).ready(function () {  
    // Cada vez que haga submit, renderiza en nueva pestanha.
    document.getElementById('reporte-form').setAttribute("target", "_blank");
});
JS;

$JS_EVENTS = <<<JS
JS;

$scripts = [];
$scripts[] = $JS_FUNCTIONS;
$scripts[] = $JS_DOCUMENT_ON_READY;
$scripts[] = $JS_EVENTS;
foreach ($scripts as $s) $this->registerJs($s);
?>