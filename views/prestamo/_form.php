<?php

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\Prestamo;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model backend\modules\contabilidad\models\Prestamo */
/* @var $detalle backend\modules\contabilidad\models\Prestamo */

?>

<?php
$actvFormOptions = ['id' => 'prestamo_form'];

echo "<p>";
echo Html::a("Regresar a la lista de Préstamos", ['index'], ['class' => 'btn btn-info']);
echo "</p>";

?>

<div class="prestamo-form">

    <?php $form = ActiveForm::begin($actvFormOptions); ?>

    <?php

    try {

        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'nro_prestamo' => [
                            'columnOptions' => ['colspan' => '2'],
                            'type' => Form::INPUT_TEXT,
                        ],
                        'fecha_operacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'name' => 'fecha_operacion',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ]
                        ],
                        'ruc' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '2'],
                        ],
                        'razon_social' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '2'],
                            'options' => [
                                'readonly' => true,
                            ],
                        ],
                        'primer_vencimiento' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'name' => 'fecha_operacion',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ]
                        ],
                        'vencimiento' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'name' => 'vencimiento',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ]
                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'cant_cuotas' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                            'label' => "Cant. Cuotas"
                        ],
                        'monto_operacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'intereses_vencer' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'gastos_bancarios' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                            'label' => $model->getAttributeLabel('gastos_bancarios') . HtmlHelpers::InfoHelpIcon('tooltip-gastos-bancarios'),
                        ],
                        'capital' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'caja' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                            'label' => $model->getAttributeLabel('caja') . ' (Caja)',
                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'tipo_interes' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'disabled' => true,
                                'pluginOptions' => [
                                    'data' => Prestamo::getTipoInteresLista(),
                                ],
                                'options' => ['placeholder' => 'Seleccione una opción...'],
                                'initValueText' => Prestamo::getTipoInteresTxt($model->tipo_interes),
                            ]
                        ],
//                        'tasa' => [
//                            'type' => Form::INPUT_WIDGET,
//                            'columnOptions' => ['colspan' => '2'],
//                            'widgetClass' => MaskedInput::className(),
//                            'options' => [
//                                'clientOptions' => [
//                                    'rightAlign' => true,
//                                    'alias' => 'decimal',
//                                    'groupSeparator' => '.',
//                                    'radixPoint' => ',',
//                                    'autoGroup' => true,
//                                    'prefix' => "",
//                                ],
//                            ],
//                        ],
                        'usar_iva_10' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::class,
                            'columnOptions' => ['colspan' => '2'],
                            'options' => [
                                'data' => ['si' => 'Si', 'no' => 'No'],
                                'options' => ['placeholder' => 'Seleccione uno ...'],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ], 'disabled' => false
                            ]
                        ],
                        'tiene_factura' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::class,
                            'columnOptions' => ['colspan' => '2'],
                            'options' => [
                                'options' => ['placeholder' => 'Seleccione uno ...'],
                                'pluginOptions' => [
                                    'data' => Prestamo::getTieneFacturaList(),
                                    'allowClear' => false,
                                ],
                                'disabled' => false,
                                'initValueText' => Prestamo::getTieneFacturaTxt($model->tiene_factura),
                            ]
                        ],
                        'incluye_iva' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'pluginOptions' => [
                                    'data' => Prestamo::getEsGravadaLista(),
                                ],
                                'options' => ['placeholder' => 'Seleccione una opción...'],
                                'initValueText' => Prestamo::getEsGravadaTxt($model->incluye_iva),
                            ],
                            'label' => $model->getAttributeLabel('incluye_iva') . HtmlHelpers::InfoHelpIcon('tooltip-incluye-iva'),
                        ],
                        'iva_10' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'es_del_periodo_anterior' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'es_del_periodo_anterior')->widget(Select2::className(), [
                                'data' => ['si' => "Si", 'no' => "No"],
                                'options' => [
                                    'placeholder' => 'Seleccione uno...',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                ],
                            ])
                        ],
                    ]
                ],
                [

                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'concepto_devengamiento_cuota' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '10'],
                        ],
                    ],
                ],
            ],
        ]);

        \yii\widgets\Pjax::begin(['id' => 'cuentas_grid']);
        echo $this->render('detalle/_form_detalle_cuentas', ['model' => $model, 'form' => $form]);
        \yii\widgets\Pjax::end();

        \yii\widgets\Pjax::begin(['id' => 'cuotas_grid']);
        echo $this->render('detalle/_form_detalle', [
            'model' => $model,
            'form' => $form,
        ]);
        \yii\widgets\Pjax::end();

        \yii\widgets\Pjax::begin(['id' => 'simulador_asiento_grid']);
        echo $this->render('simulacion_asiento/_simulador_asiento', ['panel_color' => 'success']);
        \yii\widgets\Pjax::end();

    } catch (Exception $exception) {
        throw $exception;
    }

    $botonText = '';
    $botonClass = '';
    if (Yii::$app->controller->action->id == 'create') {
        $botonText = 'Guardar';
        $botonClass = 'btn btn-success';
    } else {
        $botonText = 'Actualizar';
        $botonClass = 'btn btn-primary';
    }

    ?>

    <div class="form-group">
        <?php echo Html::submitButton($botonText, ['class' => $botonClass,
            "data" => (Yii::$app->controller->action->id == 'update') ? [
                'confirm' => 'Desea guardar los cambios?',
                'method' => 'post',
            ] : []
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php

    $action_id = Yii::$app->controller->action->id;

    $css = <<<CSS
.modal-dialog {
   width:1000px;
}
CSS;

    $this->registerCss($css);

    $script_head = <<<JS
var action_id = "$action_id";
var prestamo_tasa_changed_by_another = false;

function applyDateInput() {
    $(":input[data-inputmask-alias]").inputmask();
}

function removeNumberInput() {
    $(":input.monto").inputmask('remove');

    $(":input.nro-cuota").inputmask('remove');
}

function applyNumberInput() {
    $(":input.monto").inputmask({
        "alias": "numeric",
        "digits": 0,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true
    });

    $(":input.nro-cuota").inputmask({
        "alias": "numeric",
        "digits": 0,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true
    });

    // $(":input.monto").number( true, 2, ',', '.' );
}

function removeCheckBoxExtraLabels() {
    $(".selected").not(document.getElementById('PrestamoDetalle___new___selected')).each(function () {

        $('label[for=' + $(this).attr('id') + ']').remove();
    });
}

var comma_pressed = false;

function personal(field_id, entire, keyred) {
    if (!isNumber(keyred) && !isComma(keyred) && !isArrowKey(keyred)) return entire; // como un break;

    let comma_pos = entire.indexOf(",");
    let unit_pos = -1;
    let last_pos = -1;
    let cursor_pos = -1;

    if (comma_pos === -1) {
        unit_pos = entire.length - 1;
    } else {
        unit_pos = comma_pos - 1;
    }
    last_pos = unit_pos + 2;
    
    cursor_pos = document.getElementById($(this).attr('id')).selectionStart;

    let integers = entire.substr(0, (unit_pos + 1));
    let decimals = entire.substr(last_pos, entire.length);

    if (keyred === ',') {
        comma_pressed = true;
    }

    integers = addIntegerDigit(integers, keyred, cursor_pos);
    decimals = addDecimalDigit(decimals, keyred, cursor_pos);

    let number = integers + ',' + decimals;
    return number;
}

function addIntegerDigit(integers, keyred, cursor_pos) {
    if (comma_pressed === false && isNumber(keyred)) {

    }
}

function addDecimalDigit(decimals, keyred) {
    if (comma_pressed === true && isNumber(keyred)) {

    }
}

function isArrowKey(keyCode) {
    return keyCode <= 40 && keyCode >= 37;
}

function isNumber(keyCode) {
    return keyCode > 95 && keyCode < 106 || keyCode > 47 && keycode < 58;
}

function isComma(keyCode) {
    return keyCode === 188 || keycode === 110;
}
JS;

    $this->registerJs($script_head, \yii\web\View::POS_HEAD);

    $url_generarCuotas = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/generar-cuotas'])));
    $url_simularAsiento = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/simular-asiento'])));
    $url_getRazonSocial = Json::htmlEncode(\Yii::t('app', Url::to(['entidad/get-column-value-by', 'column' => 'razon_social'])));
    $url_removerAsientoSimulado = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/remover-asiento-simulado'])));
    $url_cargarCuotasForUpdate = Json::htmlEncode(\Yii::t('app', Url::to(['cargar-cuotas-for-update', 'id' => $model->id])));

    $script = <<<JS
$(':input').keydown(function (evt) {
    // console.log(evt.keyCode, document.getElementById($(this).attr('id')).selectionStart, document.getElementById($(this).attr('id')).selectionEnd);
});
// evento para campo nro_cuota para agregar barra / al salir de foco y borrar al entrar en foco.
// ahora no se usa ese campo, pero se deja como backup por si se necesite en algun momento
$('#prestamo-nro_credito').on('focusout', function () {
    let value = $(this).val();

    value = value.replace(/\D/g, '');
    value = value.replace(/[\`\~\!\@\#\$\%\^\&\*\(\)\_\+\=\-\[\]\|\{\}\'\"\<\>\,\.\/\?]/g, '');
    let new_value = [value.slice(0, value.length - 2), '/', value.slice(value.length - 2)].join('');
    $(this).val(new_value).trigger('change');

}).on('focusin', function () {
    let value = $(this).val();
    value = value.replace('/', '');
    $(this).val(value).trigger('change');
});

// no se usa pero se deja como backup
$('#new-detalle').on('click', function () {
    detalle_k += 1;
    $('#tabla-detalles').find('tbody')
        .append('<tr class="fila-cuotas">' + $('#prestamo-new-detalle-block').html().replace(/__new__/g, 'new' + detalle_k).replace(/__new_class__/g, '') + '</tr>');

    applyDateInput();
    applyNumberInput();
    removeCheckBoxExtraLabels();

});

// no se usa pero se deja como backup
$(document).on('click', '.delete-detalle', function () {
    $(this).closest('tbody tr').remove();
});

// calcula cantidad de meses
$(document).on('change', '#prestamo-vencimiento, #prestamo-primer_vencimiento', function () {
    let val1 = $('#prestamo-primer_vencimiento').val();
    let val2 = $('#prestamo-vencimiento').val();

    let f1 = val1.split('-');
    let f2 = val2.split('-');

    let anho1 = parseInt(f1[2]);
    let anho2 = parseInt(f2[2]);
    let mes1 = parseInt(f1[1]);
    let mes2 = parseInt(f2[1]);

    let sign_changer = (anho2 - anho1) !== 0 ? ((anho2 - anho1) / Math.abs(anho2 - anho1)) : 1;

    let meses = 12 * (anho2 - anho1) + sign_changer * (-mes1 + 1 + mes2);

    $('#prestamo-cant_cuotas').val(meses).trigger('change');
});

function camposRequeridosNoVacios() {
    let tasa_no_vacio = $('#prestamo-tasa').val() !== "";

    return (
        $('#prestamo-vencimiento').val() !== "" &&
        $('#prestamo-primer_vencimiento').val() !== "" &&
        $('#prestamo-cant_cuotas').val() !== "" &&
        $('#prestamo-monto_operacion').val() !== "" &&
        $('#prestamo-intereses_vencer').val() !== "" &&
        $('#prestamo-gastos_bancarios').val() !== "" &&
        $('#prestamo-caja').val() !== ""
    ) && ((['fr', 'de']).includes($('#prestamo-tipo_interes').val()) ? tasa_no_vacio : true);
}

function cuentas() {
    let array = [];
    let campos = ['cuenta_caja', 'cuenta_intereses_vencer', 'cuenta_gastos_bancarios', 'cuenta_iva_10',
     'cuenta_gastos_no_deducibles', 'cuenta_monto_operacion', 'cuenta_intereses_a_pagar'];

    campos.forEach(function (campo, orden) {
        let fila = {atributo: campo, valor: $('#prestamo-' + campo).val()};
        array.push(fila);
    });

    return array;
}

$(':input[id^="prestamo-cuenta_"]').change(function () {
    simularAsiento();
});

function simularAsiento() {
    console.log('simularAsiento()');
    if (camposRequeridosNoVacios()) {
        let tipo_interes = $('#prestamo-tipo_interes');
        if (tipo_interes.val() !== '') {
            $.ajax({
                url: $url_simularAsiento,
                type: 'get',
                data: {
                    cuentas: cuentas(),
                    tipo_interes: tipo_interes.val(),
                    vencimiento: $('#prestamo-vencimiento').val(),
                    primer_venc: $('#prestamo-primer_vencimiento').val(),
                    cant_cuotas: $('#prestamo-cant_cuotas').val(),
                    monto_operacion: $('#prestamo-monto_operacion').val(),
                    interes: $('#prestamo-intereses_vencer').val(),
                    gastos_bancarios: $('#prestamo-gastos_bancarios').val(),
                    neto_entregar: $('#prestamo-caja').val(),
                    iva_10: $('#prestamo-iva_10').val(),
                    usar_iva_10: $('#prestamo-usar_iva_10').val(),
                    incluye_iva: $('#prestamo-incluye_iva').val(),
                    tiene_factura: $('#prestamo-tiene_factura').val(),
                },
                success: function (data) {
                    if (data.error) {
                        $.pjax.reload({container: "#flash_message_id", async: false});
                    }
                    $.pjax.reload({container: "#simulador_asiento_grid", async: false});
                }
            });
        }
    } else {
        console.log('se remueve simulacion porque faltan datos necesarios.');
        $.ajax({
            url: $url_removerAsientoSimulado,
            type: 'get',
            data: {},
            success: function (data) {
                $.pjax.reload({container: "#simulador_asiento_grid", async: false});
            },
        });
    }
}

function generarCuotas() {
    console.log('generarCuotas()');
    
    if ("$action_id" !== 'create') {
        console.log('no se re-genera cuotas por no ser create');
        return false;
    }

    if (camposRequeridosNoVacios()) {
        let tipo_interes = $('#prestamo-tipo_interes');
        if (tipo_interes.val() !== '') {
            $.ajax({
                url: $url_generarCuotas,
                type: 'get',
                data: {
                    tipo_interes: tipo_interes.val(),
                    vencimiento: $('#prestamo-vencimiento').val(),
                    primer_venc: $('#prestamo-primer_vencimiento').val(),
                    cant_cuotas: $('#prestamo-cant_cuotas').val(),
                    monto_operacion: $('#prestamo-monto_operacion').val(),
                    interes: $('#prestamo-intereses_vencer').val(),
                    gastos_bancarios: $('#prestamo-gastos_bancarios').val(),
                    neto_entregar: $('#prestamo-caja').val(),
                    capital: $('#prestamo-capital').val(),
                    iva_10: $('#prestamo-iva_10').val(),
                    usar_iva_10: $('#prestamo-usar_iva_10').val(),
                    incluye_iva: $('#prestamo-incluye_iva').val(),
                    tasa: $('#prestamo-tasa').val(),
                    tiene_factura: $('#prestamo-tiene_factura').val(),
                },
                success: function (data) {
                    if (data.error) {
                        $.pjax.reload({container: "#flash_message_id", async: false});
                    }
                    $.pjax.reload({container: "#cuotas_grid", async: false});
                    applyDateInput();
                    applyNumberInput();
                    removeCheckBoxExtraLabels();
                }
            });
        }
    }
}

function calcularIva() {
    console.log('calcularIva()');
    let element = $('#prestamo-usar_iva_10');

    if (element.val() === 'no') return "";

    let iva_10 = 0;
    let monto_operacion = $("#prestamo-monto_operacion").val().replace(/\./g, '').replace(/\,/g, '.');
    let interes_vencer = $("#prestamo-intereses_vencer").val().replace(/\./g, '').replace(/\,/g, '.');
    let gastos_bancarios = $("#prestamo-gastos_bancarios").val().replace(/\./g, '').replace(/\,/g, '.');

    if (monto_operacion && interes_vencer && gastos_bancarios) {

        monto_operacion = parseFloat(monto_operacion);
        interes_vencer = parseFloat(interes_vencer);
        gastos_bancarios = parseFloat(gastos_bancarios);
        iva_10 = Math.round(gastos_bancarios / 11); // gastos bancarios siempre con iva

        console.log('gastos bancarios:', gastos_bancarios, 'intereses_vencer:', interes_vencer, 'iva:', iva_10);

        if ($('#prestamo-incluye_iva').val() === 'si') {
            iva_10 += Math.round(interes_vencer / 11);
            // caja = monto_operacion - (interes_vencer + gastos_bancarios);
        } else {
            iva_10 += Math.round(interes_vencer / 10);
            // caja = monto_operacion - (interes_vencer + gastos_bancarios + iva_10);
        }

        // $('form[id^="prestamo"]').yiiActiveForm('validateAttribute', 'prestamo-usar_iva_10');
    }

    return iva_10;
}

function calcularCaja() {
    console.log('calcularCaja()');
    let iva_10 = 0;
    let caja = 0;
    let monto_operacion = $("#prestamo-monto_operacion").val().replace(/\./g, '').replace(/\,/g, '.');
    let interes_vencer = $("#prestamo-intereses_vencer").val().replace(/\./g, '').replace(/\,/g, '.');
    let gastos_bancarios = $("#prestamo-gastos_bancarios").val().replace(/\./g, '').replace(/\,/g, '.');

    if (monto_operacion && interes_vencer && gastos_bancarios) {

        monto_operacion = parseFloat(monto_operacion);
        interes_vencer = parseFloat(interes_vencer);
        gastos_bancarios = parseFloat(gastos_bancarios);
        let iva_interes = 0;

        if ($('#prestamo-incluye_iva').val() === 'si') {
            caja = monto_operacion - (interes_vencer + gastos_bancarios);
        } else {
            caja = monto_operacion - (Math.round(interes_vencer * 1.1) + gastos_bancarios);
        }

        // // console.log(iva_interes);
        // if ($('#prestamo-iva_interes_cobrado').val() === 'si') {
        //     caja -= iva_interes;
        // }
    }

    return caja;
}

function calcularInteres() {
    return $('#prestamo-intereses_vencer').val(); /*
    
    console.log('calcularInteres()');
    let tasa = $('#prestamo-tasa').val().replace(/\./g, '').replace(/\,/g, '.');
    
    if (tasa === "") return $('#prestamo-intereses_vencer').val(); // retornar lo que habia
    
    tasa = parseFloat(tasa);
   
    if (tasa > 1.00000)
        tasa /= 100;
   
    let capital = $('#prestamo-caja').val().replace(/\./g, '').replace(/\,/g, '.');
    capital = parseInt(capital);
   
    let interes_total = capital * tasa;
    
    return interes_total;*/
}

/** -------------------------------------------- */

function calcularCapital() {
    console.log('calcularCapital()');
    console.log('calcularCapital() ejecuta a la funcion calcularCaja()');
    let capital = calcularCaja() + parseInt($('#prestamo-gastos_bancarios').val().replace(/\./g, '').replace(/\,/g, '.'));
    let interes = parseInt(calcularInteres().replace(/\./g, '').replace(/\,/g, '.'));
    let iva = ($('#prestamo-incluye_iva').val() === 'si') ? interes / 11 : interes / 10;
    iva = Math.round(iva);
    capital += ($('#prestamo-tiene_factura').val() === 'por_prestamo') ? iva : 0;
    return capital;
}

$(document).on('change', '#prestamo-caja', function () {
    console.log($(this).attr('id'), 'changed');
    $('#prestamo-capital').val(calcularCapital()).trigger('change');
    generarCuotas();
    simularAsiento();
});

$(document).on('change', '#prestamo-monto_operacion', function () {
    console.log('monto_operacion changed');
    $('#prestamo-caja').val(calcularCaja()).trigger('change');
});

$(document).on('change', '#prestamo-intereses_vencer, #prestamo-gastos_bancarios, #prestamo-incluye_iva', function () {
    console.log($(this).attr('id'), 'changed');
    $('#prestamo-iva_10').val(calcularIva()).trigger('change');
    $('#prestamo-caja').val(calcularCaja()).trigger('change');
});

$(document).on('change', '#prestamo-tipo_interes, #prestamo-cant_cuotas', function () {
    console.log($(this).attr('id'), 'changed');

    if ($(this).attr('id') === 'prestamo-tipo_interes') {
        console.log('prestamo-tipo_interes gonna throw ocultarCampoTasa()', 'flag = true');
        prestamo_tasa_changed_by_another = true;
        ocultarCampoTasa();
    }
    generarCuotas();
});

$(document).on('change', '#prestamo-tipo_interes', function () { // si es simple o americano, no se requiere tasa, por tanto, si otros campos estan full, se puede simular.
    console.log($(this).attr('id'), 'changed');
    simularAsiento();
});

$(document).on('change', '#prestamo-usar_iva_10', function () {
    console.log('prestamo-usar_iva_10 changed');
    readonlyIva10();
    $('#prestamo-iva_10').val(calcularIva()).trigger('change'); // iva_10 solo cambia por usar_iva_10, asi que no vale pena crear evento onchange para iva_10 aparte.
    $('#prestamo-caja').val(calcularCaja()).trigger('change');

    let form = $('form[id^="prestamo"]');
    form.yiiActiveForm('validateAttribute', 'prestamo-tiene_factura');
    form.yiiActiveForm('validateAttribute', 'prestamo-incluye_iva');
});

// $(document).on('change', '#prestamo-iva_interes_cobrado', function () {
//     console.log('prestamo-iva_interes_cobrado changed');
//     $('#prestamo-caja').val(calcularCaja()).trigger('change');
// });
$(document).on('change', '#prestamo-tiene_factura', function() {
    console.log('prestamo-tiene_factura changed');
    
    let form = $('form[id^="prestamo"]');
    form.yiiActiveForm('validateAttribute', 'prestamo-incluye_iva');
    
    $('#prestamo-capital').val(calcularCapital()).trigger('change');
    generarCuotas();
    simularAsiento();
});

$(document).on('focusin', '#prestamo-tasa', function () {
    console.log('prestamo-tasa focus gained, flag = false');
    prestamo_tasa_changed_by_another = false;
});

$(document).on('change', '#prestamo-tasa', function () {
    console.log('prestamo-tasa changed');
    if (prestamo_tasa_changed_by_another === false)
        $('#prestamo-intereses_vencer').val(calcularInteres()).trigger('change');
});

/** -------------------------------------------- */

$(document).on('click', '#btn-recalcular-todo', function () {
    console.log('btn-recalcular-todo clicked');
    // generarCuotas();
    // simularAsiento();
    $('#prestamo-caja').trigger('change');

    // Si alguno de los campos de la cabecera esta marcada en rojo por la validacion "validarMontoCuotas()" que no es
    // clientSideValidation, se valida de nuevo para remover la pintura roja.
    let form = $('form[id^="prestamo"]');
    form.yiiActiveForm('validateAttribute', 'prestamo-monto_operacion');
    form.yiiActiveForm('validateAttribute', 'prestamo-intereses_vencer');
    form.yiiActiveForm('validateAttribute', 'prestamo-gastos_bancarios');
    form.yiiActiveForm('validateAttribute', 'prestamo-caja');
    form.yiiActiveForm('validateAttribute', 'prestamo-iva_10');
});

$(document).on('focusout', '#prestamo-iva_10', function () {
    $('form[id^="prestamo"]').yiiActiveForm('validateAttribute', 'prestamo-usar_iva_10');
});

function readonlyIva10() {
    $('#prestamo-iva_10').attr('readonly', $('#prestamo-usar_iva_10').val() === 'no');
}

$(document).on('change', '#prestamo-ruc', function () {
    $.ajax({
        type: 'get',
        url: $url_getRazonSocial +'&attribute=ruc&value=' + $('#prestamo-ruc').val(),
        data: {},
        success: function (data) {
            $('#prestamo-razon_social').val(data).trigger('change');
        }
    });
});

$(document).on('focusout', '#prestamo-fecha_operacion', function () {
    if ($(this).val() === "") {
        let d = new Date();
        d = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
        $(this).val(d).trigger('change');
    }
});

function cargarCuotasForUpdate() {
    $.ajax({
        url: $url_cargarCuotasForUpdate,
        type: 'get',
        data: {id: "$model->id"},
        success: function (result_data) {
            $.pjax.reload({container: "#cuotas_grid", async: false});
            applyDateInput();
            applyNumberInput();
            removeCheckBoxExtraLabels();
            // $('#btn-recalcular-todo').click();
        },
        error: function (request, status, error) {
            krajeeDialog.alert(request.responseText);
        },
    });
}

function ocultarCampoTasa() {
    let tipo_interes = $('#prestamo-tipo_interes').val();
    let prestamo_tasa = $("#prestamo-tasa");

    if (!(['fr', 'de']).includes(tipo_interes)) {
        prestamo_tasa.val('').trigger('change');
        prestamo_tasa.parent().parent().css('display', 'none');
    } else {
        prestamo_tasa.parent().parent().css('display', '');
    }
}

function popOvers() {
    applyPopOver($('span.tooltip-gastos-bancarios'), 'Información', 'Debe ingresar siempre el monto total incluído el I.V.A.', {'theme-color': 'info-l', 'placement': 'top'});
    applyPopOver($('span.tooltip-incluye-iva'), 'Información', 'Si el interés ingresado es con o sin I.V.A.', {'theme-color': 'info-l', 'placement': 'top'});
    applyPopOver($('span.tooltip-iva-interes'), 'Información', 'Si el I.V.A. del interés total se factura al sacar el préstamo o no.', {'theme-color': 'info-l', 'placement': 'top'});
}

function setCapital() {
    console.log('setCapital()');
    $('#prestamo-capital').val(calcularCapital()).trigger('change');
}

$(document).on('change', 'input.monto', function () {

    let regex = /(PrestamoDetalle_new([0-9]+)_)([\s\S]*)/gm;
    let m = regex.exec($(this).prop('id'));
    try {
        let fila = '#' + m[1];  // extraer 'PrestamoDetalle_new1_'
        let campo = m[3];       // extraer 'interes' o 'capital'
        let campo_c = {'interes': "capital", 'capital': "interes"};

        // Si el campo modificado es interes o capital
        if (['interes', 'capital'].includes(campo)) {
            // actualizar cuota
            let monto = $(fila + campo).val();
            let monto_c = $(fila + campo_c[campo]).val();
            
            $(fila + 'monto_cuota').val(monto + monto_c).trigger('change');  // total a la derecha
        } else if (campo === 'impuesto') {
            // Si el campo modificado es iva
            let capital = $(fila + 'capital').val(),
                interes = $(fila + 'interes').val(),
                iva = $(this).val();

            $(fila + 'monto_cuota').val(capital + interes + iva).trigger('change');  // total a la derecha
        }
        
        // Total columnar
        let totalColumna = {'capital': 0, 'interes': 0, 'impuesto': 0, 'cuota': 0, 'monto_cuota': 0};
        $('input.monto[id$="' + campo + '"]').each(function(i, e) {
            totalColumna[campo] += e.value;
        });
        $('#' + campo + '-total-disp').val(totalColumna[campo]).trigger('change');
    } catch (e) {
        console.log(e);
        alert("Ocurrió una excepción al actualizar los totales");
    }
});

$(document).ready(function () {
    popOvers();
    $('#prestamo-capital').prop('readonly', true);
    // $('#prestamo-caja').prop('readonly', true);
    readonlyIva10();

    // if (action_id === 'create') {
    //     $("#new-detalle").click();
    // }

    applyDateInput();
    applyNumberInput();
    removeCheckBoxExtraLabels();

    if (action_id === 'update') {
        prestamo_tasa_changed_by_another = true;
        cargarCuotasForUpdate();
        simularAsiento();
        ocultarCampoTasa(); // esta funcion cambia el valor de tasa, por eso se setea a true la bandera.
        setCapital(); // solo desde update
    }
});
JS;

    $this->registerJs($script);
    ?>
</div>
