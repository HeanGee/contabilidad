<?php

use backend\modules\contabilidad\models\Prestamo;
use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\PrestamoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Préstamos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prestamo-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-prestamo-create') ?
            Html::a('Crear Nuevo Prestamo', ['create'], ['class' => 'btn btn-success']) : null ?>
        <?= PermisosHelpers::getAcceso('contabilidad-recibo-prestamo-index') ?
            Html::a('Ir a Recibos', ['recibo-prestamo/index'], ['class' => 'btn btn-info']) : null ?>
        <?= (Yii::$app->user->identity->username == 'admin') ?
            Html::a('Ver log', ['ver-log'], ['class' => 'btn btn-info pull-right']) : null ?>
    </p>

    <?php

    try {
        $template = "";

        foreach (['view', 'update', 'delete', 'bloquear', 'desbloquear'] as $_v)
            if (PermisosHelpers::getAcceso("contabilidad-prestamo-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'class' => DataColumn::class,
                    'attribute' => 'nro_prestamo',

                ],
                [
                    'attribute' => 'entidad_id',
                    'label' => "Entidad",
//                    'value' => 'entidadRazonSocial',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return "{$model->entidad->razon_social} <strong>({$model->entidad->ruc})</strong>";
                    }
                ],
                [
                    'label' => 'Fecha Operación',
                    'attribute' => 'fecha_operacion',
                    'format' => ['date', 'php:d-m-Y'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha_operacion',
                        'language' => 'es',
                        'pickerButton' => false,
//                        'options' => [
//                            'style' => 'width:90px',
//                        ],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                            'weekStart' => '0',
                        ]
                    ]),
                    'contentOptions' => ['style' => 'padding:8px 0 0px 0px; text-align:center;']
                ],
                [
                    'attribute' => 'monto_operacion',
                    'value' => 'formattedMontoOperacion',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right;'],
                ],
//                [
//                    'attribute' => 'intereses_vencer',
//                    'value' => 'formattedInteresesVencer',
//                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right;'],
//                ],
//                [
//                    'attribute' => 'gastos_bancarios',
//                    'value' => 'formattedGastosBancarios',
//                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right;'],
//                ],
//                [
//                    'attribute' => 'caja',
//                    'value' => 'formattedCaja',
//                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right;'],
//                ],
                [
                    'attribute' => 'cant_cuotas',
                    'contentOptions' => ['style' => 'padding:8px 0px 0px 0px; text-align:center;'],
                    'label' => 'Cant. Cuotas'
                ],
                [
                    'attribute' => 'usar_iva_10', 'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->usar_iva_10 == 'si') ? 'success">Sí' : 'danger">No') . '</label>';
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'usar_iva_10',
                        'data' => ['todos' => 'Todos', 'si' => 'Si', 'no' => "No"],
                        'pluginOptions' => [
//                            'width' => '90px',
                            'allowClear' => false,
                        ],
                        'initValueText' => 'todos',
                    ]),
                    'contentOptions' => ['style' => 'padding:8px 0px 0px 0px; text-align:center;']
                ],
                [
                    'attribute' => 'tiene_factura', 'format' => 'raw',
                    'value' => function ($model) {
                        $tiene_factura = Prestamo::getTieneFacturaTxt($model->tiene_factura);
                        return '<label class="label label-' . (($model->tiene_factura != 'no') ? "success\">{$tiene_factura}" : 'danger">No') . '</label>';
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'tiene_factura',
                        'data' => ['todos' => 'Todos', 'si' => 'Si', 'no' => "No"],
                        'pluginOptions' => [
//                            'width' => '90px',
                            'allowClear' => false,
                        ],
                        'initValueText' => 'todos',
                    ]),
                    'contentOptions' => ['style' => 'padding:8px 0px 0px 0px; text-align:center;']
                ],
//                [
//                    'attribute' => 'iva_interes_cobrado',
//                    'value' => function ($model) {
//                        return '<label class="label label-' . (($model->iva_interes_cobrado == 'si') ? 'success">Sí' : 'danger">No') . '</label>';
//                    },
//                    'label' => 'IVA Interes cobrado?',
//                    'format' => 'raw',
//                    'filterType' => GridView::FILTER_SELECT2,
//                    'filter' => ['todos' => 'Todos', 'si' => 'Si', 'no' => "No"],
//                    'filterWidgetOptions' => [
//                        'pluginOptions' => [
//                            'allowClear' => false,
//                        ],
//                    ],
//                    'contentOptions' => ['style' => 'padding:8px 0px 0px 0px; text-align:center;']
//                ],
                [
                    'attribute' => 'nro_factura',
                    'format' => 'raw',
                    'value' => function ($data) {
                        $url = Url::to(['compra/view', 'id' => $data->factura_compra_id]);
                        return Html::a(isset($data->factura_compra_id) ? $data->compra->nro_factura : '', $url);
                    },
                    'contentOptions' => ['style' => 'padding:8px 0px 0px 0px; text-align:center;']

                ],
                //'vencimiento',
                //'tasa',
                //'periodo_contable_id',
                //'empresa_id',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        /** @var Prestamo $model */
                        'update' => function ($url, $model) {
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Editar'),
                            ]) : '';
                        },
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('yii', 'Ver'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            /** @var Prestamo $model */
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => '',
                                'title' => Yii::t('yii', 'Borrar'),
                                'data' => [
                                    'confirm' => '¿Está seguro de eliminar este elemento?',
                                    'method' => 'post',
                                ],
                            ]) : '';
                        },
                        'bloquear' => function ($url, $model, $key) {
                            /** @var Prestamo $model */
                            $url = ($model->bloqueado == 'no') ?
                                Html::a('<span>Bloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => Yii::t('app', 'Bloquear'),
                                        'data-confirm' => 'Está seguro que desea bloquear el prestamo?'
                                    ]) : '';
                            return $url;
                        },
                        'desbloquear' => function ($url, $model, $key) {
                            /** @var Prestamo $model */
                            $url = ($model->bloqueado == 'si') ?
                                Html::a('<span>Desbloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-success btn-xs',
                                        'title' => Yii::t('app', 'Desbloquear'),
                                        'data-confirm' => 'Está seguro que desea desbloquear el prestamo?'
                                    ]) : '';
                            return $url;
                        },
                    ],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $exception) {
        print $exception->getMessage();
    } ?>
</div>
