<?php

use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\form\ActiveForm;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \backend\modules\contabilidad\models\PrestamoSelector */

/* @var $fila_plantilla string */

?>

<div class="prestamo-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'prestamo-sin-factura',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <?php

    try {

        echo $form->field($model, 'prestamo_id')->widget(Select2::classname(), [
            'options' => ['placeholder' => 'Seleccione un prestamo ...'],
            'initValueText' => $model->prestamo_id != null ?
                'Nro Crédito ' . $model->prestamo_id : '',
            'pluginOptions' => [
                'allowClear' => false,
                'ajax' => [
                    'url' => Url::to(['prestamo/get-prestamos-sin-factura-ajax']),
                    'dataType' => 'json',
                    'data' => new JsExpression("
                            function(params) { 
                                return {
                                    q:params.term, 
                                    id: '',
                                    compra_id: \"$compra_id\",
                                    deducible: \"$deducible\"
                                };
                            }
                        "),
                ]
            ],
        ])->label('Préstamo');

        echo Html::tag('p', Html::submitButton('Guardar', ['class' => 'btn btn-success']), []);

    } catch (Exception $exception) {
        print $exception->getMessage();
    }
    ?>

    <?php ActiveForm::end(); ?>

    <?php
    $script = <<<JS
$("form#prestamo-sin-factura").on("beforeSubmit", function (e) {
    var form = $(this);
    let url = form.attr("action") + "&submit=true";

    $.post(
        url,
        form.serialize()
    )
    .done(function (result) {
        form.parent().html(result.message);        
        console.log('se proceso bien el modal.');

        $.pjax.reload({container: "#flash_message_id", async: false});
        $("#modal").modal("hide");
        $("modal-body").html("");
    });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});
JS;

    $this->registerJs($script);
    ?>
</div>
