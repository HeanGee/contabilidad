<?php

?>

<?php
try {
    echo '<td class="check-this" style="text-align: center;">';
    echo $form->field($detalle, 'selected', [
        'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div><div class=\"col-lg-8\">{error}</div>",
    ])->checkbox([
        'id' => "PrestamoDetalle_{$key}_selected",
        'name' => "PrestamoDetalle[$key][selected]",
        'class' => 'selected'
    ], false);
    echo '</td>';

    echo '<td style="">';
    echo $form->field($detalle, 'id')->textInput([
        'label' => '',
        'id' => "PrestamoDetalle_{$key}_id",
        'name' => "PrestamoDetalle[$key][id]",
        'class' => "id",
        'readonly' => true,
    ])->label(false);
    echo '</td>';
} catch (Exception $exception) {
    print $exception->getMessage();
}
?>

<td>
    <?= $form->field($detalle, 'nro_cuota')->textInput([
        'id' => "PrestamoDetalle_{$key}_nro_cuota",
        'name' => "PrestamoDetalle[$key][nro_cuota]",
        'class' => "form-control nro-cuota",
        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'vencimiento')->textInput([
        'id' => "PrestamoDetalle_{$key}_vencimiento",
        'name' => "PrestamoDetalle[$key][vencimiento]",
        'class' => "form-control fecha",
        'data-inputmask-alias' => "dd-mm-yyyy",
        'data-inputmask' => "'yearrange': { 'minyear': '1917', 'maxyear': '2080' }",
        'data-val' => "true",
        'data-val-required' => "Required",
        'placeholder' => "dd-mm-yyyy",
        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'capital_saldo')->textInput([
        'id' => "PrestamoDetalle_{$key}_capital_saldo",
        'name' => "PrestamoDetalle[$key][capital_saldo]",
        'class' => "form-control monto",
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'interes_saldo')->textInput([
        'id' => "PrestamoDetalle_{$key}_interes_saldo",
        'name' => "PrestamoDetalle[$key][interes_saldo]",
        'class' => "monto",
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'monto_cuota_saldo')->textInput([
        'id' => "PrestamoDetalle_{$key}_monto_cuota_saldo",
        'name' => "PrestamoDetalle[$key][monto_cuota_saldo]",
        'class' => "form-control monto",
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<?php try {
    Yii::warning(Yii::$app->controller->action->id);
    if (Yii::$app->controller->action->id == "manejar-cuota-prestamo-desde-compra") {
        echo '<td>';
        echo $form->field($detalle, 'recibo_capital_nro')->textInput([
            'id' => "PrestamoDetalle_{$key}_recibo_capital_nro",
            'name' => "PrestamoDetalle[$key][recibo_capital_nro]",
            'class' => "form-control",
            'style' => 'text-align: left;'
        ])->label(false);
        echo '</td>';
    }
} catch (Exception $exception) {
    print $exception->getMessage();
} ?>

<!--<td class=" " style="text-align: center">-->
<!--    <? //= Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
//        'class' => 'delete-detalle btn btn-danger btn-sm',
//        'id' => "PrestamoDetalle_{$key}_delete_button",
//        'title' => 'Eliminar fila',
//    ]) ?>-->
<!--</td>-->