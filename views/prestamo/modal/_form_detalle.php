<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 20/09/2018
 * Time: 11:23
 */

use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\PrestamoDetalleTemp;
use kartik\form\ActiveForm;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $form ActiveForm */
/* @var $model Prestamo */
?>

    <fieldset>
        <legend>&nbsp;Cuotas
        </legend>
        <?php

        echo '<table id="tabla-detalles" class="table table-condensed table-bordered">';
        echo '<thead>';
        echo '<tr>';
        echo '<th style="text-align: center;"></th>';
        echo '<th style="text-align: center;">ID</th>';
        echo '<th style="text-align: center;"># Cuota</th>';
        echo '<th style="text-align: center;">Vencimiento</th>';
        echo '<th style="text-align: center;">Capital</th>';
        echo '<th style="text-align: center;">Interés</th>';
        echo '<th style="text-align: center;">Monto Cuota</th>';
        if (Yii::$app->controller->action->id == "manejar-cuota-prestamo-desde-compra") {
            echo '<th style="text-align: center;">Nº Recibo por Capital</th>';
        }
        //        echo '<th style="text-align: center;"></th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        // existing detalles fields
        $skey = 'cont_prestamo_cuotas';
        $detalles = Yii::$app->session->get($skey)->allModels;
        $key = 0;
        foreach ($detalles as $index => $prestamo_detalle) {
            $key = $prestamo_detalle->id != null ? $prestamo_detalle->id - 1 : $index;
            echo '<tr class="fila-cuotas">';
            echo $this->render('_form_detalle_fields', [
                'key' => 'new' . ($key + 1),
                'form' => $form,
                'detalle' => $prestamo_detalle,
            ]);
            echo '</tr>';
            $key++;
        }

        // new detalles fields
        $detalle = new PrestamoDetalleTemp();
        echo '<tr id="prestamo-new-detalle-block" style="display: none;">';
        echo $this->render('_form_detalle_fields', [
            'key' => '__new__',
            'form' => $form,
            'detalle' => $detalle,
        ]);
        echo '</tr>';
        echo '</tbody>';
        echo '</table>';

        \kartik\select2\Select2Asset::register($this);

        $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
        ?>

    </fieldset>
<?php
$action_id = Yii::$app->controller->action->id;

$script_head = <<<JS
var action_id = "$action_id";
var detalle_k = $key;
JS;

$this->registerJs($script_head, \yii\web\View::POS_HEAD);
