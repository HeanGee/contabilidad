<?php

use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model backend\modules\contabilidad\models\Prestamo */
/* @var $detalle backend\modules\contabilidad\models\Prestamo */

/* @var $fila_plantilla string */

?>

<div class="prestamo-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'prestamo-desde-cuota',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <?php

    try {

        echo $this->render('_action_btns_div', []);

        echo $form->field($model, 'prestamo_id_selector')->widget(Select2::classname(), [
            'options' => ['placeholder' => 'Seleccione un prestamo ...'],
            'initValueText' => $model->prestamo_id_selector != null ?
                'Nro Crédito ' . $model->prestamo_id_selector : '',
            'pluginOptions' => [
                'allowClear' => false,
                'ajax' => [
                    'url' => Url::to(['prestamo/get-prestamos-libres-para-compra-ajax']),
                    'dataType' => 'json',
                    'data' => new JsExpression("
                            function(params) { 
                                return {
                                    q:params.term, 
                                    id: '',
                                    compra_id: \"$compra_id\",
                                    ruc: {$model->entidad->ruc}
                                };
                            }
                        "),
                ]
            ],
        ])->label('Préstamo');

        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'fecha_operacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'name' => 'fecha_operacion',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ],
                            'label' => "Fecha Op."
                        ],
                        'primer_vencimiento' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'name' => 'fecha_operacion',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ],
                            'label' => "1º Venc."
                        ],
                        'vencimiento' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '2'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'name' => 'vencimiento',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ],
                            'label' => "Últimmo Venc."
                        ],
                        'cant_cuotas' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'integer',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'tiene_factura' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::class,
                            'columnOptions' => ['colspan' => '3'],
                            'options' => [
                                'data' => ['si' => 'Si', 'no' => 'No'],
                                'options' => ['placeholder' => 'Seleccione usuario ...'],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                                'disabled' => true
                            ]
                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'monto_operacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'intereses_vencer' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'gastos_bancarios' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'usar_iva_10' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => Select2::class,
                            'columnOptions' => ['colspan' => '3'],
                            'options' => [
                                'data' => ['si' => 'Si', 'no' => 'No'],
                                'options' => ['placeholder' => 'Seleccione usuario ...'],
                                'pluginOptions' => [
                                    'allowClear' => false
                                ],
                                'disabled' => true
                            ]
                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'iva_10' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'caja' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => true,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true,
                                    'prefix' => "",
                                ],
                            ],
                        ],
                        'tipo_interes' => [
                            'type' => Form::INPUT_WIDGET,
                            'columnOptions' => ['colspan' => '3'],
                            'widgetClass' => Select2::className(),
                            'options' => [
                                'disabled' => true,
                                'pluginOptions' => [
                                    'data' => \backend\modules\contabilidad\models\Prestamo::getTipoInteresLista(),
                                ],
                                'options' => ['placeholder' => 'Seleccione una opción...'],
                                'initValueText' => \backend\modules\contabilidad\models\Prestamo::getTipoInteresTxt($model->tipo_interes),
                            ]
                        ],
//                        'iva_interes_cobrado' => [
//                            'type' => Form::INPUT_WIDGET,
//                            'columnOptions' => ['colspan' => '3'],
//                            'widgetClass' => Select2::className(),
//                            'options' => [
//                                'disabled' => true,
//                                'data' => ['si' => "Si", 'no' => "No"],
//                                'options' => ['placeholder' => 'Seleccione una opción...'],
//                                'initValueText' => (['si' => "Si", 'no' => "No"])[$model->iva_interes_cobrado],
//                            ]
//                        ],
                    ],
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'razon_social' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '3'],
                            'options' => [
                                'readonly' => true,
                            ],
                            'value' => $model->entidad->razon_social,
                        ],
                    ],
                ],
            ],
        ]);

        \yii\widgets\Pjax::begin(['id' => 'cuotas_grid']);
        echo $this->render('_form_detalle', ['model' => $model, 'form' => $form,]);
        \yii\widgets\Pjax::end();

    } catch (Exception $exception) {
        print $exception->getMessage();
    }
    ?>

    <?php ActiveForm::end(); ?>

    <?php
    $css = <<<CSS
.modal-dialog {
   width:850px;
}
CSS;

    $this->registerCss($css);

    $action_id = Yii::$app->controller->action->id;

    $script_head = <<<JS
var action_id = "$action_id";

function applyDateInput() {
    $(":input[data-inputmask-alias]").inputmask();
}

function applyNumberInput() {
    $(":input.monto").inputmask({
        "alias": "numeric",
        "digits": 2,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true
    });

    $(":input.nro-cuota").inputmask({
        "alias": "numeric",
        "digits": 0,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true
    });

    // $(":input.monto").number( true, 2, ',', '.' );
}

function removeCheckBoxExtraLabels() {
    $("input[id$=\"selected\"]").not(document.getElementById('PrestamoDetalle___new___selected')).each(function () {

        $('label[for=' + $(this).attr('id') + ']').remove();
    });
}
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);

    $url_manejarDesdeCompra = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/manejar-cuota-prestamo-desde-compra'])));
    $url_valorParaExenta = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/get-valor-para-exenta'])));
    $script = <<<JS
$("form#prestamo-desde-cuota").on("beforeSubmit", function (e) {
    var form = $(this);
    let url = form.attr("action") + "&submit=true";
    let fila_plantilla = "#" + "$fila_plantilla";

    $(fila_plantilla + 'ivas-iva_0-disp').val('');
    $(fila_plantilla + 'ivas-iva_5-disp').val('');
    $(fila_plantilla + 'ivas-iva_10-disp').val('');

    $.post(
        url,
        form.serialize()
    )
    .done(function (result) {
        form.parent().html(result.message);        
        $.ajax({
            url: $url_valorParaExenta,
            type: 'get',
            data: {},
            success: function(data) {
                console.log('montos para ivas en la fila ' + fila_plantilla + ' despues de procesar cuotas: ', data);

                if (data['exenta'])
                    $(fila_plantilla + 'ivas-iva_0-disp').val(data['exenta']).trigger('change');
                if (data['iva_10'])
                    $(fila_plantilla + 'ivas-iva_10-disp').val(data['iva_10']).trigger('change');
            }
        });

        $.pjax.reload({container: "#flash_message_id", async: false});
        $("#modal").modal("hide");
        $("modal-body").html("");
    });
    return false;
}).on("submit", function (e) {
    console.log('onsub: ', "$fila_plantilla");
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$('#prestamo-prestamo_id_selector').on('change', function () {
    $.get(
        $url_manejarDesdeCompra +'&prestamo_id=' + $(this).val() + '&fila_plantilla=' + "$fila_plantilla" + '&primera_vez=false&accion=mostrar_datos',
        {},
        function (data) {
            $('.modal-body').html(data);
            $('#modal').modal();
            $('.modal-header').css('background', '#3c8dbc'); //Color del header
            $('.modal-title').html("Seleccione una empresa para trabajar"); // Título para el header del modal
        }
    )
});

$('button.btn-movimiento').click(function () {
    let boton = $(this);
    let prestamo_id = $('#prestamo-prestamo_id_selector').val();
    let accion = boton.attr('accion');
    let url = $url_manejarDesdeCompra + '&prestamo_id=' + prestamo_id + '&fila_plantilla=' + "$fila_plantilla" + '&primera_vez=false&accion=' + accion;
    $.ajax({
        url: url,
        type: 'get',
        data: {},
        success: function (data) {
            $('.modal-body', modal).html("");
            $('.modal-body', modal).html(data);
            $('#modal').trigger('change');
        }
    })
});

$(document).ready(function () {

    applyDateInput();
    removeCheckBoxExtraLabels();

    $('input[id^="prestamo-"]').prop('readonly', true);
    $('select[id^="prestamo"]').prop('readonly', true)
})
JS;

    $this->registerJs($script);
    ?>
</div>
