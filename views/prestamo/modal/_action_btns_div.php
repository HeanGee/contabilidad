<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 12/10/2018
 * Time: 10:30
 */

use yii\helpers\Html;

?>

<?php
echo Html::beginTag('div', ['class' => 'form-group text-right btn-toolbar']);
echo Html::submitButton('Guardar', ['class' => 'btn btn-success']);
if (!in_array(Yii::$app->controller->action->id, ['create', 'update', 'delete', 'index'])) {
    echo Html::button('<span class="glyphicon glyphicon-arrow-right"></span>', [
        'id' => 'btn_forward',
        'style' => "display: yes;",
        'title' => 'Siguiente',
        'class' => 'btn btn-primary pull-right btn-movimiento',
        'accion' => 'siguiente',
    ]);
    echo Html::button('<span class="glyphicon glyphicon-trash"></span>', [
        'id' => 'btn_delete_current',
        'style' => "display: yes;",
        'title' => 'Borrar actual',
        'class' => 'btn btn-warning pull-right btn-movimiento',
        'accion' => 'borrar-actual',
    ]);
    echo Html::button('<span class="glyphicon glyphicon-arrow-left"></span>', [
        'id' => 'btn_rewind',
        'style' => "display: yes;",
        'title' => 'Anterior',
        'class' => 'btn btn-primary pull-right btn-movimiento',
        'accion' => 'anterior',
    ]);
}
echo Html::tag('br/');
echo Html::endTag('div');
?>
