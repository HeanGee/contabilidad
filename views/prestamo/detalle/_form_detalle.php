<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 20/09/2018
 * Time: 11:23
 */

use backend\modules\contabilidad\models\Prestamo;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $form ActiveForm */
/* @var $model Prestamo */
?>

    <fieldset>
        <legend class="text-info">&nbsp;Cuotas
            <div class="pull-right">
                <?php
                // new detalle button
                //            echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                //                'id' => 'new-detalle',
                //                'class' => 'pull-left ight btn btn-success'
                //            ])

                echo (Yii::$app->controller->action->id == 'create') ? Html::a('Recalcular', 'javascript:void(0);', [
                    'id' => 'btn-recalcular-todo',
                    'class' => 'btn btn-warning pull-right'
                ]) : null;

                ?>
            </div>
        </legend>

        <?php
        echo '<table id="tabla-detalles" class="table table-condensed table-responsive">';
        echo '<thead>';
        echo '<tr>';
        echo '<th style="text-align: center;"># Cuota</th>';
        echo '<th style="text-align: center;">Vencimiento</th>';
        echo '<th style="text-align: center;">Capital</th>';
        echo '<th style="text-align: center;">Interés</th>';
        echo '<th style="text-align: center;">IVA 10%</th>';
        echo '<th style="text-align: center;">Monto Cuota</th>';
        echo (Yii::$app->controller->id == 'prestamo' && Yii::$app->controller->action->id == 'update') ?
            '<th style="text-align: center;">Acciones</th>' : null;
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        echo $this->render('_fila_total');

        // existing detalles fields
        $skey = 'cont_prestamo_cuotas';
        $detalles = Yii::$app->session->get($skey)->allModels;
        $key = 0;
        foreach ($detalles as $index => $prestamo_detalle) {
            $key = $prestamo_detalle->id != null ? $prestamo_detalle->id - 1 : $index;
            echo '<tr class="fila-cuotas">';
            echo $this->render('_form_detalle_fields', [
                'key' => 'new' . ($key + 1),
                'form' => $form,
                'detalle' => $prestamo_detalle,
            ]);
            echo '</tr>';
            $key++;
        }

        echo '</table>';

        \kartik\select2\Select2Asset::register($this);

        $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
        ?>

    </fieldset>
<?php
$action_id = Yii::$app->controller->action->id;

$script_head = <<<JS
var action_id = "$action_id";
var detalle_k = $key;
JS;

$this->registerJs($script_head, \yii\web\View::POS_HEAD);
