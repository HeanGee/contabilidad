<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 20/09/2018
 * Time: 15:13
 */

use backend\modules\contabilidad\models\Prestamo;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model Prestamo */

?>

<?php
$columns = [
    [
        'class' => SerialColumn::className(),
    ],

    [
        'label' => 'Nro Cuota',
        'value' => 'nro_cuota'
    ],
    [
        'label' => 'Vencimiento',
        'value' => 'formattedVencimiento'
    ],
    [
        'label' => 'Capital',
        'value' => 'formattedCapital',
        'contentOptions' => ['style' => 'text-align:right'],
    ],
//    [
//        'label' => 'Saldo',
//        'value' => 'formattedCapitalSaldo'
//    ],
    [
        'label' => 'Interés',
        'value' => 'formattedInteres',
        'contentOptions' => ['style' => 'text-align:right'],
    ],
//    [
//        'label' => 'Saldo',
//        'value' => 'formattedInteresSaldo'
//    ],
    [
        'label' => 'Cuota',
        'value' => 'formattedMontoCuota',
        'contentOptions' => ['style' => 'text-align:right'],
    ],
    [
        'label' => 'Asiento de Devengamiento',
        'format' => 'raw',
        'value' => function ($model) {
            if ($model->asiento_devengamiento_id != '') {
                $url = Url::to(['asiento/view', 'id' => $model->asiento_devengamiento_id]);
                $value = '<span class="fa fa-check text-success"></span>&nbsp;&nbsp;' . Html::a('Ver', $url);
            } else {
                $value = '<span class="fa fa-times text-danger"></span>';
            }
            return $value;
        },
        'contentOptions' => ['style' => 'text-align:center'],
    ],
//    [
//        'label' => 'Saldo',
//        'value' => 'formattedMontoCuotaSaldo'
//    ],
];

$template = '{view}';
$buttons = [];

//array_push($columns, [
//    'class' => ActionColumn::class,
//    'template' => $template,
//    'buttons' => []
//]);

$dataProvider = new ArrayDataProvider([
    'allModels' => $model->prestamoDetalles,
    'pagination' => false,
]);

$nombre_sistema_cuota = Prestamo::tipoIntereses()[$model->tipo_interes];
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'hover' => true,
        'id' => 'grid',
        'panel' => ['type' => 'info', 'heading' => "Cuotas (Sistema {$nombre_sistema_cuota})", 'footer' => false,],
        'toolbar' => [
        ]
    ]);
} catch (Exception $exception) {
    throw $exception;
}
?>