<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 07/11/18
 * Time: 11:57 AM
 */

use kartik\number\NumberControl;

?>

<?php
$skey = 'cont_prestamo_cuotas';
$detalles = Yii::$app->session->get($skey)->allModels;
$capital_t = 0;
$interes_t = 0;
$monto_c_t = 0;
$interes_impuesto = 0;

foreach ($detalles as $index => $prestamo_detalle) {
    $capital_t += (int)$prestamo_detalle->capital;
    $interes_t += (int)$prestamo_detalle->interes;
    $monto_c_t += (int)$prestamo_detalle->monto_cuota;
    $interes_impuesto += (int)$prestamo_detalle->impuesto;
}

try {
    echo '<tr>';
    {
        echo '<td></td>';
        echo '<td></td>';

        echo '<td>';
        {
            echo NumberControl::widget([
                'name' => 'total-capital',
                'id' => 'capital-total',
                'readonly' => true,
                'value' => $capital_t,
                'class' => 'form-control',
                'maskedInputOptions' => [
                    'prefix' => '₲ ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 0
                ],
                'displayOptions' => [
                    'class' => 'form-control kv-monospace float-right',
                    'style' => [
                        'font-weight' => 'bold',
                    ]
                ]
            ]);
        }
        echo '</td>';

        echo '<td>';
        {
            echo NumberControl::widget([
                'name' => 'total-interes',
                'id' => 'interes-total',
                'readonly' => true,
                'value' => $interes_t,
                'class' => 'form-control',
                'maskedInputOptions' => [
                    'prefix' => '₲ ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 0
                ],
                'displayOptions' => [
                    'class' => 'form-control kv-monospace float-right',
                    'style' => [
                        'font-weight' => 'bold',
                    ]
                ]
            ]);
        }
        echo '</td>';

        echo '<td>';
        {
            echo NumberControl::widget([
                'name' => 'total-impuesto',
                'id' => 'impuesto-total',
                'readonly' => true,
                'value' => $interes_impuesto,
                'class' => 'form-control',
                'maskedInputOptions' => [
                    'prefix' => '₲ ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 0
                ],
                'displayOptions' => [
                    'class' => 'form-control kv-monospace float-right',
                    'style' => [
                        'font-weight' => 'bold',
                    ]
                ]
            ]);
        }
        echo '</td>';

        echo '<td>';
        {
            echo NumberControl::widget([
                'name' => 'total-cuota',
                'id' => 'monto_cuota-total',
                'readonly' => true,
                'value' => $monto_c_t,
                'class' => 'form-control',
                'maskedInputOptions' => [
                    'prefix' => '₲ ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 0
                ],
                'displayOptions' => [
                    'class' => 'form-control kv-monospace float-right',
                    'style' => [
                        'font-weight' => 'bold',
                    ]
                ]
            ]);
        }
        echo '</td>';
    }
    echo '</tr>';
} catch (Exception $e) {
    print $e;
}
?>
