<?php

use kartik\helpers\Html; ?>
<td style="width: 90px;">
    <?= $form->field($detalle, 'nro_cuota')->textInput([
        'id' => "PrestamoDetalle_{$key}_nro_cuota",
        'name' => "PrestamoDetalle[$key][nro_cuota]",
        'class' => "form-control nro-cuota",
        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'vencimiento')->textInput([
        'id' => "PrestamoDetalle_{$key}_vencimiento",
        'name' => "PrestamoDetalle[$key][vencimiento]",
        'class' => "form-control fecha",
        'data-inputmask-alias' => "dd-mm-yyyy",
        'data-inputmask' => "'yearrange': { 'minyear': '1917', 'maxyear': '2080' }",
        'data-val' => "true",
        'data-val-required' => "Required",
        'placeholder' => "dd-mm-yyyy",
        'style' => 'text-align: center;'
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'capital')->textInput([
        'id' => "PrestamoDetalle_{$key}_capital",
        'name' => "PrestamoDetalle[$key][capital]",
        'class' => "form-control monto",
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'interes')->textInput([
        'id' => "PrestamoDetalle_{$key}_interes",
        'name' => "PrestamoDetalle[$key][interes]",
        'class' => "monto",
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'impuesto')->textInput([
        'id' => "PrestamoDetalle_{$key}_impuesto",
        'name' => "PrestamoDetalle[$key][impuesto]",
        'class' => "monto",
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($detalle, 'monto_cuota')->textInput([
        'id' => "PrestamoDetalle_{$key}_monto_cuota",
        'name' => "PrestamoDetalle[$key][monto_cuota]",
        'class' => "form-control monto",
        'style' => 'text-align: right;'
    ])->label(false) ?>
</td>

<td class=" " style="text-align: center">
    <?= (Yii::$app->controller->id == 'prestamo' && Yii::$app->controller->action->id == 'update') ?
        Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
            'class' => 'delete-detalle btn btn-danger btn-sm',
            'id' => "PrestamoDetalle_{$key}_delete_button",
            'title' => 'Eliminar fila',
        ]) : null ?>
</td>