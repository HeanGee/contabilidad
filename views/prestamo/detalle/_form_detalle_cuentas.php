<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 20/09/2018
 * Time: 11:23
 */

use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\Prestamo;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $form ActiveForm */
/* @var $model Prestamo */
?>

<fieldset>
    <legend class="text-info">&nbsp;Cuentas
    </legend>
    <?php

    echo '<table id="tabla-detalles" class="table table-condensed">';
    //    echo '<thead>';
    //    echo '<tr>';
    //    echo '<th width="20%" style="text-align: center;">Para</th>';
    //    echo '<th width="20%" style="text-align: center;">Cuenta</th>';
    //    echo '<th width="20%" style="text-align: center;">Para</th>';
    //    echo '<th width="20%" style="text-align: center;">Cuenta</th>';
    //    //        echo '<th style="text-align: center;"></th>';
    //    echo '</tr>';
    //    echo '</thead>';
    echo '<tbody>';

    $data = ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text');

    // existing detalles fields
    $detalles = $model->getCuentasFields();
    foreach ($detalles as $index => $field) {
        if ($index % 2 == 0)
            echo '<tr>';

        {
            echo '<td align="right"><strong>';
            {
                echo "{$model->getAttributeLabel($field)}";
            }
            echo '</strong></td>';

            echo '<td>';
            {
                try {
                    echo Select2::widget([
                        'model' => $model,
                        'attribute' => $field,
                        'data' => $data,
                        'options' => ['placeholder' => 'Seleccione uno ...'],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'width' => '85%',
                        ],
                    ]);
                } catch (Exception $exception) {
                    print $exception;
                }
            }
            echo '</td>';
        }

        if ($index % 2 != 0)
            echo '</tr>';
    }

    echo '</tr>';
    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);

    $css = <<<CSS
.table > tbody > tr > td {
    vertical-align: middle;
}
CSS;
    $this->registerCss($css);

    ?>

</fieldset>
