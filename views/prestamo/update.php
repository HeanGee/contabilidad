<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Prestamo */

$this->title = 'Modificar Préstamo #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Préstamos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Préstamo #' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="prestamo-update">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
