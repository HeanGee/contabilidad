<?php

use kartik\daterange\DateRangePicker;
use kartik\helpers\Html;
use kartik\switchinput\SwitchInput;

$this->title = 'Asiento de Cuotas de Prestamo';
$this->params['breadcrumbs'][] = ['label' => 'Préstamos', 'url' => ['']];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $facturaDetallesProvider array */
/* @var $reciboDetallesProvider array */
isset($facturaDetallesProvider) || $facturaDetallesProvider = [];
isset($reciboDetallesProvider) || $reciboDetallesProvider = [];

class ProviderModel extends \yii\base\Model
{
    public $detalle_id;
    public $recibo_prestamo_detalle_id;
    public $factura_compra_id;
    public $subtotal;
    public $partida;
    public $plan_cuenta_id;
    public $cod_completo;
    public $cuenta_nombre;
    public $prestamo_cuota_id;

    public function rules()
    {
        $parent_rules = parent::rules(); // TODO: Change the autogenerated stub
        return array_merge($parent_rules, [
            [['detalle_id', 'recibo_prestamo_detalle_id', 'factura_compra_id', 'subtotal', 'partida', 'plan_cuenta_id',
                'cod_completo', 'cuenta_nombre', 'prestamo_cuota_id'], 'safe'],
        ]);
    }
}
?>

<div class="asiento-cuota-prestamo form-group">
    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-asiento-index') ? Html::a('Ir a Asientos', ['asiento/index'], ['class' => 'btn btn-info']) : null ?>
    </p>

    <?php $form = \kartik\form\ActiveForm::begin(); ?>

    <?php
    try {
        echo '<fieldset>';
        echo '<legend class="text-info"><small class="info">Criterios</small></legend>';
        echo '<div class="row">';
        echo '<div class="col-sm-2">';
        echo '<label class="control-label">Rango de Fecha</label>';
        echo '<div class="form-group drp-container" style="width: 80% !important;">';
        echo DateRangePicker::widget([
            'name' => 'rango_fecha',
            'value' => Yii::$app->request->post('rango_fecha'),
            'convertFormat' => true,
            'useWithAddon' => false,
            'language' => 'es',
            'pluginOptions' => [
                'allowClear' => true,
                'autoApply' => true,
                'locale' => ['format' => 'd-m-Y'],
                'opens' => 'right'
            ],
        ]);
        echo '</div>';
        echo '</div>';

        echo '<div class="col-sm-2">';
        echo '<label class="control-label">Desde Factura</label>';
        echo SwitchInput::widget(['name' => 'desde_factura', 'value' => Yii::$app->request->post('desde_factura', (Yii::$app->request->isPost) ? false : true), 'pluginOptions' => [
            'onColor' => 'success',
            'offColor' => 'danger',
            'onText' => 'Si',
            'offText' => 'No',
            'size' => 'small',
            'handleWidth' => '60',
        ]]);
        echo '</div>';

        echo '<div class="col-sm-2">';
        echo '<label class="control-label">Desde Recibo</label>';
        echo SwitchInput::widget(['name' => 'desde_recibo', 'value' => Yii::$app->request->post('desde_recibo', (Yii::$app->request->isPost) ? false : true), 'pluginOptions' => [
            'onColor' => 'success',
            'offColor' => 'danger',
            'onText' => 'Si',
            'offText' => 'No',
            'size' => 'small',
            'handleWidth' => '60',
        ]]);
        echo '</div>';
        echo '</div>';

        echo Html::a('Vista Previa', ['prestamo-asiento/generar-asiento-cuota'], [
                'class' => 'btn btn-primary',
                'data' => [
                    'method' => 'post',
                ]
            ]) . '&nbsp;&nbsp;';
        echo Html::a('Generar y guardar', ['prestamo-asiento/generar-asiento-cuota', 'guardar' => 'si'], [
            'class' => 'btn btn-success',
            'data' => [
                'method' => 'post',
            ]
        ]);
        echo '</fieldset>';

        if (isset($facturaDetallesProvider) && sizeof($facturaDetallesProvider) ||
            isset($reciboDetallesProvider) && sizeof($reciboDetallesProvider)) {
            echo '<br/><fieldset>';
            echo '<legend class="text-info"><small class="info">Vista previa de asientos</small></legend>';
            echo (isset($facturaDetallesProvider) && sizeof($facturaDetallesProvider)) ? $this->render('gridview-cuentas', [
                'facturaDetallesProvider' => $facturaDetallesProvider
            ]) : '';
            echo (isset($reciboDetallesProvider) && sizeof($reciboDetallesProvider)) ? $this->render('gridview-cuentas-recibo', [
                'reciboDetallesProvider' => $reciboDetallesProvider
            ]) : '';
            echo '</fieldset>';
        }
    } catch (\Exception $exception) {
        echo $exception;
    }
    ?>

    <?php \kartik\form\ActiveForm::end(); ?>
</div>

<?php
$script = <<<JS
$(document).ready(function () {
    $.pjax.reload({container: "#flash_message_id", async: false});
});
JS;
$this->registerJs($script);
