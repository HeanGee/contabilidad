<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 01/03/19
 * Time: 03:21 PM
 */

use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use kartik\helpers\Html;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $reciboDetallesProvider array */
isset($reciboDetallesProvider) || $reciboDetallesProvider = [];
?>

<?php
$cuotas = [];
$models = [];
$total_partidas = ['debe' => 0, 'haber' => 0];
foreach ($reciboDetallesProvider as $detalle) {
    if (!in_array($detalle['prestamo_cuota_id'], $cuotas))
        $cuotas[] = $detalle['prestamo_cuota_id'];

    $model = new ProviderModel();
    $load['ProviderModel'] = $detalle;
    $model->load($load);
    $models[] = $model;

    $total_partidas[$model->partida] += (float)$model->subtotal;
}

try {
    $columns = [
        [
            'class' => SerialColumn::className(),
            'contentOptions' => ['style' => 'font-size: 90%;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],

        [
            'label' => 'Código cuenta',
            'value' => 'cod_completo',
            'contentOptions' => ['style' => 'font-size: 90%; width: 160px !important;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'label' => 'Cuenta',
            'value' => 'cuenta_nombre',
            'contentOptions' => function ($model) {
                return ['style' => 'font-size: 90%; width: 600px !important;' . (($model->partida == 'debe') ? '' : ' text-align: right;')];
            },
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'label' => 'Debe',
            'value' => function ($model) {
                return ($model->partida == 'debe') ? number_format($model->subtotal, 2, ',', '.') : 0;
            },
            'contentOptions' => ['style' => 'font-size: 90%; text-align: right; width: 200px !important;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'label' => 'Haber',
            'value' => function ($model) {
                return ($model->partida == 'haber') ? number_format((float)$model->subtotal, 2, ',', '.') : 0;
            },
            'contentOptions' => ['style' => 'font-size: 90%; text-align: right; width: 200px !important;'],
            'headerOptions' => ['style' => 'font-size: 90%;'],
        ],
        [
            'label' => 'Nº de Cuota',
            'value' => function ($model) {
                $prestamoDetalle = \backend\modules\contabilidad\models\PrestamoDetalle::findOne(['id' => $model->prestamo_cuota_id]);
                $fecha = implode('-', array_reverse(explode('-', $prestamoDetalle->prestamo->fecha_operacion)));
                $entidad = $prestamoDetalle->prestamo->entidad;
                $razon_social = substr($entidad->razon_social, 0, 22);
                $razon_social .= (strlen($razon_social) == strlen($entidad->razon_social)) ? '' : '...';
                $text = "Cuota #$prestamoDetalle->nro_cuota - Préstamo #{$prestamoDetalle->prestamo->id} del $fecha de $razon_social - $entidad->ruc";
                return $text;
            },
            'contentOptions' => ['style' => 'font-size: 90%; text-align: center;'],
            'headerOptions' => ['style' => 'font-size: 90%; text-align: center;'],
        ],
    ];

    echo GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $models, 'pagination' => false]),
        'columns' => $columns,
        'hover' => true,
        'id' => 'grid',
        'panel' => ['type' => 'primary', 'heading' => 'Asientos desde recibos de cuotas', 'footer' => false,],
        'toolbar' => [
        ]
    ]);

    echo Html::beginTag('div', ['class' => 'parent']);
    echo Html::beginTag('div', ['class' => 'pull-right child']);
    echo Html::label('Total Haber: ', 'total-haber-recibo-disp');
    echo NumberControl::widget([
        'name' => 'totalchaber',
        'id' => 'total-haber-recibo',
        'readonly' => true,
        'value' => $total_partidas['haber'],
        'class' => 'form-control',
        'maskedInputOptions' => [
            'prefix' => '₲ ',
            'groupSeparator' => '.',
            'radixPoint' => ',',
            'digits' => 0
        ],
        'displayOptions' => [
            'class' => 'form-control kv-monospace float-right',
            'placeholder' => 'Total Haber...',
            'style' => [
                'width' => '95%',
//                'margin' => '0 auto',
//                'text-align' => 'right',
//                'clear' => 'right',
//                'float' => 'right',
//                'position' => 'relative',
                'font-weight' => 'bold',
            ]
        ]
    ]);
    echo Html::endTag('div');
    echo Html::beginTag('div', ['class' => 'pull-right child']);
    echo Html::label('Total Debe: ', 'total-debe-recibo-disp');
    echo NumberControl::widget([
        'name' => 'total-debe',
        'id' => 'total-debe-recibo',
        'readonly' => true,
        'value' => $total_partidas['debe'],
        'class' => 'form-control',
        'maskedInputOptions' => [
            'prefix' => '₲ ',
            'groupSeparator' => '.',
            'radixPoint' => ',',
            'digits' => 0
        ],
        'displayOptions' => [
            'class' => 'form-control kv-monospace float-right',
            'placeholder' => 'Total Debe...',
            'style' => [
                'width' => '95%',
//                'margin' => '0 auto',
//                'text-align' => 'right',
//                'clear' => 'right',
//                'float' => 'right',
//                'position' => 'relative',
                'font-weight' => 'bold',
            ]
        ]
    ]);
    echo Html::endTag('div');
    echo Html::tag('br');
    echo Html::tag('br');
    echo Html::tag('br');
    echo Html::tag('br');
    echo Html::endTag('div');
} catch (\Exception $exception) {
    echo $exception;
} ?>
