<?php

use backend\helpers\Helpers;
use backend\helpers\HtmlHelpers;
use common\helpers\FlashMessageHelpsers;
use console\controllers\CronController;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/** @var $this View */

$this->title = 'On-line Log file Viewer.';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$files = [];
$dir = "/var/log/jmg-contabilidad-log/";
if ($handle = opendir($dir)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $files[$entry] = $entry;
        }
    }
    closedir($handle);
}
setlocale(LC_TIME, 'es_ES');
Pjax::begin(['id' => 'prestamo-log-viewer']);
try {
    echo '<div class="row">';
    {
        echo '<div class="col-sm-3">';
        echo Html::label('Seleccione archivo de log', 'log_file_selector');
        echo Select2::widget([
            'id' => 'log_file_selector',
            'name' => 'log_file_selector',
            'data' => $files,
            'options' => ['placeholder' => 'Seleccione uno ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
            'initValueText' => Yii::$app->session->get('log-file', ''),
            'value' => Yii::$app->session->get('log-file', ''),
        ]);
        echo '</div>';

        echo '<div class="col-sm-3">';
        echo Html::label('Ahora', 'fecha');
        $now = ucfirst(strftime("%A, %e de %B del %Y"));
        echo Html::input('text', 'fecha', $now, ['class' => 'form-control', 'style' => 'readonly: true', 'disabled' => true]);
        echo '</div>';
    }
    echo '</div>';

    echo '<br/><legend class="text-info"><small>Registros</small></legend>';
    $file_name = Yii::$app->session->get('log-file', "");
    $path = "/var/log/jmg-contabilidad-log/$file_name";
    try {
        fopen($path, 'r');
        $content = Helpers::tailCustom($path, 40);
    } catch (Exception $exception) {
        $content = "";
    } ?>
    <div id="container">
        <div class="box1" style="width: 23%; position: relative; vertical-align: top;">
            <div class="cab"><h4></h4></div>
            <div class="con"></div>
        </div>
        <div class="box2" style="width: 76%; position: relative; text-align: left;">
            <table class="tg table" style="undefined; table-layout: auto; width: 100%">
                <tr>
                    <th class="tg-gh1y" style="vertical-align: middle; background-color: lightgrey; width: 10%">Fecha
                    </th>
                    <th class="tg-gh1y" style="vertical-align: middle; background-color: lightgrey; width: 9%">Hora
                    </th>
                    <th class="tg-gh1y" style="vertical-align: middle; background-color: lightgrey; width: 12%">Día
                    </th>
                    <th class="tg-gh1y" style="vertical-align: middle; background-color: lightgrey;">Mensaje</th>
                </tr>
                <?php
                foreach (preg_split("/((\r?\n)|(\r\n?))/", $content) as $line) {
                    $slices = explode(';', $line);
                    $statusCode = array_shift($slices);
                    $date = array_shift($slices);
                    $hour = array_shift($slices);
                    $day = array_shift($slices);
                    $message = array_shift($slices);
                    if ($statusCode == '') continue;
                    switch ($statusCode) {
                        case CronController::WARNING:
                            $color = "#" . HtmlHelpers::WarningColorHex(true);
                            break;
                        case CronController::ERROR:
                            $color = "#" . HtmlHelpers::DangerColorHex(true);
                            break;
                        case CronController::INFO:
                            $color = "#" . HtmlHelpers::InfoColorHex(true);
                            break;
                        case CronController::SUCCESS:
                            $color = "#" . HtmlHelpers::SuccessColorHex(true);
                    }

                    if ($statusCode != '') {
                        echo "<tr>";
                        echo "<td style='vertical-align: middle; width: 10%'>{$date}</td>";
                        echo "<td style='vertical-align: middle; width: 9%'>{$hour}</td>";
                        echo "<td style='vertical-align: middle; width: 12%'>{$day}</td>";
                        echo "<td style='vertical-align: middle; background-color: {$color}'>{$message}</td>";
                        echo "</tr>";
                    } else
                        echo "<tr><td colspan='4'></td></tr>";
                } ?>
            </table>
        </div>
    </div>
    <style>
        #container > div {
            display: inline-block;
            /*border: solid 1px #0f0f0f;*/
        }

        #container {
            /*text-align: center;*/
            margin: 0 auto;
            width: 100%;
            /*border: solid 1px #9f191f;*/
        }

        div.cab {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: bold;
            /*border-bottom: 1px solid dimgrey;*/
        }

        div.cab > h4 {
            color: dimgrey;
            vertical-align: top;
            text-decoration: underline;
            font-size: 23px;
        }
    </style>
<?php } catch (Exception $exception) {
    echo $exception;
    FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 5000);
}
Pjax::end();

ob_start() ?>

    <script>
        var isOpened = false;
    </script>

<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean()), View::POS_HEAD);

ob_start();
?>

    <script>
        $(document).on("select2:open", "#log_file_selector", function (e) {
            isOpened = true;
            // console.log('abre');
        }).on("select2:close", "#log_file_selector", function (e) {
            isOpened = false;
            // console.log('cierra');
        });

        (function worker() {
            console.log("worker...");
            let fileSelector = $('#log_file_selector');
            let wasOpened = isOpened;
            fileSelector.select2('close');
            fileSelector.select2("destroy");
            fileSelector.html("<option><option>");
            fileSelector.select2({
                theme: 'krajee',
                placeholder: '',
                language: 'en',
                width: '100%',
                data: [],
            });
            $.ajax({
                type: 'get',
                url: '<?= Url::to(['ver-log-get-files']) ?>',
                data: {},
                success: function (data) {
                    let arr = [{id: "", text: ''}];
                    data['results'].forEach(function (e, i) {
                        arr.push(e);
                    });

                    // Rellenar select2 con las opciones
                    fileSelector.select2({
                        theme: 'krajee',
                        placeholder: '',
                        language: 'en',
                        width: '100%',
                        data: arr,
                    });
                    // console.log(wasOpened);
                    // if (wasOpened === true)
                    //     fileSelector.select2('open');
                }
            });
            $.pjax.reload({container: "#prestamo-log-viewer", async: false});
            $.pjax.reload({container: "#flash_message_id", async: false});
            // setTimeout(worker, 5000);
        })();

        $(document).on('change', '#log_file_selector', function () {
            console.log('changed');
            let value = $(this).val();
            $.ajax({
                type: 'get',
                url: '<?= Url::to(['ver-log-select-file'])?>',
                data: {
                    log_file_selector: value,
                },
                success: function (result) {
                    $.pjax.reload({container: "#prestamo-log-viewer", async: false});
                    $.pjax.reload({container: "#flash_message_id", async: false});
                    $('div.cab >h4 ').html(result[0]);
                    $('div.con').html((value === '') ? ('') : ("Tamaño: " + (result[1] < 100000 ? (result[1] / 1000) + ' K' : (result[1] / 1000000) + ' M') + "bytes."));
                }
            });
        });
    </script>

<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>