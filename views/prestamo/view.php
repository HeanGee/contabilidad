<?php

use backend\modules\contabilidad\models\Prestamo;
use backend\modules\contabilidad\models\PrestamoDetalle;
use kartik\detail\DetailView;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Prestamo */

$this->title = "Prestamo #{$model->id}";
$this->params['breadcrumbs'][] = ['label' => 'Prestamos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
    <div class="prestamo-view">

        <p>
            <?php $permisos = [
                'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-prestamo-view'),
                'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-prestamo-index'),
                'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-prestamo-create'),
                'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-prestamo-update'),
                'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-prestamo-delete'),
                'bloquear' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-prestamo-bloquear'),
                'desbloquear' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-prestamo-desbloquear')
            ]; ?>
            <?= $permisos['index'] ? Html::a('Ir a Préstamos', ['index'/*, 'operacion' => Yii::$app->request->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
            <?= $permisos['update'] && $model->bloqueado == 'no' ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
            <?= $permisos['delete'] && $model->bloqueado == 'no' ? Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Está seguro?',
                    'method' => 'post',
                ],
            ]) : null ?>
            <?= "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" ?>
            <?= $permisos['bloquear'] && $model->bloqueado == 'no' ? Html::a('<span>Bloquear</span>', ['bloquear', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'title' => Yii::t('app', 'Bloquear'),
                    'data-confirm' => 'Está seguro que desea bloquear el prestamo?'
                ]) : '' ?>
            <?= $permisos['bloquear'] && $model->bloqueado == 'si' ? Html::a('<span>Desbloquear</span>', ['desbloquear', 'id' => $model->id],
                [
                    'class' => 'btn btn-success',
                    'title' => Yii::t('app', 'Desbloquear'),
                    'data-confirm' => 'Está seguro que desea desbloquear el prestamo?'
                ]) : '' ?>
        </p>

        <?php try {

            echo DetailView::widget([
                'model' => $model,
                'condensed' => true,
                'hover' => true,
                'mode' => DetailView::MODE_VIEW,
                'enableEditMode' => false,
                'fadeDelay' => true,
                'panel' => [
                    'heading' => 'Prestamo',
                    'type' => DetailView::TYPE_INFO,
                ],
                'attributes' => [
                    [
                        'columns' => [
//                            [
//                                'attribute' => 'id',
//                                'value' => $model->id,
//                                'label' => "Nro. de Credito",
//                                'valueColOptions' => ['style' => 'width:30%']
//                            ],
                            [
                                'attribute' => 'fecha_operacion',
                                'value' => $model->getFormattedFechaOperacion(),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'attribute' => 'primer_vencimiento',
                                'value' => $model->getFormattedPrimerVencimiento(),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [

                            [
                                'attribute' => 'tipo_interes',
                                'value' => Prestamo::tipoIntereses()[$model->tipo_interes],
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'attribute' => 'vencimiento',
                                'value' => $model->getFormattedVencimiento(),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [

                            [
                                'attribute' => 'monto_operacion',
                                'value' => $model->getFormattedMontoOperacion(),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'attribute' => 'cant_cuotas',
                                'value' => $model->cant_cuotas,
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [

                            [
                                'attribute' => 'intereses_vencer',
                                'value' => $model->getFormattedInteresesVencer(),
                                'valueColOptions' => ['style' => 'width:30%']

                            ],
                            [
                                'attribute' => 'usar_iva_10',
                                'value' => ucfirst($model->usar_iva_10),
                                'valueColOptions' => ['style' => 'width:30%']

                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'attribute' => 'gastos_bancarios',
                                'value' => $model->getFormattedGastosBancarios(),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'attribute' => 'tasa',
                                'value' => (isset($model->tasa) ? number_format($model->tasa, 2, ',', '.') . ' %' : ''),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],

                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'attribute' => 'incluye_iva',
                                'value' => ucfirst($model->incluye_iva),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'attribute' => 'iva_10',
                                'value' => $model->formattedIva10,
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'attribute' => 'entidad_id',
                                'value' => $model->entidad->razon_social,
                                'label' => "Entidad",
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'value' => isset($model->entidad) ? $model->entidad->ruc : '',
                                'label' => 'R.U.C.',
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'attribute' => 'empresa_id',
                                'value' => $model->empresa->razon_social,
                                'label' => "Empresa",
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'attribute' => 'periodo_contable_id',
                                'value' => $model->periodoContable->anho,
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                ],
            ]);

            echo $this->render('detalle/_view_detalle', ['model' => $model]);

            \yii\widgets\Pjax::begin(['id' => 'simulador_asiento_grid']);
            echo $this->render('simulacion_asiento/_simulador_asiento', ['panel_color' => 'info']);
            \yii\widgets\Pjax::end();

            // Facturas relacionadas.
            echo $this->render('@backend/modules/contabilidad/views/facturas-relacionadas/_facturas_relacionadas', ['model' => $model]);

            // Facturas cuotas relacionadas.
            echo $this->render('view-details/visor-factura-cuotas.php', ['model' => $model]);

            // Recibos relacionados
            echo $this->render('view-details/visor-recibos-relacionados', ['model' => $model]);

            echo $this->render('../auditoria/_auditoria', ['modelo' => $model]);
            echo $this->render('../auditoria/_auditoria_multiple_models', ['modelo' => new PrestamoDetalle(), 'ids' => $model->getDetallesId()]);

        } catch (Exception $exception) {
            print $exception->getMessage();
        } ?>

    </div>

<?php
$model->monto_operacion = number_format($model->monto_operacion, '2', ',', '.');
$model->intereses_vencer = number_format($model->intereses_vencer, '2', ',', '.');
$model->gastos_bancarios = number_format($model->gastos_bancarios, '2', ',', '.');
$model->caja = number_format($model->caja, '2', ',', '.');
$model->iva_10 = number_format($model->iva_10, '2', ',', '.');
$model_js = \yii\helpers\Json::encode($model);

$url_simularAsiento = Json::htmlEncode(\Yii::t('app', Url::to(['prestamo/simular-asiento'])));

$script = <<<JS
function cuentas() {
    let array = [];
    let campos = ['cuenta_caja', 'cuenta_intereses_vencer', 'cuenta_gastos_bancarios', 'cuenta_iva_10', 'cuenta_gastos_no_deducibles', 'cuenta_monto_operacion', 'cuenta_intereses_a_pagar'];
    
    campos.forEach(function(campo, orden) {
        let model = $model_js;
        let fila = {atributo: campo, valor: model[campo]};
        array.push(fila);
    });

    return array;
}
function simularAsiento() {
    
    let data = $model_js;
    
    let tipo_interes = $('#prestamo-tipo_interes');
    if (tipo_interes.val() !== '') {
        $.ajax({
            url: $url_simularAsiento,
            type: 'get',
            data: {
                cuentas: cuentas(),
                tipo_interes: data.tipo_interes,
                vencimiento: data.vencimiento,
                primer_venc: data.primer_venc,
                cant_cuotas: data.cant_cuotas,
                monto_operacion: data.monto_operacion,
                interes: data.intereses_vencer,
                gastos_bancarios: data.gastos_bancarios,
                neto_entregar: data.caja,
                capital: data.capital,
                iva_10: data.iva_10,
                usar_iva_10: data.usar_iva_10,
                incluye_iva: data.incluye_iva,
                // iva_interes_cobrado: data.iva_interes_cobrado,
                tiene_factura: data.tiene_factura,
            },
            success: function(data) {
                if (data.error) {
                    $.pjax.reload({container: "#flash_message_id", async: false});
                }
                $.pjax.reload({container: "#simulador_asiento_grid", async: false});
            }   
        });
    }
}

$(document).ready(function() {
   simularAsiento(); 
});
JS;

$this->registerJs($script);
