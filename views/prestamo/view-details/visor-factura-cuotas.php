<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 24/10/18
 * Time: 10:02 AM
 */

use backend\modules\contabilidad\models\Compra;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use kartik\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;

/* @var $model \backend\models\BaseModel */
/* @var $model \backend\models\BaseModel */
?>

<?php

/** ------------------------ [inicio] Facturas relacionadas ------------------------ */
// Obtener compras o ventas y a la vez determinar el nombre para label de la columna del gridview.
$label = 'Cliente';
$error = false;
$allModels = null;

$allModels = Compra::find()->alias('compra')
    ->leftJoin('cont_prestamo_detalle_compra as pdc', 'pdc.factura_compra_id = compra.id')
    ->leftJoin('cont_prestamo_detalle as pd', 'pd.id = pdc.prestamo_detalle_id')
    ->leftJoin('cont_prestamo as p', 'p.id = pd.prestamo_id')
    ->where(['p.id' => $model->id])
    ->all();


if (!$error) {

    $label = 'Cliente';

    $columns = [
        [
            'class' => SerialColumn::className(),
        ],

        'id',
        [
            'label' => 'Proveedor',
            'value' => function ($model, $key, $index, $column) {
                if ($model instanceof Compra)
                    return $model->entidad->razon_social;
                return "";
            },
        ],
        [
            'label' => 'Factura No',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                if ($model instanceof Compra) {
                    $url = Url::to(['compra/view', 'id' => $model->id]);
                    return Html::a((isset($model) && isset($model->id)) ? $model->nro_factura : '', $url);
                }
                return "";
            },
        ],
    ];

    $template = '{view}';
    $buttons = [];

//    array_push($columns, [
//        'class' => ActionColumn::class,
//        'template' => $template,
//        'buttons' => $buttons,
//        'urlCreator' => function ($action, $model, $key, $index) {
//            if ($action === 'view') {
//                $url = Url::to(['compra/view', 'id' => $model->id]);
//                return $url;
//            }
//            return '';
//        },
//    ]);

    $dataProvider = new ArrayDataProvider([
        'allModels' => $allModels,
        'pagination' => false,
    ]);

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'hover' => true,
            'id' => 'grid',
            'panel' => ['type' => 'info', 'heading' => 'Facturas de Cuotas', 'footer' => false,],
            'toolbar' => [
            ]
        ]);
    } catch (Exception $exception) {
        print $exception->getMessage();
    }
}
/** ------------------------ [fin] Facturas relacionadas ------------------------ */
?>
