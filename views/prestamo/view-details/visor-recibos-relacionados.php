<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 24/10/18
 * Time: 10:02 AM
 */

use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use kartik\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;

/* @var $model \backend\models\BaseModel */
/* @var $model \backend\models\BaseModel */
?>

<?php

/** ------------------------ [inicio] Facturas relacionadas ------------------------ */
// Obtener compras o ventas y a la vez determinar el nombre para label de la columna del gridview.
$label = 'Cliente';
$error = false;
$allModels = null;

try {
    $allModels = $model instanceof \backend\modules\contabilidad\models\Prestamo ? $model->getRecibos()->all() : $error = true;
} catch (Exception $exception) {
    $error = true;
}

if (!$error) {

    $label = 'Cliente';

    $columns = [
        [
            'class' => SerialColumn::className(),
        ],

        [
            'label' => $label,
            'value' => function ($model, $key, $index, $column) {
                if ($model instanceof \backend\modules\contabilidad\models\ReciboPrestamo)
                    return $model->entidad->razon_social;
                return "";
            },
        ],
        array(
            'label' => 'Nro Recibo',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                if ($model instanceof \backend\modules\contabilidad\models\ReciboPrestamo) {
                    $url = Url::to(['recibo-prestamo/view', 'id' => $model->id]);
                    return Html::a(isset($model->id) ? $model->numero : '', $url);
                }
                return null;
            },
        ),
    ];

    $template = '{view}';
    $buttons = [];

//    array_push($columns, [
//        'class' => ActionColumn::class,
//        'template' => $template,
//        'buttons' => $buttons,
//        'urlCreator' => function ($action, $model, $key, $index) {
//            if ($action === 'view') {
//                $url = Url::to(['recibo-prestamo/view', 'id' => $model->id]);
//                return $url;
//            }
//            return '';
//        },
//    ]);

    $dataProvider = new ArrayDataProvider([
        'allModels' => $allModels,
        'pagination' => false,
    ]);

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'hover' => true,
            'id' => 'grid',
            'panel' => ['type' => 'info', 'heading' => 'Recibos relacionados', 'footer' => false,],
            'toolbar' => [
            ]
        ]);
    } catch (Exception $exception) {
        print $exception->getMessage();
    }
}
/** ------------------------ [fin] Facturas relacionadas ------------------------ */
?>
