<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Prestamo */

$this->title = 'Registrar Nuevo Préstamo';
$this->params['breadcrumbs'][] = ['label' => 'Préstamos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prestamo-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
