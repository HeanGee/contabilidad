<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\search\PrestamoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prestamo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nro_credito') ?>

    <?= $form->field($model, 'fecha_operacion') ?>

    <?= $form->field($model, 'capital') ?>

    <?= $form->field($model, 'interes') ?>

    <?php // echo $form->field($model, 'vencimiento') ?>

    <?php // echo $form->field($model, 'tasa') ?>

    <?php // echo $form->field($model, 'cant_cuotas') ?>

    <?php // echo $form->field($model, 'periodo_contable_id') ?>

    <?php // echo $form->field($model, 'empresa_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
