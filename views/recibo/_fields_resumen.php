<?php

use kartik\number\NumberControl;
use yii\helpers\Html;

try {
    echo Html::beginTag('div', ['class' => 'parent']);
    echo Html::beginTag('div', ['class' => 'float-right child']);
    echo Html::label('Error de redondeo: ', 'error-redondeo-disp');
    echo NumberControl::widget([
        'name' => 'error-redondeo',
        'id' => 'error-redondeo',
        'readonly' => true,
        'class' => 'form-control',
        'maskedInputOptions' => [
            'prefix' => '',
            'groupSeparator' => '.',
            'radixPoint' => ',',
            'digits' => 2
        ],
        'displayOptions' => [
            'class' => 'form-control kv-monospace float-right',
            'placeholder' => '',
            'style' => [
//                'width' => '34.9%',
//                'margin' => '0 auto',
//                'text-align' => 'right',
//                'clear' => 'right',
//                'float' => 'right',
//                'position' => 'relative',
                'font-weight' => 'bold',
            ]
        ]
    ]);

    echo Html::endTag('div');
    echo Html::beginTag('div', ['class' => 'float-right child']);
    echo Html::label('Total Cuentas: ', 'total-cuenta-disp');
    echo NumberControl::widget([
        'name' => 'total-cuenta',
        'id' => 'total-cuenta',
        'readonly' => true,
        'class' => 'form-control',
        'maskedInputOptions' => [
            'prefix' => '',
            'groupSeparator' => '.',
            'radixPoint' => ',',
            'digits' => 2
        ],
        'displayOptions' => [
            'class' => 'form-control kv-monospace float-right',
            'placeholder' => '',
            'style' => [
//                'width' => '34.9%',
//                'margin' => '0 auto',
//                'text-align' => 'right',
//                'clear' => 'right',
//                'float' => 'right',
//                'position' => 'relative',
                'font-weight' => 'bold',
            ]
        ]
    ]);
    echo Html::endTag('div');
    echo Html::beginTag('div', ['class' => 'float-right child']);
    echo Html::label('Total Recibo detalle: ', 'total-recibo-detalle-disp');
    echo NumberControl::widget([
        'name' => 'total-recibo-detalle',
        'id' => 'total-recibo-detalle',
        'readonly' => true,
        'class' => 'form-control',
        'maskedInputOptions' => [
            'prefix' => '',
            'groupSeparator' => '.',
            'radixPoint' => ',',
            'digits' => 2
        ],
        'displayOptions' => [
            'class' => 'form-control kv-monospace float-right',
            'placeholder' => '',
            'style' => [
//                'width' => '10%',
//                'margin' => '0 auto',
//                'text-align' => 'right',
//                'clear' => 'right',
//                'float' => 'right',
//                'position' => 'relative',
                'font-weight' => 'bold',
            ]
        ]
    ]);
    echo Html::endTag('div');
    echo Html::tag('br');
    echo Html::tag('br');
    echo Html::tag('br');
    echo Html::tag('br');
    echo Html::endTag('div');
} catch (Exception $exception) {
    echo $exception;
}


$CSS = <<<CSS
.float-right {
  float: right;
}
.child {
  /*border: 1px solid indigo;*/
  padding-left: 1rem;
}
CSS;
$this->registerCss($CSS);