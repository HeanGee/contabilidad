<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 10/07/2018
 * Time: 8:28
 */

use backend\modules\contabilidad\controllers\ReciboDetalleTemp;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $model backend\modules\contabilidad\models\Recibo */
/* @var $form yii\widgets\ActiveForm */
/* @var $detalles \backend\modules\contabilidad\models\ReciboDetalle[] */

?>

<fieldset>
    <legend>&nbsp;Detalles
        <?php
        // new detalle button
        echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
            'id' => 'recibo-new-detalle-button',
            'class' => 'pull-left ight btn btn-success'
        ]);

        echo Html::a('Igualar total', 'javascript:void(0);', [
            'id' => 'set-total-equal-details',
            'class' => 'pull-right btn btn-success',
            //'data' => ['confirm' => "Desea establecer el total de la factura igual a la suma de los montos de facturas?"],
            'style' => 'display:none;',
        ]);
        ?>
    </legend>
    <?php

    $detalle = new ReciboDetalleTemp();
    $detalle->factura_tipo = $model->factura_tipo;
    //        $detalle->loadDefaultValues();
    echo '<table id="recibo-detalles" class="table table-condensed table-bordered">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Factura Nro</th>';
    echo '<th style="text-align: center;">' . $detalle->getAttributeLabel('monto') . '</th>';
    echo '<th style="text-align: center;">Moneda</th>';
    echo '<th style="text-align: center;">Cotizac. Factura</th>';
    echo '<th style="text-align: center;">Cotizac. utilizar</th>';
    echo '<th style="text-align: center;">Valorizado</th>';
    echo '<th style="text-align: center;">Saldo</th>';
    echo '<th style="text-align: center;">Acciones</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    $campo_factura_id = Yii::$app->getRequest()->getQueryParam('operacion') == 'compra' ? "factura_compra_id" : 'factura_venta_id';
    // existing detalles fields
    $key = 0;
    foreach ($detalles as $index => $_reciboDetalle) {
        $key = $_reciboDetalle->id != null ? $_reciboDetalle->id - 1 : $index;
        echo '<tr class="fila-facturas">';
        echo $this->render('detalle/_form-recibo-detalle', [
//            'key' => $_reciboDetalle->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_reciboDetalle->id,
            'key' => 'new' . ($key + 1),
            'form' => $form,
            'detalle' => $_reciboDetalle,
            'campo' => $campo_factura_id,
            'class' => 'form-control selector-factura no-selector-factura',
            'class_monto' => 'form-control sumable-factura no-event-factura',
            'class_btn_borrar' => ' no-event-btn-borrar-factura',
        ]);
        echo '</tr>';
        $key++;
    }

    // new detalles fields
    echo '<tr id="recibo-new-detalle-block" style="display: none;">';
    echo $this->render('detalle/_form-recibo-detalle', [
        'key' => 'new',
        'form' => $form,
        'detalle' => $detalle,
        'campo' => $campo_factura_id,
        'class' => 'form-control selector-factura',
        'class_monto' => 'form-control sumable-factura',
        'class_btn_borrar' => '',
    ]);
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);
    $operacion = Yii::$app->getRequest()->getQueryParam('operacion');
    $actionID = Yii::$app->controller->action->id;

    $url_checkRuc = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/check-ruc', 'operacion' => $operacion])));
    $url_getFacturaByEntidad = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-facturas-by-entidad', 'operacion' => $operacion])));
    ?>

    <?php ob_start(); // output buffer the javascript to register later ?>
    <script>
        var detalle_k = <?php echo isset($key) ? $key : 0; ?>;
        let campo_id = "<?php echo Yii::$app->getRequest()->getQueryParam('operacion') == 'compra' ? 'factura_compra_id' : 'factura_venta_id' ?>";
        $('#recibo-new-detalle-button').on('click', function () {
            if ($('#recibo-entidad_id').val() === null) {
                krajeeDialog.confirm('Debe especificar una entidad primero.', function (result) {
                    // if (result)
                    //     $('#recibo-entidad_id').select2('open'); // se abre el select2 mientras la animacion de modal-off sigue en pie.
                });
                return false;
            }
            let id_campo_factura_id = "<?php echo $campo_factura_id; ?>";
            let inputs_factura_id = $('.fila-facturas :input[name$="[' + id_campo_factura_id + ']"]'), i;
            let array_factura_id = [];
            for (i = 0; i < inputs_factura_id.length; i++) {
                array_factura_id.push(inputs_factura_id[i].value);
            }

            $.ajax({
                url: <?php echo $url_getFacturaByEntidad; ?>,
                type: 'get',
                data: {
                    entidad_id: $('#recibo-entidad_id').val(),
                    facturas_id: array_factura_id,
                    action_id: "<?= $actionID ?>",
                    fecha_recibo: $('#recibo-fecha').val(), // -disp envia d-m-Y, sin -disp envia Y-m-d
                },
                success: function (data) {
                    // Obtener facturas por ruc
                    if (data.length === 0) {
                        krajeeDialog.alert('No hay factura pendiente. O falta cargar cotización de la SET.');
                        return false;
                    }

                    detalle_k += 1;
                    $('#recibo-detalles').find('tbody')
                        .append('<tr class="fila-facturas">' + $('#recibo-new-detalle-block').html().replace(/new/g, 'new' + detalle_k) + '</tr>');

                    $('#Detalles_new' + detalle_k + '_' + campo_id).select2({
                        theme: 'krajee',
                        placeholder: '',
                        language: 'en',
                        width: '100%',
                        data: data,
                    });

                    // let tiene_decimales = $('#Detalles_new' + detalle_k + '_' + campo_id).select2('data')[0]['moneda']['tiene_decimales'];
                    // generarInputNumber($('#Detalles_new' + detalle_k + '_monto'), tiene_decimales);
                    // generarInputNumber($('#Detalles_new' + detalle_k + '_valor_moneda'), "si");
                    // generarInputNumber($('#Detalles_new' + detalle_k + '_saldo'), tiene_decimales);
                    onChangeFacturaId('#Detalles_new' + detalle_k + '_' + campo_id);
                    $('#Detalles_new' + detalle_k + '_' + campo_id).val(data[0]['id']).trigger('change');
                    //
                    // autocompletarMontoSaldo(
                    //     '#Detalles_new' + detalle_k + '_' + campo_id,
                    //     '#Detalles_new' + detalle_k + '_moneda_nombre',
                    //     '#Detalles_new' + detalle_k + '_monto',
                    //     '#Detalles_new' + detalle_k + '_valor_moneda',
                    //     '#Detalles_new' + detalle_k + '_saldo',
                    // );

                    // onchangeMontoSaldo(
                    //     '#Detalles_new' + detalle_k + '_monto');

                    // remove detalle button
                    clickDeleteButtonFactura(
                        '#Detalles_new' + detalle_k + '_delete_button',
                        '#Detalles_new' + detalle_k + '_monto'
                    );
                }
            });
        });

        <?php
        //        if (!Yii::$app->request->isPost && $model->isNewRecord)
        // TODO: moverlo a documento on ready.
        //        if (Yii::$app->controller->action->id == 'create')
        //            echo "$('#recibo-new-detalle-button').click();";
        ?>

    </script>
    <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>

</fieldset>