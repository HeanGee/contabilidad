<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<td>
    <?= $form->field($detalle, $campo)->textInput([
        'id' => "Detalles_{$key}_" . $campo,
        'name' => "Detalles[$key][" . $campo . "]",
        'class' => $class,
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($detalle, 'monto')->textInput([
        'id' => "Detalles_{$key}_monto",
        'name' => "Detalles[$key][monto]",
        'class' => $class_monto,
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($detalle, 'moneda_nombre')->textInput([
        'id' => "Detalles_{$key}_moneda_nombre",
        'name' => "Detalles[$key][moneda_nombre]",
        'value' => Yii::$app->getRequest()->getQueryParam('operacion') == 'compra' ?
            ($detalle != null && $detalle->factura_compra_id ? $detalle->compra->moneda->nombre : "")
            :
            ($detalle != null && $detalle->factura_venta_id ? $detalle->venta->moneda->nombre : ""),
        'readonly' => true,
        'style' => 'text-align: center;',
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($detalle, 'valor_moneda')->textInput([
        'id' => "Detalles_{$key}_valor_moneda",
        'name' => "Detalles[$key][valor_moneda]",
        'value' => Yii::$app->getRequest()->getQueryParam('operacion') == 'compra' ?
            ($detalle != null && $detalle->factura_compra_id ? $detalle->compra->cotizacion : '')
            :
            ($detalle != null && $detalle->factura_venta_id ? $detalle->venta->valor_moneda : ''),
        'readonly' => true,
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($detalle, 'cotizacion')->textInput([
        'id' => "Detalles_{$key}_cotizacion",
        'name' => "Detalles[$key][cotizacion]",
        'class' => 'form-control cotizacion-detalle',
        'readonly' => true,
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($detalle, 'valorizado')->textInput([
        'id' => "Detalles_{$key}_valorizado",
        'name' => "Detalles[$key][valorizado]",
        'class' => 'form-control valorizado-detalle ',
        'readonly' => false,
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($detalle, 'saldo')->textInput([
        'id' => "Detalles_{$key}_saldo",
        'name' => "Detalles[$key][saldo]",
        'readonly' => true,
        'style' => 'text-align: right;',
        'class' => 'form-control campo-saldo'
    ])->label(false) ?>
</td>
<td class=" " style="text-align: center">
    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'recibo-remove-detalle-button btn btn-danger btn-sm' . $class_btn_borrar,
        'id' => "Detalles_{$key}_delete_button",
        'title' => 'Eliminar fila',
    ]) ?>
    &nbsp;&nbsp;
    <!--<? //= Html::a('<i class="glyphicon glyphicon-pencil"></i>', 'javascript:void(0);', [
    //        'class' => 'recibo-add-retencion-button btn btn-primary' . $class_btn_borrar,
    //        'id' => "Detalles_{$key}_add_retencion_button",
    //        'title' => 'Agregar/Editar retenciones',
    //    ]) ?>    -->
    <?= Html::button('<i class="fa fa-anchor"></i>', [
        'id' => "Detalles_{$key}_btn_retencion",
        'style' => "display: yes;",
        'type' => 'button',
        'title' => 'Manejar retenciones.',
        'class' => 'btn btn-success btn-sm tiene_modal',
        'data-toggle' => 'modal',
        'data-target' => '#modal',
        'data-url' => Url::to(['retencion/manejar-retenciones']),
        'data-pjax' => '0',
    ]); ?>
</td>