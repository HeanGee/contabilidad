<?php

use yii\helpers\Html;

?>
<td>
    <?= $form->field($detalle, $campo)->textInput([
        'id' => "Detallescontracuenta_{$key}_" . $campo,
        'name' => "Detallescontracuenta[$key][" . $campo . "]",
        'class' => $class,
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($detalle, 'monto')->textInput([
        'id' => "Detallescontracuenta_{$key}_monto",
        'name' => "Detallescontracuenta[$key][monto]",
        'class' => $class_monto,
    ])->label(false) ?>
</td>
<td style="text-align: center;">
    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'recibo-remove-detalle-contracuenta-button btn btn-danger btn-sm' . $class_btn_borrar,
        'id' => "Detallescontracuenta_{$key}_delete_button",
    ]) ?>
</td>