<?php

use kartik\helpers\Html;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ReciboDetalle */
/* @var $index int */
/* @var $form yii\widgets\ActiveForm */
/* @var $campo_factura_id string */
/* @var $facturas_data array */
?>
<td>
    <?= $form->field($model, "[{$index}]{$campo_factura_id}")->widget(Select2::className(), [
        'options' => [
            'placeholder' => 'Seleccione...',
            'class' => 'form-control select2-hidden-accessible factura-selector'
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'data' => $facturas_data,
        ],
        'initValueText' => !empty($model->factura_compra_id) ? $model->factura_compra_id : !empty($model->factura_venta_id) ? $model->factura_venta_id : '',
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($model, "[{$index}]monto")->textInput(['class' => 'agregarNumber'])->label(false) ?>
</td>
<td>
    <?= $form->field($model, "[{$index}]moneda_nombre")->textInput(['class' => 'agregarNumber', 'readonly' => true, 'style' => 'text-align: center;'])->label(false) ?>
</td>
<td style="text-align: center">
    <?= Html::a('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', 'javascript:void(0);', [
        'class' => 'eliminar-detalle-btn btn btn-danger btn-xs',
    ]) ?>
    <?= Html::a('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', 'javascript:void(0);', [
        'class' => 'add-retencion-btn btn btn-primary btn-xs',
    ]) ?>
</td>
