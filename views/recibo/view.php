<?php

use common\helpers\FlashMessageHelpsers;
use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Recibo */

$actionId = Yii::$app->controller->action->id;
$operacion = Yii::$app->request->getQueryParam('operacion');
$this->title = 'Datos del Recibo: ' . $model->numero;
$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' => ['index', 'operacion' => $operacion]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-delete'),
            'bloquear' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-bloquear'),
            'desbloquear' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-recibo-desbloquear'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Recibos', ['index', 'operacion' => $operacion], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] && $model->bloqueado == 'no' ? Html::a('Modificar', ['update', 'id' => $model->id, 'operacion' => $operacion], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] && $model->bloqueado == 'no' ? Html::a('Eliminar', ['delete', 'id' => $model->id, 'operacion' => $operacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Está seguro de eliminar este recibo?',
                'method' => 'post',
            ],
        ]) : null ?>
        <?= "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" ?>
        <?= $permisos['bloquear'] && $model->bloqueado == 'no' ? Html::a('<span>Bloquear</span>', ['bloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'title' => Yii::t('app', 'Bloquear'),
                'data-confirm' => 'Está seguro que desea bloquear el recibo?'
            ]) : '' ?>
        <?= $permisos['bloquear'] && $model->bloqueado == 'si' ? Html::a('<span>Desbloquear</span>', ['desbloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-success',
                'title' => Yii::t('app', 'Desbloquear'),
                'data-confirm' => 'Está seguro que desea desbloquear el recibo?'
            ]) : '' ?>
    </p>

    <?php try {
        // Datos del recibo.
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'numero',
                'fecha',
                ['attribute' => 'monedaNombre', 'label' => 'Moneda',],
                'valor_moneda',
                ['attribute' => 'totalFormatted', 'label' => 'Total'],
            ],
        ]);

        // Facturas relacionadas.
        echo $this->render('@backend/modules/contabilidad/views/facturas-relacionadas/_facturas_relacionadas', ['model' => $model]);

        echo $this->render('../auditoria/_auditoria', ['modelo' => $model]);
    } catch (Exception $e) {
        FlashMessageHelpsers::createWarningMessage($e->getMessage());
    } ?>

</div>
