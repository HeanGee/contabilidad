<?php

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\Recibo;
use backend\modules\contabilidad\models\ReciboDetalle;
use backend\modules\contabilidad\models\ReciboDetalleContracuentas;
use backend\modules\contabilidad\models\Retencion;
use backend\modules\contabilidad\models\Venta;
use common\models\ParametroSistema;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\datecontrol\DateControl;
use kartik\dialog\Dialog;
use kartik\dialog\DialogAsset;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Recibo */
/* @var $form yii\widgets\ActiveForm */
/* @var $detallescontracta ReciboDetalleContracuentas[] */
/* @var $detalles ReciboDetalle[] */
?>

<?php
$actionId = Yii::$app->controller->action->id;
$operacion = Yii::$app->getRequest()->getQueryParam('operacion');
$empresa_id = \Yii::$app->session->get('core_empresa_actual');
$periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
$moneda_base = ParametroSistema::getMonedaBaseId();

$tipo_retencion = ($operacion == 'compra') ? "cuenta_retenciones_emitidas" : "cuenta_retenciones_recibidas";
$parmName = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-{$tipo_retencion}";
$parmCont = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $parmName]);
if (!isset($parmCont))
    echo HtmlHelpers::BootstrapInfoPanel("Información", "Si desea ver el asiento de retenciones en la simulación,
 &nbsp;edite la empresa actual y establezca las cuentas correspondientes a retenciones `emitidas` y `recibidas`.", '', true, true);
else {
    $tipo_retencion = ($operacion == 'compra') ? "cuenta_retenciones_renta_emitidas" : "cuenta_retenciones_renta_recibidas";
    $parmName = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-{$tipo_retencion}";
    $parmCont = \backend\modules\contabilidad\models\ParametroSistema::findOne(['nombre' => $parmName]);
    if (!isset($parmCont))
        echo HtmlHelpers::BootstrapInfoPanel("Información", "Si desea ver el asiento de retenciones en la simulación,
 &nbsp;edite la empresa actual y establezca las cuentas correspondientes a retenciones `emitidas` y `recibidas`.", '', true, true);
}
?>

    <div class="recibo-form">

        <?php $form = ActiveForm::begin(['id' => 'form-recibo-and-detalle']); ?>

        <?php
        try {
            echo $form->errorSummary($model);

            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 3,
                        'attributes' => [
                            'fecha' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => DateControl::class,
                                'options' => [
                                    'type' => DateControl::FORMAT_DATE,
                                    'ajaxConversion' => false,
                                    'language' => 'es',
                                    'widgetOptions' => [
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'dd-MM-yyyy',
                                            'todayHighlight' => true,
                                            'weekStart' => '0',
                                        ],
                                    ],
                                ]
                            ],
                            'numero' => [
                                'type' => Form::INPUT_TEXT,
                                'options' => [
                                    'maxlength' => 15,
                                ],
                            ],
                            'moneda_id' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    //                            'data' => Recibo::getMonedas(true),
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                        'data' => Recibo::getMonedas(),
                                    ],
                                    'initValueText' => !empty($model->moneda_id) ? ($model->moneda_id . ' - ' . $model->moneda->nombre) : null
                                ],
                                'label' => 'Moneda',
                            ],
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 4,
                        'attributes' => [
                            'entidad_id' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    //                        'data' => Yii::$app->getRequest()->getQueryParam('operacion') == 'compra' ? Compra::getEntidades() : Venta::getEntidades(),
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                        //                                'ajax' => [
                                        //                                    'url' => Url::to(['recibo/get-entidades', 'operacion' => $operacion]),
                                        //                                    'dataType' => 'json',
                                        //                                    'data' => new JsExpression("
                                        //                                    function(params) {
                                        //                                        return {
                                        //                                            q:params.term,
                                        //                                            id:$('#recibo-fecha').val(),
                                        //                                        };
                                        //                                    }
                                        //                                "),
                                        //                                ]
                                    ],
                                    'initValueText' => !empty($model->entidad_id) ? ($model->entidad->razon_social . ' - ' . $model->entidad->ruc) : null
                                ],
                            ],
                            //                    'ruc' => [
                            //                        'type' => Form::INPUT_TEXT,
                            //                    ],
                            //                    'razon_social' => [
                            //                        'label' => 'Entidad',
                            //                        'type' => Form::INPUT_TEXT,
                            //                        'options' => [
                            //                            'readonly' => true
                            //                        ]
                            //                    ],
                            'valor_moneda' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => NumberControl::class,
                                'options' => [
                                    'value' => 0.00,
                                    'maskedInputOptions' => [
                                        'groupSeparator' => '.',
                                        'radixPoint' => ',',
                                        'rightAlign' => true,
                                        'allowMinus' => false,
                                    ],
                                    'readonly' => true,
                                ],
                                'label' => 'Cotización',
                            ],
                            'total' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => NumberControl::class,
                                'options' => [
                                    'readonly' => false,
                                    'value' => 0.00,
                                    'maskedInputOptions' => [
                                        'groupSeparator' => '.',
                                        'radixPoint' => ',',
                                        'rightAlign' => true,
                                        'allowMinus' => false,
                                    ],
                                ],
                                'label' => $model->getAttributeLabel('total') . ' del Recibo' . HtmlHelpers::InfoHelpIcon('total-info'),
                            ],
                            'monto_extra' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => NumberControl::class,
                                'options' => [
                                    'readonly' => true,
                                    'value' => 0.00,
                                    'maskedInputOptions' => [
                                        'groupSeparator' => '.',
                                        'radixPoint' => ',',
                                        'rightAlign' => true,
                                        'allowMinus' => true,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 3,
                        'attributes' => [
                            'factura_tipo' => [
                                'type' => Form::INPUT_HIDDEN,
                            ],
                        ],
                    ],
                ],
            ]);

            echo Dialog::widget();

        } catch (Exception $e) {
            print $e;
        } ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success',
                "data" => (Yii::$app->controller->action->id == 'update') ? [
                    'confirm' => 'Desea guardar los cambios?',
                    'method' => 'post',
                ] : []
            ]) ?>
            <!--<? //= Html::a('Simular', '', ['class' => 'btn btn-info', 'id' => 'btn-simular']) ?>            -->
        </div>

        <?php
        echo $this->render('_fieldset_facturas', [
            'model' => $model,
            'form' => $form,
            'detalles' => $detalles,
        ]);
        echo $this->render('_fieldset_contracuentas', [
            'model' => $model,
            'form' => $form,
            'detallescontracta' => $detallescontracta
        ]);

        echo $this->render('_fields_resumen', []);

        echo $this->render('simulator/_form_asientoSimulator', []);
        ?>

        <!--<?php //echo $this->render('detalle/_form-recibo-detalle', []); ?>    -->

        <?php ActiveForm::end(); ?>

    </div>

<?php
DialogAsset::register($this);
$CSS = <<<CSS
input.total-recibo, label.total-recibo-label {
    width: 34.9%;
    float: right;
    clear: both;
}
CSS;
$this->registerCss($CSS);

$url_getCotizacion = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-cotizacion', 'operacion' => $operacion])));
$url_montoValidator = Url::toRoute(['recibo/recibo-detalle-monto-validator', 'operacion' => $operacion]);
$url_simularAsiento = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/simular-asiento', 'operacion' => $operacion])));
$url_getRazonSocialByRuc = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-razon-social-by-ruc'])));
$url_getFacturaByEntidad = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-facturas-by-entidad', 'operacion' => $operacion])));
$url_getCotizacionFactura = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-cotizacion-factura', 'operacion' => $operacion])));
$url_simularAsientoRemoverItem = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/simular-asiento-remover-item', 'operacion' => $operacion])));
$url_getEntidadesConFacturaCredito = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-entidades-fact-credito', 'operacion' => $operacion])));
$url_getMontoCurrentDetalleContracta = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-monto-current-detalle-contracta'])));
$url_getMontoCotizacionCurrentDetalle = Json::htmlEncode(\Yii::t('app', Url::to(['recibo/get-monto-and-cotizacion-current-detalle'])));

$_campo_factura = $operacion == 'compra' ? 'factura_compra_id' : 'factura_venta_id';
$data_select2_facturas = $operacion == 'compra' ? Compra::getFacturas(false) : Venta::getFacturas();
$planCuentaLista = PlanCuenta::getCuentaLista(true);
$planCuentaLista = Json::encode($planCuentaLista);
$data_select2_facturas = Json::encode($data_select2_facturas);
$model_id = $model->id;
$moneda_base_id = ParametroSistema::getMonedaBaseId();
$entidad_asociada_id = $actionId == 'update' ? $model->entidad_id : ""; // id de entidad del recibo en modificacion
$moneda_prefixs = [];
foreach (\backend\models\Moneda::find()->all() as $moneda) {
    $moneda_prefixs["{$moneda->id}"] = "{$moneda->simbolo} ";
}
$moneda_prefixs = Json::encode($moneda_prefixs);

$errors = false;
try {
    $tipoDocSet = Retencion::getTipodocSetRetencion();
    if ($operacion == 'compra') {
        $timbrado = Retencion::getTimbradoRetencion();
    }
} catch (\Exception $exception) {
    $errors = $exception->getMessage();
}

$script_pos_head = <<<JS
var entidad_asociada_id = "$entidad_asociada_id"; // id de entidad del recibo en modificacion
var factura_monto_saldo = [];
var moneda_prefixs = $moneda_prefixs;
var global_recibo_id = "$model_id";
var cot_detalle_manual_edit = false;
var valorizado_det_changedByAnother = true;
var from_document_ready = false;
JS;
$this->registerJs($script_pos_head, \yii\web\View::POS_HEAD);


$scripts = <<<JS
/** <---------------------- JS_VARIABLES ---------------------->**/

/** <---------------------- JS_VARIABLES ---------------------->**/


/** <---------------------- JS_FUNCTIONS ---------------------->**/
// function generarInputNumber(elemento, class_name, moneda_id) {
function generarInputNumber(elemento, tiene_decimales) {
    elemento.inputmask({
        "alias": "numeric",
        "digits": tiene_decimales === "si" ? 2 : 0,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ","
    });
}

function campoTotalSetDigits() {
    let campo_moneda_id = $('#recibo-moneda_id');
    let isMonedaSelected = campo_moneda_id.select2('data').length;
    $('#recibo-total-disp').inputmask({
        "alias": "numeric",
        "digits": isMonedaSelected ? (campo_moneda_id.select2('data')[0]['tiene_decimales'] === "si" ? 2 : 0) : 'no',
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "prefix": moneda_prefixs[$('#recibo-moneda_id').val()],
    }).attr('style', "font-weight: bold; text-align: right; ");
}

function setCotizacionDetalle(factura_selector_id) {
    let tiene_decimales = $(factura_selector_id).select2('data')[0]['moneda']['tiene_decimales']; // moneda de la factura, no la del recibo.
    const regex = /Detalles_new([0-9]+)_/gm;
    let m = regex.exec($(factura_selector_id).attr('id'));
    let action_id = "$actionId";

    // Poner cotizacion en el campo cotizacion del detalle si la moneda del recibo es diferente a la de la factura
    if ($(factura_selector_id).select2('data')[0]['moneda_id'] !== $('#recibo-moneda_id').val()) {
        // quitar readonly y limpiar campo cotizacion.
        $('#' + m[0] + 'cotizacion').prop('readonly', false);
        $('#' + m[0] + 'cotizacion').val("").trigger('change');

        // Si moneda de factura es diferente a moneda del recibo y 
        // la moneda de factura NO es guarani, poner cotizacion de la moneda de la factura.
        if ($(factura_selector_id).select2('data')[0]['moneda_id'] !== '1') {
            let operacion = "$operacion";
            let compra_venta = operacion === 'compra' ? 'venta' : 'compra';
            $('#' + m[0] + 'cotizacion').val($(factura_selector_id).select2('data')[0][compra_venta]).trigger('change');
        }
        // Si moneda de factura es diferente a la del recibo y
        // la moneda de factura es guarani, poner la cotizacion de la moneda del recibo.
        else {
            let operacion = "$operacion";
            let compra_venta = operacion === 'compra' ? 'venta' : 'compra';
            $('#' + m[0] + 'cotizacion').val($('#recibo-valor_moneda-disp').val()).trigger('change');
        }
    } else {
        $('#' + m[0] + 'cotizacion').val("").trigger('change');
        $('#' + m[0] + 'cotizacion').prop('readonly', true);
    }
}

function valorizar(thiss) {
    try {
        let monto = thiss.parent().parent().parent().find('input[id$="_monto"]').val(),
            valorizado = 0,
            cotizac_factura = thiss.parent().parent().parent().find('input[id*="_valor_moneda"]').val(),
            cotizac_propia = thiss.parent().parent().parent().find('input[id*="_cotizacion"]').val(),
            monedaField = $('#recibo-moneda_id'),
            valorizadoField = thiss.parent().parent().parent().find('input[id$="_valorizado"]');
        
        if (cotizac_factura === '' || cotizac_factura === 0) cotizac_factura = 1;
        if (cotizac_propia === '' || cotizac_propia === 0) cotizac_propia = 1;
    
        if (thiss.select2('data')[0]['moneda_id'] === monedaField.val())
            valorizadoField.val(monto).trigger('change');
        else {
            if (monedaField.val() === '1') {
                if (cotizac_propia !== 1) {
                    valorizado = monto * cotizac_propia;
                } else {
                    valorizado = monto * cotizac_factura;
                }
            } else {
                if (cotizac_propia !== 1) {
                    valorizado = monto / cotizac_propia;
                } else {
                    valorizado = monto / cotizac_factura;
                }
            }
            valorizado = parseFloat(valorizado).toFixed(2);
            if (monedaField.val() === '1') {
                valorizado = Math.round(valorizado);
            }
            valorizadoField.val(valorizado).trigger('change');
        }
    } catch (err) {
        console.log("Ops! Excepcion desde `valorizar()`:", err);
    }
}

function onChangeFacturaId(id_campo_factura_id) {
    // Prevenir la seleccion, si no existe moneda.
    $(id_campo_factura_id).on('select2:selecting', function (e) {
        if ($('#recibo-moneda_id').select2('data').length === 0) {
            krajeeDialog.alert('Debe seleccionar una Moneda primero.');
            e.preventDefault();
            return false;
        }
    })
    
    // Al seleccionar una factura, completar campo del monto, saldo y cotizacion
    .on('change select2:select', function () {
        if ($(this).select2('data').length > 0) {
            let tiene_decimales = $(this).select2('data')[0]['moneda']['tiene_decimales']; // moneda de la factura, no la del recibo.
            const regex = /Detalles_new([0-9]+)_/gm;
            let m = regex.exec($(this).attr('id'));
            let action_id = "$actionId";
            let id = '#' + $(this).attr('id');
    
            // Aplicar NumberInput
            generarInputNumber($('#' + m[0] + 'monto'), tiene_decimales);
            generarInputNumber($('#' + m[0] + 'valor_moneda'), "si");
            generarInputNumber($('#' + m[0] + 'cotizacion'), "si");
            generarInputNumber($('#' + m[0] + 'saldo'), tiene_decimales);
            generarInputNumber($('#' + m[0] + 'valorizado'), 'si');
    
            // Determinar valores para monto y cotizacion, dependiendo del id de action.
    
            setCotizacionDetalle(id_campo_factura_id);
            $('#' + m[0] + 'saldo').val($(this).select2('data')[0]['saldo']).trigger('change'); // TODO: ver para realizar aparte.
            $('#' + m[0] + 'valor_moneda').val($(this).select2('data')[0]['valor_moneda']).trigger('change');
            $('#' + m[0] + 'valor_moneda').prop('readonly', true);
            $('#' + m[0] + 'moneda_nombre').val($(this).select2('data')[0]['moneda']['nombre']).trigger('change');
            if (action_id === "create") {
                $('#' + m[0] + 'monto').val($(this).select2('data')[0]['saldo']).trigger('change');
                calcularTotalRecibo();
            }
            else {
                if (factura_monto_saldo.filter(p => p.id === $(this).val()).length !== 0) {
                    let fila = factura_monto_saldo.filter(p => p.id === $(this).val())[0];
                    $('#' + m[0] + 'saldo').val(fila.saldo).trigger('change');
                    $('#' + m[0] + 'monto').val(fila.monto).trigger('change');
                    $('#' + m[0] + 'cotizacion').val(fila.cotizacion);
                }
                else {
                    let recibo_detalle_id = m[1], id_campomonto = '#' + m[0] + 'monto';
                    let saldo = $(this).select2('data')[0]['saldo'];
    
                    $.ajax({
                        url: $url_getMontoCotizacionCurrentDetalle,
                        type: 'get',
                        data: {
                            id_recibo_detalle: recibo_detalle_id,
                            recibo_id: "$model_id",
                            factura_id: $('#' + m[0] + "$_campo_factura").val()
                        },
                        success: function (data) {
                            // si no tiene monto, es nuevo detalle, proceder como si fuera un create.
                            if (data === "") {
                                setCotizacionDetalle(id_campo_factura_id);
                                $(id_campomonto).val(saldo).trigger('change'); // se sa el monto = saldo, caso create.
                            } else {
                                data = data.split('|');
                                $('#' + m[0] + 'cotizacion').val(data[1] === 0.0 ? "" : data[1]);
                                $(id_campomonto).val(data[0]).trigger('change');
    
                                // Habilitar/Deshabilitar campos
                                if ($(id_campo_factura_id).select2('data')[0]['moneda_id'] !== $('#recibo-moneda_id').val()) {
                                    $('#' + m[0] + 'cotizacion').prop('readonly', false);
                                } else {
                                    if ($(id_campo_factura_id).select2('data')[0]['moneda_id'] === '1')
                                        $('#' + m[0] + 'cotizacion').prop('readonly', true);
    
                                }
                            }
                            // $('#' + m[0] + 'saldo').val(saldo).trigger('change');
                            // $('#' + m[0] + 'valor_moneda').val($(id).select2('data')[0]['valor_moneda']).trigger('change');
                            // $('#' + m[0] + 'valor_moneda').prop('readonly', true);
                            // $('#' + m[0] + 'moneda_nombre').val($(id).select2('data')[0]['moneda']['nombre']).trigger('change');
                            calcularTotalRecibo();
                            
                            let fila = [];
                            fila = {
                                'id': $(id).val(),
                                'saldo': $('#' + m[0] + 'saldo').val(),
                                'monto': $('#' + m[0] + 'monto').val(),
                                'cotizacion': $('#' + m[0] + 'cotizacion').val()
                            };
                            
                            if (factura_monto_saldo.filter(p => p.id === $(id).val()).length === 0) {
                                factura_monto_saldo.push(fila);                            
                            }
                        },
                    });
                }
            }
        }
    });
}

function applyEventsOnChangeFacturaId() {
    let inputs = document.getElementsByClassName('sumable-factura'), i;
    for (i = 0; i < inputs.length; i++) {
        const regex = /Detalles_new([0-9]+)_/gm;
        let m = regex.exec($(inputs[i]).attr('id'));
        if (m !== null && m[0] !== null) {
            onChangeFacturaId('#' + m[0] + '$_campo_factura');
        }
    }
}

function onchangeMontoSaldo(campo_monto) {
    // $(campo_monto).on('change focusout', function () {
    //     calcularTotalRecibo();
    //     simular();
    // });
}

// function onselectContracuenta(campo_id, campo_monto) {
function onselectContracuenta(campo_id) {
    $(campo_id).on('change', function () {
        let tiene_decimales = $('#recibo-moneda_id').select2('data')[0]['tiene_decimales'];
        const regex = /Detallescontracuenta_new([0-9]+)_/gm;
        let m = regex.exec($(this).attr('id'));

        generarInputNumber($('#' + m[0] + 'monto'), tiene_decimales);
    });
}

function onchangeMontoContraCta(campo_monto) {
    $(campo_monto).on('change focusout', function (evt) {
        // no hace falta calcular total de recibo porque el mismo se basa en el monto que se especifica por cada factura.
        simular();
    });
}

function setEventsOnDeleteRowButtonFactura(className) {
    let inputs = document.getElementsByClassName(className), i;
    for (i = 0; i < inputs.length; i++) {
        let boton_id = '#' + $(inputs[i]).attr('id');
        let campo_monto_id = boton_id.substring(0, (boton_id.length - "delete_button".length)) + 'monto';
        clickDeleteButtonFactura(boton_id, campo_monto_id);
    }
}

function setEventsOnDeleteRowButtonContracta(className) {
    let inputs = document.getElementsByClassName(className), i;
    for (i = 0; i < inputs.length; i++) {
        let boton_id = '#' + $(inputs[i]).attr('id');
        let campo_monto_id = boton_id.substring(0, (boton_id.length - "delete_button".length)) + 'monto';
        clickDeleteButtonContracta(boton_id, campo_monto_id);
    }
}

// TODO: cambiar esta logica de usar getMontoCotizacioNCurrentDetalle.
function clickDeleteButtonFactura(btn_borrar_id, campo_monto_id) {
    $(document).on('click', btn_borrar_id, function () {
        removeFactura(campo_monto_id);
        let thiss = $(this).closest('tbody tr');
        thiss.remove();
        calcularTotalRecibo();

//        let action_id = "$actionId";
//        if (action_id === "update") {
//            const regex = /Detalles_new([0-9]+)_/gm;
//            let m = regex.exec(campo_monto_id);
//            if (m !== null) {
//                let msg = 'ADVERTENCIA: Esta acción se aplicará directamente en la BASE DE DATOS si es que el detalle existe en la misma. Desea proceder?';
//                let id_recibo_detalle = m[1]; // el número concatenado, en update, es id del detalle en la primera fila y posiblemente algunas subsecuentes.
//                let thiss = $(this).closest('tbody tr');
//                $.ajax({
//                    url: $url_getMontoCotizacionCurrentDetalle,
//                    type: 'get',
//                    data: {
//                        id_recibo_detalle: id_recibo_detalle,
//                        recibo_id: "$model_id",
//                        factura_id: $('#' + m[0] + "$_campo_factura").val()
//                    },
//                    // TODO: utilizar otro ajax.
//                    success: function (data) {
//                        // lanzar confirmación si hay detalle en la base de datos. Si hay monto, existe en la BD.
//                        if (data !== "") {
//                            if (confirm(msg) === true) {
//                                removeFactura(campo_monto_id);
//                                thiss.remove();
//                                calcularTotalRecibo();
//                            }
//                        } else {
//                            removeFactura(campo_monto_id);
//                            thiss.remove();
//                            calcularTotalRecibo();
//                        }
//                    },
//                });
//            }
//        } else {
//            removeFactura(campo_monto_id);
//            $(this).closest('tbody tr').remove();
//            calcularTotalRecibo();
//        }
    });
}

function clickDeleteButtonContracta(btn_borrar_id, campo_monto_id) {
    $(document).on('click', btn_borrar_id, function () {
        let action_id = "$actionId";
        if (action_id === "update") {
            const regex = /Detallescontracuenta_new([0-9]+)_/gm;
            let m = regex.exec(campo_monto_id);
            if (m !== null) {
                let msg = 'ADVERTENCIA: Esta acción se aplicará directamente en la BASE DE DATOS si es que el detalle existe en la misma. Desea proceder?';
                let id_recibo_detalle_contracta = m[1];
                let thiss = $(this).closest('tbody tr');
                $.ajax({
                    url: $url_getMontoCurrentDetalleContracta,
                    type: 'get',
                    data: {
                        id_recibo_detalle_contracta: id_recibo_detalle_contracta,
                        recibo_id: "$model_id"
                    },
                    success: function (data) {
                        if (data !== "") { // si hay monto, el detalle existe en la BD.
                            if (confirm(msg) === true) {
                                removeContracta(campo_monto_id);
                                thiss.remove();
                            }
                        } else {
                            removeContracta(campo_monto_id);
                            thiss.remove();
                        }
                    },
                });
            }
        } else {
            removeContracta(campo_monto_id);
            $(this).closest('tbody tr').remove();
        }
    });
}

function removeFactura(campo_monto_id) {
    let sim_url = $url_simularAsientoRemoverItem;
    $.ajax({
        url: sim_url,
        type: 'post',
        data: {
            id_input: $(campo_monto_id).attr('id'),
            del: 'factura',
            recibo_id: "$model_id",
            action_id: "$actionId",
        },
        success: function (data) {
            if (data === '') $.pjax.reload({container: "#flash_message_id", async: false});
            $.pjax.reload({container: "#asiento-simulator-div", async: false});
        }
    });
}

function removeContracta(campo_monto_id) {
    let sim_url = $url_simularAsientoRemoverItem;
    $.ajax({
        url: sim_url,
        type: 'post',
        data: {
            id_input: $(campo_monto_id).attr('id'),
            del: 'contracta',
            recibo_id: "$model_id",
            action_id: "$actionId",
        },
        success: function (data) {
            if (data === '') $.pjax.reload({container: "#flash_message_id", async: false});
            $.pjax.reload({container: "#asiento-simulator-div", async: false});
        }
    });
}

function applyCssToContractaSumableInputs() {
    let inputs = document.getElementsByClassName('sumable-contracta');
    for (i = 0; i < inputs.length; i++) {
        let campo_moneda_id = $('#recibo-moneda_id');
        let isMonedaSelected = campo_moneda_id.select2('data').length;
        let tiene_decimales = isMonedaSelected ? campo_moneda_id.select2('data')[0]['tiene_decimales'] : 'no';
        generarInputNumber($(inputs[i]), tiene_decimales);
    }
}

function applyCssToSelectors(data, className) {
    let inputs = document.getElementsByClassName(className), i;
    for (i = 0; i < inputs.length; i++) {
        $(inputs[i]).select2({
            theme: 'krajee',
            placeholder: '',
            language: 'en',
            width: '100%',
            data: data,
        });
    }
    if (className === "no-selector-factura") cargarFacturas();
}

function cargarFacturas() {
    let campos_monto = $('tr.fila-facturas :input[id$="monto"]'), i;
    for (i = 0; i < campos_monto.length; i++) {
        let element_campo_monto = $(campos_monto[i]);
        const regex = /Detalles_new([0-9]+)_/gm;
        let m = regex.exec(element_campo_monto.attr('id'));
        let campo_factura_id = m[0] + "factura_compra_id";
        let element = document.getElementById(campo_factura_id);
        if (element === null)
            campo_factura_id = m[0] + "factura_venta_id";


        let inputs_factura_id = $('.fila-facturas :input[name$="[factura_venta_id]"]');
        if (inputs_factura_id.length === 0)
            inputs_factura_id = $('.fila-facturas :input[name$="[factura_compra_id]"]');
        let array_factura_id = [], j;
        for (j = 0; j < (inputs_factura_id.length - campos_monto.length + i); j++) {
            array_factura_id.push(inputs_factura_id[j].value);
        }
        $.ajax({
            url: $url_getFacturaByEntidad,
            type: 'get',
            data: {
                entidad_id: $('#recibo-entidad_id').val(),
                facturas_id: array_factura_id,
                action_id: "$actionId",
                fecha_recibo: $('#recibo-fecha').val(), // -disp envia d-m-Y, sin -disp envia Y-m-d
            },
            success: function (data) {
                $('#' + m[0] + "$_campo_factura").select2('destroy');
                $('#' + m[0] + "$_campo_factura").select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: data,
                });
                $('#' + m[0] + "$_campo_factura").attr("selected", "selected");
                $('#' + m[0] + "$_campo_factura").trigger('change');
            }
        });
    }
}

function setEventsOnFacturaMonto(className) {
    let inputs = document.getElementsByClassName(className), i;
    for (i = 0; i < inputs.length; i++) {
        const regex = /Detalles_new([0-9]+)_/gm;
        let campo_monto_id = $(inputs[i]).attr('id');
        let m = regex.exec(campo_monto_id);
        if (m !== null && m[1] !== null) {
            onchangeMontoSaldo('#' + campo_monto_id);
        }
    }
}

function setEventsOnContractaMonto(className) {
    let inputs = document.getElementsByClassName(className), i;
    for (i = 0; i < inputs.length; i++) {
        const regex = /Detallescontracuenta_new([0-9]+)_/gm;
        let campo_monto_id = $(inputs[i]).attr('id');
        let m = regex.exec(campo_monto_id);
        if (m !== null && m[1] !== null) {
            onchangeMontoContraCta('#' + campo_monto_id);
        }
    }
}

function getDetalles() {
    let inputs_monto_factura = document.getElementsByClassName('sumable-factura'), i;
    let simulacion_post = [];
    for (i = 0; i < inputs_monto_factura.length; i++) {
        let common_name_prefix = $(inputs_monto_factura[i]).attr('id').substring(0, ($(inputs_monto_factura[i]).attr('id').length - "monto".length));
        const regex = /Detalles_([a-z]+[0-9]+)_/gm;
        let m = regex.exec(common_name_prefix);

        if (m !== null) {
            let fila = {};
            let monto = $('#' + common_name_prefix + 'monto').val();
            monto = monto === "" ? 0.0 : parseFloat(monto);
            let cotizacion_factura = $('#' + common_name_prefix + "$_campo_factura").select2('data')[0]['valor_moneda'];
            cotizacion_factura = cotizacion_factura === null ? 1.0 : parseFloat(cotizacion_factura); // Se envia 1 en caso, porque siempre se multiplica con el monto.
            let cotizacion_a_usar = $('#' + common_name_prefix + "cotizacion").val();
            cotizacion_a_usar = cotizacion_a_usar === "" ? 0 : parseFloat(cotizacion_a_usar); // Se envia 0 en caso, indicando que no se especifico nada en el campo.
            let recibo_cotizacion = $('#recibo-valor_moneda-disp').val();
            recibo_cotizacion = recibo_cotizacion === "" ? 0 : parseFloat(recibo_cotizacion);

            fila['monto'] = monto;
            fila['cotizacion_factura'] = cotizacion_factura;
            fila['cotizacion_a_usar'] = cotizacion_a_usar;
            fila['id_cuenta'] = '';
            fila['id_factura'] = $('#' + common_name_prefix + "$_campo_factura").val();
            fila['id_input'] = common_name_prefix + 'monto';
            fila['recibo_moneda_id'] = $('#recibo-moneda_id').val();
            fila['recibo_valor_moneda'] = $('#recibo-valor_moneda-disp').val();
            fila['recibo_fecha'] = $('#recibo-fecha-disp').val();
            simulacion_post.push(fila);
        }
    }
    return simulacion_post;
}

function getDetallesContracta() {
    let inputs_monto_factura = document.getElementsByClassName('sumable-contracta'), i;
    let simulacion_post = [];
    for (i = 0; i < inputs_monto_factura.length; i++) {
        let common_name_prefix = $(inputs_monto_factura[i]).attr('id').substring(0, ($(inputs_monto_factura[i]).attr('id').length - "monto".length));
        const regex = /Detallescontracuenta_([a-z]+[0-9]+)_/gm;
        let m = regex.exec(common_name_prefix);
        if (m !== null) {
            let fila = {};
            fila['monto'] = $('#' + common_name_prefix + 'monto').val();
            fila['cotizacion_factura'] = "";
            fila['cotizacion_a_usar'] = "";
            fila['id_cuenta'] = $('#' + common_name_prefix + 'plan_cuenta_id').val();
            fila['id_factura'] = '';
            fila['id_input'] = common_name_prefix + 'monto';
            fila['recibo_moneda_id'] = $('#recibo-moneda_id').val();
            fila['recibo_valor_moneda'] = $('#recibo-valor_moneda-disp').val();
            fila['recibo_fecha'] = $('#recibo-fecha-disp').val();
            simulacion_post.push(fila);
        }
    }
    return simulacion_post;
}

$(document).on('change', 'input[id^="Detalles_new"][id$="_valorizado"]:not([id="Detalles_new_valorizado"])', function() {
    if (valorizado_det_changedByAnother) return;
    
    let moneda = $('#recibo-moneda_id'),
        parent = $(this).parent().parent().parent(),
        valorizado = $(this).val(),
        cotizacion = parent.find('input[id^="Detalles_new"][id$="_cotizacion"]:not([id="Detalles_new_cotizacion"])').val(),
        monto;
    
    if (moneda.val() === '$moneda_base') {
        if (!cotizacion)
            cotizacion = 1;
        
        monto = valorizado / cotizacion;
        monto = Math.round(monto);
        parent.find('input[id^="Detalles_new"][id$="_monto"]:not([id="Detalles_new_monto"])').val(monto).trigger('change');
    } else {
        parent.find('input[id^="Detalles_new"][id$="_monto"]:not([id="Detalles_new_monto"])').val(valorizado).trigger('change');
    }
}).on('focusin', 'input[id^="Detalles_new"][id$="_valorizado"]:not([id="Detalles_new_valorizado"])', function() {
    valorizado_det_changedByAnother = false;
});

$(document).on('focusin', 'input[id^="Detalles_new"][id$="_cotizacion"]:not([id="Detalles_new_cotizacion"])', function() {
    cot_detalle_manual_edit = true;
}).on('focusout change', 'input[id^="Detalles_new"][id$="_cotizacion"]:not([id="Detalles_new_cotizacion"])', function(evt) {
    if (!cot_detalle_manual_edit) return false;
    
    cot_detalle_manual_edit = false;
    
    let pref = $(this).attr('id'),
        val_id = "#"+pref.replace(/cotizacion/g, 'valorizado'),
        mon_id = "#"+pref.replace(/cotizacion/g, 'monto'),
        fac_id = "#"+pref.replace(/cotizacion/g, 'factura_'+'$operacion'+'_id');
    let valoriz = $(val_id).val(),
        cotizac = $(this).val(),
        fac_moned_id = $(fac_id).select2('data')[0]['moneda_id'],
        rec_moned_id = $("#recibo-moneda_id").val(),
        monto;
    
    if (!cotizac) cotizac = 1;
    
    if (!valoriz) return false;
    if (rec_moned_id === fac_moned_id) {
        $(mon_id).val(valoriz).trigger('change');
        return false;
    }

    if (fac_moned_id === '1') {
        monto = valoriz * cotizac;
    } else {
        monto = valoriz / cotizac;
    }
    $(mon_id).val(parseFloat(monto).toFixed((rec_moned_id === '1' ? 0 : 2))).trigger('change');
});

function readyToSim() {
    let ready  = $('#recibo-entidad_id').val();
    
    ready = (ready && ($('.sumable-factura:not([id="Detalles_new_monto"])').length > 0 && $('.sumable-factura:not([id="Detalles_new_monto"])').val() !== ''));
    ready = (ready && ($('.sumable-contracta:not([id="Detallescontracuenta_new_monto"])').length > 0 && $('.sumable-contracta:not([id="Detallescontracuenta_new_monto"])').val() !== ''));
    
    return ready;
}

function simular() { console.log('\tentra');
    // para calcular valorizado, que es meramente a nivel de vista.
    // Es necesario que esto este aqui primero, para que a pesar de que no reuna las condiciones para simular, el calculo de valorizado debe ocurrir si es necesario.
    valorizado_det_changedByAnother = true;
    $('input[id^="Detalles_new"][id$="_factura_' + "$operacion" + '_id"]:not([id="Detalles_new_factura_' + "$operacion" + '_id"])').each(function(e, i) {
        valorizar($(this));
    });

    resumen();
    
    if (!readyToSim()) return;
    
        try {
        $.ajax({
            url: $url_simularAsiento,
            type: 'post',
            data: {
                detalles_factura: getDetalles(),
                detalles_contracta: getDetallesContracta(),
            },
            success: function (data) {
                if (data === '') $.pjax.reload({container: "#flash_message_id", async: false});
                $.pjax.reload({container: "#asiento-simulator-div", async: false});
            }
        });
    } catch (err) {
    }
}

function showHideValorMoneda() {
    if ($('#recibo-moneda_id').val() === '1') {
        //$('#recibo-valor_moneda-disp').attr('readonly', true);
        $('#recibo-valor_moneda-disp').val('');
    } else {
        //$('#recibo-valor_moneda-disp').attr('readonly', false);
    }
}

function calcularTotalRecibo(triggerByItself = false) {
    let inputs_monto_factura = document.getElementsByClassName('sumable-factura'), i;
    let total_recibo = 0.0;
    for (i = 0; i < inputs_monto_factura.length; i++) {
        const regex = /Detalles_new([0-9]+)/gm;
        let m = regex.exec($(inputs_monto_factura[i]).attr('id'));
        if (m !== null && $('#' + m[0] + "_$_campo_factura").val() !== "" && m[1] !== null) {
            try {
                let valor_moneda = $('#' + m[0] + "_$_campo_factura").select2('data')[0]['valor_moneda'];
                let factura_moneda_id = $('#' + m[0] + "_$_campo_factura").select2('data')[0]['moneda_id'];
                let recibo_moneda_id = $('#recibo-moneda_id').val();
                let valor_moneda_usar = $('#' + m[0] + "_cotizacion").val();

                if (recibo_moneda_id !== factura_moneda_id) {
                    if (factura_moneda_id === "$moneda_base_id")
                        total_recibo += $(inputs_monto_factura[i]).val() / (valor_moneda_usar === null ? 1.00 : parseFloat(valor_moneda_usar));
                    else
                        total_recibo += $(inputs_monto_factura[i]).val() * (valor_moneda === null ? 1.00 : parseFloat(valor_moneda));
                }
                else
                    total_recibo += $(inputs_monto_factura[i]).val();
            } catch (err) {

            }
        }
    }
    let campo_moneda_id = $('#recibo-moneda_id');
    let isMonedaSelected = campo_moneda_id.select2('data').length;
    if (isMonedaSelected && campo_moneda_id.select2('data')[0]['tiene_decimales'] === "no") {
        total_recibo = Math.round(total_recibo);
    }
    
    //Calcula y pone el total del recibo
    let campoTotal = $('#recibo-total-disp'),
        campoExtra = $('#recibo-monto_extra-disp'); 
    //Pone total sii esta vacio o es cero.
    // 24-julio-19: Jose solicita que obligue a los usuarios a escribir manualmente como un mecanismo de control.
    // if ((campoTotal.val() === '' || campoTotal.val() === 0) && (!triggerByItself))
    //     campoTotal.val(total_recibo).trigger('change');
}

function getSumaryDetalles() {
    try {
        let total_recibo = 0;
        let cotizacionRecibo = $('#recibo-valor_moneda').val();
        let moneda_field = $('#recibo-moneda_id');
        let moneda_recibo = moneda_field.val();
        
        $("input[id*='Detalles_new'][id$='_monto']:not([id='Detalles_new_monto'])").each(function(i, e) {
            let cotizacionFactura = $(this).parent().parent().parent().find('input[id$="valor_moneda"]'),  // cotizacion que uso la factura en su creacion
                cotizacionPorFact = $(this).parent().parent().parent().find('input[id$="cotizacion"]'),  // cotizacion que se va a usar en este recibo por la factura
                moneda_factura = $(this).parent().parent().parent().find('input[id*="factura_"][id$="_id"]').select2('data')[0]['moneda_id'],
                factor = 1;
            
            if (moneda_field.val() === '1') {            // recibo en guaranies
                if (cotizacionFactura.val() !== '') {    // factura en moneda extranjera
                    if (cotizacionPorFact.val() === '')  // cotizacion propia en la fila de factura
                        factor = cotizacionFactura.val();
                    else
                        factor = cotizacionPorFact.val();
                }
            } else {
                if (cotizacionFactura.val() !== '') {
                    factor = cotizacionFactura.val();
                } else {
                    factor = cotizacionRecibo;
                }
            }
            
            if (moneda_recibo === '1') {
                if (moneda_factura === '1') {
                    total_recibo += e.value * factor;
                } else if (moneda_factura !== '1') {
                    total_recibo += e.value * factor;
                }
            } else if (moneda_recibo !== '1') {
                if (moneda_factura === '1') {
                    total_recibo += e.value / factor;
                } else if (moneda_factura !== '1') {
                    total_recibo += e.value;
                }
            }
        });
    
        return (moneda_field.val() === '1' ? Math.round(total_recibo) : total_recibo);
    } catch (err) {
        return 0;
    }
}

function getSumaryContracuentas() {  
    try {
        let suma = 0;
        $("input[id*='Detallescontracuenta_new'][id$='_monto']:not([id='Detallescontracuenta_new_monto'])").each(function(i, e) {
            suma += e.value;
        });
    
        return suma;
    } catch (err) {
        return 0;
    }
}

function calcularMontoExtra() {
    let campoTotal = $('#recibo-total-disp'),
        total_recibo = getSumaryDetalles(),
        campoExtra = $('#recibo-monto_extra-disp');
    
    campoExtra.val(campoTotal.val() - total_recibo).trigger('change');
}

function resumen() {
    let sumaryDet = getSumaryDetalles(),
        sumaryCta = getSumaryContracuentas();
    $('#total-recibo-detalle-disp').val(sumaryDet).trigger('change');
    $('#total-cuenta-disp').val(sumaryCta).trigger('change');
    $('#error-redondeo-disp').val(sumaryDet - sumaryCta).trigger('change');    
}
/** <---------------------- JS_FUNCTIONS ---------------------->**/


/** <---------------------- JS_DOCONREADYS ---------------------->**/
$(document).ready(function () {
    setTotalInfo();
    cargarEntidades();

    // $('#recibo-factura_tipo').parent().parent()[0].style.display = 'none';

    if (("$actionId") === 'update') {
        $('#recibo-moneda_id').prop('disabled', true);
    } else if ("$actionId" === 'create') {
        let element = $('#recibo-fecha').parent()[0].getElementsByClassName('glyphicon-calendar');
        $(element).click();
    }


    // Estos aplicadores solo son para los campos prerenderizados cuando es update.
    applyCssToSelectors($data_select2_facturas, 'no-selector-factura');
    applyCssToSelectors($planCuentaLista, 'no-selector-contracta');
    applyEventsOnChangeFacturaId();
    applyCssToContractaSumableInputs();
    setEventsOnFacturaMonto('no-event-factura');
    setEventsOnContractaMonto('no-event-contracta');
    setEventsOnDeleteRowButtonFactura('no-event-btn-borrar-factura');
    setEventsOnDeleteRowButtonContracta('no-event-btn-borrar-contracta');
    showHideValorMoneda();
    campoTotalSetDigits();
    calcularTotalRecibo();

    $.pjax.reload({container: "#flash_message_id", async: false});
    simular();
});
$(document).on('click', '.tiene_modal', (function (evt) {
    let boton = $(this);
    let title = boton.attr('title');
    let boton_id = boton.attr('id');
    const regex = /Detalles_new([0-9]+)_/gm;
    let m = regex.exec(boton_id);
    let idPrefix = m[0]; // Obtengo Detalles_new<nro>_
    let factura_id = $('#' + idPrefix + "$_campo_factura").val();
    let url = boton.data('url') + '&operacion=' + "$operacion" + '&factura_id=' + factura_id;
    
    if ("{$errors}".length) {
        $("#modal").modal("hide");
        $("modal-body").html("");
        krajeeDialog.alert("{$errors}");
        evt.preventDefault();
        return false;
    }
    
    $.get(
        url,
        function (data) {
            let modal = $(boton.data('target'));
            $('.modal-body', modal).html("");
            if (data !== "") {
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                // if (title)
                $('.modal-title', modal).html(title);
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
                $("#modal").modal("hide");
                $("modal-body").html("");
            }
        }
    );
}));
/** <---------------------- JS_DOCONREADYS ---------------------->**/


/** <---------------------- JS_EVENTS ---------------------->**/
// Obtiene y muestra la razon social del ruc introducido.
$('#recibo-ruc').on('change', function () {
    var url_getRazonSocialByRuc = $url_getRazonSocialByRuc;
    $.ajax({
        url: url_getRazonSocialByRuc,
        type: 'post',
        data: {
            ruc: $(this).val(),
        },
        success: function (data) {
            $('#recibo-razon_social').val(data);
            if (data === '') $.pjax.reload({container: "#flash_message_id", async: false});
        }
    });
});

// Si cambia fecha, hay que cambiar cotizacion
// Si cambia cotizacion, hay que cambiar el formato de los campos numericos: mostrar decimales o no.
// Si cambia cotizacion, cambian los totales valorizados.
$('#recibo-moneda_id, #recibo-fecha-disp').on('change', function () {
    showHideValorMoneda();
    $.ajax({
        url: $url_getCotizacion,
        type: 'get',
        data: {
            fecha: $('#recibo-fecha').val(),
            moneda_id: $('#recibo-moneda_id').val(),
        },
        success: function (data) {
            $('#recibo-valor_moneda-disp').val(data.result).trigger('change');
            if (data.error) {
                krajeeDialog.alert(data.error);
            }
            // else {
            //     simular();
            // }
        }
    });
    $('.selector-factura').trigger('change'); // para que ponga la cotizacion por factura.
    campoTotalSetDigits(); // en este orden se debe colocar: primero seteador de inputmask, luego calcular el total del recibo.
    calcularTotalRecibo();
    applyCssToContractaSumableInputs();
});

function cargarEntidades() {
    // Primero validamos que la fecha este escrita correctamente. Esto lo hacemos para que cuando cambie del widget,
    // no haya problema de formato.

    let recibo_fecha = $('#recibo-fecha');

    let fecha = recibo_fecha.val().replace(new RegExp('-', 'g'), '/'); // reemplazar '-' por '/'
    // fecha = ((fecha.split('/')).reverse()).join('/'); // invertir dia y anho de lugares. Utilizar solamente si se usa MaskedInput.

    let date = new Date(fecha); // crear objeto Date.

    if (!isNaN(date)) { // isNaN() verifica si se creo bien el objeto.

        let url = $url_getEntidadesConFacturaCredito +'&fecha_hasta=' + recibo_fecha.val();

        $.ajax({
            url: url,
            type: 'get',
            data: {
                entidad_id: entidad_asociada_id, // id de entidad del recibo en modificacion, guardado en POS_HEAD, para que aparezca la factura asociada.
            },
            success: function (data) {
                let select2 = $('#recibo-entidad_id');
                let entidad_id = select2.val();

                select2.select2("destroy");
                select2.html("<option><option>");
                select2.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: [],
                });

                let arr = [];

                data['results'].forEach(function (e, i) {
                    arr.push(e);
                });

                select2.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: arr,
                });

                select2.val(entidad_id).trigger('change');
            }
        });
    }
}

// Cuando solamente cambia fecha, hay que actualizar lista de entidades,
// cuyas facturas credito tengan fecha de emision menor o igual a la fecha de recibo
$('#recibo-fecha').on('change', function () {
    cargarEntidades();

    $('.fila-facturas :input.selector-factura').each(function (i, e) {
        let fecha_emision = $(e).select2('data')[0]['fecha_emision'];
        // fecha_emision = ((fecha_emision.split('-')).reverse()).join('-'); // usar si fecha_emision es formato paraguayo

        // Eliminar facturas con fecha emision posterior a la fecha de recibo.
        if (fecha_emision > $('#recibo-fecha').val()) {
            let del_btn = $(this).parent().parent().parent()[0].getElementsByClassName('recibo-remove-detalle-button');
            krajeeDialog.confirm('Se removerán las facturas con fecha de emisión posterior a la fecha del recibo.',
                function (result) {
                    $(del_btn).click();
                }
            );
        }
    });
});

$('#recibo-valor_moneda-disp').on('change', function () {
    $('.cotizacion-detalle').each(function (i, e) { // campo cotizacion del detalle
        let m = (/Detalles_new([0-9]+)/gm).exec($(e).attr('id'));
        if (m !== null) {
            let id = '#' + $(e).attr('id').replace('cotizacion', "$_campo_factura");
            let element = document.getElementById('recibo-moneda_id');
            if ($(id).select2('data')[0]['moneda_id'] !== $(element).val()) {
                $(e).prop('readonly', false);
                setCotizacionDetalle(id);
            } else {
                $(e).prop('readonly', true);
            }
        }
    });
    simular();
});

$('#recibo-entidad_id').on('select2:selecting', function (e) {
    let filas = $('.fila-facturas :input[name$="[' + "$_campo_factura" + ']"]');
    let hay = false;
    filas.each(function () {
        if ($(this).val() !== "") {
            hay = true;
            return false; // es break
        }
    });
    if (hay) {
        krajeeDialog.alert('SE REMOVIERON TODAS LAS FACTURAS agregadas hasta ahora debido a que Ud. ha seleccionado una NUEVA ENTIDAD y las mismas NO CORRESPONDEN A LA ENTIDAD seleccionada.');
    }
    $('.recibo-remove-detalle-button').each(function () {
        const regex = /Detalles_new([0-9]+)_delete_button/gm;
        let m = regex.exec($(this).attr('id'));
        if (m !== null && m[0] !== null) {
            $(this).click();
        }
    });
}).on('change', function () {
    if ($(this).val() !== '') {
        let new_btn = $('#recibo-new-detalle-button');
        new_btn.focus(); // para que ocurra validacion.
        
        // Agregar una nueva fila de factura, solamente si no hay ninguna fila.
        // Cuando se llama a cargarEntidades(), dentro del mismo hay un trigger('change') al campo recibo-entidad_id
        // lo que hace que se lance este evento.
        // Si se llama a esa funcion desde document.ready y la action es update,
        // va a agregar 1 fila de factura extra aparte de los que ya estaban en la BD. Si el usuario es despistado y guarda,
        // se guardaria un detalle mas sin realmente desear.
        if ($('.fila-facturas').length === 0 && $(this).val() !== '' && "$actionId" === 'create') {
            new_btn.click();
        }
    }
});

function getRetencionesFactura() {
    $.ajax({
        type: 'get',
        url: 'index.php?r=contabilidad/recibo/get-retenciones-factura',
        data: {},
        async:false,
        success: function(result) {
            return result;
        },
    });
}

// Poner el total de recibo en la primera cuenta, si solamente hay 1 cuenta.
$(document).on('change', '.plan-cuenta:not([id="Detallescontracuenta_new_plan_cuenta_id"])', function () {
    calcularTotalRecibo();
    let m = (/Detallescontracuenta_new([0-9])+_plan_cuenta_id/gm).exec($(this).attr('id'));
    if (m !== null && m[1] !== null) {
        let cuenta_selectores = document.getElementsByClassName('plan-cuenta');
        if (cuenta_selectores.length === 2 && $(cuenta_selectores[1]).attr('id') === $(this).attr('id')) {
            let id_campo_monto = $(this).attr('id').replace(/plan_cuenta_id/g, "monto");
            $('#' + id_campo_monto).val(getSumaryDetalles()).trigger('change');
        }
    }
});

$(document).on('change', '.cotizacion-detalle, .sumable-factura', function () { console.log('entra');
    calcularTotalRecibo();
    calcularMontoExtra();
    simular();
});

$(document).on('change', '#recibo-total-disp', function(evt) {
    calcularMontoExtra();
    
    //Actualizar monto de contracuenta sii hay solo 1
    // let ctaCtas = $("input[id*='Detallescontracuenta_new'][id$='_monto']:not([id='Detallescontracuenta_new_monto'])");
    // if (ctaCtas.length === 1) {
    //     ctaCtas = $(ctaCtas[0]);
    //     if (ctaCtas.val() !== $(this).val())
    //         ctaCtas.val($(this).val()).trigger('change');
    // }
}).on('focusin', '#recibo-total-disp', function() {
    if ($(this).val() === '')
        $('#set-total-equal-details').click();
});

function setTotalInfo() {
    applyPopOver($('span.total-info'), 'Información', "Puede incluir el monto extra abonado al cancelar una o varias facturas. Dicho monto no participa en el asiento del recibo.", {'theme-color': 'info-l', 'placement': 'right'});
}

$(document).on('click', '#set-total-equal-details', function(evt) {
    $('#recibo-total-disp').val(getSumaryDetalles()).trigger('change');
});
/** <---------------------- JS_EVENTS ---------------------->**/
JS;
$this->registerJs($scripts);
?>