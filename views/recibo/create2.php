<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Recibo */
/* @var $detalles backend\modules\contabilidad\models\ReciboDetalle[] */

$this->title = 'Registrar Nuevo Recibo de ' . ucwords(Yii::$app->getRequest()->getQueryParam('operacion'));
$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recibo-create">

    <?= $this->render('_form2', [
        'model' => $model,
        'detalles' => $detalles,
    ]) ?>

</div>
