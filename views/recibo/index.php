<?php

use backend\models\Moneda;
use backend\modules\contabilidad\models\Entidad;
use backend\modules\contabilidad\models\Recibo;
use backend\modules\contabilidad\models\ReciboDetalle;
use common\helpers\PermisosHelpers;
use common\models\ParametroSistema;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ReciboSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recibos';
$this->params['breadcrumbs'][] = $this->title;
$operacion = Yii::$app->getRequest()->getQueryParam('operacion');
?>
<div class="recibo-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if (PermisosHelpers::getAcceso('contabilidad-recibo-create')) {
            echo $operacion == 'compra' ?
                Html::a('Registrar Nuevo Recibo de COMPRA', ['create', 'operacion' => 'compra'], ['class' => 'btn btn-success']) :
                Html::a('Registrar Nuevo Recibo de VENTA', ['create', 'operacion' => 'venta'], ['class' => 'btn btn-success']);
        } ?>
    </p>

    <?php try {
        $template = '';
        foreach (['view', 'update', 'delete', 'bloquear', 'desbloquear'] as $_v)
            if (PermisosHelpers::getAcceso("contabilidad-recibo-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        #filtro entidades
        $entidades = Entidad::find()->alias('entidad')
            ->innerJoin('cont_recibo recibo', 'recibo.entidad_id = entidad.id')
            ->select([
                'id' => "entidad.id",
                'text' => "CONCAT(entidad.ruc, ' - ', entidad.razon_social)",
            ])->orderBy(['entidad.ruc' => SORT_ASC])
            ->asArray()->all();
        $entidades = ArrayHelper::map($entidades, 'id', 'text');

        #filtro facturas
        $facturas = ReciboDetalle::find()->alias('recibo')
            ->innerJoin("cont_factura_{$operacion} as factura", "factura.id = recibo.factura_{$operacion}_id")
            ->innerJoin('cont_entidad entidad', 'entidad.id = factura.entidad_id')
            ->select([
                'id' => 'factura.id',
                'text' => ($operacion == 'compra') ? "CONCAT(nro_factura, ' - ', entidad.ruc, ' ', entidad.razon_social)"
                    : "CONCAT(prefijo, '-', nro_factura, ' - ', entidad.ruc, ' ', entidad.razon_social)"
            ])->asArray()->all();
        $facturas = ArrayHelper::map($facturas, 'id', 'text');

        $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'value' => 'id',
                'contentOptions' => ['style' => 'width:60px; text-align:center;'],
            ],

            [
                'attribute' => 'numero',
                'value' => 'numero',
                'contentOptions' => ['style' => 'width:100px; text-align:left;'],
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'fecha',
                'format' => ['date', 'php:d-m-Y'],
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'attribute' => 'fecha',
                    'language' => 'es',
                    'pickerButton' => false,
//                        'options' => [
//                            'style' => 'width:90px',
//                        ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'weekStart' => '0',
                    ]
                ],
                'contentOptions' => ['style' => 'width:160px; text-align:center;'],
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'entidad_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return "{$model->entidad->razon_social} <strong>({$model->entidad->ruc})</strong>";
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'attribute' => 'entidad_id',
                    'data' => $entidades,
                    'pluginOptions' => [
                        'placeholder' => "Todos",
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'nro_factura',
                'format' => 'raw',
                'value' => function ($model) use ($operacion) {
                    /** @var $model Recibo */
                    $return = "<span class='text-danger small'><strong>***</strong></span>";
                    if (sizeof($model->reciboDetalles) == 1) {
                        $factura = $model->reciboDetalles[0]->$operacion;
                        $url = Url::to(["$operacion/view", 'id' => $factura->id]);
                        $return = ($operacion == 'compra') ?
                            $model->reciboDetalles[0]->$operacion->nro_factura :
                            $model->reciboDetalles[0]->$operacion->getNroFacturaCompleto();
                        $return = Html::a($return, $url, ['target' => '_blank']);
                    }

                    return $return;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'attribute' => 'nro_factura',
                    'data' => $facturas,
                    'pluginOptions' => [
                        'placeholder' => "Todos",
                        'allowClear' => true,
                    ],
                ],
                'contentOptions' => ['style' => 'width:250px; text-align:left;'],
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'moneda_id',
                'label' => 'Moneda',
                'value' => "monedaNombre",
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'attribute' => 'moneda_id',
                    'data' => Moneda::getMonedas(),
                    'pluginOptions' => [
                        'placeholder' => "All",
                        'allowClear' => true,
                    ],
                ],
                'contentOptions' => ['style' => 'width:150px; text-align:center;'],
            ],
            [
                'class' => DataColumn::class,
                'attribute' => 'total',
                'value' => function ($model) {
                    /** @var $model Recibo */
                    return Yii::$app->formatter->asDecimal($model->total, ($model->moneda_id == ParametroSistema::getMonedaBaseId() ? 0 : 2));
                },
                'contentOptions' => ['style' => 'width:150px; text-align:right;'],
            ],
            [
                'class' => DataColumn::class,
                'label' => 'Extra',
                'attribute' => 'monto_extra',
                'value' => function ($model) {
                    /** @var $model Recibo */
                    return Yii::$app->formatter->asDecimal($model->monto_extra, (($model->moneda_id == ParametroSistema::getMonedaBaseId() || round($model->monto_extra) == 0) ? 0 : 2));
                },
                'contentOptions' => ['style' => 'width:150px; text-align:right;'],
            ],
            [
                'class' => DataColumn::class,
                'label' => 'Con Extra?',
                'attribute' => 'con_monto_extra',
                'format' => 'raw',
                'value' => function ($model) {
                    $class = (round($model->monto_extra) != 0) ? 'fa fa-check text-warning' : 'fa fa-times text-success';
                    return "<span class='$class'></span>";
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'model' => $searchModel,
                    'attribute' => 'con_monto_extra',
                    'data' => ['>' => 'Si', '=' => "No"],
                    'pluginOptions' => [
                        'placeholder' => "Todos",
                        'allowClear' => true,
                        'width' => '300px;',
                    ],
                ],
                'contentOptions' => ['style' => 'width:300px !important; text-align:center;'],
            ],
            [
                'attribute' => 'asiento_id',
                'label' => "Asentado?",
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var Recibo $recibo */
                    $recibo = $model;
                    $type = ($recibo->asiento_id != '') ? "success" : "warning";
                    $content = ($recibo->asiento_id != '') ? "Si" : "No";
                    $btnVer = ($recibo->asiento_id != '') ? Html::a("<span class='label label-info'>Ver</span>", ['/contabilidad/asiento/view/', 'id' => $recibo->asiento_id], ['target' => '_blank']) : null;
                    return "<label style='text-align:center;' class='label label-{$type}'>$content</label>&nbsp;&nbsp;$btnVer";
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'asiento_id',
                    'data' => ['si' => "SI", 'no' => "NO"],
                    'pluginOptions' => [
                        'placeholder' => "Todos",
                        'allowClear' => true,
                        'width' => '100px',
                    ],
                ]),
                'contentOptions' => ['style' => 'text-align:center;'],
            ],
        ];
        $actionColumns = [[
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'update' => function ($url, $model, $index) {
                    return $model->bloqueado == 'no' ? Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        $url,
                        [
                            'class' => 'btn_edit_recibo',
                            'id' => $index,
                            'title' => Yii::t('app', 'Editar'),
                        ]
                    ) : '';
                },
                'view' => function ($url, $model, $index) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        $url,
                        [
                            'class' => 'btn_view_recibo',
                            'id' => $index,
                            'title' => Yii::t('app', 'Ver'),
                        ]
                    );
                },
                'delete' => function ($url, $model, $index) {
                    return $model->bloqueado == 'no' ? Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        $url,
                        [
                            'class' => 'tn_delete_recibo',
                            'data' => [
                                'method' => 'post',
                                'confirm' => 'Está seguro de eliminar este recibo ?',
                            ],
                            'id' => $index,
                            'title' => Yii::t('app', 'Eliminar'),
                        ]
                    ) : '';
                },
                'bloquear' => function ($url, $model, $key) {
                    /** @var Recibo $model */
                    $url = ($model->bloqueado == 'no') ?
                        Html::a('<span>Bloquear</span>', $url,
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'title' => Yii::t('app', 'Bloquear'),
                                'data-confirm' => 'Está seguro que desea bloquear el recibo?'
                            ]) : '';
                    return $url;
                },
                'desbloquear' => function ($url, $model, $key) {
                    /** @var Recibo $model */
                    $url = ($model->bloqueado == 'si') ?
                        Html::a('<span>Desbloquear</span>', $url,
                            [
                                'class' => 'btn btn-success btn-xs',
                                'title' => Yii::t('app', 'Desbloquear'),
                                'data-confirm' => 'Está seguro que desea desbloquear el recibo?'
                            ]) : '';
                    return $url;
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'update') {
                    // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                    $operacion = $model instanceof Recibo && sizeof($model->reciboDetalles) > 0 && $model->reciboDetalles[0]->factura_compra_id ? 'compra' : 'venta';
                    $url = Url::to(['recibo/update', 'id' => $model->id, 'operacion' => $operacion]);
                    return $url;
                }
                if ($action === 'view') {
                    // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                    $operacion = $model instanceof Recibo && sizeof($model->reciboDetalles) > 0 && $model->reciboDetalles[0]->factura_compra_id ? 'compra' : 'venta';
                    $url = Url::to(['recibo/view', 'id' => $model->id, 'operacion' => $operacion]);
                    return $url;
                }
                if ($action === 'delete') {
                    // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                    $operacion = $model instanceof Recibo && sizeof($model->reciboDetalles) > 0 && $model->reciboDetalles[0]->factura_compra_id ? 'compra' : 'venta';
                    $url = Url::to(['recibo/delete', 'id' => $model->id, 'operacion' => $operacion]);
                    return $url;
                }
                if ($action === 'bloquear') {
                    // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                    $operacion = $model instanceof Recibo && sizeof($model->reciboDetalles) > 0 && $model->reciboDetalles[0]->factura_compra_id ? 'compra' : 'venta';
                    $url = Url::to(['recibo/bloquear', 'id' => $model->id, 'operacion' => $operacion]);
                    return $url;
                }
                if ($action === 'desbloquear') {
                    // TODO: cambiar agregando una columna distintiva de compra/venta en Recibo.
                    $operacion = $model instanceof Recibo && sizeof($model->reciboDetalles) > 0 && $model->reciboDetalles[0]->factura_compra_id ? 'compra' : 'venta';
                    $url = Url::to(['recibo/desbloquear', 'id' => $model->id, 'operacion' => $operacion]);
                    return $url;
                }
                return '';
            },
            'template' => $template,
        ]];

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function ($model) {
                /** @var $model Recibo */
                if (round($model->monto_extra, 2) != 0.00) {
                    return ['class' => 'danger'];
                }
            },
            'columns' => array_merge($columns, $actionColumns),
        ]);
    } catch (Exception $e) {
        throw $e;
    } ?>
</div>
