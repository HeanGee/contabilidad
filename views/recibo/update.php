<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Recibo */
/* @var $detallescontracta \backend\modules\contabilidad\models\ReciboDetalleContracuentas[] */
/* @var $detalles \backend\modules\contabilidad\models\ReciboDetalle[] */

$actionId = Yii::$app->controller->action->id;
$operacion = Yii::$app->request->getQueryParam('operacion');
$this->title = 'Modificar Recibo: ' . $model->numero;
$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' => ['index', 'operacion' => $operacion]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recibo-update">

    <?= $this->render('_form', [
        'model' => $model,
        'detalles' => $detalles,
        'detallescontracta' => $detallescontracta
    ]) ?>

</div>
