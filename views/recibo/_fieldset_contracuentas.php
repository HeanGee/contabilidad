<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 10/07/2018
 * Time: 8:28
 */

use backend\modules\contabilidad\controllers\ReciboDetalleContracuentaTemp;
use backend\modules\contabilidad\models\PlanCuenta;
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $model backend\modules\contabilidad\models\Recibo */
/* @var $form yii\widgets\ActiveForm */
/* @var $detallescontracta \backend\modules\contabilidad\models\ReciboDetalleContracuentas[] */

?>

<fieldset>
    <legend>&nbsp;Cuentas
        <?php
        // new detalle button
        echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
            'id' => 'recibo-new-detalle-contracuenta-button',
            'class' => 'pull-left btn btn-success'
        ])
        ?>
    </legend>
    <?php

    // parcel table
    $detalle_contracta = new ReciboDetalleContracuentaTemp();
    //        $detalle->loadDefaultValues();
    echo '<table id="recibo-detalles-contracuenta" class="table table-condensed table-bordered">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Cuenta</th>';
    echo '<th style="text-align: center;">' . "Cuentas (por medio de pago)" . '</th>';
    echo '<th style="text-align: center;">Acciones</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    // existing detalles fields
    $campo_plan_cta_id = 'plan_cuenta_id';
    $key_ctracta = 0;
    foreach ($detallescontracta as $key => $_reciboDetalle) {
        $key_ctracta = $_reciboDetalle->id != null ? $_reciboDetalle->id - 1 : $key;
        echo '<tr>';
        echo $this->render('detalle/_form-recibo-detalle-contracuentas', [
            'key' => 'new' . ($key_ctracta + 1),
            'form' => $form,
            'detalle' => $_reciboDetalle,
            'campo' => $campo_plan_cta_id,
            'class' => 'form-control plan-cuenta no-selector-contracta',
            'class_monto' => 'form-control sumable-contracta no-event-contracta',
            'class_btn_borrar' => ' no-event-btn-borrar-contracta',
        ]);
        echo '</tr>';
        $key_ctracta++;
    }

    // new detalles fields
    echo '<tr id="recibo-new-detalle-contracuenta-block" style="display: none;">';
    echo $this->render('detalle/_form-recibo-detalle-contracuentas', [
        'key' => 'new',
        'form' => $form,
        'detalle' => $detalle_contracta,
        'campo' => $campo_plan_cta_id,
        'class' => 'form-control plan-cuenta ',
        'class_monto' => 'form-control sumable-contracta',
        'class_btn_borrar' => '',
    ]);
    echo '</tr>';
    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);
    $planCuentaLista = PlanCuenta::getCuentaLista(true);
    ?>

    <?php ob_start(); // output buffer the javascript to register later ?>
    <script>

        // add detalle button
        var detalle_contracuenta_k = <?php echo isset($key_ctracta) ? $key_ctracta : 0; ?>;
        let data_contracuenta = <?php echo $planCuentaLista == null ? Json::encode([]) : Json::encode($planCuentaLista); ?>;
        let campo_id_contracuenta = "<?php echo $campo_plan_cta_id; ?>";
        $('#recibo-new-detalle-contracuenta-button').on('click', function () {
            detalle_contracuenta_k += 1;
            $('#recibo-detalles-contracuenta').find('tbody')
                .append('<tr>' + $('#recibo-new-detalle-contracuenta-block').html().replace(/new/g, 'new' + detalle_contracuenta_k) + '</tr>');

            $('#Detallescontracuenta_new' + detalle_contracuenta_k + '_' + campo_id_contracuenta).select2({
                theme: 'krajee',
                placeholder: '',
                language: 'en',
                width: '100%',
                data: data_contracuenta,
            });

            generarInputNumber($('#Detallescontracuenta_new' + detalle_contracuenta_k + '_monto'), 'sumable-contracta', $('#recibo-moneda_id'));

            onselectContracuenta(
                '#Detallescontracuenta_new' + detalle_contracuenta_k + '_' + campo_id_contracuenta,
                // '#Detallescontracuenta_new' + detalle_contracuenta_k + '_monto',
            );

            onchangeMontoContraCta(
                '#Detallescontracuenta_new' + detalle_contracuenta_k + '_monto');

            clickDeleteButtonContracta(
                '#Detallescontracuenta_new' + detalle_contracuenta_k + '_delete_button',
                '#Detallescontracuenta_new' + detalle_contracuenta_k + '_monto'
            );
        });

        <?php
        if (!Yii::$app->request->isPost && $model->isNewRecord)
            if (Yii::$app->controller->action->id == 'create')
                echo "$('#recibo-new-detalle-contracuenta-button').click();";
        ?>

    </script>
    <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>

</fieldset>