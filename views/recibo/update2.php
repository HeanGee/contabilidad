<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Recibo */
/* @var $detalles backend\modules\contabilidad\models\ReciboDetalle[] */

$this->title = 'Modificar Recibo: ' . $model->numero;
$this->params['breadcrumbs'][] = ['label' => 'Recibos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recibo-update">

    <?= $this->render('_form2', [
        'model' => $model,
        'detalles' => $detalles,
    ]) ?>

</div>
