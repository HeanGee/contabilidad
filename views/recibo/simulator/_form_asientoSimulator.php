<?php

use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\ValorHelpers;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */

$empresa_id = Yii::$app->session->get('core_empresa_actual');
$periodo_id = Yii::$app->session->get('core_empresa_actual_pc');

$nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";  # el sufijo debe/haber estan invertidos con respecto a la ganancia/perdida. Si la diferencia de cambio resulta en ganancia, va en el haber y viceversa.
$parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);
$cuentaDebe = PlanCuenta::findOne(['id' => $parmsys_debe->valor]);

$nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";  # el sufijo debe/haber estan invertidos con respecto a la ganancia/perdida. Si la diferencia de cambio resulta en ganancia, va en el haber y viceversa.
$parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);
$cuentaHaber = PlanCuenta::findOne(['id' => $parmsys_haber->valor]);

$nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_error_redondeo_ganancia";
$ctaErrRedondeoGanancia = PlanCuenta::findOne(['id' => ParametroSistema::findOne(['nombre' => $nombre])->valor]);

$nombre = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cuenta_error_redondeo_perdida";
$ctaErrRedondeoPerdida = PlanCuenta::findOne(['id' => ParametroSistema::findOne(['nombre' => $nombre])->valor]);
?>

<?php Pjax::begin(['id' => 'asiento-simulator-div']);

$skey1 = 'cont_recibo_detalle_sim_data';
$skey2 = 'cont_recibo_detalle_contracuenta_sim_data';
$skey3 = 'cont_recibo_detalle_diff_costo_sim_data';
$skey4 = 'cont_recibo_detalle_retenciones';
$skey5 = 'cont_recibo_detalle_retenciones_renta';
$sesion = Yii::$app->session;

$detalles_factura = $sesion->has($skey1) ? $sesion->get($skey1) : [];
$detalles_contracta = $sesion->has($skey2) ? $sesion->get($skey2) : [];
$detalles_difercosto = $sesion->has($skey3) ? $sesion->get($skey3) : [];
$detalles_retenciones = $sesion->has($skey4) ? $sesion->get($skey4) : [];
$detalles_retenciones_renta = $sesion->has($skey5) ? $sesion->get($skey5) : [];

$hayDetalles = (empty($detalles_factura) && empty($detalles_contracta)) ? "no" : "si";
$total_debe = 0.0;
$total_haber = 0.0;
$operacion = Yii::$app->request->getQueryParam('operacion');

function agruparPartida($data)
{
    $newData = [];
    foreach ($data as $key => $item) {
        $index = itemExist($item, $newData);
        if ($index != -1) {
            $newData[$index]['monto'] += (float)$item['monto'];
        } else {
            $newData[] = $item;
        }
    }
    return $newData;
}

function itemExist($needle, $conjunto)
{
    foreach ($conjunto as $key => $item) {
        if ($needle['codigo_cuenta'] == $item['codigo_cuenta']) {
            return $key;
        }
    }

    return -1;
}

$detalles_factura = agruparPartida($detalles_factura);
$detalles_contracta = agruparPartida($detalles_contracta);
$diff = 0.0; // si es negativo, ubicarlo en debe ya que es perdida, sino ganancia.

// de todas las diferencias que puedan haber, en debe y haber, se obtiene en uno solo.
foreach ($detalles_difercosto as $key => $item) {
    if ($item['partida'] == 'debe')
        $diff -= round((float)$item['monto']);
    else
        $diff += round((float)$item['monto']);
}
?>

<div class="panel panel-primary" id="asientoSimulator" style="display: none;">
    <div class="panel-heading"><h3 class="panel-title">Simulación de asientos</h3></div>
    <div class="panel-body">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Código</th>
                <th>Cuenta</th>
                <th></th>
                <th style="text-align: right">Debe</th>
                <th style="text-align: right">Haber</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $html_options = ["style" => "text-align: right;", "class" => "borde-medio"];
            if ($hayDetalles === 'si') {

                $detalles = ($operacion == 'compra') ? $detalles_factura : $detalles_contracta;
                $detalles2 = ($operacion == 'compra') ? $detalles_contracta : $detalles_factura;

                foreach ($detalles as $detalle) {
                    $total_debe += round((float)$detalle['monto']);
                }

                foreach ($detalles2 as $detalle) {
                    $total_haber += round((float)$detalle['monto']);
                }

                if ($diff < 0.0) {
                    $total_debe += abs($diff);
                } else {
                    $total_haber += $diff;
                }

                if (sizeof($detalles_retenciones) && $operacion == 'venta') {
                    foreach ($detalles_retenciones as $field_id => $data) {
                        $total_debe += intval($data['monto']);
                    }
                }

                if (sizeof($detalles_retenciones_renta) && $operacion == 'venta') {
                    foreach ($detalles_retenciones_renta as $field_id => $data) {
                        $total_debe += intval($data['monto']);
                    }
                }

                if (sizeof($detalles_retenciones) && $operacion == 'compra') {
                    foreach ($detalles_retenciones as $field_id => $data) {
                        $total_haber += intval($data['monto']);
                    }
                }

                if (sizeof($detalles_retenciones_renta) && $operacion == 'compra') {
                    foreach ($detalles_retenciones_renta as $field_id => $data) {
                        $total_haber += intval($data['monto']);
                    }
                }

                $diferencia = $total_debe - $total_haber;

                foreach ($detalles as $detalle) {
                    echo Html::beginTag('tr');
                    echo Html::beginTag('td') . $detalle['codigo_cuenta'] . Html::endTag('td');
                    echo Html::beginTag('td') . $detalle['nombre_cuenta'] . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat((float)$detalle['monto'], 0) . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::endTag('tr');
                }
                if ($diff < 0.0 && $operacion == 'compra' || $diff > 0 && $operacion == 'venta') { // perdida
                    $empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                    $nombre_debe = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_haber";
                    $parmsys_debe = ParametroSistema::findOne(['nombre' => $nombre_debe]);

                    $planCuenta = PlanCuenta::findOne(['id' => $parmsys_debe->valor]); // TODO: parametro del modulo
                    echo Html::beginTag('tr');
                    echo Html::beginTag('td') . $planCuenta->cod_completo . Html::endTag('td');
                    echo Html::beginTag('td') . $planCuenta->nombre . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat(abs($diff), 0) . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::endTag('tr');
                }
                if ($diferencia < 0 && $operacion == 'compra' || $diferencia > 0 && $operacion == 'venta') {
                    $total_debe += abs($diferencia);
                    echo Html::beginTag('tr');
                    echo Html::beginTag('tr');
                    echo Html::beginTag('td') . $ctaErrRedondeoPerdida->cod_completo . Html::endTag('td');
                    echo Html::beginTag('td') . $ctaErrRedondeoPerdida->nombre . " <strong>(por error de redondeo)</strong>" . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat(abs($diferencia), 0) . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::endTag('tr');
                }
                if (sizeof($detalles_retenciones) && $operacion == 'venta') {
                    foreach ($detalles_retenciones as $field_id => $data) {
                        echo Html::beginTag('tr');
                        echo Html::beginTag('tr');
                        echo Html::beginTag('td') . $data['codigo_cuenta'] . Html::endTag('td');
                        echo Html::beginTag('td') . $data['nombre_cuenta'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat(abs($data['monto']), 0) . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::endTag('tr');
                    }
                }
                if (sizeof($detalles_retenciones_renta) && $operacion == 'venta') {
                    foreach ($detalles_retenciones_renta as $field_id => $data) {
                        echo Html::beginTag('tr');
                        echo Html::beginTag('tr');
                        echo Html::beginTag('td') . $data['codigo_cuenta'] . Html::endTag('td');
                        echo Html::beginTag('td') . $data['nombre_cuenta'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat(abs($data['monto']), 0) . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::endTag('tr');
                    }
                }

                foreach ($detalles2 as $detalle) {
                    echo Html::beginTag('tr');
                    echo Html::beginTag('tr');
                    echo Html::beginTag('td') . $detalle['codigo_cuenta'] . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td') . "a " . $detalle['nombre_cuenta'] . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat((float)$detalle['monto'], 0) . Html::endTag('td');
                    echo Html::endTag('tr');
                }
                if ($diff > 0.0 && $operacion == 'compra' || $diff < 0 && $operacion == 'venta') { // ganancia
                    $empresa_id = Yii::$app->session->get('core_empresa_actual');
                    $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
                    $nombre_haber = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-cta_resultado_diff_cambio_debe";
                    $parmsys_haber = ParametroSistema::findOne(['nombre' => $nombre_haber]);

                    $planCuenta = PlanCuenta::findOne(['id' => $parmsys_haber->valor]); // TODO: parametro del modulo
                    echo Html::beginTag('tr');
                    echo Html::beginTag('tr');
                    echo Html::beginTag('td') . $planCuenta->cod_completo . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td') . "a " . $planCuenta->nombre . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat(abs($diff), 0) . Html::endTag('td');
                    echo Html::endTag('tr');
                }
                if ($diferencia > 0 && $operacion == 'compra' || $diferencia < 0 && $operacion == 'venta') { // perdida
                    $total_haber += abs($diferencia);
                    echo Html::beginTag('tr');
                    echo Html::beginTag('tr');
                    echo Html::beginTag('td') . $ctaErrRedondeoGanancia->cod_completo . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td') . $ctaErrRedondeoGanancia->nombre . " <strong>(por error de redondeo)</strong>" . Html::endTag('td');
                    echo Html::beginTag('td') . Html::endTag('td');
                    echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat(abs($diferencia), 0) . Html::endTag('td');
                    echo Html::endTag('tr');
                }
                if (sizeof($detalles_retenciones) && $operacion == 'compra') {
                    foreach ($detalles_retenciones as $field_id => $data) {
                        echo Html::beginTag('tr');
                        echo Html::beginTag('tr');
                        echo Html::beginTag('td') . $data['codigo_cuenta'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td') . "a " . $data['nombre_cuenta'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat(abs($data['monto']), 0) . Html::endTag('td');
                        echo Html::endTag('tr');
                    }
                }
                if (sizeof($detalles_retenciones_renta) && $operacion == 'compra') {
                    foreach ($detalles_retenciones_renta as $field_id => $data) {
                        echo Html::beginTag('tr');
                        echo Html::beginTag('tr');
                        echo Html::beginTag('td') . $data['codigo_cuenta'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td') . "a " . $data['nombre_cuenta'] . Html::endTag('td');
                        echo Html::beginTag('td') . Html::endTag('td');
                        echo Html::beginTag('td', $html_options) . ValorHelpers::numberFormat(abs($data['monto']), 0) . Html::endTag('td');
                        echo Html::endTag('tr');
                    }
                }

                $text = "<strong><b><i><font size='5'>TOTAL:</font></i></b></strong>";
                $debe = '<strong><b><i><font size="5">' . ValorHelpers::numberFormatMonedaSensitive($total_debe, 0) . '</font></i></b></strong>';
                $haber = '<strong><b><i><font size="5">' . ValorHelpers::numberFormatMonedaSensitive($total_haber, 0) . '</font></i></b></strong>';

                echo '<tr style="border-top:2pt solid black" class="fila-total">';
                echo Html::beginTag('td') . Html::endTag('td');
                echo Html::beginTag('td') . Html::endTag('td');
                echo Html::beginTag('td') . $text . Html::endTag('td');
                echo Html::beginTag('td', $html_options) . $debe . Html::endTag('td');
                echo Html::beginTag('td', $html_options) . $haber . Html::endTag('td');
                echo Html::endTag('tr');
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<?php

$scripts = <<<JS
/** FUNCIONS **/
/** FUNCIONS **/

/** DOC ON READYS **/
// $(document).ready(function () {
    // Mostrar/ocultar dependiendo de si hay detalles con cuentas
    var hayDetalles = "$hayDetalles";
    if(hayDetalles === "si")
        $('#asientoSimulator').show();
// });
/** DOC ON READYS **/

/** EVENTS **/
/** EVENTS **/
JS;

$this->registerJs($scripts);

Pjax::end();

$CSS = <<<CSS
td.borde_medio, tr.borde_medio {
    border-right: 2px solid black;
    border-left: 2px solid black;
}
CSS;
$this->registerCss($CSS);
?>
