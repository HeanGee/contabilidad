<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 05/07/2018
 * Time: 14:16
 */

use backend\modules\contabilidad\models\PlantillaCompraventaRubro;
use backend\modules\contabilidad\models\Rubro;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $dataProvider ActiveDataProvider */
?>

<?php try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'id' => 'plantilla-compraventa-rubro-grid',
        'pjax' => false, // es necesario sii se va a usar en javascript, para referenciarle mediante id por ej.
//        'pjaxSettings' => [
//            'options' => [
//                'id' => 'empresa_obligaciones_pjax_grid'
//            ],
//            'loadingCssClass' => false,
//        ],
        'panel' => [
            'type' => 'primary',
            'heading' => 'Rubros',
            'after' => '<em>* Para confirmar los datos de los rubros se debe guardar la plantilla.</em>'
        ],
        'striped' => true,
        'hover' => true,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'headerOptions' => [
                    'class' => 'kartik-sheet-style',
                    'onchange' => '
                        $.ajax({
                            type: "POST",
                            url: "index.php?r=contabilidad/plantilla-compraventa/guardar-rubros-en-session&all=true",
                            data: {seleccionados: $("#plantilla-compraventa-rubro-grid").yiiGridView("getSelectedRows")},
                            success: function (data) {
                            }
                        });'
                ],
                'checkboxOptions' => function ($model) {
                    $skey = 'cont_plantilla_en_edicion';
                    $checked = false;
                    if (Yii::$app->session->has($skey)) {
                        $plantilla_rubros = PlantillaCompraventaRubro::findAll(['plantilla_compraventa_id' => \Yii::$app->session->get($skey)]);
                        foreach ($plantilla_rubros as $plantilla_rubro) {
                            if ($model instanceof Rubro && $model->id == $plantilla_rubro->rubro_id) {
                                $checked = true;
                            }
                        }
                    } else {
                        if (Yii::$app->controller->action->id == 'create' && Yii::$app->session->has('rubros_seleccionados')) {
                            $rubros_seleccionados = Yii::$app->session->get('rubros_seleccionados');
                            foreach ($rubros_seleccionados as $rubro_id) {
                                if ($model instanceof Rubro && $model->id == $rubro_id)
                                    $checked = true;
                            }
                        }
                    }

                    return [
                        'onchange' => '
                            $.ajax({
                                type: "POST",
                                url: "index.php?r=contabilidad/plantilla-compraventa/guardar-rubros-en-session&checked=" + this.checked,
                                data: {id_registro_seleccionado: ' . $model->id . '},
                                success: function (data) {
                                }
                            });',
                        'checked' => $checked
                    ];
                },
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'nombre',
                'label' => 'Nombre',
                'value' => 'nombre',
                'hidden' => false,
            ],
            'descripcion',
        ],
        'toolbar' => []
    ]);
} catch (Exception $e) {
    print $e;
}
?>