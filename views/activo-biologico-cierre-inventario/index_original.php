<?php

use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\ActivoBiologicoCierreInventarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cierre de Inventario de Activos Biológicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-biologico-cierre-inventario-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php $access = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-delete'),
        ]; ?>
        <?= $access['create'] ? Html::a('Registrar Cierre', ['create'], ['class' => 'btn btn-success']) : "" ?>
    </p>

    <?php

    try {
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (\common\helpers\PermisosHelpers::getAcceso("activo-biologico-cierre-inventario-{$_v}"))
                $template .= "&nbsp;&nbsp;&nbsp;&nbsp;{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'especie_id',
                    'value' => 'especie.nombre',
                    'format' => 'raw',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(ActivoBiologicoEspecie::find()->all(), 'nombre', 'nombre'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Filtro por Especie...'],
                ],
                [
                    'attribute' => 'clasificacion_id',
                    'value' => 'clasificacion.nombre',
                    'format' => 'raw',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(ActivoBiologicoEspecieClasificacion::find()->all(), 'nombre', 'nombre'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Filtro por Clasificación...'],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $exception) {
        \common\helpers\FlashMessageHelpsers::createWarningMessage("Error renderizando index: {$exception->getMessage()}");
    }
    ?>
</div>
