<?php

use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $especies array */
/* @var $clasif array */
/* @var $detalles ActivoBiologicoCierreInventario[] */

$action = Yii::$app->controller->action->id;
?>


<div class="activo-biologico-cierre-inventario-datos-consumo">

    <!--<fieldset>
        <legend>&nbsp;&nbsp;Agregar
            <div class="pull-left">
                <?php
                // new detalle button
                echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                    'id' => 'new-datos-consumo',
                    'class' => 'pull-left btn btn-success'
                ]);
                ?>
            </div>
        </legend>
    </fieldset>-->

    <?php

    echo '<table id="tabla-datos-consumo" class="table table-condensed table-responsive">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Especie</th>';
    echo '<th style="text-align: center;">Clasificación</th>';
    echo '<th style="text-align: center;">Precio Foro</th>';
    echo '<th style="text-align: center;">Cosumo</th>';
    echo '<th style="text-align: center;">Cosumo GD</th>';
    echo '<th style="text-align: center;">Cosumo GND</th>';
    echo '<th style="text-align: center;">Total Gs.</th>';
    echo '<th style="text-align: center;">Total GD Gs.</th>';
    echo '<th style="text-align: center;">Total GND Gs.</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    \kartik\select2\Select2Asset::register($this);
    \yii\widgets\MaskedInputAsset::register($this);

    // existing detalles fields
    $skey = '';
    $key = 0;
    foreach ($detalles as $index => $detalle) {
        $key = $detalle->id != null ? $detalle->id - 1 : $index;
        echo '<tr class="fila-detalle">';
        echo $this->render('_tds_datos_consumo', [
            'key' => 'new-consumo' . ($key + 1),
            'form' => $form,
            'model' => $detalle,
        ]);
        echo '</tr>';
        $key++;
    }

    //    $html = $this->render('_tds_datos_consumo', [
    //        'key' => '__new-consumo__',
    //        'form' => $form,
    //        'model' => $model,
    //    ]);
    //
    //    // remover todos los \n \r y combinacion de ellos segun https://stackoverflow.com/questions/5258543/remove-all-the-line-breaks-from-the-html-source
    //    $output = str_replace(array("\r\n", "\r"), "\n", $html);
    //    $lines = explode("\n", $output);
    //    $substrings = array();
    //
    //    foreach ($lines as $i => $line) {
    //        if (!empty($line))
    //            $substrings[] = trim($line);
    //    }
    //    $html_without_enter_and_spaces = implode($substrings);
    $bluePrint = "";//Json::encode($html_without_enter_and_spaces);

    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);

    $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
    ?>

    <?php

    $script_head = <<<JS
var action_id = "$action";
var detalle_k_datos_consumo = $key;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);
    $script = <<<JS
//$('#new-datos-consumo').on('click', function () {
//    detalle_k_datos_consumo += 1;
//    $('#tabla-datos-consumo').find('tbody')
//        .append('<tr class="fila-detalle">' + ($bluePrint).replace(/__new-consumo__/g, 'new-consumo' + detalle_k_datos_consumo) + '</tr>');
//
//    construirSelect2(getSelectorForTab('consumo', detalle_k_datos_consumo, 'especie_id'), false, ($especies));
//    construirSelect2(getSelectorForTab('consumo', detalle_k_datos_consumo, 'clasificacion_id'), false, ($clasif));
//    construirMaskedInput(getSelectorForTab('consumo', detalle_k_datos_consumo, '', 'monto'), 'integer');
//});
//
//$(document).on('click', '.delete-detalle-datos-consumo', function () {
//    makeZeroBeforeDeleteRow($(this), 'consumo');
//    $(this).closest('tbody tr').remove();
//});

function setProrrateoConsumoEvent(input, evt = null) {
    if (is_especieClasif_selected(input)) {
        let prefix = '#' + getPrefijoId(input);
        
        if (is_currentVal_greaterThan_stock_actual($(prefix + 'consumo_cantidad'))) {
            $(prefix + 'consumo_cantidad').val('').trigger('change');
            $('#' + getPrefijoId(input) + 'consumo_cantidad_gd').val('').trigger('change');
            $('#' + getPrefijoId(input) + 'consumo_cantidad_gnd').val('').trigger('change');
        } else {
            if (evt !== null) {
                let keyCode = evt.keyCode;
                if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key))
                    prorratear('consumo', $(prefix + 'consumo_cantidad'));
            } else {
                prorratear('consumo', $(prefix + 'consumo_cantidad'));                
            }
        }
    } else {
        $('#' + getPrefijoId(input) + 'consumo_cantidad_gd').val('').trigger('change');
        $('#' + getPrefijoId(input) + 'consumo_cantidad_gnd').val('').trigger('change');
    }
}

$(document).on('keyup', getSelectorForTab('consumo', '', 'consumo_cantidad'), function (evt) {
    // setProrrateoConsumoEvent($(this), evt);
    setEvent_prorrateo($(this), 'consumo', evt);
}).on('change', getSelectorForTab('consumo', '', 'consumo_cantidad'), function () {
    // setProrrateoConsumoEvent($(this), evt);
    setEvent_prorrateo($(this), 'consumo');
});

especieClasific = [];
especieClasific.push(getSelectorForTab('consumo', '', 'especie_id'));
especieClasific.push(getSelectorForTab('consumo', '', 'clasificacion_id'));
especieClasific.forEach(function (e, i) {
    $(document).on('change', e, function () {
        setEvent_prorrateo($(this), 'consumo');
        // setProrrateoConsumoEvent($(this), null);
    });
});
delete especieClasific;

/**
 * Calcula el consumo deducible y no deducible del consumo indicada.
 *
 * @param    {string}    contains    Subcadena del prefijo del input en el que se acaba de escribir que lo identifica univocamente.
 * @param    {jquery}    input       Representa el input para consumo en el que se acaba de escribir.
 */
function prorratearConsumo(contains = '', input) {
    let selector = getSelectorForTab('consumo', contains, 'consumo_cantidad');
    $(selector)
        .each(function () {
            let especieClasificacion = getEspecieClasif(input);
            let prefix = '#' + getPrefijoId(input);

            if (isNaN(especieClasificacion[0]) || isNaN(especieClasificacion[1]))
                return false;

            let campo = 'stock_actual';
            let consumo_permitida = 0.01;
            let stock_actual = getValFromDatosIniciales(especieClasificacion, campo);
            let consumo = input.val();

            if (stock_actual) {
                let consumo_cantidad_gd = Math.round(stock_actual * consumo_permitida);
                let consumo_cantidad_gnd = consumo - consumo_cantidad_gd;
                if (consumo <= consumo_cantidad_gd) {
                    consumo_cantidad_gd = consumo;
                    consumo_cantidad_gnd = 0;
                }
                $(prefix + 'consumo_cantidad_gd').val(consumo_cantidad_gd).trigger('change');
                $(prefix + 'consumo_cantidad_gnd').val(consumo_cantidad_gnd).trigger('change');
            } else {
                $(prefix + 'consumo_cantidad_gd').val('').trigger('change');
                $(prefix + 'consumo_cantidad_gnd').val('').trigger('change');
            }
        });
}

$(document).ready(function () {
    construirSelect2(getSelectorForTab('consumo', '', 'especie_id'), true, ($especies));
    construirSelect2(getSelectorForTab('consumo', '', 'clasificacion_id'), true, ($clasif));
    construirMaskedInput(getSelectorForTab('consumo', '', '', 'monto'), 'integer');
})
JS;
    $this->registerJs($script);
    ?>

</div>
