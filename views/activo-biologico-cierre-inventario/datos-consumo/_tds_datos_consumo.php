<?php

use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \backend\modules\contabilidad\models\activobiologico */
?>
<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'especie_id')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][especie_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'especie_id')->hiddenInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][especie_id]",
    ])->label(false) ?>
</td>

<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'clasificacion_id')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][clasificacion_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'clasificacion_id')->hiddenInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][clasificacion_id]",
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'precio_foro')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-precio_foro",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][precio_foro]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'consumo_cantidad')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-consumo_cantidad",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][consumo_cantidad]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'consumo_cantidad_gd')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-consumo_cantidad_gd",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][consumo_cantidad_gd]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'consumo_cantidad_gnd')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-consumo_cantidad_gnd",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][consumo_cantidad_gnd]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_guaranies')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-total_guaranies",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][total_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_gd_guaranies')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-total_gd_guaranies",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][total_gd_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_gnd_guaranies')->textInput([
        'id' => "activobiologicocierreinventarioconsumo-{$key}-total_gnd_guaranies",
        'name' => "ActivoBiologicoCierreInventarioConsumo[$key][total_gnd_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<!--<td class=" " style="text-align: center">-->
<?= ""/*Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'delete-detalle-datos-consumo btn btn-danger btn-sm',
        'id' => "activobiologicocierreinventarioconsumo-{$key}-delete_button",
        'title' => 'Eliminar fila',
    ])*/ ?>
<!--</td>-->