<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 28/11/18
 * Time: 12:15 PM
 */

use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $especies array */
/* @var $clasif array */
/* @var $detalles ActivoBiologicoCierreInventario[] */
/* @var $divClass string */

isset($divClass) || $divClass = 'panel panel-success';
?>

<?php $div = <<<HTML
<div class="{$divClass}">
    <div class="panel-heading"><strong>Panel Heading</strong></div>
    <div class="panel-body">Panel Content</div>
</div>
HTML;

$div_procreo = $div;
$div_procreo = str_replace('Panel Heading', '&nbsp;&nbsp;PROCREO', $div_procreo);
$div_procreo = str_replace('Panel Content', $this->render('../datos-procreo/_form_datos_procreo',
    ['form' => $form, 'model' => $model, 'detalles' => $detalles, 'especies' => $especies, 'clasif' => $clasif]),
    $div_procreo);

$div_costo_v = $div;
$div_costo_v = str_replace('Panel Heading', '&nbsp;&nbsp;COSTO DE VENTA', $div_costo_v);
$div_costo_v = str_replace('Panel Content', $this->render('../datos-venta/_form_datos_venta',
    ['form' => $form, 'model' => $model, 'detalles' => $detalles, 'especies' => $especies, 'clasif' => $clasif]),
    $div_costo_v);

$div_mortandad = $div;
$div_mortandad = str_replace('Panel Heading', '&nbsp;&nbsp;MORTANDAD', $div_mortandad);
$div_mortandad = str_replace('Panel Content', $this->render('../datos-mortandad/_form_datos_mortandad',
    ['form' => $form, 'model' => $model, 'detalles' => $detalles, 'especies' => $especies, 'clasif' => $clasif]),
    $div_mortandad);

$div_consumo = $div;
$div_consumo = str_replace('Panel Heading', '&nbsp;&nbsp;CONSUMO', $div_consumo);
$div_consumo = str_replace('Panel Content', $this->render('../datos-consumo/_form_datos_consumo',
    ['form' => $form, 'model' => $model, 'detalles' => $detalles, 'especies' => $especies, 'clasif' => $clasif]),
    $div_consumo);

$div_datos = $div;
$div_datos = str_replace('Panel Heading', '&nbsp;&nbsp;OTROS DATOS', $div_datos);
$div_datos = str_replace('Panel Content', $this->render('../datos/_form_datos',
    ['form' => $form, 'model' => $model, 'detalles' => $detalles, 'especies' => $especies, 'clasif' => $clasif]),
    $div_datos);

echo $div_procreo;
echo $div_costo_v;
echo $div_mortandad;
echo $div_consumo;
echo $div_datos;
?>