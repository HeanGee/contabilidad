<?php

use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $especies array */
/* @var $clasif array */
/* @var $detalles ActivoBiologicoCierreInventario[] */

$action = Yii::$app->controller->action->id;
?>


<div class="activo-biologico-cierre-inventario-datos-mortandad">

    <!--<fieldset>
        <legend>&nbsp;Agregar
            <div class="pull-left">
                <?php
                // new detalle button
                echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                    'id' => 'new-datos-mortandad',
                    'class' => 'pull-left btn btn-success'
                ]);
                ?>
            </div>
        </legend>
    </fieldset>-->

    <?php

    echo '<table id="tabla-datos-mortandad" class="table table-condensed table-responsive">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Especie</th>';
    echo '<th style="text-align: center;">Clasificación</th>';
    echo '<th style="text-align: center;">Precio Foro</th>';
    echo '<th style="text-align: center;">Mortandad</th>';
    echo '<th style="text-align: center;">Mortandad GD</th>';
    echo '<th style="text-align: center;">Mortandad GND</th>';
    echo '<th style="text-align: center;">Total Gs.</th>';
    echo '<th style="text-align: center;">Total GD Gs.</th>';
    echo '<th style="text-align: center;">Total GND Gs.</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    \kartik\select2\Select2Asset::register($this);
    \yii\widgets\MaskedInputAsset::register($this);

    $query = ActivoBiologicoEspecie::find();
    $query->select(['id', 'nombre as text']);
    $query->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
    $especies = [['id' => '', 'text' => ''],];
    $especies = array_merge($especies, $query->asArray()->all());
    $especies = \yii\helpers\Json::encode($especies);

    $query = \backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion::find();
    $query->select(['id', 'nombre as text']);
    $query->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
    $clasif = [['id' => '', 'text' => ''],];
    $clasif = array_merge($clasif, $query->asArray()->all());
    $clasif = \yii\helpers\Json::encode($clasif);

    // existing detalles fields
    $skey = '';
    $key = 0;
    foreach ($detalles as $index => $detalle) {
        $key = $detalle->id != null ? $detalle->id - 1 : $index;
        echo '<tr class="fila-detalle">';
        echo $this->render('_tds_datos_mortandad', [
            'key' => 'new-mortandad' . ($key + 1),
            'form' => $form,
            'model' => $detalle,
        ]);
        echo '</tr>';
        $key++;
    }

    //    $html = $this->render('_tds_datos_mortandad', [
    //        'key' => '__new-mortandad__',
    //        'form' => $form,
    //        'model' => $model,
    //    ]);
    //
    //    // remover todos los \n \r y combinacion de ellos segun:
    //    // https://stackoverflow.com/questions/5258543/remove-all-the-line-breaks-from-the-html-source
    //    $output = str_replace(array("\r\n", "\r"), "\n", $html);
    //    $lines = explode("\n", $output);
    //    $substrings = array();
    //
    //    foreach ($lines as $i => $line) {
    //        if (!empty($line))
    //            $substrings[] = trim($line);
    //    }
    //    $html_without_enter_and_spaces = implode($substrings);
    $bluePrint = "";//Json::encode($html_without_enter_and_spaces);

    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);

    $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
    ?>

    <?php

    $script_head = <<<JS
var action_id = "$action";
var detalle_k_datos_mortandad = $key;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);
    $script = <<<JS
//$('#new-datos-mortandad').on('click', function () {
//    detalle_k_datos_mortandad += 1;
//    $('#tabla-datos-mortandad').find('tbody')
//        .append('<tr class="fila-detalle">' + 
// ($bluePrint).replace(/__new-mortandad__/g, 'new-mortandad' + detalle_k_datos_mortandad) + '</tr>');
//
//    construirSelect2(getSelectorForTab('mortandad', detalle_k_datos_mortandad, 'especie_id'), false, ($especies));
//    construirSelect2(getSelectorForTab('mortandad', detalle_k_datos_mortandad, 'clasificacion_id'), false, ($clasif));
//    construirMaskedInput(getSelectorForTab('mortandad', detalle_k_datos_mortandad, '', 'monto'), 'integer');
//});
//
//$(document).on('click', '.delete-detalle-datos-mortandad', function () {
//    makeZeroBeforeDeleteRow($(this), 'mortandad');
//    $(this).closest('tbody tr').remove();
//});

function setProrrateoMortandadEvent(input, evt = null) {
    if (is_especieClasif_selected(input)) {
        let prefix = '#' + getPrefijoId(input);
        
        if (is_currentVal_greaterThan_stock_actual($(prefix + 'mortandad_cantidad'))) {
            $(prefix + 'mortandad_cantidad').val('').trigger('change');
            $('#' + getPrefijoId(input) + 'mortandad_cantidad_gd').val('').trigger('change');
            $('#' + getPrefijoId(input) + 'mortandad_cantidad_gnd').val('').trigger('change');
        } else {
            if (evt !== null) {
                let keyCode = evt.keyCode;
                if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key))
                    prorratear('mortandad', $(prefix + 'mortandad_cantidad'));
            } else {
                prorratear('mortandad', $(prefix + 'mortandad_cantidad'));                
            }
        }
    } else {
        $('#' + getPrefijoId(input) + 'mortandad_cantidad_gd').val('').trigger('change');
        $('#' + getPrefijoId(input) + 'mortandad_cantidad_gnd').val('').trigger('change');
    }
}

$(document).on('keyup', getSelectorForTab('mortandad', '', 'mortandad_cantidad'), function (evt) {
    // setProrrateoMortandadEvent($(this), evt);
    setEvent_prorrateo($(this), 'mortandad', evt);
}).on('change', getSelectorForTab('mortandad', '', 'mortandad_cantidad'), function () {
    // setProrrateoMortandadEvent($(this), evt);
    setEvent_prorrateo($(this), 'mortandad');
});

especieClasific = [];
especieClasific.push(getSelectorForTab('mortandad', '', 'especie_id'));
especieClasific.push(getSelectorForTab('mortandad', '', 'clasificacion_id'));
especieClasific.forEach(function (e, i) {
    $(document).on('change', e, function () {
        setEvent_prorrateo($(this), 'mortandad');
        // setProrrateoMortandadEvent($(this), null);
    });
});
delete especieClasific;

/**
 * Calcula la mortandad deducible y no deducible de la mortandad indicada.
 *
 * @param contains   {string}    Subcadena del prefijo del input en el que se acaba de escribir que lo identifica univocamente.
 * @param input      {jquery}    Representa el input para mortandad en el que se acaba de escribir.
 */
function prorratearMortandad(contains = '', input) {
    let selector = getSelectorForTab('mortandad', contains, 'mortandad_cantidad');
    $(selector)
        .each(function () {
            let especieClasificacion = getEspecieClasif(input);
            let prefix = '#' + getPrefijoId(input);

            if (isNaN(especieClasificacion[0]) || isNaN(especieClasificacion[1]))
                return false;

            let campo = 'stock_actual';
            let mortandad_permitida = 0.03;
            let stock_actual = getValFromDatosIniciales(especieClasificacion, campo);
            let mortandad = input.val();

            if (stock_actual) {
                console.log('a');
                let mortandad_cantidad_gd = Math.round(stock_actual * mortandad_permitida);
                let mortandad_cantidad_gnd = mortandad - mortandad_cantidad_gd;
                if (mortandad <= mortandad_cantidad_gd) {
                    mortandad_cantidad_gd = mortandad;
                    mortandad_cantidad_gnd = 0;
                }
                $(prefix + 'mortandad_cantidad_gd').val(mortandad_cantidad_gd).trigger('change');
                $(prefix + 'mortandad_cantidad_gnd').val(mortandad_cantidad_gnd).trigger('change');
            } else {
                console.log('b');
                $(prefix + 'mortandad_cantidad_gd').val('').trigger('change');
                $(prefix + 'mortandad_cantidad_gnd').val('').trigger('change');
            }
        });
}

$(document).ready(function () {
    construirSelect2(getSelectorForTab('mortandad', '', 'especie_id'), true, ($especies));
    construirSelect2(getSelectorForTab('mortandad', '', 'clasificacion_id'), true, ($clasif));
    construirMaskedInput(getSelectorForTab('mortandad', '', '', 'monto'), 'integer');
})
JS;
    $this->registerJs($script);
    ?>

</div>
