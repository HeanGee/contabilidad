<?php

use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \backend\modules\contabilidad\models\activobiologico */
?>
<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'especie_id')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][especie_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'especie_id')->hiddenInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][especie_id]",
    ])->label(false) ?>
</td>

<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'clasificacion_id')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][clasificacion_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'clasificacion_id')->hiddenInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][clasificacion_id]",
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'precio_foro')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-precio_foro",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][precio_foro]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'mortandad_cantidad')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-mortandad_cantidad",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][mortandad_cantidad]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'mortandad_cantidad_gd')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-mortandad_cantidad_gd",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][mortandad_cantidad_gd]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'mortandad_cantidad_gnd')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-mortandad_cantidad_gnd",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][mortandad_cantidad_gnd]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_guaranies')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-total_guaranies",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][total_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_gd_guaranies')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-total_gd_guaranies",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][total_gd_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_gnd_guaranies')->textInput([
        'id' => "activobiologicocierreinventariomortandad-{$key}-total_gnd_guaranies",
        'name' => "ActivoBiologicoCierreInventarioMortandad[$key][total_gnd_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<!--<td class=" " style="text-align: center">-->
<?= ""/*Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'delete-detalle-datos-mortandad btn btn-danger btn-sm',
        'id' => "activobiologicocierreinventariomortandad-{$key}-delete_button",
        'title' => 'Eliminar fila',
    ])*/ ?>
<!--</td>-->