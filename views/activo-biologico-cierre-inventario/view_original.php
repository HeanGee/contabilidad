<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologicoCierreInventario */

$this->title = "Datos del Cierre de {$model->fecha}";
$this->params['breadcrumbs'][] = ['label' => 'Cierre', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="activo-biologico-cierre-inventario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php $access = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-delete'),
        ]; ?>
        <?= $access['index'] ? Html::a('Ir a Cierre', ['index'], ['class' => 'btn btn-primary']) : "" ?>
        <?= $access['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : "" ?>
        <?= $access['delete'] ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Está seguro de eliminar este ítem?',
                'method' => 'post',
            ],
        ]) : "" ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Prestamo',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                // TODO: implementar.
            ],
        ]);

    } catch (Exception $exception) {
        \common\helpers\FlashMessageHelpsers::createWarningMessage("Error renderizando view: {$exception->getMessage()}");
    } ?>

</div>
