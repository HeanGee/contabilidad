<?php

use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \backend\modules\contabilidad\models\activobiologico */
?>
<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'especie_id')->textInput([
        'id' => "activobiologicocierreinventariodatoscalculados-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioDatosCalculados[$key][especie_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
    <?= $form->field($model, 'especie_id')->hiddenInput([
        'id' => "activobiologicocierreinventariodatoscalculados-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioDatosCalculados[$key][especie_id]",
    ])->label(false) ?>
</td>

<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'clasificacion_id')->textInput([
        'id' => "activobiologicocierreinventariodatoscalculados-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioDatosCalculados[$key][clasificacion_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
    <?= $form->field($model, 'clasificacion_id')->hiddenInput([
        'id' => "activobiologicocierreinventariodatoscalculados-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioDatosCalculados[$key][clasificacion_id]",
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'stock_final')->textInput([
        'id' => "activobiologicocierreinventariodatoscalculados-{$key}-stock_final",
        'name' => "ActivoBiologicoCierreInventarioDatosCalculados[$key][stock_final]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>