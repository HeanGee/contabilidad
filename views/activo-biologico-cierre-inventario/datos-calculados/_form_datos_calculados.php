<?php

use kartik\form\ActiveForm;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $especies array */
/* @var $clasif array */

$action = Yii::$app->controller->action->id;
?>


<div class="activo-biologico-cierre-inventario-datos-calculados">

    <fieldset>
        <legend class="text-info">&nbsp;&nbsp;Resumen
        </legend>
    </fieldset>

    <?php

    echo '<table id="tabla-datos-calculados" class="table table-condensed table-responsive">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Especie</th>';
    echo '<th style="text-align: center;">Clasificación</th>';
    echo '<th style="text-align: center;">Stock Final</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    \kartik\select2\Select2Asset::register($this);
    \yii\widgets\MaskedInputAsset::register($this);

    // existing detalles fields
    $skey = '';
    //    $detalles = ActivoBiologico::find()->where([
    //        'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
    //        'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])->all();
    $key = 0;
    foreach ($detalles as $index => $detalle) {
        $key = $detalle->id != null ? $detalle->id - 1 : $index;
        echo '<tr class="fila-detalle">';
        echo $this->render('_tds_datos_calculados', [
            'key' => 'new-calculados' . ($key + 1),
            'form' => $form,
            'model' => $detalle,
        ]);
        echo '</tr>';
        $key++;
    }

    echo '</tbody>';
    echo '</table>';

    $submitBtn_text = ($action == 'create') ? 'Guardar' : 'Actualizar';
    $submitBtn_class = ($action == 'create') ? 'btn btn-success' : 'btn btn-primary';
    //    echo Html::submitButton($submitBtn_text, ['id' => 'btn-submit', 'class' => $submitBtn_class]);

    $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
    ?>

    <?php

    $script_head = <<<JS
var action_id = "$action";
var detalle_k_datos_calculados = $key;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);

    $url_simularAsiento = Json::htmlEncode(\Yii::t('app', Url::to(['simular-asiento'])));
    $script = <<<JS
$('btn#btn-submit').on('click', function () {
    console.log('asdf;dsjfajsdf');
});

/**
 *  Retorna un array bidimensional que contiene los valores de los input de la tabla, en orden natural,
 *  cuyo id recibe como parametro.
 *
 *  FUEENTAS:
 *  * https://stackoverflow.com/questions/34349403/convert-table-to-array-in-javascript-without-using-jquery
 *  * https://stackoverflow.com/questions/1144705/best-way-to-store-a-key-value-array-in-javascript/1144737
 *  * https://www.xul.fr/javascript/associative.php
 *  * https://stackoverflow.com/questions/4091257/variable-as-index-in-an-associative-array-javascript
 *  * 
 *
 * @param table_id
 * @return {any[]}
 */
function generateArrayFromTab(table_id) {
    let array = {};
    $('table[id="' + table_id + '"] tr').each(function (i, tr) {
        let fila = 'fila' + i;
        let obj = {};
        $(tr).find('td input:not([type="hidden"])').each(function (i, input) {
            let id = '#' + $(input).prop('id');
            let slices = id.split('-');
            let attribute = slices.splice(-1, 1);
            let key = attribute[0];
            obj[key] = $(id).val();
        });
        array[fila] = obj;
    });
    return array;
}

/**
 *  Realiza simulacion de asiento.
 */
function simularAsiento() {
    let procreoData = generateArrayFromTab('tabla-datos-procreo'),
        ventaData = generateArrayFromTab('tabla-datos-venta'),
        consumoData = generateArrayFromTab('tabla-datos-consumo'),
        mortandadData = generateArrayFromTab('tabla-datos-mortandad'),
        datosIniciales = generateArrayFromTab('tabla-datos-iniciales');

    $.ajax({
        url: $url_simularAsiento,
        type: 'get',
        data: {
            procreo: procreoData,
            venta: ventaData,
            consumo: consumoData,
            mortandad: mortandadData,
            datosIniciales: datosIniciales
        },
        success: function (data) {
            $.pjax.reload({container: "#flash_message_id", async: false});
            if (data.error)
                $('div[id="pjax_asiento_simulator"]').prop('style', 'display:none');
            else
                $.pjax.reload({container: "#pjax_asiento_simulator", async: false});
        },
    });
}

$('a[data-toggle="tab"][id="datos-calculados"]').on('click', function (e) {
    $('div[id="pjax_asiento_simulator"]').prop('style', 'display:');
    simularAsiento();
});

$(document).ready(function () {
    construirSelect2(getSelectorForTab('calculados', '', 'especie_id'), true, ($especies));
    construirSelect2(getSelectorForTab('calculados', '', 'clasificacion_id'), true, ($clasif));
    construirMaskedInput(getSelectorForTab('calculados', '', '', 'monto'), 'integer');
})
JS;
    $this->registerJs($script);
    ?>

</div>
