<?php

use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use common\helpers\FlashMessageHelpsers;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $model ActivoBiologicoCierreInventario */
/* @var $form ActiveForm */
/* @var $show_tab string */
/* @var $detalles ActivoBiologicoCierreInventario[] */
/* @var $topButtons string */
/* @var $divClass string */

isset($divClass) || $divClass = 'panel panel-success';
$action = Yii::$app->controller->action->id;
?>

<div class="activo-biologico-cierre-inventario-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-activo-biologico-cierre-inventario',
    ]); ?>

    <?= $topButtons ?>

    <?php
    try {
        // construir json array de especies para Select2
        $query = ActivoBiologicoEspecie::find()->alias('especie');
        $query->innerJoin('cont_activo_biologico as bio', 'bio.especie_id = especie.id');
        $query->select(['especie.id as id', 'especie.nombre as text']);
        $query->where(['especie.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'especie.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        $especies = [['id' => '', 'text' => '']];
        $especies = array_merge($especies, $query->asArray()->all());
        $especies = \yii\helpers\Json::encode($especies);

        // construir json array de clasificaciones para Select2
        $query = ActivoBiologicoEspecieClasificacion::find()->alias('clasif');
        $query->innerJoin('cont_activo_biologico as bio', 'bio.clasificacion_id = clasif.id');
        $query->select(['clasif.id as id', 'clasif.nombre as text']);
        $query->where(['clasif.empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'clasif.periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
        $clasif = [['id' => '', 'text' => '']];
        $clasif = array_merge($clasif, $query->asArray()->all());
        $clasif = \yii\helpers\Json::encode($clasif);

        // para que se pueda renderizar bien los select2 sin quebrarse.
        echo '<div style="display: none;">';
        echo Select2::widget([
            'name' => 'state_10',
            'data' => ['a' => 'a'],
            'options' => [
                'placeholder' => 'Select provinces ...',
                'multiple' => true
            ],
        ]);
        echo '</div>';

        $items = [
            [
                'label' => '<i class="fa fa-home"></i> Datos Iniciales',
                'content' => $this->render('datos-iniciales/_form_datos_iniciales', ['form' => $form, 'especies' => $especies, 'clasif' => $clasif]),
                'active' => $show_tab == 'datos-iniciales',
                'linkOptions' => array('id' => 'datos-iniciales'),
            ],
            [
                'label' => '<i class="fa fa-pencil"></i> Datos Finales', // se pidio cambiar de 'de entrada' a 'finales'
                'content' => $this->render('datos-entrada/_form_datos_entrada', ['form' => $form, 'model' => $model, 'detalles' => $detalles, 'especies' => $especies, 'clasif' => $clasif, 'divClass' => $divClass]),
                'active' => $show_tab == 'datos-entrada',
                'linkOptions' => array('id' => 'datos-entrada'),
            ],
            [
                'label' => '<i class="fa fa-calculator"></i> Datos Calculados</span>',
                'content' => $this->render('datos-calculados/_form_datos_calculados', ['form' => $form, 'especies' => $especies, 'detalles' => $detalles, 'clasif' => $clasif]),
                'active' => $show_tab == 'datos-calculados',
                'linkOptions' => array('id' => 'datos-calculados'),
            ],
        ];

        echo TabsX::widget([
            'items' => $items,
            'position' => TabsX::POS_ABOVE,
            'bordered' => false,
            'encodeLabels' => false
        ]);

        /** SIMULACION DE ASIENTO */
        \yii\widgets\Pjax::begin(['id' => 'pjax_asiento_simulator',]);
        echo $this->render('simulacion-asiento/_simulador_asiento');
        \yii\widgets\Pjax::end();

    } catch (Exception $exception) {
        print $exception->getMessage();
        FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    }
    ?>

    <?php ActiveForm::end(); ?>

    <?php
    $scripts = <<<JS
function construirSelect2(selector, disabled = true, select_data) {
    // console.log('construir select2:', selector);
    $(selector)
        .each(function () {
            let e = $(this);

            if (!$(e).data('select2')) {
                $(e).select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: select_data,
                    disabled: disabled,
                });
            }
        });
}

function construirMaskedInput(selector, number_type = 'integer', integerDigits = 17) {
    // console.log('aplicar maskedinput:', selector);
    $(selector).inputmask({
        "alias": "numeric",
        "digits": (number_type === 'integer') ? 0 : 2,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true,
        // allowMinus: false,
        integerDigits: integerDigits,
        fractionalDigits: (number_type === 'integer') ? 0 : 2,
        removeMaskOnSubmit: true,
    });
}

/**
 * Construye y retorna un selector de jQuery.
 *
 * @param tab           String. Indica la tabla (concepto) cuyos campos se desea filtrar.
 * @param contains      String. Sirve para restringir al jQuery. Es el nro que representa la fila de la tabla.
 * @param field         String. Indica el campo sobre el cual construir el selector.
 *                      Normalmente es uno de los atributos del modelo.
 * @param classs        String. Sirve para construir el selector solamente por inputs que tengan la clase classs.
 * @return {string}     El selector jQuery.
 */
function getSelectorForTab(tab, contains = '', field = '', classs = '') {
    let selector = '';
    switch (tab) {
        case "procreo":
            selector = ':input[id^="activobiologicocierre"][id*="new-procreo' + contains + '"]';
            break;
        case "venta":
            selector = ':input[id^="activobiologicocierre"][id*="new-venta' + contains + '"]';
            break;
        case "mortandad":
            selector = ':input[id^="activobiologicocierre"][id*="new-mortandad' + contains + '"]';
            break;
        case "consumo":
            selector = ':input[id^="activobiologicocierre"][id*="new-consumo' + contains + '"]';
            break;
        case "datos-iniciales":
            selector = ':input:not([id*="cierre"])[id^="activobiologico"][id*="new' + contains + '"]';
            break;
        case "calculados":
            selector = ':input[id^="activobiologicocierre"][id*="new-calculados' + contains + '"]';
            break;
        case "datos":
            selector = ':input[id^="activobiologicocierre"][id*="new-datos' + contains + '"]';
            break;
    }

    if (field !== '') {
        selector = selector + '[id$="' + field + '"]';
    }
    if (classs !== '') {
        selector = selector + '.' + classs;
    }
    return selector.concat(':not([type*="hidden"])');
}

/**
 * Obtiene valor del campo deseado de la primera pestanha segun especie y clasificacion.
 *
 * @param especieClasificacion   {array}     Contiene dos elementos que son id de especie y clasificacion.
 * @param campo                  {string}    String que especifica el campo cuyo valor se desea retornar.
 * @returns {int}
 */
function getValFromDatosIniciales(especieClasificacion, campo) {
    let selector = getSelectorForTab('datos-iniciales', '', 'especie_id');
    // console.log(especieClasificacion);
    // console.log(selector);

    let valor = 0;
    $(selector)
        .each(function () {
            let prefix = '#' + getPrefijoId($(this));
            let especie_id = prefix + 'especie_id';
            let clasificacion_id = prefix + 'clasificacion_id';

            if ($(especie_id).val() === especieClasificacion[0] && $(clasificacion_id).val() === especieClasificacion[1]) {
                // masked input aplicado por jQ hace que devuelva numero al hacer val()
                valor = $(prefix + campo).val();
                if (isNaN(valor)) valor = 0;
                else valor = parseInt(valor);
                return false;   // break para .each().
            }
        });

    return valor;
}

/**
 * Obtiene el valor del input campo de la tabla (concepto) tab segun a.bio especieClasificacion.
 *
 * @param tab                   String que indica la tabla de cuyo input se desea retornar el valor.
 * @param especieClasificacion  Array de ids de especie/clasificacion para identificar el a.bio.
 * @param campo                 String que indica de que input retornar el valor.
 *                              Normalmente es uno de los atributos del modelo.
 * @return {int}     El valor contenido en el input campo de la tabla tab para a.bio especieClasificacion.
 */
function getValFromTab(tab, especieClasificacion, campo) {
    let selector = getSelectorForTab(tab, '', 'especie_id');

    let valor = 0;
    $(selector)
        .each(function () {
            let prefix = '#' + getPrefijoId($(this));
            let especie_id = prefix + 'especie_id';
            let clasificacion_id = prefix + 'clasificacion_id';

            // se usa == para convertir al mismo tipo.
            // si no se ha elegido especie o clasif o ambos, simplemente devuelve 0.
            // $(prefijo + campo).val() devuelve false si la combinacion especie//clasificacion no existe.
            if ($(especie_id).val() == especieClasificacion[0] && $(clasificacion_id).val() == especieClasificacion[1]) {
                // masked input aplicado por jQ hace que devuelva numero al hacer val()
                valor = $(prefix + campo).val();
                if (isNaN(valor)) valor = 0;
                else valor = parseInt(valor);
                return false;   // break para .each().
            }
        });

    return valor;
}

/**
 * Establece el valor de un input de una tabla (concepto) segun a.bio.
 *
 * Es utilizado por updateFinalStock(...)
 *
 * @param tab                       Indica la tabla al cual pertenece el input a modificar.
 * @param especieClasificacion      Array con id de especie y clasificacion para identificar el a.bio
 * @param campo                     String que indica a que input modificar.
 *                                  Normalmente es uno de los atributos del modelo.
 * @param value                     Nuevo valor para el input correspondiente al campo.
 */
function setValueForTab(tab, especieClasificacion, campo, value) {
    let selector = getSelectorForTab(tab, '', 'especie_id');

    $(selector)
        .each(function () {
            let prefix = '#' + getPrefijoId($(this));
            let especie_id = prefix + 'especie_id';
            let clasificacion_id = prefix + 'clasificacion_id';

            // se usa == para convertir al mismo tipo.
            // si no se ha elegido especie o clasif o ambos, simplemente devuelve 0.
            if ($(especie_id).val() == especieClasificacion[0] && $(clasificacion_id).val() == especieClasificacion[1]) {
                // masked input aplicado por jQ hace que devuelva numero al hacer val()
                // console.log($(prefix + campo), value);
                $(prefix + campo).val(value).trigger('change');
                return false;   // break para .each().
            }
        });
}

/**
 * Funcion invocada por eventos 'keyup/change' del input recibido como parametro.
 *
 * Si especie y clasificacion estan seleccionadas y el valor del input
 * no es mayor al stock actual, del a.bio correspondiente, invoca a prorratear(...)
 * para ejecutar la funcion de prorrateo adecuado y actualiza el stock_final
 * del a.bio correspondiente invocando updateFinalStock(...).
 *
 * @param input     jQuery del input modificado recientemente.
 *                  Puede ser Select2 de especie/casificacion como asi tambien campo de [<...>]_cantidad.
 * @param concepto  Indica la tabla a la que pertenece el input.
 * @param evt       Objeto Event o 'null'.
 *                  Se recibe Event si se escribe sobre el input usando el teclado (lanza evento keyup)
 *                  Se recibe 'null' si el input modificado es un select2 de especie o de clasificacion
 *                  (lanza evento change).
 */
function setEvent_prorrateo(input, concepto, evt = null) {
    if (is_especieClasif_selected(input)) {
        let prefix = '#' + getPrefijoId(input);

        if (is_currentVal_greaterThan_stock_actual($(prefix + concepto + '_cantidad'))) {
            $(prefix + concepto + '_cantidad').val('').trigger('change');
            $('#' + getPrefijoId(input) + concepto + '_cantidad_gd').val('').trigger('change');
            $('#' + getPrefijoId(input) + concepto + '_cantidad_gnd').val('').trigger('change');
        } else {
            if (evt !== null) {
                if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
                    prorratear(concepto, $(prefix + concepto + '_cantidad'));
                    updateFinalStock($(prefix + concepto + '_cantidad'));
                    calcularTotales($(prefix + concepto + '_cantidad'));
                }
            } else {
                prorratear(concepto, $(prefix + concepto + '_cantidad'));
                updateFinalStock($(prefix + concepto + '_cantidad'));
                calcularTotales($(prefix + concepto + '_cantidad'));
            }
        }
    } else {
        $('#' + getPrefijoId(input) + concepto + '_cantidad_gd').val('').trigger('change');
        $('#' + getPrefijoId(input) + concepto + '_cantidad_gnd').val('').trigger('change');
    }
}

/**
 * Invoca la funcion de prorrateo segun sea para mortandad o para consumo.
 *
 * @param {string}  tab     Tabla (concepto) de mortandad o de consumo.
 * @param {jquery}  input   Input que representa mortandad o consumo totales ingresados.
 * @return {function}       Funcion de prorrateo segun sea para mortandad o consumo.
 */
function prorratear(tab, input) {
    let regex, groups;
    switch (tab) {
        case 'mortandad':
            regex = /([a-z]+)-(new-mortandad)([\d]+)/gm;
            groups = regex.exec(input.attr('id'));
            prorratearMortandad(groups[3], input); // groups[3] captura el numero del string id del input.
            break;
        case 'consumo':
            regex = /([a-z]+)-(new-consumo)([\d]+)/gm;
            groups = regex.exec(input.attr('id'));
            prorratearConsumo(groups[3], input);
            break;
        case 'venta':
            regex = /([a-z]+)-(new-venta)([\d]+)/gm;
            groups = regex.exec(input.attr('id'));
            prorratearVenta(groups[3], input);
            break;
    }
}

/**
 * Retorna un array de dos elementos donde el primero es especie_id y el segundo es clasificacion_id
 *
 * @param   {jquery}    input   Representa el input que acaba de sufrir cambios.
 * @return  {array}     El array conteniendo especie y clasificaion ids.
 */
function getEspecieClasif(input) {
    let prefix = '#' + getPrefijoId(input);
    let especie_id = prefix + 'especie_id';
    let clasificacion_id = prefix + 'clasificacion_id';

    let especieClasificacion = [];
    especieClasificacion.push(parseInt($(especie_id).val()));
    especieClasificacion.push(parseInt($(clasificacion_id).val()));

    return especieClasificacion;
}

/**
 * Retorna el prefijo mas largo del id del input, excluyendo el identificador del atributo del modelo.
 *
 * @param {jquery} input Representa el input que acaba de sufrir cambios.
 * @return {string} El prefijo del id del input que acaba de sufrir cambios.
 */
function getPrefijoId(input) {
    let input_id = input.attr('id');
    let input_id_slices = input_id.split('-');
    input_id_slices.splice(-1, 1);
    return input_id_slices.join('-') + '-';
}

/**
 * Devuelve TRUE si la especie y clasificacion correspondiente al input
 * estan seleccionadas. Sino FALSE.
 *
 * @param   {jquery}    input   Representa al input modificada recientemente.
 * @return  {boolean}   TRUE si clasificacion y especie estan seleccionadas. Sino FALSE.
 */
function is_especieClasif_selected(input) {
    let prefix = getPrefijoId(input);
    let especie = $('#' + prefix + 'especie_id').val();
    let clasificacion = $('#' + prefix + 'clasificacion_id').val();

    return (especie !== '0' && clasificacion !== '0');
}

/**
 * Devuelve TRUE si el valor del input es mayor al stock actual del
 * activo biologico correspondiente al dicho input. Sino devuelve FALSE.
 *
 * @param   {jquery}    input   Representa al input de cantidad modificada recientemente.
 * @return  {boolean}   TRUE si el valor del input es mayor al stock actual del a.bio correspondiente. Sino FALSE.
 */
function is_currentVal_greaterThan_stock_actual(input) {
    let especieClasificacion = getEspecieClasif(input);
    let stock_actual = getValFromDatosIniciales(especieClasificacion, 'stock_actual');
    let currentVal = input.val();

    return currentVal > stock_actual;
}

/**
 * Recalcula el stock final del activo biologico correspondiente
 * al input de [<...>]_cantidad modificada recientemente tomando
 * los valores de cantidad de todas las tablas.
 *
 * @param   {jquery}    input   Representa al input de [<...>]cantidad modificada recientemente.
 */
function updateFinalStock(input) {
    let especieClasif = getEspecieClasif(input);
    let stock_final = getValFromDatosIniciales(especieClasif, 'stock_actual'), tmp = stock_final;
    let arr = [];
    arr.push({val: getValFromTab('venta', especieClasif, 'venta_cantidad'), op: '-'});
    arr.push({val: getValFromTab('consumo', especieClasif, 'consumo_cantidad'), op: '-'});
    arr.push({val: getValFromTab('mortandad', especieClasif, 'mortandad_cantidad'), op: '-'});
    arr.push({val: getValFromTab('procreo', especieClasif, 'procreo_cantidad'), op: '+'});
    arr.push({val: getValFromTab('datos', especieClasif, 'ajuste_inventario'), op: '+'});
    arr.push({val: getValFromTab('datos', especieClasif, 'recoluta'), op: '+'});
    arr.push({val: getValFromTab('datos', especieClasif, 'reclasificacion_entrada'), op: '+'});
    arr.push({val: getValFromTab('datos', especieClasif, 'faena'), op: '-'});
    arr.push({val: getValFromTab('datos', especieClasif, 'faena_gnd'), op: '-'});
    arr.push({val: getValFromTab('datos', especieClasif, 'reclasificacion_salida'), op: '-'});
    arr.push({val: getValFromTab('datos', especieClasif, 'abigeato'), op: '-'});

    arr.forEach(function (el, index) {
        if (el.op === '-')
            stock_final -= el.val;
        else
            stock_final += el.val;
    });
    // console.log(arr, tmp, stock_final);

    setValueForTab('calculados', especieClasif, 'stock_final', stock_final);
}

/**
 * Funcion llamada antes de eliminar una fila
 * que pone cero en el campo [<...>]_cantidad
 * para inducir la actualizacion del stock final.
 *
 * OBS: No esta en uso pero se deja como backup.
 *
 * @param {jquery}  button  Representa al boton para eliminar fila.
 * @param {string}  tab     Indica a que tabla corresponde el boton.
 */
function makeZeroBeforeDeleteRow(button, tab) {
    let prefix = '#' + getPrefijoId(button);
    let id = prefix + tab + '_cantidad';
    var e = $.Event("keyup");
    e.key = "0"; // tipear cero.
    $(id).val(0).trigger('change');
    $(id).trigger(e);
}

/**
 * Funcion auxiliar.
 *
 * Utilizado para construir ids de inputs en
 * calcularTotales(...).
 *
 * @param tipo_gasto    String que se quiere convertir.
 * @return {string}     String convertido.
 */
function mapTipoGasto(tipo_gasto) {
    let map = [{normal: '', gd: '_gd', gnd: '_gnd'}];

    let op = "";
    switch (tipo_gasto) {
        case '_gnd_':
            op = "_gnd";
            break;
        case "_gd_":
            op = "_gd";
            break;
        default:
            op = '';
    }

    return op;
}

function calcularTotales(input, tipo_gasto = "_") {
    // console.log(input);
    let prefix = '#' + getPrefijoId(input),
        id_total = prefix + 'total' + tipo_gasto + 'guaranies',
        regex = /new-([a-z]+)([\d]+)-/gm, // capturar en el primer grupo el concepto o tab: venta/consumo/mortandad/procreo.
        m = regex.exec(prefix),
        tab = m[1],
        id_cantidad = '#' + getPrefijoId(input) + tab + '_cantidad' + mapTipoGasto(tipo_gasto),
        cantidad = $(id_cantidad).val(),
        precio_foro = $(prefix + 'precio_foro').val();

    cantidad = (cantidad === "") ? 0 : parseInt(cantidad);

    let total = cantidad * precio_foro;

    // console.log(id_total, total);

    $(id_total).val(total);
}

/**
 * Al cambiar el PRECIO_FORO, redistribuye a todas las demas tablas
 * y recalcula todos los montos de TOTALES.
 */
$(document).on('keyup', ':input[id$="precio_foro"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        // calcular total
        calcularTotales($(this));
        calcularTotales($(this), '_gnd_');
        calcularTotales($(this), '_gd_');

        // Replicar el precio de foro a todas las demas tablas con la misma especie y clasific.
        let current = $(this);
        let especieClasif = getEspecieClasif(current);
        $(':input[id$="precio_foro"]')
            .not(document.getElementById(current.attr('id'))).each(function () {
            let especieClasif2 = getEspecieClasif($(this));

            if (especieClasif2[0] == especieClasif[0] && especieClasif2[1] == especieClasif[1]) {
                $(this).val(current.val()).trigger('change');
            }
        });
    }
}).on('change', ':input[id$="precio_foro"]', function () {
    calcularTotales($(this));
    calcularTotales($(this), '_gnd_');
    calcularTotales($(this), '_gd_');
});

// /**
//  * Actualiza el monto TOTAL correspondiente.
//  * 
//  * Obs: Se ha movido dentro de setEvent_prorrateo(...) pero se deja como backp.
//  */
// $(document).on('change', ':input[id$="cantidad"]', function () {
//     calcularTotales($(this));
// }).on('keyup', ':input[id$="cantidad"]', function (evt) {
//     if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
//         calcularTotales($(this));
//     }
// });

/**
 * Actualiza el monto TOTAL_GD correspondiente.
 */
$(document).on('change', ':input[id$="cantidad_gd"]', function () {
    calcularTotales($(this), '_gd_');
}).on('keyup', ':input[id$="cantidad_gd"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        calcularTotales($(this), '_gd_');
    }
});

/**
 * Actualiza el monto TOTAL_GND correspondiente.
 */
$(document).on('change', ':input[id$="cantidad_gnd"]', function () {
    calcularTotales($(this), '_gnd_');
}).on('keyup', ':input[id$="cantidad_gnd"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        calcularTotales($(this), '_gnd_');
    }
});

/**
 * Actualiza stock_final al cambiar ajuste_inventario
 */
$(document).on('change', ':input[id$="ajuste_inventario"]', function () {
    updateFinalStock($(this));
}).on('keyup', ':input[id$="ajuste_inventario"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        updateFinalStock($(this));
    }
});

/**
 * Actualiza stock_final al cambiar recoluta
 */
$(document).on('change', ':input[id$="recoluta"]', function () {
    updateFinalStock($(this));
}).on('keyup', ':input[id$="recoluta"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        updateFinalStock($(this));
    }
});

/**
 * Actualiza stock_final al cambiar reclasificacion_entrada
 */
$(document).on('change', ':input[id$="reclasificacion_entrada"]', function () {
    updateFinalStock($(this));
}).on('keyup', ':input[id$="reclasificacion_entrada"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        updateFinalStock($(this));
    }
});

/**
 * Actualiza stock_final al cambiar faena
 */
$(document).on('change', ':input[id$="faena"]', function () {
    updateFinalStock($(this));
}).on('keyup', ':input[id$="faena"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        updateFinalStock($(this));
    }
});

/**
 * Actualiza stock_final al cambiar faena_gnd
 */
$(document).on('change', ':input[id$="faena_gnd"]', function () {
    updateFinalStock($(this));
}).on('keyup', ':input[id$="faena_gnd"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        updateFinalStock($(this));
    }
});

/**
 * Actualiza stock_final al cambiar reclasificacion_salida
 */
$(document).on('change', ':input[id$="reclasificacion_salida"]', function () {
    updateFinalStock($(this));
}).on('keyup', ':input[id$="reclasificacion_salida"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        updateFinalStock($(this));
    }
});

/**
 * Actualiza stock_final al cambiar abigeato
 */
$(document).on('change', ':input[id$="abigeato"]', function () {
    updateFinalStock($(this));
}).on('keyup', ':input[id$="abigeato"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        updateFinalStock($(this));
    }
});

/**
 * Evento keydown y focusin en los campos de monto sin readonly y disabled para remover el cero a la izquierda.
 */
$(document).on('focusin keydown', ':input.monto:not([readonly]):not([disabled])', function (evt) {
    if ($(this).val() === 0)
        $(this).val('');
});

$(document).on('keyup', ':input[id$="faena_total"]', function (evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key)) {
        let prefix = '#' + getPrefijoId($(this)),
            especieClasif = getEspecieClasif($(this)),
            stock_actual = getValFromTab('datos-iniciales', especieClasif, 'stock_actual'),
            faena = $(this).val(),
            faena_gd = stock_actual * 0.01,
            faena_gnd = 0;
        if (faena > faena_gd) {
            faena_gnd = faena - faena_gd;
            faena = faena_gd;
        }

        $(prefix + 'faena').val(faena);
        $(prefix + 'faena_gnd').val(faena_gnd);
    }
});

$('a[data-toggle="tab"][id="datos-iniciales"], a[data-toggle="tab"][id="datos-entrada"]').on('click', function (e) {
    $('div[id="pjax_asiento_simulator"]').prop('style', 'display:none');
});

$('form#form-activo-biologico-cierre-inventario').on('afterValidate', function (event, messages, errorAttributes) {
    var scrollPosision = null;

    $(errorAttributes).each(function (key, attribute) {
        $('a#datos-entrada').click(); // por el momento solo en la pestanha de datos-entrada hay campos para validar
        // let firstErrorInput = $($(this)[0].input);
        // console.log(firstErrorInput);
        // $('html, body').animate({
        //     scrollTop: firstErrorInput.offset().top,//$("#target-element").offset().top
        // }, 500);
        return false; //break;
    });
});

$(document).ready(function () {
    // provoca 'change' en el precio_foro de la 1ra tabla para que recalcule
    // todos los montos 'TOTALES'.
    if ("{$action}" === 'update') {
        $(':input[id$="cantidad"]:not([value="0"]):not([value=""])').trigger('change'); // calcular total de aquella fila cuyo campo de cantidad principal no es cero ni vacio.
    } else {
        $(getSelectorForTab('procreo', '', 'procreo_cantidad')).trigger('change'); // al crear, todos los campos son ceros, pero igual tiene que calcular el stock final.
    }
    $('div[id="pjax_asiento_simulator"]').prop('style', 'display:none');
});
JS;
    $this->registerJs($scripts);
    ?>

</div>
