<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\search\ActivoBiologicoCierreInventarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activo-biologico-cierre-inventario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'especie_id') ?>

    <?= $form->field($model, 'clasificacion_id') ?>

    <?= $form->field($model, 'precio_foro') ?>

    <?= $form->field($model, 'procreo_cantidad') ?>

    <?php // echo $form->field($model, 'venta_cantidad') ?>

    <?php // echo $form->field($model, 'consumo_cantidad') ?>

    <?php // echo $form->field($model, 'mortandad_cantidad') ?>

    <?php // echo $form->field($model, 'consumo_porcentaje_gd') ?>

    <?php // echo $form->field($model, 'mortandad_porcentaje_gd') ?>

    <?php // echo $form->field($model, 'empresa_id') ?>

    <?php // echo $form->field($model, 'periodo_contable_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
