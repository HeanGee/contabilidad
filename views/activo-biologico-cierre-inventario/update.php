<?php

use backend\modules\contabilidad\models\EmpresaPeriodoContable;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\ActivoBiologicoCierreInventario */
/* @var $show_tab string */
/* @var $detalles \backend\modules\contabilidad\models\ActivoBiologicoCierreInventario[] */

$periodo = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'));
$this->title = "Modificar Cierre de Inventario de Activos Biológicos {$periodo->anho}";
$this->params['breadcrumbs'][] = ['label' => 'Inventario de A. Bio.', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Ver Datos', 'url' => ['view', 'id' => '']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="activo-biologico-cierre-inventario-update">

    <?= $this->render('_form', [
        'model' => $model,
        'show_tab' => $show_tab,
        'detalles' => $detalles,
        'topButtons' => $this->render('top-buttons'),
        'divClass' => 'panel panel-primary',
    ]) ?>

</div>
