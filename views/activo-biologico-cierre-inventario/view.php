<?php

use backend\modules\contabilidad\models\EmpresaPeriodoContable;
use yii\helpers\Html;

/* @var $this yii\web\View */

$periodo = EmpresaPeriodoContable::findOne(\Yii::$app->session->get('core_empresa_actual_pc'));
$this->title = "Inventario de A. Biológico del {$periodo->anho}";
$this->params['breadcrumbs'][] = ['label' => 'Inventario de A. Bio.', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

(isset($detalles) && !empty($detalles)) || $detalles = [];
?>
<div class="activo-biologico-cierre-inventario-view">

    <p>
        <?php $access = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-delete'),
        ]; ?>
        <?= $access['index'] ? Html::a('Ir a Cierre', ['index'], ['class' => 'btn btn-info']) : "" ?>
        <?= $access['update'] ? Html::a('Modificar', ['update', 'id' => ''], ['class' => 'btn btn-primary']) : "" ?>
        <?= $access['delete'] ? Html::a('Borrar', ['delete', 'id' => ''], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Está seguro de eliminar este ítem?',
                'method' => 'post',
            ],
        ]) : "" ?>
    </p>

    <?php try {
        echo $this->render('_form', [
            'model' => null,
            'show_tab' => 'datos-entrada',
            'detalles' => $detalles,
            'topButtons' => null,
            'divClass' => 'panel panel-info',
        ]);

        $script = <<<JS
$(document).ready(function() {
    $(':input').attr('disabled', true);
});
JS;
        $this->registerJs($script);

    } catch (Exception $exception) {
        \common\helpers\FlashMessageHelpsers::createWarningMessage("Error renderizando view: {$exception->getMessage()}");
    } ?>

</div>
