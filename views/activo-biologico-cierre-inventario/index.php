<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inventario de A. Bio.';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activo-biologico-cierre-inventario-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php $access = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('activo-biologico-cierre-inventario-delete'),
        ]; ?>
        <?= $access['create'] ? Html::a('Registrar Cierre', ['create'], ['class' => 'btn btn-success']) : "" ?>
    </p>

    <?php

    try {
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (\common\helpers\PermisosHelpers::getAcceso("activo-biologico-cierre-inventario-{$_v}"))
                $template .= "&nbsp;&nbsp;&nbsp;&nbsp;{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'fecha',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $exception) {
        \common\helpers\FlashMessageHelpsers::createWarningMessage("Error renderizando index: {$exception->getMessage()}");
    }
    ?>
</div>
