<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 05/12/2018
 * Time: 16:32
 */

use common\helpers\PermisosHelpers;
use yii\helpers\Html;

/* @var $this yii\web\View */

?>


<div class="form-group">
    <?= PermisosHelpers::getAcceso('contabilidad-activo-biologico-cierre-inventario-index') ?
        Html::a('Ir a Cierre de Inventario', ['index'], ['class' => 'btn btn-info']) : null ?>

    <?php if (Yii::$app->controller->action->id == 'create' &&
        PermisosHelpers::getAcceso('contabilidad-activo-biologico-cierre-inventario-create') ||
        Yii::$app->controller->action->id == 'update' &&
        PermisosHelpers::getAcceso('contabilidad-activo-biologico-cierre-inventario-update')) : ?>

        <?php
        $action = Yii::$app->controller->action->id;
        $submitBtn_text = ($action == 'create') ? 'Guardar' : 'Actualizar';
        $submitBtn_class = ($action == 'create') ? 'btn btn-success' : 'btn btn-primary';
        ?>

        <?= Html::submitButton($submitBtn_text, ['class' => $submitBtn_class]) ?>
    <?php endif; ?>
</div>
