<?php

use backend\modules\contabilidad\models\ActivoBiologico;
use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $especies array */
/* @var $clasif array */
/* @var $detalles ActivoBiologicoCierreInventario[] */

$action = Yii::$app->controller->action->id;
?>


<div class="activo-biologico-cierre-inventario-datos-procreo">

    <!--<fieldset>
        <legend>&nbsp;&nbsp;Agregar
            <div class="pull-left">
                <?php
                // new detalle button
                echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                    'id' => 'new-datos-procreo',
                    'class' => 'pull-left btn btn-success'
                ]);
                ?>
            </div>
        </legend>
    </fieldset>-->

        <?php

        echo '<table id="tabla-datos-procreo" class="table table-condensed table-responsive">';
        echo '<thead>';
        echo '<tr>';
        echo '<th style="text-align: center;">Especie</th>';
        echo '<th style="text-align: center;">Clasificación</th>';
        echo '<th style="text-align: center;">Precio Foro</th>';
        echo '<th style="text-align: center;">Nacimiento</th>';
        echo '<th style="text-align: center;">Total Gs.</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        \kartik\select2\Select2Asset::register($this);
        \yii\widgets\MaskedInputAsset::register($this);

        // existing detalles fields
        $skey = '';
        $key = 0;
        foreach ($detalles as $index => $detalle) {
            $key = $detalle->id != null ? $detalle->id - 1 : $index;
            echo '<tr class="fila-detalle">';
            echo $this->render('_tds_datos_procreo', [
                'key' => 'new-procreo' . ($key + 1),
                'form' => $form,
                'model' => $detalle,
            ]);
            echo '</tr>';
            $key++;
        }

        //        $html = $this->render('_tds_datos_procreo', [
        //            'key' => '__new-procreo__',
        //            'form' => $form,
        //            'model' => $model,
        //        ]);
        //
        //        // remover todos los \n \r y combinacion de ellos segun https://stackoverflow.com/questions/5258543/remove-all-the-line-breaks-from-the-html-source
        //        $output = str_replace(array("\r\n", "\r"), "\n", $html);
        //        $lines = explode("\n", $output);
        //        $substrings = array();
        //
        //        foreach ($lines as $i => $line) {
        //            if (!empty($line))
        //                $substrings[] = trim($line);
        //        }
        //        $html_without_enter_and_spaces = implode($substrings);
        $bluePrint = ""; //Json::encode($html_without_enter_and_spaces);

        echo '</tbody>';
        echo '</table>';

        \kartik\select2\Select2Asset::register($this);

        $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
        ?>

    <?php

    $script_head = <<<JS
var action_id = "$action";
var detalle_k_datos_procreo = $key;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);
    $script = <<<JS
//$('#new-datos-procreo').on('click', function () {
//    detalle_k_datos_procreo += 1;
//    $('#tabla-datos-procreo').find('tbody')
//        .append('<tr class="fila-detalle">' + ($bluePrint).replace(/__new-procreo__/g, 'new-procreo' + detalle_k_datos_procreo) + '</tr>');
//
//    construirSelect2(getSelectorForTab('procreo', detalle_k_datos_procreo, 'especie_id'), false, ($especies));
//    construirSelect2(getSelectorForTab('procreo', detalle_k_datos_procreo, 'clasificacion_id'), false, ($clasif));
//    construirMaskedInput(getSelectorForTab('procreo', detalle_k_datos_procreo, '', 'monto'), 'integer');
//});

// no se usa pero se deja como backup
// $(document).on('click', '.delete-detalle-datos-procreo', function () {
//     makeZeroBeforeDeleteRow($(this), 'procreo');
//     $(this).closest('tbody tr').remove();
// });

$(document).on('keyup', getSelectorForTab('procreo', '', 'procreo_cantidad'), function(evt) {
    if (!isNaN(evt.key) || ['Backspace', 'Delete'].includes(evt.key))
        if (is_especieClasif_selected($(this))) {
            updateFinalStock($(this));
            calcularTotales($(this));
        }
}).on('change', getSelectorForTab('procreo', '', 'procreo_cantidad'), function() {
    if (is_especieClasif_selected($(this))) {
        updateFinalStock($(this));
        calcularTotales($(this));
    }
});

especieClasific = [];
especieClasific.push(getSelectorForTab('procreo', '', 'especie_id'));
especieClasific.push(getSelectorForTab('procreo', '', 'clasificacion_id'));
especieClasific.forEach(function (e, i) {
    $(document).on('change', e, function () {
        if (is_especieClasif_selected($(this)))
            updateFinalStock($(this));
    });
});
delete especieClasific;

$(document).ready(function () {
    construirSelect2(getSelectorForTab('procreo', "", 'especie_id'), true, ($especies));
    construirSelect2(getSelectorForTab('procreo', "", 'clasificacion_id'), true, ($clasif));
    construirMaskedInput(getSelectorForTab('procreo', '', '', 'monto'), 'integer');
})
JS;
    $this->registerJs($script);
    ?>

</div>
