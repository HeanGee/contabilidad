<?php

use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \backend\modules\contabilidad\models\activobiologico */
?>
<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'especie_id')->textInput([
        'id' => "activobiologicocierreinventarioprocreo-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioProcreo[$key][especie_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'especie_id')->hiddenInput([
        'id' => "activobiologicocierreinventarioprocreo-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioProcreo[$key][especie_id]",
    ])->label(false) ?>
</td>

<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'clasificacion_id')->textInput([
        'id' => "activobiologicocierreinventarioprocreo-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioProcreo[$key][clasificacion_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'clasificacion_id')->hiddenInput([
        'id' => "activobiologicocierreinventarioprocreo-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioProcreo[$key][clasificacion_id]",
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'precio_foro')->textInput([ // tal vez sea precio foro
        'id' => "activobiologicocierreinventarioprocreo-{$key}-precio_foro",
        'name' => "ActivoBiologicoCierreInventarioProcreo[$key][precio_foro]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'procreo_cantidad')->textInput([
        'id' => "activobiologicocierreinventarioprocreo-{$key}-procreo_cantidad",
        'name' => "ActivoBiologicoCierreInventarioProcreo[$key][procreo_cantidad]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_guaranies')->textInput([
        'id' => "activobiologicocierreinventarioprocreo-{$key}-total_guaranies",
        'name' => "ActivoBiologicoCierreInventarioProcreo[$key][total_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<!--<td class=" " style="text-align: center">-->
<?= ""/*Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'delete-detalle-datos-procreo btn btn-danger btn-sm',
        'id' => "activobiologicocierreinventarioprocreo-{$key}-delete_button",
        'title' => 'Eliminar fila',
    ])*/ ?>
<!--</td>-->