<?php

use backend\modules\contabilidad\models\ActivoBiologico;
use kartik\form\ActiveForm;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $especies string */
/* @var $clasif string */

$action = Yii::$app->controller->action->id;
?>


<div class="activo-biologico-cierre-inventario-datos-iniciales">

    <fieldset>
        <legend>&nbsp;Inventario Inicial
            <div class="pull-right">
                <?php
                // new detalle button
                //                echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
                //                    'id' => 'new-detalle-datos-iniciales',
                //                    'class' => 'pull-left btn btn-success'
                //                ])
                //                ?>
            </div>
        </legend>
    </fieldset>
        <?php

        echo '<table id="tabla-datos-iniciales" class="table table-condensed table-responsive">';
        echo '<thead>';
        echo '<tr>';
        echo '<th style="text-align: center;">Especie</th>';
        echo '<th style="text-align: center;">Clasificación</th>';
        echo '<th style="text-align: center;">Stock Inicial</th>';
        echo '<th style="text-align: center;">Precio Unitario Inicial</th>';
        echo '<th style="text-align: center;">Total Gs.</th>';
        echo '<th style="text-align: center;">Stock Actual</th>';
        echo '<th style="text-align: center;">Precio Unitario Actual</th>';
        echo '<th style="text-align: center;">Total Gs.</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        \kartik\select2\Select2Asset::register($this);
        \yii\widgets\MaskedInputAsset::register($this);
        // existing detalles fields
        $skey = '';
        $detalles = ActivoBiologico::find()->where([
            'empresa_id' => \Yii::$app->session->get('core_empresa_actual'),
            'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')])->all();
        $key = 0;
        foreach ($detalles as $index => $detalle) {
            $key = $detalle->id != null ? $detalle->id - 1 : $index;
            echo '<tr class="fila-detalle">';
            echo $this->render('_tds_datos_iniciales', [
                'key' => 'new' . ($key + 1),
                'form' => $form,
                'model' => $detalle,
            ]);
            echo '</tr>';
            $key++;
        }

        echo '<tr id="fila-sumary" style="display: ;">';
        {
            echo '<td></td>';
            echo '<td></td>';
            echo '<td></td>';
            echo '<td></td>';
            echo '<td id="td-total-inicial"></td>';
            echo '<td></td>';
            echo '<td></td>';
            echo '<td id="td-total-actual"></td>';
        }
        echo '</tr>';

        echo '</table>';

        \kartik\select2\Select2Asset::register($this);

        $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
        ?>

    <!--    </fieldset>-->

    <?php

    $script_head = <<<JS
var action_id = "$action";
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);
    $script = <<<JS
function calcularTotalDatosIniciales() {
    let total_inicial = 0;
    let total_actual = 0;
    let selector = getSelectorForTab('datos-iniciales', '', 'especie_id');
    $(selector).each(function () {
        // console.log($(this).prop('id'));
        let prefix = '#' + getPrefijoId($(this));

        let stk = $(prefix + 'stock_actual').val();
        let pu = $(prefix + 'precio_unitario_actual').val();
        let tot = stk * pu;
        total_actual += tot;
        $(prefix + 'total').val(tot).trigger('change');

        stk = $(prefix + 'stock_inicial').val();
        pu = $(prefix + 'precio_unitario').val();
        tot = stk * pu;
        total_inicial += tot;
        $(prefix + 'total_inicial').val(tot).trigger('change');
        
        // console.log(prefix, total_actual, total_inicial);
    });

    // .text() funciona tanto para html como para XML segun
    // https://stackoverflow.com/questions/23013484/jquery-set-value-of-td-in-a-table
    let td_tot_inicial = $('td#td-total-inicial');
    let td_tot_actual = $('td#td-total-actual');

    total_actual = split(total_actual, 3);
    total_inicial = split(total_inicial, 3);

    td_tot_actual.text(total_actual);
    td_tot_inicial.text(total_inicial);

    let css = {
        "text-align": "right",
        "padding": "5px 18px 5px 0",
        "background-color": "#0a9629",
        "color": "white",
        'font-weight': 'bold'
    };
    td_tot_actual.css(css);
    td_tot_inicial.css(css);
}

/**
 *  Agrupa el valor del input en grupos de 3 caracteres.
 *  
 * @param input
 * @param len
 * @returns {string}
 */
function split(input, len) {
    input = input.toString();
    return input.match(new RegExp('.{1,' + len + '}(?=(.{' + len + '})+(?!.))|.{1,' + len + '}$', 'g')).join('.');
}

$(document).ready(function () {
    construirSelect2(getSelectorForTab('datos-iniciales', '', 'especie_id'), true, ($especies));
    construirSelect2(getSelectorForTab('datos-iniciales', '', 'clasificacion_id'), true, ($clasif));
    construirMaskedInput(getSelectorForTab('datos-iniciales', '', '', 'monto'), 'integer');
    calcularTotalDatosIniciales();
})
JS;
    $this->registerJs($script);
    ?>

</div>
