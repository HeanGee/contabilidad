<?php

use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \backend\modules\contabilidad\models\activobiologico */
?>
<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'especie_id')->textInput([
        'id' => "activobiologico-{$key}-especie_id",
        'name' => "ActivoBiologico[$key][especie_id]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'especie_id')->hiddenInput([
        'id' => "activobiologico-{$key}-especie_id",
        'name' => "ActivoBiologico[$key][especie_id]",
    ])->label(false) ?>
</td>

<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'clasificacion_id')->textInput([
        'id' => "activobiologico-{$key}-clasificacion_id",
        'name' => "ActivoBiologico[$key][clasificacion_id]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'clasificacion_id')->hiddenInput([
        'id' => "activobiologico-{$key}-clasificacion_id",
        'name' => "ActivoBiologico[$key][clasificacion_id]",
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($model, 'stock_inicial')->textInput([
        'id' => "activobiologico-{$key}-stock_inicial",
        'name' => "ActivoBiologico[$key][stock_inicial]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'precio_unitario')->textInput([
        'id' => "activobiologico-{$key}-precio_unitario",
        'name' => "ActivoBiologico[$key][precio_unitario]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'total_inicial')->textInput([
        'id' => "activobiologico-{$key}-total_inicial",
        'name' => "ActivoBiologico[$key][total_inicial]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'stock_actual')->textInput([
        'id' => "activobiologico-{$key}-stock_actual",
        'name' => "ActivoBiologico[$key][stock_actual]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'precio_unitario_actual')->textInput([
        'id' => "activobiologico-{$key}-precio_unitario_actual",
        'name' => "ActivoBiologico[$key][precio_unitario_actual]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'total')->textInput([
        'id' => "activobiologico-{$key}-total",
        'name' => "ActivoBiologico[$key][total]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>