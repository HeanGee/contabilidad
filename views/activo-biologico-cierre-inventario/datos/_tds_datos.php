<?php

use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \backend\modules\contabilidad\models\activobiologico */
?>
<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'especie_id')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][especie_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'especie_id')->hiddenInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][especie_id]",
    ])->label(false) ?>
</td>

<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'clasificacion_id')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][clasificacion_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'clasificacion_id')->hiddenInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][clasificacion_id]",
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'ajuste_inventario')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-ajuste_inventario",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][ajuste_inventario]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'recoluta')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-recoluta",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][recoluta]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'reclasificacion_entrada')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-reclasificacion_entrada",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][reclasificacion_entrada]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'faena_total')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-faena_total",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][faena_total]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'faena')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-faena",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][faena]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'faena_gnd')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-faena_gnd",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][faena_gnd]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'reclasificacion_salida')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-reclasificacion_salida",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][reclasificacion_salida]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'abigeato')->textInput([
        'id' => "activobiologicocierreinventariodatos-{$key}-abigeato",
        'name' => "ActivoBiologicoCierreInventarioDatos[$key][abigeato]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>