<?php

use backend\modules\contabilidad\models\ActivoBiologicoCierreInventario;
use backend\modules\contabilidad\models\ActivoBiologicoEspecie;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $especies array */
/* @var $clasif array */
/* @var $detalles ActivoBiologicoCierreInventario[] */

$action = Yii::$app->controller->action->id;
?>


<div class="activo-biologico-cierre-inventario-datos">

    <!--<fieldset>
        <legend>&nbsp;&nbsp;Agregar
            <div class="pull-left">
                <?php
    // new detalle button
    echo Html::a('<i class="glyphicon glyphicon-plus"></i>', 'javascript:void(0);', [
        'id' => 'new-datos',
        'class' => 'pull-left btn btn-success'
    ]);
    ?>
            </div>
        </legend>
    </fieldset>-->

    <?php

    echo '<table id="tabla-datos" class="table table-condensed table-responsive">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="text-align: center;">Especie</th>';
    echo '<th style="text-align: center;">Clasificación</th>';
    echo '<th style="text-align: center;">Ajuste Inventario</th>';
    echo '<th style="text-align: center;">Ingresos Recoluta</th>';
    echo '<th style="text-align: center;">Reclasificaciones Entrada</th>';
    echo '<th style="text-align: center;">Faena</th>';
    echo '<th style="text-align: center;">Faena GND</th>';
    echo '<th style="text-align: center;">Reclasificaciones Salida</th>';
    echo '<th style="text-align: center;">Abigeato</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    \kartik\select2\Select2Asset::register($this);
    \yii\widgets\MaskedInputAsset::register($this);

    $query = ActivoBiologicoEspecie::find();
    $query->select(['id', 'nombre as text']);
    $query->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
    $especies = [['id' => '', 'text' => ''],];
    $especies = array_merge($especies, $query->asArray()->all());
    $especies = \yii\helpers\Json::encode($especies);

    $query = \backend\modules\contabilidad\models\ActivoBiologicoEspecieClasificacion::find();
    $query->select(['id', 'nombre as text']);
    $query->where(['empresa_id' => \Yii::$app->session->get('core_empresa_actual'), 'periodo_contable_id' => \Yii::$app->session->get('core_empresa_actual_pc')]);
    $clasif = [['id' => '', 'text' => ''],];
    $clasif = array_merge($clasif, $query->asArray()->all());
    $clasif = \yii\helpers\Json::encode($clasif);

    // existing detalles fields
    $skey = '';
    $key = 0;
    foreach ($detalles as $index => $detalle) {
        $key = $detalle->id != null ? $detalle->id - 1 : $index;
        echo '<tr class="fila-detalle">';
        echo $this->render('_tds_datos', [
            'key' => 'new-datos' . ($key + 1),
            'form' => $form,
            'model' => $detalle,
        ]);
        echo '</tr>';
        $key++;
    }

    //    $html = $this->render('_tds_datos_venta', [
    //        'key' => '__new-venta__',
    //        'form' => $form,
    //        'model' => $model,
    //    ]);
    //
    //    // remover todos los \n \r y combinacion de ellos segun https://stackoverflow.com/questions/5258543/remove-all-the-line-breaks-from-the-html-source
    //    $output = str_replace(array("\r\n", "\r"), "\n", $html);
    //    $lines = explode("\n", $output);
    //    $substrings = array();
    //
    //    foreach ($lines as $i => $line) {
    //        if (!empty($line))
    //            $substrings[] = trim($line);
    //    }
    //    $html_without_enter_and_spaces = implode($substrings);
    $bluePrint = "";//Json::encode($html_without_enter_and_spaces);

    echo '</tbody>';
    echo '</table>';

    \kartik\select2\Select2Asset::register($this);

    $url = Json::htmlEncode(\Yii::t('app', Url::to([''])));
    ?>

    <?php

    $script_head = <<<JS
var action_id = "$action";
var detalle_k_datos = $key;
JS;
    $this->registerJs($script_head, \yii\web\View::POS_HEAD);
    $script = <<<JS
// $(document).on('keyup', getSelectorForTab('venta', '', 'venta_cantidad'), function(evt) {
//     // setProrrateoVentaEvent($(this), evt);
//     setEvent_prorrateo($(this), 'venta', evt);
// }).on('change', getSelectorForTab('venta', '', 'venta_cantidad'), function() {
//     // setProrrateoVentaEvent($(this), evt);
//     setEvent_prorrateo($(this), 'venta');
// });
//
// especieClasific = [];
// especieClasific.push(getSelectorForTab('venta', '', 'especie_id'));
// especieClasific.push(getSelectorForTab('venta', '', 'clasificacion_id'));
// especieClasific.forEach(function (e, i) {
//     $(document).on('change', e, function () {
//         // setProrrateoVentaEvent($(this), null);
//         setEvent_prorrateo($(this), 'venta');
//     });
// });
// delete especieClasific;

/**
 * Calcula la faena GND.
 *
 * @param contains   {string}    Subcadena del prefijo del input en el que se acaba de escribir que lo identifica univocamente.
 * @param input      {jquery}    Representa el input para venta en el que se acaba de escribir.
 */
function prorratearFaena(contains = '', input) {
    console.log('prorrateo de faena en proceso de definicion.');
    // let selector = getSelectorForTab('venta', contains, 'venta_cantidad');
    // $(selector)
    //     .each(function () {
    //         let especieClasificacion = getEspecieClasif(input);
    //         especieClasificacion.push(parseInt($(especie_id).val()));
    //         especieClasificacion.push(parseInt($(clasificacion_id).val()));
    //         let campo = 'stock_actual';
    //
    //         if (isNaN(especieClasificacion[0]) || isNaN(especieClasificacion[1]))
    //             return false;
    //
    //         let venta_permitida = 0.03;
    //         let stock_actual = getValFromDatosIniciales(especieClasificacion, campo);
    //         let venta = input.val();
    //         // console.log(stock_actual);
    //
    //         if (stock_actual) {
    //             let venta_cantidad_gd = Math.round(stock_actual * venta_permitida);
    //             let venta_cantidad_gnd = venta - venta_cantidad_gd;
    //             if (venta <= venta_cantidad_gd) {
    //                 venta_cantidad_gd = venta;
    //                 venta_cantidad_gnd = 0;
    //             }
    //             $(prefix + 'venta_cantidad_gd').val(venta_cantidad_gd).trigger('change');
    //             $(prefix + 'venta_cantidad_gnd').val(venta_cantidad_gnd).trigger('change');
    //         } else {
    //             $(prefix + 'venta_cantidad_gd').val('').trigger('change');
    //             $(prefix + 'venta_cantidad_gnd').val('').trigger('change');
    //         }
    //     });
}

$(document).ready(function() {
    construirSelect2(getSelectorForTab('datos', '', 'especie_id'), true, ($especies));
    construirSelect2(getSelectorForTab('datos', '', 'clasificacion_id'), true, ($clasif));
    construirMaskedInput(getSelectorForTab('datos', '', '', 'monto'), 'integer');
})
JS;
    $this->registerJs($script);
    ?>

</div>
