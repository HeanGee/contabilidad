<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 10/10/2018
 * Time: 13:54
 */

use common\helpers\FlashMessageHelpsers;
use kartik\grid\GridView;
use kartik\number\NumberControl;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $panel_color string */
isset($panel_color) || $panel_color = 'success';
?>

<fieldset>
    <legend class="text-info">&nbsp;&nbsp;Asientos</legend>
    <?php
    try {
        $skey = 'cont_asientodetalle_abiocierre';
        $session = Yii::$app->session;

        $asientoData = $session->get($skey, []);

        foreach ($asientoData as $tablaNombre => $data) {
            $allModels = $data['allModels'];
            if (empty($allModels)) continue;
            $tablaNombre = strtoupper($tablaNombre);

            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $allModels,
                'pagination' => false
            ]);
            echo GridView::widget([
                'id' => 'grid-detalles',
                'dataProvider' => $dataProvider,
                'toolbar' => [],
                'hover' => true,
                'panel' => [
                    'type' => $panel_color,
                    'format' => 'raw',
                    'heading' => "<strong>Asiento por {$tablaNombre}</strong>",
                    'footerOptions' => ['class' => ''],
                    'beforeOptions' => ['class' => ''],
                    'afterOptions' => ['class' => ''],
                ],
                'panelFooterTemplate' => '',
                'columns' => [
                    [
                        'label' => 'Código de cuenta',
                        'value' => 'cuenta.cod_completo',
                        'contentOptions' => ['style' => 'padding:8px 0px 0px 15px; text-align:left'],
                    ],
                    [
                        'label' => 'Nombre de cuenta',
                        'value' => 'cuenta.nombre'
                    ],
                    [
                        'label' => 'Debe',
                        'value' => 'montoDebeFormatted',
                        'contentOptions' => ['style' => 'width: 300; text-align:right'],
                    ],
                    [
                        'label' => 'Haber',
                        'value' => 'montoHaberFormatted',
                        'contentOptions' => ['style' => 'width: 300; text-align:right'],
                    ],
                ],
            ]);

            $totaldebe = $data['debe'];
            $totalhaber = $data['haber'];

            echo Html::beginTag('div', ['class' => 'parent']);
            echo Html::beginTag('div', ['class' => 'float-right child']);
            echo Html::label('Total Haber: ', 'total-haber-disp');
            echo NumberControl::widget([
                'name' => "total-haber-{$tablaNombre}",
                'id' => "total-haber-{$tablaNombre}",
                'readonly' => true,
                'value' => $totalhaber,
                'class' => 'form-control',
                'maskedInputOptions' => [
                    'prefix' => '₲ ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 0
                ],
                'displayOptions' => [
                    'class' => 'form-control kv-monospace float-right',
                    'placeholder' => 'Total Haber...',
                    'style' => [
//                'width' => '34.9%',
//                'margin' => '0 auto',
//                'text-align' => 'right',
//                'clear' => 'right',
//                'float' => 'right',
//                'position' => 'relative',
                        'font-weight' => 'bold',
                    ]
                ]
            ]);

            echo Html::endTag('div');
            echo Html::beginTag('div', ['class' => 'float-right child']);
            echo Html::label('Total Debe: ', 'total-debe-disp');
            echo NumberControl::widget([
                'name' => "total-debe-{$tablaNombre}",
                'id' => "total-debe-{$tablaNombre}",
                'readonly' => true,
                'value' => $totaldebe,
                'class' => 'form-control',
                'maskedInputOptions' => [
                    'prefix' => '₲ ',
                    'groupSeparator' => '.',
                    'radixPoint' => ',',
                    'digits' => 0
                ],
                'displayOptions' => [
                    'class' => 'form-control kv-monospace float-right',
                    'placeholder' => 'Total Debe...',
                    'style' => [
                        'font-weight' => 'bold',
                    ]
                ]
            ]);
            echo Html::endTag('div');
            echo Html::tag('br');
            echo Html::tag('br');
            echo Html::tag('br');
            echo Html::tag('br');
            echo Html::endTag('div');
        }

//        try {
//            if (Yii::$app->request->isPjax)
//                ActivoBiologicoCierreInventario::isCuentasForAsiento();
//        } catch (\yii\base\Exception $exception) {
//            FlashMessageHelpsers::createWarningMessage($exception->getMessage(), 12000);
//        }

    } catch (Exception $exception) {
        $msg = "Error desde _simulador_asiento.php: {$exception}";
        echo $msg;
        FlashMessageHelpsers::createWarningMessage($msg);
    }

    $CSS = <<<CSS
.float-right {
  float: right;
}
.child {
  /*border: 1px solid indigo;*/
  padding-left: 1rem;
}
CSS;
    $this->registerCss($CSS);

    $script = <<<JS
/**
 *  Alinea nombre de cuenta a la derecha y agrega prefijo 'a ' si es para haber.
 */
$('table.kv-grid-table td:nth-child(3)').filter(function () {
    return $(this).text() === "0";
}).each(function (i, td) {
    let ctaNombreCell = $(td).closest('td').prev('td');
    ctaNombreCell.prop('align', 'right');
    ctaNombreCell.html('a ' + ctaNombreCell.html());
});
JS;
    $this->registerJs($script);
    ?>
</fieldset>
