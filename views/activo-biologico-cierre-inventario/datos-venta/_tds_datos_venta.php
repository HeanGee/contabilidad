<?php

use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \backend\modules\contabilidad\models\activobiologico */
?>
<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'especie_id')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][especie_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'especie_id')->hiddenInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-especie_id",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][especie_id]",
    ])->label(false) ?>
</td>

<td width="9%; !important" style="font-weight: bold;">
    <?= $form->field($model, 'clasificacion_id')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][clasificacion_id]",
        'class' => "form-control",
        'style' => 'text-align: right;',
    ])->label(false) ?>
    <?= $form->field($model, 'clasificacion_id')->hiddenInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-clasificacion_id",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][clasificacion_id]",
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'precio_foro')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-precio_foro",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][precio_foro]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'venta_cantidad')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-venta_cantidad",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][venta_cantidad]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'venta_cantidad_gd')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-venta_cantidad_gd",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][venta_cantidad_gd]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
//        'readonly' => true,
    ])->label(false) ?>
</td>

<td>
    <?= $form->field($model, 'venta_cantidad_gnd')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-venta_cantidad_gnd",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][venta_cantidad_gnd]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
//        'readonly' => true,
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_guaranies')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-total_guaranies",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][total_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_gd_guaranies')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-total_gd_guaranies",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][total_gd_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<td style="font-weight: bold;">
    <?= $form->field($model, 'total_gnd_guaranies')->textInput([
        'id' => "activobiologicocierreinventariocostoventa-{$key}-total_gnd_guaranies",
        'name' => "ActivoBiologicoCierreInventarioCostoVenta[$key][total_gnd_guaranies]",
        'class' => "form-control monto",
        'style' => 'text-align: right;',
        'readonly' => true,
    ])->label(false) ?>
</td>

<!--<td class=" " style="text-align: center">-->
<?= ""/*Html::a('<i class="glyphicon glyphicon-trash"></i>', 'javascript:void(0);', [
        'class' => 'delete-detalle-datos-venta btn btn-danger btn-sm',
        'id' => "activobiologicocierreinventariocostoventa-{$key}-delete_button",
        'title' => 'Eliminar fila',
    ]) */ ?>
<!--</td>-->