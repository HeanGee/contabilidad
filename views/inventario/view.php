<?php

use backend\modules\contabilidad\models\PolizaDevengamiento;
use common\helpers\FlashMessageHelpsers;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Poliza */

$this->title = 'Póliza ' . $model->nro_poliza;
$this->params['breadcrumbs'][] = ['label' => 'Pólizas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poliza-view">

    <p>
        <?= Html::a('Modificar Póliza', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar esta Póliza', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Realmente desea borrar esta Póliza?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos principales',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'attribute' => 'fecha_desde',
                    'value' => date('d-m-Y', strtotime($model->fecha_desde))
                ],
                [
                    'attribute' => 'fecha_hasta',
                    'value' => date('d-m-Y', strtotime($model->fecha_hasta))
                ],
                'nro_poliza',
                [
                    'label' => 'Sección',
                    'value' => isset($model->seccion) ? $model->seccion->nombre : "-no definido-",
                ],
            ],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => PolizaDevengamiento::find()->where(['poliza_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => 'Devengamientos',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'columns' => [
                [
                    'label' => 'Mes Año',
                    'value' => function ($model, $key, $index, $column) {
                        return date_create_from_format('Y-m', $model->anho_mes)->format('m-Y');
                    },
                ],
                [
                    'label' => 'Días',
                    'value' => 'dias'
                ],
                [
                    'label' => 'Importe',
                    'value' => function ($model, $key, $index, $column) {
                        return number_format($model->monto, '0', ',', '.');
                    },
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],

                ],
                [
                    'label' => '¿Asentado?',
                    'value' => function ($model, $key, $index, $column) {
                        return isset($model->asiento) ? 'Si' : 'No';
                    },
                ],
            ],
        ]);

        // Facturas relacionadas
        echo $this->render('@backend/modules/contabilidad/views/facturas-relacionadas/_facturas_relacionadas', ['model' => $model]);
    } catch (Exception $exception) {
        FlashMessageHelpsers::createWarningMessage($exception);
    } ?>

</div>
