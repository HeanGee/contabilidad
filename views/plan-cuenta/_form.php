<?php

use backend\modules\contabilidad\models\PlanCuenta;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlanCuenta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plan-cuenta-form">

    <?php
        $form = ActiveForm::begin();

        try{
            if(!empty($model->id))
                $model->es_global = empty($model->empresa_id) ? 'si' : 'no';

            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'padre_id' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '6'],
                                'value' => $form->field($model, 'padre_id')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione...'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'data' => PlanCuenta::getCuentaLista()
                                    ],
                                    'initValueText' => !empty($model->padre_id) ? ($model->padre->cod_completo.' - '.$model->padre->nombre) : '',
                                    'pluginEvents' => [
                                        'change' => "function(){
                                            manejar_cambio();
                                        }"
                                    ]
                                ])
                            ],
                            'es_global' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '3'],
                                'value' => $form->field($model, 'es_global')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione...'],
                                    'pluginOptions' => [
                                        'data' => PlanCuenta::getValoresEnum('asentable', true) // tomo nomás igual este
                                    ],
                                    'hideSearch' => true,
                                    'initValueText' => !empty($model->es_global) ? ucfirst($model->es_global) : '',
                                    'pluginEvents' => [
                                        'change' => "function(){
                                            manejar_cambio();
                                        }"
                                    ]
                                ])
                            ],
                            'cod_cuenta' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '3'],
                                'options' => [
                                    'placeholder ' => 'Código...',
                                    'readonly' => (Yii::$app->controller->action->id == 'create') ? false : true,
                                ],
                            ]
                        ]
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'nombre' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '6'],
                                'options' => [ 'placeholder ' => 'Nombre...' ]
                            ],
                            'asentable' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '1'],
                                'value' => $form->field($model, 'asentable')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione...'],
                                    'pluginOptions' => [
                                        'data' => PlanCuenta::getValoresEnum('asentable', true)
                                    ],
                                    'hideSearch' => true,
                                    'initValueText' => !empty($model->asentable) ? ucfirst($model->asentable) : ''
                                ])
                            ],
                            'estado' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'estado')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione...'],
                                    'pluginOptions' => [
                                        'data' => PlanCuenta::getValoresEnum('estado', true)
                                    ],
                                    'hideSearch' => true,
                                    'initValueText' => !empty($model->estado) ? ucfirst($model->estado) : ''
                                ])
                            ],
                            'flujo_efectivo' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'flujo_efectivo')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione...'],
                                    'pluginOptions' => [
                                        'data' => PlanCuenta::getValoresEnum('flujo_efectivo', true, true)
                                    ],
                                    'hideSearch' => false,
                                    'initValueText' => !empty($model->flujo_efectivo) ? ucfirst(str_replace('_', ' ', $model->flujo_efectivo)) : ''
                                ])
                            ]
                        ]
                    ]
                ]
            ]);

        }catch (\Exception $e){
//            print $e;
        }

        $url = \yii\helpers\Url::to(['get-siguiente']);
        $script = <<<JS
function manejar_cambio() {
    var padre_id = $('#plancuenta-padre_id').val();
    var es_global = $('#plancuenta-es_global').val();
    if(es_global != ''){
        var data = { es_global: es_global };
        if(padre_id != '')
            data['cuenta_id'] = padre_id;
        $.ajax({
            type: "POST",
            url: "{$url}",
            data: data
        }).done(function( data ) {
            var elemento = $('#plancuenta-cod_cuenta');
            try{
                var response = JSON.parse(data);
                if(parseInt(response['siguiente']) < 10)
                    response['siguiente'] = '0' + response['siguiente'];
                elemento.val(response['siguiente']);
            }catch(ex){
                elemento.val('');
            }
        });
    }
}
JS;
        $this->registerJs($script);

    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
