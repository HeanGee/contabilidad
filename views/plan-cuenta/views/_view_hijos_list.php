<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 21/12/18
 * Time: 01:51 PM
 */

use common\helpers\FlashMessageHelpsers;
use kartik\grid\GridView;

/* @var $this yii\web\View */
?>

<?php
try {
    \yii\widgets\Pjax::begin(['id' => 'pjax_hijos_cuenta']);
    $dataProvider = Yii::$app->session->get('dataProviderHijos');
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'toolbar' => [],
        'hover' => true,
        'panel' => [
            'type' => 'info',
            'heading' => "Hijos",
            'footerOptions' => ['class' => ''],
            'beforeOptions' => ['class' => ''],
            'afterOptions' => ['class' => '']
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'cod_ordenable',
                'value' => function ($model, $key, $index, $column) {
                    return $model->cod_completo;
                }
            ],
            'cod_completo',
            'cod_cuenta',
            'nombre',
            [
                'attribute' => 'asentable',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<label class="label label-' . (($model->asentable == 'si') ? 'success">Sí' : 'danger">No') . '</label>';
                },
                'contentOptions' => ['style' => 'text-align:center;'],
            ],
            'padre_id',
            [
                'attribute' => 'estado',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                },
                'contentOptions' => ['style' => 'text-align:center;'],
            ],
        ],
    ]);
    echo "</fieldset>";
    \yii\widgets\Pjax::end();

} catch (Exception $exception) {
    FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    echo $exception;
}
