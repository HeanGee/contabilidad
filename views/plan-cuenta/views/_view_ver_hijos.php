<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 21/12/18
 * Time: 01:45 PM
 */

use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\PermisosHelpers;
use kartik\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

$this->title = 'Visualizador de Hijos';
$this->params['breadcrumbs'][] = ['label' => 'Plan de Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php try {
    echo '<div class="form-group">';
    echo PermisosHelpers::getAcceso('contabilidad-plan-cuenta-index') ?
        Html::a('Ir a Cuentas', ['index'], ['class' => 'btn btn-info']) : null;
    echo '</div>';
    echo '<div class="form-group">';
    echo Html::label('Cuenta Padre', 'padre_id');
    echo Select2::widget([
        'name' => 'padre_id',
        'id' => 'padre_id',
        'data' => ArrayHelper::map(PlanCuenta::getCuentaLista(false), 'id', 'text'),
        'pluginOptions' => [
//            'allowClear' => true,
//            'width' => '20%',
        ],
    ]);
    echo '</div>';

    echo '<div class="form-group">';
    echo $this->render('_view_hijos_list');
    echo '</div>';

    $url = \yii\helpers\Url::to(['generate-hijos']);
    $script = <<<JS
$('#padre_id').change(function() {
    $.ajax({
        url: "{$url}",
        type: 'get',
        data: {padre_id: $('#padre_id').val()},
        success: function (data) {
            console.log("en success");
            $.pjax.reload({container:"#pjax_hijos_cuenta", async:false});
            $.pjax.reload({container:"#flash_message_id", async:false});
        }
    });
});
$(document).ready(function() {
    $('#padre_id').trigger('change');
});
JS;
    $this->registerJs($script);

} catch (Exception $exception) {
    \common\helpers\FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    echo $exception;
}
