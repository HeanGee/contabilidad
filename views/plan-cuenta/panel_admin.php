<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 09/01/19
 * Time: 04:54 PM
 */

use kartik\helpers\Html;

?>

<div class="text-right btn-toolbar">
    <?php
    $btn1 = Html::a('Arbol', ['arbol'], ['class' => 'btn btn-arbol btn-warning btn-xs']);
    $btn2 = Html::a('Visualizador de Cuentas Hijos', ['get-hijos'], ['class' => 'btn btn-warning btn-xs']);
    echo $btn2;
    echo $btn1;
    ?>
</div>
