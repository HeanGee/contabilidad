<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 02/10/2018
 * Time: 9:20
 */

use backend\modules\contabilidad\models\CuentaPrestamoSelector;
use backend\modules\contabilidad\models\PlanCuenta;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model CuentaPrestamoSelector */
?>

<div class="modal-add-cuenta">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => '_modal_add_cuenta',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <?php try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [       // 2 column layout
                'concepto' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => CuentaPrestamoSelector::conceptosLista(),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno',
                        ],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'hint' => 'Seleccione una opcion',
                    'label' => 'Para'
                ],
                'cuenta_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno',
                        ],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'hint' => 'Seleccione una cuenta',
                    'label' => 'Cuenta'
                ],
            ]
        ]);
    } catch (Exception $e) {
        print $e->getMessage();
    } ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['data' => ['disabled-text' => 'Guardando...'], 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $scripts = [];

    $scripts[] = <<<JS
// obtener la id del formulario y establecer el manejador de eventos
$("form#_modal_add_cuenta").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#grid_cuentas_prestamo", async: false});
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$('#modal').on('shown.bs.modal', function () {
    let element = $('#cuentaprestamoselector-concepto');
    if (element.val().length === 0)
        element.select2('open');
});
JS;

    foreach ($scripts as $script) $this->registerJs($script);
    ?>
</div>