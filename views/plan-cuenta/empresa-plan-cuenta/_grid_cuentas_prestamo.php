<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 02/10/2018
 * Time: 9:02
 */

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>

<?php ?>
<?php

$columns = [
    [
        'class' => SerialColumn::className(),
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Código',
        'value' => 'cuenta.cod_completo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Cuenta',
        'value' => 'cuenta.nombre',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Concepto',
        'value' => 'conceptoText',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
];

$template = '{update} &nbsp {delete}';
$buttons = [
    'delete' => function ($url, $model, $index) {
        return Html::a(
            '<span class="glyphicon glyphicon-trash"></span>',
            false,
            [
                'class' => 'delete-cuenta-prestamo',
                'delete-url' => $url,
                'id' => $index,
                'title' => Yii::t('app', 'Borrar')
            ]
        );
    },
    'update' => function ($url, $model) {
        return Html::a(
            '<span class="glyphicon glyphicon-pencil"></span>',
            false,
            [
                'title' => 'Editar cuenta',
                'class' => 'modal_cuentas_prestamo',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => $url,
                'data-pjax' => '0',
                'data-title' => 'Editar cuenta'
            ]
        );
    }
];

array_push($columns, [
    'class' => ActionColumn::class,
    'template' => $template,
    'buttons' => $buttons,
    'urlCreator' => function ($action, $model, $key, $index) {
        if ($action === 'delete') {
            $url = Url::to(['contabilidad/plan-cuenta/borrar-cuenta-prestamo', 'indice' => $index]);
            return $url;
        } elseif ($action === 'update') {
            $url = Url::to(['contabilidad/plan-cuenta/modificar-cuenta-prestamo', 'indice' => $index]);
            return $url;
        }
        return '';
    },
    'contentOptions' => ['style' => 'font-size: 90%;'],
    'headerOptions' => ['style' => 'font-size: 90%;'],
]);

$skey = 'cont_cuentas_prestamo_empresa';
if (!Yii::$app->session->has($skey)) {
    Yii::$app->session->set($skey, new \yii\data\ArrayDataProvider([
        'allModels' => [],
        'pagination' => false,
    ]));
}

Yii::trace('desdeaqui', 'grid_cuentas_prestamo');
Pjax::begin(['id' => 'grid_cuentas_prestamo']);
try {
    echo GridView::widget([
        'dataProvider' => Yii::$app->getSession()->get($skey),
        'columns' => $columns,
        'hover' => true,
        'id' => 'grid_cuentas',
        'panel' => ['type' => 'primary', 'heading' => 'Cuentas para Préstamos', 'footer' => false,],
        'toolbar' => [
            'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'id' => 'btn_add_cuenta',
                'type' => 'button',
                'title' => '',
                'class' => 'btn btn-success modal_cuentas_prestamo',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['contabilidad/plan-cuenta/add-cuenta-prestamo']),
                'data-pjax' => '0',
                'data-title' => 'Agregar cuenta'
            ]),
        ]
    ]);

} catch (Exception $e) {
    echo "asdf <br/>";
    print $e;
}

Pjax::end();
Yii::trace('termino fin', 'grid_cuentas_prestamo');
?>

<?php ?>