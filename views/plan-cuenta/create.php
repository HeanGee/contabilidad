<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlanCuenta */

$this->title = 'Crear Cuenta';
$this->params['breadcrumbs'][] = ['label' => 'Plan de Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-cuenta-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
