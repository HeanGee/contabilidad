<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlanCuenta */

$this->title = 'Modificar Cuenta: ';
$this->params['breadcrumbs'][] = ['label' => 'Plan Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="plan-cuenta-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
