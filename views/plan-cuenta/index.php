<?php

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\PlanCuenta;
use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\PlanCuentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Plan Cuentas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-cuenta-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="btn-toolbar">
        <?= PermisosHelpers::getAcceso('contabilidad-plan-cuenta-create') ?
            Html::a('Crear Cuenta', ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?php
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'value' => 'id',
                    'width' => '60px',
                ],
                [
                    'attribute' => 'cod_ordenable',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->cod_completo;
                    }
                ],
//                    'cod_completo',
//                    'cod_cuenta',
                'nombre',
//                    'empresa_id',
                [
                    'attribute' => 'asentable',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->asentable == 'si') ? 'success">Sí' : 'danger">No') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'asentable',
                        'data' => array_map('ucfirst', PlanCuenta::getValoresEnum('asentable')),
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                        ],
                    ])
                ],
                //'padre_id',
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => array_map('ucfirst', PlanCuenta::getValoresEnum('estado')),
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                        ],
                    ])
                ],
                [
                    'label' => 'Es global?',
                    'attribute' => 'es_global',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (!(isset($model->empresa_id)) ? 'success">Sí' : 'danger">No') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'es_global',
                        'data' => ['si' => "Si", 'no' => "No"],
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                        ],
                    ])
                ],
                [
                    'label' => 'Empresa asociada',
                    'attribute' => 'empresa_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return (($model->empresa_id != '') ? "{$model->empresa->razon_social} <strong>({$model->empresa->ruc})</strong>" : '');
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'empresa_id',
                        'data' => ArrayHelper::map(
                            (new Query())
                                ->from('cont_plan_cuenta as cta')
                                ->innerJoin('core_empresa empresa', 'empresa.id = cta.empresa_id')
                                ->select(['id' => "cta.empresa_id", 'text' => "CONCAT(empresa.razon_social, ' (', empresa.ruc, ')')"])
                                ->all(),
                            'id', 'text'
                        ),
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                        ],
                    ])
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);

        if (PermisosHelpers::esSuperUsuario()) {
            $adminFunctions = $this->render('panel_admin');
            $header = "PANEL DE ADMINISTRADOR";
            echo HtmlHelpers::BootstrapWarningPanel($header, $adminFunctions);
        }

    } catch (\Exception $e) {
        print $e;
    }

    $infocolor = HtmlHelpers::InfoColorHex(true);
    $script = <<<JS
            $(document).on('pjax:success', function() {
                var inputs = $('tr.filters input');
                $.each(inputs, function() {
                    if($(this).val() != ''){
                        var tmpStr = $(this).val();
                        $(this).focus().val('').val(tmpStr);
                        return false;
                    }
                });
            });

    applyPopOver($('a.btn-arbol'), 'Atención', 'Puede que la página tarde en responder debido a manejos de árbol.', {'placement': 'top', 'title-css': {'background-color': '#FF8800', 'color': '#FFFFFF', 'font-weight': 'bold'}, });
    applyPopOver($("tr th:contains('Es global')"), 'Atención', 'Si <u>es SuperUsuario</u> y elige <strong>\'No\'</strong>, se mostrarán todas las cuentas no globales sin importar empresa actual. Si <u>no es SuperUsuario</u>, sólo se mostrarán cuentas asociadas a la empresa actual', {'placement': 'left', 'title-css': {'background-color': '#FF8800', 'color': '#FFFFFF', 'font-weight': 'bold'}, });
    applyPopOver($("a:contains('Empresa asociada')"), 'Información', 'Se ordenan por razón social.', {'placement': 'top', 'title-css': {'background-color': '#$infocolor', 'color': '#FFFFFF', 'font-weight': 'bold'}, });
JS;
    $this->registerJs($script);

    ?>
    <?php Pjax::end(); ?>
</div>

<?php
//$CSS = <<<CSS
//.table tbody tr:hover {
//    background-color: #ebffdd;
//}
//CSS;
//$this->registerCss($CSS);
?>


