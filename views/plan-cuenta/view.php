<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlanCuenta */

$this->title = "Cuenta Contable: {$model->nombre}";
$this->params['breadcrumbs'][] = ['label' => 'Plan Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-cuenta-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plan-cuenta-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plan-cuenta-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plan-cuenta-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plan-cuenta-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plan-cuenta-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Plan de Cuentas', ['index'], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de querer borrar esta Cuenta?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php
        try{
            echo DetailView::widget([
                'model' => $model,
                'condensed' => true,
                'hover' => true,
                'mode' => DetailView::MODE_VIEW,
                'enableEditMode' => false,
                'fadeDelay' => true,
                'panel' => [
                    'heading' => "Datos",
                    'type' => DetailView::TYPE_INFO,
                ],
                'attributes' => [
                    [
                        'columns' => [
                            [
                                'attribute' => 'cod_cuenta',
                                'value' => $model->cod_cuenta,
                                'valueColOptions' => ['style' => 'width:30%'],
                            ],
                            [
                                'attribute' => 'cod_completo',
                                'value' => $model->cod_completo,
                                'valueColOptions' => ['style' => 'width:30%'],
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'attribute' => 'estado',
                                'format' => 'raw',
                                'value' => '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>',
                                'valueColOptions' => ['style' => 'width:30%'],
                            ],
                            [
                                'attribute' => 'padre_id',
                                'value' => isset($model->padre) ? $model->padre->cod_completo : '-',
                                'valueColOptions' => ['style' => 'width:30%'],
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'attribute' => 'asentable',
                                'format' => 'raw',
                                'value' => '<label class="label label-' . (($model->asentable == 'si') ? 'success">Sí' : 'danger">No') . '</label>',
                                'valueColOptions' => ['style' => 'width:30%'],
                            ],
                            [
                                'attribute' => 'empresa_id',
                                'value' => (isset($model->empresa_id) ? $model->empresa->razon_social : '-'),
                                'valueColOptions' => ['style' => 'width:30%'],
                            ],
                        ],
                    ],
                ],
            ]);

        }catch (\Exception $e){
            \common\helpers\FlashMessageHelpsers::createWarningMessage($e->getMessage());
        }
    ?>

</div>
