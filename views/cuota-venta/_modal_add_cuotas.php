<?php

use backend\modules\administracion\models\Item;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
?>

<div class="add_cuotas_div">
    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'add-cuotas-form',
        'options' => ['class' => 'disable-submit-buttons'],
    ]); ?>

    <?php try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 12,
            'attributes' => [
                'nro_cuotas' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => MaskedInput::className(),
                    'options' => [
                        'clientOptions' => [
                            'rightAlign' => false,
                            'alias' => 'integer',
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'autoGroup' => true
                        ],
                    ],
                    'label' => 'Nro de cuotas',
                    'columnOptions' => ['colspan' => '3']
                ],
                'monto' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => MaskedInput::className(),
                    'options' => [
                        'clientOptions' => [
                            'rightAlign' => false,
                            'alias' => 'decimal',
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'autoGroup' => true
                        ],
                    ],
                    'label' => 'Monto',
                    'columnOptions' => ['colspan' => '4']
                ],
                'fecha' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => DateControl::class,
                    'hint' => 'Ingrese la fecha (dd-mm-aaaa)',
                    'options' => [
                        'type' => DateControl::FORMAT_DATE,
                        'ajaxConversion' => false,
                        'language' => 'es',
                        'widgetOptions' => [
                            'pluginOptions' => [
                                'autoclose' => true,
                                'todayHighlight' => true,
                            ]
                        ],
                    ],
                    'columnOptions' => ['colspan' => '5']
                ],
            ]
        ]);
    } catch (Exception $e) {
    } ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Agregar'), ['data' => ['disabled-text' => 'Agregando ...'], 'class' => 'btn btn-success', 'data-confirm' => 'Se van a generar nuevas cuotas. Se reemplazaran las cuotas cargadas anteriormente.']) ?>
    </div>
    <?php ActiveForm::end(); ?>

    <?php
    $script1 = <<<JS
// obtener la id del formulario y establecer el manejador de eventos
var form = $("form#add-cuotas-form");
form.on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#cuotas_grid", async: false});
            $("#modal").modal("hide");
        });
    return false;
}).on("submit", function (e) {

    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});
JS;
    $this->registerJs($script1);
    ?>
</div>
