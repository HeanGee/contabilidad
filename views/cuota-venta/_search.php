<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\search\CuotaVentaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuota-venta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'factura_venta_id') ?>

    <?= $form->field($model, 'fecha_vencimiento') ?>

    <?= $form->field($model, 'monto') ?>

    <?= $form->field($model, 'nro_cuota') ?>

    <?php // echo $form->field($model, 'pagado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
