<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\CuotaVenta */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cuota Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuota-venta-view">

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estás seguro de borrar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Regresar a Cuotas', ['index',], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'factura_venta_id',
            'fecha_vencimiento',
            'monto',
            'nro_cuota',
            'pagado',
        ],
    ]) ?>

</div>
