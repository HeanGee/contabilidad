<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\CuotaVenta */

$this->title = 'Modificar Cuota Venta: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cuota Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cuota-venta-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
