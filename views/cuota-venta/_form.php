<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\CuotaVenta */
/* @var $form kartik\form\ActiveForm */
?>

<div class="cuota-venta-form">

    <?= Html::a('Regresar a Cuotas', ['index',], ['class' => 'btn btn-success']) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'factura_venta_id')->textInput() ?>

    <?= $form->field($model, 'fecha_vencimiento')->textInput() ?>

    <?= $form->field($model, 'monto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nro_cuota')->textInput() ?>

    <?= $form->field($model, 'pagado')->dropDownList([ 'si' => 'Si', 'no' => 'No', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
