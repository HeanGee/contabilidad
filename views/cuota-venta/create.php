<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\CuotaVenta */

$this->title = 'Crear nueva Cuota Venta';
$this->params['breadcrumbs'][] = ['label' => 'Cuota Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuota-venta-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
