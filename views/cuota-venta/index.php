<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\CuotaVentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cuotas Venta';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuota-venta-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear nueva Cuota Venta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $template = '';
    foreach (['view', 'update', 'delete'] as $_v)
        if (\common\helpers\PermisosHelpers::getAcceso("cuota-venta-{$_v}"))
            $template .= "&nbsp{{$_v}}";
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'factura_venta_id',
                'fecha_vencimiento',
                'monto',
                'nro_cuota',
                //'pagado',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $e) {
    } ?>
</div>
