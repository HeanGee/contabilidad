<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventaDetalle */

$this->title = "Datos del detalle ".$model->id.' de la plantilla '.$model->plantilla->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Plantilla Compraventa Detalles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantilla-compraventa-detalle-view">

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Realmente desea borrar?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Nombre de la plantilla',
                'value' => $model->plantilla->nombre,
            ],
            [
                'label' => 'Porcentaje IVA',
                'value' => $model->ivaCta->iva->porcentaje.'%',
            ],
            [
                'label' => 'Código cuenta',
                'value' => $model->pCGravada->cod_completo,
            ],
            [
                'label' => 'Cuenta',
                'value' => $model->pCGravada->nombre,
            ],
        ],
    ]) ?>

</div>
