<?php

use yii\helpers\Html;
use backend\modules\contabilidad\models\PlantillaCompraventa;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventaDetalle */

$this->title = 'Nuevo detalle para la plantilla '.PlantillaCompraventa::findOne(['id' => $model->plantilla_id])->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Plantilla Compraventa Detalles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantilla-compraventa-detalle-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
