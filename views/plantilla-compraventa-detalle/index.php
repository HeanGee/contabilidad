<?php

use backend\modules\contabilidad\models\PlantillaCompraventa;
use common\helpers\PermisosHelpers;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\PlantillaCompraventaDetalleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $id int */

$this->title = 'Detalles de la plantilla ' . PlantillaCompraventa::findOne(['id' => $id])->nombre;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantilla-compraventa-detalle-index">

    <p>
        <?= Html::a('Crear nuevo detalle', ['create', 'id' => $id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php

    $template = '';
    foreach (['view', 'update', 'delete'] as $_v)
        if (PermisosHelpers::getAcceso("contabilidad-plantilla-compraventa-{$_v}"))
            $template .= "&nbsp{{$_v}}";

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                'id',
                [
                    'label' => 'Nombre plantilla',
                    'attribute' => 'plantilla_id',
                    'value' => 'plantilla.nombre',
                ],
                [
                    'label' => 'Porcentaje IVA',
                    'attribute' => 'iva_cta_id',
                    'value' => 'ivaCta.iva.porcentaje',
                ],
                [
                    'label' => 'Código cuenta',
                    'attribute' => 'iva_cta_id',
                    'value' => 'ivaCta.cuentaVenta.cod_completo',
                ],
                [
                    'label' => 'Cuenta del IVA',
                    'attribute' => 'iva_cta_id',
                    'value' => 'ivaCta.cuentaVenta.nombre',
                ],
                [
                    'label' => 'Código cuenta',
                    'attribute' => 'p_c_gravada_id',
                    'value' => 'pCGravada.cod_completo',
                ],
                [
                    'label' => 'Cuenta gravada',
                    'attribute' => 'p_c_gravada_id',
                    'value' => 'pCGravada.nombre',
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
//                    'buttons' =>
//                        [
//                            'delete' => function ($url, $model, $plantillaId) {
//                                return Html::a('Detalles', "index.php?r=contabilidad/plantilla-compraventa-detalle/delete&id=" . $model->id . '&plantillaId=' . $plantillaId, ['class' => 'btn btn-primary btn-xs']);
//                            }
//                        ],
                    'template' => $template,
//                    'urlCreator' => function ($action, $model, $key, $index) {
//                        if ($action === 'delete') {
//                            return Url::to(['/rrhh/empleado/recontratar', 'id' => $model->id]);
//                        } else if ($action === 'view') {
//                            return Url::to(['/rrhh/empleado/view', 'id' => $model->id]);
//                        } else if ($action === 'update') {
//                            return Url::to(['/rrhh/empleado/update', 'id' => $model->id]);
//                        }
//                        return Url::to(['/rrhh/empleado/delete', 'id' => $model->id]);
//                    }
                ],
            ],
        ]);
    } catch (Exception $e) {
        throw $e;
    }
    ?>
</div>


