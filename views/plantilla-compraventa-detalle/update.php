<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventaDetalle */

$this->title = 'Modificar detalle ' . $model->id . ' de la plantilla '.$model->plantilla->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Plantilla Compraventa Detalles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plantilla-compraventa-detalle-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
