<?php

use backend\modules\contabilidad\models\PlanCuenta;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventaDetalle */
/* @var $form kartik\form\ActiveForm */
?>

<div class="detalle-plantilla-costoventa-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => '_form_costoventa_modal',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <?php
//    echo '<br/><br/>el puto es: '.($model == null).'<br/><br/>';
    try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'p_c_gravada_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                        'pluginEvents' => [
                            'change' => ""
                        ],
                        'data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ],
                    ],
                    'label' => 'Cuenta'
                ],
                'tipo_saldo' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                        'pluginEvents' => [
                            'change' => ""
                        ],
                        'data' => ['debe' => 'Debe', 'haber' => 'Haber'],
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                ],
                'cta_principal' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                        'pluginEvents' => [
                            'change' => ""
                        ],
                        'data' => ['si' => 'Si', 'no' => 'No'],
                        'initValueText' => 'No',
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                ],
            ]
        ]);
    } catch (Exception $e) {
    }

    $btn = 'success';
    $texto = 'Agreg';
    if(!empty($model->id)){
        $btn = 'primary';
        $texto = 'Edit';
    }

    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', "{$texto}ar"), ['data' => ['disabled-text' => "{$texto}ando..."], 'class' => "btn btn-{$btn}"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $scripts = [];

    $scripts[] = <<<JS
// obtener la id del formulario y establecer el manejador de eventos
$("form#_form_costoventa_modal").on("beforeSubmit", function (e) {
    var form = $(this);
    $.post(
        form.attr("action") + "&submit=true",
        form.serialize()
    )
        .done(function (result) {
            form.parent().html(result.message);
            $.pjax.reload({container: "#detallesplantilla_costoventa_grid", async: false});
            $.pjax.reload({container: "#flash_message_id", async: false});
            $("#modal").modal("hide");
            $("modal-body").html("");
        });
    return false;
}).on("submit", function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    return false;
});

$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        $(this).siblings('select').select2('open');
    }
});

$('#modal').on('shown.bs.modal', function () {
    // if ($('#detalleventa-plan_cuenta_id').val().length == 0)
    //     $('#detalleventa-plan_cuenta_id').select2('open');
});
JS;

    foreach($scripts as $script) $this->registerJs($script);
    ?>
</div>
