<?php

use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PlanCuenta;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventaDetalle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plantilla-compraventa-detalle-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => [       // 1 column layout
                'iva_cta_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'data' => IvaCuenta::getIvasCuentasVenta(),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                    'hint' => 'Seleccione un tipo',
                ],
                'p_c_gravada_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'data' => ArrayHelper::map(PlanCuenta::getCuentaLista(), 'id', 'text'),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                    'hint' => 'Seleccione un estado',
                ],
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
