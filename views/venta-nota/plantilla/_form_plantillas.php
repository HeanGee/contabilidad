<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 24/07/2018
 * Time: 8:32
 */

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\Venta;
use backend\modules\contabilidad\models\VentaIvaCuentaUsada;
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $form kartik\form\ActiveForm */
/* @var $model Venta */
/** @var IvaCuenta[] $iva_ctas */
$es_nota = $model->tipo != 'factura';

$iva_ctas = IvaCuenta::find()->all();
foreach ($iva_ctas as $iva_cuenta)
    $porcentajes_iva[] = $iva_cuenta->iva->porcentaje;
asort($porcentajes_iva);
$porcentajes_iva = array_values($porcentajes_iva);
$porcentajes_iva_json = Json::encode($porcentajes_iva);

$ivaCtaUsada = new VentaIvaCuentaUsada();
$ivaCtaUsada->loadDefaultValues();

echo Html::beginTag('fieldset');

echo Html::beginTag('leyend', []);
echo "<div class='form-group btn-toolbar'>";
{
    echo Html::button('<i class="glyphicon glyphicon-plus"></i>', [
        'id' => 'iva-cta-usada-new-button',
        'class' => 'pull-left btn btn-success',
    ]);

    // Si es update, habilitar boton que trae los detalles por defecto
    if (Yii::$app->controller->id == 'venta' && Yii::$app->controller->action->id == 'update') {
        echo Html::button('Mostrar detalles originales', [
            'id' => 'detalle-defecto',
            'class' => 'pull-right btn btn-info hidden',
        ]);
    }
}
echo "</div>";
echo Html::endTag('leyend');

echo Html::beginTag('table', ['class' => 'table table-condensed table-bordered', 'id' => 'plantillas-table']);

echo Html::beginTag('thead');
echo Html::beginTag('tr', ['class' => 'title-row']);
echo Html::tag('th', 'Plantilla&nbsp;&nbsp;' . HtmlHelpers::InfoHelpIcon('info-plantilla'), ['style' => 'text-align: center; ']);
foreach ($iva_ctas as $iva_cta) {
    if ($iva_cta->iva->porcentaje == 0)
        echo Html::tag('th', 'Exenta', ['class' => '', 'style' => "text-align: center; "]);
    else
        echo Html::tag('th', 'I.V.A. ' . $iva_cta->iva->porcentaje . '%', ['class' => '', 'style' => "text-align: center; "]);
}
//if (!$es_nota) {
echo Html::tag('th', 'Acciones', ['style' => "text-align: center; "]);
echo Html::endTag('tr');
echo Html::endTag('thead');

echo Html::beginTag('tbody');
{
    if (!empty($model->_iva_ctas_usadas)) {
        $plantillaOrden = 1;

        #registrar primero evento onChange del campo de los ivas
        //$this->registerJs("$(document).on('change', 'input.agregarNumber', function() { setTotalFactura(); });");

        #indicar que es doc ready
        $this->registerJs('fromDocReady = true;');
        //if (Yii::$app->controller->action->id == 'update') $this->registerJs('rendering_plantilla_byupdate = true;');

        #renderizar fila de plantillas
        $contador = 0;
        $limite = sizeof($model->_iva_ctas_usadas);
        foreach ($model->_iva_ctas_usadas as $plantilla_id => $ivaCuenta) {
            $contador++;

            #renderizar una fila de plantilla
            echo "
                <tr>
                    {$this->render('_form_plantillas_filas', [
                        'key' => "new{$plantillaOrden}",
                        'form' => $form,
                        'ivaCtaUsada' => $ivaCtaUsada,
                        'porcentajesIva' => $porcentajes_iva,
                    ])}
                </tr>
            ";

            #construir select2 y poner valor de plantilla_id
            $js = <<<JS
field = $("input[id$='new{$plantillaOrden}-plantilla_id']:not([type='hidden'])");
field.select2({
    theme: 'krajee',
    placeholder: '',
    language: 'en',
    width: '100%',
    data: plantillasForSelect2,  //variable registrado en POS_HEAD desde _form.php
    disabled: false,
}).val('$plantilla_id').trigger('change');

activateButtonsForModals(field);
JS;
            $this->registerJs($js);

            #poner valor de ivas
            $tieneDecimales = ($model->moneda_id != '' && $model->moneda_id != \common\models\ParametroSistema::getMonedaBaseId()) ? 'decimal' : 'integer';
            $_contador = 0;
            $_limite = sizeof($ivaCuenta);
            foreach ($ivaCuenta as $iva_porcentaje => $monto) {
                $_contador++;
                if ($contador == $limite && $_contador == $_limite) {
                    $this->registerJs("console.log('antes de entrar'); simular({$model->total}); delete field;");
                }

                $js = <<<JS
// quitar readonly, disable, mostrar boton para remover plantilla y poner valor de iva
$("input[id$='new{$plantillaOrden}-ivas-iva_{$iva_porcentaje}']:not([type='hidden'])")
    .prop('disabled', false).prop('readonly', false)
    .val('{$monto}').trigger('change')
    .parent().parent().parent().find("button[id$='delete_plantilla']").css('display', '');

// remover el hidden, ya que se va a poder modificar plantilla y demas cosas.
$("input[id$='new{$plantillaOrden}-plantilla_id'][type='hidden']").parent().remove();

//masked input
construirMaskedInput("input[id$='new{$plantillaOrden}-ivas-iva_{$iva_porcentaje}']:not([type='hidden'])", "{$tieneDecimales}");

// no se habilitan botones para activos fijos/biologicos porque no se conoce la factura, asi que no hay necesidad.
JS;
                $this->registerJs($js);
            }
            $plantillaOrden++;
        }
        #actualizar variable js `plantillaOrden`
        $this->registerJs("plantillaOrden = {$plantillaOrden}");
        //if (Yii::$app->controller->action->id == 'update') $this->registerJs('rendering_plantilla_byupdate = false;');
        $this->registerJs("fromDocReady = false;");
    } else {
        Yii::warning('no hace plantilla');
        Yii::warning(empty($model->_iva_ctas_usadas));
    }
}
// Mostrar filas por plantillas
//$key = 1;
//foreach ($model->_iva_ctas_usadas as $index => $_iva_cta) {
//    echo Html::beginTag('tr', ['class' => 'fila-data']);
//    echo $this->render('_form_plantillas_filas', [
//        'key' => 'new' . $key,
//        'form' => $form,
//        'ivaCtaUsada' => $ivaCtaUsada,
//        'porcentajesIva' => $porcentajes_iva,
//        'es_nota' => $es_nota
//    ]);
//    echo Html::endTag('tr');
//
//    $noDeducible = $model->usar_tim_vencido == 1 ? true : false;
//    $plantilla_data = Json::encode(PlantillaCompraventa::getPlantillasLista('venta', false, $noDeducible, false, $model->obligacion_id));
//    $key++;
//}
// Fila ejemplar
$bluePrint = $this->render('_form_plantillas_filas', [
    'key' => 'new',
    'form' => $form,
    'ivaCtaUsada' => $ivaCtaUsada,
    'porcentajesIva' => $porcentajes_iva,
    'es_nota' => $es_nota
]);
$bluePrint = HtmlHelpers::trimmedHtml(str_replace('"', '\'', $bluePrint));
Yii::$app->session->set('$bluePrint', $bluePrint);
echo Html::endTag('tbody');

echo Html::endTag('table');

echo Html::endTag('fieldset');
?>

<?php
//$url_get_plantilla = Json::htmlEncode(\Yii::t('app', Url::to(['plantilla-compraventa/get-plantilla'])));
ob_start(); // output buffer the javascript to register later ?>
    <script>
        function generarInputNumber(elemento, tiene_decimales) {
            elemento.inputmask({
                "alias": "numeric",
                "digits": tiene_decimales === "si" ? 2 : 0,
                "groupSeparator": ".",
                "autoGroup": true,
                "autoUnmask": true,
                "unmaskAsNumber": true,
                "radixPoint": ",",
                "digitsOptional": false,
                "placeholder": "0",
                "rightAlign": true
            });
        }

        function g_aplicarNumberInput() {
            console.log('g_aplicarNumberInput');
            let tiene_decimales = esNota ? decimalesMonedaParaNota : $('#venta-moneda_id').select2('data')[0]['tiene_decimales'];
            // campos ivas por plantillas
            $('input.agregarNumber').each(function () {
                generarInputNumber($(this), tiene_decimales);
            });

            // campos sumatoria y discriminadas
            $('.totales :input.form-control').each(function () {
                generarInputNumber($(this), tiene_decimales);
            });
            $('.ivas :input.form-control').each(function () {
                generarInputNumber($(this), tiene_decimales);
            });

            // campos total y total_ivas
            generarInputNumber($('#venta-total-disp'), tiene_decimales);
            generarInputNumber($('#venta-_total_ivas-disp'), tiene_decimales);
            generarInputNumber($('#venta-_total_gravadas-disp'), tiene_decimales);
        }

        // function completarTotales() {
        //     let ivas = getIvas();
        //     let total_iva = 0.0;
        //     let total_gravada = 0.0;
        //     let total_factura = 0.0;
        //     ivas.forEach(function (element) {
        //         let sum = 0.0;
        //         let iva = 0.0;
        //         let gravada = 0.0;
        //         $('td.column-iva-' + element + ' :input').each(function () {
        //             const regex = /ventaivacuentausada-new([0-9]+)-ivas-iva_([0-9]+)/gm;
        //             let m = regex.exec($(this).attr('id'));
        //             if (m !== null && $(this).val() !== "") {
        //                 sum += parseFloat($(this).val());
        //             }
        //         });
        //         total_factura += sum;
        //         $('#totals-iva-' + element + '-disp').val(sum);
        //         if (element !== 0) {
        //             // 5500 - (5500 / ((10 + 100) / 100))
        //             iva = sum - (sum / ((element + 100) / 100));
        //             gravada = sum - iva;
        //             $('#ivas-iva-' + element + '-disp').val(iva);
        //             $('#ivas-gravada-' + element + '-disp').val(gravada);
        //             total_iva += parseFloat(iva);
        //             total_gravada += parseFloat(gravada);
        //         }
        //     });
        //     let toFix = $('#venta-moneda_id').val() === 1 ? 0 : 2;
        //     total_iva = Number((total_iva).toFixed(toFix));
        //     total_gravada = Number((total_gravada).toFixed(toFix));
        //     total_factura = Number((total_factura).toFixed(toFix));
        //     $('#venta-_total_ivas-disp').val(total_iva).trigger('change');
        //     $('#venta-_total_gravadas-disp').val(total_gravada).trigger('change');
        //     $('#venta-total-disp').val(total_factura).trigger('change');
        // }

        $(document).ready(function () {
            completarTotales();
            //setTotalFactura();

            let css = {
                'title-css': {
                    'background-color': '#<?= HtmlHelpers::InfoColorHex(true) ?>',
                    'color': 'white',
                    'text-align': 'center',
                    'font-weight': 'bold'
                }
            }; //D9EDF7
            let content = 'Si la lista aparece vacía, verifique:<br/><br/>' +
                '<ol>' +
                '<li>Existe al menos una plantilla para venta registrada en el sistema.</li>' +
                '<li>Existe al menos una plantilla para venta asociada a la obligación seleccionada más arriba.</li>' +
                '</ol>';
            applyPopOver($('span.info-plantilla'), 'Atención', content, css);
        });
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>