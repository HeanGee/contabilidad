$(document).ready(function () {

    // Incrustar boton entre campos.
    let campo_nro_factura = $('#venta-nro_factura');
    let campo_tipodoc = $('#venta-tipo_documento_id');
    var a = campo_nro_factura.parent().parent()[0];
    $('<div id = "button_facturaventa_addtimbrado" style="display: block;" class="col-sm-1"><div class="form-group field-button_facturaventa_addtimbrado" style="text-align: center;">' + boton + '<div class="help-block"></div></div></div>').insertAfter(a);
    if (campo_tipodoc.val() === '') {
        $('#boton_add_timbrado').prop('disabled', true);
    }

});

// Quitar disabled y readonly al postear
$('#nota_form').on('beforeSubmit', function () {
    $('.select2-hidden-accessible').prop('disabled', false);
});

function obtenerTotalFactura() { // al modificar este, tambien modificar en _form_detalle.php
    var factura_total = 0.0;

    // calcular suma de todos los totales
    let ivas = getIvas();
    ivas.forEach(function (element) {
        $('td.column-iva-' + element + ' :input').each(function () {
            let id = '#' + $(this).attr('id');
            const regex = /#ventaivacuentausada-new([0-9]+)-/gm;
            let m = regex.exec(id);
            if (m !== null) {
                if ($(this).val() !== "")
                    factura_total += (parseFloat($(this).val()));
            }
        })
    });

    return factura_total;
}

function readyToCalcDetalle() {
    if (['anulada', 'faltante'].includes($("#venta-estado").val())) return false;
    if ($('#venta-tipo_documento_id').val() === '') return false;
    let monedaField = $('#venta-moneda_id');
    if (monedaField.val() === '') return false;
    else if (monedaField.val() !== '1') {
        if ($('#venta-valor_moneda').val() === '') return false;
    }

    //recorre plantillas y verifica si hay uno seleccionado y con monto distinto de cero
    let plantillaValueEmpty = true,
        plantillas = $('input[id$="plantilla_id"]'),
        cant_plantillas = plantillas.length,
        cant_plantillas_vacias = 0;
    plantillas.each(function (i, plantilla) {
        if ($(plantilla).val() === '') {
            cant_plantillas_vacias++;
            if (cant_plantillas_vacias === cant_plantillas) {
                plantillaValueEmpty = true;
                return false;
            }
        } else {
            let emptyValue = true;
            $(plantilla).parent().parent().parent().find('input:not([id$="plantilla_id"]):not([id$="gnd_motivo"])').each(function (j, element) {
                if (element.value !== '' && element.value > 0) {
                    emptyValue = false;
                    return false;
                }
            });
            if (!emptyValue) {
                plantillaValueEmpty = false;
                return false;
            }
        }
    });
    return !plantillaValueEmpty;
}

function setTotalFactura() { //console.log('setTotalFactura()');
    if (fromDocReady) return;

    if (readyToCalcDetalle()) {
        // inhabilitar temporalmente boton submit
        let submitBtn = $('#btn_submit_factura_venta');
        submitBtn.prop('disabled', true);
        submitBtn.html('Generando detalles y simulacion ... ');

        //calcular ivas y mostrar en los campos disableds de ivas
        // calcularIvasYRellenar();
        // $('#venta-factura_venta_id').select2('data')[0]
        let tipo_docu_id = $('#venta-tipo_documento_id').val();//(($('#venta-tipo').select2('data')[0]['id'] === "nota_credito" ? tipoDocNtaCreditoId : tipoDocNotaDebitoId));
        $.ajax({
            url: url_calcularDetalle,
            type: 'post',
            data: {
                moneda_id: $('#venta-moneda_id').val(),
                valor_moneda: $('#venta-valor_moneda' + (!esNota ? '-disp' : '')).val(),
                cont_factura_total: obtenerTotalFactura(),
                plantillas: getDataTable(),
                tipo_docu_id: tipo_docu_id,
                ivas: getIvas(),
                estado_comprobante: $('#venta-estado').val(),
                es_nota: esNota
                // totales_valor: getTotalFieldValues(),
                // plantilla_id: $('#venta-plantilla_id').val(),
            },
            success: function (data) {
                if (data !== '') {// si retorna falso, aquí se vé como ''. si retorna true, aquí se vé como 1.
                    $.pjax.reload({container: "#detallesventa_grid", async: false});
                    simular();
                } else {
                    $.pjax.reload({container: "#flash_message_id", async: false});

                    submitBtn.prop('disabled', false);
                    submitBtn.html('Guardar y siguiente');
                }

                // re-habilitar boton submit LO REHABILITA EN _form_asientoSimulator_costo.php en document.onReady
                // $('#btn_submit_factura_venta').prop('disabled', false);
                // $('#btn_submit_factura_venta').html('Guardar y siguiente');
            }
        });
    }
}

function getIvas() {
    return Object.values(JSON.parse(porcentajes_iva))
}

//Retorna array con datos de la tabla de plantillas - ivas
function getDataTable() {
    let ivas = getIvas();
    let data_table = [];
    $('td.plantilla-selector :input:not([type="hidden"])').each(function () {
        if ($(this).val() !== "") { // tiene seleccionado una plantilla
            let id_prefijo = (/#ventaivacuentausada-new([0-9]+)-/g).exec('#' + $(this).prop('id'));
            if (id_prefijo != null) {
                let fila = {};
                for (let j = 0; j < ivas.length; j++) {
                    fila['iva_' + ivas[j]] = $(id_prefijo[0] + 'ivas-iva_' + ivas[j]).val();
                }
                fila["plantilla_id"] = $(this).val();
                data_table.push(fila);
            }
        }
    });
    return data_table;
}

/**
 * Funcion que ejecuta la simulacion de asiento (valorizado) segun los detalles en la sesion de Yii2.
 *
 * Recibe como parametro opcional el monto total de la factura.
 * Es utilizado hasta el momento desde _form_plantillas.php donde en la ultima iteracion de plantilla y campos de iva,
 * se invoca a esta funcion. En ese punto el form aun no tiene calculado el total del mismo.
 * @param total_factura
 */
function simular(total_factura = 0) {
    if (simulando) {
        console.log('simular no se realiza');
        return;
    }

    simulando = true;

    $.ajax({
        url: url_simular,
        type: 'post',
        data: {
            moneda_id: $('#venta-moneda_id').val(),
            valor_moneda: $('#venta-valor_moneda-disp').val(),
            cont_factura_total: (total_factura !== 0 ? total_factura : obtenerTotalFactura()),
            plantillas: getDataTable(),
            hoy: $('#venta-emision_factura').val(),
            estado_comprobante: $('#venta-estado').val(),
            es_nota: esNota
        },
        success: function (data) {
            if (data !== "") {
                $.pjax.reload({container: "#asiento-simulator-div", async: false});
                $.pjax.reload({container: "#asiento-simulator-costo-div", async: false});
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
            simulando = false;
        }
    });
}

function construirMaskedInput(selector, number_type = 'integer', integerDigits = 17) {
    // console.log('aplicar maskedinput:', selector);
    $(selector).inputmask({
        "alias": "numeric",
        "digits": (number_type === 'integer') ? 0 : 2,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true,
        // allowMinus: false,
        integerDigits: integerDigits,
        fractionalDigits: (number_type === 'integer') ? 0 : 2,
        removeMaskOnSubmit: true,
    });
}

function esCredito() {
    return ($('#venta-tipo').val() === "nota_credito");
}

function esDebito() {
    return ($('#venta-tipo').val() === "nota_debito");
}

/**
 * Retorna el prefijo mas largo del id del input, excluyendo el identificador del atributo del modelo.
 *
 * @param {jquery} input Representa el input que acaba de sufrir cambios.
 * @return {string} El prefijo del id del input que acaba de sufrir cambios.
 */
function getPrefijoId(input) {
    let input_id = input.attr('id');
    let input_id_slices = input_id.split('-');
    input_id_slices.splice(-1, 1);
    return input_id_slices.join('-') + '-';
}

/**
 * Muestra u oculta botones para modal en base a los datos de la plantilla.
 *
 * @param inputPlantillaId
 */
function activateButtonsForModals(inputPlantillaId) {
    if (!esCredito()) return false; // segun jose, hasta ahora debito solamente es jugar con cuentas y nada de stocks.

    let prefijo = '#' + inputPlantillaId.prop('id').replace(/plantilla_id/g, ''),
        sufijo;

    // Mostrar botón para activo fijo.
    sufijo = 'actfijo_for_nota_credito_manager_button';
    if (inputPlantillaId.select2('data')[0]['activo_fijo'] === 'si') {
        $(prefijo + sufijo).prop('style', "display: yes");
    } else {
        $(prefijo + sufijo).prop('style', "display: none");
    }

    // Mostrar botón para activo biologico.
    sufijo = 'actbio_nota_credito_manager_button';
    if (inputPlantillaId.select2('data')[0]['activo_biologico'] === 'si') {
        $(prefijo + sufijo).prop('style', "display: yes");
    } else {
        $(prefijo + sufijo).prop('style', "display: none");
    }
}

/**
 * Pone la cotización de la moneda en el campo de valor_moneda.
 */
function setCotizaciones() {
    $.ajax({
        url: url_getCotizacion,
        type: 'post',
        data: {
            fecha_emision: $('#venta-emision_factura').val(),
            moneda_id: $('#venta-moneda_id').val(),
        },
        success: function (data) {
            if (parseFloat(data) % 1 === 0) {
                data = parseFloat(data) - (parseFloat(data) % 1);
            }
            $('#venta-valor_moneda-disp').val(data);
            $('#venta-valor_moneda').val(data);
            setTotalFactura();
        }
    });
}

/**
 * Rellena las opciones del Select2 de factura, en base a la entidad seleccionada.
 */
function rellenarFacturasByEntidad() {
    let select2Facturaventaid = $('#venta-factura_venta_id');
    if (select2Facturaventaid.data('select2')) {
        select2Facturaventaid.select2("destroy");
        select2Facturaventaid.html("<option><option>");
        select2Facturaventaid.select2({
            theme: 'krajee',
            placeholder: '',
            language: 'en',
            width: '100%',
            data: [],
        });
    }
    $.ajax({
        url: url_getFacturasByRuc,
        type: 'get',
        data: {
            ruc: $('#venta-ruc').val(),
            tipo: $('#venta-tipo').select2('data')[0]['registro_tipo'],
        },
        success: function (data) {
            select2Facturaventaid.select2({
                theme: 'krajee',
                placeholder: '',
                language: 'en',
                width: '100%',
                data: data.results,
                allowClear: true,
            });

            $.each(data.results, function (index, element) {
                if (element['id'] === facturaVentaIdInicial) {
                    select2Facturaventaid.val(facturaVentaIdInicial).trigger('change');
                    return false;
                }
            });
        }
    });
}

function getTipoDocumento() {
    return $('#venta-tipo_documento_id').val(); //$('#venta-tipo').select2('data')[0]['tipo_doc_set_id'];
}

function completarTotales() {
    let ivas = getIvas();
    let total_iva = 0.0;
    let total_gravada = 0.0;
    let total_factura = 0.0;
    ivas.forEach(function (element) {
        let sum = 0.0;
        let iva = 0.0;
        let gravada = 0.0;
        $('td.column-iva-' + element + ' :input').each(function () {
            const regex = /ventaivacuentausada-new([0-9]+)-ivas-iva_([0-9]+)/gm;
            let m = regex.exec($(this).attr('id'));
            if (m !== null && $(this).val() !== "") {
                sum += parseFloat($(this).val());
            }
        });
        total_factura += sum;
        $('#totals-iva-' + element + '-disp').val(sum);
        if (element !== 0) {
            // 5500 - (5500 / ((10 + 100) / 100))
            iva = sum - (sum / ((element + 100) / 100));
            gravada = sum - iva;
            $('#ivas-iva-' + element + '-disp').val(iva);
            $('#ivas-gravada-' + element + '-disp').val(gravada);
            total_iva += parseFloat(iva);
            total_gravada += parseFloat(gravada);
        }
    });
    let toFix = $('#venta-moneda_id').val() === 1 ? 0 : 2;
    total_iva = Number((total_iva).toFixed(toFix));
    total_gravada = Number((total_gravada).toFixed(toFix));
    total_factura = Number((total_factura).toFixed(toFix));
    $('#venta-_total_ivas-disp').val(total_iva).trigger('change');
    $('#venta-_total_gravadas-disp').val(total_gravada).trigger('change');
    $('#venta-total-disp').val(total_factura).trigger('change');
}

$(document).on('click', '.tiene_modal', (function () {
    var boton = $(this);
    var title = boton.data('title');
    $.get(
        boton.data('url'),
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

$(document).on('click', ':button.btn-simular', function () {
    simular();
});

$(document).on('click', '.tiene_modal2', (function () {
    var boton = $(this);
    var title = boton.data('title');
    var select2Prefijo = $('#venta-timbrado_detalle_id_prefijo');
    var hayOpciones = select2Prefijo.select2('data').length !== 0;
    var prefijoSinGuion = hayOpciones ? select2Prefijo[0].options[select2Prefijo[0].selectedIndex].text.split('-') : "";
    let extra_params = '&prefijo=' + prefijoSinGuion + '&tipoDocumentoId=' + getTipoDocumento();
    extra_params += '';
    $.get(
        boton.data('url') + extra_params,
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

$(document).on('change', '#venta-moneda_id', function () {
    setCotizaciones();

    // Usabilidad: abrir prefijo si esta vacio
    let fechaField = $('#venta-fecha_emision');
    let select2Prefijo = $('#venta-timbrado_detalle_id_prefijo');
    if (select2Prefijo.val() === '' && fechaField.val() !== '') {
        select2Prefijo.select2('open');
    }
});

$(document).on('change', '#venta-obligacion_id', function () {
    if (plantillaManualPermitido()) {
        $('.iva-cta-usada-remove-button').click();
        $('#iva-cta-usada-new-button').click();
    }
});

$(document).on('change', '#venta-factura_venta_id', function () {
    let select2Facturaventaid = $(this);
    let datosFactura = $(this).select2('data')[0];

    // Establecer fecha de emision de la factura.
    // Debe estar primero que el change de moneda_id para que el mismo pueda usar la fecha de emision de la factura.
    $('#venta-emision_factura').val($(this).select2('data')[0]['fecha_emision']);

    if ($(this).val() !== '') $("#venta-obligacion_id").val('').trigger('change');
    $("#nota_form").yiiActiveForm('validateAttribute', 'venta-obligacion_id');

    // Activar o no boton para mas plantillas
    $('#iva-cta-usada-new-button').prop('disabled', $(this).val() !== '');

    // Cambiar moneda de la nota.
    $('#venta-moneda_id').val($(this).select2('data')[0]['moneda_id']).trigger('change');

    // Renderizar plantillas.
    // Rellenar montos de ivas.
    $.ajax({
        url: url_datosFacturaForNota,
        type: 'get',
        data: {
            factura_id: select2Facturaventaid.val(),
            nota_id: nota_id,
            action_id: action_id
        },
        success: function (ivasPorPlantilla) {
            let tablePlantillas = $('#plantillas-table');
            tablePlantillas.find('tbody tr:visible').remove();

            var totalValorNota = 0.0;
            var _tieneDecimales = datosFactura['tiene_decimales'];
            // agrega una fila html por cada iva cuenta.
            $.each(ivasPorPlantilla, function (index, element) {
                // agrega fila
                tablePlantillas.find('tbody')
                    .append('<tr class="fila-data">' + (bluePrint).replace(/new/g, 'new' + plantillaOrden) + '</tr>');

                // aplicar widget Select2
                let plantillas = data;
                let inputPlantillaId = $(':input[id="ventaivacuentausada-new' + plantillaOrden + '-plantilla_id"]:not([type="hidden"])');
                inputPlantillaId.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: plantillasForSelect2,
                    disabled: ($('#venta-factura_venta_id').val() !== ''),
                }).val(index).trigger('change');

                // rellena los campos de iva.
                $.each(element, function (ivaPorcentaje, monto) {
                    totalValorNota += parseFloat(monto);
                    let _jQselector = '#ventaivacuentausada-new' + plantillaOrden + '-ivas-iva_' + ivaPorcentaje;
                    $('#ventaivacuentausada-new' + plantillaOrden + '-ivas-iva_' + ivaPorcentaje).val(monto).prop('disabled', false).prop('readonly', false);
                    construirMaskedInput(_jQselector, (_tieneDecimales === 'si' ? 'decimal' : 'integer'));
                });

                // activar botones necesarios como: activo fijo, activo biologico....
                activateButtonsForModals(inputPlantillaId);

                plantillaOrden++;
            });

            // completa el campo del total de factura, que en este caso es el monto total de nota
            _jQselector = '#venta-total-disp';
            $(_jQselector).val(totalValorNota).trigger('change');
            construirMaskedInput(_jQselector, (_tieneDecimales === 'si' ? 'decimal' : 'integer'));
            delete _jQselector;

            // Total exenta
            let total = 0;
            $('input[id$="ivas-iva_0"]').each(function (i, e) {
                total += $(this).val();
            });
            $('#totals-iva-0-disp').val(total).trigger('change');

            // Total 5
            let impuesto = 0;
            total = 0;
            $('input[id$="ivas-iva_5"]').each(function (i, e) {
                total += $(this).val();
            });
            $('#totals-iva-5-disp').val(total).trigger('change');
            // Iva 5
            $('#totals-gravada-5-disp').val(total / 21);
            impuesto += total / 21;

            // Total 10
            total = 0;
            $('input[id$="ivas-iva_10"]').each(function (i, e) {
                total += $(this).val();
            });
            $('#totals-iva-10-disp').val(total).trigger('change');
            // Iva 10
            $('#totals-gravada-10-disp').val(total / 11).trigger('change');
            impuesto += total / 11;

            // Impuesto total
            $('#venta-_total_ivas-disp').val((_tieneDecimales === 'si') ? impuesto : Math.round(impuesto));

            // Calcular detalles y simular.
            setTotalFactura();
        }
    });
});

$("#venta-nro_factura").keydown(function (event) {
    var key = event.which;
    var esTeclaBorrar = key === 8;

    if (key !== 9) { // si no es tab
        var text = $(this).val().replace(/_/g, "").replace(/-/g, ''); // borrar los underscores a la derecha
        if (key > 57) key -= 96; else key -= 48; // convertir a un nro
        // var lastgroupstart = 6; // index donde comienza el ultimo grupo de nro
        var lastgroupstart = 0; // index donde comienza el ultimo grupo de nro
        var lastgroup = text.substring(lastgroupstart, text.length); // el ultimo grupo de nro

        // console.log('text: '+text);
        // console.log('longitud: '+text.length);
        // console.log('key: '+key);
        // console.log('lastgroup: ' + lastgroup);

        // if (text.length > 5 && !esTeclaBorrar && (key > -1 && key < 10)) {
        if (!esTeclaBorrar && (key > -1 && key < 10)) {

            if (lastgroup.length === 0) { // si es el 1er nro tecleado
                text = text.replaceAt(lastgroupstart, Array(7).join("0") + key);
                $(this).val(text);
            } else {
                var i, p = -1;
                // localizar donde comienza un nro <> 0
                for (i = 0; i < lastgroup.length; i++) {
                    if (lastgroup[i] !== '0') {
                        p = i;
                        break;
                    }
                }
                // si previamente no hubo ningún número, hacer un espacio forzadamente.
                if (lastgroup === '0000000') lastgroup = '000000';
                // extraer solamente los nros y concatenar el nro tecleado
                lastgroup = lastgroup.substring(p, lastgroup.length) + key; // si p > lastgroup.length, significa que no hubo ningún nro antes de keydown y no extraerá nada el substring.
                // console.log('lastgroup: ' + lastgroup);
                // agregar el ultimo nro tecleado, rellenando debidamente con ceros a la izquierda.
                // text = text.replaceAt(lastgroupstart, Array((7 - lastgroup.length) + 1).join("0") + lastgroup)
                //text = (Array((7 - lastgroup.length) + 1).join("0") + lastgroup);
                text = text.replaceAt(lastgroupstart, Array((7 - lastgroup.length) + 1).join("0") + lastgroup)
            }

            // console.log('ultimo grupo final: '+lastgroup);
            // console.log('final: '+text);
        }
        $(this).val(text);
    }
}).keyup(function (event) {
    var key = event.which;
    var esTeclaBorrar = key === 8;
    console.log(key);

    // si es borrar, rellena a la izquierda
    if (key === 8) {
        let val = $(this).val().replace(/_/g, "");
        console.log(val);
        $(this).val(val.padStart(7, '0')).trigger('change');
        if ($(this).val() === '0000000')
            $(this).val('').trigger('change');
    }
});
// Metodo replaceAt personalizado
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
};

$('#venta-timbrado_detalle_id_prefijo').on('change', function () {

    let element1 = $('#venta-nro_factura');

    // Para usar en cargarPrefijo.
    // prefijo_selected = $(this).val(); // por el momento no hacer nada.
    let nro_setted = element1.val();

    if ($(this).val() !== '' && $(this).val() !== null) {
        element1.prop('readonly', false);
        $('#venta-nro_timbrado').val($(this).select2('data')[0]['nro_timbrado']).trigger('change');
    } else {
        element1.val('');
        $('#venta-nro_timbrado').val('');
        element1.css('readonly', false);
    }
});

$("#venta-tipo").on('change', function () {
    esNota = $(this).val();
    $('#venta-tipo_documento_id').val('').trigger('change');
    $('#venta-ruc').val('').trigger('change');
});

// $(document).on('change', '#venta-ruc', function () {
//     rellenarFacturasByEntidad();
// });

$(document).on('change', '#venta-ruc', function () {
    if (action_id === 'update')
        rellenarFacturasByEntidad(); // es necesario porque en update, al elegir el elemento que tenia inicialmente, los datos extras no se setean. muy raro. Debido a esto, se generan 9 ajax al iniciar update, en lugar de solo 3.

    let ruc = $(this).val();
    $.ajax({
        url: url_getRazonSocialByRuc,
        type: 'post',
        data: {
            ruc: ruc,
        },
        success: function (data) {
            $('#venta-razon_social').val(data);
        }
    });
});

function plantillaManualPermitido() {
    return ($('#venta-factura_venta_id').val() === '');
}

$(document).on('click', '#iva-cta-usada-new-button', function (evt) {
    if (plantillaManualPermitido()) {
        let tablePlantillas = $('#plantillas-table');
        tablePlantillas.find('tbody')
            .append('<tr class="fila-data">' + (bluePrint).replace(/new/g, 'new' + plantillaOrden) + '</tr>');

        let inputPlantillaId = $('#ventaivacuentausada-new' + plantillaOrden + '-plantilla_id');
        if ($('#venta-factura_venta_id').val() === "")
            $('#ventaivacuentausada-new' + plantillaOrden + '-plantilla_id[type="hidden"]').parent().remove();

        // cargar select2:data con las plantillas
        $.ajax({
            url: url_getPlantillas,
            type: 'get',
            data: {
                tipo: 'venta',
                obligacion_id: $('#venta-obligacion_id').val(),
            },
            success: function (data) {
                inputPlantillaId.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: data,
                    disabled: false,
                });
                plantillaOrden++;
            }
        });
    }
});

$(document).on('change', ':input.agregarNumber', function () {
    completarTotales();
    setTotalFactura();
});

$(document).on('click', 'button.iva-cta-usada-remove-button', function () {
    if (plantillaManualPermitido())
        $(this).closest('tbody tr').remove();
});

$(document).on('change', '.plantilla-select2', function () {
    if (plantillaManualPermitido()) {
        let prefijo = "#" + $(this).attr('id').replace('plantilla_id', ''),
            plantilla_select_id = $(this).val();

        //Deshabilitar todos primero
        $('input[id*="' + prefijo.replace('#', '') + 'ivas-iva_"]').prop('disabled', true).prop('readonly', true);

        //Mostrar boton para remover plantilla
        $(prefijo + 'remove-button').css('display', '');

        for (const iva of ivasPorPlantilla[plantilla_select_id]) {
            let element = $(prefijo + "ivas-iva_" + iva);

            //Habilitar selectivamente segun iva
            element.prop('disabled', false).prop('readonly', false);

            //Aplicar mascara
            construirMaskedInput(element, ($('#compra-moneda_id').val() !== '1' ? 'decimal' : 'integer'));  //como no depende de factura, se mira la moneda de la nota.
        }
    }
});

$(document).on('change', '#venta-tipo_documento_id', function (evt) {
    setTotalFactura();
});
