<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 14/12/2018
 * Time: 17:27
 */

// use imports here...

/* @var $this yii\web\View */
/* @var $model \backend\modules\contabilidad\models\Venta */
?>

<?php
$scripts = <<<JS
$(document).on('click', '.actfijo-nota-credito-manager', (function () {
    let boton = $(this);
    let title = boton.attr('title');
    let prefijo = getPrefijoId(boton) + 'ivas-iva_';
    let factura_venta_id = $('#venta-factura_venta_id').val();
    $.get(
        boton.data('url') + '&prefijoFilaPlantilla=' + prefijo + '&venta_id=' + factura_venta_id + "&ventaNotaCredito_id={$model->id}",
        function (data) {
            if (data !== "") {
                let modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                // if (title)
                $('.modal-title', modal).html(title);
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
                $("#modal").modal("hide");
                $("modal-body").html("");
            }
        }
    );
}));

// Código estático recurrente para abrir modales.
$(document).on('click', '.modal-act-bio-nota-credito-manager', (function () {
    var boton = $(this);
    var title = boton.attr('title');
    let fila_id = boton.attr('id').replace('actbio_nota_credito_manager_button', '');
    let campo_iva_activo = "";
    let venta_id = $('#venta-factura_venta_id').val();
    $(':input[id^="'+fila_id+'ivas-iva"]').each(function() {
        if ($(this).attr('disabled') !== 'disabled') {
            campo_iva_activo = $(this).attr('id'); return false;
        }
    });
    let url = boton.data('url') + "&nota_id={$model->id}&venta_id=" + venta_id + '&campo_iva_activo=' + campo_iva_activo;
    
    $.get(
        url,
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            $('.modal-header', modal).css('background', '#3c8dbc');
            $('.modal-title', modal).html(title);
            modal.modal();
        }
    );
}));
JS;
$this->registerJs($scripts);
?>