<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 14/03/19
 * Time: 09:16 AM
 */

use backend\modules\contabilidad\models\ParametroSistema as ParamContabilidad;
use backend\modules\contabilidad\models\Venta;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model Venta */

try {
    $es_nota = in_array($model->tipo, ['nota_credito', 'nota_debito']) ? $model->tipo : false;
    $actionId = Yii::$app->controller->action->id;
    $prefijo_anterior = Yii::$app->session->has('cont_valores_usados') ? Yii::$app->session->get('cont_valores_usados')['timbrado_detalle_id'] : "";
    $empresa_id = Yii::$app->session->get('core_empresa_actual');
    $periodo_id = \Yii::$app->session->get('core_empresa_actual_pc');
    $param_cont_key = "core_empresa-{$empresa_id}-periodo-{$periodo_id}-fact_venta_paralelo";
    $permitir_nro_manual = ParamContabilidad::getValorByNombre($param_cont_key); // devuelve si/no o ''
    $ivas_por = \backend\modules\contabilidad\models\IvaCuenta::find()->all();
    $porcentajes_iva = [];
    foreach ($ivas_por as $iva_por)
        $porcentajes_iva[] = $iva_por->iva->porcentaje;

    asort($porcentajes_iva);
    $porcentajes_iva = array_values($porcentajes_iva);
    $porcentajes_iva = Json::encode($porcentajes_iva);
    $tipodocNotaCreditoId = null;
    $tipodocNotaDebitoId = null;
    if (Yii::$app->controller->id == 'venta-nota') {
        $tipodocNotaCreditoId = Venta::getTipodocNotaCredito()->id;
        $tipodocNotaDebitoId = Venta::getTipodocNotaDebito()->id;
    }
    $JQ_VARIABLES = <<<JS
var prefijo_selected = "$prefijo_anterior";
var permitir_nro_manual = "$permitir_nro_manual";
var action_id = "$actionId";
var campo_moneda = '#venta-moneda_id';
var campo_tipodoc = '#venta-tipo_documento_id';

if ("$es_nota".length) {
    var esNota = "$es_nota";
} else {
    const esNota = 0;
}
JS;
    if ($es_nota && !empty($model->factura_venta_id)) {
        if (!empty($model->facturaVenta->tipoDocumento)) {
            $td_id = $model->facturaVenta->tipo_documento_id;
        } else {
            $_fac = $model->getFacturaVenta()->one();
            $td_id = $_fac->tipo_documento_id;
        }
        $_decimales = $model->moneda->tiene_decimales;
        $fecha_em_fac_orig = $model->tipo == 'nota_debito' ? date('d-m-Y', strtotime($model->facturaVenta->facturaVenta->fecha_emision)) : '';
        $JQ_VARIABLES .= <<<JS
var tipoDocumentoParaNota = $td_id;
var decimalesMonedaParaNota = "$_decimales";
var fechaEmisionFacturaOriginal = "$fecha_em_fac_orig";
JS;
    } else {
        $JQ_VARIABLES .= <<<JS
var tipoDocumentoParaNota;
var decimalesMonedaParaNota;
var fechaEmisionFacturaOriginal;
JS;
    }
    $actionId = Yii::$app->controller->action->id;
    $url_nextnro = Json::htmlEncode(\Yii::t('app', Url::to(['venta/get-next-nro'])));
    $url_simular = Json::htmlEncode(\Yii::t('app', \yii\helpers\Url::to(['venta/simular'])));
    $urlCalcDetalle = Json::htmlEncode(\Yii::t('app', Url::to(['venta/calc-detalle'])));

    $this->registerJs($JQ_VARIABLES, \yii\web\View::POS_HEAD);

    $script = <<<JS
function simular() {
    $.ajax({
        url: $url_simular,
        type: 'post',
        data: {
            moneda_id: $('#venta-moneda_id').val(),
            valor_moneda: $('#venta-valor_moneda-disp').val(),
            cont_factura_total: obtenerTotalFactura(),
            plantillas: getDataTable(),
            // hoy: !esNota ? $('#venta-fecha_emision').val() : (esNota === 'nota_debito' ? fechaEmisionFacturaOriginal : $('#venta-emision_factura').val()),
            hoy: !esNota ? $('#venta-fecha_emision').val() : $('#venta-emision_factura').val(),
            estado_comprobante: $('#venta-estado').val(),
            es_nota: esNota
        },
        success: function (data) {
            if (data !== "") {
                $.pjax.reload({container: "#asiento-simulator-div", async: false});
                $.pjax.reload({container: "#asiento-simulator-costo-div", async: false});
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
            }
        }
    });
}

//Retorna array con datos de la tabla de plantillas - ivas
function getDataTable() {
    let ivas = getIvas();
    let data_table = [];
    $('td.plantilla-selector :input:not([type="hidden"])').each(function () {
        if ($(this).val() !== "") { // tiene seleccionado una plantilla
            let id_prefijo = (/#ventaivacuentausada-new([0-9]+)-/g).exec('#' + $(this).prop('id'));
            if (id_prefijo != null) {
                let fila = {};
                for (let j = 0; j < ivas.length; j++) {
                    fila['iva_' + ivas[j]] = $(id_prefijo[0] + 'ivas-iva_' + ivas[j]).val();
                }
                fila["plantilla_id"] = $(this).val();
                data_table.push(fila);
            }
        }
    });
    return data_table;
}

function obtenerTotalFactura() { // al modificar este, tambien modificar en _form_detalle.php
    var factura_total = 0.0;

    // calcular suma de todos los totales
    let ivas = getIvas();
    ivas.forEach(function (element) {
        $('td.column-iva-' + element + ' :input').each(function () {
            let id = '#' + $(this).attr('id');
            const regex = /#ventaivacuentausada-new([0-9]+)-/gm;
            let m = regex.exec(id);
            if (m !== null) {
                if ($(this).val() !== "")
                    factura_total += (parseFloat($(this).val()));
            }
        })
    });

    return factura_total;
}

function hayPlantillaSeleccionada() {
    let hay = false;
    $('td.plantilla-selector :input').each(function () {
        if ($(this).val() !== "") hay = true;
    });
    return hay;
}

function getIvas() {
   return Object.values(JSON.parse("$porcentajes_iva"))
}

function setTotalFactura() { //console.log('setTotalFactura()');
    if (!['anulada', 'faltante'].includes($('#venta-estado').val()) && hayPlantillaSeleccionada() && $("#venta-total-disp").val() > 0) {
        // inhabilitar temporalmente boton submit
        $('#btn_submit_factura_venta').prop('disabled', true);
        $('#btn_submit_close_factura_venta').css('display', 'none');
        $('#btn_submit_factura_venta').html('Generando detalles y simulacion ... ');

        //calcular ivas y mostrar en los campos disableds de ivas
        // calcularIvasYRellenar();
        // $('#venta-factura_venta_id').select2('data')[0]
        let tipo_docu_id = !esNota ? $('#venta-tipo_documento_id').val() :
         ($('#venta-tipo').select2('data')[0]['id'] === "nota_credito" ? "{$tipodocNotaCreditoId}" : "{$tipodocNotaDebitoId}");
        $.ajax({
            url: $urlCalcDetalle,
            type: 'post',
            data: {
                moneda_id: $('#venta-moneda_id').val(),
                valor_moneda: $('#venta-valor_moneda' + (!esNota ? '-disp' : '')).val(),
                cont_factura_total: obtenerTotalFactura(),
                plantillas: getDataTable(),
                tipo_docu_id: tipo_docu_id,
                ivas: getIvas(),
                estado_comprobante: $('#venta-estado').val(),
                es_nota: esNota
                // totales_valor: getTotalFieldValues(),
                // plantilla_id: $('#venta-plantilla_id').val(),
            },
            success: function (data) {
                if (data !== '') {// si retorna falso, aquí se vé como ''. si retorna true, aquí se vé como 1.
                    $.pjax.reload({container: "#detallesventa_grid", async: false});
                    simular();
                } else {
                    $.pjax.reload({container: "#flash_message_id", async: false});
                    
                    $('#btn_submit_factura_venta').prop('disabled', false);
                    $('#btn_submit_close_factura_venta').css('display', '');
                    $('#btn_submit_factura_venta').html('Guardar y siguiente');
                }

                // re-habilitar boton submit LO REHABILITA EN _form_asientoSimulator_costo.php en document.onReady
                // $('#btn_submit_factura_venta').prop('disabled', false);
                // $('#btn_submit_factura_venta').html('Guardar y siguiente');
            }
        });
    }
}

$('#venta-timbrado_detalle_id_prefijo').on('change', function () {
    let element1 = $('#venta-nro_factura');

    // Para usar en cargarPrefijo.
    prefijo_selected = $(this).val();
    let nro_setted = element1.val();

    if ($(this).val() !== '' && $(this).val() !== null) {
        var url = $url_nextnro;//+'&isExistentRecord='+"$model->id";
        $.ajax({
            url: url,
            type: 'post',
            data: {
                id_detalle: $(this).val(),
                model_id: "$model->id",
                action_id: "$actionId"
            },
            success: function (data) {
                var sel = document.getElementById("venta-timbrado_detalle_id_prefijo");
                var prefijo = sel.options[sel.selectedIndex].text;
                var data_array = data.split("|");
                var nro_actual = data_array[0];

                // TODO: No se porque esta logica pero se deja comentado por si sirva

                // if (nro_setted === '') {
                //     var lastgroup = Array((7 - nro_actual.length) + 1).join("0") + nro_actual;
                //     element1.val(lastgroup).trigger('change'); // el trigger para ejecutar validacion
                // }
                // else {
                //     element1.val(nro_setted).trigger('change');
                // }

                if (nro_actual !== "") {
                    var lastgroup = Array((7 - nro_actual.length) + 1).join("0") + nro_actual;
                    element1.val(lastgroup).trigger('change'); // el trigger para ejecutar validacion
                }

                $('#venta-nro_timbrado').val(data_array[1]);
            }
        });
    } else {
        element1.val('');
        $('#venta-nro_timbrado').val('');
    }
});
JS;

    $this->registerJs($script);
} catch (\Exception $exception) {
    Yii::warning($exception);
}