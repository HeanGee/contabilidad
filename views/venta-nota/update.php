<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Venta */

$this->title = 'Modificar Nota';
$this->params['breadcrumbs'][] = ['label' => 'Notas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notas-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
