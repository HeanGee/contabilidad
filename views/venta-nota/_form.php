<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 7/25/18
 * Time: 9:45 PM
 */

use backend\modules\contabilidad\assets\VentaNotaAssetBundle;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\Venta;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Venta */
/* @var $form yii\widgets\ActiveForm */

$bundle = VentaNotaAssetBundle::register($this);
$bundle->js[] = 'venta-nota.js';
$action = Yii::$app->controller->action->id;
$tipodocNotaCreditoId = null;
$tipodocNotaDebitoId = null;
$tipodocNotaCreditoId = Venta::getTipodocNotaCredito()->id;
$tipodocNotaDebitoId = Venta::getTipodocNotaDebito()->id;
$porcentajes_iva = [];
$ivas_por = IvaCuenta::find()->all();
foreach ($ivas_por as $iva_por)
    $porcentajes_iva[] = $iva_por->iva->porcentaje;

asort($porcentajes_iva);
$porcentajes_iva = array_values($porcentajes_iva);
$porcentajes_iva = Json::encode($porcentajes_iva);
?>

    <div class="venta-form">
        <?php
        $form = ActiveForm::begin(['id' => 'nota_form']);

        $boton = Html::label('Agregar', 'boton_add_timbrado', ['class' => 'control-label']) . '<br/>'
            . Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'id' => 'boton_add_timbrado',
                'style' => "display: yes;",
                'type' => 'button',
                'title' => 'Agregar.',
                'class' => 'btn btn-success tiene_modal2',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['timbrado/create-new-timbrado']),
                'data-pjax' => '0',
            ]);

        try {
            if (empty($model->tipo)) { // sólo cuando el registro es nuevo va a estar vacío
                $model->tipo = 'nota_credito';
            } elseif ($model->factura_venta_id != '') {
                $model->emision_factura = date('d-m-Y', strtotime($model->facturaVenta->fecha_emision));
            }

            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => false,
                'rows' => [
                    [
                        'attributes' => [
                            'tipo' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '1'],
                                'value' => $form->field($model, 'tipo')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione un tipo de Nota...'],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                        'data' => [
                                            [
                                                'id' => 'nota_credito',
                                                'text' => 'Nota Credito',
                                                'tipo_doc_set_id' => ParametroSistema::getValorByNombre('nota_credito_set_id'),
                                                'registro_tipo' => 'factura'
                                            ],
                                            [
                                                'id' => 'nota_debito',
                                                'text' => 'Nota Debito',
                                                'tipo_doc_set_id' => ParametroSistema::getValorByNombre('nota_debito_set_id'),
                                                'registro_tipo' => 'nota_credito'
                                            ]
                                        ],
                                        'disabled' => !empty($model->id)
                                    ],
                                    'pluginEvents' => [
                                        'change' => "function() {
                                            $().val('').trigger('change');
                                            manejarSeleccionTipo();
                                        }"
                                    ],
                                    'initValueText' => !empty($model->tipo) ? implode(' ', array_map('ucfirst', explode('_', $model->tipo))) : 'Credito'
                                ])
                            ]
                        ]
                    ]
                ]
            ]);

            echo '<fieldset><legend>Datos de Factura</legend></fieldset>';
            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'ruc' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '1'],
                            ],
                            'razon_social' => [
                                'columnOptions' => ['colspan' => '4'],
                                'type' => Form::INPUT_TEXT,
                                'options' => [
                                    'readonly' => true,
                                ],
                            ],
                            'factura_venta_id' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '3'],
                                'value' => $form->field($model, 'factura_venta_id')->widget(Select2::className(), [
                                    'options' => [
                                        'placeholder' => 'Seleccione una factura...',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'ajax' => [
                                            'url' => Url::to(['get-facturas-by-ruc']),
                                            'dataType' => 'json',
                                            'changeOnReset' => true,
                                            'pluginLoading' => false,
                                            'data' => new JsExpression("
                                                function(params) { 
                                                    return {
                                                        q:params.term, 
                                                        ruc:$('#venta-ruc').val(),
                                                        tipo: ($('#venta-tipo').val() === 'nota_credito') ? 'factura' : 'nota_credito',
                                                    };
                                                }
                                            "),
                                        ],
                                    ],
                                    'initValueText' => !empty($model->facturaVenta->nro_factura) ? $model->facturaVenta->getNroFacturaCompleto() : '',
                                ])->label(($model->tipo == 'nota_credito' ? 'Factura' : 'Nota') . '<span class="popuptrigger-factura-venta-id text-info glyphicon glyphicon-info-sign" style="padding: 0 4px;"></span>')
                            ],
                            'emision_factura' => [
                                'columnOptions' => ['colspan' => '2'],
                                'type' => Form::INPUT_TEXT,
                                'label' => 'Emisión ' . ($model->tipo == 'nota_credito' ? 'Factura' : 'Nota'),
                                'options' => [
                                    'readonly' => true
                                ]
                            ]
                        ]
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'moneda_id' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'moneda_id')->widget(Select2::className(), [
                                    'disabled' => false,
                                    'options' => [
                                        'placeholder' => 'Seleccione una moneda',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'data' => Venta::getMonedas(),
                                    ],
                                    'initValueText' => !empty($model->moneda_id) ? ($model->moneda_id . ' - ' . $model->moneda->nombre) : ''
                                ])
                            ],
                            'valor_moneda' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'valor_moneda')->widget(NumberControl::className(), [
                                    'value' => 0.00,
                                    'maskedInputOptions' => [
                                        'groupSeparator' => '.',
                                        'radixPoint' => ',',
                                        'rightAlign' => true,
                                        'allowMinus' => false
                                    ],
                                    'readonly' => true
                                ])
                            ]
                        ]
                    ]
                ]
            ]);
            //echo $form->field($model, 'moneda_id')->hiddenInput()->label(false);
//        // Trick para poder postear valores de campos disabled.
//        if (Yii::$app->controller->action->id == 'update') {
//            echo $form->field($model, 'factura_venta_id')->hiddenInput();
//            echo $form->field($model, 'ruc')->hiddenInput()->label(false);
//        }

            echo '<fieldset><legend>Datos de Nota</legend></fieldset>';
            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'tipo_documento_id' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'tipo_documento_id')->widget(Select2::className(), [
                                    'options' => [
                                        'placeholder' => 'Seleccione un tipo de doc...',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'ajax' => [
                                            'url' => Url::to(['tipo-documento/get-tipodoc-para-nota']),
                                            'dataType' => 'json',
                                            'data' => new JsExpression("
                                            function(params) { 
                                                return {
                                                    q:params.term, 
                                                    asociacion: 'cliente',
                                                    id: '" . ($action == 'update' ? $model->id : '') . "',
                                                    para: $('#venta-tipo').select2('data')[0]['id'],
                                                };
                                            }
                                        "),
                                        ],
                                    ],
                                    'initValueText' => ($model->tipo_documento_id != '') ? "{$model->tipo_documento_id} - {$model->tipoDocumento->nombre}" : ''
                                ]),
                            ],
                            'fecha_emision' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'fecha_emision')->widget(MaskedInput::className(), [
                                    'name' => 'fecha_emision',
                                    'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                                ]),
                            ],
                            'timbrado_detalle_id_prefijo' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'timbrado_detalle_id_prefijo')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione uno ...'],
                                    'initValueText' => !empty($model->timbrado_detalle_id_prefijo) ? $model->prefijo : '',
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'ajax' => [
                                            'url' => Url::to(['timbrado/get-prefs-emp-actual']),
                                            'dataType' => 'json',
                                            'data' => new JsExpression("
                                            function(params) { 
                                                return {
                                                    q:params.term, 
                                                    id:$('#venta-tipo').select2('data')[0]['tipo_doc_set_id'],
                                                    es_tipo_set:1,
                                                    fecha_factura: $('#venta-fecha_emision').val(),
                                                    action_id: '$action',
                                                    model_id: '$model->id',
                                                    vencido: '0',
                                                };
                                            }
                                        "),
                                        ]
                                    ]
                                ])->label('Prefijo ' . '<span class="popuptrigger-prefijo text-info glyphicon glyphicon-info-sign" style="padding: 0 4px;"></span>')
                            ],
                            'nro_factura' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '1'],
                                'value' => $form->field($model, 'nro_factura')->widget(MaskedInput::className(), [
                                    'mask' => '9999999',
                                    'options' => [
                                        'class' => 'form-control',
                                        'readonly' => false,
                                    ]
                                ])->label('Nro. de Nota'),
                                'options' => ['placeholder' => 'Ingrese nro de Nota...'],
                            ],
                            'nro_timbrado' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '1'],
                                'options' => ['readonly' => true]
                            ],
                            'obligacion_id' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'obligacion_id')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione una obligac...'],
                                    'pluginOptions' => [
                                        'data' => Venta::getObligacionesEmpresaActual(Yii::$app->session->get('core_empresa_actual'), ($action == 'update') ? $model : null),
                                        'allowClear' => false,
                                    ],
                                    'initValueText' => isset($model->obligacion) ? ucfirst($model->obligacion->nombre) : "",
                                ]),
                            ],
                        ]

                    ]
                ]
            ]);

            $totales = [];
            $gravadas = [];
            /** @var IvaCuenta $iva_cta */
            foreach (IvaCuenta::find()->all() as $iva_cta) {
                $totales['iva-' . $iva_cta->iva->porcentaje . ''] = [
                    'label' => 'Total ' . (($iva_cta->iva->porcentaje !== 0) ? ' ' . $iva_cta->iva->porcentaje . '%' : 'Exenta'),
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::class,
                    'columnOptions' => ['colspan' => '2'],
                    'options' => [
                        'value' => 0.00,
                        'maskedInputOptions' => [
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'rightAlign' => true,
                            'allowMinus' => false,
                        ],
                        'readonly' => true,
                    ],
                ];
                if ($iva_cta->iva->porcentaje != 0) {
                    $gravadas['gravada-' . $iva_cta->iva->porcentaje . ''] = [
                        'label' => 'I.V.A. ' . $iva_cta->iva->porcentaje . '%',
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => NumberControl::class,
                        'columnOptions' => ['colspan' => '2'],
                        'options' => [
                            'value' => 0.00,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'radixPoint' => ',',
                                'rightAlign' => true,
                                'allowMinus' => false,
                            ],
                            'readonly' => true,
                        ],
                    ];
                }
            }
            // Ordenar campos
            uksort($totales, 'strnatcasecmp');
            uksort($gravadas, 'strnatcasecmp');

            // concatenar para renderizar con Form::widget
            $totales = array_merge($totales, $gravadas);
            echo Form::widget([
                // formName is mandatory for non active forms
                // you can get all attributes in your controller
                // using $_POST['kvform']
                'formName' => 'totals',

                // default grid columns
                'columns' => 12,

                // set global attribute defaults
                'attributeDefaults' => [
                    'type' => Form::INPUT_TEXT,
                    'labelOptions' => ['colspan' => '2'],
                    'inputContainer' => ['colspan' => '2'],
                    'container' => ['class' => 'form-group totales'],
                ],

                'attributes' => $totales,
            ]);

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 2,
                'attributes' => [       // 1 column layout
                    'total' => [
                        'label' => 'Total Factura',
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => NumberControl::class,
                        'options' => [
                            'readonly' => true,
                            'class' => 'form-control .total-class',
                            'value' => 0.00,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'radixPoint' => ',',
                                'rightAlign' => true,
                                'allowMinus' => false,
                            ],
                        ],
                    ],
                    '_total_ivas' => [
                        'label' => 'Total I.V.A.',
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => NumberControl::class,
                        'options' => [
                            'readonly' => true,
                            'class' => 'form-control .total-class',
                            'value' => 0.00,
                            'maskedInputOptions' => [
                                'groupSeparator' => '.',
                                'radixPoint' => ',',
                                'rightAlign' => true,
                                'allowMinus' => false,
                            ],
                        ],
                    ],
                ],
            ]);

            $botonText = 'Guardar';
            $botonClass = 'btn btn-';
            if (Yii::$app->controller->action->id == 'create') {
                $botonText .= ' y Siguiente';
                $botonClass .= 'success';
            } else {
                $botonClass .= 'primary';
            }
            echo '<div class="form-group">';
            echo Html::submitButton($botonText, ['id' => 'btn_submit_factura_venta', 'class' => $botonClass,
                "data" => (Yii::$app->controller->action->id == 'update') ? [
                    'confirm' => 'Desea guardar los cambios?',
                    'method' => 'post',
                ] : []
            ]);
            echo '</div>';

            //echo $this->render('../venta/_form2_js', ['totales' => $totales, 'model' => $model, 'boton' => $boton]);
            echo $this->render('plantilla/_form_plantillas', ['model' => $model, 'form' => $form]);
            $bluePrint = Yii::$app->session->get('$bluePrint');
            $bluePrint = Yii::$app->session->remove('$bluePrint');
            echo $this->render('detalle/_form_detalle', []);
            echo $this->render('../venta/_form_asientoSimulator');
            echo $this->render('../venta/_form_asientoSimulator_costo');
            echo $this->render('js-files/modal-manager-js', ['model' => $model]);
            echo Html::button('', ['class' => 'btn-simular hidden']);

            ActiveForm::end();

            $nuevo = $model->isNewRecord;
            $entidades_url = Url::to(['venta-nota/get-entidades-for-notas', 'action' => Yii::$app->controller->action->id]);
            $popoverTitle = <<<HTML
<strong>title_replace<br></strong>
HTML;
            $infoColor = \backend\helpers\HtmlHelpers::InfoColorHex(true);
            $js_script_ready = <<<JS
//if ("$nuevo".length) {
//    $('#venta-ruc').select2('open');
//}
// TODO: si es credito, al cambiar fecha de emision debe setear cotizacion
// TODO: si es debito, al cambiar fecha de factura? o sea al cambiar factura? o al cambiar fecha de emision.

$(document).on('select2:select', '#venta-timbrado_detalle_id_prefijo', function () {
    let inputFechaEmision = $('#venta-fecha_emision');
    if ($(this).val() !== '' && inputFechaEmision.val() !== '') {
        let select2 = $(this);
        // $('html, body').stop().animate({
        //     scrollTop: $('label[for="venta-total"]').offset().top
        // }, 500);
    }
});
$(document).ready(function () {
    //$('#venta-moneda_id').prop('disabled', true);
    
    // Para que se formatee el prefijo?
    let rucField = $('#venta-ruc'); // es el campo de seleccion de entidades, por eso se formatea el prefijo al cambiar este.
    if (rucField.val().length) rucField.trigger('change');
    
    var css = {'background-color': '#$infoColor', 'color': 'white', 'text-align': 'center', 'font-weight': 'bold'}; //D9EDF7
    // Popover para selector de factura.
    popoverTitle = "{$popoverTitle}".replace('title_replace', 'Atención:');
    popOverContent = 'Si no aparece ninguna factura a elegir, revise cada una de las facturas asociadas a la entidad elegida' +
        ' y asegúrese de que cada una de las mismas tengan asociadas al menos una plantilla.';
    applyPopOver($('span[class*="popuptrigger-factura-venta-id"]'), popoverTitle, popOverContent, {'title-css': css});

    // Popover para prefijo
    popoverTitle = "{$popoverTitle}".replace('title_replace', 'Atención:');
    popOverContent = '<u>Si no aparece ningún prefijo a elegir, verifique lo siguiente</u>:<br/><br/>' +
        '<ol>' +
        '<li>Existe al menos un timbrado de la empresa actual con un detalle asociado al tipo de documento set para notas de crédito (o débito).</li>' +
        '<li>Existe en el parámetro de contabilidad, un registro de nombre <strong>`nota_credito_set_id`</strong> ' +
        '(o <strong>`nota_debito_set_id`</strong> según corresponda) asociado correctamente al ID del Tipo de Documento SET correspondiente.</li>' +
        '<li>Si existe ya un timbrado según 1 y 2:</li>' +
        '<ol>' +
        '<li>Verificar que sus fechas de inicio y fin de vigencia estén correctamente establecidas.</li>' +
        '<li>Verificar que la fecha de emisión de esta nota esté comprendido en el periodo de vigencia del timbrado.</li>' +
        '<li>Verificar que el periodo actual configurado corresponda con el periodo de vigencia del timbrado.</li>' +
        '</ol>' +
        '</ol>';
    let template = ['<div class="popover prefijo-popover" role="tooltip">',
        '<div class="arrow"></div>',
        '<h3 class="popover-title"></h3>',
        '<div class="popover-content"></div>',
        '</div>',
        '</div>'].join('');
    applyPopOver($('span[class*="popuptrigger-prefijo"]'), popoverTitle, popOverContent, {
        'theme-color': 'info-l',
        'placement': 'right',
        'template': template
    });

    delete popoverTitle;
    delete popOverContent;

});
JS;
            $plantillasForSelect2 = Json::encode(PlantillaCompraventa::getPlantillasLista('venta', false, false));
            $ivasPorPlantilla = [];
            foreach (Json::decode($plantillasForSelect2) as $plantilla) {
                foreach (PlantillaCompraventa::findOne(['id' => $plantilla['id']])->plantillaCompraventaDetalles as $plDetalle) {
                    $ivasPorPlantilla[$plantilla['id']][] = (isset($plDetalle->ivaCta) ? $plDetalle->ivaCta->iva->porcentaje : 0);
                }
            }
            $ivasPorPlantilla = Json::encode($ivasPorPlantilla);

            $js_script_head = <<<JS
function manejarSeleccionTipo() {
    // $("#venta-ruc").data('select2').trigger('select', {data: {"id": '', "text": ''}});
    $("#venta-ruc").val('').trigger('change');
    let venta_tipo = $('#venta-tipo').select2('data')[0];
    esNota = venta_tipo['id'];
    let tipo = venta_tipo['registro_tipo'];
    if (tipo.indexOf('_') > 0)
        tipo = tipo.substring(0, tipo.indexOf('_'));
    tipo = tipo.charAt(0).toUpperCase() + tipo.slice(1);
    $('#venta-factura_venta_id').prev('label').html(tipo);
    $('#venta-emision_factura').prev('label').html('Emisión ' + tipo);
    $('#select2-venta-factura_venta_id-container span.select2-selection__placeholder').html('Seleccione una ' + tipo + '...');
}
var facturaVentaIdInicial = "{$model->factura_venta_id}";
var data = $plantillasForSelect2;
var plantillasForSelect2 = $plantillasForSelect2;  // usado desde _form_plantillas.php
var plantillaOrden = 1;
var ivasPorPlantilla = $ivasPorPlantilla;
const bluePrint = "$bluePrint";
var simulando = false;
var fromDocReady = false;
//var rendering_plantilla_byupdate = false;
JS;
            $this->registerJs($js_script_head, $this::POS_HEAD);
            $this->registerJs($js_script_ready);

            $css = <<<CSS
.popover.prefijo-popover {
    max-width: none;
    width: 450px;
}
CSS;
            $this->registerCss($css);
        } catch (\Exception $e) {
            print $e;
        } ?>

    </div>

<?php //ob_start(); ?>
    <script>
        const action_id = '<?= $action ?>';
        const boton = '<?= $boton ?>';
        const nota_id = '<?= $model->id; ?>';
        const tipoDocNtaCreditoId = '<?= $tipodocNotaCreditoId ?>';
        const tipoDocNotaDebitoId = '<?= $tipodocNotaDebitoId ?>';
        const url_calcularDetalle = '<?= Url::to(['calc-detalle']) ?>';
        const url_datosFacturaForNota = '<?= Url::to(['venta-nota/get-datos-facturas-by-id']) ?>';
        const url_getCotizacion = '<?= Url::to(['detalle-venta/get-cotizacion']) ?>';
        const url_getFacturasByRuc = '<?= Url::to(['venta-nota/get-facturas-by-ruc']) ?>';
        const url_getPlantillas = '<?= $url_getPlantillas = Url::to(['plantilla-compraventa/get-plantilla']); ?>';
        const url_getRazonSocialByRuc = '<?= Url::to(['venta/get-razon-social-by-ruc']) ?>';
        const url_simular = '<?= Url::to(['simular']) ?>';
        var esMarte = 'si';
        var esNota = '<?= empty($model->tipo) ? 'nota_credito' : $model->tipo ?>';
        var porcentajes_iva = "<?= $porcentajes_iva ?>";
    </script>
<?php //$this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean()), View::POS_HEAD); ?>