<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Venta */

$this->title = 'Nueva Nota';
$this->params['breadcrumbs'][] = ['label' => 'Notas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nota-venta-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
