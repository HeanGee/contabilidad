<?php

use backend\modules\contabilidad\models\Venta;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\VentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notas Crédito/Débito';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nota-venta-index">

    <p>
        <?= Html::a('Registrar Nota', ['create'], ['id' => 'boton_crear_nota', 'class' => 'btn btn-success']) ?>
    </p>

    <?php
    $template = '';
    foreach (['view', 'update', 'delete', 'bloquear', 'desbloquear'] as $_v)
        if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-venta-nota-{$_v}"))
            $template .= "&nbsp&nbsp&nbsp{{$_v}}";
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'hover' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'label' => 'Nro Comprobante',
                    'value' => 'nro_completo',
                    'attribute' => 'nro_completo',
                ],
                'fecha_emision',
                [
                    'label' => 'Cliente',
                    'attribute' => 'nombre_entidad',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return "{$model->entidad->razon_social} <strong>(RUC: {$model->entidad->ruc})</strong>";
                    }
                ],
                [
                    'attribute' => 'total',
                    'value' => 'formattedTotal',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'label' => 'Tipos',
//                    'attribute' => 'nombre_documento',
//                    'value' => 'tipoDocumento.nombre',
                    'attribute' => 'tipo_documento_id',
                    'value' => 'tipoDocumento.nombre',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'tipo_documento_id',
                        'data' => Venta::getTipoDocumentos(),
//                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '250px'
                        ],
                        'initValueText' => 'todos',
                    ])
                ],
                [
                    'attribute' => 'tipo_documento_id',
                    'label' => 'Tipo de Documento',
                    'value' => function ($model) {
                        return $model->tipoDocumento->nombre;
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'tipo_documento_id',
                        'data' => Venta::getTipoDocumentos(),
                        'pluginOptions' => [
                            'width' => '250px'
                        ],
                        'initValueText' => 'todos',
                    ])
                ],
                [
                    'label' => 'Timbrado',
                    'attribute' => 'nro_timbrado',
                    'value' => 'nroTimbrado',
                ],
                [
                    'attribute' => 'moneda_id',
                    'value' => 'moneda.nombre',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'moneda_id',
                        'data' => Venta::getMonedas(true),
                        'pluginOptions' => [
                            'width' => '120px'
                        ],
                        'initValueText' => 'todos',
                    ])
                ],
                [
                    'attribute' => 'estado',
                    'value' => 'estado',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => Venta::getEstados(true),
                        'pluginOptions' => [
                            'width' => '120px'
                        ]
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        /** @var Venta $model */
                        'update' => function ($url, $model) {
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Editar'),
                            ]) : '';
                        },
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('yii', 'Ver'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            /** @var Venta $model */
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => '',
                                'title' => Yii::t('yii', 'Borrar'),
                                'data' => [
                                    'confirm' => '¿Está seguro de eliminar este elemento?',
                                    'method' => 'post',
                                ],
                            ]) : '';
                        },
                        'bloquear' => function ($url, $model, $key) {
                            /** @var Venta $model */
                            $url = ($model->bloqueado == 'no') ?
                                Html::a('<span>Bloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => Yii::t('app', 'Bloquear'),
                                        'data-confirm' => 'Está seguro que desea bloquear la nota?'
                                    ]) : '';
                            return $url;
                        },
                        'desbloquear' => function ($url, $model, $key) {
                            /** @var Venta $model */
                            $url = ($model->bloqueado == 'si') ?
                                Html::a('<span>Desbloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-success btn-xs',
                                        'title' => Yii::t('app', 'Desbloquear'),
                                        'data-confirm' => 'Está seguro que desea desbloquear la nota?'
                                    ]) : '';
                            return $url;
                        },
                    ],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $e) {
//        print $e;
    }
    ?>
</div>
