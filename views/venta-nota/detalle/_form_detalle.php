<?php

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
?>
<?php Pjax::begin(['id' => 'detallesventa_grid']); ?>
<?php

$columns = [
    [
        'class' => SerialColumn::className(),
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Código',
        'value' => 'planCuenta.cod_completo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Cuenta',
        'value' => 'planCuenta.nombre',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
//    [
//        'value' => 'subtotalFormat',
//        'label' => 'Monto',
//        'contentOptions' => ['style' => 'font-size: 90%;'],
//        'headerOptions' => ['style' => 'font-size: 90%;'],
//    ],
//    [
//        'value' => 'cta_contable',
//        'label' => 'Debe / Haber',
//        'contentOptions' => ['style' => 'font-size: 90%;'],
//        'headerOptions' => ['style' => 'font-size: 90%;'],
//    ],
    [
        'value' => 'subtotalDebe',
        'label' => 'Debe',
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'value' => 'subtotalHaber',
        'label' => 'Haber',
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'value' => 'cta_principal',
        'label' => 'PRINCIPAL',
        'contentOptions' => ['style' => 'width: 50px; font-size: 90%; text-align: center;'],
        'headerOptions' => ['style' => 'font-size: 90%; text-align: center;'],
    ],
];

$template = '{update}';
$buttons = [
    'delete' => function ($url, $model, $index) {
        return Html::a(
            '<span class="glyphicon glyphicon-trash"></span>',
            false,
            [
                'class' => 'ajaxDelete',
                'delete-url' => $url,
                'id' => $index,
                'title' => Yii::t('app', 'Borrar')
            ]
        );
    },
    'update' => function ($url, $model) {
        return Html::a(
            '<span class="glyphicon glyphicon-pencil"></span>',
            false,
            [
                'title' => 'Editar cuenta',
                'class' => 'tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => $url,
                'data-pjax' => '0',
                'data-title' => 'Editar cuenta'
            ]
        );
    }
];

array_push($columns, [
    'class' => ActionColumn::class,
    'template' => $template,
    'buttons' => $buttons,
    'urlCreator' => function ($action, $model, $key, $index) {
        if ($action === 'delete') {
            $url = Url::to(['detalle-venta/borrar-cuenta', 'indice' => $index]);
            return $url;
        } elseif ($action === 'update') {
            $url = Url::to(['detalle-venta/modificar-cuenta', 'indice' => $index]);
            return $url;
        }
        return '';
    },
    'contentOptions' => ['style' => 'font-size: 90%;'],
    'headerOptions' => ['style' => 'font-size: 90%;'],
]);

try {
    echo GridView::widget([
        'dataProvider' => Yii::$app->getSession()->get('cont_detalleventa-provider'),
        'columns' => $columns,
        'hover' => true,
        'id' => 'grid',
        'panel' => ['type' => 'primary', 'heading' => 'Detalles', 'footer' => false,],
        'toolbar' => [],
    ]);
} catch (Exception $e) {
    throw  $e;
}

$scripts = [];

$scripts[] = <<<JS
$(document).ready(function() {
    $($('#grid')[0].getElementsByTagName('tr')).each(function () {
        if (typeof $(this)[0].getElementsByTagName('td')[0] !== 'undefined') {
            if ($(this)[0].getElementsByTagName('td').length > 3 && $(this)[0].getElementsByTagName('td')[3].textContent === '') {
                $($(this)[0].getElementsByTagName('td')[2]).css('text-align', 'right');
            }
        }
    });
});
JS;

foreach ($scripts as $s) $this->registerJs($s);

?>
<?php Pjax::end() ?>