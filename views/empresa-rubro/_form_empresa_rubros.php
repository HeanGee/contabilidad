<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 05/07/2018
 * Time: 14:16
 */

use backend\modules\contabilidad\models\EmpresaRubro;
use backend\modules\contabilidad\models\Rubro;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $dataProvider ActiveDataProvider */
?>

<?php try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'id' => 'empresa_rubros_grid',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'id' => 'empresa_rubros_pjax_grid'
            ],
            'loadingCssClass' => false,
        ],
        'panel' => [
            'type' => 'primary',
            'heading' => 'Rubros',
            'after' => '<em>* Para confirmar los datos de los rubros se debe guardar la empresa.</em>'
        ],
        'striped' => true,
        'hover' => true,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'options' => ['id' => 'rubro_por_empresa_selections',],
                'headerOptions' => [
                    'class' => 'kartik-sheet-style',
                    'onchange' => '
                        $.ajax({
                            type: "POST",
                            url: "index.php?r=empresa/guardar-rubros-en-session&all=true",
                            data: {seleccionados: $("#empresa_rubros_grid").yiiGridView("getSelectedRows")},
                            success: function (data) {
                            }
                        });'
                ],
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    $empresa_rubros = EmpresaRubro::findAll(['empresa_id' => \Yii::$app->session->get('core_empresa_en_edicion')]);
                    $checked = false;
                    foreach ($empresa_rubros as $empresa_rubro) {
                        if ($model instanceof Rubro && $model->id == $empresa_rubro->rubro_id) {
                            $checked = true;
                        }
                    }

                    return [
                        'onchange' => '
                            $.ajax({
                                type: "POST",
                                url: "index.php?r=empresa/guardar-rubros-en-session&checked=" + this.checked,
                                data: {id_registro_seleccionado: ' . $model->id . '},
                                success: function (data) {
                                }
                            });',
                        'checked' => $checked
                    ];
                },
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'nombre',
                'label' => 'Nombre',
                'value' => 'nombre',
                'hidden' => false,
            ],
            'descripcion',
        ],
        'toolbar' => []
    ]);
} catch (Exception $e) {
    print $e;
}
?>