<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\CoeficienteRevaluo */

$this->title = Yii::t('app', 'Modificar Coeficiente de Revaluo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coeficiente de Revaluo'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coeficiente-revaluo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
