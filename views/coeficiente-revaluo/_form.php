<?php

use common\helpers\PermisosHelpers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\CoeficienteRevaluo */
/* @var $form ActiveForm */
?>

<div class="coeficiente_revaluo-form">
    <?php
    $form = ActiveForm::begin();
    try {
        $minViewMode = 'months';
        $format = 'mm-yyyy';
        $mostar_extra = false;

        echo '<fieldset><legend>Datos Principales</legend></fieldset>';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
            [
                    'autoGenerateColumns' => false,
                    'columns' => 2,
                    'attributes' => [
                        'periodo' => [
                            'type' => Form::INPUT_RAW,
                            'placeholder' => 'Periodo',
                            'value' => $form->field($model, 'periodo')->widget(DatePicker::className(), [
                                  'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                  'language' => 'es',
                                  'pluginOptions' => [
                                        'autoclose' => true,
                                        'startView' => 'year',
                                        'minViewMode' => $minViewMode,
                                        'format' => $format,
                                        'language' => 'es',
                                  ],
                            ])
                        ],
                        'resolucion' => [
                          'type' => Form::INPUT_TEXT,
                          'options' => [
                            'placeholder ' => 'Agregar nro de resolución...']
                        ],
                    ],
            ],
            ]
        ]);

        echo '<br/><fieldset><legend>Existencias</legend></fieldset>';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 6,
                    'attributes' => [
                        'enero_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Enero'
                        ],
                        'febrero_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Febrero'
                        ],
                        'marzo_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Marzo'
                        ],
                        'abril_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Abril'
                        ],
                        'mayo_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Mayo'
                        ],
                        'junio_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Junio'
                        ],
                        'julio_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Julio'
                        ],
                        'agosto_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Agosto'
                        ],
                        'septiembre_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Septiembre'
                        ],
                        'octubre_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Octubre'
                        ],
                        'noviembre_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Noviembre'
                        ],
                        'diciembre_existencia' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Diciembre'
                        ],
                    ]
                ],
            ]
        ]);

        echo '<br/><fieldset><legend>Enajenaciones</legend></fieldset>';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 6,
                    'attributes' => [
                        'enero_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Enero'
                        ],
                        'febrero_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Febrero'
                        ],
                        'marzo_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Marzo'
                        ],
                        'abril_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Abril'
                        ],
                        'mayo_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Mayo'
                        ],
                        'junio_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Junio'
                        ],
                        'julio_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Julio'
                        ],
                        'agosto_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Agosto'
                        ],
                        'septiembre_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Septiembre '
                        ],
                        'octubre_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Octubre'
                        ],
                        'noviembre_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Noviembre'
                        ],
                        'diciembre_enajenacion' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => MaskedInput::className(),
                            'options' => [
                                'clientOptions' => [
                                    'rightAlign' => false,
                                    'alias' => 'decimal',
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'autoGroup' => true
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                ]
                            ],
                            'label' => 'Diciembre'
                        ],
                    ]
                ],
            ]
        ]);
    } catch (\Exception $e) {
        print $e;
    }
    ?>


    <div class="form-group">
            <?= PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-create') ? Html::submitButton($model->isNewRecord ? 'Crear' : 'Guardar',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) : '' ?>
       </div>
    <?php ActiveForm::end(); ?>
</div>
