<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\CoeficienteRevaluo */

$this->title = 'Cargar Coeficiente de Revaluo';
$this->params['breadcrumbs'][] = ['label' => 'Coeficientes de Revaluo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coeficiente-revaluo-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
