<?php

use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\CoeficienteRevaluoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coeficientes de Revalúo';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="coeficiente-revaluo-index">

<?php Pjax::begin(); ?>

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-create') ? Html::a('Cargar Coeficiente de Revalúo', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>
<?php
try {
    $template = '';
    $template = '';
    $template = $template . (PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-view') ? '{view} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-update') ? '{update} &nbsp&nbsp&nbsp' : '');
    $template = $template . (PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-delete') ? '{delete} &nbsp&nbsp&nbsp' : '');

    echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'hover' => true, 'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                            'attribute' => 'periodo',
                            'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'periodo',
                                    'language' => 'es',
                                    'pluginOptions' => [
                                            'autoclose' => true,
                                            'startView' => 'year',
                                            'minViewMode' => 'months',
                                            'format' => 'mm-yyyy',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                    ],
                                    'options' => [
                                            'class' => 'no_tomar'
                                    ],
                            ]),
                            'format' => 'html',
                    ],
                    'resolucion',
                    [
                            'class' => 'yii\grid\ActionColumn',
                            'buttons' => [],
                            'template' => $template,
                    ]
        ],
    ]);
} catch (Exception $e) {
    throw $e;
}
Pjax::end();
?>