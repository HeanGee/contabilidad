<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\search\CoeficienteRevaluoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coeficiente-revaluo-search">

    <?php $form = ActiveForm::begin(['action' => ['index'], 'method' => 'get',]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'periodo') ?>

    <?= $form->field($model, 'resolucion') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
