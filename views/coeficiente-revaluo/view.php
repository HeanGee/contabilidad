<?php

use backend\modules\contabilidad\models\CoeficienteRevaluo;
use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\CoeficienteRevaluo */

$this->title = 'Coeficientes de Revalúo';
$this->params['breadcrumbs'][] = ['label' => 'Coeficientes de Revalúo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coeficiente-revaluo-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-coeficiente-revaluo-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Coef. Reval.', ['index'], ['class' => 'btn btn-info']) : '' ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= $permisos['delete'] ? Html::a('Eliminar', ['delete', 'id' => $model->id],
            ['class' => 'btn btn-danger', 'data' => [
                'confirm' => '¿Está seguro de querer borrar?', 'method' => 'post',],
        ]) : '' ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
          'model' => $model,
          'hover' => true,
          'mode' => DetailView::MODE_VIEW,
          'enableEditMode' => false,
          'fadeDelay' => true,
          'panel' => [
            'heading' => 'Detalles',
            'type' => DetailView::TYPE_INFO,
          ],
          'attributes' => [
              'periodo',
              'resolucion',
          ],
        ]);

    } catch (Exception $e) {
        throw $e;
    }

    try {
        echo DetailView::widget([
            'model' => $model,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Coeficientes de revalúo para los bienes de Existencia del mes:',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'attribute' => 'enero_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->enero_existencia),
                    'label' => 'Enero'
                ],
                [
                    'attribute' => 'febrero_existencia',
                    'value' =>CoeficienteRevaluo::formatNumber($model->febrero_existencia),
                    'label' => 'Febrero'
                ],
                [
                    'attribute' => 'marzo_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->marzo_existencia),
                    'label' => 'Marzo'
                ],
                [
                    'attribute' => 'abril_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->abril_existencia),
                    'label' => 'Abril'
                ],
                [
                    'attribute' => 'mayo_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->mayo_existencia),
                    'label' => 'Mayo'
                ],
                [
                    'attribute' => 'junio_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->junio_existencia),
                    'label' => 'Junio'
                ],
                [
                    'attribute' => 'julio_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->julio_existencia),
                    'label' => 'Julio'
                ],
                [
                    'attribute' => 'agosto_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->agosto_existencia),
                    'label' => 'Agosto'
                ],
                [
                    'attribute' => 'septiembre_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->septiembre_existencia),
                    'label' => 'Septiembre'
                ],
                [
                    'attribute' => 'octubre_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->octubre_existencia),
                    'label' => 'Octubre'
                ],
                [
                    'attribute' => 'noviembre_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->noviembre_existencia),
                    'label' => 'Noviembre'
                ],
                [
                    'attribute' => 'diciembre_existencia',
                    'value' => CoeficienteRevaluo::formatNumber($model->diciembre_existencia),
                    'label' => 'Diciembre'
                ],
            ],
        ]);

    } catch (Exception $e) {
        throw $e;
    }

    try {
        echo DetailView::widget([
            'model' => $model,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Coeficientes de revalúo de Enajenaciones realizadas en el mes:',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [

                [
                    'attribute' => 'enero_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->enero_enajenacion),
                    'label' => 'Enero'
                ],

                [
                    'attribute' => 'febrero_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->febrero_enajenacion),
                    'label' => 'Febrero'
                ],

                [
                    'attribute' => 'marzo_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->marzo_enajenacion),
                    'label' => 'Marzo'
                ],

                [
                    'attribute' => 'abril_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->abril_enajenacion),
                    'label' => 'Abril'
                ],

                [
                    'attribute' => 'mayo_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->mayo_enajenacion),
                    'label' => 'Mayo'
                ],

                [
                    'attribute' => 'junio_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->junio_enajenacion),
                    'label' => 'Junio'
                ],

                [
                    'attribute' => 'junio_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->junio_enajenacion),
                    'label' => 'Julio'
                ],
                [
                    'attribute' => 'agosto_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->agosto_enajenacion),
                    'label' => 'Agosto'
                ],
                [
                    'attribute' => 'septiembre_enajenacion',
                    'value' =>CoeficienteRevaluo::formatNumber($model->septiembre_enajenacion),
                    'label' => 'Septiembre'
                ],

                [
                    'attribute' => 'octubre_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->octubre_enajenacion),
                    'label' => 'Octubre'
                ],

                [
                    'attribute' => 'noviembre_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->noviembre_enajenacion),
                    'label' => 'Noviembre'
                ],
                [
                    'attribute' => 'diciembre_enajenacion',
                    'value' => CoeficienteRevaluo::formatNumber($model->diciembre_enajenacion),
                    'label' => 'Diciembre'
                ],
            ],
        ]);

    } catch (Exception $e) {
        throw $e;
    } ?>

</div>
