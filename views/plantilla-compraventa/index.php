<?php

use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\PlantillaCompraventaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $tipo string */

$this->title = \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta' ? 'Plantillas de Venta' : 'Plantillas de Compra';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantilla-compraventa-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if (\common\helpers\PermisosHelpers::getAcceso('contabilidad-plantilla-compraventa-create')) {
            echo Html::a('Crear nueva plantilla', ['create', 'tipo' => $tipo], ['id' => 'boton_crear_plantilla', 'class' => 'btn btn-success']);
        } ?>
    </p>

    <?php

    $template = '';
    //    foreach(['view', 'update', 'delete', 'ver_detalles'] as $_v)
    foreach (['view', 'update', 'delete'] as $_v)
        if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-plantilla-compraventa-{$_v}"))
            $template .= "&nbsp&nbsp&nbsp{{$_v}}";
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'value' => 'id',
                    'width' => '60px',
                ],
                'nombre',
                [
                    'attribute' => 'deducible',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->deducible == 'si') ? 'success">Sí' : 'danger">No') . '</label>';
                    },
                    'width' => '60px',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'deducible',
                        'data' => ['si' => 'Sí', 'no' => 'No'],
                        'hideSearch' => true,
                    ]),
                    'contentOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'width' => '60px',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => ['activo' => 'Activo', 'inactivo' => 'Inactivo'],
                        'hideSearch' => true,
                    ]),
                    'contentOptions' => ['style' => 'text-align:center;']
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' =>
                        [
                            'update' => function ($url, $model) {
                                return Html::a('', "index.php?r=contabilidad%2Fplantilla-compraventa%2Fupdate&id=" . $model->id . '&tipo=' . $model->tipo, ['class' => 'glyphicon glyphicon-pencil']);
                            },
                            'view' => function ($url, $model) {
                                return Html::a('', "index.php?r=contabilidad%2Fplantilla-compraventa%2Fview&id=" . $model->id . '&tipo=' . $model->tipo, ['class' => 'glyphicon glyphicon-eye-open']);
                            },
                        ],
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e;
        \common\helpers\FlashMessageHelpsers::createWarningMessage($e->getMessage());
    } ?>
</div>

<?php

$scripts = [];

$scripts[] =
    <<<JS
// Abrir modal para agregar detalles al presionar tecla +
$(document).keypress(function (e) {
    if(document.activeElement.tagName !== "INPUT" && (e.keycode === 43 || e.which === 43)) {
        document.getElementById('boton_crear_plantilla').click();
    }
});
JS;

foreach ($scripts as $s) $this->registerJs($s);
?>

<?php
//$CSS = <<<CSS
//.table tbody tr:hover {
//    background-color: #ebffdd;
//}
//CSS;
//$this->registerCss($CSS);
//?>
