<?php

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
?>
<?php Pjax::begin(['id' => 'detallesplantilla_grid']); ?>
<?php

$columns = [
    [
        'class' => SerialColumn::className(),
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Cuenta',
        'value' => 'pCGravada.nombreConCodigo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Cuenta del IVA (si existe)',
        'value' => 'ivaCta.cuentaVenta.nombreConCodigo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Tipo de asiento',
        'value' => 'tipo_asiento',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Debe / Haber',
        'value' => 'tipo_saldo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
];

$template = '{update} &nbsp {delete}';
$buttons = [
    'delete' => function ($url, $model, $index) {
        return Html::a(
            '<span class="glyphicon glyphicon-trash"></span>',
            false,
            [
                'class' => 'ajax_boton_borrar_detalle',
                'delete-url' => $url,
                'id' => $index,
                'title' => Yii::t('app', 'Borrar')
            ]
        );
    },
    'update' => function ($url, $model) {
        return Html::a(
            '<span class="glyphicon glyphicon-pencil"></span>',
            false,
            [
                'title' => 'Editar detalle',
                'class' => 'tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => $url,
                'data-pjax' => '0',
                'data-title' => 'Editar detale'
            ]
        );
    }
];

array_push($columns, [
    'class' => ActionColumn::class,
    'template' => $template,
    'buttons' => $buttons,
    'urlCreator' => function ($action, $model, $key, $index) {
        if ($action === 'delete') {
            $url = Url::to(['plantilla-compraventa-detalle/borrar-detalle', 'index' => $key, 'tipo_asiento' => \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta' ? 'venta' : 'compra']);
            return $url;
        } elseif ($action === 'update') {
            $url = Url::to(['plantilla-compraventa-detalle/modificar-detalle', 'index' => $key, 'tipo_asiento' => \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta' ? 'venta' : 'compra']);
            return $url;
        }
        return '';
    },
    'contentOptions' => ['style' => 'font-size: 90%;'],
    'headerOptions' => ['style' => 'font-size: 90%;'],
]);

/** @var \backend\modules\contabilidad\models\PlantillaCompraventaDetalle[] $allModels */
$dataProvider = Yii::$app->getSession()->get('cont_detalleplantilla-provider');

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'hover' => true,
        'id' => 'grid',
        'panel' => ['type' => 'primary', 'heading' => \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta' ? 'Detalles para asiento de venta' : 'Detalles para asiento de compra', 'footer' => false,],
        'toolbar' => [
            'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'id' => 'boton_add_detalle',
                'type' => 'button',
                'title' => 'Agregar Detalle.',
                'class' => 'btn btn-success tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['plantilla-compraventa-detalle/add-detalle', 'tipo_asiento' => \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta' ? 'venta' : 'compra']),
                'data-pjax' => '0',
                'data-title' => 'Agregar detalle'
            ]),
        ]
    ]);
} catch (Exception $e) {
    echo $e;
}

$sesion = Yii::$app->session;

$scripts = [];
$scripts[] = <<<JS
// Código estático recurrente para abrir modales.
$(document).on('click', '.tiene_modal', (function () {
    var boton = $(this);
    var title = boton.data('title');
    $.get(
        boton.data('url'),
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));
JS;

foreach ($scripts as $s) $this->registerJs($s); ?>
<?php Pjax::end() ?>