<?php

use backend\modules\contabilidad\models\Obligacion;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\Rubro;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventa */
/* @var $form \kartik\form\ActiveForm */
?>

<div class="plantilla-compraventa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    try {
        $attributes = [       // 1 column layout
            'nombre' => [
                'type' => Form::INPUT_TEXT,
            ],
            'estado' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['activo' => "Activo", 'inactivo' => "Inactivo"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione un estado',
            ],
            'activo_fijo' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['si' => "Sí", 'no' => "No"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione uno',
            ],
            'deducible' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['si' => "Si", 'no' => "No"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione una opción',
            ],
            'activo_biologico' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['si' => "Si", 'no' => "No"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione una opción',
            ],
            'para_descuento' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['si' => "Si", 'no' => "No"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione una opción',
            ],
        ];
        if (\Yii::$app->getRequest()->getQueryParam('tipo') == 'venta') {
            $attributes['costo_mercad'] = [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['si' => "Si", 'no' => "No"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione una opción',
            ];
        } else {
            $attributes['para_seguro_a_devengar'] = [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['si' => "Si", 'no' => "No"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione una opción',
            ];
            $attributes['para_prestamo'] = [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['si' => "Si", 'no' => "No"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione una opción',
            ];
            $attributes['para_cuota_prestamo'] = [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => ['si' => "Si", 'no' => "No"],
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione una opción',
            ];
            $attributes['para_importacion'] = [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                    'data' => PlantillaCompraventa::getParaImportacionValues(),
                    'options' => [
                        'placeholder' => 'Por favor Seleccione Uno',
                        'disabled' => false
                    ]
                ],
                'hint' => 'Seleccione una opción',
            ];
        }

        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => $attributes
        ]);
    } catch (Exception $e) {
        echo $e;
    }

    echo $this->render('_form_detalle', []);

    if (\Yii::$app->getRequest()->getQueryParam('tipo') == 'venta') {
        echo $this->render('_form_detalle_costoventa', []);
    }


    $plantilla_rubro = new ActiveDataProvider([
        'query' => Rubro::find()->where(['estado' => 'activo']),
    ]);
    $plantilla_oblig = new ActiveDataProvider([
        'query' => Obligacion::find()->where(['estado' => 'activo']),
    ]);
    echo $this->render('@backend/modules/contabilidad/views/plantilla-compraventa-rubro/_form_plantilla_rubro', [
        'dataProvider' => $plantilla_rubro,
    ]);
    echo $this->render('@backend/modules/contabilidad/views/plantilla-compraventa-obligacion/_form_plantilla_obligacion', [
        'dataProvider' => $plantilla_oblig,
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$scripts = [];

$scripts[] = <<<JS
// Desplegar select2 automáticamente al obtener focus.
$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        $(this).siblings('select').select2('open');
    }
});
JS;

$url_reset_costoventadetalle_provider = Url::to(['reset-costoventadetalle-provider']);
$url_insertCtaPrestamoBancoX = Url::to(['insertar-cta-prestamo-banco-x']);
$url_removerCtaPrestamoBancoX = Url::to(['remover-cta-prestamo-banco-x']);
$color = \backend\helpers\HtmlHelpers::InfoColorHex(true);
$scripts =
    <<<JS
// Evento al cambiar el select2 'Tiene costo de mercadería?' 
var campo = $('#plantillacompraventa-costo_mercad');
var previous;
campo.on('select2:selecting', function (evt) {
    previous = $(this).val();
});
campo.on('select2:select', function (evt) {
    var now_selected = $(this).val();
    if (now_selected === 'no') {
        krajeeDialog.confirm('Esta operación BORRARÁ TODOS LOS DETALLES DE COSTO DE VENTA de la base de datos al guardar. Si está de acuerdo, presione \'De acuerdo\', de lo contrario \'Cancelar\'.',
            function (result) {
                if (result) {
                    $.ajax({
                        url: "$url_reset_costoventadetalle_provider",
                        type: 'post',
                        error: function (xhr, status, error) {
                            alert('There was an error with your request.' + xhr.responseText);
                        }
                    }).done(function (data) {
                        // DESCOMENTAR SI SE QUIERE OCULTAR SIN ELIMINAR DE LA GRILLA
                        // $('#detallesplantilla_costoventa_grid').hide();
                    });
                } else {
                    campo.val(previous).trigger('change');
                }
                $.pjax.reload({container: "#detallesplantilla_costoventa_grid", async: false});
            }
        );
    } else
        $.pjax.reload({container: "#detallesplantilla_costoventa_grid", async: false});

});

// Operaciones al borrar un detalle.
// En lugar de poner el código en del _form_detalle y _form_detalle_costoventa, pongo aquí para que no se buguee y abra multiples krajeedialogs.
$(document).on('click', '.ajax_boton_borrar_detalle', function (e) {
    e.preventDefault();
    var deleteUrl = $(this).attr('delete-url');
    krajeeDialog.confirm('DESEA CONTINUAR?',
        function (result) {
            if (result) {
                $.ajax({
                    url: deleteUrl,
                    type: 'post',
                    error: function (xhr, status, error) {
                        alert('There was an error with your request.' + xhr.responseText);
                    }
                }).done(function (data) {
                    $.pjax.reload({container: "#detallesplantilla_grid", async: false});
                    $.pjax.reload({container: "#flash_message_id", async: false});
                });
            }
        }
    );
});

$(document).on('change', '#plantillacompraventa-para_prestamo', function() {
//    let url = $(this).val() === 'si' ? "$url_insertCtaPrestamoBancoX" : "$url_removerCtaPrestamoBancoX";
//    $.ajax({
//        url: url,
//        type: 'get',
//        data: {},
//        success: function(data) {
//            console.log("aqui");
//            $.pjax.reload({container: "#detallesplantilla_grid", async: false});
//            $.pjax.reload({container: "#flash_message_id", async: false});  
//        },
//    });
});

// Operaciones al borrar un detalle de costo de venta.
$(document).on('click', '.ajax_boton_borrar_detalle_costoventa', function (e) {
    e.preventDefault();
    var deleteUrl = $(this).attr('delete-url');
    krajeeDialog.confirm('Esta acción ELIMINARA el detalle de costo de venta de la base de datos. DESEA CONTINUAR?',
        function (result) {
            if (result) {
                $.ajax({
                    url: deleteUrl,
                    type: 'post',
                    error: function (xhr, status, error) {
                        alert('There was an error with your request.' + xhr.responseText);
                    }
                }).done(function (data) {
                    $.pjax.reload({container: "#detallesplantilla_costoventa_grid", async: false});
                    $.pjax.reload({container: "#flash_message_id", async: false});
                });
            }
        }
    );
});

$(document).ready(function() {
    var css = {'background-color': '#$color', 'color': 'white', 'text-align': 'center', 'font-weight': 'bold'};
    applyPopOver($('label[for="plantillacompraventa-para_importacion"]'), 'Importación', 'Si es para importación debe seleccionar a que tipo de despacho corresponde. En caso contrario solo dejar en "no"', {'title-css': css});
});
JS;

$this->registerJs($scripts);
?>
