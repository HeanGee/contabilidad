<?php

use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventa */

$this->title = 'Datos de la plantilla ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => $model->tipo == 'venta' ? 'Plantillas de Venta' : 'Plantillas de Compra', 'url' => ['index', 'tipo' => $model->tipo]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantilla-compraventa-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plantilla-compraventa-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plantilla-compraventa-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plantilla-compraventa-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plantilla-compraventa-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-plantilla-compraventa-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Plantillas', ['index', 'tipo' => $model->tipo], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id, 'tipo' => $model->tipo], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Desea borrar la plantilla?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php
    try {
        $attributes = [
            [
                'attribute' => 'nombre',
                'value' => $model->nombre
            ],
            [
                'attribute' => 'tipo',
                'value' => ucfirst($model->tipo)
            ],
            [
                'attribute' => 'estado',
                'format' => 'raw',
                'value' => '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>',
//                'valueColOptions' => ['style' => 'width:30%'],
            ],
            [
                'attribute' => 'deducible',
                'format' => 'raw',
                'value' => '<label class="label label-' . (($model->deducible == 'si') ? 'success">Sí' : 'danger">No') . '</label>',
//                'valueColOptions' => ['style' => 'width:30%'],
            ],
        ];
        if ($model->tipo == 'venta') {
            array_push($attributes,
                [
                    'attribute' => 'costo_mercad',
                    'format' => 'raw',
                    'value' => '<label class="label label-' . (($model->costo_mercad == 'si') ? 'success">Sí' : 'danger">No') . '</label>',
                ]);
        }
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Plantilla',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => $attributes
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => PlantillaCompraventaDetalle::find()->where(['plantilla_id' => $model->id, 'tipo_asiento' => Yii::$app->getRequest()->getQueryParam('tipo')]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => $model->tipo == 'venta' ? 'Detalles para asiento de venta' : 'Detalles para asiento de compra',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'columns' => [
                [
                    'label' => 'Cuenta',
                    'value' => 'pCGravada.nombreConCodigo'
                ],
                [
                    'label' => 'Cuenta del IVA (si existe)',
                    'value' => 'ivaCta.cuentaVenta.nombreConCodigo'
                ],
//                [
//                    'label' => 'Tipo de asiento',
//                    'value' => 'tipoAsiento'
//                ],
                [
                    'label' => 'Debe / Haber',
                    'value' => 'tipo_saldo'
                ],
                [
                    'label' => 'Cuenta principal ?',
                    'value' => function ($model) {
                        return ucfirst($model->cta_principal);
                    },
                ],
            ],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => PlantillaCompraventaDetalle::find()->where(['plantilla_id' => $model->id, 'tipo_asiento' => 'costo_venta']),
            'pagination' => false
        ]);
        echo $model->tipo == 'venta' ? GridView::widget([
            'id' => 'grid-detalles-costoventa',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => 'Detalles para asiento de costo de venta',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'columns' => [
                [
                    'label' => 'Cuenta',
                    'value' => 'pCGravada.nombreConCodigo'
                ],
//                [
//                    'label' => 'Tipo de asiento',
//                    'value' => 'tipoAsiento'
//                ],
                [
                    'label' => 'Debe / Haber',
                    'value' => 'tipo_saldo'
                ],
            ],
        ]) : '';

    } catch (\Exception $e) {
        print $e;
        /* TODO: manejar */
    }
    ?>

</div>
