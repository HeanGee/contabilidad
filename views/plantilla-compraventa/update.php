<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventa */

$this->title = \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta' ? 'Modificar Plantilla Venta' : 'Modificar Plantilla Compra';
$this->params['breadcrumbs'][] = ['label' => 'Plantilla Compraventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plantilla-compraventa-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
