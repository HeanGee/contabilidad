<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\PlantillaCompraventa */

$this->title = \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta' ? 'Crear Plantilla Venta' : 'Crear Plantilla Compra';
$this->params['breadcrumbs'][] = ['label' => \Yii::$app->getRequest()->getQueryParam('tipo') == 'venta' ? 'Plantillas Venta' : 'Plantillas Compra', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plantilla-compraventa-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
