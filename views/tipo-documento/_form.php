<?php

use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\TipoDocumentoSet;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\contabilidad\models\TipoDocumento $model
 * @var yii\widgets\ActiveForm $form
 */
?>

    <div class="tipo-documento-form">

        <?php
        $form = ActiveForm::begin(['id' => 'tipo-documento-form']);

        try {
            $first_row = [
                'nombre' => [
                    'type' => Form::INPUT_TEXT,
                    'columnOptions' => ['colspan' => '4'],
                    'options' => ['placeholder ' => 'Nombre...']
                ],
                'tipo_documento_set_id' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => $form->field($model, 'tipo_documento_set_id')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Seleccione...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'data' => TipoDocumentoSet::getLista(true)
                        ],
                        'initValueText' => !empty($model->tipo_documento_set_id) ? ($model->tipoDocumentoSet->nombre) : ''
                    ])
                ],
                'asociacion' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => $form->field($model, 'asociacion')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Seleccione...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'data' => TipoDocumento::getValoresEnum('asociacion', true)
                        ],
                        'initValueText' => !empty($model->asociacion) ? ucfirst($model->asociacion) : ''
                    ])
                ],
//                'asiento_independiente' => [
//                    'type' => Form::INPUT_RAW,
//                    'columnOptions' => ['colspan' => '2'],
//                    'value' => $form->field($model, 'asiento_independiente')->widget(Select2::className(), [
//                        'options' => ['placeholder' => 'Seleccione...'],
//                        'data' => ['si' => "Si", 'no' => "No"],
//                        'pluginOptions' => [
//                        ],
//                    ]),
//                ],
                'autofactura' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => $form->field($model, 'autofactura')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Seleccione...'],
                        'data' => ['si' => "Si", 'no' => "No"],
                        'pluginOptions' => [
                            'allowClear' => false,
                        ],
                    ]),
                ],
            ];
            if (!$model->isNewRecord) {
                $first_row['estado'] = [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '2'],
                    'value' => $form->field($model, 'estado')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Seleccione...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'data' => TipoDocumento::getValoresEnum('estado', true)
                        ],
                        'initValueText' => !empty($model->estado) ? ucfirst($model->estado) : ''
                    ])
                ];
            }
            echo FormGrid::widget([
                'model' => $model,
                'form' => $form,
                'autoGenerateColumns' => true,
                'rows' => [
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => $first_row
                    ],
                    [
                        'autoGenerateColumns' => false,
                        'columns' => 12,
                        'attributes' => [
                            'asiento_independiente' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '2'],
                                'value' => $form->field($model, 'asiento_independiente')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione...'],
                                    'data' => ['si' => "Si", 'no' => "No"],
                                    'pluginOptions' => [
                                    ],
                                ]),
                            ],
                            'concepto_asiento_independiente' => [
                                'type' => Form::INPUT_TEXT,
                                'columnOptions' => ['colspan' => '10'],
                                'options' => ['placeholder ' => 'Concepto para asiento independiente...']
                            ],
                        ]
                    ],
                    [
                        'attributes' => [
                            'detalle' => [
                                'type' => Form::INPUT_TEXT,
                                'options' => ['placeholder ' => 'Detalle...']
                            ],
                            'cuenta_id' => [
                                'type' => Form::INPUT_RAW,
                                'value' => $form->field($model, 'cuenta_id')->widget(Select2::className(), [
                                    'options' => ['placeholder' => 'Seleccione...'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'data' => PlanCuenta::getCuentaLista(true)
                                    ],
                                    'initValueText' => !empty($model->cuenta_id) ? ($model->cuenta->cod_completo . ' - ' . $model->cuenta->nombre) : ''
                                ])
                            ],
                            'condicion' => [
                                'type' => Form::INPUT_RAW,
                                'columnOptions' => ['colspan' => '1'],
                                'value' => $form->field($model, 'condicion')->widget(Select2::className(), [
                                    'data' => TipoDocumento::getCondiciones(),
                                    'options' => ['placeholder' => 'Seleccione una condición ...'],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                    ],
                                    'pluginEvents' => [
                                        'change' => "function(){}"
                                    ]
                                ])
                            ],
                            'para_importacion' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                    'data' => ['si' => "Si", 'no' => "No"],
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                        'disabled' => false
                                    ]
                                ],
                                'hint' => 'Seleccione una opción',
                            ],
                            'solo_exento' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'pluginOptions' => [
                                        'allowClear' => false
                                    ],
                                    'data' => ['si' => "Si", 'no' => "No"],
                                    'options' => [
                                        'placeholder' => 'Por favor Seleccione Uno',
                                        'disabled' => false,
                                        'allowClear' => false,
                                    ]
                                ],
                                'hint' => 'Seleccione una opción',
                            ],
                            'para' => [
                                'type' => Form::INPUT_WIDGET,
                                'widgetClass' => Select2::className(),
                                'options' => [
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                    'data' => TipoDocumento::getValoresEnum('para', false),
                                    'options' => [
                                        'placeholder' => 'Seleccione uno...',
                                        'disabled' => false,
                                    ]
                                ],
                                'hint' => 'Opcional',
                            ],
                        ]
                    ],
                ]
            ]);

        } catch (\Exception $e) {
            echo $e;
        }

        ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
ob_start(); // output buffer the javascript to register later ?>
    <script>
        $("#tipodocumento-asiento_independiente").on('change', function () {
            $('form[id="tipo-documento-form"]').yiiActiveForm('validateAttribute', 'tipodocumento-concepto_asiento_independiente');
        });
    </script>
<?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>