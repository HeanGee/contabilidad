<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\contabilidad\models\TipoDocumento $model
 */

$this->title = 'Modificar Tipo de Documento';
$this->params['breadcrumbs'][] = ['label' => 'Tipo Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="tipo-documento-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
