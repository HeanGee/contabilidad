<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\contabilidad\models\TipoDocumento $model
 */

$this->title = 'Detalle Tipo de Documento';
$this->params['breadcrumbs'][] = ['label' => 'Tipo Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-documento-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a T. de D.', ['index'/*, 'operacion' => Yii::$app->getRequest()->getQueryParam('operacion')*/], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro que quiere eliminar este Tipo de Documento?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Prestamo',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'columns' => [
                        [
                            'attribute' => 'nombre',
                            'value' => $model->nombre,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'condicion',
                            'value' => ucfirst($model->condicion),
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'tipo_documento_set_id',
                            'value' => $model->tipoDocumentoSet->nombre,
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'asociacion',
                            'value' => ucfirst($model->asociacion),
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'estado',
                            'format' => 'raw',
                            'value' => '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>',
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'cuenta_id',
                            'value' => "{$model->cuenta->cod_completo} - {$model->cuenta->nombre}",
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'attribute' => 'detalle',
                    'value' => $model->detalle,
                    'options' => ['rows' => 4],
                ],
            ],
        ]);

    } catch (\Exception $e) {
        \common\helpers\FlashMessageHelpsers::createWarningMessage($e->getMessage());
    }


    ?>

</div>
