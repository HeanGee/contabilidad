<?php

use backend\modules\contabilidad\models\PlanCuenta;
use backend\modules\contabilidad\models\TipoDocumento;
use common\helpers\PermisosHelpers;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var backend\modules\contabilidad\models\search\TipoDocumentoSearch $searchModel
 */

$this->title = 'Tipos de Documentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-documento-index">

    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-tipo-documento-create') ?
            Html::a('Crear Tipo de Documento', ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?php

    try {
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (PermisosHelpers::getAcceso("contabilidad-tipo-documento-{$_v}"))
                $template .= "&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'hover' => true,
            'striped' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id',
                    'value' => function ($model, $key, $index, $column) {
                        return ucfirst($model->id);
                    },
                    'label' => 'Código (ID)',
                ],
                'nombre',
                'detalle',
                [
                    'attribute' => 'cuenta_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var TipoDocumento $model */
                        return $model->cuenta->getNombreConCodigo();
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'model' => $searchModel,
                        'attribute' => 'cuenta_id',
                        'data' => PlanCuenta::getCuentaLista(false, 'index'),
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                        ],
                    ],
                ],
                [
                    'attribute' => 'tipo_documento_set_nombre',
                    'label' => 'Tipo Documento SET',
                    'value' =>
                        function ($model) {
                            /** @var TipoDocumento $model */
                            return $model->tipoDocumentoSet != null ? ucfirst($model->tipoDocumentoSet->nombre) : '';
                        },
                ],
                [
                    'attribute' => 'asociacion',
                    'value' => function ($model, $key, $index, $column) {
                        return ucfirst($model->asociacion);
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'asociacion',
                        'data' => ['' => 'Todos'] + array_map('ucfirst', TipoDocumento::getValoresEnum('asociacion')),
                        'hideSearch' => true
                    ])
                ],
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => ['' => 'Todos'] + array_map('ucfirst', TipoDocumento::getValoresEnum('estado')),
                        'hideSearch' => true
                    ])
                ],
                [
                    'attribute' => 'condicion',
                    'value' => function ($model, $key, $index, $column) {
                        return ucfirst($model->condicion);
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'condicion',
                        'data' => TipoDocumento::getCondiciones(true),
                        'hideSearch' => true
                    ])
                ],
                [
                    'attribute' => 'asiento_independiente',
                    'value' => function ($model, $key, $index, $column) {
                        return ucfirst($model->asiento_independiente);
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'asiento_independiente',
                        'data' => ['si' => "Si", 'no' => "No"],
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                        ],
                    ])
                ],
                [
                    'attribute' => 'para',
                    'value' => function ($model, $key, $index, $column) {
                        return strtoupper(str_replace('_', ' ', trim($model->para)));
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'para',
                        'data' => array_merge(TipoDocumento::getValoresEnum('para'), ['not_null' => "No vacío"]),
                        'pluginOptions' => [
                            'placeholder' => "Todos",
                            'allowClear' => true,
                        ],
                    ])
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [],
                    'template' => $template,
                ],
            ],
        ]);

    } catch (\Exception $e) {
        print $e;
        /* TODO: manejar excepción */
    }

    $css = <<<CSS
.kartik-sheet-style {
	background: #ffffff;
	background: -moz-linear-gradient(top, #ffffff 0%, #f5f5f5 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f5f5f5));
	background: -webkit-linear-gradient(top, #ffffff 0%,#f5f5f5 100%);
	background: -o-linear-gradient(top, #ffffff 0%,#f5f5f5 100%);
	background: -ms-linear-gradient(top, #ffffff 0%,#f5f5f5 100%);
	background: linear-gradient(to bottom, #ffffff 0%,#f5f5f5 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f5f5f5',GradientType=0 );
}
CSS;
    $this->registerCss($css);

    ?>

</div>