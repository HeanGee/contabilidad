<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var backend\modules\contabilidad\models\TipoDocumento $model
 */

$this->title = 'Crear Tipo de Documento';
$this->params['breadcrumbs'][] = ['label' => 'Tipo Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-documento-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
