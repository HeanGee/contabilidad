<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 12/1/17
 * Time: 4:00 PM
 */

use bedezign\yii2\audit\models\AuditTrail;
use bedezign\yii2\audit\models\AuditTrailSearch;
use common\models\User;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\daterange\DateRangePicker;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $modelo \yii\db\ActiveRecord */

?>

<?php

try {
    $searchModel = new AuditTrailSearch();
    $searchModel->load(Yii::$app->request->get());
    /* esta busqueda la tengo que hacer así, porque el usuario realmente no ve los datos que están guardados en la tabla de auditoría */
    $date_range = Yii::$app->request->get('date_range_1');
    preg_match('/^((\d\d)-(\d\d)-(\d\d\d\d))\s?-\s?((\d\d)-(\d\d)-(\d\d\d\d))$/', $date_range, $matches);
    $search_usuario = !empty($searchModel->user_id) ? \yii\helpers\ArrayHelper::map(User::find()->where(['LIKE', 'username', $searchModel->user_id])->asArray()->all(), 'username', 'id') : '';
    $dataProvider = new ActiveDataProvider([
        'query' => AuditTrail::find()->where("model = '" . addslashes($modelo::className()) . "'")
            ->andWhere(['IN', "model_id", $ids])
            ->andFilterWhere(['action' => $searchModel->action])
            ->andFilterWhere(['LIKE', 'field', $searchModel->field])->indexBy('id')
            ->andFilterWhere(['user_id' => $search_usuario])->indexBy('id')
            ->andFilterWhere(['>=', 'created', !empty($matches[1]) ? "{$matches[4]}-{$matches[3]}-{$matches[2]}" : ''])
            ->andFilterWhere(['<=', 'created', !empty($matches[5]) ? "{$matches[8]}-{$matches[7]}-{$matches[6]}" : ''])->indexBy('id'),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    Pjax::begin(['id' => 'detalles-audit-grid']);

    ActiveFormDisableSubmitButtonsAsset::register($this);

    $_modelo_class = get_class($modelo);

    echo GridView::widget([
        'id' => 'grid-auditoria',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => [
            'type' => 'info',
            'heading' => "<label class='label label-info'>HISTORIAL $_modelo_class</label>",
        ],
        'columns' => [
            'id',
            'entry_id',
            [
                'attribute' => 'user_id',
                'label' => Yii::t('audit', 'Usuario'),
                'class' => 'yii\grid\DataColumn',
                'options' => ['width' => '150px'],
                'value' => function ($data) {
                    $usuario = User::findOne($data->user_id);
                    return $usuario->username;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'action',
                'filter' => ['CREATE' => 'CREADO', 'UPDATE' => 'MODIFICADO'],
                'value' => function ($model) {
                    return $model->action == 'UPDATE' ? 'MODIFICADO' : 'CREADO';
                }
            ],
            [
                'attribute' => 'model',
                'value' => 'model'
            ],
            [
                'attribute' => 'model_id',
                'value' => 'model_id'
            ],
            [
                'attribute' => 'field',
                'format' => 'raw',
                'value' => function ($model) {
                    $array = ($model->model)::attributeLabels();
                    $key = $model->field;
                    $msg = '';

                    if (array_key_exists($key, $array))
                        return $array[$key];
                    else {
                        if ($key == '' || !isset($key) || empty($key))
                            $msg = "<label class='label label-warning'>null</label>";
                        else
                            $msg = "<label class='label label-warning'>El campo <strong>{$key}</strong> no existe.</label>";
                    }

                    return $msg;
                }
            ],
            [
                'label' => Yii::t('audit', 'Diff'),
                'value' => function ($model) {
                    $old = explode("\n", $model->old_value);
                    $new = explode("\n", $model->new_value);

                    foreach ($old as $i => $line) {
                        $old[$i] = rtrim($line, "\r\n");
                    }
                    foreach ($new as $i => $line) {
                        $new[$i] = rtrim($line, "\r\n");
                    }

//                    $no_diferencias = ($model->model)::manejarTextoAuditoria($model->field, $old, $new);

                    $diff = new \Diff($old, $new);
                    $return = $diff->render(new \Diff_Renderer_Html_Inline);
                    return $return; //$no_diferencias ? $return : str_replace(['<del>', '</del>', '<ins>', '</ins>'], '', $return);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'created',
                'label' => 'Fecha',
                'options' => ['width' => '150px'],
                'format' => 'html',
                'filter' => DateRangePicker::widget([
                    'name' => 'date_range_1',
                    'useWithAddon' => false,
                    'language' => 'es',
                    'hideInput' => true,
                    //'presetDropdown' => true,
                    'pluginOptions' => [
                        'locale' => ['format' => 'DD-MM-YYYY'],
                        'separator' => '-',
                        'opens' => 'left'
                    ]
                ])
            ],
        ],
    ]);

    $js = <<<JS
    $(document).ready(function() {
        $('input[name="date_range_1"]').parent().find('span.range-value').next().removeClass('pull-right');
    });
JS;
    $this->registerJs($js);

    $css = file_get_contents(Yii::$app->basePath . '/../vendor/bedezign/yii2-audit/src/web/assets/css/audit.css');
    $css .= "
    table.DifferencesInline > thead,
    table.DifferencesInline tbody.ChangeReplace > tr > th { display: none; }
";
    $this->registerCss($css);
    Pjax::end();

} catch (Exception $exception) {
    print $exception;
}
?>
