<?php

use backend\modules\contabilidad\models\AsientoDetalle;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Asiento */

$this->title = 'Detalle';
$this->params['breadcrumbs'][] = ['label' => 'Asientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asiento-view">
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Asiento',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'attribute' => 'empresa_id',
                    'value' => ($model->empresa_id) ? $model->empresa->nombre : ''
                ],
                [
                    'attribute' => 'periodo_contable_id',
                    'value' => ($model->periodo_contable_id) ? $model->periodoContable->anho : ""
                ],
                [
                    'attribute' => 'fecha',
                    'value' => date_create_from_format('Y-m-d', $model->fecha)->format('d-m-Y')
                ],
                [
                    'attribute' => 'monto_debe',
                    'value' => $model->montoDebeFormatted,
                    'label' => 'Monto'
                ],
                [
                    'attribute' => 'descripcion',
                    'value' => $model->concepto
                ]
            ]
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => AsientoDetalle::find()->where(['asiento_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'toolbar' => [],
            'hover' => true,
            'panel' => [
                'type' => 'info',
                'heading' => 'Detalles',
                'footerOptions' => ['class' => ''],
                'beforeOptions' => ['class' => ''],
                'afterOptions' => ['class' => '']
            ],
            'panelFooterTemplate' => '',
            'columns' => [
                [
                    'attribute' => 'asiento_id',
                    'value' => 'cuenta.cod_completo'
                ],
                [
                    'attribute' => 'cuenta_id',
                    'value' => 'cuenta.nombre'
                ],
                [
                    'attribute' => 'monto_debe',
                    'value' => 'montoDebeFormatted',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ],
                [
                    'attribute' => 'monto_haber',
                    'value' => 'montoHaberFormatted',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                ]
            ],
        ]);

    } catch (\Exception $e) {
        print $e;
        /* TODO: manejar */
    }
    ?>

    <?php
    echo $this->render('@backend/modules/contabilidad/views/facturas-relacionadas/_facturas_relacionadas', ['model' => $model]);

    echo $this->render('../auditoria/_auditoria', ['modelo' => $model]);
    //    $columns = [
    //        [
    //            'class' => SerialColumn::className(),
    //        ],
    //
    //        [
    //            'label' => 'Cliente/Proveedor',
    //            'value' => function ($model, $key, $index, $column) {
    //                if ($model instanceof Venta || $model instanceof Compra)
    //                    return $model->entidad->razon_social;
    //                return "";
    //            },
    //        ],
    //        [
    //            'label' => 'Nro Factura',
    //            'value' => function ($model, $key, $index, $column) {
    //                if ($model instanceof Venta) {
    //                    return $model->getNroFacturaCompleto();
    //                } elseif ($model instanceof Compra)
    //                    return $model->nro_factura;
    //                return null;
    //            },
    //        ],
    //    ];
    //
    //    $template = '{view}';
    //    $buttons = [];
    //
    //    array_push($columns, [
    //        'class' => ActionColumn::class,
    //        'template' => $template,
    //        'buttons' => $buttons,
    //        'urlCreator' => function ($action, $model, $key, $index) {
    //            if ($action === 'view') {
    //                $factura = $model instanceof Venta ? 'venta' : 'compra';
    //                $url = Url::to([$factura . '/view', 'id' => $model->id]);
    //                return $url;
    //            }
    //            return '';
    //        },
    //    ]);
    //
    //    try {
    //        $allModels = $model->getVentas()->all();
    //        if (sizeof($allModels) == 0)
    //            $allModels = $model->getCompras()->all();
    //        $dataProvider = new ArrayDataProvider([
    //            'allModels' => $allModels,
    //            'pagination' => false,
    //        ]);
    //        echo GridView::widget([
    //            'dataProvider' => $dataProvider,
    //            'columns' => $columns,
    //            'hover' => true,
    //            'id' => 'grid',
    //            'panel' => ['type' => 'info', 'heading' => 'Facturas relacionadas', 'footer' => false,],
    //            'toolbar' => [
    ////            'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
    ////                'id' => 'boton_add_detalle',
    ////                'type' => 'button',
    ////                'title' => 'Agregar Cuenta. Utilice el signo \'+\' para acceder.',
    ////                'class' => 'btn btn-success tiene_modal',
    ////                'data-toggle' => 'modal',
    ////                'data-target' => '#modal',
    ////                'data-url' => Url::to(['detalle-venta/add-cuenta']),
    ////                'data-pjax' => '0',
    ////                'data-title' => 'Agregar cuenta'
    ////            ]),
    //            ]
    //        ]);
    //    } catch (Exception $e) {
    //        throw  $e;
    //    }
    ?>

</div>
