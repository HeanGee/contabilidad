<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Asiento */
/* @var $detalles backend\modules\contabilidad\models\AsientoDetalle[] */
/* @var $totaldebe string */
/* @var $totalhaber string */

$this->title = 'Crear Asiento de Sueldos e IPS';
$this->params['breadcrumbs'][] = ['label' => 'Asientos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asiento-create">

    <?= $this->render('_form', [
        'model' => $model,
        'totaldebe' => $totaldebe,
        'totalhaber' => $totalhaber,
    ]) ?>

</div>
