<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 02/07/2018
 * Time: 17:06
 */

use kartik\form\ActiveForm;
use yii\base\Model;

$model = new TemplateModel();
$form = ActiveForm::begin([
    'id' => 'login-form-inline',
    'type' => ActiveForm::TYPE_INLINE
]);
?>

<?= $form->field($model, 'totalDebe') ?>
<?= $form->field($model, 'totalHaber') ?>

<?php ActiveForm::end(); ?>

<?php

/**
 * @property string $totalDebe
 * @property string $totalHaber
 */
class TemplateModel extends Model
{
    public $totalDebe;
    public $totalHaber;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['totalDebe', 'totalHaber'], 'safe'],
            [['totalDebe', 'totalHaber'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'totalDebe' => 'Debe',
            'totalHaber' => 'Haber',
        ];
    }
}
