<?php

use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use kartik\number\NumberControl;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $totaldebe string */
/* @var $totalhaber string */
?>
<?php Pjax::begin(['id' => 'grid_asientos_detalle']); ?>
<?php

$columns = [
    [
        'class' => SerialColumn::className(),
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],

    [
        'label' => 'Código cuenta',
        'value' => 'cuenta.cod_completo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Cuenta',
        'value' => 'cuenta.nombre',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
//    [
//        'label' => 'Cuenta',
//        'value' => 'cuentaNombreDebe',
//        'contentOptions' => ['style' => 'font-size: 90%;'],
//        'headerOptions' => ['style' => 'font-size: 90%;'],
//    ],
//    [
//        'label' => '',
//        'value' => 'cuentaNombreHaber',
//        'contentOptions' => ['style' => 'font-size: 90%;'],
//        'headerOptions' => ['style' => 'font-size: 90%;'],
//    ],
    [
        'label' => 'Debe',
        'value' => 'montoDebeFormatted',
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Haber',
        'value' => 'montoHaberFormatted',
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%; width: 20%'],
    ],
];

//$template = '{update} &nbsp {delete}';
$template = '';
$buttons = [];

//array_push($columns, [
//    'class' => ActionColumn::class,
//    'template' => $template,
//    'buttons' => $buttons,
//    'contentOptions' => ['style' => 'font-size: 90%;'],
//    'headerOptions' => ['style' => 'font-size: 90%;'],
//]);

try {
    $sessionKey = 'cont_asiento_from_rrhh-provider';
    $dataProvider = Yii::$app->getSession()->has($sessionKey) ? Yii::$app->getSession()->get($sessionKey) : new ArrayDataProvider([
        'allModels' => [],
        'pagination' => false,
    ]);

    if ($dataProvider->allModels != null) {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'hover' => true,
            'id' => 'grid',
            'panel' => ['type' => 'primary', 'heading' => 'Asientos', 'footer' => false,],
            'toolbar' => [
                'content' => ''
            ]
        ]);
        echo Html::beginTag('div', ['class' => 'parent']);
        echo Html::beginTag('div', ['class' => 'float-right child']);
        echo Html::label('Total Debe: ', 'total-debe-disp');
        echo NumberControl::widget([
            'name' => 'totalcdebe',
            'id' => 'total-debe',
            'readonly' => true,
            'value' => $totaldebe,
            'class' => 'form-control',
            'maskedInputOptions' => [
                'prefix' => '₲ ',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'digits' => 0
            ],
            'displayOptions' => [
                'class' => 'form-control kv-monospace float-right',
                'placeholder' => 'Total Debe...',
                'style' => [
//                'width' => '34.9%',
//                'margin' => '0 auto',
//                'text-align' => 'right',
//                'clear' => 'right',
//                'float' => 'right',
//                'position' => 'relative',
                    'font-weight' => 'bold',
                ]
            ]
        ]);
        echo Html::endTag('div');
        echo Html::beginTag('div', ['class' => 'float-right child']);
        echo Html::label('Total Haber: ', 'total-haber-disp');
        echo NumberControl::widget([
            'name' => 'total-haber',
            'id' => 'total-haber',
            'readonly' => true,
            'value' => $totalhaber,
            'class' => 'form-control',
            'maskedInputOptions' => [
                'prefix' => '₲ ',
                'groupSeparator' => '.',
                'radixPoint' => ',',
                'digits' => 0
            ],
            'displayOptions' => [
                'class' => 'form-control kv-monospace float-right',
                'placeholder' => 'Total Haber...',
                'style' => [
//                'width' => '34.9%',
//                'margin' => '0 auto',
//                'text-align' => 'right',
//                'clear' => 'right',
//                'float' => 'right',
//                'position' => 'relative',
                    'font-weight' => 'bold',
                ]
            ]
        ]);
        echo Html::endTag('div');
        echo Html::tag('br');
        echo Html::tag('br');
        echo Html::tag('br');
        echo Html::tag('br');
        echo Html::endTag('div');
    }

} catch (Exception $e) {
    throw  $e;
}
?>
<?php Pjax::end() ?>

<?php
$CSS = <<<CSS
.float-right {
  float: right;
}
.child {
  /*border: 1px solid indigo;*/
  padding-left: 1rem;
}
CSS;
$this->registerCss($CSS);

$JS = <<<JS

JS;
$this->registerJs($JS);
?>