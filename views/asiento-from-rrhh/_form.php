<?php

use backend\modules\contabilidad\controllers\AsientoDeSueldosForm;
use backend\modules\contabilidad\models\PlanCuenta;
use kartik\builder\Form;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model AsientoDeSueldosForm */
/* @var $detalles backend\modules\contabilidad\models\AsientoDetalle[] */
/* @var $form yii\widgets\ActiveForm */
/* @var $totaldebe string */
/* @var $totalhaber string */
?>

<?php $form = ActiveForm::begin([]);
try {
    // Additional input fields passed as params to the child dropdown's pluginOptions

    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'mes_anho' => [
                'label' => "Seleccione un mes",
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => DatePicker::class,
                'options' => [
                    'name' => 'mes-selector',
                    'class' => 'col-md-6',
                    'language' => 'es',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'startView' => 'year',
                        'minViewMode' => 'months',
                        'format' => 'mm-yyyy',
                        'language' => 'es',
                    ]
                ],
            ],
            'concepto_salida' => [
                'type' => Form::INPUT_TEXT,
            ],
//            'empresa_id' => [
//                'type' => Form::INPUT_DROPDOWN_LIST,
//                'items' => AsientoDeSueldosForm::getEmpresa(),
//                'options' => [
//                    'id' => 'empresa-id',
//                    'placeholder' => 'Por favor Seleccione Empresa',
//                ],
//                'pluginOptions' => ['allowClear' => false],
//            ],
            'empresa_id' => [
                'type' => Form::INPUT_RAW,
                'value' => $form->field($model, 'empresa_id')->widget(Select2::className(), [
                    'data' => AsientoDeSueldosForm::getEmpresa(),
                    'options' => [
                        'placeholder' => 'Seleccione un Empresa ...',
                        'id' => 'empresa-id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'pluginEvents' => [
                        'change' => "function(){
                            $('#sucursal-id').val('').trigger('change');
                        }"
                    ]
                ])
            ],
//            'sucursal_id' => [
//                'type' => Form::INPUT_WIDGET,
//                'widgetClass' => DepDrop::classname(),
//                'options' => [
//                    'id' => 'sucursal-id',
//                    'options' => [
//                        'data' => AsientoDeSueldosForm::getEmpresa($model->empresa_id),
//                        'placeholder' => 'Por favor Seleccione Sucursal',
//                    ],
//                    'pluginOptions' =>
//                    ['depends'=>['empresa-id'],
//                    'url'=> Url::to(['asiento/sucursal']),
//                    ],
//                ],
//            ],
            'sucursal_id' => [
                'type' => Form::INPUT_RAW,
                'value' => $form->field($model, 'sucursal_id')->widget(Select2::className(), [
                    'options' => [
                        'placeholder' => 'Seleccione una Sucursal ...',
                        'id' => 'sucursal-id',
                    ],
                    'initValueText' => isset($model->sucursal_id) ? $model->getSucursal()['nombre']: '',
                    'pluginOptions' => [
                        'allowClear' => true,
                        'ajax' => [
                            'url' => Url::to(['asiento/get-sucursal-lista']),
                            'dataType' => 'json',
                            'data' => new JsExpression("function(params) { return {q:params.term,empresa_id:$('#empresa-id').val()}; }")
                        ]
                    ]
                ])
            ],
            'cuenta_salida_id' => [
                'type' => Form::INPUT_RAW,
                'value' => $form->field($model, 'cuenta_salida_id')->widget(Select2::className(), [
                    'options' => ['placeholder' => 'Seleccione...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'data' => PlanCuenta::getCuentaLista()
                    ],
                    'initValueText' => isset($model->cuenta_salida_id) ? (ucfirst($model->cuentaSalida->cod_completo . ' - ' . $model->cuentaSalida->nombre)) : '',
                    'pluginEvents' => [
                        'change' => ""
                    ]
                ])
            ],
        ]
    ]);


} catch (Exception $e) {
    echo $e;
}

echo Html::a('Vista Previa', ['asiento/create-from-rrhh'], [
        'class' => 'btn btn-info',
        'data' => [
            'method' => 'post',
//        'confirm' => 'Are you sure?',
//        'params'=>['operacion'=>Yii::$app->getRequest()->getQueryParam('operacion')], // mas parametros al $_POST
        ]
    ]) . '&nbsp;&nbsp;';
echo Html::a('Generar y guardar', ['asiento/create-from-rrhh', 'tipo_operacion' => 'generar_guardar'], [
    'class' => 'btn btn-success',
    'data' => [
        'method' => 'post',
//        'confirm' => 'Are you sure?',
//        'params'=>['operacion'=>Yii::$app->getRequest()->getQueryParam('operacion'), 'tipo_operacion' => 'generar_guardar'],
    ]
]);

echo Html::tag('br/');
$view = '_form_asiento_detalles_grid';
$vars = [
    'totaldebe' => $totaldebe,
    'totalhaber' => $totalhaber,
];
echo $this->render($view, $vars);

ActiveForm::end() ?>

<?php
$JS = <<<JS
$(document).ready(function () {
    $($('#grid_asientos_detalle, #grid_asientos_detalle, #grid_asientos_detalle_venta')[0].getElementsByTagName('tr')).each(function () {
        if (typeof $(this)[0].getElementsByTagName('td')[0] !== 'undefined') {
            if ($(this)[0].getElementsByTagName('td').length > 3 && $(this)[0].getElementsByTagName('td')[3].textContent === '0') {
                $($(this)[0].getElementsByTagName('td')[2]).css('text-align', 'right');
            }
        }
    });
});
JS;

$this->registerJs($JS);
?>