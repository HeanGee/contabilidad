<?php

use backend\modules\contabilidad\models\PlanCuenta;
use kartik\helpers\Html;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\AsientoDetalle */
/* @var $index int */
/* @var $form yii\widgets\ActiveForm */
?>
<td>
    <?= $form->field($model, "[{$index}]cuenta_id")->widget(Select2::className(), [
        'options' => ['placeholder' => 'Seleccione...'],
        'pluginOptions' => [
            'allowClear' => true,
            'data' => PlanCuenta::getCuentaLista(true)
        ],
        'initValueText' => !empty($model->cuenta) ? $model->cuenta->nombre : '',
    ])->label(false) ?>
</td>
<td>
    <?= $form->field($model, "[{$index}]monto_debe")->textInput(['class' => 'agregarNumber'])->label(false) ?>
</td>
<td>
    <?= $form->field($model, "[{$index}]monto_haber")->textInput(['class' => 'agregarNumber'])->label(false) ?>
</td>
<td>
    <?= Html::a('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', 'javascript:void(0);', [
        'class' => 'eliminar-detalle-btn btn btn-danger btn-xs'
    ]) ?>
</td>
