<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\IvaCuenta */

$this->title = 'Modificar Iva Cuenta: ' . $model->iva->porcentaje.'%';
$this->params['breadcrumbs'][] = ['label' => 'Iva Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="iva-cuenta-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
