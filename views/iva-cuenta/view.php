<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\IvaCuenta */

$this->title = 'IVA ' . $model->iva->porcentaje . '%';
$this->params['breadcrumbs'][] = ['label' => 'Iva Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iva-cuenta-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-iva-cuenta-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-iva-cuenta-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-iva-cuenta-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-iva-cuenta-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-iva-cuenta-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a I.V.A - Ctas', ['index'], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Estás seguro de eliminar?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => "Datos",
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'attribute' => 'iva_id',
                    'value' => $model->iva->porcentaje,
                    'label' => "Porcentaje"
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'cuenta_compra_id',
                            'value' => isset($model->cuenta_compra_id) ? $model->cuentaCompra->cod_completo : '',
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'cuenta_compra_id',
                            'value' => isset($model->cuenta_compra_id) ? $model->cuentaCompra->nombre : '',
                            'label' => "Nombre de Cuenta",
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'cuenta_venta_id',
                            'value' => isset($model->cuenta_venta_id) ? $model->cuentaVenta->cod_completo : '',
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                        [
                            'attribute' => 'cuenta_venta_id',
                            'value' => isset($model->cuenta_venta_id) ? $model->cuentaVenta->nombre : '',
                            'label' => "Nombre de Cuenta",
                            'valueColOptions' => ['style' => 'width:30%'],
                        ],
                    ],
                ],
            ],
        ]);
    } catch (Exception $e) {
        \common\helpers\FlashMessageHelpsers::createWarningMessage($e->getMessage());
    } ?>

</div>
