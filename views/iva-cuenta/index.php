<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Iva Cuentas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iva-cuenta-index">

    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-iva-cuenta-create') ?
            Html::a('Crear Iva Cuenta', ['create'], ['id' => 'boton_add_ivacta', 'class' => 'btn btn-success']) : null ?>
    </p>

    <?php
    $template = '';
    foreach (['view', 'update', 'delete'] as $_v)
        if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-entidad-{$_v}"))
            $template .= "&nbsp{{$_v}}";
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//            'id',
                'iva.porcentaje',
                [
                    'attribute' => 'cuenta_compra_id',
                    'value' => 'cuentaCompra.cod_completo',
                    'label' => 'Código de cuenta',
                ],
                [
                    'attribute' => 'cuenta_compra_id',
                    'value' => 'cuentaCompra.nombre',
                ],
                [
                    'attribute' => 'cuenta_venta_id',
                    'value' => 'cuentaVenta.cod_completo',
                    'label' => 'Código de cuenta',
                ],
                [
                    'attribute' => 'cuenta_venta_id',
                    'value' => 'cuentaVenta.nombre',
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
    } catch (Exception $e) {
        throw $e;
    } ?>
</div>

<?php
$scripts = [];

$scripts[] = <<<JS
// Abrir modal para agregar detalles al presionar tecla +
$(document).keypress(function (e) {
    if(document.activeElement.tagName !== "INPUT" && (e.keycode === 43 || e.which === 43)) {
        document.getElementById('boton_add_ivacta').click();
    }
});
JS;

foreach ($scripts as $s) $this->registerJs($s);
?>
<!---->
<?php
//$CSS = <<<CSS
//.table tbody tr:hover {
//    background-color: #ebffdd;
//}
//CSS;
//$this->registerCss($CSS);
//?>
