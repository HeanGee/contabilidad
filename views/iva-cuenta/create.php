<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\IvaCuenta */

$this->title = 'Crear Iva Cuenta';
$this->params['breadcrumbs'][] = ['label' => 'Iva Cuentas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iva-cuenta-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
