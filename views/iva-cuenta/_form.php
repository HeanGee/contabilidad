<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;

use backend\models\Iva;
use backend\modules\contabilidad\models\PlanCuenta;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\IvaCuenta */
/* @var $form kartik\form\ActiveForm */
?>

<div class="iva-cuenta-form">

    <?php
    $form = ActiveForm::begin();

    try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => [       // 1 column layout
                'iva_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => ArrayHelper::map(
                            Iva::find()
                                ->asArray()
                                ->all(),
                            'id', 'porcentaje'
                        ),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                    'hint' => 'Seleccione un IVA',
                ],
                'cuenta_compra_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                    'hint' => 'Seleccione una cuenta',
                ],
                'cuenta_venta_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                    'hint' => 'Seleccione una cuenta',
                ],
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
