<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\search\DetalleCompraSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalle-compra-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'factura_compra_id') ?>

    <?= $form->field($model, 'iva_id') ?>

    <?= $form->field($model, 'subtotal') ?>

    <?= $form->field($model, 'plan_cuenta_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
