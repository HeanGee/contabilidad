<?php

use backend\modules\contabilidad\models\PlanCuenta;
use faryshta\assets\ActiveFormDisableSubmitButtonsAsset;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\DetalleCompra */
/* @var $form kartik\form\ActiveForm */
?>

<div class="detalle-compra-form">

    <?php ActiveFormDisableSubmitButtonsAsset::register($this) ?>

    <?php $form = ActiveForm::begin([
        'id' => '_form_modal',
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options' => ['class' => 'disable-submit-buttons']
    ]); ?>

    <?php try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [       // 2 column layout
                'plan_cuenta_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => ArrayHelper::map(PlanCuenta::getCuentaLista(true), 'id', 'text'),
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno',
                        ],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'hint' => 'Seleccione una cuenta',
                    'label' => 'Cuenta'
                ],
                'subtotal' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => 'Monto'
                ],
                'cta_contable' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => ['debe' => 'Debe', 'haber' => 'Haber'],
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno',
                        ],
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'hint' => 'Seleccione una cuenta',
                    'label' => 'Debe/Haber'
                ],
                'cta_principal' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                        'pluginEvents' => [
                            'change' => ""
                        ],
                        'data' => ['si' => 'Si', 'no' => 'No'],
                        'initValueText' => 'No',
                        'options' => [
                            'placeholder' => 'Por favor Seleccione Uno', 'disabled' => false
                        ]
                    ],
                ],
            ]
        ]);
    } catch (Exception $e) {
    } ?>

    <div class="form-group">
        <?= Html::submitButton('Agregar', ['data' => ['disabled-text' => 'Creando...'], 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $script = <<<JS
        // obtener la id del formulario y establecer el manejador de eventos
        $("form#_form_modal").on("beforeSubmit", function(e) {
            var form = $(this);
            $.post(
                form.attr("action")+"&submit=true",
                form.serialize()
            )
            .done(function(result) {
                form.parent().html(result.message);
                $.pjax.reload({container:"#detalles_grid", async:false});
                $.pjax.reload({container:"#flash_message_id", async:false});
                $("#modal").modal("hide");      
                $("modal-body").html("");
            });
            return false;
        }).on("submit", function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        });

        $(document).on('focus', '.select2', function (e) {
          if (e.originalEvent) {
            $(this).siblings('select').select2('open');    
          } 
        });
        
        $("#modal").on("shown.bs.modal", function () {
            var plancuenta = $("#detallecompra-plan_cuenta_id");
            if(typeof plancuenta.val() !== 'undefined' && plancuenta.val().length === 0)
                plancuenta.select2("open");
            else {
                $('#detallecompra-subtotal').focus();
            }
        });
JS;
    //    js para guardao y actualización de grilla
    $this->registerJs($script);
    ?>


</div>
