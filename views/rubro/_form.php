<?php

use backend\modules\contabilidad\models\Rubro;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Rubro */
/* @var $form ActiveForm */
?>

<div class="rubro-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'nombre' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Nombre...'],
                ],
                'estado' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => Rubro::getEstados(false),
                    ],
                ],
            ],
        ]);
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 5,
            'autoGenerateColumns' => false,
            'attributes' => [
                'descripcion' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Descripción...'],
                ],
            ],
        ]);
    } catch (Exception $e) {
        print $e;
    }
    ?>

    <div class="form-group">
        <?php $class = Yii::$app->controller->action->id == 'create' ? 'btn btn-success' : 'btn btn-primary';
        echo Html::submitButton('Guardar', ['class' => $class]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
