<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Rubro */

$this->title = 'Datos del Rubro: ' . ucfirst($model->nombre);
$this->params['breadcrumbs'][] = ['label' => 'Rubros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubro-view">

    <p>
        <?php $permisos = [
            'view' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-rubro-view'),
            'index' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-rubro-index'),
            'create' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-rubro-create'),
            'update' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-rubro-update'),
            'delete' => \common\helpers\PermisosHelpers::getAcceso('contabilidad-rubro-delete'),
        ]; ?>
        <?= $permisos['index'] ? Html::a('Ir a Rubros', ['index'], ['class' => 'btn btn-info']) : null ?>
        <?= $permisos['update'] ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= $permisos['delete'] ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Está seguro de borrar este Rubro?',
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Prestamo',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'nombre',
                'descripcion',
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => ($model->estado == 'activo') ? "<label class='label label-success'>$model->formattedEstado</label>" : "<label class='label label-danger'>$model->formattedEstado</label>",
                ],
            ],
        ]);
    } catch (Exception $e) {
        print $e;
    } ?>

</div>
