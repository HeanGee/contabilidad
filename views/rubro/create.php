<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Rubro */

$this->title = 'Crear Nuevo Rubro';
$this->params['breadcrumbs'][] = ['label' => 'Rubros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubro-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
