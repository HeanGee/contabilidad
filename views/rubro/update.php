<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Rubro */

$this->title = 'Modificar Rubro: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Rubros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="rubro-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
