<?php

use backend\modules\contabilidad\models\Rubro;
use common\helpers\PermisosHelpers;
use kartik\select2\Select2;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\RubroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rubros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubro-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= \common\helpers\PermisosHelpers::getAcceso('contabilidad-rubro-create') ?
            Html::a('Registrar Nuevo Rubro', ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?php try {
        $template = '';
        foreach (['view', 'update', 'delete'] as $_v)
            if (PermisosHelpers::getAcceso("recibo-{$_v}"))
                $template .= "&nbsp&nbsp&nbsp{{$_v}}";

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                'id',
                'nombre',
                'descripcion',
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => Rubro::getEstados(true),
//                        'hideSearch' => true,
                        'pluginOptions' => [
//                            'width' => '120px'
                        ]
                    ])
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
    } catch (Exception $e) {
        print $e;
    } ?>
</div>
