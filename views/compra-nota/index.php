<?php

use backend\modules\contabilidad\models\Compra;
use common\helpers\PermisosHelpers;
use kartik\date\DatePicker;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\CompraSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notas de Crédito/Débito';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compra-index">
    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-compra-nota-create') ? Html::a('Registrar Nota', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php
    $template_grid = '';
    foreach (['view', 'update', 'delete', 'bloquear', 'desbloquear'] as $_v)
        if (\common\helpers\PermisosHelpers::getAcceso("contabilidad-compra-nota-{$_v}"))
            $template_grid .= "&nbsp&nbsp&nbsp{{$_v}}";

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'hover' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'label' => 'Nro Comprobante',
                    'value' => 'nro_factura',
                    'attribute' => 'nro_factura',
                ],
                [
                    'attribute' => 'fecha_emision',
                    'format' => ['date', 'php:d-m-Y'],
                    'headerOptions' => ['class' => 'col-md-2'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'fecha_emision_f',
                        'language' => 'es',
                        'pjaxContainerId' => 'comprobante-list',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true,
                        ]
                    ])
                ],
                [
                    'label' => 'Proveedor',
                    'attribute' => 'nombre_entidad',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return "{$model->entidad->razon_social} <strong>(RUC: {$model->entidad->ruc})</strong>";
                    }
                ],
                [
                    'attribute' => 'total',
                    'label' => 'Total',
                    'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
                    'value' => function ($model) {
                        /** @var Compra $model */
                        return $model->getTotal();
                    },
                ],
//                [
//                    'attribute' => 'tipo',
//                    'label' => 'Tipo',
//                    'value' => function ($model) {
//                        return implode(' ', array_map('ucfirst', explode('_', $model->tipo)));
//                    },
//                    'filter' => Select2::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'tipo',
//                        'data' => [
//                            'todos' => 'Todos',
//                            'nota_credito' => 'Nota Credito',
//                            'nota_debito' => 'Nota Debito'
//                        ],
//                        'pluginOptions' => [
//                            'width' => '120px'
//                        ],
//                        'initValueText' => 'todos'
//                    ])
//                ],

                [
                    'label' => 'Tipo',
                    'attribute' => 'nombre_documento',
                    'value' => 'tipoDocumento.nombre'
                ],
                [
                    'attribute' => 'timbrado',
                    'value' => 'timbradoDetalle.timbrado.nro_timbrado'
                ],
                [
                    'attribute' => 'moneda_id',
                    'value' => 'moneda.nombre',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'moneda_id',
                        'data' => Compra::getMonedas(true),
                        'pluginOptions' => [
                            'width' => '120px'
                        ],
                        'initValueText' => 'todos',
                    ])
                ],
                [
                    'attribute' => 'estado',
                    'value' => 'estado',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => Compra::getEstados(true),
                        'pluginOptions' => [
                            'width' => '120px'
                        ]
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if (in_array($action, ['update', 'view']) && $model->factura_compra_id == '' && $model->tipo == 'factura') {
                            $url = Url::to(["/contabilidad/compra/$action", 'id' => $model->id]);
                            return $url;
                        }
                        return Url::to([$action, 'id' => $model->id]);
                    },
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => '',
                                'title' => Yii::t('yii', 'Borrar'),
                                'data' => [
                                    'confirm' => '¿Está seguro de eliminar este elemento?',
                                    'method' => 'post',
                                ],
                            ]) : '';
                        },
                        'update' => function ($url, $model) {
                            return $model->bloqueado == 'no' ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Editar'),
                            ]) : '';
                        },
                        'view' => function ($url, $model) {
                            return 1 ? Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('yii', 'Ver'),
                            ]) : '';
                        },
                        'bloquear' => function ($url, $model, $key) {
                            /** @var Compra $model */
                            $url = ($model->bloqueado == 'no') ?
                                Html::a('<span>Bloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => Yii::t('app', 'Bloquear'),
                                        'data-confirm' => 'Está seguro que desea bloquear la nota?'
                                    ]) : '';
                            return $url;
                        },
                        'desbloquear' => function ($url, $model, $key) {
                            /** @var Compra $model */
                            $url = ($model->bloqueado == 'si') ?
                                Html::a('<span>Desbloquear</span>', $url,
                                    [
                                        'class' => 'btn btn-success btn-xs',
                                        'title' => Yii::t('app', 'Desbloquear'),
                                        'data-confirm' => 'Está seguro que desea desbloquear la nota?'
                                    ]) : '';
                            return $url;
                        },
                    ],
                    'template' => $template_grid,
                ],
            ]
        ]);

    } catch (\Exception $e) {
//        print $e;

    }
    ?>
</div>