<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 14/12/2018
 * Time: 17:27
 */

// use imports here...

/* @var $this yii\web\View */
/* @var $model \backend\modules\contabilidad\models\compra */
?>

<?php
$scripts = <<<JS
// Código estático recurrente para abrir modales.
$(document).on('click', '.tiene_modal', (function () {
    var boton = $(this);
    var title = boton.data('title');
    $.get(
        boton.data('url'),
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

// Operaciones al borrar un detalle compra.
$(document).on('click', '.ajaxDelete', function (e) {
    e.preventDefault();
    var deleteUrl = $(this).attr('delete-url');
    krajeeDialog.confirm('Está seguro?',
        function (result) {
            if (result) {
                $.ajax({
                    url: deleteUrl,
                    type: 'post',
                    error: function (xhr, status, error) {
                        alert('There was an error with your request.' + xhr.responseText);
                    }
                }).done(function (data) {
                    $.pjax.reload({container: "#detalles_grid", async: false});
                });
            }
        }
    );
});

$(document).on('click', '.modal-actfijo', (function () {
    let boton = $(this);
    let title = boton.attr('title');
    let prefijo = getPrefijoId(boton) + 'ivas-iva_';
    let factura_compra_id = $('#compra-factura_compra_id').val();
    $.get(
        boton.data('url') + '&prefijoFilaPlantilla=' + prefijo + '&compra_id=' + factura_compra_id + "&compraNotaCredito_id={$model->id}",
        function (data) {
            if (data !== "") {
                let modal = $(boton.data('target'));
                $('.modal-body', modal).html(data);
                modal.modal();
                $('.modal-header', modal).css('background', '#3c8dbc');
                // if (title)
                $('.modal-title', modal).html(title);
            } else {
                $.pjax.reload({container: "#flash_message_id", async: false});
                $("#modal").modal("hide");
                $("modal-body").html("");
            }
        }
    );
}));

$(document).on('click', '.add_timbrado', (function () {
    var boton = $(this);
    var title = boton.data('title');
    var tipo_documento_id = getTipodocumentoId(); // aunque es nota, este metodo devuelve id del tipo de documento y no el de set.
    var fieldNroNota = $('#compra-nro_factura');
    var extra_url = '&entidadRuc=' + $('#compra-ruc').val()
        + '&prefijo=' + fieldNroNota.val().substring(0, 7)
        + '&nro=' + fieldNroNota.val().substring(8, 15)
        + '&tipoDocumentoId=' + tipo_documento_id
        + '&timbrado_id=' + $('#compra-timbrado_id').val();
    // ya no hace falta enviarle si es nota o no.
    
    $.get(
        boton.data('url') + extra_url,
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

$(document).on('click', '.update_timbrado', (function () {
    var boton = $(this);
    var title = boton.data('title');
    var tipo_documento_id = getTipodocumentoId();
    var prefijo = $('#compra-nro_factura').val().substring(0, 7);
    var nro = $('#compra-nro_factura').val().substring(8, 15);
    var timbrado_id = $('#compra-timbrado_id').val();
    $.get(
        boton.data('url')
         + '&entidadRuc=' + $('#compra-ruc').val()
         + '&prefijo=' + prefijo
         + '&nro=' + nro
         + '&tipoDocumentoId=' + tipo_documento_id
         + '&timbradoId=' + timbrado_id
         + '&facturaCompraId=' + "{$model->id}",
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            modal.modal();
            $('.modal-header', modal).css('background', '#3c8dbc');
            if (title)
                $('.modal-title', modal).html(title);
        }
    );
}));

// Código estático recurrente para abrir modales.
$(document).on('click', '.modal-act-bio-compra-nota-manager', (function () {
    var boton = $(this);
    var title = boton.attr('title');
    let fila_id = boton.attr('id').replace('actbio_compra_nota_manager_btn', '');
    let campo_iva_activo = "";
    let compra_id = $('#compra-factura_compra_id').val();
    $(':input[id^="'+fila_id+'ivas-iva"]').each(function() {
        if ($(this).attr('disabled') !== 'disabled') {
            campo_iva_activo = $(this).attr('id'); return false;
        }
    });
    let url = boton.data('url') + "&nota_id={$model->id}&compra_id=" + compra_id + '&campo_iva_activo=' + campo_iva_activo;
    
    $.get(
        url,
        function (data) {
            var modal = $(boton.data('target'));
            $('.modal-body', modal).html(data);
            $('.modal-header', modal).css('background', '#3c8dbc');
            $('.modal-title', modal).html(title);
            modal.modal();
        }
    );
}));
JS;
$this->registerJs($scripts);
?>