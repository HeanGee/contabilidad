<?php
/**
 * Created by PhpStorm.
 * User: miguelpereiralegal
 * Date: 7/17/18
 * Time: 6:29 PM
 */

use backend\helpers\HtmlHelpers;
use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\ParametroSistema;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use common\helpers\FlashMessageHelpsers;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\dialog\Dialog;
use kartik\form\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Compra */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nota-form">
    <?php
    $form = ActiveForm::begin(['id' => 'factura_form']);
    $action = Yii::$app->controller->action->id;

    try {
        if (empty($model->tipo)) { // sólo cuando el registro es nuevo va a estar vacío
            $model->tipo = 'nota_credito';
        } else {
            (!isset($model->facturaCompra)) || $model->emision_factura = date('d-m-Y', strtotime($model->facturaCompra->fecha_emision));
        }

        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => false,
            'rows' => [
                [
                    'attributes' => [
                        'tipo' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '1'],
                            'value' => $form->field($model, 'tipo')->widget(Select2::className(), [
                                'options' => [
                                    'placeholder' => 'Seleccione un tipo de Nota...',
                                    'disabled' => !empty($model->id)
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                    'data' => [
                                        [
                                            'id' => 'nota_credito',
                                            'text' => 'Nota Credito',
                                            'tipo_doc_set_id' => ParametroSistema::getValorByNombre('nota_credito_set_id'),
                                            'registro_tipo' => 'factura'
                                        ],
                                        [
                                            'id' => 'nota_debito',
                                            'text' => 'Nota Debito',
                                            'tipo_doc_set_id' => ParametroSistema::getValorByNombre('nota_debito_set_id'),
                                            'registro_tipo' => 'nota_credito'
                                        ]
                                    ]
                                ],
                                'pluginEvents' => [
                                    'change' => "function() { manejarSeleccionTipo(); }"
                                ],
                                'disabled' => !empty($model->id),
                                'initValueText' => !empty($model->tipo) ? implode(' ', array_map('ucfirst', explode('_', $model->tipo))) : 'Credito'
                            ])
                        ]
                    ]
                ]
            ]
        ]);

        echo '<fieldset><legend>Datos de Factura</legend></fieldset>';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'ruc' => [
                            'type' => Form::INPUT_TEXT,
                            'columnOptions' => ['colspan' => '1'],
                        ],
                        'razon_social' => [
                            'columnOptions' => ['colspan' => '4'],
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'readonly' => true,
                            ],
                        ],
                        'factura_compra_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '3'],
                            'value' => $form->field($model, 'factura_compra_id')->widget(Select2::className(), [
                                'options' => [
                                    'placeholder' => 'Seleccione una factura...',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => Url::to(['get-facturas-by-ruc']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression("
                                            function(params) { 
                                                return {
                                                    q:params.term, 
                                                    ruc:$('#compra-ruc').val(),
                                                    tipo: ($('#compra-tipo').val() === 'nota_credito') ? 'factura' : 'nota_credito',
                                                };
                                            }
                                        "),
                                    ],
                                ],
                                'initValueText' => isset($model->facturaCompra) ? "{$model->facturaCompra->nro_factura} ({$model->facturaCompra->tipoDocumento->nombre})" : '',
                            ])->label(($model->tipo == 'nota_credito' ? 'Factura ' : 'Nota ') . HtmlHelpers::InfoHelpIcon('factura'))
                        ],

                        'emision_factura' => [
                            'columnOptions' => ['colspan' => '2'],
                            'type' => Form::INPUT_TEXT,
                            'label' => 'Emisión ' . ($model->tipo == 'nota_credito' ? 'Factura' : 'Nota'),
                            'options' => [
                                'readonly' => true
                            ]
                        ]
                    ]
                ],
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'moneda_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'moneda_id')->widget(Select2::className(), [
//                                'disabled' => true,
                                'options' => [
                                    'placeholder' => 'Seleccione una moneda',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'data' => Compra::getMonedas(),
                                ],
                                'initValueText' => !empty($model->moneda_id) ? ($model->moneda_id . ' - ' . $model->moneda->nombre) : ''
                            ])
                        ],
                        'cotizacion' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '3'],
                            'value' => $form->field($model, 'cotizacion')->widget(NumberControl::className(), [
                                'value' => 0.00,
                                'maskedInputOptions' => [
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'rightAlign' => true,
                                    'allowMinus' => false
                                ],
//                                'readonly' => true
                            ]),
                        ],
                        'cotizacion_factura_asociada' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '3'],
                            'value' => $form->field($model, 'cotizacion_factura_asociada')->widget(NumberControl::className(), [
                                'value' => 0.00,
                                'maskedInputOptions' => [
                                    'groupSeparator' => '.',
                                    'radixPoint' => ',',
                                    'rightAlign' => true,
                                    'allowMinus' => false
                                ],
                                'readonly' => true
                            ]),
                        ],
                        'nota_remision' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'nota_remision')->widget(MaskedInput::className(), [
                                'mask' => '999-999-9999999',
                                'options' => [
                                    'class' => 'form-control',
                                    'readonly' => true
                                ]
                            ])
                        ],
                    ]
                ]
            ]
        ]);
//        echo $form->field($model, 'moneda_id')->hiddenInput()->label(false);

        // No se sabe por que utilidad se hizo esto, lo que provocaba fue que al postear, nunca va el ruc y factura_compra_id modificada.
//        if (Yii::$app->controller->action->id == 'update') {
//            echo $form->field($model, 'factura_compra_id')->hiddenInput()->label(false);
//            echo $form->field($model, 'ruc')->hiddenInput()->label(false);
//        }

        $tipodocNotaCreditoId = Compra::getTipodocNotaCredito()->id;
        $tipodocNotaDebitoId = Compra::getTipodocNotaDebito()->id;

        echo '<fieldset><legend>Datos de Nota</legend></fieldset>';
        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'columns' => 12,
                    'attributes' => [
                        'tipo_documento_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'tipo_documento_id')->widget(Select2::className(), [
                                'options' => [
                                    'placeholder' => 'Seleccione un tipo de doc...',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => Url::to(['tipo-documento/get-tipodoc-para-nota']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression("
                                            function(params) { 
                                                return {
                                                    q:params.term, 
                                                    asociacion: 'proveedor',
                                                    id: '" . ($action == 'update' ? $model->id : '') . "',
                                                    para: $('#compra-tipo').select2('data')[0]['id'],
                                                };
                                            }
                                        "),
                                    ],
                                ],
                                'initValueText' => ($model->tipo_documento_id != '') ? "{$model->tipo_documento_id} - {$model->tipoDocumento->nombre}" : ''
                            ]),
                        ],
                        'nro_factura' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'nro_factura')->widget(MaskedInput::className(), [
                                'mask' => '999-999-9999999',
                                'options' => [
                                    'class' => 'form-control',
//                                    'disabled' => !empty($model->id)
                                ]
                            ])->label('Nro. de Nota'),
                            'options' => ['placeholder' => 'Ingrese nro de Nota...'],
                        ],
                        'fecha_emision' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'fecha_emision')->widget(MaskedInput::className(), [
                                'name' => 'fecha_emision',
                                'clientOptions' => ['alias' => 'dd-mm-yyyy'],
                            ]),
                        ],
                        'timbrado_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'timbrado_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione uno ...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => Url::to(['timbrado/get-timbrados-vigentes']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression("
                                            function(params) { 
                                                return {
                                                    q:params.term, 
                                                    ruc:$('#compra-ruc').val(),
                                                    fecha:$('#compra-fecha_emision').val(),
                                                    nro_factura:$('#compra-nro_factura').val(),
                                                    tipo_documento_id: $('#compra-tipo_documento_id').val(),
                                                    vencidos: '0',
                                                };
                                            }
                                        "),
                                    ]
                                ],
                                'initValueText' => !empty($model->timbrado_id) ? $model->getTimbrado() : ''
                            ])->label('Timbrado ' . HtmlHelpers::InfoHelpIcon('timbrado'))
                        ],
                        'obligacion_id' => [
                            'type' => Form::INPUT_RAW,
                            'columnOptions' => ['colspan' => '2'],
                            'value' => $form->field($model, 'obligacion_id')->widget(Select2::className(), [
                                'options' => ['placeholder' => 'Seleccione una obligac...'],
                                'pluginOptions' => [
                                    'data' => Compra::getObligacionesEmpresaActual(Yii::$app->session->get('core_empresa_actual'), null),
                                    'allowClear' => true,
                                ],
                                'initValueText' => ($model->obligacion_id != '') ? ucfirst($model->obligacion->nombre) : "",
                            ]),
                        ],
                    ]
                ]
            ]
        ]);

        // Plantillas Dinamicas
//        echo $this->render('plantilla/_fieldset', ['model' => $model, 'form' => $form]);
        echo $this->render('plantilla/_form_plantillas', ['model' => $model, 'form' => $form]);
        $bluePrint = Yii::$app->session->get('$bluePrint');
        $bluePrint = Yii::$app->session->remove('$bluePrint');
        echo '<fieldset><legend>&nbsp;</legend></fieldset>';

        $totales = [];
        $impuestos = [];
        /** @var IvaCuenta $iva_cta */
        foreach (IvaCuenta::find()->all() as $iva_cta) {
            $totales['iva-' . $iva_cta->iva->porcentaje . ''] = [
                'label' => 'Total ' . (($iva_cta->iva->porcentaje !== 0) ? $iva_cta->iva->porcentaje . '%' : 'Exenta'),
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => NumberControl::class,
                'columnOptions' => ['colspan' => '2'],
                'value' => 0.00,
                'options' => [
                    'maskedInputOptions' => [
                        'groupSeparator' => '.',
                        'radixPoint' => ',',
                        'allowMinus' => false,
                    ],
                    'readonly' => true,
                ],
            ];
            if ($iva_cta->iva->porcentaje != 0) {
                $impuestos['impuesto-' . $iva_cta->iva->porcentaje . ''] = [
                    'label' => 'I.V.A. ' . $iva_cta->iva->porcentaje . '%',
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => NumberControl::class,
                    'columnOptions' => ['colspan' => '2'],
                    'options' => [
                        'value' => 0.00,
                        'maskedInputOptions' => [
                            'groupSeparator' => '.',
                            'radixPoint' => ',',
                            'rightAlign' => true,
                            'allowMinus' => false,
                        ],
                        'readonly' => true,
                    ],
                ];
            }
        }
        uksort($totales, 'strnatcasecmp');
        uksort($impuestos, 'strnatcasecmp');
        $totales = array_merge($totales, $impuestos);
        echo Form::widget([
            'formName' => 'totales_iva',
            'columns' => 12,
            'attributeDefaults' => [
                'type' => Form::INPUT_TEXT,
                'labelOptions' => ['colspan' => '2'],
                'inputContainer' => ['colspan' => '2'],
                'container' => ['class' => 'form-group'],
            ],
            'attributes' => $totales,
        ]);

        echo FormGrid::widget([
            'model' => $model,
            'form' => $form,
            'autoGenerateColumns' => true,
            'rows' => [
                [
                    'autoGenerateColumns' => false,
                    'attributes' => [
                        'array' => [
                            'columns' => 12,
                            'attributes' => [
                                'total' => [
                                    'type' => Form::INPUT_RAW,
                                    'value' => $form->field($model, 'total')->widget(NumberControl::className(), [
                                        'value' => 0.00,
                                        'maskedInputOptions' => [
                                            'groupSeparator' => '.',
                                            'radixPoint' => ',',
                                            'rightAlign' => false,
                                            'allowMinus' => false,
                                        ],
                                        'readonly' => true,
                                    ]),
                                    'columnOptions' => ['colspan' => '3'],
                                ],
                                '_total_ivas' => [
                                    'type' => Form::INPUT_RAW,
                                    'value' => $form->field($model, '_total_ivas')->widget(NumberControl::className(), [
                                        'value' => 0.00,
                                        'readonly' => true,
                                        'maskedInputOptions' => [
                                            'groupSeparator' => '.',
                                            'radixPoint' => ',',
                                            'rightAlign' => false,
                                            'allowMinus' => false,
                                        ],
                                    ]),
                                    'columnOptions' => ['colspan' => '3']
                                ]
                            ]
                        ]
                    ],
                ],
            ]
        ]);

//        echo $this->render('../compra/_form_js.php', ['totales' => $totales, 'model' => $model]);

        $botonText = 'Guardar';
        $botonClass = 'btn btn-';
        if ($action == 'create') {
//            $botonText .= ' y Siguiente';
            $botonClass .= 'success';
        } else {
            $botonClass .= 'primary';
        }

        echo '<div class="form-group">';
        echo Html::submitButton($botonText, ['id' => 'btn_submit_nota', 'class' => $botonClass,
            "data" => ($action == 'update') ? [
                'confirm' => 'Desea guardar los cambios?',
                'method' => 'post',
            ] : []
        ]);
        echo '</div>';

        $es_nota = in_array($model->tipo, ['nota_credito', 'nota_debito']) ? $model->tipo : false;
        echo $this->render('detalle/_form_detalle', ['tipodocNotaDebitoId' => $tipodocNotaDebitoId, 'tipodocNotaCreditoId' => $tipodocNotaCreditoId, 'es_nota' => $es_nota]);
        echo $this->render('../compra/_form_asiento_simulator');
        echo $this->render('js-files/modal-manager-js', ['model' => $model]);

        ActiveForm::end();
        echo Dialog::widget();

        $boton = Html::label('Add Tim.', 'boton_add_timbrado', ['class' => 'control-label']) . '<br/>'
            . Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'id' => 'boton_add_timbrado',
                'disabled' => false,
                'style' => "display: yes;",
                'type' => 'button',
                'title' => 'Agregar Timbrado.',
                'class' => 'btn btn-success add_timbrado',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['timbrado/create-new-timbrado-compra']),
                'data-pjax' => '0',
            ]) . '<br/><br/>';
        $boton_update = Html::label('Update Tim.', 'boton_update_timbrado', ['class' => 'control-label']) . '<br/>'
            . Html::button('<i class="glyphicon glyphicon-pencil"></i>', [
                'id' => 'boton_update_timbrado',
                'disabled' => ($action == 'create'),
                'style' => "display: yes;",
                'type' => 'button',
                'title' => 'Editar Timbrado.',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'class' => 'btn btn-primary update_timbrado',
                'data-url' => Url::to(['compra-nota/update-timbrado-compra']),
                'data-pjax' => '0',
            ]) . '<br/><br/>';
        $infoColor = HtmlHelpers::InfoColorHex(true);
        $nuevo = $model->isNewRecord;
        $plantillas = Json::encode(PlantillaCompraventa::getPlantillasLista('compra', false, false));
        $entidades_url = Url::to(['compra-nota/get-entidades-for-notas', 'action' => $action]);
        $url_cotizacion = Json::htmlEncode(\Yii::t('app', Url::to(['detalle-compra/get-cotizacion'])));
        $url_getPlantillas = Url::to(['plantilla-compraventa/get-plantilla']);
        $url_timbrado_load = Json::htmlEncode(\Yii::t('app', Url::to(['compra-nota/cargar-timbrado'])));
        $url_getFacturasByRuc = Url::to(['compra-nota/get-facturas-by-ruc']);
        $url_datosFacturaForNota = Json::htmlEncode(\Yii::t('app', Url::to(['compra-nota/get-datos-facturas-by-id'])));
        $url_getRazonSocialByRuc = Json::htmlEncode(\Yii::t('app', Url::to(['compra/get-razon-social-by-ruc'])));
        $js_script_ready = <<<JS
        var id_to_focus = ""; //Variable para volver y hacer focus a input de donde proviene un evento
        
//if ("$nuevo".length) {
//    $('#compra-ruc').select2('open');
//}

function construirMaskedInput(selector, number_type = 'integer', integerDigits = 17) {
    // console.log('aplicar maskedinput:', selector);
    $(selector).inputmask({
        "alias": "numeric",
        "digits": (number_type === 'integer') ? 0 : 2,
        "groupSeparator": ".",
        "autoGroup": true,
        "autoUnmask": true,
        "unmaskAsNumber": true,
        "radixPoint": ",",
        "digitsOptional": false,
        "placeholder": "0",
        "rightAlign": true,
        // allowMinus: false,
        integerDigits: integerDigits,
        fractionalDigits: (number_type === 'integer') ? 0 : 2,
        removeMaskOnSubmit: true,
    });
}

function rellenarFacturasByEntidad() {
    let select2Facturacompraid = $('#compra-factura_compra_id');
    if (select2Facturacompraid.data('select2')) {
        select2Facturacompraid.select2("destroy");
        select2Facturacompraid.html("<option><option>");
        select2Facturacompraid.select2({
            theme: 'krajee',
            placeholder: '',
            language: 'en',
            width: '100%',
            data: [],
        });
    }

    //Para que se ejecute el evento asociado al change.
    select2Facturacompraid.trigger('change');
    $.ajax({
        url: "{$url_getFacturasByRuc}",
        type: 'get',
        data: {
            ruc: $('#compra-ruc').val(),
            tipo: $('#compra-tipo').select2('data')[0]['registro_tipo'],
        },
        success: function (data) {
            select2Facturacompraid.select2({
                theme: 'krajee',
                placeholder: 'Seleccione una opcion...',
                language: 'en',
                width: '100%',
                allowClear: true, //hacer true al implementar lo nuevo 
                data: data.results,
            });

            let valueSetted = false;
            if (select2Facturacompraid.val() === '') {
                $.each(data.results, function (index, element) {
                    if (element['id'] === facturacompraIdInicial) {
                        select2Facturacompraid.val(facturacompraIdInicial).trigger('change');
                        valueSetted = true;
                        return false;
                    }
                });
            }

            // if (!valueSetted) {
                select2Factura = $('#compra-factura_compra_id');
                select2Entidad = $('#compra-ruc');
                if (select2Factura.val() === '' && select2Entidad.val() !== '')
                    select2Factura.select2('open');
                delete select2Factura;
                delete select2Entidad;
            // }
        }
    });
}

function setCotizaciones() {
    $.ajax({
        url: $url_cotizacion,
        type: 'post',
        data: {
            fecha_emision: $('#compra-fecha_emision').val(),
            moneda_id: $('#compra-moneda_id').val(),
        },
        success: function (data) {
            $.pjax.reload({container: "#flash_message_id", async: false});
            if (parseFloat(data) % 1 === 0) {
                data = parseFloat(data) - (parseFloat(data) % 1);
            }
            let monedaField = $('#compra-cotizacion-disp');
            if (monedaField.val() === '') {
                monedaField.val(data).trigger('change');
                $('#compra-cotizacion').val(data);
                $('form[id^="factura_form"]').yiiActiveForm('validateAttribute', 'compra-cotizacion');
            }
        }
    });
}

function activateButtonsForModals(inputPlantillaId, showHideExtraBtn = true) {
    if (!esCredito()) return false;
    
    let prefijo = '#' + getPrefijoId(inputPlantillaId),
        sufijo;

    if (showHideExtraBtn) {
        // Mostrar botón para activo fijo.
        sufijo = 'actfijo_manager_button';
        if (inputPlantillaId.select2('data')[0]['activo_fijo'] === 'si') {
            $(prefijo + sufijo).prop('style', "display: yes");
        } else {
            $(prefijo + sufijo).prop('style', "display: none");
        }
    
        // Mostrar botón para activo biologico.
        sufijo =  'actbio_compra_nota_manager_btn';
        if (inputPlantillaId.select2('data')[0]['activo_biologico'] === 'si') {
            console.log(prefijo + sufijo, $(prefijo + sufijo));
            $(prefijo + sufijo).prop('style', "display: yes");
        } else {
            $(prefijo + sufijo).prop('style', "display: none");
        }
    } else {  // cuando esta func. es invocada por el boton para agregar plantilla, no mostrar los botones extra porque
                // posiblemente corresponde al caso donde no hay factura asociada.
        // Mostrar botón para activo fijo.
        sufijo = 'actfijo_manager_button';
        $(prefijo + sufijo).prop('style', "display: none");
    
        // Mostrar botón para activo biologico.
        sufijo =  'actbio_compra_nota_manager_btn';
        $(prefijo + sufijo).prop('style', "display: none");
    }
}

function cargarTimbrado() {
    console.log('cargarTimbrado de compra-nota/form');
    var tipo_doc_id = getTipodocumentoId();
    var nro_factura = $('#compra-nro_factura').val();
    var entidad_ruc = $('#compra-ruc').val();
    var fecha_emision = $('#compra-fecha_emision').val();
    if (nro_factura.split("_").join("").length === 15 && entidad_ruc !== "" && tipo_doc_id !== "") {
        $.ajax({
            url: $url_timbrado_load,
            type: 'post',
            data: {
                entidad_ruc: entidad_ruc,
                nro_factura: nro_factura,
                fecha_emision: fecha_emision,
                timbrado_vencido: '0',
                tipodocumento_id: tipo_doc_id,
            },
            success: function (data) {
                if (data[0] === 'mostrar_boton') {
                    if (data[1].length > 0)
                        $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": data[1][0]['id'], "text": data[1][0]['text'], "vencido": data[1][0]['vencido']}});
                    else
                        $("#compra-timbrado_id").data('select2').trigger('select', {data: {"id": '', "text": ''}});
                   
                    if (id_to_focus !== "") 
                        $("#" + id_to_focus).focus();
                    
                    $('#boton_add_timbrado').prop('disabled', false);
                    $('#boton_update_timbrado').prop('disabled', true);
                } else {
                    if (data !== '' && data.length === 1) {
                        $("#compra-timbrado_id").data('select2').trigger('select', {
                            data: {
                                "id": data[0]['id'],
                                "text": data[0]['nro_timbrado'],
                                "vencido": data[0]['vencido']
                            }
                        });
                        // $('#boton_add_timbrado').prop('disabled', true);
                        // $('#boton_update_timbrado').prop('disabled', false);
                    }
                }
            }
        });
    } else {
        // $('#compra-button-add_id')[0].style.display = 'none';
        // $('#compra-timbrado').val('');
    }
}

// Metodo prototipo personal
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
};

// Format nro factura
$(document).on('keydown', "#compra-nro_factura", function (event) {
    var text = $(this).val().replace(/_/g, "").replace(/-/g, ''); // borrar los underscores a la derecha
    var key = event.which;
    var esTeclaBorrar = key === 8;
    if (key > 57) key -= 96; else key -= 48; // convertir a un nro
    var lastgroupstart = 6; // index donde comienza el ultimo grupo de nro
    var lastgroup = text.substring(lastgroupstart, text.length); // el ultimo grupo de nro

    if (text.length > 5 && !esTeclaBorrar && (key > -1 && key < 10)) {
        if (lastgroup.length === 0) { // si es el 1er nro tecleado
            text = text.replaceAt(lastgroupstart, Array(7).join("0") + key);
        } else {
            var i, p = -1;
            // localizar donde comienza un nro <> 0
            for (i = 0; i < lastgroup.length; i++) {
                if (lastgroup[i] !== '0') {
                    p = i;
                    break;
                }
            }
            // si previamente no hubo ningún número, hacer un espacio forzadamente.
            if (lastgroup === '0000000') lastgroup = '000000';
            // extraer solamente los nros y concatenar el nro tecleado
            lastgroup = lastgroup.substring(p, lastgroup.length) + key; // si p > lastgroup.length, significa que no hubo ningún nro antes de keydown y no extraerá nada el substring.
            // agregar el ultimo nro tecleado, rellenando debidamente con ceros a la izquierda.
            text = text.replaceAt(lastgroupstart, Array((7 - lastgroup.length) + 1).join("0") + lastgroup)
        }
    }
    //if (parseInt(text.substring(lastgroupstart, text.length)) === 0){
    //   text = text.replaceAt(lastgroupstart, Array(7).join("0")+1);
    // }
    $(this).val(text);
});

// Selecciona automaticamente timbrado si corresponde al nro de nota.
$(document).on('focusout', '#compra-nro_factura', function() {
    id_to_focus = "compra-fecha_emision";
    cargarTimbrado();
}).on('change', '#compra-fecha_emision', function() {
    id_to_focus = "";
    // if (esCredito()) { // no se por que estuvo esta condicion. pero se deja por si acaso pero comentado
        cargarTimbrado();
    // }
    setTotalFactura();
});

$(document).on('change', '#compra-ruc', function () {
    $.ajax({
        url: $url_getRazonSocialByRuc,
        type: 'post',
        data: {
            ruc: $(this).val(),
        },
        success: function (data) {
            $('#compra-razon_social').val(data).trigger('change');
        }
    });
    rellenarFacturasByEntidad();
});

function plantillaManualPermitido() {
    return ($('#compra-factura_compra_id').val() === '' && $('#iva-cta-usada-new-button').prop('disabled') === false);
}

$(document).on('change', '#compra-obligacion_id', function() {
    if ($('#compra-factura_compra_id').val() === '') {
        $('.delete-plantilla').click();
        $('#iva-cta-usada-new-button').click();
    }
});

$(document).on('click', '#iva-cta-usada-new-button', function(evt) {
    if(plantillaManualPermitido()) {
        let tablePlantillas = $('#plantillas-table');
        tablePlantillas.find('tbody')
            .append('<tr>' + ($bluePrint).replace(/__id__/g, 'new' + plantillaOrden) + '</tr>');
        
        let inputPlantillaId = $('#compraivacuentausada-new' + plantillaOrden + '-plantilla_id');
        if ($('#compra-factura_compra_id').val() === "")
            $('#compraivacuentausada-new' + plantillaOrden + '-plantilla_id[type="hidden"]').parent().remove();
        $.ajax({
            url: '$url_getPlantillas',
            type: 'get',
            data: {
                tipo: 'compra',
                obligacion_id: $('#compra-obligacion_id').val(),
            },
            success: function(data) {
                inputPlantillaId.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: data,
                    disabled: false,
                });
                plantillaOrden++;
            }
        });
    }
});

$(document).on('change', 'input[id$="plantilla_id"]:not([disabled]):not([type="hidden"])', function() {
    if (plantillaManualPermitido()) {
        let prefijo = "#" + $(this).attr('id').replace('plantilla_id', ''),
            plantilla_select_id = $(this).val();
        
        //Deshabilitar todos primero
        $('input[id*="'+prefijo.replace('#', '')+'ivas-iva_"]').prop('disabled', true).prop('readonly', true);
        
        for (const iva of ivasPorPlantilla[plantilla_select_id]) {
            let element = $(prefijo+"ivas-iva_"+iva);
            
            //Habilitar selectivamente segun iva
            element.prop('disabled', false).prop('readonly', false);
            
            //Aplicar mascara
            construirMaskedInput(element, ($('#compra-moneda_id').val() !== '1' ? 'decimal' : 'integer'));  //como no depende de factura, se mira la moneda de la nota.

            //Mostrar boton para remover plantilla
            $(prefijo+'delete_plantilla').css('display', '');
        }
    }
});

$(document).on('click', 'button.delete-plantilla', function () {
    if (plantillaManualPermitido())
        $(this).closest('tbody tr').remove();
});

$(document).on('change', '#compra-tipo_documento_id', function(evt) {
    setTotalFactura();
});

$(document).on('change', '#compra-factura_compra_id', function () {
    mutex = 'unlocked';
    fromDocReady = false;
    
    let select2Facturacompraid = $(this);
    let datosFactura = $(this).select2('data')[0];
    
    // Validar campo de obligacion dependiendo de la factura asociada.
    if ($(this).val() !== '') $("#compra-obligacion_id").val('').trigger('change');
    $("#factura_form").yiiActiveForm('validateAttribute', 'compra-obligacion_id');
    
    // Activar o no boton para mas plantillas
    $('#iva-cta-usada-new-button').prop('disabled', $(this).val() !== '');

    // Establecer fecha de emision de la factura.
    // Debe estar primero que el change de moneda_id para que el mismo pueda usar la fecha de emision de la factura.
    $('#compra-emision_factura').val(datosFactura['fecha_emision']);
    if ("{$action}" === 'update') {
        if ($(this).val() === "") {
            $('#compra-emision_factura').val(fecha_emision_factura_asoc).trigger('change');
            $('#compra-obligacion_id').val(obligacion_id_original).trigger('change');
            $('#compra-moneda_id').val(moneda_id_factura_asoc).trigger('change');
        }
    }

    // Cambiar moneda de la nota.
    $('#compra-moneda_id').val(datosFactura['moneda_id']).trigger('change');
    if ("{$action}" === 'update') {
        if ($('#compra-moneda_id').val() === "")
            $('#compra-moneda_id').val(moneda_id_factura_asoc).trigger('change');
    }
    $('#compra-cotizacion_factura_asociada-disp').val(datosFactura['cotizacion']).trigger('change');

    // Renderizar plantillas.
    // Rellenar montos de ivas.
    $.ajax({
        url: $url_datosFacturaForNota,
        type: 'get',
        data: {
            factura_id: select2Facturacompraid.val(),
            action: "{$action}",
            nota_id: "{$model->id}",
            notaTipo: $('#compra-tipo').select2('data')[0]['id'],
        },
        success: function (ivasPorPlantilla) {
            // if (ivasPorPlantilla.length === 0)
            //     $('#iva-cta-usada-new-button').prop('disabled', false);
            // else
            //     $('#iva-cta-usada-new-button').prop('disabled', true);
            
            // let tablePlantillas = $('#iva-cta-usadas');
            let tablePlantillas = $('#plantillas-table');
            tablePlantillas.find('tbody tr:visible').remove();

            var totalValorNota = 0.0;
            var _tieneDecimales = datosFactura['tiene_decimales'];
            // agrega una fila html por cada iva cuenta.
            $.each(ivasPorPlantilla, function (index, element) {
                // agrega fila
                tablePlantillas.find('tbody').append('<tr>' + (($bluePrint).replace(/__id__/g, 'new' + plantillaOrden) + '</tr>'));

                // aplicar widget Select2
                let plantillas = $plantillas;
                let inputPlantillaId = $('#compraivacuentausada-new' + plantillaOrden + '-plantilla_id');
                inputPlantillaId.select2({
                    theme: 'krajee',
                    placeholder: '',
                    language: 'en',
                    width: '100%',
                    data: plantillas,
                    disabled: $('#compra-factura_compra_id').val() !== '',
                }).val(index).trigger('change');
                $('#compraivacuentausada-new' + plantillaOrden + '-plantilla_id[type="hidden"]').val(index);
                // console.log($('#compraivacuentausada-new' + k + '-plantilla_id[type="hidden"]').val());

                // rellena los campos de iva.
                $.each(element, function (ivaPorcentaje, monto) {
                    totalValorNota += parseFloat(monto);
                    let _jQselector = '#compraivacuentausada-new' + plantillaOrden + '-ivas-iva_' + ivaPorcentaje;
                    $('#compraivacuentausada-new' + plantillaOrden + '-ivas-iva_' + ivaPorcentaje).val(monto).prop('disabled', false).prop('readonly', false);
                    construirMaskedInput(_jQselector, (_tieneDecimales === 'si' ? 'decimal' : 'integer'));
                });

                // activar botones necesarios como: activo fijo, activo biologico....
                activateButtonsForModals(inputPlantillaId);

                plantillaOrden++;
            });

            // completa el campo del total de factura, que en este caso es el monto total de nota
            _jQselector = '#compra-total-disp';
            $(_jQselector).val(totalValorNota);
            construirMaskedInput(_jQselector, (_tieneDecimales === 'si' ? 'decimal' : 'integer'));
            delete _jQselector;

            setTotalFactura();
        }
    });
});

$(document).on('change', '#compra-moneda_id', function() {
    setCotizaciones();
});

$(document).on('change', 'input.agregarNumber', function() {
    setTotalFactura();
});

$(document).on('change', '#compra-cotizacion', function() {
    if (cotizacChangedManually) {
        cotizacChangedManually = false;
        setTotalFactura();
    }
}).on('keyup', function() {
    cotizacChangedManually = true;
});

$('#factura_form').on('beforeSubmit', function () {
    $('.select2-hidden-accessible').prop('disabled', false);
});

$(document).ready(function () {
    fromDocReady = true;
    
    var a = $("#compra-timbrado_id").parent().parent()[0];
    if ("{$action}" === 'update')
        $('<div id = "compra-button-update_id" style="display: block; float:left;" class="col-sm-1"><div class="form-group field-compra-button-add" style="text-align:center;">$boton_update<div class="help-block"></div></div></div>').insertAfter(a);
    $('<div id = "compra-button-add_id" style="display: block; float:left;" class="col-sm-1"><div class="form-group field-compra-button-add" style="text-align:center;">$boton<div class="help-block"></div></div></div>').insertAfter(a);
    
    let select2FacturaCompra = $('#compra-factura_compra_id');
    if (select2FacturaCompra.val() !== '' && $('table[id="plantillas-table"] > tbody > tr:not([id="fila-template"])').length === 0) {
        rellenarFacturasByEntidad();
    }
    
    popOverContent = 'Si no aparece ninguna factura a elegir, revise cada una de las facturas asociadas a la entidad elegida' +
        ' y asegúrese de que cada una de las mismas tengan asociadas al menos una plantilla.';
    applyPopOver($('span.factura'), 'Atención', popOverContent, {'placement': 'bottom', 'title-css': {'background-color' : "#{$infoColor}", 'color': 'white', 'font-weight': 'bold', 'text-align': 'center'}});
    
    // Popover para timbrado
    popOverContent = '<u>Si no aparece ningún prefijo a elegir, verifique lo siguiente</u>:<br/><br/>' +
        '<ol>' +
        '<li>Existe al menos un timbrado de la empresa actual con un detalle asociado al tipo de documento set para notas de crédito (o débito).</li>' +
        '<li>Existe en el parámetro de contabilidad, un registro de nombre <strong>`nota_credito_set_id`</strong> ' +
         '(o <strong>`nota_debito_set_id`</strong> según corresponda) asociado correctamente al ID del Tipo de Documento SET correspondiente.</li>' +
        '<li>Si existe ya un timbrado según 1 y 2:</li>' + 
        '<ol>' +
         '<li>Verificar que sus fechas de inicio y fin de vigencia estén correctamente establecidas.</li>' +
         '<li>Verificar que la fecha de emisión de esta nota esté comprendido en el periodo de vigencia del timbrado.</li>' +
         '<li>Verificar que el periodo actual configurado corresponda con el periodo de vigencia del timbrado.</li>' +
         '</ol>'+
        '</ol>';
    let template = ['<div class="popover timbrado-popover" role="tooltip">',
        '<div class="arrow"></div>',
        '<h3 class="popover-title"></h3>',
        '<div class="popover-content"></div>',
        '</div>',
        '</div>'].join('');
    applyPopOver($('span.timbrado'), 'Atención', popOverContent, {'theme-color': 'info-l', 'placement': 'right', 'template': template});

    delete popOverContent; 
});
JS;
        $isNewRecord = $model->isNewRecord;// Se espera que jamas lance excepcion porque si es propenso a tal, ya se captura en el test de isNotaCreable() del controller.
        $porcentajes_iva = [];
        $iva_cuentas = IvaCuenta::find()->all();
        /** @var IvaCuenta $iva_cuenta */
        foreach ($iva_cuentas as $iva_cuenta)
            $porcentajes_iva[] = $iva_cuenta->iva->porcentaje;
        asort($porcentajes_iva);
        $porcentajes_iva = array_values($porcentajes_iva);
        $porcentajes_iva = Json::encode($porcentajes_iva);
        $urlCalcDetalle = Json::htmlEncode(\Yii::t('app', Url::to(['compra-nota/calcular-detalle'])));
        if (Yii::$app->controller->action->id == 'update') {
            $urlCalcDetalle = Json::htmlEncode(\Yii::t('app', Url::to(['compra-nota/calcular-detalle-update'])));
        }
        $es_nota = in_array($model->tipo, ['nota_credito', 'nota_debito']) ? $model->tipo : false;
        $ivasPorPlantilla = [];
        foreach (Json::decode($plantillas) as $plantilla) {
            foreach (PlantillaCompraventa::findOne(['id' => $plantilla['id']])->plantillaCompraventaDetalles as $plDetalle) {
                $ivasPorPlantilla[$plantilla['id']][] = (isset($plDetalle->ivaCta) ? $plDetalle->ivaCta->iva->porcentaje : 0);
            }
        }
        $ivasPorPlantilla = Json::encode($ivasPorPlantilla);
        $js_script_head = <<<JS
function manejarUrlRuc() {  return "$entidades_url" + "&tipo=" + $('#compra-tipo').select2('data')[0]['registro_tipo']; }
function manejarSeleccionTipo() {
    $("#compra-tipo_documento_id").val('').trigger('change');
    $("#compra-ruc").val('').trigger('change');
    let compra_tipo = $('#compra-tipo').select2('data')[0];
    esNota = compra_tipo['id'];
    let tipo = compra_tipo['registro_tipo'];
    if (tipo.indexOf('_') > 0)
        tipo = tipo.substring(0, tipo.indexOf('_'));
    tipo = tipo.charAt(0).toUpperCase() + tipo.slice(1);
    $('#compra-factura_compra_id').prev('label').html(tipo);
    $('#compra-emision_factura').prev('label').html('Emisión ' + tipo);
    $('#select2-compra-factura_compra_id-container span.select2-selection__placeholder').html('Seleccione una ' + tipo + '...');
}
function getPrefijoId(input) {
    let input_id = input.attr('id');
    let input_id_slices = input_id.split('-');
    input_id_slices.splice(-1, 1);
    return input_id_slices.join('-') + '-';
}
function getTotalFieldNames() {
    const prefijo = "totales_iva-iva-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}
function calcularIvasYRellenar() {
    let ctotales = getTotalFieldNames(), cimpuestos = getImpuestosFieldNames(), ivas = getIvas();
    let totales_iva = 0.0, i;
    let select2Moneda = $('#compra-moneda_id');
    for (i = 0; i < ctotales.length; i++) {
        let iva = ivas[i];        
        if (iva !== '0') {
            let ctotal = document.getElementById(ctotales[i] + '-disp');
            let cgrav = document.getElementById(cimpuestos[i] + '-disp');
            if (ctotal !== null && cgrav !== null) {
                cgrav.value = ctotal.value - (ctotal.value / (1.0 + iva / 100.0));
                if (select2Moneda.val() === '1') {
                    cgrav.value = Math.round(cgrav.value);
                }
                totales_iva += cgrav.value;
            }
        }
    }
    
    // Total exenta
    let total = 0;
    $('input[id$="ivas-iva_0"]').each(function(i, e) {
        total += $(this).val();
    });
    $('#totales_iva-iva-0-disp').val(total).trigger('change');
    
    // Total 5
    let impuesto = 0;
    total = 0;
    $('input[id$="ivas-iva_5"]').each(function(i, e) {
        total += $(this).val();
    });
    $('#totales_iva-iva-5-disp').val(total).trigger('change');
    // Iva 5
    $('#totales_iva-impuesto-5-disp').val(total / 21);
    impuesto += total / 21;
    
    // Total 10
    total = 0;
    $('input[id$="ivas-iva_10"]').each(function(i, e) {
        total += $(this).val();
    });
    $('#totales_iva-iva-10-disp').val(total).trigger('change');
    // Iva 10
    $('#totales_iva-impuesto-10-disp').val(total / 11).trigger('change');
    impuesto += total / 11;
    
    // Total de impuesto
    $('#compra-_total_ivas-disp').val((select2Moneda.val() === '1') ? Math.round(impuesto) : impuesto).trigger('change');
}

//Retorna array con datos de la tabla de plantillas - ivas
function getDataTable() {
    var data_table = []; 
    // $('input[id$="plantilla_id"]:disabled:not([id*="__id__"])').each(function(index, element) {
    
    //Se remueve el selector :disabled porque ahora se puede permitir agregar plantillas segun sea el caso.
    $('input[id$="plantilla_id"]:not([id*="__id__"]):not([type="hidden"])').each(function(index, element) {
        let prefijo = getPrefijoId($(this));
        var fila = {};
        $('input[id^="' + prefijo + '"][clave^="ivaMonto"]').each(function(index, element) {
            let regex = /ivas-iva_([0-9]+)/gm;
            let matches = regex.exec($(this).prop('id'));
            
            fila['iva_' + matches[1]] = $(this).val(); // this == campo ivaMontoN
        });
        fila["plantilla_id"] = $(this).val(); // this == input select2 plantila
        data_table.push(fila);
    });
    return data_table;
}
function getIvas() {
    return Object.values(JSON.parse("$porcentajes_iva"))
}
function getTotalFieldValues() {
    var total_nombres = getTotalFieldNames();
    var i, total_valores = [];
    for (i = 0; i < total_nombres.length; i++) {
        total_valores.push($('#' + total_nombres[i] + '-disp').val());
    }
    return total_valores;
}
function getImpuestosFieldNames() {
    const prefijo = "discriminados-impuesto-";
    var porcentajes = JSON.parse("$porcentajes_iva");
    var arr = Object.values(porcentajes);
    var i, nombres = [];
    for (i = 0; i < arr.length; i++) {
        nombres.push(prefijo.concat(arr[i]));
    }
    return nombres;
}
function checkNeededFields() {
    let ok = true;
    // let ids = ['#compra-fecha_emision', '#compra-total-disp'];
    let ids = ['#compra-fecha_emision', '#compra-moneda_id'];
    // more fields's id his value must be checked should be added to the 'ids' array.
    ids.forEach(function(element, index) {
        if ($(element).val() === '') {
            let label = $('label[for="' + $(element).prop('id').replace('-disp', '') + '"]');
            // $('html, body').stop().animate({
            //     scrollTop: label.offset().top
            // }, 500);
            ok = ok && false;
            // krajeeDialog.alert(label.html() + ' no debe estar vacio.');
        }
    });
    
    if (ok) {
        //detalles_grid
        // $('html, body').stop().animate({
        //     scrollTop: $('label[for="compra-fecha_emision"]').offset().top
        // }, 500);
    }
    return ok;
}
function getTipodocumentoId() {
    return $('#compra-tipo_documento_id').val(); //(esCredito() ? "{$tipodocNotaCreditoId}" : "{$tipodocNotaDebitoId}");
}
function esCredito() {
    return ($('#compra-tipo').select2('data')[0]['id'] === "nota_credito");
}
function esDebito() {
    return ($('#compra-tipo').select2('data')[0]['id'] === "nota_debito");
}
function setTotalFactura() {
    if (fromDocReady) {
        console.log('settotalfactura return by fromDocReady');
        if (!rendering_plantillas_byupdate) fromDocReady = false ;
        return;
    }
    
    if (mutex === 'locked') {
        console.log('settotalfactura return by mutex');
        return;
    }
    mutex = 'locked';
    
    console.log('setTotalFactura()');
    let montoTotalNota = 0.0;
    var total_disp_field = $('#compra-total-disp');
    let value;
    $('input.agregarNumber:enabled').each(function(index, element) {
        value = (element.value === '' ? 0 : parseFloat(element.value));
        montoTotalNota += value;
    });
    total_disp_field.val(montoTotalNota).trigger('change');
    
    console.log(montoTotalNota);
    
    if (checkNeededFields()) {
        var url = $urlCalcDetalle;

        var moneda_id_field = $('#compra-moneda_id');
        var cotizacion_disp_field = $('#compra-cotizacion-disp');
        var tipo_doc_id = getTipodocumentoId();
        if (tipo_doc_id !== '') {
            // inhabilitar temporalmente boton submit
            let submit_button = $('#btn_submit_nota');
            let submit_cerrar_button = $('#btn_submit_factura_compra_guardar_cerrar');
            
            submit_button.prop('disabled', true);
            submit_button.html('Generando detalles y simulacion ... ');
            
            submit_cerrar_button.prop('disabled', true);
            submit_cerrar_button.html('Generando detalles y simulacion ... ');
        
            // calcular ivas y mostrar en los campos disableds de ivas
            calcularIvasYRellenar();
            $.ajax({
                url: url,
                type: 'post',
                data: {
                    moneda_id: moneda_id_field.val(),
                    cotizacion: cotizacion_disp_field.val(),
                    cont_factura_total: montoTotalNota,
                    plantillas: getDataTable(),
                    tipo_docu_id: tipo_doc_id,
                    ivas: getIvas(),
                    totales_valor: getTotalFieldValues(),
                    estado_comprobante: 'vigente',
                    es_nota: $('#compra-tipo').select2('data')[0]['id'],
                    factura_id: $('#compra-factura_compra_id').val(),
                    nota_id: "{$model->id}"
                },
                success: function (data) {
                    if (data !== '') { // si retorna falso, aquí se vé como ''. si retorna true, aquí se vé como 1.
                        $.pjax.reload({container: "#detalles_grid", async: false});
                    } else {
                        console.log('no retorno nada, no se realiza simulacion');
                        $.pjax.reload({container: "#flash_message_id", async: false});
                    }
                    mutex = 'unlocked';
                }
            });
        } else {
            console.log('tipo_doc_id es vacio, no se realiza simulacion');
        }
    } else {
        console.log('falta campos a completar, no se realiza simulacion');
    }
    mutex = 'unlocked';
}
var facturacompraIdInicial = "{$model->factura_compra_id}";
var plantillasForSelect2 = $plantillas;  // usado desde _form_plantillas.php
var esNota = "$es_nota";
var cotizacChangedManually = false;
var fromDocReady = false;
var plantillaOrden = 1;
var ivasPorPlantilla = $ivasPorPlantilla;
var mutex = 'unlocked';
var mutex_simular = 'unlocked';
var rendering_plantillas_byupdate = false;
const moneda_id_factura_asoc = "{$model->moneda_id}";
const obligacion_id_original = "{$model->obligacion_id}";

JS;
        if ($action == 'update' && $model->factura_compra_id != '') {
            $fecha_emision_factura_asoc = implode('-', array_reverse(explode('-', $model->facturaCompra->fecha_emision)));
            $js_script_head .= <<<JS
const fecha_emision_factura_asoc = "{$fecha_emision_factura_asoc}";
JS;

        } else {
            $js_script_head .= <<<JS
const fecha_emision_factura_asoc = "";
JS;

        }
        $this->registerJs($js_script_head, View::POS_HEAD);
        $this->registerJs($js_script_ready);

        $css = <<<CSS
.popover.timbrado-popover {
    max-width: none;
    width: 450px;
}
CSS;
        $this->registerCss($css);

    } catch (\Exception $e) {
        echo $e;
        FlashMessageHelpsers::createWarningMessage($e->getMessage());
    }
    ?>
</div>
