<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 21/12/18
 * Time: 08:13 AM
 */

use backend\modules\contabilidad\models\Compra;
use common\helpers\FlashMessageHelpsers;
use common\helpers\ValorHelpers;
use kartik\detail\DetailView;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Compra */
?>

<?php try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'toolbar' => [],
        'hover' => true,
        'panel' => [
            'type' => 'info',
            'heading' => $heading,
            'footerOptions' => ['class' => ''],
            'beforeOptions' => ['class' => ''],
            'afterOptions' => ['class' => '']
        ],
        'panelFooterTemplate' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'ID',
                'headerOptions' => [
                    'width' => '5%'
                ],
                'value' => 'id',
                'attribute' => 'id',
            ],
            [
                'label' => 'Nro Comprobante',
                'attribute' => 'nro_factura',
                'format' => 'raw',
                'value' => function ($model) {
                    $url = Url::to(['/contabilidad/compra-nota/view', 'id' => $model->id]);
                    return Html::a($model->nro_factura, $url, []);
                },
            ],
            [ // the attribute
                'attribute' => 'fecha_emision',
                // format the value
                'format' => ['date', 'php:d-m-Y'],
                'headerOptions' => ['class' => 'col-md-2'],
            ],
            [
                'label' => 'Proveedor',
                'attribute' => 'nombre_entidad',
                'value' => 'entidad.razon_social',
            ],
            [
                'value' => function ($model) {
                    /** @var Compra $model */
                    return $model->getTotal();
                },
                'attribute' => 'total',
                'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
            ],
            [
                'value' => function ($model) {
                    /** @var Compra $model */
                    return $model->getSaldo();
                },
                'attribute' => 'saldo',
                'contentOptions' => ['style' => 'padding:8px 6px 0px 0px; text-align:right'],
            ],
            [
                'label' => 'Tipo',
                'attribute' => 'nombre_documento',
                'value' => 'tipoDocumento.nombre'
            ],
            [
                'attribute' => 'timbrado',
                'value' => 'timbradoDetalle.timbrado.nro_timbrado'
            ],
            [
                'attribute' => 'moneda_id',
                'value' => 'moneda.nombre',
            ],
            [
                'attribute' => 'condicion',
                'value' => function ($model) {
                    /** @var Compra $model */
                    return ucfirst($model->condicion);
                },
                'label' => 'Condición',
            ],
            [
                'attribute' => 'estado',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<label class="label label-' . (($model->estado == 'vigente') ? 'success">Vigente' : (($model->estado == 'faltante') ? 'warning">Faltante' : 'danger">Anulada')) . '</label>';
                },
                'contentOptions' => ['style' => 'text-align:center;']
            ],
        ],
    ]);
} catch (Exception $exception) {
    echo $exception;
    FlashMessageHelpsers::createWarningMessage($exception->getMessage());
}
