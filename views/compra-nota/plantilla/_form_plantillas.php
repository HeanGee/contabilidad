<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 24/07/2018
 * Time: 8:32
 */

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use backend\modules\contabilidad\models\IvaCuenta;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

/* @var $form yii\widgets\ActiveForm */
/* @var $model Compra */
/* @var IvaCuenta[] $iva_ctas */
/* @var $this View */

$iva_ctas = IvaCuenta::find()->all();
foreach ($iva_ctas as $iva_cuenta)
    $porcentajes_iva[] = $iva_cuenta->iva->porcentaje;
asort($porcentajes_iva);
$porcentajes_iva = array_values($porcentajes_iva);
$porcentajes_iva_json = Json::encode($porcentajes_iva);

$ivaCtaUsada = new CompraIvaCuentaUsada();
$ivaCtaUsada->loadDefaultValues();

echo Html::beginTag('fieldset');

echo Html::beginTag('leyend', []);
echo Html::button('<i class="glyphicon glyphicon-plus"></i>', [
    'id' => 'iva-cta-usada-new-button',
    'class' => 'pull-left btn btn-success',
    'disabled' => ($model->factura_compra_id == '' ? false : true),
]);
echo Html::endTag('leyend');

echo Html::beginTag('table', ['class' => 'table table-condensed table-bordered', 'id' => 'plantillas-table']);
echo Html::beginTag('thead');
echo Html::beginTag('tr', ['class' => 'title-row']);
echo Html::tag('th', 'Plantilla', ['style' => 'text-align: center; ']);
foreach ($iva_ctas as $iva_cta) {
    if ($iva_cta->iva->porcentaje == 0)
        echo Html::tag('th', 'Exenta', ['class' => '', 'style' => "text-align: center; "]);
    else
        echo Html::tag('th', 'I.V.A. ' . $iva_cta->iva->porcentaje . '%', ['class' => '', 'style' => "text-align: center; "]);
}
//if (!$es_nota) {
echo Html::tag('th', 'Acciones', ['style' => "text-align: center; "]);
echo Html::endTag('tr');
echo Html::endTag('thead');
echo Html::beginTag('tbody');
{
//    if (Yii::$app->controller->action->id == 'update' && $model->factura_compra_id == '') {
    if (Yii::$app->controller->action->id == 'update') {
        $plantillaOrden = 1;

        #registrar primero evento onChange del campo de los ivas
        $this->registerJs("$(document).on('change', 'input.agregarNumber', function() { setTotalFactura(); });");
        $this->registerJs('rendering_plantillas_byupdate = true; fromDocReady = true;');
        $this->registerJs("const id_factura_asociada = '{$model->factura_compra_id}';");

        #renderizar fila de plantillas
        foreach ($model->_iva_ctas_usadas as $plantilla_id => $ivaCuenta) {
            #renderizar una fila de plantilla
            echo $this->render('_form_plantillas_filas', [
                'key' => "new{$plantillaOrden}",
                'form' => $form,
                'ivaCtaUsada' => $ivaCtaUsada,
                'porcentajesIva' => $porcentajes_iva,
            ]);

            #construir select2 y poner valor de plantilla_id
            $js = <<<JS
$("input[id$='new{$plantillaOrden}-plantilla_id']:not([type='hidden'])").select2({
    theme: 'krajee',
    placeholder: '',
    language: 'en',
    width: '100%',
    data: plantillasForSelect2,  //variable registrado en POS_HEAD desde _form.php
    disabled: false,
}).val('$plantilla_id').trigger('change').prop('disabled', id_factura_asociada.length > 0).prop('readonly', id_factura_asociada.length > 0);
JS;
            $this->registerJs($js);

            #poner valor de ivas
            $tieneDecimales = ($model->moneda_id != '' && $model->moneda_id != \common\models\ParametroSistema::getMonedaBaseId()) ? 'decimal' : 'integer';
            foreach ($ivaCuenta as $iva_porcentaje => $monto) {
                $js = <<<JS
// quitar readonly, disable, mostrar boton para remover plantilla y poner valor de iva
$("input[id$='new{$plantillaOrden}-ivas-iva_{$iva_porcentaje}']:not([type='hidden'])")
    .prop('disabled', false).prop('readonly', false)
    .val('{$monto}').trigger('change')
    .parent().parent().parent().find("button[id$='delete_plantilla']").css('display', '');

// remover el hidden, ya que se va a poder modificar plantilla y demas cosas.
$("input[id$='new{$plantillaOrden}-plantilla_id'][type='hidden']").parent().remove();

//masked input
construirMaskedInput("input[id$='new{$plantillaOrden}-ivas-iva_{$iva_porcentaje}']:not([type='hidden'])", "{$tieneDecimales}");

// no se habilitan botones para activos fijos/biologicos porque no se conoce la factura, asi que no hay necesidad.
JS;
                $this->registerJs($js);
            }
            $plantillaOrden++;
        }
        #actualizar variable js `plantillaOrden`
        $this->registerJs("plantillaOrden = {$plantillaOrden}");
    }
    $this->registerJs('rendering_plantillas_byupdate = false;');
}
echo Html::endTag('tbody');
echo Html::endTag('table');

echo Html::endTag('fieldset');

$html = $this->render('_form_plantillas_filas', [
    'key' => '__id__',
    'form' => $form,
    'ivaCtaUsada' => $ivaCtaUsada,
    'porcentajesIva' => $porcentajes_iva,
]);

// remover todos los \n \r y combinacion de ellos segun https://stackoverflow.com/questions/5258543/remove-all-the-line-breaks-from-the-html-source
$output = str_replace(array("\r\n", "\r"), "\n", $html);
$lines = explode("\n", $output);
$substrings = array();

foreach ($lines as $i => $line) {
    if (!empty($line))
        $substrings[] = trim($line);
}
$html_without_enter_and_spaces = implode($substrings);
$bluePrint = Json::encode($html_without_enter_and_spaces);
Yii::$app->session->set('$bluePrint', $bluePrint);
?>