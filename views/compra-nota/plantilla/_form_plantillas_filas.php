<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 24/07/2018
 * Time: 10:14
 */

use backend\modules\contabilidad\models\Compra;
use backend\modules\contabilidad\models\CompraIvaCuentaUsada;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */
/* @var $model Compra */
/* @var $key string */
/* @var $ivaCtaUsada CompraIvaCuentaUsada */
/* @var $porcentajesIva array */
?>

<?php

if (Yii::$app->controller->id == 'compra')
    echo Html::tag('td', $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->textInput([])->label(false),
        ['id' => "plantillas-selector-field-{$key}", 'class' => 'plantilla-selector', "style" => "width: 25%;"]
    );
else {
    echo "<td id='plantillas-selector-field-{$key}' class='plantilla-selector' style='width: 25%;'>";
    echo $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->textInput([])->label(false);
    echo $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->hiddenInput([])->label(false);
    echo '</td>';
}

foreach ($porcentajesIva as $pi) {
    echo Html::tag('td', $form->field($ivaCtaUsada, "[{$key}]ivas[iva_{$pi}]")->textInput([
        "class" => "form-control agregarNumber",
        "clave" => "ivaMonto{$pi}",
        "readonly" => true,
        "disabled" => true,
    ])->label(false), ['class' => "column-iva-{$pi}", 'style' => "text-align: right; "]);
}

?>
<td style="text-align: center;">
    <?= Html::button('<span class="fa fa-cubes" aria-hidden="true"></span>', [
        'class' => 'actfijo-manager btn btn-success modal-actfijo',
        'title' => 'Activo Fijo',
        'id' => "compraivacuentausada-{$key}-actfijo_manager_button",
        'style' => "display: none;",
        'type' => 'button',
        'data-toggle' => 'modal',
        'data-target' => '#modal',
        'data-url' => Url::to(['activo-fijo/manejar-desde-nota-credito-compra']),
        'data-pjax' => '0',
    ]) ?>
    <?= Html::button('<span class="fa fa-paw" aria-hidden="true"></span>', [
        'class' => 'act-bio-compra-nota-manager btn btn-success modal-act-bio-compra-nota-manager',
        'title' => 'Devolución de Activos Biológicos',
        'id' => "compraivacuentausada-{$key}-actbio_compra_nota_manager_btn",
        'style' => "display: none;",
        'type' => 'button',
        'data-toggle' => 'modal',
        'data-target' => '#modal',
        'data-url' => Url::to(['activo-biologico/devolver-act-bio']),
        'data-pjax' => '0',
    ]) ?>
    <?= Html::button('<span class="fa fa-trash" aria-hidden="true"></span>', [
        'id' => "compraivacuentausada-{$key}-delete_plantilla",
        'class' => 'btn btn-danger delete-plantilla',
        'title' => 'Remover esta plantilla',
        'style' => "display: none;",
        'type' => 'button',
    ]) ?>
</td>
