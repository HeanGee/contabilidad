<?php

use yii\helpers\Html;
use yii\helpers\Url;

$es_nota = !empty($es_nota);
?>
<?php if (Yii::$app->controller->id == 'compra') : ?>
    <td style="width: 25%;">
        <?= $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->textInput([
            'class' => 'form-control plantilla',
        ])->label(false) ?>
    </td>
<?php else :
    echo "<td style='width: 25%;'>";
    echo $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->textInput([])->label(false);
    echo $form->field($ivaCtaUsada, "[{$key}]plantilla_id")->hiddenInput([])->label(false);
    echo '</td>';
endif; ?>


<?php
/** @var array $porcentajes_iva */
foreach ($porcentajes_iva as $ic) {
    echo '
        <td>
            ' . $form->field($ivaCtaUsada, "[{$key}]ivas[iva_" . $ic . "]")->textInput([
            "class" => "form-control agregarNumber",
            "clave" => "ivaMonto{$ic}",
            "disabled" => true,
        ])->label(false)
        . '</td>
    ';
}
?>

    <td style="text-align: center;">
        <?= Html::button('<span class="fa fa-cubes" aria-hidden="true"></span>', [
            'class' => 'actfijo-manager btn btn-success modal-actfijo',
            'title' => 'Activo Fijo',
            'id' => "compraivacuentausada-{$key}-actfijo_manager_button",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['activo-fijo/manejar-desde-nota-credito-compra']),
            'data-pjax' => '0',
        ]) ?>
        <?= Html::button('<span class="fa fa-paw" aria-hidden="true"></span>', [
            'class' => 'act-bio-compra-manager btn btn-success modal-act-bio-compra-manager',
            'title' => 'Compra de A. Biológicos',
            'id' => "compraivacuentausada-{$key}-actbio_compra_manager_btn",
            'style' => "display: none;",
            'type' => 'button',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-url' => Url::to(['activo-biologico/comprar-act-bio']),
            'data-pjax' => '0',
        ]) ?>
    </td>
<?php
//Solo en caso de update se recargan todos los campos y recalcula
if (Yii::$app->controller->action->id == 'update' || $es_nota) {
    $string = <<<JS
    // reloadAllNumberField();
JS;
    $this->registerJS($string);
}
?>