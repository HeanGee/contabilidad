<?php

use backend\modules\contabilidad\controllers\CompraController;
use backend\modules\contabilidad\models\ActivoBiologicoStockManager;
use backend\modules\contabilidad\models\ActivoFijoStockManager;
use backend\modules\contabilidad\models\DetalleCompra;
use common\helpers\ValorHelpers;
use kartik\detail\DetailView;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\Compra */

$mapTxt = ['factura' => 'Factura', 'nota_credito' => 'Nota de Crédito', 'nota_debito' => "Nota de Débito"];

$this->title = "Datos de {$mapTxt[$model->tipo]}";
$this->params['breadcrumbs'][] = ['label' => 'Notas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notas-view">

    <p>
        <?= $model->bloqueado == 'no' && CompraController::getAcceso('update') ? Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= $model->bloqueado == 'no' && CompraController::getAcceso('delete') ? Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de borrar esta Nota?',
                'method' => 'post',
            ],
        ]) : '' ?>
        <?= "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" ?>
        <?= CompraController::getAcceso('bloquear') && $model->bloqueado == 'no' ? Html::a('<span>Bloquear</span>', ['bloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'title' => Yii::t('app', 'Bloquear'),
                'data-confirm' => 'Está seguro que desea bloquear la factura?'
            ]) : '' ?>
        <?= CompraController::getAcceso('desbloquear') && $model->bloqueado == 'si' ? Html::a('<span>Desbloquear</span>', ['desbloquear', 'id' => $model->id],
            [
                'class' => 'btn btn-success',
                'title' => Yii::t('app', 'Desbloquear'),
                'data-confirm' => 'Está seguro que desea desbloquear la factura?'
            ]) : '' ?>
    </p>

    <?php
    try {
        $tipo_str = implode(' ', array_map('ucfirst', explode('_', $model->tipo)));

        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => $tipo_str . ' ' . $model->nro_factura,
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                [
                    'columns' => [
                        [
                            'format' => 'raw',
                            'label' => 'Nro de Nota',
                            'value' => $model->nro_factura,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Total factura',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->total, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Timbrado',
                            'value' => $model->timbradoDetalle->timbrado->nro_timbrado,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Saldo',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->saldo, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],

                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Tipo de Documento',
                            'value' => $model->tipo_documento_id != null ? $model->tipoDocumento->nombre : '',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Exenta',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->exenta, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Fecha de emisión',
                            'value' => date_create_from_format('Y-m-d', $model->fecha_emision) ?
                                date_create_from_format('Y-m-d', $model->fecha_emision)->format('d-m-Y') : "-no definido-",
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Gravada 10%',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->gravada10, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Entidad',
                            'format' => 'raw',
                            'value' => ($model->entidad_id) ? "{$model->entidad->razon_social} <strong>(RUC: {$model->entidad->ruc})</strong>" : '',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Impuesto 10%',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->impuesto10, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Moneda',
                            'value' => ($model->moneda_id) ? $model->moneda->nombre : '',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Gravada 5%',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->gravada5, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Empresa',
                            'value' => $model->empresa->razon_social,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Impuesto 5%',
                            'value' => ValorHelpers::numberFormatMonedaSensitive($model->impuesto5, 2, $model->moneda),
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Estado',
                            'format' => 'raw',
                            'value' => '<label class="label label-' . (($model->estado == 'vigente') ? 'success">Vigente' : (($model->estado == 'faltante') ? 'warning">Faltante' : 'danger">Anulada')) . '</label>',
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                        [
                            'label' => 'Plantillas',
                            'value' => $model->plantillas,
                            'valueColOptions' => ['style' => 'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'label' => 'Timbrado Detalle',
                            'format' => 'raw',
                            'value' => $model->timbrado_detalle_id,
                            'valueColOptions' => ['style' => 'width:100%']
                        ],
                    ],
                ],
            ]
        ]);

        // detalles
        $dataProvider = new ActiveDataProvider([
            'query' => DetalleCompra::find()->where(['factura_compra_id' => $model->id]),
            'pagination' => false
        ]);
        echo GridView::widget([
            'id' => 'grid-detalles',
            'dataProvider' => $dataProvider,
            'panel' => ['type' => 'info', 'heading' => 'Detalles', 'footer' => false,],
            'toolbar' => [],
            'hover' => true,
            'panelFooterTemplate' => '',
            'showPageSummary' => true,
            'pageSummaryPosition' => GridView::POS_BOTTOM,
            'pageSummaryRowOptions' => [
                'class' => 'kv-page-summary warning',
                'style' => 'text-align: right;',
            ],
            'columns' => [
                'id',
                [
                    'class' => DataColumn::className(),
                    'label' => 'Código de cuenta',
                    'value' => 'planCuenta.cod_completo'
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Nombre de cuenta',
                    'value' => 'planCuenta.nombre',
                    'pageSummaryOptions' => [
                        'data-colspan-dir' => 'ltr',
                        'colspan' => 1,
                        'style' => 'text-align: right;',
                    ],
                    'pageSummary' => function ($summary, $data, $widget) {
                        return "Totales";
                    },
                    'contentOptions' => function ($model) {
                        if ($model->cta_contable == 'debe') return ['style' => 'padding:8px 6px 0px 10px; text-align:left'];
                        return ['style' => 'padding:8px 6px 0px 150px; text-align:left'];
                    },
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Debe',
                    'value' => function ($model) {
                        /** @var $model DetalleCompra */
                        if ($model->cta_contable == 'debe') return $model->subtotal;
                        return 0;
                    },
                    'format' => 'decimal',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'append' => '',
                        'prepend' => '',
                    ],
                    'contentOptions' => ['style' => 'width: 12%; text-align:right'],
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Haber',
                    'value' => function ($model) {
                        /** @var $model DetalleCompra */
                        if ($model->cta_contable == 'haber') return $model->subtotal;
                        return 0;
                    },
                    'format' => 'decimal',
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM,
                    'pageSummaryOptions' => [
                        'append' => '',
                        'prepend' => '',
                    ],
                    'contentOptions' => ['style' => 'width: 12%; text-align:right'],
                ],
                [
                    'class' => DataColumn::className(),
                    'label' => 'Cuenta principal ?',
                    'value' => function ($model) {
                        if ($model->cta_principal == 'si')
                            return ucfirst($model->cta_principal);
                        return '';
                    },
                    'contentOptions' => ['style' => 'width: 8%; text-align:center;'],
                    'headerOptions' => ['style' => 'text-align:center;'],
                ],
            ],
        ]);

        $mapUrl = ['factura' => 'compra', 'nota_credito' => 'compra-nota'];
        if (isset($model->facturaCompra))
            echo DetailView::widget([
                'model' => $model->facturaCompra,
                'condensed' => true,
                'hover' => true,
                'mode' => DetailView::MODE_VIEW,
                'enableEditMode' => false,
                'fadeDelay' => true,
                'panel' => [
                    'heading' => "Datos de {$mapTxt[$model->facturaCompra->tipo]} Asociada",
                    'type' => DetailView::TYPE_INFO,
                ],
                'attributes' => [
                    [
                        'columns' => [
                            [
                                'format' => 'raw',
                                'label' => "Nro de {$mapTxt[$model->facturaCompra->tipo]}",
                                'value' => Html::a($model->facturaCompra->nro_factura, Url::to(["/contabilidad/{$mapUrl[$model->facturaCompra->tipo]}/view", 'id' => $model->facturaCompra->id]), []),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'label' => 'Total factura',
                                'value' => ValorHelpers::numberFormatMonedaSensitive($model->facturaCompra->total, 2, $model->facturaCompra->moneda),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'label' => 'Timbrado',
                                'value' => $model->facturaCompra->timbradoDetalle->timbrado->nro_timbrado,
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'label' => 'Saldo',
                                'value' => ValorHelpers::numberFormatMonedaSensitive($model->facturaCompra->saldo, 2, $model->facturaCompra->moneda),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],

                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'label' => 'Tipo de Documento',
                                'value' => $model->facturaCompra->tipo_documento_id != null ? $model->facturaCompra->tipoDocumento->nombre : '',
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'label' => 'Exenta',
                                'value' => ValorHelpers::numberFormatMonedaSensitive($model->facturaCompra->exenta, 2, $model->facturaCompra->moneda),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'label' => 'Fecha de emisión',
                                'value' => date_create_from_format('Y-m-d', $model->facturaCompra->fecha_emision) ?
                                    date_create_from_format('Y-m-d', $model->facturaCompra->fecha_emision)->format('d-m-Y') : "-no definido-",
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'label' => 'Gravada 10%',
                                'value' => ValorHelpers::numberFormatMonedaSensitive($model->facturaCompra->gravada10, 2, $model->facturaCompra->moneda),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'label' => 'Entidad',
                                'format' => 'raw',
                                'value' => ($model->facturaCompra->entidad_id) ? "{$model->facturaCompra->entidad->razon_social} <strong>(RUC: {$model->facturaCompra->entidad->ruc})</strong>" : '',
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'label' => 'Impuesto 10%',
                                'value' => ValorHelpers::numberFormatMonedaSensitive($model->facturaCompra->impuesto10, 2, $model->facturaCompra->moneda),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'label' => 'Moneda',
                                'value' => ($model->facturaCompra->moneda_id) ? $model->facturaCompra->moneda->nombre : '',
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'label' => 'Gravada 5%',
                                'value' => ValorHelpers::numberFormatMonedaSensitive($model->facturaCompra->gravada5, 2, $model->facturaCompra->moneda),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'label' => 'Empresa',
                                'value' => $model->facturaCompra->empresa->razon_social,
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'label' => 'Impuesto 5%',
                                'value' => ValorHelpers::numberFormatMonedaSensitive($model->facturaCompra->impuesto5, 2, $model->facturaCompra->moneda),
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                    [
                        'columns' => [
                            [
                                'label' => 'Estado',
                                'format' => 'raw',
                                'value' => '<label class="label label-' . (($model->facturaCompra->estado == 'vigente') ? 'success">Vigente' : (($model->facturaCompra->estado == 'faltante') ? 'warning">Faltante' : 'danger">Anulada')) . '</label>',
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                            [
                                'label' => 'Plantillas',
                                'value' => $model->facturaCompra->plantillas,
                                'valueColOptions' => ['style' => 'width:30%']
                            ],
                        ],
                    ],
                ]
            ]);

        // Mostrar activos fijos devueltos
        $actfijoManagers = ActivoFijoStockManager::find()->where(['compra_nota_credito_id' => $model->id]);
        if ($actfijoManagers->exists()) {
            $allModels = [];
            /** @var ActivoFijoStockManager $manager */
            foreach ($actfijoManagers->all() as $manager) {
                $allModels[] = $manager->activoFijo;
            }
            echo $this->render('../activo-fijo/views/_view_actfijos', [
                'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $allModels, 'pagination' => false]),
                'heading' => 'Activos Fijos Devueltos'
            ]);
        }

        // Mostrar activos fijos biologicos devueltos
        $actbioManagers = ActivoBiologicoStockManager::find()->where(['factura_compra_id' => $model->id]);
        if ($actbioManagers->exists()) {
            $allModels = [];
            /** @var ActivoBiologicoStockManager $manager */
            foreach ($actbioManagers->all() as $manager) {
                $allModels[] = $manager->activoBiologico;
            }
            echo $this->render('../activo-biologico/views/_view_actbios', [
                'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $allModels, 'pagination' => false]),
                'heading' => 'Activos Biológicos Devueltos'
            ]);
        }

    } catch (\Exception $e) { //throw $e;
        \common\helpers\FlashMessageHelpsers::createWarningMessage($e->getMessage());
        echo $e;
    }

    ?>

</div>
