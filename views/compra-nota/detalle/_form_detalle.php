<?php

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
?>
<?php Pjax::begin(['id' => 'detalles_grid']); ?>
<?php
$columns = [
    [
        'class' => \kartik\grid\SerialColumn::className(),
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Código',
        'value' => 'planCuenta.cod_completo',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'label' => 'Cuenta',
        'value' => 'planCuenta.nombre',
        'contentOptions' => ['style' => 'font-size: 90%;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
//    [
//        'value' => 'subtotalFormat',
//        'label' => 'Subtotal',
//        'contentOptions' => ['style' => 'font-size: 90%;'],
//        'headerOptions' => ['style' => 'font-size: 90%;'],
//    ],
//    [
//        'value' => 'cta_contable',
//        'label' => 'Debe / Haber',
//        'contentOptions' => ['style' => 'font-size: 90%;'],
//        'headerOptions' => ['style' => 'font-size: 90%;'],
//    ],
    [
        'value' => 'subtotalDebe',
        'label' => 'Debe',
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'value' => 'subtotalHaber',
        'label' => 'Haber',
        'contentOptions' => ['style' => 'font-size: 90%; text-align: right;'],
        'headerOptions' => ['style' => 'font-size: 90%;'],
    ],
    [
        'value' => 'cta_principal',
        'label' => 'PRINCIPAL',
        'contentOptions' => ['style' => 'width: 50px; font-size: 90%; text-align: center;'],
        'headerOptions' => ['style' => 'font-size: 90%; text-align: center;'],
    ],
];
//$template = '{update} &nbsp {delete}';
$template = '{update}';
$buttons = [
    'delete' => function ($url, $model, $index) {
        return Html::a(
            '<span class="glyphicon glyphicon-trash"></span>',
            false,
            [
                'class' => 'ajaxDelete',
                'delete-url' => $url,
                'id' => $index,
                'title' => Yii::t('app', 'Borrar')
            ]
        );
    },
    'update' => function ($url, $model) {
        return Html::a(
            '<span class="glyphicon glyphicon-pencil"></span>',
            false,
            [
                'title' => 'Editar cuenta',
                'class' => 'tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => $url,
                'data-pjax' => '0',
                'data-title' => 'Editar'
            ]
        );
    }
];
array_push($columns, [
    'class' => ActionColumn::class,
    'template' => $template,
    'buttons' => $buttons,
    'urlCreator' => function ($action, $model, $key, $index) {
        if ($action === 'delete') {
            $url = Url::to(['detalle-compra/borrar', 'indice' => $index]);
            return $url;
        } elseif ($action === 'update') {
            $url = Url::to(['detalle-compra/modificar', 'indice' => $index]);
            return $url;
        }
        return '';
    },
    'contentOptions' => ['style' => 'font-size: 90%;'],
    'headerOptions' => ['style' => 'font-size: 90%;'],
]);
try {
    echo GridView::widget([
        'dataProvider' => Yii::$app->getSession()->get('cont_detallecompra-provider'),
        'columns' => $columns,
        'hover' => true,
        'id' => 'grid',
        'panel' => ['type' => 'primary', 'heading' => 'Detalles', 'footer' => false,],
        'toolbar' => [], /*[
            'content' => Html::button('<i class="glyphicon glyphicon-plus"></i>', [
                'type' => 'button',
                'id' => 'boton_add_detalle',
                'title' => 'Agregar Cuenta. Utilice el signo \'+\' para acceder.',
                'class' => 'btn btn-success tiene_modal',
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'data-url' => Url::to(['detalle-compra/add']),
                'data-pjax' => '0',
                'data-title' => 'Agregar cuenta'
            ]),
        ]*/
    ]);
} catch (Exception $e) {
    print $e->getMessage();
}

$sesion = Yii::$app->session;
$detalles = ($sesion->has('cont_detallecompra-provider') ? $sesion['cont_detallecompra-provider']->allModels : []);
$cant_detalles = sizeof($detalles);
$hayDetalles = empty($detalles) ? "no" : "si";
$total_debe = 0.0;
$total_haber = 0.0;
foreach ($detalles as $detalle) {
    if ($detalle->cta_contable == 'debe') {
        $total_debe += $detalle->subtotal;
    } else {
        $total_haber += $detalle->subtotal;
    }
}

$url_simular = Json::htmlEncode(\Yii::t('app', Url::to(['compra-nota/simular'])));

$script = <<<JS
// $('#compra-total-disp').val($total_haber>0.0? $total_haber : $total_debe);
// $('#compra-total').val($total_haber>0.0? $total_haber : $total_debe);

function simular() {
    if (mutex_simular === 'locked') return;
    mutex_simular = 'locked';
    
    var esNota = "$es_nota";
    $.ajax({
        url: $url_simular,
        type: 'get',
        data: {
            cotizacionFactura: $('#compra-cotizacion_factura_asociada').val(),
            cotizacionNota: $('#compra-cotizacion').val(),
            moneda_id: $('#compra-moneda_id').val(),
            cont_factura_total: $('#compra-total-disp').val(),
            plantilla_id: $('#compra-plantilla_id').val(),
            hoy: (esNota === 'nota_debito' ? $('#compra-emision_factura').val() : $('#compra-fecha_emision').val()),
            estado_comprobante: $('#compra-estado').val(),
            tipo_documento_id: ($('#compra-tipo').select2('data')[0]['id'] === "nota_credito" ? "{$tipodocNotaCreditoId}" : "{$tipodocNotaDebitoId}"),
            es_nota: esNota,
            factura_id: $('#compra-factura_compra_id').val()
        },
        success: function (data) {
            if (data !== "") {
                $.pjax.reload({container: "#asiento-simulator-div", async: false});
            }
            $.pjax.reload({container: "#flash_message_id", async: false});
            mutex_simular = 'unlocked';
        }
    });
}

simular();
$($('#grid')[0].getElementsByTagName('tr')).each(function () {
    if (typeof $(this)[0].getElementsByTagName('td')[0] !== 'undefined') {
        if ($(this)[0].getElementsByTagName('td').length > 3 && $(this)[0].getElementsByTagName('td')[3].textContent === '') {
            $($(this)[0].getElementsByTagName('td')[2]).css('text-align', 'right');
        }
    }
});
JS;
if ($cant_detalles > 0) {
    $this->registerJs($script);
}
?>
<?php Pjax::end() ?>