<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 05/07/2018
 * Time: 14:16
 */

use backend\modules\contabilidad\models\EmpresaObligacion;
use backend\modules\contabilidad\models\EmpresaSubObligacion;
use backend\modules\contabilidad\models\Obligacion;
use backend\modules\contabilidad\models\SubObligacion;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $dataProvider ActiveDataProvider */
?>

<?php try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'id' => 'empresa_obligaciones_grid',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'id' => 'empresa_obligaciones_pjax_grid'
            ],
            'loadingCssClass' => false,
        ],
        'panel' => [
            'type' => 'primary',
            'heading' => 'Obligaciones',
            'after' => '<em>* Para confirmar los datos de las obligaciones se debe guardar la empresa.</em>'
        ],
        'striped' => true,
        'hover' => true,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'headerOptions' => [
                    'class' => 'kartik-sheet-style',
                    'onchange' => '
                        $.ajax({
                            type: "POST",
                            url: "index.php?r=empresa/guardar-obligaciones-en-session&all=true",
                            data: {seleccionados: $("#empresa_obligaciones_grid").yiiGridView("getSelectedRows")},
                            success: function (data) {
                            }
                        });'
                ],
                'checkboxOptions' => function ($model) {
                    $empresa_obligaciones = EmpresaObligacion::findAll(['empresa_id' => \Yii::$app->session->get('core_empresa_en_edicion')]);
                    $checked = false;
                    foreach ($empresa_obligaciones as $empresa_obligacion) {
                        if ($model['id'] == $empresa_obligacion->obligacion_id) {
                            $checked = true;
                            break;
                        }
                    }

//                    $slices = explode('_', $model['id']);
//
//                    if (sizeof($slices) == 1) {
//                        $empresa_obligaciones = EmpresaObligacion::findAll(['empresa_id' => \Yii::$app->session->get('core_empresa_en_edicion')]);
//                        $checked = false;
//                        foreach ($empresa_obligaciones as $empresa_obligacion) {
//                            if ($slices[0] == $empresa_obligacion->obligacion_id) {
//                                $checked = true;
//                            }
//                        }
//                    } else {
//                        $empresa_sub_obligaciones = EmpresaSubObligacion::findAll(['empresa_id' => \Yii::$app->session->get('core_empresa_en_edicion')]);
//                        $checked = false;
//                        foreach ($empresa_sub_obligaciones as $empresa_sub_obligacion) {
//                            if ($slices[1] == $empresa_sub_obligacion->sub_obligacion_id) {
//                                $checked = true;
//                            }
//                        }
//                    }

                    return [
                        'onchange' => '
                            $.ajax({
                                type: "POST",
                                url: \'index.php?r=empresa/guardar-obligaciones-en-session&checked=\' + this.checked,
                                data: {id_registro_seleccionado: "' . $model['id'] . '"},
                                success: function (data) {
                                    console.log(data);
                                }
                            });',
                        'checked' => $checked, 'value' => $model['id']
                    ];
                },
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'nombre',
                'label' => 'Nombre',
                'value' => 'nombre',
                'hidden' => false,
            ],
            'descripcion',
        ],
        'toolbar' => []
    ]);
} catch (Exception $e) {
    print $e;
}
?>