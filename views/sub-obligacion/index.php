<?php

use backend\modules\contabilidad\models\Obligacion;
use backend\modules\contabilidad\models\SubObligacion;
use common\helpers\PermisosHelpers;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\contabilidad\models\search\SubObligacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sub-Obligaciones');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-obligacion-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= PermisosHelpers::getAcceso('contabilidad-sub-obligacion-create') ? Html::a(Yii::t('app', 'Crear Sub-Obligación'), ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php
    try {
        $template = '';
        $template .= PermisosHelpers::getAcceso('contabilidad-sub-obligacion-view') ? '{view} &nbsp&nbsp&nbsp' : '';
        $template .= PermisosHelpers::getAcceso('contabilidad-sub-obligacion-update') ? '{update} &nbsp&nbsp&nbsp' : '';
        $template .= PermisosHelpers::getAcceso('contabilidad-sub-obligacion-delete') ? '{delete} &nbsp&nbsp&nbsp' : '';
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'nombre',
                'descripcion',
                [
                    'attribute' => 'obligacion_id',
                    'value' => function ($model) {
                        return $model->obligacion->nombre;
                    },
                    'label' => 'Obligación'
                ],
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<label class="label label-' . (($model->estado == 'activo') ? 'success">Activo' : 'danger">Inactivo') . '</label>';
                    },
                    'contentOptions' => ['style' => 'text-align:center;'],
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'estado',
                        'data' => SubObligacion::getEstados(true),
//                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]),
                ],

                [
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center'],
                    'header' => 'Acciones',
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>
</div>
