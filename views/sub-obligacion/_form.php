<?php

use backend\modules\contabilidad\models\SubObligacion;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\SubObligacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-obligacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    try {
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 3,
            'attributes' => [
                'obligacion_id' => [
                    'type' => Form::INPUT_RAW,
                    'columnOptions' => ['colspan' => '1'],
                    'value' => $form->field($model, 'obligacion_id')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Seleccione una obligación...'],
                        'pluginOptions' => [
                            'data' => SubObligacion::getObligaciones(),
                            'allowClear' => false,
                        ],
                        'initValueText' => isset($model->obligacion_id) ? ucfirst($model->obligacion->nombre) : "",
                    ])->label('Obligación'),
                ],
                'nombre' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Nombre...'],
                ],
                'estado' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Select2::className(),
                    'options' => [
                        'data' => SubObligacion::getEstados(false),
                    ],
                ],
            ],
        ]);
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 5,
            'autoGenerateColumns' => false,
            'attributes' => [
                'descripcion' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => ['placeholder' => 'Descripción...'],
                ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    ?>

    <div class="form-group">
        <?php $class = Yii::$app->controller->action->id == 'create' ? 'btn btn-success' : 'btn btn-primary';
        echo Html::submitButton('Guardar', ['class' => $class]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
