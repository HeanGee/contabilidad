<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\SubObligacion */

$this->title = Yii::t('app', 'Crear Sub-Obligacion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sub-Obligaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-obligacion-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
