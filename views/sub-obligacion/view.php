<?php

use common\helpers\PermisosHelpers;
use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contabilidad\models\SubObligacion */

$this->title = 'Detalles de la sub-obligación ' . ucfirst($model->nombre);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sub Obligacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sub-obligacion-view">
    <p>
        <?= PermisosHelpers::getAcceso('') ? Html::a('Ir a Sub-Obligaciones', ['index'], ['class' => 'btn btn-info']) : '' ?>
        <?= PermisosHelpers::getAcceso('') ? Html::a(Yii::t('app', 'Modificar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= PermisosHelpers::getAcceso('') ? Html::a(Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Realmente desea BORRAR esta sub-obligación?'),
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'condensed' => true,
            'hover' => true,
            'mode' => DetailView::MODE_VIEW,
            'enableEditMode' => false,
            'fadeDelay' => true,
            'panel' => [
                'heading' => 'Datos',
                'type' => DetailView::TYPE_INFO,
            ],
            'attributes' => [
                'nombre',
                'descripcion',
                [
                    'attribute' => 'obligacion_id',
                    'value' => $model->obligacion->nombre,
                    'label' => 'Obligación'
                ],
                [
                    'attribute' => 'estado',
                    'format' => 'raw',
                    'value' => ($model->estado == 'activo') ? "<label class='label label-success'>$model->formattedEstado</label>" : "<label class='label label-danger'>$model->formattedEstado</label>",
                ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
