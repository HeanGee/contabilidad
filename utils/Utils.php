<?php

use backend\models\Moneda;
use backend\modules\contabilidad\models\DetalleVenta;
use backend\modules\contabilidad\models\IvaCuenta;
use backend\modules\contabilidad\models\PlantillaCompraventa;
use backend\modules\contabilidad\models\PlantillaCompraventaDetalle;
use backend\modules\contabilidad\models\TipoDocumento;
use backend\modules\contabilidad\models\Venta;
use common\helpers\FlashMessageHelpsers;

/**
 * @param $detalles DetalleVenta[]
 * @param $tipoDoc TipoDocumento
 * @param $total_factura
 * @param $array_totales_por_iva
 * @param $porcentajes_iva
 * @param $plantillas PlantillaCompraventa[]
 * @param $moneda_id
 * @return mixed
 */
function genDetallesByDefault($detalles, $tipoDoc, $total_factura, $totales_valor, $porcentajes_iva, $plantillas, $moneda_id)
{
    /** @var PlantillaCompraventaDetalle[] $plantilla_detalles */
    /** @var IvaCuenta $iva_cta */
    $es_nota_credito = Yii::$app->request->post('es_nota') == 'nota_credito';
    $moneda = Moneda::findOne(['id' => $moneda_id]);

//        if ($total_factura == 0) {
//            $detalles = [];
//            goto retorno;
//        }

    // Crear detalle por Tipo de Documento
    $cuenta_debe = $tipoDoc != null ? $tipoDoc->cuenta : null;
    if (isset($cuenta_debe)) {
        /* Al elegir tipo_documento, SIEMPRE se trae 1 sola cuenta. */
        $detalle = new DetalleVenta();
        $detalle->cta_contable = !$es_nota_credito ? 'debe' : 'haber';
        $detalle->cta_principal = 'no';
        $detalle->plan_cuenta_id = $cuenta_debe->id;
        $detalle->subtotal = numberFormatUseDotByMoneda($total_factura, $moneda); // TODO: averiguar que valor debe tomar, dependiendo del tipo_documento.

        array_push($detalles, $detalle);
    }

    $total = 0;
    foreach ($plantillas as $plantilla) {
        // Crear detalles segun detalles de la plantilla.
        $plantilla_detalles = $plantilla->getPlantillaCompraventaDetalles()->all();
        $gravada_total = 0.0;
        $mapeo_inverso = ['debe' => 'haber', 'haber' => 'debe'];
        $descuento = $plantilla->isForDiscount();
        // TODO: Aclarar con el cliente sobre como desea ver los costos de venta. Suma de costos o por venta gravada.
        foreach ($plantilla_detalles as $p_detalle) {
            if ($p_detalle->tipo_asiento == 'venta') {
                $i = 0;
                foreach ($totales_valor[$plantilla->id] as $total_iva) {
                    if ($p_detalle->ivaCta != null && $total_iva !== '' && round($total_iva, 2) > 0.0 &&
                        $p_detalle->ivaCta->iva->porcentaje == $porcentajes_iva[$i]) {
                        // detalle por cuenta mas importante (ventas de merca, ingresos varios, etc)
                        $detalle = new DetalleVenta();
                        $detalle->plan_cuenta_id = $p_detalle->pCGravada->id;
                        $detalle->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
                        $detalle->cta_principal = $p_detalle->cta_principal;
                        $detalle->subtotal = round($total_iva / (1.00 + ($p_detalle->ivaCta->iva->porcentaje / 100.0)), 2);
                        $detalle->subtotal = numberFormatUseDotByMoneda($detalle->subtotal, $moneda);
                        $detalle->subtotal *= ($descuento ? -1 : 1);
                        $gravada_total += round($detalle->subtotal, 2);
                        // detalle por iva x%
                        $detalle2 = new DetalleVenta();
                        $detalle2->plan_cuenta_id = $p_detalle->ivaCta->cuentaVenta->id;
                        $detalle2->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
//                            $detalle2->cta_principal = $p_detalle->cta_principal;
                        $detalle2->cta_principal = 'no'; // normalmente los ivas asociados a la cta gravada no son principales.
                        $detalle2->subtotal = round($total_iva * ($descuento ? -1 : 1), 2) - round($detalle->subtotal, 2);
                        $detalle2->subtotal = numberFormatUseDotByMoneda($detalle2->subtotal, $moneda);
                        array_push($detalles, $detalle);
                        array_push($detalles, $detalle2);
                        $total += round($detalle->subtotal + $detalle2->subtotal, 2);
                        break;
                    } elseif ($p_detalle->ivaCta == null && $total_iva !== '' && round($total_iva, 2) > 0.0 &&
                        $porcentajes_iva[$i] == '0') // TODO: aqui falta definir si habra mas detalles de plantilla para asiento de venta con ivacta = null
                    {
                        $detalle = new DetalleVenta();
                        $detalle->plan_cuenta_id = $p_detalle->pCGravada->id;
                        $detalle->cta_contable = !$es_nota_credito ? $p_detalle->tipo_saldo : $mapeo_inverso[$p_detalle->tipo_saldo];
                        $detalle->cta_principal = $p_detalle->cta_principal;
                        $detalle->subtotal = round($total_iva, 2);
                        $detalle->subtotal = numberFormatUseDotByMoneda($detalle->subtotal, $moneda);
                        $detalle->subtotal *= ($descuento ? -1 : 1);
                        array_push($detalles, $detalle);
                        $total += round($detalle->subtotal, 2);
                        break;
                    }
                    $i++;
                }
            }
        }
    }

    $detalles[0]->subtotal = $total; // cuenta de entrada, unica.
    $detalles = array_values($detalles);

    retorno:;
    return $detalles;
}

/**
 * @param $value
 * @param $moneda Moneda|int
 * @return string
 */
function numberFormatUseDotByMoneda($value, $moneda)
{
    if (!($moneda instanceof Moneda)) {
        $moneda = Moneda::findOne(['id' => $moneda]);
    }

    if ($moneda->id == 1) {
        return number_format((float)$value, '0', '.', '');
    }

    return $value;
}

/**
 * @param DetalleVenta[] $array Detalles de venta
 * @return array Detalles de venta agrupados en debe / haber
 */
function agruparDebeHaber($array) // TAMBIEN ESTA EN Venta.php PARA USARLO DESDE ASIENTOCONTROLLER. ES EXACTAMENTE IGUAL
{
    $debes = [];
    $haberes = [];

    foreach ($array as $item) {
        if ($item->cta_contable == 'debe') {
            $debes[] = $item;
        } elseif ($item->cta_contable == 'haber') {
            $haberes[] = $item;
        }
    }

    return array_values(array_merge($debes, $haberes));
}

/**
 * @param DetalleVenta[] $detalles
 * @param TipoDocumento $tipoDocumento
 */
function invertirPartidas(&$detalles, $tipoDocumento)
{
    try {
        $TDS_NotaCredito = Venta::getTipodocSetNotaCredito();
        if ($tipoDocumento->tipo_documento_set_id == $TDS_NotaCredito->id) {
            $map = ['debe' => 'haber', 'haber' => 'debe'];
            foreach ($detalles as $key => $_) {
                $detalles[$key]->cta_contable = $map[$_->cta_contable];
            }
            $detalles = agruparDebeHaber($detalles);
        }
    } catch (\Exception $exception) {
        FlashMessageHelpsers::createWarningMessage($exception->getMessage());
    }
}

/**
 * @param $pl array
 * @return float
 */
function sumAllIvas($pl)
{
    $sum = 0.0;
    foreach ($pl as $key => $value) {
        if ($key != 'plantilla_id') $sum = (float)$sum + (float)$value;
    }
    return $sum;
}